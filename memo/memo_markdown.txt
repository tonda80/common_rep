head1
=====

Для просмотра маркдаун текста в Geany Markdown Preview в Sidebar-е надо включить плагин Markdown
и сделать Document->Set File Type->Markdown Source File или проще расширение файла должно быть md.
В atom надо нажать ctrl-shift-m
В apt из коробки есть retext

head2
------

*курсив*  
2 пробела в конце строки - перевод строки  
**жирный шрифт**



# head 1 also
## head 2 also
#### head 4
###### head 6


* Red
  * розовый
  * бордовый
    * идея в общем понятна
* Green
* Blue

or

+ Red
+ Green
+ Blue

or

-  Red
-  Green
-  Blue

also

1. one
1. two
1. three

[Яндекс](https://yandex.ru)
[Markdown official doc][tag_doc1]
[Маркдаун вики][tag_doc2]

> Цитата

>Тоже цитата

Уже нет

Опять обычный текст

далее блок кода

	// Code example
	#include <iostream>

	int main()
	{
		std::cout << "Hello bear";
		return 0;
	}

![](http://www.zooclub.ru/skat/img.php?w=700&h=700&img=./attach/12000/12669.jpg)

[wiki page ][tag_doc]

[и еще какая то дока по маркдауну](http://daringfireball.net/projects/markdown/syntax#precode "тут хинт")

[и ещё](https://www.markdownguide.org/basic-syntax/)

[tag_doc]:https://ru.wikipedia.org/wiki/Markdown "Хинт что это маркдаун вики"
