Тут все что относится к программированию скриптов: баш ну и такие вещи как sed с awk. Разные полезные линукс команды для работы в консоли в memo_linux

в начале файла шебанг и установка параметров
--
#!/usr/bin/env bash
#!/bin/bash
set -e			стоп после ошибки
set -x			печать команды
set -xe			можно так


Памятка по базовым конструкциям
---
* ( cmd1 ; cmd2 )		- группировка комманд и запуск в отдельном шелле
* { cmd1 ; cmd2 }		- группировка комманд и запуск в этом же шелле (**обязательно ; в конце**)
* VAR1=1 VAR2=2 cmd		- установка переменных окружения для команды
* cmd 2>&1			- stderr to stdout
* cmd &>log.txt		- stderr and stdout to log.txt
* cmd > /dev/null 2>&1		- нет вывода вообще
* [ "qw" = "q" ] ; echo $?
пример условия
* ret=0; cnt=0; while [ $ret -eq 0 ]; do SOME_TEST; ret=$?; cnt=`expr $cnt + 1`; done; echo $cnt
Пример цикла
* пример case в shell/go
* $1, $2 аргументы ком. строки
цикл по файлам в директории
* for f in *.class; do echo $f; done
пример цикла (подсчет содержимого в архивах)
* for apk in *.apk; do unzip -l $apk | grep base_mm | wc -l ; done
* set  -e выход, при ошибке  -x печать команды
* функции
test_func() { echo $1 $2; }
mkcd() {
	mkdir "$1" && cd "$1"
}
* brace expansion
echo a{b,c{1,2,3},d}e  >  abe ac1e ac2e ac3e ade


Замена строк
--
V1='qw1er2ty9'
echo "${V1/1/Q}"	-> qwQer2ty9	замена первого
echo "${V1//[0-9]/Q}"	-> qwQerQtyQ	замена всех с регуляркой
K=`openssl rand -base64 12` && K=${K//[=\/+]/0} && echo $K	генерим секрет из которого исключаем =/+ символы


Разные приемы
--
CURDIR=$(dirname "$0")
CURDIRABS=$(cd "$CURDIR" && pwd)


sed
---
sed 's/old/new/flag'		замена old на new
пишут, что можно вместо / использовать и другие символы (первое встреченное после s)


awk
---
https://www.tutorialspoint.com/awk/awk_quick_guide.htm
По дефолту команды выполняются над каждой строчкой входных данных.
BEGIN{awk-commands} и END{awk-commands} выполняются однократно в начале\конце
awk '{print}' in_file    напечатает каждую строку
awk '{print $3 $4}' in_file    напечатает 3, 4 столбцы каждой строки
$0  все столбцы
kill `ps -ef | grep "[p]ython3 e_ctxmgr.py" | awk '{print $2}'`    найти id процесса по команде
-f fname  можно взять команды из файла
-v v=1  задать переменную


Полезные ссылки
---
* [Отлично изложенные основы программирования](http://linuxgeeks.ru/bash-2.htm)
* х[Док по Parameter Expansion, магии убирающей расширение "${f%.*}"] (http://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html)
* [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/)
* [Advanced Bash-Scripting Guide перевод](http://rus-linux.net/MyLDP/BOOKS/abs-guide/flat/abs-book.html)
