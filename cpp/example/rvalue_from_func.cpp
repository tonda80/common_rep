#include "../common.h"


struct C {
    C(int i_ = 0) : i(i_) {pp("C", i);}
    ~C() {pp("~C", i);}
    C(const C& o) : i(o.i) {pp("C&", i);}
    C(C&& o) : i(o.i) {
        o.i = -1;
        pp("C&&", i);
    }
    C& operator=(const C& o) {
        i = o.i;
        pp("C=", i);
        return *this;
    }
    C& operator=(C&& o) {
        i = o.i;
        o.i = -1;
        pp("C=&&", i);
        return *this;
    }

    void do_smth() const {pp("C::do_smth", i);}

    int i;
};


C gc(99);


C retf0() {		// RVO
    C c1(1);
    return c1;
}

C retf1(bool b=true) {	// broken RVO
    C c1(1);
    C c2(2);
    if (b)
        return c1;
    return c2;
}

C& retf2() {
    return gc;
}

C&& retf30() {
    return std::move(gc);
}

//C&& retf31() {
//    C c3(3);
//    return std::move(c3);	// compilation warning
//}


void i_want_you_move_it(C&& c) {
    pp("i_want_you_move_it", c.i);
}

int main(int argc, const char** argv) {

    //C c0 = retf0();	// RVO
    //C c1 = retf1();	// сломанный RVO дает move!
    //C& c0 = retf1();	// compilation error


    //C c20 = retf2();
    //c20.i = 20;
    //C& c21 = retf2();
    //c21.i = 21;
    //i_want_you_move_it(c21);	// compilation error


    //C c30 = retf30();
    //c30.i = 30;
    // C& c30 = retf30();	// compilation error

    //retf31();	// ок
    //C c31 = retf31();		// segfault
    //retf31().do_smth();	// segfault


    //i_want_you_move_it(c1);		// compilation error
    //i_want_you_move_it(retf0());	// ok
    //i_want_you_move_it(retf1());	// ok с перемещением
    //i_want_you_move_it(retf2());	// compilation error!
    i_want_you_move_it(retf30());	// ok



    return 0;
}
