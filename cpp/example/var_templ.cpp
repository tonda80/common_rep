// variadic templates
// https://en.cppreference.com/w/cpp/language/parameter_pack
// https://habr.com/ru/post/228031/



#include <iostream>
#include <stdexcept>

using namespace std;


// example 0
// просто печатает все аргументы для теста

template <typename T>               // сперва финальный вызов
void just_print_all(const T& arg) {
    std::cout << arg << std::endl;
}

void just_print_all() {}            // и чтоб можно было не передавать ничего

template <typename T, typename... Args>
void just_print_all(const T& arg, const Args&... args) {
    std::cout << arg << " ";
    just_print_all(args...);
}

// example 1
// складываем разное

template<typename T>
T sum(T value) {return value;}

template<typename T, typename... Args>
T sum(T first, Args... args) {return first + sum(args...);}


// example 2
// можно передать в обычные функции

template <typename... Args>
const char* makeString(const char* fmt,  const Args&... args)
{
    static char buf[100];
    snprintf(buf, sizeof(buf), fmt, args...);
    return buf;
}

// example 3
// это видимо, то что в common.h

void print(const char* s) {
  while (*s) {
    if (*s == '%' && *++s != '%')
      throw std::runtime_error("invalid format string: missing arguments");
    std::cout << *s++;
  }
}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args) {
  while (*s) {
    if (*s == '%' && *++s != '%') {
        // ignore the character that follows the '%': we already know the type!
        std::cout << value;
        return print(++s, args...);
    }
    std::cout << *s++;
  }
  throw std::runtime_error("extra arguments provided to print");
}


// example 4
// можно применять функцию к эллипсису, комбинировать паки  и узнать количество элементов
int mul2(int i) {return 2*i;}

template <typename... Args>
void print_mul2_integers(const Args&... args) {
    std::cout << "Got " << sizeof...(args) << " arguments" << std::endl;
    just_print_all(mul2(args)..., args...);
}

// example 5
// подсмотрено в isp. инициализация класса keyword аргументами, как в питоне
// для распаковки нужен --std=c++17

struct S {
    int iattr = 0;
    std::string sattr;
    bool battr = false;
    // не стал думать как закрыть атрибуты, но дать доступ KwHelper-ам

    template <typename... Args>
    S(const Args&... args) {
        ( (args(this), 1) && ... );
    }

    void print_attrs() const {
        print("iattr=%x, sattr=%x, battr=%x\n", iattr, sattr, battr);
    }
};

namespace kw {

#define REGISTER_S_ATTR(ATTR) struct KwHelper##ATTR { \
    template <typename T> \
    constexpr auto operator=(const T& v) const { \
        return [v](S* self) {self->ATTR = v;}; \
    } \
} constexpr ATTR
// constexpr auto operator= (const decltype(S::ATTR)& v) const
// можно сделать такой оператор, но оно не работает со string аттрибутом, ругается на нетривиальный деструктор
// c template ok, потому что ламбда захватывает const char*

REGISTER_S_ATTR(iattr);
REGISTER_S_ATTR(sattr);
REGISTER_S_ATTR(battr);

} // namespace kw


int main(int argc, char** argv)
{
    just_print_all(1, "qwerty", 3.33, true);

    print("I can print like as printf! %z %z %z %%\n", 4, "some text", 3.25);

    print("1+2+3+4 == %z\n", sum(1,2,3,4));
    print("%z\n", sum(string("Hello"), string(", "), string("world!")));
    cout << sum<string>("hippo","potamus ") << endl;

    cout << "makeString(\"%3d %s %f\") => " << makeString("%3d %s %f", 77, "some string", 1.234) << endl;

    print_mul2_integers(1, 2, 4);

    S(kw::battr = true, kw::iattr = 7, kw::sattr="??").print_attrs();
    S(kw::sattr = "wow!", kw::iattr = 33).print_attrs();
    S().print_attrs();


    return 0;
}
