#include "../common.h"


using ull = unsigned long long int;


int operator "" _kilo(unsigned long long int v) {		// литерал для целочисленных типов принмает только ull, но возвращает что угодно (только long double для float итд)
	return v*1000;
}

constexpr long double operator "" _deg ( long double deg )
{
    return deg * 3.14159265358979323846264L / 180;
}


// литералы создающие тип, и можно даже запретить иное создание
class Parrot {
public:
	friend Parrot operator "" _parrots(ull);
	friend std::ostream& operator<< (std::ostream&, const Parrot&);

	auto get_value() const {return value;}

private:
	ull value;

	Parrot(ull v) : value(v) {}
};

Parrot operator "" _parrots(ull v) {
	p("some side effect (I like to create parrots!!)");
	return Parrot(v);
}

std::ostream& operator<< (std::ostream& out, const Parrot& p) {
	return out << p.value << " parrots";
}


int main(int argc, char** argv)
{
	P2(sizeof(ull));

	int i = 12_kilo;
	P2(i);

	auto rad_1 = 57.0_deg;	// причем 57_deg не работает
	P2(rad_1);

	// error => Parrot length(38);
	Parrot boa_length(38_parrots);
	pp("The boa has length", boa_length);


    return 0;
}
