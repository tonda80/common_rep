#include <iostream>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

template<typename T> void f(T value)
{
	if constexpr(std::is_same<T, int>()) {
		p("int");
	}
	else if constexpr(std::is_convertible<T, double>()) {
		p("double");
	}
	else if constexpr(std::is_convertible<T, std::string>()) {
		p("string");
	}
	else {
		p("unknown");
	}
}

int main()
{
    f(2.1);
    f(2);
    f("2");
    f(std::string("2"));

	return 0;
}
