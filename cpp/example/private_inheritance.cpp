#include "../common.h"


class B {
    public:
	void f() {
	    p("B::f");
	    intf();
	}
	virtual void intf() {p("B::intf");}
    protected:
	std::string bm = "bm";
};

void do_f(B* o) {o->f();}


class D : private B {
    public:
	D() : s("cn1") {
	    p("D::D");
	    do_f(this);				// а тут ОК
	    s = "cn2";
	}

	void df() {
	    do_f(this);
	}

	void check(D* ptr) {p(ptr == this);}
    private:
	std::string s = "df";
	virtual void intf() {p("D::intf "+s+bm );}
};



int main(int argc, char** argv)
{
    B b;
    b.f();
    do_f(&b);

    D d;
    d.df();
    d.check(&d);
    //do_f(&d);				// а тут не ОК, ошибка!!
    do_f((B*)&d);			// но c cast таки возможен (вроде только он)


    return 0;
}
