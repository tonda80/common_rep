#include "../common.h"

#include <fstream>

/*
на gcc7 надо использовать experimental/filesystem, флаг линкеру -lstdc++fs
#include <experimental/filesystem>
std::experimental::filesystem::rename(argv[1], argv[2]);

пример развязки
*/
#if __GNUC__ == 7
#include <experimental/filesystem>
#define _filesystem std::experimental::filesystem
#else
#error "put your code here"
#endif


// если filesystem недоступен

std::ifstream::pos_type filesize(const std::string& filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg();
}

bool file_exists(const std::string& filename) {
    return std::ifstream(filename).good();
}

using namespace std;

int main(int argc, char** argv)
{
	auto cur_dir_path = _filesystem::canonical(".");
	pp("current dir =", cur_dir_path);
	auto self_path = cur_dir_path/"file_operations.cpp";
	pp("self path =", self_path, _filesystem::is_regular_file(self_path));

	std::string pth;
	while (1) {
		std::cin >> pth;
		p(file_exists(pth));
		p(filesize(pth));
		p(_filesystem::file_size(pth));			// это падает, если нет файла
	}

    return 0;
}
