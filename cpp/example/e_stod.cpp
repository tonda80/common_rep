double to_double(const std::string& s, std::optional<double> default_= std::nullopt) {
    std::size_t pos = 0;
    double ret = 0;
    try {
        ret = std::stod(s, &pos);
    }
    catch (...) {}
    if (pos == 0 || pos != s.size()) {
        if (default_) {
            return *default_;
        }
        throw std::runtime_error("is not double ");
    }
    return ret;
}
