#include "../../common_rep/cpp/common.h"


int main(int argc, const char** argv)
{

	std::map<int, int> mm = {{1,11}, {3, 33}, {2, 22}};
	p_map(mm);

	/*
	for (const auto& [k, v] : mm) {
		if (k == 3) {
			mm.erase(3);
			pp("removed", k);
			continue;
		}
		pp(k, v);
	}
	*/
	for (auto it = mm.cbegin(); it != mm.cend(); ) {
		auto k = it->first; auto v = it->second;
		if (k == 3) {
			it = mm.erase(it);
			pp("removed", k);
			continue;
		}
		pp(k, v);
		++it;
	}

	p_map(mm);


	return 0;
}
