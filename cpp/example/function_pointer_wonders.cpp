#include "../common.h"


// нельзя преобразовать к указателю на функцию, что-то имеющее данные
// https://stackoverflow.com/questions/13238050/convert-stdbind-to-function-pointer
// https://devblogs.microsoft.com/oldnewthing/20040209-00/?p=40713
// а вот почему это так разберется более умный поздний я.. пока просто запомним это как данность


void i_want_func_clb(void(*f)(int), int i) {
	f(i);
}

template<typename F>
void i_want_any_clb(F&& f, int i) {
	f(i);
}

void gsf(int i) {
	pp("gsf", i);
}

void for_bind_f(const std::string& s, int i) {
	pp(s, i);
}

class C {
public:
	C() {p("C");}
	int mem = 7;
	void uf(int i) {}
	// типа обычный класс

	static void cf(int i) {
		pp("static", i);
	}
};



int main(int argc, const char** argv) {

	i_want_func_clb(gsf, 1);

	int a = 3;
	i_want_func_clb([](int i) {pp("lambda", i);}, 2);
	//ERROR i_want_func_clb([a](int i) {pp("lambda", i+a);}, 2);
	i_want_any_clb([a](int i) {pp("lambda", i+a);}, 2);

	//ERROR i_want_func_clb(std::bind(for_bind_f, "bind", std::placeholders::_1), 3);
	i_want_any_clb(std::bind(for_bind_f, "bind", std::placeholders::_1), 3);

	i_want_func_clb(C::cf, 7);

	return 0;
}
