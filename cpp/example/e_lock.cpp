#include "../common.h"

// g++ -std=c++17 e_lock.cpp -lpthread


class S {
	std::mutex mtx;
public:
	std::lock_guard<std::mutex> lock_guard() {
        return std::lock_guard<std::mutex>(mtx);
    }
};


void f_out(char c, S& s) {
	auto grd = s.lock_guard();
	//auto grd2 = s.lock_guard();	// recursive_mutex чтобы делать так
	for (int i=0; i < 1000; ++i) {
		std::cout << c;
	}
	std::cout << std::endl;
}


int main(int argc, char** argv)
{
	S s;

	std::list<std::thread> threads;

	for (const auto& c : {'1', '2', '3'}) {
		threads.push_back(std::thread(f_out, c, std::ref(s)));
	}

	for (auto& t : threads) {
		t.join();
	}

    return 0;
}
