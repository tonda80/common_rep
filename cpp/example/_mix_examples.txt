long _timestamp_sec() {
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

--

int rand(int min, int max) {
    return rand()%(max - min) + min;	// <cstdlib>
}

double _random_double() {
    std::srand(_timestamp_sec());		// если такую ф-ю вызывать в одну секунду, то результат будет одинаковым :)

    assert(RAND_MAX > 100000);
    int r = std::rand()%100000;
    r *= r%2 ? -1 : 1;
    return static_cast<double>(r)/1000;     // +-xx.yyy
}

--

template <std::size_t N>
void i_know_arr_size(const char (&c)[N]) {
	std::cout << N << std::endl;
}

--

template<typename T> struct TC {
	T m;
	TC(const T& m_) : m(m_) {}
	TC() = default;
	void f() {pp("TC::f", m);}
};

TC<int>(1).f();		// OK
TC(2).f();			// OK для с++17
TC<int>().f();		// OK
//TC().f();			// FAIL

--

bool startswith(const std::string& str, const std::string& what) {
	return str.find(what) == 0;
}

bool endswith(const std::string& str, const std::string& what) {
	return str.rfind(what) == str.size() - what.size();
}

template <typename T>
int index_of(const std::vector<T>& vect, const T& val) {
	auto it = std::find(vect.begin(), vect.end(), val);
	if (it == vect.end()) {
		return -1;
	}
	return it - vect.begin();
}


windows get current directory
-------------------------------

#include "windows.h"
#include <iostream>
void pwd() {
	TCHAR __buffer[MAX_PATH];
	GetCurrentDirectory(sizeof(__buffer), __buffer);
	std::cout << "I am here: " << __buffer << std::endl;
}

endiannes
---------
const char* test_endian() {
    uint16_t i = 1;
    return (*(uint8_t*)&i == 1) ? "little-endian" : "big-endian";
}

range-based loop for a map
--------------------------
for (auto& kv : myMap) {
    std::cout << kv.first << " has value " << kv.second << std::endl;
}

startswith
----------
bool startswith(const std::string& str, const std::string& what) {
	return str.rfind(what, 0) == 0;
}

cross-platform rand
-------------------
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

struct Rand
{
	Rand() {srand (time(NULL));}
	int rand(int m) {return rand()%m;}
};

  /* initialize random seed: */
  srand (time(NULL));

  /* generate secret number between 1 and 10: */
  iSecret = rand() % 10 + 1;

cross-platform sleep,
it's better to use c++11 for that
std::this_thread::sleep_for(std::chrono::milliseconds(x));
-----------------------------------------------
std::this_thread::sleep_for(std::chrono::seconds(1));
OR
#if defined (_WIN32)
#include <Windows.h>
#define SLEEP Sleep(1000)
#else
#include <unistd.h>
#define SLEEP sleep(1)
#endif  // defined (_WIN32)

#if defined (_WIN32)
#include <Windows.h>
#define SLEEP(t) Sleep((t)*1000)
#else
#include <unistd.h>
#define SLEEP(t) sleep(t)
#endif  // defined (_WIN32)

ms ticker for c++11
----------------------------------
class Timer
{
    int curr;
    int getTime() { return std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch()).count(); }
public:
    Timer() : curr(getTime()) {}
    int count() {
        int curr_ = curr;
        curr = getTime();
        return curr - curr_;
    }
};

time(NULL) in c++11
-----------------------
std::chrono::duration_cast< std::chrono::seconds >(std::chrono::system_clock::now().time_since_epoch()).count()
chrono::milliseconds d(3600000);
p(d.count());
p((chrono::duration_cast<chrono::hours>(d)).count());


// ------------------------
// helper
time_t get_utc_offset()
{
    time_t ref_p = 1000000;
    return mktime(gmtime(&ref_p)) - ref_p;
}

// parses string like "2016-02-05 09:49:58.211556+00"
uint64_t timestamp_to_time(const std::string& ts)
{
    if (ts.size() != 29)
        return 0;

    struct std::tm tm_obj = { 0 };
    int usec = 0;
    try {
        tm_obj.tm_year = std::stoi(ts.substr(0, 4)) - 1900;
        tm_obj.tm_mon = std::stoi(ts.substr(5, 2)) - 1;
        tm_obj.tm_mday = std::stoi(ts.substr(8, 2));
        tm_obj.tm_hour = std::stoi(ts.substr(11, 2)) - std::stoi(ts.substr(26, 3));
        tm_obj.tm_min = std::stoi(ts.substr(14, 2));
        tm_obj.tm_sec = std::stoi(ts.substr(17, 2)) - get_utc_offset();
        usec = std::stoi(ts.substr(20, 6));
    }
    catch (std::invalid_argument& e) {
        return 0;
    }

    time_t t = mktime(&tm_obj);
    if (t == -1)
        return 0;

    return static_cast<uint64_t>(t) * 1000000 + usec;
}



// роимер шаблоннизиремого строкой класса
template<char* S>
struct OneSymbolLex : public Lex {
    OneSymbolLex(LexType t, char c) : Lex(t), symbol(c) {}

    static bool can_be_created(StrIter it) {
        for (char* c = S; *c != '\0'; ++c) {
            if (*it == *c) {
                ++it;
                return true;
            }
        }
        return false;
    }
    const char symbol;
};

// и вот так используем
char tchar[] = "qwerty";		// это долно быть в глобальной видимости

class Z : public OneSymbolLex<tchar>
{};
