#ifndef IGNORED_WARNINGS_H
#define IGNORED_WARNINGS_H

#pragma GCC diagnostic ignored "-Wweak-vtables"
#pragma GCC diagnostic ignored "-Wpadded"


// #if defined(__GNUC__) 	такого типа макросы пригодятся
// #if __GNUC__ == 7


// для qtcreator это стоит добавить в *project.config

#endif // IGNORED_WARNINGS_H
