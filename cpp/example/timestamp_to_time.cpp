#include <iostream>

#include <string>
#include <ctime>
#include <stdexcept>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

void tm_test()
{
    std::tm tt1 = {0};
    tt1.tm_year = 70;
    tt1.tm_mday = 1;
    tt1.tm_hour = 5;
    tt1.tm_min = 50;

    std::tm tt2 = tt1;
    tt2.tm_min += 20;

    std::tm tt3 = tt1;
    tt3.tm_min -= 80;

    p(tt1.tm_min);
    p(tt2.tm_min);
    p(tt3.tm_min);

    p(mktime(&tt1));
    p(mktime(&tt2));
    p(mktime(&tt3));

    p(tt1.tm_min);
    p(tt2.tm_min);
    p(tt3.tm_min);
}

time_t get_utc_offset()
{
    time_t ref_p = 1000000;
    return mktime(gmtime(&ref_p)) - ref_p;
}

// parses string like "2016-02-05 09:49:58.211556+00";
uint64_t timestamp_to_time(const std::string& ts)
{
    if (ts.size() != 29)
        return 0;

    struct std::tm tm_obj = {0};
    int usec;
    try {
        tm_obj.tm_year = std::stoi(ts.substr(0, 4)) - 1900;
        tm_obj.tm_mon = std::stoi(ts.substr(5, 2)) - 1;
        tm_obj.tm_mday = std::stoi(ts.substr(8, 2));
        tm_obj.tm_hour = std::stoi(ts.substr(11, 2)) - std::stoi(ts.substr(26, 3));
        tm_obj.tm_min = std::stoi(ts.substr(14, 2));
        tm_obj.tm_sec = std::stoi(ts.substr(17, 2)) - get_utc_offset();
        usec = std::stoi(ts.substr(20, 6));
    }
    catch (std::invalid_argument& e) {
        return 0;
    }

    time_t t = mktime(&tm_obj);
    if (t == -1)
        return 0;

    return static_cast<uint64_t>(t)*1000000 + usec;
}

// ttrt time (uSec from the epoch time) to FIX time (YYYYMMDD-HH:MM:SS.sss)
std::string fixed64ToUTCTimestamp(const uint64_t& val)
{
    int ms = (val % 1000000)/1000;
    time_t sec = static_cast<time_t>(val / 1000000);

    char buf[30];
    strftime(buf, sizeof(buf), "%Y%m%d-%H:%M:%S", gmtime(&sec));
    std::string tstamp(buf);
    tstamp += "." + std::to_string(ms);
    return tstamp;
}

int main(int argc, char** argv)
{
    p(timestamp_to_time("q") == 0);
    p(timestamp_to_time("2z16-02-05 09:49:58.211556+00") == 0);

    p(timestamp_to_time("1970-01-01 00:00:00.123456+00") == 123456);
    p(timestamp_to_time("1970-01-01 05:00:00.654321+03") == 7200654321LL);
    p(timestamp_to_time("1970-01-01 01:00:00.222777-11") == 43200222777LL);
    p(timestamp_to_time("2016-02-05 09:49:58.211556+00") == 1454665798211556LL);
    // d = (2016-1970)*365 + 11; s = d*24*3600 + 31*24*3600 + 4*24*3600 + 9*3600 + 49*60 + 58

    return 0;
}
