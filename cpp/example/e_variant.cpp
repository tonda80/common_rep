#include "../common.h"

#include <variant>

int main(int argc, char** argv)
{
	p(sizeof(std::variant<char>));
    p(sizeof(std::variant<double>));
    p(sizeof(std::variant<double, int, float, char, bool>));
    p(sizeof(double));
    p("--");

    std::variant<int, double> v, w;
    v = 12;
    w = 1.234;

    p(std::get<int>(v));
    p(std::get<0>(v));

	p(std::get<double>(w));
    p(std::get<1>(w));

    // throws std::bad_variant_access
    //p(std::get<double>(v));
    //p(std::get<int>(w));

    return 0;
}
