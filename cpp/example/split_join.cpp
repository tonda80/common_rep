#include "common.h"

struct C {
	C() {p("C");}
	~C() {p("~C");}
	C(C&) {p("C&");}
	C(C&&) {p("C&&");}
	void do_smth() const {p("C::do_smth");}
};

std::vector<std::string> split(const std::string& src, const std::string& delim) {
    std::vector<std::string> res;

    size_t delim_len = delim.length();
    size_t pos_b = 0; 	// position of begin of search
    while (1) {
        size_t pos_d = src.find(delim, pos_b);
        if (pos_d == std::string::npos) {
            res.push_back(src.substr(pos_b));
            break;
        }
        res.push_back(src.substr(pos_b, pos_d - pos_b));
        pos_b = pos_d + delim_len;
    }
    return res;
}


template <typename Container>
static std::string join(const std::string& separator, const Container& cont) {
	if (cont.empty()) {
		return "";
	}

	auto it = std::begin(cont);
	auto end = std::end(cont);
	std::string result = *it++;
	for (; it != end; ++it) {
		result += separator;
		result += *it;
	}
	return result;
}

int main(int argc, char** argv)
{
	p_cont(split("x=2; z=12; e=3;", ";"));

	p_cont(split("i wish i knew    the reason  why  ", " "));

	p("----");
	p(join(", ", std::vector<std::string>({"1", "2", "3", "4"})));
	p(join("|", std::list<std::string>({"z"})));
	p(join("!", std::vector<std::string>()));


    return 0;
}
