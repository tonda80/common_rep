#include "../common.h"

//

template<typename... Ts>
void sizeof_demo(const Ts&... args) {
	std::cout << sizeof...(args) << std::endl;
}

// реализация из https://habr.com/ru/post/228031/

template<typename... Args>		// базовый класс
struct mytuple;

template<typename Head, typename... Tail>
struct mytuple<Head, Tail...> : mytuple<Tail...>
{
	using base_type = mytuple<Tail...>;
	using value_type = Head;

	mytuple(Head h, Tail... tail)	: base_type(tail...), head_(h) {}
	base_type& base = static_cast<base_type&>(*this);
	Head       head_;
};

template<>						// финальная специализация
struct mytuple<>
{};

template<int I, typename Head, typename... Args>
struct getter
{
	using return_type = typename getter<I-1, Args...>::return_type;
	static return_type get(mytuple<Head, Args...> t) {
		return getter<I-1, Args...>::get(t);
	}
};

template<typename Head, typename... Args>
struct getter<0, Head, Args...>
{
	using return_type = typename mytuple<Head, Args...>::value_type;
	static return_type get(mytuple<Head, Args...> t) {
		return t.head_;
	}
};

template<int I, typename Head, typename... Args>
typename getter<I, Head, Args...>::return_type get_element(mytuple<Head, Args...> t) {
	return getter<I, Head, Args...>::get(t);
}


int main(int argc, const char** argv)
{
	sizeof_demo(12, 12, 34, "vasia");

	mytuple<int, double, int, const char*> t  = {12, 2.34, 89, "zzz"};
	std::cout << t.head_ << " " << t.base.head_ << " " << t.base.base.head_ << " " << t.base.base.base.head_ << std::endl;
	// но лучше так
	p(get_element<1>(t));
	// ошибка дикая p(get_element<100>(t));

    return 0;
}
