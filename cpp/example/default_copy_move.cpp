#include "../common.h"


struct C {
	C() {p("C");}
	~C() {p("~C");}
	C(C&) {p("C&");}
	C(C&&) {p("C&&");}
	void do_smth() const {p("C::do_smth");}

	int i = 1;
};


struct D {
	D() {p("D");}
	~D() {p("~D");}
	D(D&) {p("D&");}
	D(D&&) {p("D&&");}
	void do_smth() const {p("D::do_smth");}

	int i = 1;
};

struct E {
	C c;
	D d;
};

void func(E& e) {
	e.c.i = 21;
	e.d.i = 22;

	pp("func(E&)", e.c.i, e.d.i);
}

void func(E&& e) {
	e.c.i = 31;
	e.d.i = 32;

	pp("func(E&&)", e.c.i, e.d.i);
}


int main(int argc, const char** argv)
{
	std::string s(300, 'c');	// длинные строки меняются местами, короткие - нет
	std::string s2;
	//P2(s);
	P2(reinterpret_cast<const void*>(s.data()));
	P2(reinterpret_cast<const void*>(s2.data()));
	s2 = std::move(s);
	P2(s);
	P2(reinterpret_cast<const void*>(s.data()));
	//P2(s2);
	P2(reinterpret_cast<const void*>(s2.data()));

	p("-- class test --");

	E e;
	E e2(e);
	e2.c.i = 7;
	pp("created", e.c.i, e2.c.i);

	func(e);
	pp("after func(e)", e.c.i, e.d.i);

	func(E());
	pp("after func(E())", e.c.i, e.d.i);

	func(std::move(e));
	pp("after func(std::move(e))", e.c.i, e.d.i);

	func(E(std::move(e2)));
	pp("after func(E(std::move(e2)))", e2.c.i, e2.d.i);


	return 0;
}
