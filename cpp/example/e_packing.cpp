#include "../common.h"

// https://gcc.gnu.org/onlinedocs/gcc-3.3/gcc/Type-Attributes.html


struct S1 {
	char c[3];
	int i;		// размер char[3] будет 3, но int требует 4 байтового выравнивания, поэтому размер 8, но с packed (gcc) таки 7
} __attribute__((packed));	// работает на gcc и вроде clang. для msvc '#pragma pack(push,1)'


int main(int argc, char** argv)
{
	P2(sizeof(S1));
	P2(sizeof(S2));

    return 0;
}
