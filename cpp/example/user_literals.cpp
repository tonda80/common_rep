#include "../common.h"


// https://en.cppreference.com/w/cpp/language/user_literal
// https://habr.com/ru/post/140357/


class Parrot {
    int v = 0;
public:
    Parrot(int v_): v(v_) {}
    void p() const {pp("Parrot with", v);}
};

Parrot operator"" _parrot(unsigned long long num) {
    return Parrot(num);
}


// used for side-effects. такое почему то для чисел. ??
void operator"" _print ( const char* str )
{
    std::cout << str << '\n';
}

// а для строк надо вот так
std::string operator"" _dbl(const char* s, std::size_t n) {
    std::string str(s, n);
    return str + str;
}



int main(int argc, const char** argv) {

    Parrot p1(7);
    p1.p();

    auto p2 = 8_parrot;
    p2.p();

    (10_parrot).p();

    p("---");

    12_print;

    p("---");

    auto s = "any_word"_dbl;
    p(s);

    p("any phrase "_dbl);

    return 0;
}
