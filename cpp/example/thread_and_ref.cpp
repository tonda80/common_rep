#include <iostream>
#include <stdlib.h>

#include <string>
#include <memory>
#include <list>
#include <map>
#include <deque>
#include <functional>
#include <thread>
#include <chrono>

using namespace std;

template <typename T> void p(const T& obj, const char* pref="") {std::cout << pref << obj << std::endl;}
template <typename T> void p_(const T& obj, const char* pref="") {std::cout << pref << obj << ' ' << std::flush;}

struct C
{
	int &i;
	C(int& i_):i(i_){}
};

void worker(const C& c )
{
	while (1) {
		p_(c.i);
		std::this_thread::sleep_for (std::chrono::seconds(1));
	}
}

void worker_0()
{
	int a = 8;
	C c(a);
	std::thread(worker, c).detach();
	do {
		p_('!');
		std::this_thread::sleep_for (std::chrono::seconds(3));
	} while (0);
	p_('?');
}

int main()
{
	std::thread(worker_0).detach();

	cin.ignore();
	p("exiting..");

    return 0;
}

