#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <csignal>
#include <iostream>

void my_handler(int s){
	printf("Caught signal %d\n",s);
	exit(1);
}

void signal_handler(int signal) {
	std::cout << "signal_handler " << signal << std::endl;
}

int main2(int argc,char** argv)
{

   struct sigaction sigIntHandler;

   sigIntHandler.sa_handler = my_handler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = 0;

   sigaction(SIGINT, &sigIntHandler, NULL);

   pause();

   return 0;
}

int main()
{
	// Установка обработчика сигнала
	std::signal(SIGINT, signal_handler);

	pause();
}
