#include "../common.h"


struct C {
	C(int i_ = 0) : i(i_) {pp("C", i);}
	~C() {pp("~C", i);}
	int i;
};


void f_throw() {
	p("f_throw");
	C c(99);
	throw std::runtime_error("[f_throw] oops");
}

void f_noexc_lie(int i, bool thr) noexcept {
	p("f_noexc_lie");
	C c(i);
	if (thr) {
		f_throw();
	}
}



int main(int argc, const char** argv) {

	p("\n== case 0 ==");
	try {
		f_throw();
	}
	catch (std::runtime_error& e) {
		pp("catched", e.what());
	}

	p("\n== case 1 ==");
	try {
		f_noexc_lie(0, false);
		f_throw();
	}
	catch (std::runtime_error& e) {
		pp("catched", e.what());
	}

	p("\n== case 2 ==");
	try {
		f_noexc_lie(1, true);
	}
	catch (std::runtime_error& e) {
		pp("it will not be caught! destructor in f_noexc_lie will not be called", e.what());
	}

	return 0;
}
