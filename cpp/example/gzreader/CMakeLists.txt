cmake_minimum_required (VERSION 3.10)

project (gzreader)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror=return-type -Werror=missing-field-initializers")

find_package(Boost 1.72 REQUIRED)

set(SRC_FILES
	${CMAKE_SOURCE_DIR}/gzreader.cpp
)

add_executable(${CMAKE_PROJECT_NAME} ${SRC_FILES} )

include(${CMAKE_SOURCE_DIR}/../../fmt_wrapper/CMakeLists.txt)

target_include_directories(${CMAKE_PROJECT_NAME} PRIVATE
	${Boost_INCLUDE_DIR}
)

target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE
	boost_iostreams
)
