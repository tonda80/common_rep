#include <fstream>
#include <iostream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include <fmt_wrapper.h>


std::function<void(const std::string&)> Fmt::error_handler = [](const std::string& msg){
    std::cerr << "Format error: " << msg;
};


int main(int argc, const char** argv)
{
    if (argc < 2) {
		Fmt::print("usage: 'gzreader in'\n");
		return 1;
    }

    std::ifstream file(argv[1], std::ios_base::in | std::ios_base::binary);
    boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
    in.push(boost::iostreams::gzip_decompressor());
    in.push(file);

    //boost::iostreams::copy(in, std::cout);
    std::istream incoming(&in);
    std::string line;
	while (std::getline(incoming, line)) {
		std::cout << line << std::endl;
	}
}
