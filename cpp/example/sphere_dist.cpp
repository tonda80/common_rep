#include "../common.h"

#include <cmath>

#include <fmt/format.h>
// g++ -std=c++17 -I/home/anton/rep-s/mnt2/cpp/thirdparty/fmt/include  sphere_dist.cpp  /home/anton/rep-s/mnt2/cpp/thirdparty/fmt/src/format.cc


// возвращает расстояние в километрах по координатам в градусах
double dist_by_coords(double lat1, double lon1, double lat2, double lon2) {
	static const double earth_radius = 6371;		// км по WGS 84
	static auto hav = [](double x) {return pow(sin(x/2), 2);};		// гаверсинус
	static auto g2r = [](double x) {return x*M_PI/180;};

	lat1 = g2r(lat1); lon1 = g2r(lon1); lat2 = g2r(lat2); lon2 = g2r(lon2);

	double d_lat = fabs(lat1 - lat2);
	double d_lon = fabs(lon1 - lon2);

	double angle = 2*asin(sqrt( hav(d_lat) + cos(lat1)*cos(lat2)*hav(d_lon) ));

	return angle*earth_radius;
}

// градусы в nmea строку
std::string coord2nmea(double coord, bool is_lat) {
    double u_coord = fabs(coord);
    uint16_t grad = static_cast<uint16_t>(u_coord);
    double min = (u_coord - grad)*60;
    uint16_t min_int = static_cast<uint16_t>(min);  // вроде нельзя сделать "01.23" из дробного
    uint16_t min_fract2 = static_cast<uint16_t>(round((min - min_int)*100));

    std::string s = coord >= 0 ? "E" : "W";
    if (is_lat) {
        s = coord >= 0 ? "N" : "S";
        return fmt::format("{:02d}{:02d}.{:02d}{}", grad, min_int, min_fract2, s);
    }
    return fmt::format("{:03d}{:02d}.{:02d}{}", grad, min_int, min_fract2, s);
}

struct NavigationData {
        double lat;
        double lon;
        double speed;
        double head;
        double hdop;
        double alt;
        uint16_t sats;
        int64_t time;
        bool valid;
    };


int main(int argc, char** argv)
{
	NavigationData nd = {};
	P2(nd.valid);

	double s = dist_by_coords(0, 0, 1, 1);		// 157.249381
	P2(s);

	s = dist_by_coords(55.7539, 37.6208, 59.9398, 30.3146);		// 634.568980
	P2(s);

	P2(coord2nmea(12.5, false));
	P2(coord2nmea(-1.1181, true));

    return 0;
}
