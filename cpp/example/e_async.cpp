#include "../common.h"

thread_local int gi = 0;

struct T {int a[3];};

int main(int argc, const char** argv)
{
	// p(std::thread::hardware_concurrency());
	// p(std::atomic<int>{}.is_lock_free());
	// p((std::atomic<T>{}).is_lock_free());		// нужен -latomic

	p("-- main --");
	p(gi);

	std::async(
		// std::launch::async,				// так не меняет gi 	нужен -lpthread
		// std::launch::deferred,			// а так меняет gi ибо вызывается в том же треде
		// дефолт может быть любым? но наблюдается async
	[](){
		p("-- async --");
		gi = 99;
		p(gi);
	}
	).wait();

	p("-- main2 --");
	p(gi);

    return 0;
}
