#include "../common.h"

#include <sys/socket.h>
#include <sys/un.h>
#include <syslog.h>
#include <unistd.h>

#include <fmt/format.h>

// отлаживаем рабочий код, и пример работы с journald
// export FMT_PATH=/home/ant/strange_code/fmt
// g++ -std=c++17 -I$FMT_PATH/include -L$FMT_PATH/build journald_writer.cpp -lfmt


constexpr std::string_view JOURNALD_SOCKET = "/var/run/systemd/journal/socket";

constexpr std::string_view EQ = "=";
constexpr std::string_view EOL = "\n";

const std::string PRIORITY_ERROR = fmt::format("PRIORITY={}\n", LOG_ERR);
const std::string PRIORITY_WARN = fmt::format("PRIORITY={}\n", LOG_WARNING);
const std::string PRIORITY_INFO = fmt::format("PRIORITY={}\n", LOG_INFO);
const std::string PRIORITY_DEBUG = fmt::format("PRIORITY={}\n", LOG_DEBUG);

//constexpr isp::global::JsonValue log_info{"log"};


struct LogMsg {
	std::string payload;
	int level;
};


class JournaldSink { //orig->  final : public spdlog::sinks::base_sink<std::mutex> {
public:
	JournaldSink(std::string_view app_name) {
		m_socket = socket(AF_UNIX, SOCK_DGRAM, 0);
		if (m_socket < 0) {
			std::runtime_error("JournaldSink socket create error");
		}

		struct sockaddr_un addr{0};
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, JOURNALD_SOCKET.data(), std::min(sizeof(addr.sun_path), JOURNALD_SOCKET.size()));
		int res = connect(m_socket, (sockaddr*)&addr, sizeof(addr));
		if (res < 0) {
			close(m_socket);
			std::runtime_error(fmt::format("JournaldSink socket connect error: {}", errno));
		}

		m_common_fields.push_back(fmt::format("SYSLOG_IDENTIFIER={}\n", app_name));

		//m_formatter = isp::log::formatter::MakeJournaldFormatter();
	}

	~JournaldSink() /*final*/ {
		close(m_socket);
	}

public: //orig-> protected:
	void sink_it_(const /*spdlog::details::log_msg*/LogMsg &msg) /*final*/ {
        //spdlog::memory_buf_t formatted;
		//m_formatter->format(msg, formatted);

		std::vector<iovec> vect;

		for (const auto& fld : m_common_fields) {
			vect.push_back(to_iovec(fld));
		}
		add_priority_field(vect, msg.level);
		add_message_field(vect, std::string_view(msg.payload.data(), msg.payload.size()));
		//try_add_context_fields(vect);

		// debug!
		pp("vect.size()=", vect.size());
		for (const auto& v : vect) {
			std::string_view s((char*)v.iov_base, v.iov_len);
			pp(s, v.iov_base, v.iov_len);
		}

		struct msghdr journal_msg{0};
		journal_msg.msg_iov = vect.data();
		journal_msg.msg_iovlen = vect.size();
		if (sendmsg(m_socket, &journal_msg, 0) == -1 ) {
			pp("sendmsg error");
		}
	}

	void flush_() /*final*/ {}

private:
	int m_socket = -1;
	std::vector<std::string> m_common_fields;		// fields common for all messages
	//std::unique_ptr<spdlog::formatter> m_formatter;

	static iovec to_iovec (std::string_view data) {
		return iovec{(void *)(data.data()), data.size()}; // NOLINT(google-readability-casting)
	};

	void add_field(std::vector<iovec>& vect, std::string_view key, std::string_view value)  {
		vect.push_back(to_iovec(key));
		vect.push_back(to_iovec(EQ));
		vect.push_back(to_iovec(value));
		vect.push_back(to_iovec(EOL));
	}

	void add_message_field(std::vector<iovec>& vect, std::string_view value) {
		std::string_view key = "MESSAGE";
		if (value.find('\n') == std::string::npos) {
			add_field(vect, key, value);
			return;
        }
		// if value contains a \n byte this method must be used
		uint64_t size = value.size();

		vect.push_back(to_iovec(key));
		vect.push_back(to_iovec(EOL));
		vect.push_back({&size, sizeof(size)});
		vect.push_back(to_iovec(value));
		vect.push_back(to_iovec(EOL));
	}

	void add_priority_field(std::vector<iovec>& vect, /*spdlog::level::level_enum*/int level) {
		switch (level) {
		case 5: //SPDLOG_LEVEL_CRITICAL:
		case 4: //SPDLOG_LEVEL_ERROR:
			vect.push_back(to_iovec(PRIORITY_ERROR));
			break;
		case 3: //SPDLOG_LEVEL_WARN:
			vect.push_back(to_iovec(PRIORITY_WARN));
			break;
		case 2: //SPDLOG_LEVEL_INFO:
			vect.push_back(to_iovec(PRIORITY_INFO));
			break;
		default:
			vect.push_back(to_iovec(PRIORITY_DEBUG));
			break;
		}
	}
/*
	void try_add_context_fields(std::vector<iovec>& vect) {
		if (isp::global::request.GetValue().has_value() || log_info.GetValue().has_value()) {
            if (auto value = isp::global::id.GetValue()) {
				add_field(vect, "REQUEST_ID", value.value());
            }
            if (auto value = isp::global::ip.GetValue()) {
				add_field(vect, "REQUEST_IP", value.value());
            }
            if (auto value = isp::global::user.GetValue()) {
				add_field(vect, "REQUEST_USER", value.value());
            }
		}
	}*/
};


int main(int argc, const char** argv) {
	JournaldSink sink("jd_cpp_demo");

	sink.sink_it_(LogMsg{"hello from cpp!", 2});
	sink.sink_it_(LogMsg{"multiline hello\n from the same cpp\n dude!", 4});

    return 0;
}
