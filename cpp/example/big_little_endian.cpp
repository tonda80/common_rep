#include "../common.h"

// big-endian от старших разрядов к младшим ("по человечески")
// little-endiand - наоборот, т.е. если число 0x1234 лежит по адресу 0, то в 0 байте 0x34 и в 1-м 0x12


// не используем адресацию
// 0 байт - старший байт числа (так было в реальной задаче)
// дурацкий с++ печатает символы для uint8_t поэтому возвращаем int
template <typename T>
int get_byte(T src, uint8_t n) {
	int max_n = sizeof(src) - 1;
	if (n > max_n) {
		throw std::runtime_error("[get_byte] too big n, maximum "+std::to_string(max_n));
	}

	return src >> 8*(max_n - n) & 0xff;
}



int main(void)
{
	uint16_t x = 1; /* 0x0001 */
	uint8_t * addr = (uint8_t *) &x;
	printf("%s\n", *addr == 0 ? "big-endian" : "little-endian");
	printf("%s\n", *(addr+1) == 1 ? "big-endian" : "little-endian");

	p("==");

	// and more
	uint64_t num = 0x123456;
	std::stringstream stream;
	stream << std::hex << num << std::endl << (num&0xff) << "==" << 0x56 << std::endl;	// те маскирование вообще не про порядок байт
	stream << get_byte(num, 0) << std::endl;
	stream << get_byte(num, 7) << std::endl;
	stream << get_byte(num, 5) << std::endl;
	//stream << get_byte(num, 8) << std::endl;

	p(stream.str());

	return 0;
}
