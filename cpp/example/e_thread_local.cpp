#include "common.h"


int* new_int(int i) {
	thread_local static int* ti = nullptr;
    if (!ti) {
		p("new_i "+std::to_string(i));
        ti = new int(i*100);
    }
    return ti;
}

void thread_f(int i) {
	int* ti = new_int(i);
	while (1) {
		p("thread_f "+std::to_string(*ti) + " " + std::to_string((uint64_t)ti));
		std::this_thread::sleep_for(std::chrono::milliseconds(i*100));
	}
}


int main(int argc, const char** argv)
{
	std::vector<std::thread> thrds;
	thrds.push_back(std::thread(thread_f, 3));
	thrds.push_back(std::thread(thread_f, 5));
	thrds.push_back(std::thread(thread_f, 2));

	for (auto& t : thrds) {
		t.join();
	}
    return 0;
}
