// эксперименты с jsoncpp https://github.com/open-source-parsers/jsoncpp.git
// 1.95 версия собиралась тривиально: cmake .. && make
// "библиотечная" часть изначально копипаста из ttm кода

// http://open-source-parsers.github.io/jsoncpp-docs/doxygen/class_json_1_1_value.html

// сборка без установки
// export JC=/home/berezin/strange_code/jsoncpp
// g++ -I $JC/include  -std=c++17  e_jsoncpp.cpp $JC/build/lib/libjsoncpp.a

#include <json/json.h>

#include <sstream>
#include <fstream>

#include "../common.h"


// -------- lib --------

std::unique_ptr<Json::Value> json_from_file(const std::string& path, std::string& err);
std::unique_ptr<Json::Value> json_from_buf(const char* beg, const char* end, std::string* err = nullptr);
std::unique_ptr<Json::Value> json_from_string(const std::string& str, std::string* err = nullptr);

Json::StreamWriter* json_fast_writer();
Json::StreamWriter* json_pretty_writer();
std::string json_string(const Json::Value&);
std::string json_pretty_string(const Json::Value&);

const Json::Value& req_child(const Json::Value& jvalue, const std::string& name);

// -------- lib cpp --------

// баг в jsoncpp на некорректном json, например '1:2{"":{1:2}}'
// parse возвращает true, но при попытке получить любого потомка - исключение
// также вроде замечал падение в getFormattedErrorMessages, но сейчас не могу воспроизвести
int __check_json(bool parse_result, const std::unique_ptr<Json::Value>& json_value) {
    if (!parse_result) {
        return 1;
    }
    try {
        ; //std::as_const(*json_value)[""];		// че то тут не работает уже но чинить пока некогда
    }
    catch (...) {
		p("[ERROR] __check_json 2");	// json_from_string("1:2{\"\":{1:2}}");  проверяем что так и не починили баг
        return 2;
    }

    return 0;   // ok
}
std::string __err_msg(const Json::Reader& reader, int status) {
    if (status == 2) {
        return "incorrect json";
    }
    return reader.getFormattedErrorMessages();
}


std::unique_ptr<Json::Value> json_from_file(const std::string& path, std::string& err) {
    std::ifstream ifs(path, std::fstream::in);
    if (!ifs.good()) {
        err = "cannot open file " + path;
        return nullptr;
    }
    Json::Reader reader;
    auto json_value = std::make_unique<Json::Value>();
    int status =  __check_json(reader.parse(ifs, *json_value), json_value);
    if (status) {
        err = "cannot parse json file " + path + ": " + __err_msg(reader, status);
        return nullptr;
    }
    return json_value;
}

std::unique_ptr<Json::Value> json_from_buf(const char* beg, const char* end, std::string* err) {
    Json::Reader reader;
    auto json_value = std::make_unique<Json::Value>();
    int status = __check_json(reader.parse(beg, end, *json_value), json_value);
    if (status) {
        if (err) {
            *err = "cannot parse json string: " + __err_msg(reader, status);
        }
        return nullptr;
    }
    return json_value;
}

std::unique_ptr<Json::Value> json_from_string(const std::string& str, std::string* err) {
    Json::Reader reader;
    auto json_value = std::make_unique<Json::Value>();
    int status = __check_json(reader.parse(str, *json_value), json_value);
    if (status) {
        if (err) {
            *err = "cannot parse json string: " + __err_msg(reader, status);
        }
        return nullptr;
    }
    return json_value;
}

Json::StreamWriter* json_fast_writer() {
    thread_local static Json::StreamWriter* json_fast_writer = nullptr;
    if (!json_fast_writer) {
        Json::StreamWriterBuilder builder;
        builder["commentStyle"] = "None";
        builder["indentation"] = "";
        json_fast_writer = builder.newStreamWriter();
    }
    return json_fast_writer;
}

Json::StreamWriter* json_pretty_writer() {
    thread_local static Json::StreamWriter* writer = nullptr;
    if (!writer) {
        Json::StreamWriterBuilder builder;
        builder["commentStyle"] = "None";
        //builder["indentation"] = "";
        writer = builder.newStreamWriter();
    }
    return writer;
}

std::string json_string(const Json::Value& value) {
    std::stringstream ss;
    json_fast_writer()->write(value, &ss);
    return ss.str();
}

std::string json_pretty_string(const Json::Value& value) {
    std::stringstream ss;
    json_pretty_writer()->write(value, &ss);
    return ss.str();
}

const Json::Value& req_child(const Json::Value& jvalue, const std::string& name) {
    const Json::Value& ret = jvalue[name];
    if (ret.isNull()) {
        throw std::runtime_error("json error, no required parameter: " + name);
    }
    return ret;
}

// -------- test --------




int main(int argc, const char** argv) {

	//json_from_string("1:2{\"\":{1:2}}");
	auto jv1 = json_from_string("{}");
	p(jv1.get());
	pp(jv1->type());
	p(jv1->size());
	p(jv1->empty());
	p(bool(jv1));

    return 0;
}




