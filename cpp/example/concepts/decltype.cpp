int foo();

int main(int argc, char** argv)
{
    vector<int> vv;

    decltype(vv)::value_type v1 = 11;	// int
    decltype(vv[0]) v2 = v1;			// int&
    decltype(foo()) v3 = 33;			// int

    return 0;
}
