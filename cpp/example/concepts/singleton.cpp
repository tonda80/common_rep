#include "../common.h"


struct Noncopyable {
	Noncopyable() = default;
	~Noncopyable() = default;
	Noncopyable(const Noncopyable&) = delete;
	Noncopyable& operator=(const Noncopyable&) = delete;
};

class Singleton : private Noncopyable
{
protected:
	Singleton() = default;

	std::string s = "S ";

public:
    static Singleton& instance() {
        static Singleton instance;
        return instance;
    }

    void do_smth() {p(s+"hello!");}
};

struct Derived : public Singleton
{
	std::string s2 = "D ";

	void do_smth_else() {p(s+s2+"hello again!");}

	static Derived& instance() {
        static Derived instance;
        return instance;
    }
};

int main(int argc, const char** argv)
{
	// error  Singleton s;

	Singleton::instance().do_smth();
	// obviously error  Singleton::instance().do_smth_else();

	Derived::instance().do_smth();
	Derived::instance().do_smth_else();

    return 0;
}
