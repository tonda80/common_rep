#include <iostream>
#include <memory>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

void f(int i)
{
    p("begin");

    shared_ptr<void> sq(nullptr, [](void*){p("scope guard");});

    if (i)
        throw runtime_error("");

    p("end");
}

int main(int argc, char** argv)
{
    f(0);

    try {
        f(1);
    }
    catch (...) {
        p("exception");
    }

    return 0;
}
