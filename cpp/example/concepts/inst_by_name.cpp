#include "../common.h"

// создаем объект по имени класса
// https://stackoverflow.com/questions/582331/is-there-a-way-to-instantiate-objects-from-a-string-holding-their-class-name


// 1) базовая демонстрация
struct B {
	virtual void f() = 0;
};

struct D1 : public B {
	void f() {p("D1::f");}
};

struct D2 : public B {
	void f() {p("D2::f");}
};

struct D3 : public B {
	void f() {p("D3::f");}
};

static std::map<std::string, B*(*)()> crt = {
	{"D1", []()->B* {return new D1;}},
	{"D2", []()->B* {return new D2;}},
	{"D3", []()->B* {return new D3;}},
};


// 2) хендлер с авторегистрацией типов. Не совсем вылизанный (Handler::creators можно сделать )
class Handler
{
public:
	using T_handler_map = std::map<std::string, std::function<std::unique_ptr<Handler>()>>;

	static std::unique_ptr<Handler> create(const std::string& id) {
		auto it = creators.find(id);
		if (it == creators.end()) {
			return nullptr;
		}
		return it->second();
	}

	virtual void process() = 0;

	struct Registrar {
		Registrar(const std::string& id, std::function<std::unique_ptr<Handler>()> cr) {
			creators.emplace(id, cr);
			print("registered {}\n", id);
		}
	};

private:
	static T_handler_map creators;
};
Handler::T_handler_map Handler::creators;

// если охота спрятать Handler::Registrar в protected
#define DECL_REGISTRAR(NAME) \
	static Registrar reg##NAME;
#define DEF_REGISTRAR_with_declaration(NAME) \
   Handler##NAME::Registrar Handler##NAME::reg##NAME(#NAME, []() {return std::make_unique<Handler##NAME>();});

#define DEF_REGISTRAR(NAME) \
   static Handler##NAME::Registrar reg##NAME(#NAME, []() {return std::make_unique<Handler##NAME>();});

class HandlerD1 : public Handler, private D1
{
public:
	void process() {
		p("HandlerD1::process");
		f();
	}
};
DEF_REGISTRAR(D1)		// --> static HandlerD1::Registrar regD1("D1", []() {return std::make_unique<HandlerD1>();});


class HandlerD2 : public Handler, private D2
{
public:
	void process() {
		p("HandlerD2::process");
		f();
	}
};
DEF_REGISTRAR(D2)


// 3) ещё вариант без авторегистрации, с шаблонами

class HandlerV2Base
{
public:
	static std::unique_ptr<HandlerV2Base> create(const std::string& id) {
		auto it = creators.find(id);
		if (it == creators.end()) {
			return nullptr;
		}
		return it->second();
	}

	virtual void process() = 0;
private:
	using T_handlerV2_map = std::map<std::string, std::function<std::unique_ptr<HandlerV2Base>()>>;
	static T_handlerV2_map creators;
};

template <typename T, typename D>
class HandlerV2 : public HandlerV2Base
{
public:
	static std::unique_ptr<HandlerV2Base> create() {return std::make_unique<D>();}
protected:
	T obj;
	HandlerV2() = default;
};

class HandlerV2_D1 : public HandlerV2<D1, HandlerV2_D1>
{
public:
	void process() {
		p("HandlerV2_D1::process");
		obj.f();
	}
};

class HandlerV2_D2 : public HandlerV2<D2, HandlerV2_D2>
{
public:
	void process() {
		p("HandlerV2_D2::process");
		obj.f();
	}
};

HandlerV2Base::T_handlerV2_map HandlerV2Base::creators = {
	{"D1", HandlerV2_D1::create},
	{"D2", HandlerV2_D2::create}
};


int main(int argc, char** argv)
{
	// error: invalid conversion from ‘D3* (*)()’ to ‘B* (*)()’ [-fpermissive]
	// B* ftestok() {return new D3;}; D3* ftesterror() {return new D3;}
	// но почему непонятно
	//B*(*pf)() = ftestok; pf()->f();

	crt["D1"]()->f();
	crt["D3"]()->f();

	p("\n--------\n");

	for(const auto& id : {"D1", "D2", "D100"}) {
		if (auto handler = Handler::create(id)) {
			handler->process();
		}
		else {
			print("handler for {} is not registered\n", id);
		}
	}

	p("\n--------\n");

	for(const auto& id : {"D1", "D2", "D100"}) {
		if (auto handler = HandlerV2Base::create(id)) {
			handler->process();
		}
		else {
			print("handlerV2 for {} is not registered\n", id);
		}
	}

    return 0;
}
