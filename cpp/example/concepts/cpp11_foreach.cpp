#include <iostream>

void print(const char* s) {while (*s){if (*s == '{' && *++s == '}')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '{' && *++s == '}'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

using namespace std;

struct C
{
    static const int SIZE = 8;
    int arr[SIZE];
    C() {
        for (int i=0;i<SIZE;++i)
           arr[i] = i;
    }
};

int* begin(C& c) {
    return c.arr;
}
int* end(C& c) {
    return c.arr + c.SIZE;
}


int main(int argc, char** argv)
{
    C c;
    for (auto& e : c)
        print("{}", e);

    return 0;
}
