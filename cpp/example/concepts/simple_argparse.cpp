#include "../common.h"


class SimpleArgParse
{
	int argc;
	const char** argv;

public:
	SimpleArgParse(int argc_, const char** argv_) : argc(argc_), argv(argv_) {}

	bool has_option(const std::string& option)
	{
		auto last = argv + argc;
		return std::find(argv, last, option) != last;
	}

	std::string get_option(const std::string& option)
	{
		auto last = argv + argc;
		auto it = std::find(argv, last, option);
		if (it != last && ++it != last) {
			return *it;
		}
		return "";
	}
};




int main(int argc, const char** argv)
{
	SimpleArgParse ap(argc, argv);

	pp("has_option(\"zz\") =", ap.has_option("zz"));
	pp("has_option(\"a1\") =", ap.has_option("a1"));

	pp("get_option(\"a2\") =", ap.get_option("a2"));
	pp("get_option(\"vv\") =", ap.get_option("vv"));

    return 0;
}
