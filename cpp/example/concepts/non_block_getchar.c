#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>


int main(int argc, char *argv[]) {
	struct termios initial_settings, new_settings;

	tcgetattr(0,&initial_settings);

	new_settings = initial_settings;
	new_settings.c_lflag &= ~ICANON;
	new_settings.c_lflag &= ~ECHO;
	new_settings.c_lflag &= ~ISIG;
	new_settings.c_cc[VMIN] = 0;
	new_settings.c_cc[VTIME] = 0;

	tcsetattr(0, TCSANOW, &new_settings);

	while(1) {
		char c = getchar();
		if(c != EOF) {
			if(c == 27) {
				puts("Escape key pressed");
				break;
			}

			printf("%c (%d) pressed\n", c, c);

		}
		else {
			usleep(100000);
			//puts("nothing pressed");
		}
	}

	tcsetattr(0, TCSANOW, &initial_settings);

	return(0);
}
