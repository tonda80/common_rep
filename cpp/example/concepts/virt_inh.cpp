#include <iostream>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

#define RIGHT

struct C
{
    C(){p("C");}
    ~C(){p("~C");}
};

struct B
{
    B(){p("B");}
    #if defined(RIGHT)
    virtual ~B(){p("~B");}
    #else
    ~B(){p("~B");}
    #endif
};

struct D : public B
{
    D(){p("D");}
    ~D(){p("~D");}
    C c;
};

// часть выше это пример работы виртуального деструктора



struct Z {
     void f() {p("Z::f");}
};

struct DZ1 : public virtual Z {		// virtual
	void f() {p("DZ1::f");}
};

struct DZ2 : public virtual Z {
	//void f() {p("DZ2::f");}
};

struct Y : public DZ1, public DZ2 {
	void g() {p("Y::g");}
	//void f() {p("Y::f");}
};


int main(int argc, char** argv)
{
	/*
	B *d = new D;
	delete d;
	*/ // 	тут про виртуальный деструктор

	Y* y = new Y;
	y->g();
	y->f();		// вот такой вызов скомпилируется при только при виртуальном наследовании обоих DZ* (либо если есть Y::f)


    return 0;
}
