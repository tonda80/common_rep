#include <iostream>

#include <functional>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

// a function: (also works with function object: std::divides<double> my_divide;)
double my_divide (double x, double y) {return x/y;}

struct MyPair {
  double a,b;
  double multiply() {return a*b;}

  double m3(double d) {return a*b*d;}
};


int main(int argc, char** argv)
{
  //using namespace std::placeholders;

  auto fn_five = std::bind (my_divide,10,2);
  p(fn_five());

  auto fn_half = std::bind (my_divide, std::placeholders::_1, 2);
  p(fn_half(10));

  auto fn_invert = std::bind (my_divide, std::placeholders::_2, std::placeholders::_1);
  p(fn_invert(10,2));

  auto fn_rounding = std::bind<int> (my_divide, std::placeholders::_1, std::placeholders::_2);
  p(fn_rounding(44, 4));


  // binding members:
  MyPair ten_two {10,2};

  auto bound_member_fn = std::bind (&MyPair::multiply, std::placeholders::_1);
  p(bound_member_fn(ten_two));

  auto bound_member_fn2 = std::bind (&MyPair::multiply, ten_two);
  p(bound_member_fn2());

  auto bound_member_fn3 = std::bind (&MyPair::m3, ten_two, std::placeholders::_1);
  p(bound_member_fn3(5));

  auto bound_member_data = std::bind (&MyPair::a, ten_two);
  p(bound_member_data());   // ten_two.a

  return 0;
}
