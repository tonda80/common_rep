#include <iostream>

#include <map>
#include <unordered_map>

void print(const char* s) {while (*s){if (*s == '%' && *++s != '%')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '%' && *++s != '%'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

int main(int argc, char** argv)
{
    unordered_map<int, string> um;

    auto res = um.insert(make_pair<int, string>(1, "one"));
    print("insert result - %z\n", res.second);
    res = um.insert(make_pair<int, string>(1, "odin"));
    print("insert result - %z\n", res.second);

    for (auto& kv : um) {
    std::cout << kv.first << " has value " << kv.second << std::endl;
}


    return 0;
}
