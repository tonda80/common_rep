#include "../common.h"

// https://habr.com/ru/post/185662/
// отличие new оператора и выражения

class Test {
public:
    Test() {
        std::cout << "Test::Test()" << std::endl;
    }

    void* operator new (std::size_t size) throw (std::bad_alloc) {
        std::cout << "Test::operator new(" << size << ")" << std::endl;
        return ::operator new(size);
    }
};

int main() {
    p("it's new expression");
    Test *t = new Test();
    p("");
    p("it's new operator");
    void *p = Test::operator new(100); // 100 для различия в выводе
}
