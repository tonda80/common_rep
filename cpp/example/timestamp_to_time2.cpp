#include <iostream>

#include <string>
#include <stdexcept>
#include <ctime>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

// helper functions
time_t get_utc_offset()
{
    time_t ref_p = 1000000;
    return mktime(gmtime(&ref_p)) - ref_p;
}

int strict_stoi(const std::string& st)
{
    size_t idx;
    int ret = std::stoi(st, &idx);
    if (idx != st.size())
        throw std::invalid_argument("strict_stoi");
    return ret;
}

// FIX time (YYYYMMDD-HH:MM:SS.sss OR YYYYMMDD-HH:MM:SS) to ttrt time (uSec from the epoch time)
std::string F2P_UTCTimestampToFixed64(const std::string& ts)
{
    int ts_size = ts.size();
    if ((ts_size != 17 && ts_size != 21) ||
        (ts[8] != '-' || ts[11] != ':' || ts[14] != ':') ||
        (ts_size == 21 && ts[17] != '.'))
        throw std::runtime_error("F2P_UTCTimestampToFixed64 error1, bad timestamp " + ts);

    int ms = 0;
    struct std::tm tm_obj = { 0 };
    try {
        tm_obj.tm_year = strict_stoi(ts.substr(0, 4)) - 1900;
        tm_obj.tm_mon = strict_stoi(ts.substr(4, 2)) - 1;
        tm_obj.tm_mday = strict_stoi(ts.substr(6, 2));
        tm_obj.tm_hour = strict_stoi(ts.substr(9, 2));
        tm_obj.tm_min = strict_stoi(ts.substr(12, 2));
        tm_obj.tm_sec = strict_stoi(ts.substr(15, 2)) - get_utc_offset();
        if (ts_size == 21)
            ms = strict_stoi(ts.substr(18, 3));
    }
    catch (std::invalid_argument&) {
        throw std::runtime_error("F2P_UTCTimestampToFixed64 error2, bad timestamp " + ts);
    }

    time_t sec = mktime(&tm_obj);
    if (sec == -1)
        throw std::runtime_error("F2P_UTCTimestampToFixed64 error3, bad timestamp " + ts);

    uint64_t us = static_cast<uint64_t>(sec) * 1000000 + ms * 1000;
    return std::to_string(us);
}

int main(int argc, char** argv)
{
   while (1) {
        string raw_input; cin >> raw_input;
        if (raw_input == "q")
            break;

        try {
           p(F2P_UTCTimestampToFixed64(raw_input));
        }
        catch (std::runtime_error& e) {
            p(e.what());
        }
   }

    return 0;
}

// 19981231-23:59:59
// 20151231-23:59:59.007
// 19700101-00:00:00
