#include <iostream>

#include <stdexcept>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <atomic>

void print(const char* s) {while (*s){if (*s == '%' && *++s != '%')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '%' && *++s != '%'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

struct Args
{
    int t_case;
    int n_threads;
    int job_time;
    Args() : t_case(0), n_threads(5), job_time(10) {}   // mingw works max for 4 threads (== cores)
    void output() {print("Test case %z, threads %z, time %z\n", t_case, n_threads, job_time);}

    static Args get_args(int argc, char** argv) {
        Args args;

        if (argc > 1)
            args.t_case = std::stoi(argv[1]);
        if (argc > 2)
            args.n_threads = std::stoi(argv[2]);
        if (argc > 3)
            args.job_time = std::stoi(argv[3]);

        args.output();
        return args;
    }
};



int main(int argc, char** argv)
{
    Args args;
    try {
        args = Args::get_args(argc, argv);
    }
    catch (logic_error& e) {
        p("Wrong args");
        return 1;
    }

    vector<thread> workers;
    long long int counter = 0LL;
    vector<long long int> counter0; // special for case 0
    bool work = true;
    if (args.t_case == 0) { // no any locks
        for (int i; i<args.n_threads; ++i) {
            counter0.push_back(0);
            workers.push_back(thread([&counter0, &work](int i)
                {
                    while (work)
                        if (++counter0[i] == 0) p("Overflow error!");
                }, i));
        }
    }
    else {
        p("Wrong case");
        return 1;
    }


    std::this_thread::sleep_for (std::chrono::seconds(args.job_time));
    work = false;
    for (auto &w : workers)
        w.join();

    if (args.t_case == 0) {
        for (auto &i : counter0)
            counter += i;
    }

    print("%z, %z\n", counter, counter/args.n_threads/args.job_time);

    return 0;
}
