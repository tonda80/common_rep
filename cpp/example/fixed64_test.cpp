#include <iostream>

/*
template <typename T> void p(const T& obj, const char e = '\n') { std::cout << obj << e; }
TEST(FixedClassTest, FixedConvertTest)
{
    ttrt::fxts::common::fixed f(0,0);
    f.convert(5764607523034234881);
    p(f.value);
    p((int)f.precision);
    p(f.convert());
    p(f.c_str());

}
*/

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

typedef int64_t fixed_numerator;
inline int integer(fixed_numerator x) { return (int)x; }
#define MANT_MASK ~((uint64_t)0xf << 60)
#define SIGN_MASK ((uint64_t)0x8 << 60)

const int MAX_FIXED_PRECISION = 7;
const int MAX_FIXED_LENGTH = 22;
const int BUFFER_SIZE = MAX_FIXED_LENGTH + MAX_FIXED_PRECISION;

char buffer[BUFFER_SIZE];

const char *fixed_to_str(const fixed_numerator& num)
{
    enum { ALIGN = 1, COMMAS = 2, DECIMAL = 4 };
    int options = DECIMAL;

    bool sign = num & SIGN_MASK;
    fixed_numerator value = num & MANT_MASK;;
    if (sign) value = -value;

    unsigned char precision = (num >> 60) & 0x7;

    //p("__deb2", ' '); p((int)precision);
    //p("__deb2", ' '); p((int)value);

    fixed_numerator x = value;
    bool negative;
    if (x < 0)
    {
        x = -x;
        // prevent buffer overflow if result is still negative
        if (x < 0)
            x = x - 1;
        negative = true;
    }
    else
        negative = false;
    int n = 0;
    int units;

    do
    {
        if (n == precision)
        {
            if (n > 0 || options & DECIMAL)
                buffer[BUFFER_SIZE - ++n] = '.';
            units = n;
        }
        else if (options & COMMAS && n > precision && (n - units) % 4 == 3)
            buffer[BUFFER_SIZE - ++n] = ',';
        fixed_numerator y;
        y = x / 10;
        buffer[BUFFER_SIZE - ++n] = integer(x - y * 10) + '0';
        x = y;
    }
    while (n <= precision || x != 0);
    if (negative)
        buffer[BUFFER_SIZE - ++n] = '-';
    if (options & ALIGN)
    {
        while (n - units < MAX_FIXED_LENGTH - 2)
            buffer[BUFFER_SIZE - ++n] = ' ';
    }
    return (const char *)buffer + BUFFER_SIZE - n;
}

fixed_numerator str_to_fixed(const char *s)
{
    fixed_numerator value = 0;
    unsigned char precision = 0;

    int c;
    while ((c = *s++) == ' ' || c == '\t');
    bool negative;
    if (c == '-')
    {
        negative = true;
        c = *s++;
    }
    else
        negative = false;
    bool decimal = false;
    while (precision < MAX_FIXED_PRECISION)
    {
        if ('0' <= c && c <= '9')
        {
            value = value * 10 + (c - '0');
            if (decimal)
                precision++;
        }
        else if (c == '.')
        {
            if (decimal)
                break;
            decimal = true;
        }
        else if (c != ',')
            break;
        c = *s++;
    }
    if (negative)
        value = -value;

    uint64_t result = (value < 0) ? -value : value;
    result |= ((uint64_t)(value < 0 ? 0x8 : 0x0) | (precision & 0x7)) << 60;
    //p("__deb", ' '); p(value, ' '); p((int)precision);
    return result;
}


int main(int argc, char** argv)
{
    p(fixed_to_str(5764607523034343716));
    p(fixed_to_str(5764607523034234881));
    p(fixed_to_str(5764607523034343717));

    //p(str_to_fixed("1.234"));
    //p(str_to_fixed("2.0"));

    //p(fixed_to_str(str_to_fixed("1.234")));
    //p(fixed_to_str(str_to_fixed("72.5432")));

    return 0;
}
