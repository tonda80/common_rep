#include <iostream>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

struct C
{
    static const int SIZE = 8;
    int arr[SIZE];
    C() {
        for (int i=0;i<SIZE;++i)
           arr[i] = i;
    }
};

int* begin(C& c) {
    return c.arr;
}
int* end(C& c) {
    return c.arr + c.SIZE;
}


int main(int argc, char** argv)
{
    C c;
    for (auto& e : c)
        p(e);

    return 0;
}
