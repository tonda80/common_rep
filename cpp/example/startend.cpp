#include "../common.h"



bool startswith(const std::string& s, const std::string& pref) {
	return s.rfind(pref, 0) == 0;
}

bool endswith(const std::string& s, const std::string& suf) {
	if (s.size() < suf.size()) {
		return false;
	}
	size_t n = s.size() - suf.size();
	return s.find(suf, n) == n;
}


int main(int argc, const char** argv)
{
	p("startswith");
	p(startswith("qwetitito", "we") == false);
	p(startswith("ftyrew", "fty") == true);
	p(startswith("qwe", "qwertyui") == false);
	p(startswith("qasxz", "") == true);
	p(startswith("", "asass") == false);
	p(startswith("", "") == true);

	p("endswith");
	p(endswith("qwetitito", "wet") == false);
	p(endswith("qwetitito", "tito") == true);
	p(endswith("ito", "sqsqsqsstito") == false);
	p(endswith("qweti", "") == true);
	p(endswith("", "zz") == false);
	p(endswith("", "") == true);

	return 0;
}
