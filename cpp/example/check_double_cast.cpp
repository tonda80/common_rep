#include "common.h"



int main(int argc, const char** argv)
{

	for(uint64_t i = 0; i < (1<<24); ++i) {
		double d = i;
		if (static_cast<uint64_t>(d) != i) {
			std::cout << "ooops, " << d << " != " << i << std::endl;
			return 1;
		}
		//std::cout << d << " == " << i << std::endl;
	}

	std::cout << "OK\n";
    return 0;
}
