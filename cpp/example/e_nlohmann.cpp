// эксперименты с nlohmann json
// https://github.com/nlohmann/json  тут и туториал
// https://json.nlohmann.me/api/basic_json/  дока
// g++ -I /home/ant/strange_code/json/single_include e_nlohmann.cpp

#include "../common.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;


int main(void)
{
	json j1 = {
	  {"ok", true},
	  {"f", 3.141},
	  {"i", 7},
	};

	// err p(j1["i"] + 1);
	p(j1["i"].get<int>() + 1);
	// get int as string -- exception  p(j1["i"].get<std::string>());

	json j2;
	j2["i"] = j1["i"];
	j2["absent"] = j1["absent"];

	pp("j1", j1.dump(2));
	pp("j2", j2.dump(2));

    // массивы и std::remove

    json a0 = json::array({0, 1, 2, 3, 2, 2, "qwe"});
    json a1 = {4, 5, 6, 7, 4, "q", nullptr, 4};

    pp("a0", a0.dump());
	pp("a1", a1.dump());

    a0.erase(std::remove(a0.begin(), a0.end(), 2), a0.end());
    a1.erase(std::remove(a1.begin(), a1.end(), 4), a1.end());
    // и удалим чего нет
    a0.erase(std::remove(a0.begin(), a0.end(), 222), a0.end());
    a1.erase(std::remove(a1.begin(), a1.end(), 444), a1.end());

    pp("a0", a0.dump());
	pp("a1", a1.dump());

	return 0;
}
