#pragma once

#include <iostream>

#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <tuple>
#include <unordered_map>
#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <ctime>
#include <memory>
#include <functional>
#include <thread>
#include <chrono>
#include <atomic>
#include <future>
#include <string_view>
#include <regex>


static void print(const char* s) {while (*s){if (*s == '{' && *++s == '}')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}

template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '{' && *++s == '}'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

template <typename T> void p(const T& obj, const std::string& e = "\n") {std::cout << obj << e;}
//void p(const char* p, const std::string& e = "\n") {if (p == nullptr) std::cout << "__NULL__" << e; else std::cout << p << e;}


template<typename T>
void pp(const T& t) {p(t);}
template<typename T, typename... Args>
void pp(const T& t, const Args&... args) {p(t, " "); pp(args...);}

#define P2(x) pp(#x, "=", x)


template <typename T>
void p_map(const T& mp, const std::string& pref="") {
	if (!pref.empty()) p(pref);
	for (auto it=mp.begin(); it!=mp.end(); ++it) {
		if (it != mp.begin()) p(",", " ");
		p(it->first, ": "); p(it->second, "");
	}
	p("");
}

template <typename T>
void p_cont(const T& ct, const std::string& pref="") {
	if (!pref.empty()) p(pref);
	for (auto it=std::begin(ct); it!=std::end(ct); ++it) {
		if (it != std::begin(ct)) p(",", " ");
		p(*it, "");
	}
	p("");
}

static int randint(int min, int max) {
	// returns[min, max)
	static bool once = true;
	if (once) {
		once = false;
		// auto seed = time(nullptr);	// плохо работает при одновременном запуске нескольких программ
		auto seed = std::chrono::duration_cast< std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() % 100000000;
		srand(seed);
	}
	return rand()%(max - min) + min;
}

static void sleep(int ms) {
	std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}
