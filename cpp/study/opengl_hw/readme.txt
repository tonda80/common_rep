Учимся по https://habrahabr.ru/post/311198/ (последние уроки https://habr.com/post/347354/)
Или лучше http://www.opengl-tutorial.org/ru/beginners-tutorials/tutorial-1-opening-a-window/

Линукс, сборка зависимостей
---

* GLFW
  * git@github.com:glfw/glfw.git
  * apt-get install xorg-dev
  * cmake -DBUILD_SHARED_LIBS=ON

* GLEW
  * не понял почему в репозитории нет исходников, но в архиве есть
  * apt-get install build-essential libxmu-dev libxi-dev libgl-dev (?? libosmesa-dev dos2unix wget)
  * make glew.lib
  * при подключении в исходниках требует GL/glu.h => apt-get install libglu1-mesa-dev


hw0.cpp - из https://habr.com/post/311234/, + добавил разноцветное мыргание
