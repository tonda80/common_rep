cmake_minimum_required( VERSION 3.3 )

# change it if any
set (GLFW_ROOT "/home/ant/strange_code/glfw")
set (GLFW_BUILD "/home/ant/strange_code/glfw_build")
set (GLEW_ROOT "/home/ant/strange_code/glew-2.1.0")

set (GLFW_HEADERS "${GLFW_ROOT}/include")
set (GLFW_LIB "${GLFW_BUILD}/src")
set (GLEW_HEADERS "${GLEW_ROOT}/include")
set (GLEW_LIB "${GLEW_ROOT}/lib")

project (hello_opengl)

set (CMAKE_CXX_STANDARD 11)

include_directories (${GLFW_HEADERS} ${GLEW_HEADERS})
set (SOURCES src/hw0.cpp)

# custom libs
link_directories(${GLFW_LIB} ${GLEW_LIB})

# system libs
find_package(Threads REQUIRED )
find_package(OpenGL REQUIRED)
find_package(X11 REQUIRED)

add_executable (${PROJECT_NAME} ${SOURCES})
target_link_libraries (${PROJECT_NAME} GLEW glfw3 dl ${X11_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} ${OPENGL_LIBRARIES})
# наверное уже в X11_LIBRARIES: Xrandr Xi 
