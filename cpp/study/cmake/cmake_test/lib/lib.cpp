#include "lib.h"

void libfunc2() {

	std::cout << "-- libfunc2 --" << std::endl;

#ifdef LIB_TARG_DEF2
	std::cout << "LIB_TARG_DEF2 is defined" << std::endl;
#else
	std::cout << "LIB_TARG_DEF2 is NOT defined" << std::endl;
#endif

	C().libfunc1();
}

void C::libfunc22() {
	std::cout << "-- libfunc22 --" << std::endl;
	libfunc1();
}
