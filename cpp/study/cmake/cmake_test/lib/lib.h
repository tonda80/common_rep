#include <iostream>

#ifndef LIB_H
#define LIB_H


struct C {
	void libfunc1() {
		std::cout << "-- libfunc1 --" << std::endl;

		#ifdef LIB_TARG_DEF1
			std::cout << "LIB_TARG_DEF1 is defined" << std::endl;
		#else
			std::cout << "LIB_TARG_DEF1 is NOT defined" << std::endl;
		#endif
	}

	void libfunc22();
};


void libfunc2();


#endif // LIB_H
