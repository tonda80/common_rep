#include <iostream>
#include <lib.h>

int main(int argc, const char** argv) {

	std::cout << "-- main --" << std::endl;

#ifdef TARG_COMP_DEF
	std::cout << "TARG_COMP_DEF is defined" << std::endl;
#else
	std::cout << "TARG_COMP_DEF is NOT defined" << std::endl;
#endif

	C c;
	c.libfunc1();
	c.libfunc22();

	libfunc2();

	return 0;
}
