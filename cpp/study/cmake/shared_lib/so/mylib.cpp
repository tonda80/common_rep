#include "mylib.h"
#include <stdexcept>

#include <iostream>

int f_inc(int i)
{
    return ++i;
}


C::C() : i(0)
{}

void C::inc_i(int d)
{
    std::cout << "C::inc_i " << d  << std::endl;

    if (d == -1)
        throw std::runtime_error("test runtime_error");

	i += d;
}