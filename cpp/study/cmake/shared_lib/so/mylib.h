#ifndef __MYLIB_H__
#define __MYLIB_H__

#if defined (_WIN32)
    #define COMMON_EXPORT __declspec(dllexport)

#else
    #define COMMON_EXPORT
#endif  // defined (_WIN32)

int f_inc(int i);

class COMMON_EXPORT C
{
public:
	C();
	int get_i() {return i;}
	void inc_i(int d=1);

private:
	int i;
};


#endif // __MYLIB_H__