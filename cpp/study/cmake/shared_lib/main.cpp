#include <iostream>
#include <string>
#include <thread>
#include "mylib.h"

template <typename T> void p(const T& obj, const char e = '\n') { std::cout << obj << e; }

void thread_func1()
{
    p("thread!");
    //exit(2);
    //throw std::runtime_error("test thread runtime_error");
}

void thread_func2()
{
    C c;
    c.inc_i(3);
    p(c.get_i());
}


int main(int argc, char** argv) try {
    {
        C c;

        try {
            c.inc_i(-1);
        }
        catch (const std::exception& e) {
            p(std::string("I have caught an exception from lib: ") + e.what());
        }

        try {
            std::thread thr(thread_func2);
            thr.join();
        }
        catch (...) {
            p(std::string("I catch here nothing"));
        }

        //c.inc_i(-1);

        p(c.get_i());

        return 0;
    }
}
catch (const std::exception& e) {
    p(std::string("main: an exception from lib: ") + e.what());

    return 1;
}