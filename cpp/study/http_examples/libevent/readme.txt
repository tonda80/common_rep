https://libevent.org/
http://www.wangafu.net/~nickm/libevent-book/
https://habr.com/ru/post/217437/


Сборка
--
git clone https://github.com/libevent/libevent.git -b patches-2.1 --depth=1
git checkout patches-2.1
./autogen.sh
sudo apt install libssl-dev checkinstall [+?]
./configure [--disable-shared]
make -j4
checkinstall


evthread_use_pthreads для работы (даже просто остановки loop-а) из разных потоков

Линковка
--
event[.a]
event_pthreads[.a]	для evthread_use_pthreads
