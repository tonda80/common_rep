// по мотивам кода из и libevent/sample/http-server.c и https://habr.com/ru/post/217437/
// чтоб не использовать deprecated

#include <memory>
#include <iostream>
#include <filesystem>
#include <cassert>
#include <csignal>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <event.h>
#include <event2/http.h>
#include <event2/buffer.h>
#include <event2/thread.h>


#define assertm(exp, msg) assert(((void)msg, exp))


const char* SrvAddress = "127.0.0.1";	// "0.0.0.0"
const std::uint16_t SrvPort = 12543;



bool startswith(const std::string& s, const std::string& pref) {
	return s.rfind(pref, 0) == 0;
}

int cnt = 0;


class FileToAdd {
public:
	FileToAdd(const char* path) {
		_fd = open(path, O_RDONLY);
	}

	FileToAdd(const std::filesystem::path& path) : FileToAdd(path.string().c_str()) {}

	~FileToAdd() {
		if (_fd >= 0) {
			close(_fd);
		}
	}

	operator bool() const {return _fd >= 0;}

	int fd() const {return _fd;}

private:
	int _fd = -1;
};


auto get_path = std::filesystem::canonical(GET_PATH);


event_base* g_event_base = nullptr;

void signal_handler(int/*signal*/) {
	if (g_event_base) {
		event_base_loopbreak(g_event_base);		// event_base_loopexit
	}
}


int main()
{
	// можно включить лог
	//event_enable_debug_logging(EVENT_DBG_ALL);

	// если надо (тут не надо) включаем поддержку потоков
	//evthread_use_pthreads();

	// init
	std::unique_ptr<event_base, decltype(&event_base_free)> ev_base (event_base_new(), event_base_free);
	assertm(ev_base, "event_base_new fail");

	std::unique_ptr<evhttp, decltype(&evhttp_free)> http_server (evhttp_new(ev_base.get()), evhttp_free);
	assertm(http_server, "evhttp_new fail");

	// callbacks
	// 1
	evhttp_set_cb(http_server.get(), "/hi", [](struct evhttp_request* req, void*) {
		std::cout << "received 'hi' request" << std::endl;

		auto *outbuf = evhttp_request_get_output_buffer(req);
		assertm(outbuf, "evhttp_request_get_output_buffer fail");

		evbuffer_add_printf(outbuf, "<html><body><center><h1>Hello, world (%d)!</h1></center></body></html>", cnt++);

		evhttp_send_reply(req, HTTP_OK, "", outbuf);
	}, nullptr);

	// 2
	evhttp_set_cb(http_server.get(), "/favicon.ico", [](struct evhttp_request* req, void*) {
		std::cout << "received 'favicon.ico' request" << std::endl;

		auto *outbuf = evhttp_request_get_output_buffer(req);
		assertm(outbuf, "evhttp_request_get_output_buffer fail");

		FileToAdd f(FAVICON_PATH);
		assertm(f.fd() != -1, "favicon.ico open fail");

		//struct stat st;
		//assertm(fstat(fd, &st) == 0, "fstat fail");

		assertm(
			evhttp_add_header(evhttp_request_get_output_headers(req), "Content-Type", "image/x-icon") == 0,				// https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_MIME-%D1%82%D0%B8%D0%BF%D0%BE%D0%B2
		    "evhttp_add_header fail");
		assertm(
			evbuffer_add_file(outbuf, f.fd(), 0, -1) == 0,
			"evbuffer_add_file fail");

		evhttp_send_reply(req, HTTP_OK, "", outbuf);
	}, nullptr);

	// default
	evhttp_set_gencb(http_server.get(), [](struct evhttp_request* req, void*) {
		const char* uri = evhttp_request_get_uri(req);
		int command = evhttp_request_get_command(req);
		std::cout << "received request: uri=" << uri << " command=" << command << std::endl;

		// parsed_uri выделяет такие элементы scheme://[[userinfo]@]foo.com[:port]]/[path][?query][#fragment]
		// можно сразу evhttp_request_get_evhttp_uri
		std::unique_ptr<evhttp_uri, decltype(&evhttp_uri_free)> parsed_uri(evhttp_uri_parse(uri), evhttp_uri_free);
		if (!parsed_uri) {
			std::cout << "'" << uri << "' is not a good URI. Sending BADREQUEST" << std::endl;
			evhttp_send_error(req, HTTP_BADREQUEST, "wrong uri");
			return;
		}

		const char* path = evhttp_uri_get_path(parsed_uri.get());
		if (!path) path = "/";

		const char* query = evhttp_uri_get_query(parsed_uri.get());
		const char* decoded_query = "";
		if (query) {
			decoded_query = evhttp_uridecode(query, 0, nullptr);	// %D0%93%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B0%D1%86%D0%B8%D1%8F
			assertm(decoded_query, "evhttp_uridecode fail");
		}
		// тут недоработка, после evhttp_uridecode надо высвобождать память

		std::cout << "path=" << path << " decoded_query=" << decoded_query << std::endl;
		return;
		if (!startswith(uri, "/get")) {
			evhttp_send_error(req, HTTP_BADREQUEST, "unknown request");
			return;
		}

		// есть evhttp_parse_query_str для парсинга query

		std::filesystem::path p = get_path/decoded_query;

		if (!std::filesystem::is_regular_file(p)) {
			evhttp_send_error(req, HTTP_NOTFOUND, "no such file");
			return;
		}

		FileToAdd f(p);
		std::cout << "send " << p << " " << f.fd() << std::endl;

		auto outbuf = evhttp_request_get_output_buffer(req);
		assertm(outbuf, "evhttp_request_get_output_buffer fail");

		const char* content_type = "application/octet-stream";
		// https://stackoverflow.com/questions/12539058/is-there-a-default-mime-type
		// firefox по Content-Disposition сам выводит тип
		assertm(
			evhttp_add_header(evhttp_request_get_output_headers(req), "Content-Type", content_type) == 0,
			"evhttp_add_header fail");

		assertm(
			evbuffer_add_file(outbuf, f.fd(), 0, -1) == 0,
			"evbuffer_add_file fail");

		// имя файла для браузера
		auto disp = std::string("inline; filename=") + p.filename().string();
		assertm(
			evhttp_add_header(evhttp_request_get_output_headers(req), "Content-Disposition", disp.c_str()) == 0,
			"evhttp_add_header fail");

		evhttp_send_reply(req, HTTP_OK, "", outbuf);
	}, nullptr);


	// start
	int res;
	//res = evhttp_bind_socket(http_server.get(), SrvAddress, SrvPort);
	auto bound_socket = evhttp_bind_socket_with_handle(http_server.get(), SrvAddress, SrvPort);
	//if (res != 0) {
	if (!bound_socket) {
		std::cerr << "evhttp_bind_socket fail" << std::endl;
		return -1;
	}

	g_event_base = ev_base.get();
	std::signal(SIGINT, signal_handler);	// SIGINT 2

	std::cout << "listening to http://" << SrvAddress << ":" << SrvPort << std::endl;
	std::cout << "GET_PATH=" << get_path << std::endl;
	std::cout << "starting event dispatching loop" << std::endl;
	res = event_base_dispatch(ev_base.get());
	if (res != 0) {
		std::cerr << "Failed to run event_base_dispatch loop. " << res << std::endl;
		return -1;
	}

	std::cout << "loop is stopped" << std::endl;

	// тест перезапуска
	while (0) {
		// тест закрытия сокета
		bool del_sock = false;
		if (del_sock) {
			evhttp_del_accept_socket(http_server.get(), bound_socket);
		}

		sleep(30);

		if (del_sock) {
			bound_socket = evhttp_bind_socket_with_handle(http_server.get(), SrvAddress, SrvPort);
		}

		std::cout << "starting loop again" << std::endl;
		if (event_base_dispatch(ev_base.get()) != 0) {
			std::cerr << "restart fail" << std::endl;
			return -1;
		}
		std::cout << "loop is stopped again" << std::endl;
	}



	return 0;
}
