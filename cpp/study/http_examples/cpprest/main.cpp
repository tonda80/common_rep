// https://habr.com/ru/company/infopulse/blog/226557/
// cpprestsdk/Release/samples/BingRequest/bingrequest.cpp


#include <cpprest/http_client.h>


int main(int argc, char* args[])
{
	// curl http://www.cbr.ru/scripts/XML_daily.asp

	web::http::client::http_client client(U("http://www.cbr.ru"));		// https://en.cppreference.com/w/cpp/language/character_literal
	auto request = client.request(web::http::methods::GET, U("/scripts/XML_daily.asp"));	//web::uri_builder().to_string()

	request.then([](web::http::http_response response) {
		if(response.status_code() == web::http::status_codes::OK) {
			std::cout << "response body: " << response.to_string() << std::endl;
		}
		else {
			std::cout << "bad answer: " << response.status_code() << std::endl;		// response.to_string()
		}

	})
	.wait();

    return 0;
}
