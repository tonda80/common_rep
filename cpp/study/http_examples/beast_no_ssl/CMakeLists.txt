cmake_minimum_required (VERSION 3.10)

project (beast_example)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror=return-type -Werror=missing-field-initializers")

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

find_package(Boost 1.72 REQUIRED)


set(SRC_FILES
	main.cpp
)


add_executable(${CMAKE_PROJECT_NAME} ${SRC_FILES} )

target_include_directories(${CMAKE_PROJECT_NAME} PRIVATE
	${Boost_INCLUDE_DIR}
)

target_link_libraries(${CMAKE_PROJECT_NAME}
	Threads::Threads
)
