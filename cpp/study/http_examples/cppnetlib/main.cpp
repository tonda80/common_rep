// https://cpp-netlib.org/0.13.0/index.html#
// cpp-netlib-0.13.0-final/libs/network/example/http_client.cpp


#include <boost/network/protocol/http.hpp>
#include <string>
#include <iostream>


using namespace boost::network;
using namespace boost::network::http;


int main(int argc, char* args[])
{
	client::request request_("http://127.0.0.1:8000/");
	request_ << header("Connection", "close");
	client client_;
	client::response response_ = client_.get(request_);
	std::string body_ = body(response_);


    return 0;
}


