https://cpp-netlib.org/

Сборка
--
http://downloads.cpp-netlib.org/0.13.0/cpp-netlib-0.13.0-final.zip
option( CPP-NETLIB_BUILD_TESTS ... OFF)
cmake .. -DCMAKE_BUILD_TYPE=Release && make
sudo checkinstall --pkgname cppnetlib_0.13.0 -y make install
