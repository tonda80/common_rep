#include <iostream>
#include <string>

//#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O
//#include <opencv2/imgproc.hpp>  // Gaussian Blur

void print_help_and_exit()
{
	std::cout << "usage: ./opencv_hw VideoCapture_in\nexiting\n";
	exit(1);
}

std::tuple<int, std::string> get_fourcc(double d) {
	union { int v; char c[5];} uEx ;
	uEx.v = static_cast<int>(d);
	uEx.c[4]='\0';
	return {uEx.v, uEx.c};
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		print_help_and_exit();
	}

	cv::VideoCapture vIn(argv[1]);

	const char* Wname = "TSTWIN";
	//cv::namedWindow(Wname, cv::WINDOW_AUTOSIZE);		// WINDOW_NORMAL
	//cv::moveWindow(Wname, 400, 100);
	//cv::resizeWindow(Wname, 200, 200);

	cv::Mat frame;
	int frameNum = 0;

	while (1) {
		vIn >> frame;

		if (frame.empty()) {
			std::cout << "this is the end\n";
			break;
		}
		std::cout << "Frame: " << frameNum++ << "# " << vIn.get(cv::CAP_PROP_POS_MSEC) << " " << vIn.get(cv::CAP_PROP_POS_FRAMES ) << std::endl;

		cv::imshow(Wname, frame);

		cv::waitKey(1000/24/10);
	}

	auto fourcc = get_fourcc(vIn.get(cv::CAP_PROP_FOURCC));
	std::cout << "CAP_PROP_FOURCC=" << std::get<0>(fourcc) << " " << std::get<1>(fourcc) << std::endl;
	// avc1 - h264
	// https://docs.microsoft.com/en-us/windows/win32/directshow/h-264-video-types
	// https://softron.zendesk.com/hc/en-us/articles/207695697-List-of-FourCC-codes-for-video-codecs

	//std::cout << "press enter to exit "; std::cin.get();

	return 0;
}


// и вариант с картинкой
int main2( int argc, char** argv ) {
	if( argc != 2) {
		std::cout <<" Usage: display_image ImageToLoadAndDisplay" << std::endl;
		return -1;
	}

	cv::Mat image;
	image = cv::imread(argv[1], cv::IMREAD_COLOR);   // Read the file
	if(! image.data ) {                              // Check for invalid input
		std::cout <<  "Could not open or find the image" << std::endl ;
		return -1;
	}

	cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
	cv::imshow( "Display window", image );                   // Show our image inside it.

	cv::waitKey(0);                                          // Wait for a keystroke in the window
	return 0;
}
