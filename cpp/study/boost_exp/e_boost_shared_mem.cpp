// Пытаемся разобраться с шареной памятью по книжке Полухина (с++ с использованием буст), глава быстрая передача данных из одного процесса в другой

// g++ --std=c++17  e_boost_shared_mem.cpp -lrt -pthread

#include "../../common.h"

#include <boost/interprocess/managed_shared_memory.hpp>


namespace bi = boost::interprocess;

int main(int argc, const char** argv) {

    const char* cnt_name = "def_cnt";
    if (argc > 1) {
        cnt_name = argv[1];
    }
    pp("cnt name", cnt_name);

    // managed_shared_memory типа продвинутый объект, в родной документации используют shared_memory_object + mapped_region

    bi::managed_shared_memory segm{bi::open_or_create, "e_boost_mem3", 4096};
    pp(cnt_name, "opened");
    pp("sizes", segm.get_size(), segm.get_free_memory());

    // Список методов managed_shared_memory нашел только в /usr/include/boost/interprocess/detail/managed_memory_impl.hpp

    std::atomic_int& cnt = *segm.find_or_construct<std::atomic_int>(cnt_name)(0);

    cnt++;
    pp(cnt_name, cnt.load());

    //p("\npress to continue"); std::cin.get();

    if (argc > 2 && argv[2] == std::string("d")) {
        segm.destroy<std::atomic_int>(cnt_name);
        pp(cnt_name, "deleted");
    }

    return 0;
}

