// Тестируем работу шареной памяти

// g++ --std=c++17  e_boost_shared_mem.cpp -lrt -pthread
// g++ --std=c++11  e_boost_shared_mem.cpp -lrt -pthread

#include <atomic>
#include <iostream>
#include <thread>
#include <boost/interprocess/managed_shared_memory.hpp>

namespace bi = boost::interprocess;

//using TCnt = std::atomic_uint64_t;
//using TCnt = std::uint64_t;
using TCnt = std::atomic_uint_least64_t;

TCnt* pcnt = nullptr;

void init_ptr(const char* sgm_name, const char* cnt_name) {
    std::cout << "sgm_name " << sgm_name  << " cnt_name " << cnt_name << std::endl;

    bi::managed_shared_memory segm{bi::open_or_create, sgm_name, 4096};
    std::cout << "sizes " << segm.get_size() << " " << segm.get_free_memory() << std::endl;

    pcnt = segm.find_or_construct<TCnt>(cnt_name)(7);
}


int main(int argc, const char** argv) {

    const char* sgm_name = "test0";
    const char* cnt_name = "cnt0";
    if (argc > 1) {
        cnt_name = argv[1];
    }

    init_ptr(sgm_name, cnt_name);

    while (1) {
        //std::uint64_t old_cnt = *pcnt;
        std::uint64_t old_cnt = pcnt->load();

        (*pcnt)++;

        //std::uint64_t d = *pcnt - old_cnt;
        std::uint64_t d = pcnt->load() - old_cnt;
        if (d != 1)
            std::cout << std::this_thread::get_id() << " " << old_cnt << " " << d << std::endl;

        //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    return 0;
}

