#include <iostream>
#include <string>

#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

class C
{
    int i;
    double d;
    std::string s;

public:
    C() : i(1), d(2.0) {}
    C(int i_, double d_, std::string s_="lala") : i(i_), d(d_), s(s_) {}
    int getI() const {return i;}
    double getD() const {return d;}
    std::string getS() const {return s;}

    std::string about() const {return std::to_string(i) + ":" + std::to_string(d) + ":" + s;}
};

struct CIntId;
struct CDoubleId;
struct CStrId;

using namespace std;
using namespace boost::multi_index;

using OrderBook = multi_index_container<
    C,
    indexed_by<
        ordered_unique<
                tag<CIntId> ,
                const_mem_fun<C, int, &C::getI> >,
        ordered_non_unique<
                tag<CDoubleId>,
                const_mem_fun<C, double, &C::getD> >,
        ordered_non_unique<
                tag<CStrId>,
                const_mem_fun<C, std::string, &C::getS> >
        >
    >;

int main()
{
    OrderBook bb;

    p(bb.size());

    p("----");

    bb.insert(C(5, 5.5));
    bb.insert(C(55, 5.5, "fe"));
    bb.insert(C(55, 5.5, "me"));
    bb.insert(C(555, 5.5, "me"));
    bb.insert(C(1, 5.5, "ze"));
    bb.insert(C(2, 5.25, "ze"));

    auto iters1 = bb.template get<CDoubleId>().equal_range(5.5);
    for(auto& i= iters1.first; i != iters1.second; ++i)
        p(i->about());
    p("----");
    auto iters2 = bb.template get<CStrId>().equal_range("ze");
    for(auto& i= iters2.first; i != iters2.second; ++i)
        p(i->about());

    p("----");
    p(bb.size());
    p("----");

    bb.clear();
    p(bb.size());

    return 0;
}
