g++ --std=c++17 -o e_ipc_mtx.out e_boost_ipc_mtx.cpp -lpthread -lrt -lboost_date_time || exit 1
#./e_ipc_mtx		# clean

./e_ipc_mtx qw &
#sleep 0.2
./e_ipc_mtx as &
./e_ipc_mtx df &
./e_ipc_mtx re &
./e_ipc_mtx hu &
# to kill with lock: export D=/tmp/ipc_mtx && mkdir -p "$D" && touch $D/hu

read -p " << Press enter to stop >>  "
killall -9 e_ipc_mtx
