#include "../common.h"

#include <boost/algorithm/string.hpp>

#include <boost/asio.hpp>

//g++ --std=c++17 e_boost_mix.cpp -pthread [-I/boost/headers]


using ErrCode = const boost::system::error_code&;

struct Timer {
    boost::asio::deadline_timer timer;
    const boost::posix_time::seconds period;
    const char type;
    int cnt = 0;
    std::function<void(int)> clb;
    bool work = true;

    Timer(boost::asio::io_context& io_context, uint32_t seconds, char type_, std::function<void(int)> clb_= std::function<void(int)>())
        : timer(io_context), period(seconds), type(type_), clb(clb_) {}

    void start_timer() {
		timer.expires_from_now(period);
		timer.async_wait([this] (ErrCode err) {
			if (!err) on_timer();
			else pp(type, "err", err);
		});
	}

	void stop_timer() {
		timer.cancel();	// можно вызывать много раз timer.cancel();
		work = false;
	}
private:
	void on_timer() {
		++cnt;
		pp("on_timer", type);
		if (work) {
			start_timer();
		}

		if (clb) {
			clb(cnt);
		}
	}
};


int main(int argc, char** argv)
{
	std::string_view str1 = "#P#\r\n";

	std::vector<std::string> parts;
	boost::algorithm::split(parts, str1, boost::is_any_of("#"));

	p_cont(parts);
	pp(parts.front().empty(), boost::algorithm::ends_with(parts.back(), "\r\n"));
	parts.back().resize(parts.back().size()-2);
	pp(parts.back().empty());

	std::string_view str2 = "#AL#1\r\n";

	boost::algorithm::split(parts, str2, boost::is_any_of("#"));

	p_cont(parts);
	pp(parts.front().empty(), boost::algorithm::ends_with(parts.back(), "\r\n"));
	parts.back().resize(parts.back().size()-2);
	pp(parts.back() == "1", parts.back().c_str());

	p("-- end of split test --");

	boost::asio::io_context io_context;
	auto tmr_b = std::make_unique<Timer>(io_context, 0, 'b');

	Timer tmr_a(io_context, 1, 'a', [&tmr_b](int cnt) {
		if (cnt%7 == 0) {
			p("!stop tmr_b!");
			// так tmr_b->stop_timer();
			// или так
			tmr_b.reset();
		}
	});

	tmr_a.start_timer();
	tmr_b->start_timer();


	// тестируем expired таймер
	tmr_b->timer.wait();
	p("tmr_b expired");
	tmr_b->timer.cancel();		//tmr_b.reset();
	p("io_context.run()");


	io_context.run();

    return 0;
}
