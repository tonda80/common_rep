// попробуем тут уже наконец собрать шпаргалку по полезным бустовым строковым операциям

// g++ -I/opt/boost_1.72/include e_boost_string.cpp

#include "../common.h"

#include <boost/algorithm/string.hpp>


int main(int argc, char** argv)
{
	p("-- replace all, erase");

	std::string s = "test; string; with ;;;;;;;;; symbols";
	p(s);
	boost::replace_all(s, ";;", ";");
	p(s);
	boost::replace_all(s, ";", ",");
	p(s);
	boost::replace_all(s, ",,", "@");
	p(s);
	auto s2 = boost::replace_all_copy(s, "@", "");
	p(s2);
	boost::replace_all(s2, "sym", "sym!");
	p(s2);
	boost::erase_all(s2, "st");
	p(s2);
	boost::erase_all(s2, " ");
	p(s2);

	p("-- join");
	p(boost::algorithm::join(std::list<std::string>({"i", "am", "boost"}), "! "));

	std::vector<std::string> v = {"1","2","3", "4"};
	auto sj = boost::algorithm::join(v, ",");
	pp(sj, sj.size());

	p("-- lower");
	s = "HELLO, WORLD!";
	boost::algorithm::to_lower(s);
	p(s);
	s2 = "ПрЕвЕд МЕДВЕД!";
	//std::locale loc("ru_RU.utf8");
	setlocale(LC_ALL, "ru_RU.utf8");
	p(boost::algorithm::to_lower_copy(s2));		// TODO

	p("-- starts ends");
	p(boost::algorithm::starts_with("qwerty", "qwe"));
	p(boost::algorithm::starts_with("qwerty", "asd"));
	p(boost::algorithm::ends_with("qwerty", "rty"));
	p(boost::algorithm::ends_with("qwerty", "asd"));

    return 0;
}
