// Эксперимент (а заодно и шпаргалка) по работе с boost::interprocess::named_mutex

// g++ --std=c++17 -o e_ipc_mtx e_boost_ipc_mtx.cpp -lpthread -lrt -lboost_date_time
// rt - для shared_memory, boost_date_time - для преобразования ptime в строку

#include "../../common.h"

#include <mutex>
#include <filesystem>
#include <csignal>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread_time.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

namespace bi = boost::interprocess;
namespace bpt = boost::posix_time;

std::lock_guard<bi::named_mutex> usual_lock(bi::named_mutex& mtx);
std::lock_guard<bi::named_mutex> anti_hangup_lock(std::unique_ptr<bi::named_mutex>& p_mtx);

const char* version = "2";
const char* g_mtx_name = "modbus_mutex__home_berezin_temp_ptys_tty1"; // "mtx_test";
const char* g_create_mtx_pref = "mtx_test_create";
const char* g_inst_name = nullptr;
std::uint64_t* g_creat_ts = nullptr;


uint64_t microseconds_since_epoch(const bpt::ptime& time) {
	bpt::time_duration duration = time - bpt::from_time_t(0);
    return duration.total_microseconds();
}

bool check_hangup() {
	std::filesystem::path test_path = std::filesystem::path("/tmp/ipc_mtx")/g_inst_name;
	bool res = std::filesystem::exists(test_path);
	if (res) {
		bool res2 = std::filesystem::remove(test_path);
		pp(test_path, "remove result", res2);
	}
	return res;
}


int main(int argc, const char** argv) {
	if (argc <= 1) {
		bool res = bi::named_mutex::remove(g_mtx_name);
		pp("remove mutex returned", res, "exiting");

		return 1;
	}
	g_inst_name = argv[1];

	//std::signal(SIGUSR1, signal_handler);		// terminate called after throwing an instance of 'boost::interprocess::interprocess_exception'   what():  Interrupted system call

	bi::managed_shared_memory segm{bi::open_or_create, "mtx_test_shmem", 1024};
	g_creat_ts = segm.find_or_construct<std::uint64_t>("creat_ts")(0);
	assert(g_creat_ts);

	//bi::named_mutex ipc_mtx{bi::open_or_create, g_mtx_name};
	auto p_ipc_mtx = std::make_unique<bi::named_mutex>(bi::open_or_create, g_mtx_name);
	/* // таки нет необходимости тут различать создание и открытие
	std::unique_ptr<bi::named_mutex> p_ipc_mtx;
	if (std::string("qw") == g_inst_name) {
		p_ipc_mtx = std::make_unique<bi::named_mutex>(bi::create_only, g_mtx_name);		// оно на самом деле и open делает
	} else {
		p_ipc_mtx = std::make_unique<bi::named_mutex>(bi::open_only, g_mtx_name);
	}
	*/

	pp("loop", g_inst_name, version);
	int cnt = 0;
	while (1) {
		++cnt;
		int delay = randint(250, 5000);
		{
			//std::lock_guard<bi::named_mutex> lock(ipc_mtx);
			//auto lock = usual_lock(*p_ipc_mtx);
			auto lock = anti_hangup_lock(p_ipc_mtx);

			if (check_hangup()) {
				pp("Hang up!");
				kill(getpid(),SIGKILL);
			}

			pp("LOCK", g_inst_name, delay/1000);
			sleep(delay);
		}
		pp("UNLO", g_inst_name);
		sleep(1);
	}

	return 0;
}

std::lock_guard<bi::named_mutex> usual_lock(bi::named_mutex& mtx) {
	return std::lock_guard<bi::named_mutex>(mtx);
}


std::lock_guard<bi::named_mutex> anti_hangup_lock(std::unique_ptr<bi::named_mutex>& p_mtx) {
	int wait_time = 25000;
	int wait_time2 = 10;
	 while (1) {
		//bpt::ptime pt =  bpt::microsec_clock::local_time() + bpt::milliseconds(wait_time);		// НЕ РАБОТАЕТ
		bpt::ptime lock_start =  boost::get_system_time();
		bpt::ptime pt =  lock_start + bpt::milliseconds(wait_time);
		//pp("__deb anti_hangup_lock", to_simple_string(lock_start), microseconds_since_epoch(lock_start));
		bool res = p_mtx->timed_lock(pt);
		if (res) {
			return std::lock_guard<bi::named_mutex>(*p_mtx, std::adopt_lock);
		}
		pp("Cant lock, need mutex repair", g_inst_name);

		// пробуем в лоб
		//bi::named_mutex::remove(g_mtx_name);
		//bi::named_mutex local_mtx{bi::open_or_create, g_mtx_name};			// такое пересоздание без переоткрытия не работает, мьютекс не лочится
		//p_mtx = std::make_unique<bi::named_mutex>(bi::open_or_create, g_mtx_name);	// и так не работает, в разных процессах как будто лочатся разные мьютексы
		//continue;

		// вариант с арбитром пересоздателем
		/*
		if (std::string("re") == g_inst_name) {
			bi::named_mutex::remove(g_mtx_name);
			p_mtx = std::make_unique<bi::named_mutex>(bi::create_only, g_mtx_name);
			pp("recreated", g_inst_name);
		} else {
			try {
				p_mtx = std::make_unique<bi::named_mutex>(bi::open_only, g_mtx_name);
			} catch (bi::interprocess_exception& e) {
				continue;
			}
			pp("reopened", g_inst_name);
		}
		*/

		for (int i = 0; i < 10; ++i) {
			std::string create_mtx_name = std::string(g_create_mtx_pref) + std::to_string(i);
			bi::named_mutex create_mtx{bi::open_or_create, create_mtx_name.c_str()};
			pt =  boost::get_system_time() + bpt::milliseconds(wait_time2);
			if (!create_mtx.timed_lock(pt)) {
				// такое может быть только если и этот мьютекс испорчен
				continue;
			}
			std::lock_guard<bi::named_mutex> create_guard(create_mtx, std::adopt_lock);
			// таки удалось залочить i-й мьютекс пересоздания

			if (microseconds_since_epoch(lock_start) > *g_creat_ts) {
				// мьютекс старый, надо пересоздавать
				bi::named_mutex::remove(g_mtx_name);
				p_mtx = std::make_unique<bi::named_mutex>(bi::create_only, g_mtx_name);
				pp("recreated", g_inst_name);
				*g_creat_ts = microseconds_since_epoch(boost::get_system_time());
			} else {
				// мьютекс пересозданный, надо переоткрыть
				p_mtx = std::make_unique<bi::named_mutex>(bi::open_only, g_mtx_name);
				pp("reopened", g_inst_name);
			}

			// все сделали удалим мьютексы создания
			for (int j = 0; j <= i; ++j) {
				std::string create_mtx_name2 = std::string(g_create_mtx_pref) + std::to_string(j);
				bi::named_mutex::remove(create_mtx_name2.c_str());
			}
			break;
		}
	}
}
