#ifndef TESTHANDLER_H
#define TESTHANDLER_H

#include <QTextStream>

class TestHandler : public QObject {
    Q_OBJECT

    QTextStream out;
public:
    TestHandler() : out(stdout) {}

public slots:
    void btn1_click() {
        out << "btn1 click\n";
        out.flush();
    }
    void btn2_click() {
        out << "btn2 click\n";
        out.flush();
    }
};

// #include "main.moc"
// эту магию надо вставлять, если добавлять класс в main.cpp


#endif // TESTHANDLER_H
