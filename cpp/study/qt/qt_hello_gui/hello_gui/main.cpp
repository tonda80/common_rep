#include "mainwindow.h"

#include <QApplication>

#include <QTextStream>

#include <QWidget>
#include <QPushButton>
#include <QBoxLayout>
#include <QGridLayout>

#include "testhandler.h"


QTextStream out(stdout);


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    out << "app started\n";
    out.flush();

    w.setWindowTitle("Hello GUI");

    QPushButton* b1 = new QPushButton("Button1", &w);
    QPushButton* b2 = new QPushButton("Button2", &w);
    //b->setGeometry(200, 100, 100, 50);

    // нельзя назначать лейаут главному окну, поэтому вот такой виджет
    QWidget* wdg = new QWidget(&w);
    w.setCentralWidget(wdg);

    if (1) {
        QBoxLayout* l = new QBoxLayout(QBoxLayout::Direction::LeftToRight, wdg);
        l->addSpacing(10);
        l->addWidget(b1);
        l->addWidget(b2, 2);
    }
    else {
        QGridLayout* l = new QGridLayout(wdg);
        l->addWidget(b1, 0, 0);
        l->addWidget(b2, 1, 1);
    }

    TestHandler th;
    if (0) {
        QObject::connect(b1, SIGNAL(clicked()), &th, SLOT(btn1_click()));
        QObject::connect(b2, SIGNAL(clicked()), &th, SLOT(btn2_click()));
    }
    else {
        QObject::connect(b1, &QPushButton::clicked, &th, &TestHandler::btn1_click);
        QObject::connect(b2, &QPushButton::clicked, &th, &TestHandler::btn2_click);
    }

    w.show();
    return a.exec();
}
