// модуль который просто умеет загружаться и выгружаться

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/err.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ant");
MODULE_DESCRIPTION("A test Linux module.");
MODULE_VERSION("0.01");

// ---

static struct task_struct *test_kthread;

static int test_work_func(void *data)
{
    int i = 0;
    while (!kthread_should_stop()) {
        printk(KERN_INFO "test_work_func is working %d\n", i++);
        msleep(2000);
    }
    return 0;
}

// ---

static int __init test_km_init(void) {
	printk(KERN_INFO "Hello, kernel!\n");

	test_kthread = kthread_create(test_work_func, NULL, "test_kthread");
	if (IS_ERR(test_kthread)) {
		printk(KERN_ERR "cannot creare kthread for test_work_func\n");
		return -1;
	}
	wake_up_process(test_kthread);

	return 0;
}

static void __exit test_km_exit(void) {

	kthread_stop(test_kthread);

	printk(KERN_INFO "Goodbye, kernel!\n");
}

// ---

module_init(test_km_init);
module_exit(test_km_exit);
