// модуль который создает символьное устройство в которое можно писать и читать

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/device.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("ant");
MODULE_DESCRIPTION("A test Linux module.");
MODULE_VERSION("0.02");

#define PREF "test2: "

// ---

#define DEVNAME "testdevice_ab"

static int majorNumber;
static struct class* mydevCharClass = NULL;
static struct device* mydevCharDevice = NULL;
int read_counter = 1;

static int dev_open(struct inode *inodep, struct file *filep) {
    printk(KERN_INFO PREF "Device %s has been opened\n", DEVNAME);
    return 0;
}

static int dev_release(struct inode *inodep, struct file *filep) {
    printk(KERN_INFO PREF "Device %s successfully closed\n", DEVNAME);
    return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
    const char* msg = "I have nothing to say\n";
    size_t sz = min(strlen(msg), len);
    if (copy_to_user(buffer, msg, sz)) {
	return -EFAULT;
    }
    return (read_counter--)%2 ? sz : 0;
}

static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset) {
    char deb_buffer[256];
    size_t sz = min(len, sizeof(deb_buffer)-1);
    printk(KERN_INFO PREF "dev_write! %ld %ld\n", len, sz);
    if (copy_from_user(deb_buffer, buffer, sz)) {
	printk(KERN_INFO PREF "device writing error\n");
	return -EFAULT;
    }
    deb_buffer[sz] = 0;
    printk(KERN_INFO PREF "Wow! I get a message: %s\n", deb_buffer);
    return sz;
}

static struct file_operations fops =
{
    .open = dev_open,
    .read = dev_read,
    .write = dev_write,
    .release = dev_release,
};


// ---

static int __init test_km_init(void) {
    printk(KERN_INFO PREF "Hello, kernel!\n");

    // char device
    majorNumber = register_chrdev(0, DEVNAME, &fops);
    if (majorNumber < 0) {
	printk(KERN_ERR PREF "failed to register a major number: %d\n", majorNumber);
	return -EFAULT;
    }
    printk(KERN_INFO PREF "registered correctly with major number %d\n", majorNumber);
    mydevCharClass = class_create(THIS_MODULE, "testchardev");
    if (IS_ERR(mydevCharClass)) {
	unregister_chrdev(majorNumber, DEVNAME);
	printk(KERN_ERR PREF "Failed to register device class\n");
	return -EFAULT;
    }
    printk(KERN_INFO PREF "device class registered correctly\n");
    mydevCharDevice = device_create(mydevCharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVNAME);
    if (IS_ERR(mydevCharDevice)) {
	class_destroy(mydevCharClass);
	unregister_chrdev(majorNumber, DEVNAME);
	printk(KERN_ERR PREF "Failed to create the device\n");
	return -EFAULT;
    }

    return 0;
}

static void __exit test_km_exit(void) {
    device_destroy(mydevCharClass, MKDEV(majorNumber, 0));
    class_unregister(mydevCharClass);
    class_destroy(mydevCharClass);
    unregister_chrdev(majorNumber, DEVNAME);

    printk(KERN_INFO PREF "Goodbye, kernel!\n");
}

// ---

module_init(test_km_init);
module_exit(test_km_exit);
