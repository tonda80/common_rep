Входное тестовое задание для searchinform.ru

ЗАДАЧА ТЕСТОВОГО ЗАДАНИЯ:
Тестовое задание направлено на оценку уровня квалификации и  возможности реализации выделенных проектов и оценки сроков реализации задания.

ПОСТАНОВКА ТЕСТОВОГО ЗАДАНИЯ:
Необходимо реализовать:
  (а)   загружаемый   модуль   ядра,   который   хранит   в   оперативной   памяти   очередь произвольных  сообщений  до  64К  каждое.  Размер  очереди  ограничен  1024  элементами.
		Интерфейс: push_back() и pop_front().
  (б)  консольное  приложение,  которое  взаимодействует  с  модулем  ядра  и  помещает сообщения в конец очереди (а).
  (в) user-mode демон, который вычитывает сообщения из очереди (а) и помещает в файловое хранилище. Предусмотреть корректные запуск и остановку.
  (г)  для  модуля  ядра:  синхронное  сохранение/подкачка  некоторой  частиочереди  (а)  в файловую систему,  если требуется.
  (д)   для   модуля   ядра:   системный   поток,   выполняющий   сохранение/подкачку   (г) асинхронно.


Таймшит
--
26.09 - 3 часа

Заметки
--
sudo apt install build-essential linux-headers-`uname -r`
Чтение из ядра пользовательской памяти (например в dev_write) чревато (повесил систему как в досе)



Работа с модулем
--
Простейший скопированный Makefile, чтоб собрать другой модуль, править obj-m
-
sudo insmod <name>.ko     старт
dmesg                           инфа
lsmod | grep <name>       ещё инфа
sudo rmmod <name>         стоп



Доки
--
Символьное устройство
http://derekmolloy.ie/writing-a-linux-kernel-module-part-2-a-character-device/
