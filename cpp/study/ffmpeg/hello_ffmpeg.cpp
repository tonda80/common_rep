// копипаста туториала http://dranger.com/ffmpeg/tutorial01.html  с правками
//
// https://www.ffmpeg.org/documentation.html  в 19 минте 2.8.15, судя по 'ffmpeg -version'

#include <fmt_wrapper.h>
#include <iostream>

// обязательно указание си линковки
extern "C"{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
}


std::function<void(const std::string&)> Fmt::error_handler = [](const std::string& msg){
    std::cerr << "Format error: " << msg;
};


void fatal_error(const std::string& what) {
	std::cerr << "\n" << what << "\nExiting..\n";
	std::exit(-1);
}


int main(int argc, char *argv[]) {
	if (argc < 2) {
		fatal_error("No input file");
	}
	const char* inFilePath = argv[1];

	av_register_all();

	// Open video file
	AVFormatContext *pFormatCtx = NULL;
	if(avformat_open_input(&pFormatCtx, inFilePath, NULL, NULL) != 0) {
		fatal_error(Fmt::format("Couldn't open file {}", inFilePath));
	}

	// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0) {
		fatal_error("Couldn't find stream information");
	}

	// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, inFilePath, 0);

	// Find the first video stream
	int videoStream = -1;
	for(size_t i=0; i<pFormatCtx->nb_streams; i++) {
		if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
			videoStream = i;
			break;
		}
	}
	if (videoStream == -1) {
		fatal_error("Didn't find a video stream");
	}

	// Get a pointer to the codec context for the video stream
	AVCodecContext *pCodecCtxOrig = pFormatCtx->streams[videoStream]->codec;

	// Find the decoder for the video stream
	AVCodec *pCodec = avcodec_find_decoder(pCodecCtxOrig->codec_id);
	if(pCodec == NULL) {
		fatal_error("Unsupported codec or codec not found");
	}
	Fmt::println("Found codec {}", pCodecCtxOrig->codec_id);

	// Copy context (почему то нельзя пользоваться контекстом полученным ранее)
	AVCodecContext *pCodecCtx = avcodec_alloc_context3(pCodec);
	if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0) {
		fatal_error("Couldn't copy codec context");
	}

	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
		fatal_error("Could not open codec");
	}


	// Allocate video frame
	AVFrame *pFrame = av_frame_alloc();
	// Allocate an AVFrame для преобразования в какой-то RGB формат
	AVFrame* pFrameRGB = av_frame_alloc();
	if (!pFrame || !pFrameRGB) {
		fatal_error(Fmt::format("Couldn't allocate frames {} {}", !pFrame, !pFrameRGB));
	}


	// PIX_FMT_RGB24 вместо AV_PIX_FMT_RGB24 на старых версиях
	// Determine required buffer size and allocate buffer
	int numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
	uint8_t *buffer = static_cast<uint8_t*>(av_malloc(numBytes*sizeof(uint8_t)));
	// Assign appropriate parts of buffer to image planes in pFrameRGB. Note that pFrameRGB is an AVFrame, but AVFrame is a superset of AVPicture
	avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);


	return 0;
}
