#include "../../common.h"
#include "app_src.h"


Maker::Maker() {
	p("Maker created");
}

void Maker::action1() {
	p("Maker::action1");
}

int Maker::action2(int arg1, int arg2) {
	p("Maker::action2");
	return arg1 + arg2;
}
