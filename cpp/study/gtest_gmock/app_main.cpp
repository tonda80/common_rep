#include "../../common.h"
#include "app_src.h"


int main(int argc, const char** argv) {
	p("app started");

	int i = 7;
	pp("mul3 result", mul3(7));

	Maker maker;
	maker.action1();
	maker.action2(1, 2);

	p("app finished");
	return 0;
}
