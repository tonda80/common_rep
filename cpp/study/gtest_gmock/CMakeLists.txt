cmake_minimum_required(VERSION 3.16)

project(exp_gtest)
set(CMAKE_CXX_STANDARD 17)

# app
# ----

set(APP_NAME "app")
set(SRC app_src.cpp app_main.cpp)
add_executable (${APP_NAME} ${SRC})

# тесты без авто запуска
# ----
set(TESTS test_src.cpp)

add_executable (${APP_NAME}_test ${TESTS})

# добавим gtest "руками"
set(GTEST_REPO "/home/berezin/strange_code/googletest")
set(GTEST_INCLUDE_DIR "${GTEST_REPO}/googletest/include")
set(GMOCK_INCLUDE_DIR "${GTEST_REPO}/googlemock/include")
set(GTEST_LIB_DIR     "${GTEST_REPO}/build/lib")

# find_package(Gtest REQUIRED)		# TODO научиться
find_package(Threads REQUIRED)		# без pthread ошибка линковки

target_include_directories(${APP_NAME}_test PRIVATE  ${GTEST_INCLUDE_DIR})
target_link_libraries(${APP_NAME}_test PRIVATE  Threads::Threads  ${GTEST_LIB_DIR}/libgtest.a ${GTEST_LIB_DIR}/libgtest_main.a)
