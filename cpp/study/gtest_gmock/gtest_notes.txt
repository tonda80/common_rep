https://github.com/google/googletest
в 24 году предлагали доку смотреть тут https://google.github.io/googletest/ (rather than directly in the repository)

Сборка тривиальна: clone, cmake (для корня!), make. По умолчанию gmock собирается, можно отключить опцией -DBUILD_GMOCK=OFF.
Получаем в lib 4 статических либы для мок и тест (+ main)

Тесты оформляются макросами. 1-й параметр набор тестов, 2-й имя теста - их комбинация должна быть уникальной.
TEST(TestSuiteName, TestName) {}

Внутри тестов используются проверочные макросы ASSERT_* и EXPECT_* (например ASSERT_EQ, EXPECT_TRUE).
После неудачи assert-а тест считается непройденным и выполнение теста останавливается.
Неудача expect-а  тоже делает тест непройденным, но продолжение остальных утверждений теста продолжится.
assert - фатальные ошибки, ими имеет смысл помечать места, после которых дальнейшее выполнение теста бессмысленно.
expect - нефатальная ошибка, так можно отловить больше проблем за 1 тестовый проход.

ADD_FAILURE() FAIL() SUCCEED()  -  failure - нефатальная ошибка, fail - фатальная. не могу придумать, как использовать SUCCEED)
EXPECT_FALSE(bool_arg) EXPECT_TRUE(bool_arg)
  и есть сравнения LT/LE/GT/GE, но имхо смысла в них нет
EXPECT_EQ(arg1, arg2) EXPECT_NE(arg1, arg2)
EXPECT_STREQ(cstr1, cstr2) EXPECT_STRNE(cstr1, cstr2)
  и есть STRCASEEQ/STRCASENE
Макросы для сравнения чисел с плавающей точкой
EXPECT_DOUBLE_EQ(v1,v2) EXPECT_FLOAT_EQ(v1,v2) EXPECT_NEAR(v1, v2, err)
Полный список был тут https://google.github.io/googletest/reference/assertions.html
