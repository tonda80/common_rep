#include <gtest/gtest.h>

#include "../../common.h"
#include "app_src.h"


bool g_test_cond = true;


TEST(Base, BasicAssertions) {
	if (0) {
		ASSERT_TRUE(g_test_cond);
		p("[NOTE] After unsuccessful ASSERT_ the test execution will be STOPPED");
	} else {
		EXPECT_TRUE(g_test_cond);
		p("[NOTE] After unsuccessful EXPECT_ the test execution will be continued");
	}

	EXPECT_STREQ("he", "he");
	EXPECT_STRNE("hello", "world");
	EXPECT_EQ(7 * 6, 42);
}

TEST(Base2, BasicAssertions) {
	if (1) {
		ASSERT_TRUE(g_test_cond);
		p("[NOTE] After unsuccessful ASSERT_ the test execution will be stopped");
	} else {
		EXPECT_TRUE(g_test_cond);
		p("[NOTE] After unsuccessful EXPECT_ the test execution will be continued");
	}

	EXPECT_NE(2*2, 3);
}
