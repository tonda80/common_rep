#include <memory>


template<typename T>
T mul3(const T& m1) {
	return m1*3;
}


class IMaker {

};

//class MakerImpl;

class Maker : public IMaker {
	//std::unique_ptr<MakerImpl> impl_;

public:
	Maker();
	~Maker() = default;

	void action1();
	int action2(int arg1, int arg2);
};
