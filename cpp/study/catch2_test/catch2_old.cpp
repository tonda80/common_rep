// тут старый пример для старой версии catch2 которая хоть и хедер онли, но требует библиотку для main
// git clone <catch2> && cd Catch2 && git checkout  v2.x
// mkdir Build && cd Build && cmake -DCATCH_BUILD_STATIC_LIBRARY=ON .. && make -j12
// export ch2=<>
// g++ -I $ch2/include  catch2_old.cpp  $ch2/Build/libCatch2WithMain.a

#include <catch.hpp>


unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
	INFO("Test case start INFO");

    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );

    int a = 1, b = 2, c = 3;
	CAPTURE( a, b, c, a + b, c > b, a == 1);

	//REQUIRE( Factorial(0) == 1 );
}

TEST_CASE( "Factorials are computed again", "[factorial2]" ) {
	WARN("Test case start");

	SECTION("sect1") {
		WARN("sect1 warn");
		REQUIRE( Factorial(10) == 3628800 );
	}
	SECTION("sect2") {
		WARN("sect2 warn");
		REQUIRE( Factorial(10) == 3628800 );
	}
	SECTION("sect3") {
		WARN("sect3 warn");
		REQUIRE( Factorial(10) == 3628800 );
	}
	SECTION("sect4") {
		WARN("sect4 warn");
		REQUIRE( Factorial(10) == 3628800 );
	}

	WARN("Test case end");
}


class UniqueTestsFixture {
private:
	static int uniqueID;
protected:
	int locId = 0;
public:
	UniqueTestsFixture() {WARN("UniqueTestsFixture constructor");}
	~UniqueTestsFixture() {WARN("UniqueTestsFixture destructor");}
protected:
	int getID() {
		return ++uniqueID;
	}
};

int UniqueTestsFixture::uniqueID = 0;

TEST_CASE_METHOD(UniqueTestsFixture, "Create1", "[create]") {
	REQUIRE(getID() == 1);
	REQUIRE(++locId == 1);
}

TEST_CASE_METHOD(UniqueTestsFixture, "Create2", "[create]") {
	REQUIRE(getID() == 2);
	REQUIRE(++locId == 1);
}
