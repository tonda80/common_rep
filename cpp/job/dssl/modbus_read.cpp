// Минимальная чистенькая реализация чтения термодатчика

// cd path/to/libmodbus && ./autogen.sh && ./configure --enable-static && make
// export MB=/home/berezin/strange_code/libmodbus
// g++ -I $MB/src -I $MB -o modbus_read.out modbus_read.cpp $MB/src/.libs/libmodbus.a -pthread  && sudo  ./modbus_read.out


#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <thread>

#include <modbus.h>
#include <modbus-private.h>		//  если нужны debug сообщения


const char* DEV_PATH = "/dev/ttyACM0";
const int SLAVE = 4;
const int RESPONSE_TIMEOUT_SEC = 0;
const int RESPONSE_TIMEOUT_USEC = 200000;


template <typename Arr>
void print_buf(const Arr& buf) {
    std::cout << "\nRead result\n-----------\n";
    std::cout << std::setw(4) << std::setfill('0') << std::hex;
    for (int i = 0; i < buf.size(); ) {
        auto row_size = std::min(32, static_cast<int>(buf.size()) - i);
        for (int j = i; j < i + row_size; ++j) {
            std::cout << std::setw(4) << buf[j] << " ";
        }
        i += row_size;
        std::cout << "\n";
    }
    std::cout << std::setw(0) << std::setfill(' ') << std::dec;
}

template <typename Arr>
void print_buf_short(const Arr& buf) {
    std::cout << "\nRead short result\n-----------------\n";
    std::cout << std::setw(4) << std::setfill('0') << std::hex;
    for (int i = 0; i < buf.size(); i += 32) {
        std::cout << std::setw(4) << buf[i] << " ";
    }
    std::cout << "\n" << std::setw(0) << std::setfill(' ') << std::dec;
}

void read_temperatures_thread(modbus_t* ctx, bool* work) {
    const int TEMPER_START_REG = 0x0;
    const int TEMPER_REGS_Q = 1024;
    const int READ_SIZE = 64;

    const int OP_READ_DELAY_MSEC = 10;
    const int READ_ALL_DELAY_MSEC = 2000;

    std::array<uint16_t, TEMPER_REGS_Q> buf;
    while (*work) {
        int shift = 0;
        auto read_loop_start = std::chrono::system_clock::now();

        while (shift < TEMPER_REGS_Q) {
            int n_rd = std::min(READ_SIZE, TEMPER_REGS_Q - shift);
            if (modbus_read_registers(ctx, TEMPER_START_REG + shift, n_rd, buf.data() + shift) == -1) {
                std::cout << "modbus_read_registers failed:" << modbus_strerror(errno) << std::endl;
            } else {
                shift += n_rd;
            }
            std::this_thread::sleep_until(std::chrono::system_clock::now() + std::chrono::milliseconds(OP_READ_DELAY_MSEC));
        }

        print_buf_short(buf);

        // read all timeout
        std::this_thread::sleep_until(read_loop_start + std::chrono::milliseconds(READ_ALL_DELAY_MSEC));
    }
}


int main() {
    int res = -1;

    modbus_t* ctx = modbus_new_rtu(DEV_PATH, 115200, 'N', 8, 1);
    if (ctx == NULL) {
        std::cout << "Unable to create the libmodbus context\n";
        return -1;
    }
    ctx->debug = 1;

    if (modbus_connect(ctx) == -1) {
        std::cout << "modbus_connect failed:" << modbus_strerror(errno) << std::endl;
        modbus_free(ctx);
        return -1;
    }

    if (modbus_set_response_timeout(ctx, RESPONSE_TIMEOUT_SEC, RESPONSE_TIMEOUT_USEC) == -1) {
        std::cout << "modbus_set_response_timeout failed:" << modbus_strerror(errno) << std::endl;
        modbus_free(ctx);
        return -1;
    }

    if (modbus_set_slave(ctx, SLAVE) == -1) {
        std::cout << "modbus_set_slave failed:" << modbus_strerror(errno) << std::endl;
        modbus_free(ctx);
        return -1;
    }

    std::cout << "\n-- press enter to exit --\n\n";

    bool work = true;
    std::thread read_thread(read_temperatures_thread, ctx, &work);

    std::cin.get();
    work = false;
    std::cout << "bye.." << std::endl;
    read_thread.join();

    return 0;
}

