#include "tvd_modbus_connection.h"

// SOL-23 SOL-24
#if (!defined(WIN32) && !defined(ARM_PLATFORM))

#include <errno.h>
#include <logger.h>
#include <modbus/modbus.h>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <future>
#include <map>
#include <mutex>
#include <thread>
#include <utility>

const int RESPONSE_TIMEOUT_SEC = 1;
const int MODBUS_OPERATION_DELAY_MSEC = 10;
const int MODBUS_OPERATION_MAX_TRY = 2;
const int MODBUS_READ_REG_MAX_SIZE = 64;

const int TEMPERATURE_START_REGISTER = 0x0;
const int TEMPERATURE_REGISTERS_QUANTITY = 1024;
const int DEVICE_TYPE_REGISTER = 0x40a;
const int DEVICE_TYPE_TVD450 = 0x44;

class SerialModbusTask {
  static std::atomic<int> task_counter_;

 protected:
  SerialModbusTask(int period_msec, int slave, int addr)
      : period_msec_(period_msec),
        slave_(slave),
        addr_(addr) {
  }
  const int period_msec_;
  const int slave_;
  const int addr_;
  int id_ = ++task_counter_;
  bool fail_ = false;
  std::atomic<bool> cancelled_{false};

 public:
  virtual ~SerialModbusTask() = default;
  int period_msec() const {
    return period_msec_;
  }
  int id() const {
    return id_;
  }
  bool is_cancelled() const {
    return cancelled_;
  }
  void cancel() {
    cancelled_ = true;
  }
  virtual void step(modbus_t* ctx) = 0;
  virtual bool is_finished() = 0;
  virtual void reinit() = 0;
};

std::atomic<int> SerialModbusTask::task_counter_{0};

template <typename Clb>
class SerialModbusReadTask : public SerialModbusTask {
 public:
  SerialModbusReadTask(int period_msec, int slave, int addr, int nb, Clb clb)
      : SerialModbusTask(period_msec, slave, addr),
        clb_(clb),
        result_(nb),
        cur_addr_(addr) {
  }

  void step(modbus_t* ctx) override {
    assert(!fail_);

    int shift = cur_addr_ - addr_;
    assert(shift < result_.size() && shift >= 0);
    int n_rd = std::min(MODBUS_READ_REG_MAX_SIZE, static_cast<int>(result_.size()) - shift);

    if (modbus_set_slave(ctx, slave_) == -1) {
      Log::error("SerialModbus: set slave (read) failed: %s", modbus_strerror(errno));
      fail_ = true;
      return;
    }

    int n_try = -1;
    while (++n_try < MODBUS_OPERATION_MAX_TRY) {
      if (modbus_read_registers(ctx, cur_addr_, n_rd, result_.data() + shift) != -1) {
        cur_addr_ += n_rd;
        return;
      }
      Log::warning("SerialModbus: Modbus read error: %s", modbus_strerror(errno));
    }
    Log::warning("SerialModbus: Modbus read task failed");
    fail_ = true;
  }

  bool is_finished() override {
    int shift = cur_addr_ - addr_;
    if (fail_ || shift == result_.size()) {
      notify<Clb>();
      return true;
    }

    assert(shift < result_.size());
    return false;
  }

  void reinit() override {
    cur_addr_ = addr_;
    fail_ = false;
  }

 private:
  Clb clb_;
  std::vector<uint16_t> result_;
  int cur_addr_;

  template <typename T>
  decltype(std::declval<T>()(true, 1), void()) notify() {
    clb_(!fail_, result_[0]);
  }

  template <typename T>
  decltype(std::declval<T>()(true, {1, 1}), void()) notify() {
    clb_(!fail_, std::move(result_));
  }
};

class SerialModbusWriteTask : public SerialModbusTask {
 public:
  SerialModbusWriteTask(int slave, int addr, uint16_t value, ModbusWriteCallback clb)
      : SerialModbusTask(0, slave, addr),
        clb_(clb),
        value_(value) {
  }

  void step(modbus_t* ctx) override {
    assert(!fail_);

    if (modbus_set_slave(ctx, slave_) == -1) {
      Log::error("SerialModbus: set slave (write) failed: %s", modbus_strerror(errno));
      fail_ = true;
      return;
    }

    int n_try = -1;
    while (++n_try < MODBUS_OPERATION_MAX_TRY) {
      if (modbus_write_register(ctx, addr_, value_) != -1) {
        return;
      }
      Log::warning("SerialModbus: Modbus read error: %s", modbus_strerror(errno));
    }
    Log::warning("SerialModbus: Modbus read task failed");
    fail_ = true;
  }

  bool is_finished() override {
    clb_(!fail_);
    return true;
  }

  void reinit() override {
    fail_ = false;
  }

 private:
  ModbusWriteCallback clb_;
  uint16_t value_;
};

class SerialModbusImpl {
 public:
  SerialModbusImpl() = default;

  bool open(const char* device, int baud, char parity, int data_bit, int stop_bit) {
    ctx_ = modbus_new_rtu(device, baud, parity, data_bit, stop_bit);
    if (ctx_ == NULL) {
      Log::error("SerialModbus: Unable to create the modbus context: %s", modbus_strerror(errno));
      return false;
    }

    if (modbus_connect(ctx_) == -1) {
      Log::error("SerialModbus: modbus connect failed: %s", modbus_strerror(errno));
      modbus_free(ctx_);
      ctx_ = nullptr;
      return false;
    }

    if (modbus_set_response_timeout(ctx_, RESPONSE_TIMEOUT_SEC, 0) == -1) {
      Log::error("SerialModbus: modbus_ set_timeout failed: %s", modbus_strerror(errno));
    }

    oper_thread_ = std::thread(&SerialModbusImpl::modbus_operation_thread_func, this);
    return true;
  }

  ~SerialModbusImpl() {
    oper_flag_ = false;
    if (oper_thread_.joinable()) {
      oper_thread_.join();
    }

    if (ctx_) {
      modbus_close(ctx_);
      modbus_free(ctx_);
    }
  }

  void add_task(std::shared_ptr<SerialModbusTask> task_ptr) {
    std::lock_guard<std::mutex> guard(task_queue_mtx_);
    task_queue_.emplace(0, task_ptr);
  }

  void cancel_task(int task_id) {
    std::lock_guard<std::mutex> guard(task_queue_mtx_);
    for (auto it = task_queue_.begin(); it != task_queue_.end(); ++it) {
      if (it->second->id() == task_id) {
        it->second->cancel();
      }
    }
  }

 private:
  modbus_t* ctx_ = nullptr;
  std::thread oper_thread_;
  bool oper_flag_ = true;

  std::multimap<uint64_t, std::shared_ptr<SerialModbusTask>> task_queue_;
  std::mutex task_queue_mtx_;

  uint64_t curr_timestamp() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())
        .count();
  }

  void modbus_operation_thread_func() {
    while (oper_flag_) {
      auto it = task_queue_.end();
      {
        std::lock_guard<std::mutex> guard(task_queue_mtx_);
        it = task_queue_.begin();

        if (it != task_queue_.end() && it->second->is_cancelled()) {
          task_queue_.erase(it);
          continue;
        }
      }

      const auto curr_ts = curr_timestamp();
      if (it != task_queue_.end() && (it->first < curr_ts)) {
        std::shared_ptr<SerialModbusTask> task = it->second;
        task->step(ctx_);
        if (task->is_cancelled() || task->is_finished()) {
          std::lock_guard<std::mutex> guard(task_queue_mtx_);
          task_queue_.erase(it);

          if (!task->is_cancelled() && task->period_msec() > 0) {
            task->reinit();
            task_queue_.emplace(curr_ts + task->period_msec(), task);
          }
        }
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(MODBUS_OPERATION_DELAY_MSEC));
    }
  }
};

SerialModbus::SerialModbus() {
}

SerialModbus::~SerialModbus() {
}

bool SerialModbus::reopen(const char* device, int baud, char parity, int data_bit, int stop_bit) {
  impl_.reset(new SerialModbusImpl());
  return impl_->open(device, baud, parity, data_bit, stop_bit);
}

int SerialModbus::read_registers(int slave, int addr, int nb, ModbusReadRegsCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusReadTask<decltype(clb)>>(0, slave, addr, nb, clb);
  impl_->add_task(task_ptr);
  return task_ptr->id();
}

int SerialModbus::read_registers_periodically(
    int period_msec, int slave, int addr, int nb, ModbusReadRegsCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusReadTask<decltype(clb)>>(period_msec, slave, addr, nb, clb);
  impl_->add_task(task_ptr);
  return task_ptr->id();
}

int SerialModbus::read_register(int slave, int addr, ModbusReadCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusReadTask<decltype(clb)>>(0, slave, addr, 1, clb);
  impl_->add_task(task_ptr);
  return task_ptr->id();
}

int SerialModbus::write_register(int slave, int addr, uint16_t value, ModbusWriteCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusWriteTask>(slave, addr, value, clb);
  impl_->add_task(task_ptr);
  return task_ptr->id();
}

void SerialModbus::cancel_task(int id) {
  impl_->cancel_task(id);
}

ThermalModuleTvd450::ThermalModuleTvd450(SerialModbus& modbus, int dev_id)
    : modbus_(modbus),
      slave_(dev_id) {
}

ThermalModuleTvd450::~ThermalModuleTvd450() {
  stop_read_temperatures();
}

bool ThermalModuleTvd450::check_state() {
  assert(slave_ >= 0);
  auto state = std::make_shared<std::promise<bool>>();
  auto state_future = state->get_future();
  modbus_.read_register(slave_, DEVICE_TYPE_REGISTER, [state](bool ok, uint16_t reg) {
    state->set_value(ok && (reg == DEVICE_TYPE_TVD450));
  });
  auto wait_res = state_future.wait_for(std::chrono::seconds(1));
  return (wait_res == std::future_status::ready) && state_future.get();
}

void ThermalModuleTvd450::start_read_temperatures(int period_ms, ModbusReadRegsCallback clb) {
  assert(slave_ >= 0);
  read_temperatures_task_ = modbus_.read_registers_periodically(
      period_ms, slave_, TEMPERATURE_START_REGISTER, TEMPERATURE_REGISTERS_QUANTITY,
      [this, clb](bool ok, std::vector<uint16_t>&& rr) {
        clb(ok, std::move(rr));
      });
}

void ThermalModuleTvd450::stop_read_temperatures() {
  if (read_temperatures_task_ > 0) {
    modbus_.cancel_task(read_temperatures_task_);
  }
}

#else  // !defined(WIN32) && !defined(ARM_PLATFORM)

class SerialModbusImpl {};

SerialModbus::SerialModbus() {}
SerialModbus::~SerialModbus() {}
bool SerialModbus::reopen(const char* device, int baud, char parity, int data_bit, int stop_bit) {
  return true;
}
int SerialModbus::read_registers(int slave, int addr, int nb, ModbusReadRegsCallback clb) {
  return 0;
}
int SerialModbus::read_registers_periodically(int period_msec, int slave, int addr, int nb, ModbusReadRegsCallback clb) {
  return 0;
}
int SerialModbus::read_register(int slave, int addr, ModbusReadCallback clb) {
  return 0;
}
int SerialModbus::write_register(int slave, int addr, uint16_t value, ModbusWriteCallback clb) {
  return 0;
}
void SerialModbus::cancel_task(int id) {}

ThermalModuleTvd450::ThermalModuleTvd450(SerialModbus& modbus, int dev_id) : modbus_(modbus) {}
ThermalModuleTvd450::~ThermalModuleTvd450() {}
bool ThermalModuleTvd450::check_state() {return true;}
void ThermalModuleTvd450::start_read_temperatures(int period_ms, ModbusReadRegsCallback clb) {
  std::thread([period_ms, clb](){
    while (1) {
      std::this_thread::sleep_for(std::chrono::milliseconds(period_ms));
      clb(true, {});
    }
  }).detach();
}
void ThermalModuleTvd450::stop_read_temperatures() {}

#endif  // !defined(WIN32) && !defined(ARM_PLATFORM)
