#include <iostream>
#include <chrono>
#include <thread>
#include <stdlib.h> // rand

#include "tvd_modbus_connection.h"

int rand(int min, int max) {
  return rand()%(max - min) + min;
}

int main() {
    SerialModbus ser_mod;
    if (!ser_mod.reopen("/dev/ttyACM0", 115200, 'N', 8, 1)) {
        std::cout << "Serial device opening error!\n";
        return 1;
    }

    srand(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count());

    ser_mod.read_register(4, 0x40a, [](bool ok, uint16_t r) {
      std::cout << "read device id. ok: " << ok << " Value: ";
      if (ok) {
        std::cout << r << " ";
      }
      std::cout <<  std::endl;
    });

    ser_mod.read_registers(4, 0x406, 4, [](bool ok, std::vector<uint16_t>&& rr) {
      std::cout << "read modbus parameters. ok: " << ok << " Values: ";
      if (ok) {
        for (auto& r : rr) {
          std::cout << r << " ";
        }
      }
      std::cout <<  std::endl;
    });

    int zx = rand(1, 8);
    int zy = rand(1, 8);
    std::cout << "random zones: " << zx << " " << zy << std::endl;
    ser_mod.write_register(4, 0x410, zx, [](bool ok) {
      std::cout << "write zone X. ok: " << ok << std::endl;
    });
    ser_mod.write_register(4, 0x411, zy, [](bool ok) {
      std::cout << "write zone Y. ok: " << ok << std::endl;
    });
    ser_mod.read_registers(4, 0x410, 3, [](bool ok, std::vector<uint16_t>&& rr) {
      std::cout << "read zones. ok: " << ok << " Values: ";
      if (ok) {
        for (auto& r : rr) {
          std::cout << r << " ";
        }
      }
      std::cout <<  std::endl;
    });

    std::cout << "-- enter to run periodically reading --" << std::endl;
    std::cin.get();

    ThermalModuleTvd450 tvd(ser_mod, 4);
    if (!tvd.check_state()) {
      std::cerr << "tvd is NOT tvd450\n";
      return 1;
    }

    tvd.start_read_temperatures(2000, [](bool ok, std::vector<uint16_t>&& rr) {
      std::cout << "read temperatures. ok: " << ok << " Values: ";
      if (ok) {
        for (auto& r : rr) {
          std::cout << r << " ";
        }
      }
      std::cout <<  std::endl;
    });

    std::cout << "-- enter to stop reading --" << std::endl;
    std::cin.get();

    tvd.stop_read_temperatures();

    std::cout << "-- enter to exit --" << std::endl;
    std::cin.get();
    std::cout << "bye.." << std::endl;
	return 0;
}
