#pragma once

#include <functional>
#include <memory>
#include <thread>
#include <vector>

class SerialModbusImpl;

using ModbusReadRegsCallback = std::function<void(bool, std::vector<uint16_t>&&)>;
using ModbusReadCallback = std::function<void(bool, uint16_t)>;
using ModbusWriteCallback = std::function<void(bool)>;

class SerialModbus {
 public:
  SerialModbus();
  ~SerialModbus();

  bool reopen(const char* device, int baud, char parity, int data_bit, int stop_bit);

  int read_registers(int slave, int addr, int nb, ModbusReadRegsCallback clb);
  int read_registers_periodically(int period_msec, int slave, int addr, int nb, ModbusReadRegsCallback clb);
  int read_register(int slave, int addr, ModbusReadCallback clb);
  int write_register(int slave, int addr, uint16_t value, ModbusWriteCallback clb);

  void cancel_task(int id);

 private:
  std::unique_ptr<SerialModbusImpl> impl_;
};

class ThermalModuleTvd450 {
 public:
  ThermalModuleTvd450(SerialModbus& modbus, int dev_id = -1);
  ~ThermalModuleTvd450();

  void set_device_id(int dev_id) {
    slave_ = dev_id;
  }
  bool check_state();

  void start_read_temperatures(int period_ms, ModbusReadRegsCallback clb);
  void stop_read_temperatures();

 private:
  SerialModbus& modbus_;
  int slave_;
  int read_temperatures_task_ = -1;
};
