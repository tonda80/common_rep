#include "tvd_modbus_serial.h"

#include <modbus/modbus.h>

#include <atomic>
#include <thread>

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/make_unique.hpp>

#include "json_rwutils.h"
#include "logger.h"
#include "tvd_constants.h"

//  #define USE_MODBUS_WRITE
#if defined(USE_MODBUS_WRITE)
using ModbusWriteCallback = std::function<void(bool)>;
#endif

namespace bi = boost::interprocess;
namespace bpt = boost::posix_time;

const int RESPONSE_TIMEOUT_SEC = 0;
const int RESPONSE_TIMEOUT_USEC = 200000;
const int MODBUS_OPERATION_DELAY_MSEC = 10;
const int MODBUS_LOOP_DELAY_MSEC = 10;
const int MODBUS_OPERATION_MAX_TRY = 3;
const int MODBUS_READ_REG_MAX_SIZE = 64;

class SerialModbusTask {
  static std::atomic<int> task_counter_;

 protected:
  SerialModbusTask(int period_msec, int slave, int addr)
      : period_msec_(period_msec),
        slave_(slave),
        addr_(addr) {
  }
  const int period_msec_;
  const int slave_;
  const int addr_;
  int id_ = ++task_counter_;
  bool fail_ = false;
  std::atomic<bool> cancelled_{false};

 public:
  virtual ~SerialModbusTask() = default;
  int period_msec() const {
    return period_msec_;
  }
  int id() const {
    return id_;
  }
  bool is_cancelled() const {
    return cancelled_;
  }
  void cancel() {
    cancelled_ = true;
  }
  virtual void step(modbus_t* ctx) = 0;
  virtual bool is_finished() = 0;
  virtual void reinit() = 0;
};

std::atomic<int> SerialModbusTask::task_counter_{0};

template <typename Clb>
class SerialModbusReadTask : public SerialModbusTask {
 public:
  SerialModbusReadTask(int period_msec, int slave, int addr, int nb, Clb clb)
      : SerialModbusTask(period_msec, slave, addr),
        clb_(clb),
        result_size_(nb),
        result_(nb),
        cur_addr_(addr) {
  }

  void step(modbus_t* ctx) override {
    assert(!fail_);

    int shift = cur_addr_ - addr_;
    assert(shift < result_.size() && shift >= 0);
    int n_rd = std::min(MODBUS_READ_REG_MAX_SIZE, static_cast<int>(result_.size()) - shift);

    if (modbus_set_slave(ctx, slave_) == -1) {
      Log::error("SerialModbus: set slave (read) failed: %s", modbus_strerror(errno));
      fail_ = true;
      return;
    }

    int n_try = -1;
    while (++n_try < MODBUS_OPERATION_MAX_TRY) {
      if (modbus_read_registers(ctx, cur_addr_, n_rd, result_.data() + shift) != -1) {
        cur_addr_ += n_rd;
        return;
      }
      Log::warning("SerialModbus: Modbus read error: %s", modbus_strerror(errno));
    }
    Log::warning("SerialModbus: Modbus read task failed");
    fail_ = true;
  }

  bool is_finished() override {
    int shift = cur_addr_ - addr_;
    if (fail_ || shift == result_.size()) {
      notify<Clb>();
      return true;
    }

    assert(shift < result_.size());
    return false;
  }

  void reinit() override {
    result_ = decltype(result_)(result_size_);
    cur_addr_ = addr_;
    fail_ = false;
  }

 private:
  Clb clb_;
  int result_size_;
  std::vector<uint16_t> result_;
  int cur_addr_;

  template <typename T>
  decltype(std::declval<T>()(true, 1), void()) notify() {
    clb_(!fail_, result_[0]);
  }

  template <typename T>
  decltype(std::declval<T>()(true, {1, 1}), void()) notify() {
    clb_(!fail_, std::move(result_));
  }
};

#if defined(USE_MODBUS_WRITE)
class SerialModbusWriteTask : public SerialModbusTask {
 public:
  SerialModbusWriteTask(int slave, int addr, uint16_t value, ModbusWriteCallback clb)
      : SerialModbusTask(0, slave, addr),
        clb_(clb),
        value_(value) {
  }

  void step(modbus_t* ctx) override {
    assert(!fail_);

    if (modbus_set_slave(ctx, slave_) == -1) {
      Log::error("SerialModbus: set slave (write) failed: %s", modbus_strerror(errno));
      fail_ = true;
      return;
    }

    int n_try = -1;
    while (++n_try < MODBUS_OPERATION_MAX_TRY) {
      // TODO(a.berezin) add lock
      if (modbus_write_register(ctx, addr_, value_) != -1) {
        return;
      }
      Log::warning("SerialModbus: Modbus write error: %s", modbus_strerror(errno));
    }
    Log::warning("SerialModbus: Modbus write task failed");
    fail_ = true;
  }

  bool is_finished() override {
    clb_(!fail_);
    return true;
  }

  void reinit() override {
    fail_ = false;
  }

 private:
  ModbusWriteCallback clb_;
  uint16_t value_;
};
#endif

const int modbus_shmem_size = 4096;
const int modbus_shmem_critical_free_size = 256;

class SerialModbus : public IModbus {
  std::thread oper_thread_;
  bool oper_flag_ = true;
  int slave_ = -1;
  modbus_t* ctx_ = nullptr;

  std::mutex task_queue_mtx_;
  std::multimap<uint64_t, std::shared_ptr<SerialModbusTask>> task_queue_;

  std::unique_ptr<bi::named_mutex> p_ipc_mtx_;
  std::string ipc_mtx_name_;
  bi::managed_shared_memory shmem_segm_{bi::open_or_create, "modbus_shmem", modbus_shmem_size};
  std::atomic_uint_least64_t* create_timestamp_us_ = nullptr;

  void init_ipc_mtx(const std::string& dev_path) {
    const char* mtx_pref = "modbus_mutex";

    ipc_mtx_name_ = mtx_pref + dev_path;
    std::replace(ipc_mtx_name_.begin(), ipc_mtx_name_.end(), '/', '_');

    std::string timestamp_var_name = ipc_mtx_name_ + "_ts";
    create_timestamp_us_ = shmem_segm_.find_or_construct<std::atomic_uint_least64_t>(timestamp_var_name.c_str())(0);
    if (auto fm = shmem_segm_.get_free_memory() < modbus_shmem_critical_free_size) {
      Log::warning("SerialModbus: possible shared memory isssue free=%d\n", fm);
    }

    p_ipc_mtx_ = boost::make_unique<bi::named_mutex>(bi::open_or_create, ipc_mtx_name_.c_str());
  }

  std::uint64_t usec_timestamp(const bpt::ptime& time) const {
    bpt::time_duration duration = time - bpt::from_time_t(0);
    return duration.total_microseconds();
  }

  void lock_ipc_mtx() {
    while (1) {
      bpt::ptime lock_start = boost::get_system_time();
      bpt::ptime timeout = lock_start + bpt::milliseconds(Grabbers::IPC_MUTEX_TIMEOUT_MS);
      if (p_ipc_mtx_->timed_lock(timeout)) {
        return;
      }
      Log::info("SerialModbus: Can't lock %s \n", ipc_mtx_name_);

      auto create_mtx_name_maker = [this](int idx) {
        return "cr_" + ipc_mtx_name_ + "_" + std::to_string(idx);
      };
      const int n_create_try = 10;  // quantity of tries to lock create mutex
      const int create_mtx_timeout_ms = 10;  // after this time create mutex is considered as corrupted
      for (int i = 0; i < n_create_try; ++i) {
        std::string create_mtx_name = create_mtx_name_maker(i);
        bi::named_mutex create_mtx{bi::open_or_create, create_mtx_name.c_str()};
        timeout = boost::get_system_time() + bpt::milliseconds(create_mtx_timeout_ms);
        if (!create_mtx.timed_lock(timeout)) {
          if (i == n_create_try - 1) {
            Log::error("SerialModbus: Can't recreate %s \n", ipc_mtx_name_);
            throw std::runtime_error("failed to recreate modbus mutex " + ipc_mtx_name_);
          }
          continue;
        }
        std::lock_guard<bi::named_mutex> create_guard(create_mtx, std::adopt_lock);

        if (usec_timestamp(lock_start) > *create_timestamp_us_) {
          // we have old modbus mutex, we need to recreate it
          bi::named_mutex::remove(ipc_mtx_name_.c_str());
          p_ipc_mtx_ = boost::make_unique<bi::named_mutex>(bi::create_only, ipc_mtx_name_.c_str());
          *create_timestamp_us_ = usec_timestamp(boost::get_system_time());
          Log::info("SerialModbus: recreated %s \n", ipc_mtx_name_);
        } else {
          // we have modbus mutex already recreated by other grabber, we need reopen it
          p_ipc_mtx_ = boost::make_unique<bi::named_mutex>(bi::open_only, ipc_mtx_name_.c_str());
          Log::info("SerialModbus: reopened %s \n", ipc_mtx_name_);
        }

        // clean up helper mutexes
        for (int j = 0; j <= i; ++j) {
          std::string create_mtx_name = create_mtx_name_maker(j);
          bi::named_mutex::remove(create_mtx_name.c_str());
        }
        break;  // for i
      }
    }
  }

 public:
  explicit SerialModbus(const std::string& json_modbus_connection_params) {
    Json::Value data_json;
    std::string errs;
    auto log_and_throw = [](const std::string& error_description, int errnum) {
      const auto str_error = std::string(modbus_strerror(errnum));
      Log::error("SerialModbus: %s %s\n", error_description.c_str(), str_error.c_str());
      throw std::runtime_error(error_description + str_error);
    };

    if (!JsonRWUtils::json_parse(json_modbus_connection_params, data_json, false, errs)) {
      Log::warning("SerialModbus: failed to parse connection string: %s\n", errs);
      throw std::runtime_error("failed to parse connection string: " + errs);
    }
    auto path = data_json["path"].asString();
    auto baud = data_json["baud_rate"].asInt();
    auto iparity = data_json["parity"].asInt();
    auto parity = iparity == 0 ? 'N' : (iparity == 1 ? 'O' : 'E');
    auto data_bit = data_json["data_bits"].asInt();
    auto istop = data_json["stop_bits"].asInt();
    auto stop_bit = istop == 0 || istop == 1 ? 1 : 2;
    slave_ = data_json["sensor_number"].asInt();

    init_ipc_mtx(path);

    lock_ipc_mtx();
    std::lock_guard<bi::named_mutex> lock(*p_ipc_mtx_, std::adopt_lock);
    ctx_ = modbus_new_rtu(path.c_str(), baud, parity, data_bit, stop_bit);
    if (ctx_ == NULL) {
      log_and_throw("unable to create the modbus context: ", errno);
    }

    if (modbus_connect(ctx_) == -1) {
      modbus_free(ctx_);
      ctx_ = nullptr;
      log_and_throw("modbus connect failed: ", errno);
    }

    if (modbus_set_response_timeout(ctx_, RESPONSE_TIMEOUT_SEC, RESPONSE_TIMEOUT_USEC) == -1) {
      Log::error("SerialModbus: modbus_ set_timeout failed: %s\n", modbus_strerror(errno));
    }

    oper_thread_ = std::thread(&SerialModbus::modbus_operation_thread_func, this);
  }

  ~SerialModbus() override {
    oper_flag_ = false;
    if (oper_thread_.joinable()) {
      oper_thread_.join();
    }

    if (ctx_) {
      modbus_close(ctx_);
      modbus_free(ctx_);
    }
  }

  int read_register(int addr, ModbusReadCallback clb) override;
  int read_registers_periodically(int period_msec, int addr, int nb, ModbusReadRegsCallback clb) override;
  void cancel_task(int id) override;

#if defined(USE_MODBUS_WRITE)
  int write_register(int slave, int addr, uint16_t value, ModbusWriteCallback clb);
#endif

 private:
  void add_task(std::shared_ptr<SerialModbusTask> task_ptr) {
    std::lock_guard<std::mutex> guard(task_queue_mtx_);
    task_queue_.emplace(0, task_ptr);
  }

  void modbus_operation_thread_func() {
    while (oper_flag_) {
      auto it = task_queue_.end();
      {
        std::lock_guard<std::mutex> guard(task_queue_mtx_);
        it = task_queue_.begin();

        if (it != task_queue_.end() && it->second->is_cancelled()) {
          task_queue_.erase(it);
          continue;
        }
      }

      const auto curr_ts_msec = usec_timestamp(boost::get_system_time())/1000;
      if (it != task_queue_.end() && (it->first < curr_ts_msec)) {
        std::shared_ptr<SerialModbusTask> task = it->second;
        {
          lock_ipc_mtx();
          std::lock_guard<bi::named_mutex> lock(*p_ipc_mtx_, std::adopt_lock);
          while (!task->is_cancelled() && !task->is_finished()) {
            task->step(ctx_);
            std::this_thread::sleep_for(std::chrono::milliseconds(MODBUS_OPERATION_DELAY_MSEC));
          }
        }
        std::lock_guard<std::mutex> guard(task_queue_mtx_);
        if (!task->is_cancelled() && task->period_msec() > 0) {
          task->reinit();
          task_queue_.emplace(curr_ts_msec + task->period_msec(), task);
        }
        task_queue_.erase(it);
      } else {
        std::this_thread::sleep_for(std::chrono::milliseconds(MODBUS_LOOP_DELAY_MSEC));
      }
    }
  }
};

int SerialModbus::read_registers_periodically(int period_msec, int addr, int nb, ModbusReadRegsCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusReadTask<decltype(clb)>>(period_msec, slave_, addr, nb, clb);
  add_task(task_ptr);
  return task_ptr->id();
}

int SerialModbus::read_register(int addr, ModbusReadCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusReadTask<decltype(clb)>>(0, slave_, addr, 1, clb);
  add_task(task_ptr);
  return task_ptr->id();
}

#if defined(USE_MODBUS_WRITE)
int SerialModbus::write_register(int slave, int addr, uint16_t value, ModbusWriteCallback clb) {
  auto task_ptr = std::make_shared<SerialModbusWriteTask>(slave_, addr, value, clb);
  add_task(task_ptr);
  return task_ptr->id();
}
#endif

void SerialModbus::cancel_task(int id) {
  std::lock_guard<std::mutex> guard(task_queue_mtx_);
  for (auto it = task_queue_.begin(); it != task_queue_.end(); ++it) {
    if (it->second->id() == id) {
      it->second->cancel();
    }
  }
}

boost::shared_ptr<IModbus> ModbusFactory::create(const std::string& json_modbus_connection_params) {
  return boost::make_shared<SerialModbus>(json_modbus_connection_params);
}
