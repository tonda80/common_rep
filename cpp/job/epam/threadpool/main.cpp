
#define _CRT_RAND_S
#include <stdlib.h>     /* srand, rand */

#include <iostream>

#include "threadpool.h"

#if defined (_WIN32)
#include <Windows.h>
#define SLEEP(t) Sleep((t)*1000)
#define RAND rand_s
#else
#include <unistd.h>
#define SLEEP(t) sleep(t)
#define RAND rand_r
#endif  // defined (_WIN32)

#include <time.h> 
#include <string> 

using namespace std;

std::mutex print_lock;
auto main_id = std::this_thread::get_id();
template <typename T> void print(const T& obj, const string& pref = "") {
    auto id = std::this_thread::get_id();
    {
        std::unique_lock<std::mutex> lck(print_lock);
        std::cout << id << " " << pref << obj << std::endl;
    }
}

struct Rand
{
    Rand() { srand(time(NULL)); }
    int get(int m) { unsigned int i; RAND(&i); return i% m; }
};

struct C
{
    int i;
    C(int i_=0) : i(i_) {}
    //~C() { print("~C"); }
    //C(C&& c) : i(c.i) { print("Cmove"); }
    operator std::string() const { return std::to_string(i); }
};

Rand g_rand;

void handlerC(C&& c, int i)
{
    int sl = g_rand.get(4);
    print(c.i, "\thandler_" + std::to_string(i) + " begin (" + std::to_string(sl) + "):\t ");
    SLEEP(sl);
    print("", "\t  handler_" + std::to_string(i) + " end");
}

int main()
{
    const int N_QUEUE = 3;
    const int N_WORKER = 8;
    const int N_REPEAT = 8;

    print("", " - main_id");

    ThreadPool<C> thPool;
    
    std::shared_ptr<EventQueue<C>> qArr[N_QUEUE];
    for (int i = 0; i < N_QUEUE; ++i) {
        qArr[i] = make_shared<EventQueue<C>>(thPool, [i](C&& c) {handlerC(std::move(c), i); });
        thPool.add_queue(qArr[i]);
    }
    auto hpQueue = make_shared<EventQueue<C>>(thPool, [](C&& c) {handlerC(std::move(c), 1000); });
    thPool.set_high_priority_queue(hpQueue);
    
    thPool.start(N_WORKER);

    for (int i = 0; i < N_REPEAT; ++i) {
        for (int j = 0; j < N_QUEUE; ++j)
            qArr[j]->push(C(i));

        qArr[0]->push(C(i+100));
        hpQueue->push(i+1000);
        hpQueue->push(i + 1100);
        hpQueue->push(i + 1200);
    }
    
    //thPool.start(N_WORKER);
    
    print("Press enter to stop");
    cin.ignore();
    thPool.halt();

    return 0;
}
