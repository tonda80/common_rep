
TEST(ThreadpoolTest, test1)
{
    const int N_QUEUE = 8;
    const int N_WORKER = 4;
    auto DELAY = std::chrono::milliseconds(200);

    // -- init
    ThreadPool<std::string> thrPool;

    std::shared_ptr<EventQueue<std::string>> qArr[N_QUEUE];
    std::shared_ptr<EventQueue<std::string>> hpQ;

    std::list<std::string> results[N_QUEUE];
    std::list<std::string> hpResult;

    for (int i = 0; i < N_QUEUE; ++i) {
        qArr[i] = std::make_shared<EventQueue<std::string>>(thrPool, [i, &results](std::string&& ev) {results[i].push_back(std::move(ev)); });
        thrPool.add_queue(qArr[i]);
    }
    hpQ = std::make_shared<EventQueue<std::string>>(thrPool, [&hpResult](std::string&& ev) {hpResult.push_back(std::move(ev)); });
    thrPool.add_queue(hpQ);

    // -- job
    int cnt = 0;
    const int N1 = 5, N2 = 10;
    std::list<std::string> test_result;
    for (int i = 0; i < N2; ++i)
        test_result.push_back(std::to_string(i));

    for (; cnt < N1; ++cnt) {
        for (int i = 0; i < N_QUEUE; ++i) {
            qArr[i]->push(std::to_string(cnt));
        }
        hpQ->push(std::to_string(cnt));
    }

    thrPool.start(N_WORKER);

    for (; cnt < N2; ++cnt) {
        for (int i = 0; i < N_QUEUE; ++i) {
            qArr[i]->push(std::to_string(cnt));
        }
        hpQ->push(std::to_string(cnt));
    }

    std::this_thread::sleep_for(DELAY);

    // -- test
    for (int i = 0; i < N_QUEUE; ++i)
        ASSERT_EQ(results[i], test_result);

    ASSERT_EQ(hpResult, test_result);
}

TEST(ThreadpoolTest, test2)
{
    const int N_QUEUE = 8;
    const int N_WORKER = 4;
    auto DELAY = std::chrono::milliseconds(200);

    // -- init
    ThreadPool<std::string> thrPool;

    std::shared_ptr<EventQueue<std::string>> qArr[N_QUEUE];

    std::unordered_map<int, std::string> result;
    std::mutex mtx_result;
    int cnt[N_QUEUE]{};

    thrPool.start(N_WORKER);

    for (int i = 0; i < N_QUEUE; ++i) {
        qArr[i] = std::make_shared<EventQueue<std::string>>(thrPool, [i, &result, &cnt, &mtx_result](std::string&& ev) {std::lock_guard<std::mutex>lck(mtx_result); int n = 1000 * i + cnt[i]++;  result.insert(std::make_pair(n, ev)); });
        thrPool.add_queue(qArr[i]);
    }

    // -- job
    const int N1 = 10;
    for (int i = 0; i < N1; ++i)
        for (int j = 0; j < N_QUEUE; ++j)
            qArr[j]->push(std::to_string(i));

    std::this_thread::sleep_for(DELAY);

    // -- test
    std::unordered_map<int, std::string> test_result;
    for (int j = 0; j < N_QUEUE; ++j)
        for (int i = 0; i < N1; ++i) {
            int n = 1000 * j + i;
            test_result.insert(std::make_pair(n, std::to_string(i)));
        }

    ASSERT_EQ(result, test_result);
}

TEST(ThreadpoolTest, test3)
{
    const int N_QUEUE = 8;
    const int N_WORKER = 4;
    auto DELAY = std::chrono::milliseconds(200);

    // -- init
    std::unordered_set<string> result;
    std::mutex mtx_result;
    int cnt[N_QUEUE]{};

    PoolQueueMap<int, string> queueMap([&result, &mtx_result](string&& ev) {std::lock_guard<std::mutex>lck(mtx_result); result.insert(ev); });
    queueMap.start(N_WORKER);

    // -- job
    const int N1 = 10;

    for (int i = 0; i < N1; ++i)
        for (int j = 0; j < N_QUEUE; ++j)
            queueMap.push(j, to_string(1000*j+i));

    std::this_thread::sleep_for(DELAY);

    // -- test
    std::unordered_set<string> test_result;
    for (int j = 0; j < N_QUEUE; ++j)
        for (int i = 0; i < N1; ++i)
            test_result.insert(to_string(1000*j + i));

    ASSERT_EQ(result, test_result);
}

TEST(ThreadpoolTest, test4)
{
    const int N_QUEUE = 8;
    const int N_WORKER = 4;
    auto DELAY = std::chrono::milliseconds(200);

    // -- init
    std::mutex mtx_result;
    int result = 0;

    PoolQueueMap<int, string> queueMap([&result, &mtx_result](string&& ev) {std::lock_guard<std::mutex>lck(mtx_result); ++result; });
    queueMap.start(N_WORKER);

    std::this_thread::sleep_for(DELAY);

    const int N1 = 1000000;
    string s;
    volatile int wait_result;
    for (int i = 0; i < N1; ++i) {
        wait_result = result + 1;
        long long int cnt_wait = 500000000;
        queueMap.push(0, std::move(s));
        while (result != wait_result) {
            if (--cnt_wait < 0)
                ASSERT_EQ(result, -1);   // We must not be here; so just for output of result value
        }
    }
}
