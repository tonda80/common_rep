#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <deque>
#include <list>
#include <unordered_map>
#include <functional>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <thread>

#include <cassert>

template <typename EvT>
class ThreadPool;

template <typename EvT>
class EventQueue
{    
    friend class ThreadPool<EvT>;

public:
    EventQueue(const ThreadPool<EvT>& th_pool, std::function<void(EvT&&)> handler_)
        : external_notifier(th_pool.q_condition)
        , handler(handler_)
    {
        assert(external_notifier);
    }
    
    EventQueue(EventQueue&) = delete;
    EventQueue& operator = (EventQueue&) = delete;

    void push(EvT&& ev);
    
private:
    void process();
    bool isReadable();
    void int_to_ext();

    std::deque<EvT> out_queue;
    std::deque<EvT> in_queue;
    std::mutex in_mtx;
    std::mutex out_mtx;
    std::shared_ptr<std::condition_variable> external_notifier;
    std::function<void(EvT&&)> handler;
};

template <typename EvT>
class ThreadPool
{
    friend class EventQueue<EvT>;
public:
    ThreadPool()
        : stop(false)
        , isWorked(false)
        , q_condition(std::make_shared<std::condition_variable>())
    {};
    ~ThreadPool();

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(ThreadPool&) = delete;

    void add_queue(const std::shared_ptr<EventQueue<EvT>>& q);
    void set_high_priority_queue(const std::shared_ptr<EventQueue<EvT>>& q);

    bool start(size_t n_threads);
    void halt();

private:
    bool stop;
    bool isWorked;
    
    void worker_function();

    std::deque< std::thread > workers;

    using queue_container_t = std::list<std::shared_ptr<EventQueue<EvT>>>;
    queue_container_t main_queues;
    std::shared_ptr<EventQueue<EvT>> hp_queue;
    
    // synchronization
    std::mutex q_mutex;
    std::shared_ptr<std::condition_variable> q_condition;
    inline bool queue_choice(std::shared_ptr<EventQueue<EvT>> &q, typename queue_container_t::iterator& it);

};

template <typename KeyT, typename EvT>
class PoolQueueMap : public ThreadPool<EvT>
{
    std::unordered_map<KeyT, std::shared_ptr<EventQueue<EvT>>> queue_map;
    std::function<void(EvT&&)> handler;
public:
    PoolQueueMap(std::function<void(EvT&&)> handler_)
        : handler(handler_)
    {}

    void push(const KeyT& k, EvT&& ev) {
        std::shared_ptr<EventQueue<EvT>> ptrQueue;
        const auto it = queue_map.find(k);
        if (it == queue_map.end())
            ptrQueue = insert(k);
        else
            ptrQueue = it->second;

        if (ptrQueue)
            ptrQueue->push(std::move(ev));
    }

    std::shared_ptr<EventQueue<EvT>> insert(const KeyT& k) {
        auto res = queue_map.insert(std::pair<KeyT, std::shared_ptr<EventQueue<EvT>>>(k, std::make_shared<EventQueue<EvT>> (*this, handler)));
        if (res.second) {
            this->add_queue(res.first->second);
        }
        return res.first->second;
    }
};

#include "threadpool.hpp"

#endif /* THREADPOOL_H_ */
