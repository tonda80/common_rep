#include "threadpool.h"


template <typename EvT>
bool EventQueue<EvT>::isReadable()
{
    std::unique_lock<std::mutex> out_lock(out_mtx, std::try_to_lock);

    if (out_lock) {
        bool isEmpty = false;
        if (out_queue.empty()) {
            int_to_ext();
            isEmpty = out_queue.empty();
        }

        if (!isEmpty) {
            out_lock.release();     // out_mtx remains locked
            return true;
        }
    }

    return false;
}

template <typename EvT>
void EventQueue<EvT>::push(EvT&& ev)
{
    {
        std::unique_lock<std::mutex> out_lock(out_mtx, std::try_to_lock);
        if (out_lock) {
            int_to_ext();

            out_queue.push_back(std::move(ev));
        }
        else {
            std::unique_lock<std::mutex> in_lock(in_mtx);
            in_queue.push_back(std::move(ev));
        }
    }

    external_notifier->notify_one();
}

template <typename EvT>
void EventQueue<EvT>::int_to_ext()
{
    std::unique_lock<std::mutex> in_lock(in_mtx);

    if (!in_queue.empty()) {
        // out_mtx must be locked outside!

        for (auto& e : in_queue) {
            out_queue.push_back(std::move(e));
        }
    }
    in_queue.clear();
}

template <typename EvT>
void EventQueue<EvT>::process()
{
    std::unique_lock<std::mutex> out_lock(out_mtx, std::adopt_lock);    // locked into empty

    handler(std::move(out_queue.front()));
    out_queue.pop_front();
}

template <typename EvT>
void ThreadPool<EvT>::add_queue(const std::shared_ptr<EventQueue<EvT>>& q)
{
    if (q) {
        main_queues.push_back(q);
    }
}

template <typename EvT>
void ThreadPool<EvT>::set_high_priority_queue(const std::shared_ptr<EventQueue<EvT>>& q)
{
    std::unique_lock<std::mutex> lock(q_mutex);
    hp_queue = q;
}


template <typename EvT>
bool ThreadPool<EvT>::start(size_t n_threads)

{
    if (isWorked)
        return false;

    for (size_t i = 0; i < n_threads; ++i)
        workers.emplace_back(&ThreadPool<EvT>::worker_function, this);

    isWorked = true;
    return true;
}

template <typename EvT>
void ThreadPool<EvT>::halt()
{
    if (!isWorked)
        return;

    {
        std::unique_lock<std::mutex> lock(q_mutex);
        stop = true;
    }
    q_condition->notify_all();

    for (auto& w : workers)
        w.join();

    isWorked = false;
}

template <typename EvT>
ThreadPool<EvT>::~ThreadPool()
{
    halt();
}

template <typename EvT>
void ThreadPool<EvT>::worker_function()
{
    typename queue_container_t::iterator it_queues = main_queues.begin();

    while (1) {
        std::shared_ptr<EventQueue<EvT>> process_queue;

        if (!queue_choice(process_queue, it_queues)) {
            std::unique_lock<std::mutex> lock(q_mutex);
            if (stop)
                return;
            q_condition->wait(lock, [this, &process_queue, &it_queues] { return queue_choice(process_queue, it_queues); });
        }

        if (stop)
            return;

        assert(process_queue);
        process_queue->process();
    }
}

template <typename EvT>
inline bool ThreadPool<EvT>::queue_choice(std::shared_ptr<EventQueue<EvT>> &queue, typename queue_container_t::iterator& it_queues)
{
    if (stop)
        return true;

    auto beg_it_queues = it_queues;
    while (1) {    // loop for all queues from the last one
        if (hp_queue && hp_queue->isReadable()) {
            queue = hp_queue;
            return true;
        }

        if (it_queues == main_queues.end()) {
            beg_it_queues = it_queues = main_queues.begin();    // one-time initialization of iterator
            if (it_queues == main_queues.end())                 // main_queues is empty
                return false;
        }

        ++it_queues;
        if (it_queues == main_queues.end())
            it_queues = main_queues.begin();

        auto q = *it_queues;
        if (q->isReadable()) {   // all pointers in main_queues are not null
            queue = q;
            return true;
        }

        if (beg_it_queues == it_queues)
            break;
    }

    return false;
}
