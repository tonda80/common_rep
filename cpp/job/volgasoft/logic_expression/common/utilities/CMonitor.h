#ifndef CMONITOR_H
#define CMONITOR_H

#include <string>
#include <set>

class QString;

class CMonitor
{
public:
	// levels of messages
	enum {ERROR=5, WARNING=10, NOTICE=20, INFO=30, DEBUG=40, BLAB=50};

	CMonitor();
	~CMonitor();
	void openFile(const char* fileName);

public:
	int readSettings(const char* fileName);	// it reads monitor settings from file

	int operator()(unsigned int level, const char *messType, const char *fmt, ...);
	int Puts(unsigned int level, const char *messType, const char *fmt, ...);
	// dumps memory to active output devices
	int Dump(unsigned int level, const char *messType, const void *mem, unsigned int size, unsigned int flags = 0);
	int DumpText(unsigned int level, const char *messType, const char *mem, unsigned int size, unsigned int flags = 0);

private:
	int ld; //file descriptor
	unsigned int lv; //level
	bool enableMessages; //enables log

	std::set<std::string> messTypes;
	bool filterMessTypes;

	bool checkMessType(const char* messType);
	const char *levelString(int level);

	static const unsigned int MAXLOGSTR = 4096;
	char m_buffStr[MAXLOGSTR];

	inline bool checkOut(unsigned int level, const char *messType);
	inline int makePrefix(unsigned int level, const char *messType);

	std::string strip(std::string str);
	int PrintTimestamp(char * szString);

};

extern CMonitor mon;

#endif // CMONITOR_H
