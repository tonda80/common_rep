/*
 * strHelper.h
 *
 *  Created on: Mar 12, 2014
 *      Author: ant
 */

#ifndef STRHELPER_H_
#define STRHELPER_H_

#include <sstream>

using namespace std;

template<typename T>
bool stringTo(string st, T& var)
{
	istringstream buffer(st);
	T value;
	buffer >> value;

	if (!buffer.fail() && buffer.eof()) {
		var = value;
		return true;
	}

	return false;
}

template<typename T>
string toString(const T& var)
{
	stringstream ss;
	ss << var;

	return ss.str();
}

#endif /* STRHELPER_H_ */
