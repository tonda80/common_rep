/*
 * mutexLocker.h
 *
 *  Created on: Feb 14, 2014
 *      Author: ant
 */

#ifndef MUTEXWRAPPER_H_
#define MUTEXWRAPPER_H_

#include <pthread.h>
#include <stdexcept>

#include <tr1/memory>
#include <tr1/shared_ptr.h>

class Mutex
{
private:
	tr1::shared_ptr<pthread_mutex_t> _mutex;
	static void releaseMutex(pthread_mutex_t* m)
	{
		pthread_mutex_destroy(m);
		delete m;
	}
public:
	Mutex() : _mutex(new pthread_mutex_t, Mutex::releaseMutex)
	{
		if (pthread_mutex_init(_mutex.get(), NULL) != 0)
			throw std::runtime_error("pthread_mutex_init error");
	}
	void lock()
	{
		if (pthread_mutex_lock(_mutex.get()) != 0)
			throw std::runtime_error("pthread_mutex_lock error");
	}
	bool tryLock()
	{
		if (pthread_mutex_trylock(_mutex.get()) == 0)
			return true;
		return false;
	}
	void unlock()
	{
		if (pthread_mutex_unlock(_mutex.get()) != 0)
			throw std::runtime_error("pthread_mutex_unlock error");
	}
};

class MutexLocker
{
public:
	MutexLocker(const Mutex& m) : _mutex(m) {_mutex.lock();}
	~MutexLocker() {_mutex.unlock();}
private:
	Mutex _mutex;
};


#endif /* MUTEXWRAPPER_H_ */
