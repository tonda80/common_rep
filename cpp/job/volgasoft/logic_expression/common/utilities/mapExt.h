/*
 * mapExt.h
 *
 *  Created on: Mar 14, 2014
 *      Author: ant
 */

#ifndef MAPEXT_H_
#define MAPEXT_H_

#include <map>
#include "../appException.h"

template <typename TKey, typename T>
struct MapExt : public map<TKey, T>
{
	virtual const string keyErrorMessage(const TKey&) const = 0;
	virtual ~MapExt() {}

	bool has(const TKey& key) const
	{
		if (map<TKey, T>::find(key) == map<TKey, T>::end())
			return false;
		return true;
	}

	T& at(const TKey& key)
	{
		if (! has(key))
			throw AppException(keyErrorMessage(key));
		return this->operator[](key);
	}
 };


#endif /* MAPEXT_H_ */

