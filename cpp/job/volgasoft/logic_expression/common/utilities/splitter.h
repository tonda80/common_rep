/*
 * splitter.h
 *
 *  Created on: Mar 12, 2014
 *      Author: ant
 */

#ifndef SPLITTER_H_
#define SPLITTER_H_

#include <string>
#include <vector>

class Splitter : public std::vector<std::string>
{
    public:
    Splitter(const std::string src_string, const std::string delim)
    {
		size_t delimLen = delim.length();
		size_t posB = 0; 	// position of begin of search
		size_t posD;		// delimiter position

		while (1)
		{
			posD = src_string.find(delim, posB);

			if (posD == std::string::npos) {
				push_back(src_string.substr(posB));
				break;
			}

			push_back(src_string.substr(posB, posD - posB));
			posB = posD + delimLen;
		}
	}
};


#endif /* SPLITTER_H_ */
