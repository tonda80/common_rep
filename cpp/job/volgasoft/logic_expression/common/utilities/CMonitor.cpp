#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include "CMonitor.h"
#include <climits>
#include <pthread.h>
#include <stdio.h>

#define LOG_FILE "/tmp/elamscan.log"
#define CLOCK_REALTIME  0
#define RLOG_DATE_FORMAT	"%d-%b %H:%M:%S"
#define TOPRINTABLE(a) ((a<' ' || a>'z') ? '.' : (char)a)
//////////////////////////////////////////////////////////////////////////
// CMonitor
//////////////////////////////////////////////////////////////////////////

CMonitor mon;

std::string CMonitor::strip(std::string str)
{
    const std::string stripped = " \t\n\v\f\r";
	str.erase(0, str.find_first_not_of(stripped));       //prefixing
    str.erase(str.find_last_not_of(stripped)+1);         //suffixing
    return str;
}

int CMonitor::PrintTimestamp(char * szString)
{

	struct timespec rTs;
	clock_gettime(CLOCK_REALTIME, &rTs);
	struct tm rTm;
	localtime_r(&rTs.tv_sec, &rTm);

	char szTimeStamp[30];
	szTimeStamp[sizeof(szTimeStamp) - 1] = 0;
	szTimeStamp[0]=0;
	strftime(szTimeStamp, sizeof(szTimeStamp) - 1,	RLOG_DATE_FORMAT, &rTm);
	int iLenTime = sprintf(szString, "%s.%ld ", szTimeStamp, rTs.tv_nsec / 1000000);
	return iLenTime;

}

void CMonitor::openFile(const char* fileName)
{
	if(ld > 0)
		::close(ld);

	ld = ::open(fileName,O_WRONLY|O_APPEND|O_CREAT|O_SYNC,0644);
}

CMonitor::CMonitor() : ld(-1), lv(WARNING), enableMessages(true), filterMessTypes(false)
{

}

CMonitor::~CMonitor()
{
	if(ld > 0)
	{
		::close(ld);
	}
}
int CMonitor::DumpText(unsigned int level, const char *messType, const char *pBuf, unsigned int nSize, unsigned int flags)
{
	if(!checkOut(level, messType))
		return 1;

	if(pBuf == NULL || nSize == 0 || ld<=0)
		return 0;
	memset(m_buffStr,0, MAXLOGSTR);
	int iRv = makePrefix(level, messType);
	if ((nSize+iRv)>MAXLOGSTR){nSize=MAXLOGSTR-iRv;}
	memcpy(&m_buffStr[iRv],pBuf,nSize);
	memcpy(&m_buffStr[iRv+nSize],"\n\n",sizeof("\n\n"));
    ::write(ld, m_buffStr,iRv+nSize+sizeof("\n\n"));
	memset(m_buffStr,0, MAXLOGSTR);
	return 0;
}
int CMonitor::Dump(unsigned int level, const char *messType, const void *pBuf, unsigned int nSize, unsigned int flags)
{
	if(!checkOut(level, messType))
		return 1;

	if(pBuf == NULL || nSize == 0 || ld<=0)
		return 0;

	int iRv = makePrefix(level, messType);
	if ((nSize+iRv)>MAXLOGSTR){nSize=MAXLOGSTR-iRv;}
	bool firstStr = true;

	const unsigned char * pMem = (const unsigned char *)pBuf;
	const unsigned char * pStartMem = (const unsigned char *)pBuf;

	for(unsigned int nStartAddr=0; nStartAddr<nSize;
		nStartAddr+=16, pStartMem += 16)
	{
	    char * cur_pos = m_buffStr + iRv;
		*cur_pos = 0;

	    cur_pos += sprintf(cur_pos, "%04x: ", nStartAddr);
	    int j = 0;
	    for (j=0; j<16; j++,pMem++)
	    {
			if(nStartAddr+j < nSize)
				cur_pos += sprintf(cur_pos, "%02x ", *pMem);
			else
				cur_pos += sprintf(cur_pos, "   ");
	    }
	    pMem = pStartMem;
	    cur_pos += sprintf(cur_pos, ": ");
	    for (j = 0; j<16; j++, pMem++)
	    {
			if(nStartAddr+j < nSize)
				cur_pos += sprintf(cur_pos, "%c", TOPRINTABLE(*pMem));
			else
				cur_pos += sprintf(cur_pos, ".");
	    }
	    cur_pos += sprintf(cur_pos, " :\n");
	    ::write(ld, m_buffStr, cur_pos - m_buffStr);

	    if (firstStr)
	    {
	    	memset(m_buffStr, ' ', iRv);
	    	firstStr = false;
	    }
	}
	return 0;
}

int CMonitor::readSettings(const char* fileName)
{
	FILE * pFile;
	pFile = fopen (fileName, "r");
	if (pFile == NULL) {
		//enableMessages = false;
		return 1;
	}


	char buff[50];

	// first parameter is level
	if (fgets(buff, sizeof(buff), pFile)) {
		lv = atoi(buff);
	}

	// other parameters are the enabled message types
	while (!feof(pFile)) {
		if (fgets(buff, sizeof(buff), pFile))
		{
			std::string newType = strip(buff);
			if (newType.size()>0 && newType[0]!='#')
			{
				//puts(newType.c_str());
				messTypes.insert(newType);
			}
		}

	}
	if (!messTypes.empty())
		filterMessTypes = true; //File don't empty and we will filter the output messages

	fclose(pFile);
	return 0;
}

int CMonitor::operator()(unsigned int level, const char *messType, const char *fmt, ...)
{
	if (!checkOut(level, messType))
		return 1;

	va_list pvar;
	va_start(pvar, fmt);

	int nWrSym = makePrefix(level, messType);	// number of the written symbols
	
	vsnprintf(m_buffStr+nWrSym, MAXLOGSTR-nWrSym-1, fmt, pvar);
    ::write(ld,m_buffStr,strlen(m_buffStr));

    va_end(pvar);

    return 0;
}

int CMonitor::Puts(unsigned int level, const char *messType, const char *fmt, ...)
{
	if (!checkOut(level, messType))
		return 1;

	va_list pvar; va_start(pvar,fmt);

	int nWrSym = makePrefix(level, messType);	// number of the written symbols

	vsnprintf(m_buffStr+nWrSym, MAXLOGSTR-nWrSym-1, fmt, pvar);
	::write(ld,m_buffStr,strlen(m_buffStr));
	::write(ld,"\n",1);

    va_end (pvar);
	return 0;
}

bool CMonitor::checkMessType(const char* messType)
{
	if (!filterMessTypes || messType==NULL)
		return true;
	if (messTypes.find(messType) != messTypes.end())
		return true;

	return false;
}

const char* CMonitor::levelString(int level)
{
	switch (level)
	{
		case ERROR: return "ERROR";
		case WARNING: return "WARNING";
		case NOTICE: return "NOTICE";
		case INFO: return "INFO";
		case DEBUG: return "DEBUG";
		case BLAB: return "BLAB";
		default: return "";
	}
	return "";
}

bool CMonitor::checkOut(unsigned int level, const char *messType)
{
	if (!enableMessages)
		return false;
	if(ld <= 0)
		return false;
	if(level > lv)
		return false;
	if (!checkMessType(messType))
		return false;
	return true;
}

int CMonitor::makePrefix(unsigned int level, const char *messType)
{
	int nWrSym;	// number of the written symbols
	nWrSym = PrintTimestamp(m_buffStr);

	if (messType == NULL)
		return nWrSym;

	nWrSym += snprintf(m_buffStr+nWrSym, MAXLOGSTR-nWrSym-1, "[%s] %s: ",levelString(level), messType);

	return nWrSym;
}
