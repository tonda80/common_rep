/*
 * iWellJSON.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#include "iWellJSON.h"

#include "../utilities/CMonitor.h"
#include "../utilities/strHelper.h"

#include <memory>
#include <time.h>

// a link with other modules
extern bool noJsonDynData;
extern string appName;
//-----------

const string errorReplySign = "Unsuccessful";
const string okReplySign = "Ok";


int cntUniqueReqID = 0;
inline string formUniqueReqID()
{
	return appName + toString(cntUniqueReqID++);
}

void Request::deserialize(const string& st, list<Request>& lstReq)
{
	lstReq.clear();
	Request baseReq;

	JSONNode root;
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException("Invalid JSON");
	}

	if (! findJSONStr(root, "uniqueReqID", baseReq.uniqueReqID))
		throw CJSONException("Bad uniqueReqID");

	if (! findJSONInt(root, "ts", baseReq.ts))
		throw CJSONException("Bad ts");

	if (! findJSONStr(root, "method", baseReq.method))
		throw CJSONException("Bad method");

	JSONNode vNode;
	if (! findJSONArray(root, "values", vNode))
		throw CJSONException("Bad values");

	JSONNode pNode;
	if (! findJSONArray(root, "params", pNode))
		throw CJSONException("Bad params");

	// libjson evaluates size of an array wrongly if it has a bad object ("_":_ object into an array, but not into a node). See IWCD-160
	// Test for this bug
	// cout << "__debug0 " << vNode.size() << " " << pNode.size() << endl;
	// cout << "__debug0 " << vNode.size() << " " << pNode.size() << endl;
	// Output:
	// __debug0 1 1
	// __debug0 1 0

	if (vNode.size() != pNode.size())
		 throw CJSONException("Number of params and values are not equal");

	for (JSONNode::iterator itV = vNode.begin(), itP = pNode.begin(); itV != vNode.end() && itP != pNode.end(); itV++, itP++) {
		if (itV->type() != JSON_ARRAY || (itV->size() < 1) || (itV->as_array()[0].type() != JSON_STRING))
			throw CJSONException("Bad values");
		if (itP->type() != JSON_NODE)
			throw CJSONException("Bad params");
		baseReq.value = itV->as_array()[0].as_string();
		baseReq.param = itP->as_node();

		lstReq.push_back(baseReq);

	}
}

Reply::Reply(const string& st)
{
	JSONNode root;
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException("Invalid JSON");
	}

	if (! findJSONStr(root, "uniqueReqID", uniqueReqID))
		throw CJSONException("Bad reply uniqueReqID");

	if (! findJSONInt(root, "ts", ts))
		throw CJSONException("Bad reply ts");

	if (! findJSONArray(root, "values", values) || values.size() < 1)
		throw CJSONException("Bad reply values");
}

void Reply::checkValueFormat(const string& valueName)
{
	JSONNode& value = values[0];

	if (value.type() != JSON_ARRAY || value.size() != 3)
		throw CJSONException("Reply value is not a right array");
	if (value[0].type() != JSON_STRING || value[0].as_string() != valueName)
		throw CJSONException("Reply value error");
	if (value[2].type() != JSON_STRING)		//  || value[2].as_string()!=""
		throw CJSONException("Reply third string is not a string");
}

void Reply::checkOk(const string& valueName)
{
	checkValueFormat(valueName);

	if (payload().type() != JSON_STRING || payload().as_string() != okReplySign)
		throw CJSONException("Reply is not Ok");
}

void Reply::checkNotError(const string& valueName)
{
	checkValueFormat(valueName);

	if (payload().type() == JSON_STRING && payload().as_string() == errorReplySign)
		throw CJSONException("Reply is unsuccessful");
}

bool parseStandartOkReply(const string &msg, const string &type)
{
	try {
		Reply reply(msg);
		reply.checkOk(type);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR, "JSON_PARSER","Error parse standart reply for %s", type.c_str());
		return false;
	}

	return true;
}

void setReply(JSONNode& reply, const string &p1, const string &p2)
{
	while (reply.size()<2)
		reply.push_back(JSONNode());
	reply[0] = JSONNode("", p1);
	reply[1] = JSONNode("", p2);
}
void setReply(JSONNode& reply, const JSONNode &p1, const string &p2)
{
	while (reply.size()<2)
		reply.push_back(JSONNode());
	reply[0] = p1;
	reply[1] = JSONNode("", p2);
}

string createCommonReply(const list<Request> &lstReqs, list<JSONNode> &replyParts)
{
	JSONNode reply;

	if (lstReqs.size() == 0) {
		mon.Puts(CMonitor::ERROR, "JSON", "Empty request list");
		return "{\"values\":[[\"\",\"" + errorReplySign + "\",\"" + "Empty request list" + "\"]]}";
	}

	reply.push_back(JSONNode("uniqueReqID", lstReqs.front().uniqueReqID));
	if (! noJsonDynData) {
		reply.push_back(JSONNode("ts", time(NULL)));
	}

	JSONNode values(JSON_ARRAY);
	values.set_name("values");

	list<Request>::const_iterator itReq = lstReqs.begin();
	list<JSONNode>::iterator itRepP = replyParts.begin();
	for (; itReq != lstReqs.end() && itRepP != replyParts.end(); itReq++, itRepP++) {
		JSONNode value(JSON_ARRAY);
		value.push_back(JSONNode("", (*itReq).value));

		for (JSONNode::iterator it = (*itRepP).begin(); it != (*itRepP).end(); it++) {
			value.push_back(*it);
		}

		values.push_back(value);
	}

	reply.push_back(values);

	return reply.write();
}

SentRequest::SentRequest(const string& method, const string& value)
{
	if (! noJsonDynData) {
		req.push_back(JSONNode("uniqueReqID", formUniqueReqID()));
		req.push_back(JSONNode("ts", time(NULL)));
	}
	req.push_back(JSONNode("method", method));

	JSONNode values(JSON_ARRAY);
	values.set_name("values");
	JSONNode value0(JSON_ARRAY);
	value0.push_back(JSONNode("",value));
	values.push_back(value0);
	req.push_back(values);

	JSONNode params(JSON_ARRAY);
	params.set_name("params");
	req.push_back(params);

	JSONNode version;
	version.set_name("version");
	version.push_back(JSONNode("major", 1));
	version.push_back(JSONNode("minor", 0));
	req.push_back(version);
}

EMCSentRequest::EMCSentRequest(const string& method)
{
	if (! noJsonDynData) {
		req.push_back(JSONNode("uniqueReqID", formUniqueReqID()));
		req.push_back(JSONNode("ts", time(NULL)));
	}
	req.push_back(JSONNode("method", method));

	JSONNode values(JSON_ARRAY);
	values.set_name("values");
	JSONNode value0(JSON_ARRAY);
	values.push_back(value0);
	req.push_back(values);
}

EMCReply::EMCReply(const string& st)
{
	JSONNode root;
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException("Invalid JSON");
	}

	if (! findJSONStr(root, "method", method))
		throw CJSONException("Bad EMC reply method");

	if (! findJSONArray(root, "values", values) || values.size() < 1)
		throw CJSONException("Bad EMC reply values");

	// maybe it's unnecessary. Then value0() should be checked somewhere
	if (values.at(0).type() != JSON_ARRAY)
		throw CJSONException("Bad EMC reply values[0]");
}

SubscribeMsg::SubscribeMsg(const string& st)
{
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException("Invalid subscription message JSON");
	}

	if (root.type() != JSON_ARRAY || root.size() != 3 ||
			root[0].type() != JSON_ARRAY || root[0].size() < 2 ||
			root[1].type() != JSON_NODE || root[2].type() != JSON_NODE)
		throw CJSONException("Bad format of the subscription message");
}

DalSentRequest::DalSentRequest(const string& method)
{
	if (! noJsonDynData) {
		req.push_back(JSONNode("uniqueReqID", formUniqueReqID()));
		req.push_back(JSONNode("ts", time(NULL)));
	}
	req.push_back(JSONNode("application_name", appName));
	req.push_back(JSONNode("method", method));

	JSONNode params(JSON_ARRAY);
	params.set_name("params");
	req.push_back(params);

	JSONNode version;
	version.set_name("version");
	version.push_back(JSONNode("major", 1));
	version.push_back(JSONNode("minor", 0));
	req.push_back(version);
}

JSONNode DalSentRequest::newParam(const string& locationType, JSONNode& name)
{
	JSONNode location(JSON_NODE);
	location.set_name("location");
	location.push_back(JSONNode("document_type", locationType));
	location.push_back(JSONNode("document_subtype", ""));

	name.set_name("name");

	JSONNode newParam;
	newParam.push_back(location);
	newParam.push_back(name);

	return newParam;
}

void DalSentRequest::pushParam(const string& locationType, JSONNode& name)
{
	req.at("params").push_back(newParam(locationType, name));
}

void DalSentRequest::pushParam(const string& locationType, JSONNode& name, JSONNode& content)
{
	JSONNode param = newParam(locationType, name);
	content.set_name("content");
	param.push_back(content);

	req.at("params").push_back(param);
}

DalReply::DalReply(const string& st)
{
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException("Invalid JSON of DAL reply");
	}

	checkStatus();
}

void DalReply::checkStatus() {
	JSONNode::const_iterator it = root.find("status");
	if (it == root.end())
		throw CJSONException("DAL reply error. No status");

	if (it->type() == JSON_NODE) {
		try {
			throw AppException("DAL reply error. " + root["status"]["error"].toStr() + ". " + root["status"]["reason"].toStr());
		}
		catch (CJSONException&) {
			throw CJSONException("DAL reply error. Wrong status");
		}
	}

	if (it->type() == JSON_STRING) {
		if (it->as_string() == okReplySign)
			return;		// it's all right
		else
			throw AppException("DAL reply error. Status is not Ok");
	}

	throw CJSONException("DAL reply error. Wrong status");
}

const JSONNodeExt DalReply::value0()
{
	if (! findJSONArray(root, "values", _values) || _values.size() < 1)
		throw CJSONException("Bad DAL reply values");

	return _values.at(0);
}

const JSONNodeExt DalReply::values()
{
	if (! findJSONArray(root, "values", _values))
		throw CJSONException("Bad DAL reply values");

	return _values;
}
