/*
 * libjsonHelper.cpp
 *
 *  Created on: Mar 15, 2014
 *      Author: ant
 */

#include "libjsonHelper.h"
#include "../utilities/strHelper.h"

#include <stdexcept>

using namespace std;

string JSONNodeExt::toStr() const
{
	if (type() != JSON_STRING)
		throw CJSONException(ePrefix() + "is not string");
	return as_string();
}

int JSONNodeExt::toInt() const
{
	if (type() != JSON_NUMBER)
		throw CJSONException(ePrefix() + "is not number");
	return as_int();
}

double JSONNodeExt::toDouble() const
{
	if (type() != JSON_NUMBER)
		throw CJSONException(ePrefix() + "is not number");
	return as_float();
}

bool JSONNodeExt::toBool() const
{
	if (type() != JSON_BOOL)
		throw CJSONException(ePrefix() + "is not bool");
	return as_bool();
}

const JSONNodeExt& JSONNodeExt::operator[](const string& k) const
{
	if (type() != JSON_NODE)
		throw CJSONException(ePrefix() + "is not node");
	try {
		return static_cast<const JSONNodeExt&>(JSONNode::at(k));
	}
	catch (out_of_range&) {
		throw CJSONException(ePrefix() + "key error " + k);
	}
}

const JSONNodeExt& JSONNodeExt::operator[](int i) const
{
	if (type() != JSON_ARRAY)
		throw CJSONException(ePrefix() + "is not array");
	try {
		return static_cast<const JSONNodeExt&>(JSONNode::at(i));
	}
	catch (out_of_range&) {
		throw CJSONException(ePrefix() + "index error " + toString(i));
	}
}

bool findJSONInt(const JSONNode& nd, const char* name, int& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_NUMBER) return false;
	obj = it->as_int();
	return true;
}

bool findJSONDouble(const JSONNode& nd, const char* name, double& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_NUMBER) return false;
	obj = it->as_float();
	return true;
}

bool findJSONStr(const JSONNode& nd, const char* name, string& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_STRING) return false;
	obj = it->as_string();
	return true;
}

bool findJSONNode(const JSONNode& nd, const char* name, JSONNode& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_NODE) return false;
	obj = *it;
	return true;
}

bool findJSONArray(const JSONNode& nd, const char* name, JSONNode& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_ARRAY) return false;
	obj = *it;
	return true;
}

bool findJSONBool(const JSONNode& nd, const char* name, bool& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_BOOL) return false;
	obj = it->as_bool();
	return true;
}



