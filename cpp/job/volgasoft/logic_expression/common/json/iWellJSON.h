/*
 * iWellJSON.h
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 *
 *      This module implements a common approach for JSON parsing into iWell applications
 */

#ifndef IWELLJSON_H_
#define IWELLJSON_H_

#include <string>
#include <list>

#include "libjsonHelper.h"

using namespace std;

extern const string errorReplySign;
extern const string okReplySign;

// received reply
class Reply
{
public:
	Reply(const string& st);
	void checkValueFormat(const string& valueName);		// check an ordinary reply format (valueName, Something, "")
	void checkOk(const string& valueName);				// check an ordinary reply Ok (valueName, "Ok", "")
	void checkNotError(const string& valueName);		// check an ordinary reply Error (valueName, "Unsuccessful", "")
	const JSONNode& payload() {return values[0][1];}	// it returns the payload for some get requests

	string uniqueReqID;
	int ts;
	JSONNode values;
};

// sent request
class SentRequest
{
public:
	SentRequest(const string& method, const string& value);
	void paramsPush(const JSONNode& p) {req.at("params").push_back(p);}
	string write() {return req.write();}
	JSONNode req;
};

// received request
class Request
{
public:
	static void deserialize(const string& st, list<Request>& lstReq);

	string uniqueReqID;
	int ts;
	string method;
	string value;
	const JSONNode& getParam() const {return param;}	// param - JSON_NODE
private:
	JSONNode param;
};


void setReply(JSONNode& reply, const string &p1, const string &p2);
void setReply(JSONNode& reply, const JSONNode &p1, const string &p2);
string createCommonReply(const list<Request> &lstReqs, list<JSONNode> &replyParts);

bool parseStandartOkReply(const string &msg, const string &type);

// sent request to EMC
class EMCSentRequest
{
public:
	EMCSentRequest(const string& method);
	void pushInValue0(const JSONNode& p) {req.at("values").at(0).push_back(p);}
	string write() {return req.write();}
	JSONNode req;
};

// received reply from EMC
class EMCReply
{
public:
	EMCReply(const string& st);
	const JSONNode& value0() const {return values.at(0);}	// values[0] - JSON_ARRAY

	string method;
	JSONNode values;
};

// received publication
class SubscribeMsg
{
public:
	SubscribeMsg(const string& st);
	JSONNodeExt root;	// root[0] - JSON_ARRAY with size>=2, others - JSON_NODE
};

class DalSentRequest
{
public:
	JSONNode req;
	string write() {return req.write();}

	DalSentRequest(const string& method);
	void pushParam(const string& locationType, JSONNode& name);						// name will be named
	void pushParam(const string& locationType, JSONNode& name, JSONNode& content);	// name, content will be named

private:
	static JSONNode newParam(const string& type, JSONNode& name);
};

class DalReply
{
public:
	DalReply(const string& st);
	const JSONNodeExt value0();
	const JSONNodeExt values();

	JSONNodeExt root;

	void checkStatus();	// it throws CJSONException with a description of problem
private:
	JSONNode _values;
};


#endif /* IWELLJSON_H_ */
