/*
 * libjsonHelper.h
 *
 *  Created on: Mar 15, 2014
 *      Author: ant
 */

#ifndef LIBJSONHELPER_H_
#define LIBJSONHELPER_H_

#define NDEBUG
#include <libjson/libjson.h>

#include <string>

#include "../appException.h"

class CJSONException : public AppException
{
public:
	CJSONException(const std::string& txt) : AppException(txt) {}
};

class JSONNodeExt : public JSONNode
{
	std::string ePrefix() const {return "\"" + name() + "\" ";}
public:
	JSONNodeExt(const JSONNode& nd) : JSONNode(nd) {}
	JSONNodeExt() : JSONNode() {}

	const JSONNodeExt& operator[](const std::string& k) const;
	const JSONNodeExt& operator[](int i) const;
	std::string toStr() const;
	int toInt() const;
	double toDouble() const;
	bool toBool() const;
};


// it creates JSON_NODE object from 1 element
template <typename T>
inline JSONNode jsonObject(const std::string& name, const T& el)
{
	JSONNode obj(JSON_NODE);
	obj.push_back(JSONNode(name, el));
	return obj;
}

bool findJSONInt(const JSONNode& nd, const char* name, int& obj);
bool findJSONDouble(const JSONNode& nd, const char* name, double& obj);
bool findJSONStr(const JSONNode& nd, const char* name, std::string& obj);
bool findJSONNode(const JSONNode& nd, const char* name, JSONNode& obj);
bool findJSONArray(const JSONNode& nd, const char* name, JSONNode& obj);
bool findJSONBool(const JSONNode& nd, const char* name, bool& obj);

#endif /* LIBJSONHELPER_H_ */
