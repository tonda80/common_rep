#ifndef ZMQMANAGER_H_
#define ZMQMANAGER_H_

#include <zmq.h>
#include <string>
#include <list>
#include <map>

#define ZMQ_DAL_REQ 	1
#define ZMQ_EMC_REQ 	2
#define ZMQ_PUBLISHER 	3
#define ZMQ_LISTENER 	4
#define ZMQ_SUBSCRIBER 	5
#define ZMQ_SCADA_REQ   6

#define ZMQ_MSG_SIZE 16384
#define ZMQ_ATTEMPT_DELAY 1

#define REQ 		1
#define REP 		2
#define PUBLISH 	3
#define SUBSCRIBE 	4

using namespace std;

struct ZMQEnvironment
{
	map<int, string> endpoints;
	void (*requestCallback)(const string& stReq, string& stReply);
	void (*subscribeCallback)(const string& stMsg);

	int retryCount;
	int pollTimeout;   // timeout in ms
	ZMQEnvironment();		// default values
};

class ZmqManager
{
public:
	ZmqManager();
	static ZMQEnvironment env;
	static void init(void (*requestCallback)(const string& , string& ), void (*subscribeCallback)(const string&));

	int SendReq(string &msg,int endpoint);
	int Listener();
	int PublishMulti(const list<string> &msg);
	void Subscriber();
	int  SetSubscribeFilter(string filter);
	int  UnSetSubscribeFilter(string filter);

	int  CreateSubscriberThrd(const string& addr="");
	int  CreateListenerThrd(const string& addr="");

	int JoinListenerThrd();
	int JoinSubscriberThrd();
private:
	static void *ZmqContext;

	static void *ZmqListenerSock;
	static void *ZmqSubscribeSock;
	static void *ZmqPublishSock;

	static void *ZmqDALSock;
	static void *ZmqEMCSock;
	static void *ZmqSCADASock;

	static pthread_mutex_t ZmqPublishMutex;
	static pthread_t ZmqListenerThread;
	static pthread_t ZmqSubscriberThread;
	void *ConnectTo(int zmqPath,int connType,void **sock);
	void *CreateSubscribeSock(string connectionPath,void **sock);
	void *CreatePublishSock(string connectionPath,void **sock);
	void *CreateRequestSock(string connectionPath,void **sock);
	void *CreateReplySock(string connectionPath,void **sock);
	void **getSocket(int type);
	string getEndPoint(int type);
	void ProcessSubscrMsg(string recv_buf);
};

void *CreateSubscriberThrd(void *);

#endif /* ZMQMANAGER_H_ */
