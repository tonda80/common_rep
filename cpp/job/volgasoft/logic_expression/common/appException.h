/*
 * appException.h
 *
 *  Created on: Mar 4, 2014
 *      Author: ant
 */

#ifndef APPEXCEPTION_H_
#define APPEXCEPTION_H_

#include <string>

class AppException
{
public:
	AppException(const std::string& txt) : text(txt) {}
	std::string text;
	const char* c_str() {return text.c_str();}
};


#endif /* APPEXCEPTION_H_ */
