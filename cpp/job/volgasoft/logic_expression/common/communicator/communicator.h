/*
 * communicator.h
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include <string>
#include "../appException.h"
#include "../json/iWellJSON.h"

using namespace std;

namespace Communicator
{
	typedef int(*TRequestProcesser)(const Request& req, JSONNode& answer);
	// function of a handler of request
	// Returned values should be:
	// 0 - request was not processed
	// 1 - there was processed the set request with the answer of type {"Ok", ""}
	// 2 - there was processed the got request with the answer of type {answer, ""}
	// If error happens function should throw an exception of types RequestException or CJSONException

	void processRequest(const string& stReq, string& stReply);
}

#endif /* COMMUNICATOR_H_ */
