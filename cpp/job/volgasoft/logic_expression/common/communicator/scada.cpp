/*
 * scada.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: ant
 */

#include "../json/iWellJSON.h"
#include "../utilities/CMonitor.h"
#include "../zmq/ZmqManager.h"
#include "../appException.h"
#include "scada.h"

using namespace std;

namespace Scada
{

ZmqManager zmq;
inline void zmqSendToScada(string& msg)
{
	if (zmq.SendReq(msg, ZMQ_SCADA_REQ) <= 0)
		throw AppException("SCADA ZMQ send error");
}


JSONNodeExt requestUpdatePublishList(const JSONNode& param)
{
	SentRequest req("data-commit", "update_publish_list");
	req.paramsPush(param);

	string msg = req.write();

	mon.Puts(CMonitor::DEBUG, "SCADA", "SCADA sent request is %s", msg.c_str());
	zmqSendToScada(msg);

	Reply rep(msg);
	rep.checkValueFormat("update_publish_list");
	return rep.payload();
}

void requestDeletePublishList(const JSONNode& param)
{
	SentRequest req("data-commit", "delete_publish_list");
	req.paramsPush(param);

	string msg = req.write();

	mon.Puts(CMonitor::DEBUG, "SCADA", "SCADA sent request is %s", msg.c_str());
	zmqSendToScada(msg);

	Reply rep(msg);
	rep.checkOk("delete_publish_list");
}

} // end of namespace Scada

