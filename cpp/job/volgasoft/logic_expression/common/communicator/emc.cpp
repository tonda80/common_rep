/*
 * emc.cpp
 *
 *  Created on: Mar 4, 2014
 *      Author: ant
 */

#include "../json/iWellJSON.h"
#include "../utilities/CMonitor.h"
#include "../zmq/ZmqManager.h"
#include "emc.h"
#include "../appException.h"

using namespace std;

namespace Emc
{

ZmqManager zmq;
inline void zmqSendToEmc(string& msg)
{
	if (zmq.SendReq(msg, ZMQ_EMC_REQ) <= 0)
		throw AppException("EMC ZMQ send error");
}

JSONNode requestStatus(const string& id)
{
	EMCSentRequest req("data-request");

	JSONNode val(JSON_NODE);
	val.push_back(JSONNode("ValueID", id));
	val.push_back(JSONNode("parameter", "Status"));
	val.push_back(JSONNode("value", ""));
	val.push_back(JSONNode("Units", ""));
	req.pushInValue0(val);

	string msg = req.write();

	mon.Puts(CMonitor::DEBUG, "EMC", "EMC sent request is %s", msg.c_str());

	zmqSendToEmc(msg);

	//mon.Puts(CMonitor::DEBUG, "EMC", "EMC reply is %s", msg.c_str());

	EMCReply rep(msg);
	if (rep.method != "data-reply" || rep.value0().size() <3 || rep.value0().at(0) != "Status" ||
			rep.value0().at(1).type() != JSON_ARRAY || rep.value0().at(1).size() != 1 || rep.value0().at(1).at(0).type() != JSON_NODE)
		throw CJSONException("Bad EMC reply format");

	string valueId;
	if (!findJSONStr(rep.value0().at(1).at(0), "ValueID", valueId))
		throw CJSONException("Bad EMC reply (ValueID)");
	if (id != valueId)
		throw CJSONException("Bad EMC reply (different ID)");

	return rep.value0().at(1).at(0);
}

} // end of namespace Emc


