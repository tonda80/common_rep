/*
 * communicator.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#include "communicator.h"
#include "../utilities/CMonitor.h"

#include <sstream>

namespace Communicator
{

extern TRequestProcesser requestProcessers[];
extern int numProcessers;


void processRequest(const string& stReq, string& stReply)
{
	mon.Puts(CMonitor::NOTICE,"Communicator::processRequest", "Start processing request");

	list<Request> lstReqs;
	list<JSONNode> replyParts;

	try {
		Request::deserialize(stReq, lstReqs);	// parsing of a common request
	}
	catch (CJSONException& e) {					// error of parsing
		mon.Puts(CMonitor::ERROR, "Communicator::processRequest", "CJSONException. %s", e.text.c_str());
		stReply = "{\"values\":[[\"\",\"" + errorReplySign + "\",\"" + e.text + "\"]]}";
		return;
	}

	// Here we have formed a list of a particular requests. Next we process them.
	for (list<Request>::iterator itReq = lstReqs.begin(); itReq != lstReqs.end(); itReq++) {
		mon.Puts(CMonitor::NOTICE, "Communicator::processRequest", "Process request [%s %s]", itReq->method.c_str(), itReq->value.c_str());

		JSONNode partReply(JSON_ARRAY);		// particular reply
		JSONNode answer;					// Node for getting of data

		int iPr = 0;
		for (; iPr < numProcessers; iPr++) {
			try {
				int ret = requestProcessers[iPr](*itReq, answer);
				if (ret == 1) {								// the set request was processed without error
					setReply(partReply, okReplySign, "");
					break;
				}
				else if (ret == 2) {						// the got request was processed without error
					setReply(partReply, answer, "");
					break;
				}
			}
			catch (CJSONException& e) {						// JSON parsing error
				setReply(partReply, errorReplySign, "JSON error. "+e.text);
				break;
			}
			catch (AppException& e) {					// request was processed with error
				setReply(partReply, errorReplySign, e.text);
				break;
			}
		}

		if (iPr == numProcessers)
			setReply(partReply, errorReplySign, "Unknown request");		// form error about unknown request

		replyParts.push_back(partReply);
	}

	// forming of a common reply from the particular replies
	stReply = createCommonReply(lstReqs, replyParts);
	mon.Puts(CMonitor::BLAB, "Communicator::processRequest", "Reply is ready\n%s", stReply.c_str());
}

} // end of namespace Communicator


