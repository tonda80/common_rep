/*
 * emc.h
 *
 *  Created on: Mar 4, 2014
 *      Author: ant
 */

#ifndef EMC_H_
#define EMC_H_

#include <string>

class JSONNode;

namespace Emc
{
	JSONNode requestStatus(const std::string& id);	// function returns JSON_NODE may throw CJSONException, AppException

} // end of namespace Emc

#endif /* EMC_H_ */
