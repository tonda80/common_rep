/*
 * scada.h
 *
 *  Created on: Mar 14, 2014
 *      Author: ant
 */

#ifndef SCADA_H_
#define SCADA_H_

class JSONNodeExt;

namespace Scada
{

// if request were unsuccessful functions throw AppException
JSONNodeExt requestUpdatePublishList(const JSONNode& param);		// function takes "params" node and returns a list with values
void requestDeletePublishList(const JSONNode& param);

} // end of namespace Scada


#endif /* SCADA_H_ */
