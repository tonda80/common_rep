#include "JsonParser.h"
#include "../iwell/IWellEntities.h"
#include "../../common/utilities/CMonitor.h"
#include "../iwell/LeaSettings.h"

#include <iostream>
#include <sstream>
#include <memory>
#include <time.h>


bool findJSONInt(const JSONNode& nd, const char* name, int& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_NUMBER) return false;
	obj = it->as_int();
	return true;
}

bool findJSONStr(const JSONNode& nd, const char* name, string& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_STRING) return false;
	obj = it->as_string();
	return true;
}

bool findJSONNode(const JSONNode& nd, const char* name, JSONNode& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_NODE) return false;
	obj = it->as_node();
	return true;
}

bool findJSONArray(const JSONNode& nd, const char* name, JSONNode& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_ARRAY) return false;
	obj = it->as_array();
	return true;
}

bool findJSONBool(const JSONNode& nd, const char* name, bool& obj)
{
	JSONNode::const_iterator it = nd.find(name);
	if (it == nd.end() || it->type() != JSON_BOOL) return false;
	obj = it->as_bool();
	return true;
}

int cntUniqueReqID = 0;
inline string formUniqueReqID()
{
	stringstream ss;
	ss << "lea" << cntUniqueReqID++;
	return ss.str();
}

void Request::deserialize(const string& st, list<Request>& lstReq)
{
	lstReq.clear();
	Request baseReq;

	JSONNode root;
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException(CJSONException::INVALID_JSON, "JSON protocol error");
	}

	if (! findJSONStr(root, "uniqueReqID", baseReq.uniqueReqID))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad uniqueReqID");

	if (! findJSONInt(root, "ts", baseReq.ts))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad ts");

	if (! findJSONStr(root, "method", baseReq.method))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad method");

	JSONNode vNode;
	if (! findJSONArray(root, "values", vNode))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad values");

	JSONNode pNode;
	if (! findJSONArray(root, "params", pNode))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad params");

// Test for libjson bug when an error JSON ("__":__ object into an array, but not into a node). IWCD-160
//	cout << "__debug0 " << vNode.size() << " " << pNode.size() << endl;
//	cout << "__debug0 " << vNode.size() << " " << pNode.size() << endl;

	if (vNode.size() != pNode.size())
		 throw CJSONException(CJSONException::INVALID_STRUCTURE, "Number of params and values are not equal");

	for (JSONNode::iterator itV = vNode.begin(), itP = pNode.begin(); itV != vNode.end() && itP != pNode.end(); itV++, itP++) {

		if (itV->type() != JSON_ARRAY || (itV->size() < 1) || (itV->as_array()[0].type() != JSON_STRING))
			throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad values");
		if (itP->type() != JSON_NODE)
			throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad params");
		baseReq.value = itV->as_array()[0].as_string();
		baseReq.param = itP->as_node();

		lstReq.push_back(baseReq);

	}
}

void Reply::deserialize(const string& st, Reply& reply)
{
	JSONNode root;
	try {
		root = libjson::parse(st);
	}
	catch (invalid_argument&) {
		throw CJSONException(CJSONException::INVALID_JSON, "JSON protocol error");
	}

	if (! findJSONStr(root, "uniqueReqID", reply.uniqueReqID))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad uniqueReqID");

	if (! findJSONInt(root, "ts", reply.ts))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad ts");

	if (! findJSONArray(root, "values", reply.values) || reply.values.size() < 1)
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad values");
}

bool Reply::checkValueFormat(const string& method)
{
	JSONNode& value = values[0];

	if (value.type() != JSON_ARRAY || value.size()!=3) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER. Reply::checkOk", "Array error");
		return false;
	}
	if (value[0].type() != JSON_STRING || value[0].as_string() != method) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER. Reply::checkOk","Method error");
		return false;
	}
	if (value[2].type() != JSON_STRING || value[2].as_string()!="") {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER. Reply::checkOk","Empty string error");
		return false;
	}
	return true;
}

bool Reply::checkOk(const string& method)
{
	if (!checkValueFormat(method))
		return false;

	if (payload().type() != JSON_STRING || payload().as_string()!="Ok") {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER. Reply::checkOk","Ok error");
		return false;
	}

	return true;
}

void getLogicalEquationFromJSON(const JSONNode& jEq, LogicalEquation& eq)
{
	if (! findJSONInt(jEq, "le_id", eq.le_id))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad create_logical_equation.le_id");

	if (! findJSONStr(jEq, "resultant", eq.resultant))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad create_logical_equation.resultant");

	JSONNode _equation_array;
	if (! findJSONArray(jEq, "equation_array", _equation_array))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad create_logical_equation.equation_array");

	for (JSONNode::iterator itAr = _equation_array.begin(); itAr != _equation_array.end(); itAr++) {
		if (itAr->type() != JSON_NODE) throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad create_logical_equation.equation_array");
		JSONNode elNode = itAr->as_node();
		eqElement el;
		findJSONStr(elNode, "component_type", el.component_type);
		findJSONStr(elNode, "hw_interp_uuid", el.hw_interp_uuid);
		eq.equation_array.push_back(el);
	}

	findJSONInt(jEq, "logic_delay", eq.logicDelay);
}

RequestCreateLogicalEquation RequestCreateLogicalEquation::deserialize(const Request& baseReq)
{
	RequestCreateLogicalEquation crReq(baseReq);
	getLogicalEquationFromJSON(baseReq.getParam(), crReq);
	return crReq;
}

RequestDeleteLogicalEquation RequestDeleteLogicalEquation::deserialize(const Request& baseReq)
{
	RequestDeleteLogicalEquation dReq(baseReq);

	if (! findJSONInt(baseReq.getParam(), "le_id", dReq.le_id))
		throw CJSONException(CJSONException::INVALID_STRUCTURE, "Bad delete_logical_equation.le_id");

	return dReq;
}

void setReply(JSONNode& reply, const string &p1, const string &p2)
{
	while (reply.size()<2)
		reply.push_back(JSONNode());
	reply[0] = JSONNode("", p1);
	reply[1] = JSONNode("", p2);
}

string createReply(const list<Request> &lstReqs, list<JSONNode> &replyParts)
{
	JSONNode reply;

	if (lstReqs.size() == 0) {
		mon.Puts(CMonitor::ERROR, "JSON", "Empty request list");
		return "{\"values\":[[\"\",\"Unsuccessful\",\"Empty request list\"]]}";
	}

	reply.push_back(JSONNode("uniqueReqID", lstReqs.front().uniqueReqID));
	if (! CmdLineSettings::noJsonDynData) {
		reply.push_back(JSONNode("ts", time(NULL)));
	}

	JSONNode values(JSON_ARRAY);
	values.set_name("values");

	list<Request>::const_iterator itReq = lstReqs.begin();
	list<JSONNode>::iterator itRepP = replyParts.begin();
	for (; itReq != lstReqs.end() && itRepP != replyParts.end(); itReq++, itRepP++) {
		JSONNode value(JSON_ARRAY);
		value.push_back(JSONNode("", (*itReq).value));

		for (JSONNode::iterator it = (*itRepP).begin(); it != (*itRepP).end(); it++) {
			value.push_back(*it);
		}

		values.push_back(value);
	}

	reply.push_back(values);

	return reply.write();
}

void createPublishCalculationResultMsg(int id, const string& resultant, const string& action, bool value, list<string> &msg)
{
	msg.clear();

	JSONNode p1(JSON_ARRAY);
	p1.push_back(JSONNode("", "data"));
	p1.push_back(JSONNode("", id));
	msg.push_back(p1.write());
	mon.Puts(CMonitor::BLAB, "JSON", "Multipart 1 %s", p1.write().c_str());

	JSONNode p2(JSON_NODE);
	p2.push_back(JSONNode("source", "LEA"));
	p2.push_back(JSONNode("destination", "all"));
	msg.push_back(p2.write());

	JSONNode p3(JSON_NODE);
	p3.push_back(JSONNode("resultant", resultant));
	p3.push_back(JSONNode("evalutation_status", "successful"));
	p3.push_back(JSONNode("value", value));
	p3.push_back(JSONNode("action", action));
	msg.push_back(p3.write());
}

void createPublishErrorResultMsg(int id, const string& resultant, list<string> &msg)
{
	msg.clear();

	JSONNode p1(JSON_ARRAY);
	p1.push_back(JSONNode("", "data"));
	p1.push_back(JSONNode("", id));
	msg.push_back(p1.write());
	//mon.Puts(CMonitor::BLAB, "JSON", "Multipart 1 %s", p1.write().c_str());

	JSONNode p2(JSON_NODE);
	p2.push_back(JSONNode("source", "LEA"));
	p2.push_back(JSONNode("destination", "all"));
	msg.push_back(p2.write());

	JSONNode p3(JSON_NODE);
	p3.push_back(JSONNode("resultant", resultant));
	p3.push_back(JSONNode("evalutation_status", "unsuccessful"));
	msg.push_back(p3.write());
}

void createDisabledInterpretationsMsg(int id, const list<string>& invalidObjs, list<string> &msg)
{
	msg.clear();

	JSONNode p1(JSON_ARRAY);
	p1.push_back(JSONNode("", "exception"));
	p1.push_back(JSONNode("", id));
	msg.push_back(p1.write());

	JSONNode p2(JSON_NODE);
	p2.push_back(JSONNode("source", "LEA"));
	p2.push_back(JSONNode("destination", "all"));
	msg.push_back(p2.write());

	JSONNode p3(JSON_NODE);
	JSONNode arrDisObjs(JSON_ARRAY);
	arrDisObjs.set_name("disabled_interpretations");
	for (list<string>::const_iterator it = invalidObjs.begin(); it != invalidObjs.begin(); it++) {
		JSONNode disObj(JSONNode("", *it));
		arrDisObjs.push_back(disObj);
	}
	p3.push_back(arrDisObjs);
	msg.push_back(p3.write());
}

string createEmcStatusReq(const string &interpretation)
{
	EMCSentRequest request("data-request");

	JSONNode jInterp(JSON_NODE);
	jInterp.push_back(JSONNode("ValueID", interpretation));
	jInterp.push_back(JSONNode("parameter", "Status"));
	jInterp.push_back(JSONNode("value", ""));
	jInterp.push_back(JSONNode("Units", ""));

	request.pushInValue0(jInterp);
	return request.write();
}

string createEmcIoReq(const string &interpretation)
{
	EMCSentRequest request("data-request");
	JSONNode jInterp(JSON_NODE);
	jInterp.push_back(JSONNode("ValueID", interpretation));
	jInterp.push_back(JSONNode("parameter", "Get_EMC_Interpretations"));
	jInterp.push_back(JSONNode("value", ""));
	jInterp.push_back(JSONNode("Units", ""));

	request.pushInValue0(jInterp);
	return request.write();
}

int parseEmcHeader(const string &msg,const string &req_type,JSONNode& value)
{
	JSONNode root;
	try {
		root = libjson::parse(msg);
	}
	catch (invalid_argument&)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse JSON");
		return -1;
	}

	JSONNode::const_iterator field = root.find("uniqueReqID");
	if (field == root.end() || field->type() != JSON_STRING)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse uniqueReqID");
		return -1;
	}
	field = root.find("ts");
	if (field == root.end())
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse ts");
		return -1;
	}
	field = root.find("method");
	if (field == root.end() || field->type() != JSON_STRING || field->as_string()!="data-reply")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse method");
		return -1;
	}
	field = root.find("values");
	if (field == root.end() || field->type() != JSON_ARRAY || field->size()!=1)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse values");
		return -1;
	}
	value = field->as_array()[0];
	if (value.type() != JSON_ARRAY || value.size()!=3)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse values array");
		return -1;
	}

	if (value[0].type() != JSON_STRING || value[0].as_string()!=req_type)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse string [%s]",req_type.c_str());
		return -1;
	}
	if (value[2].type() != JSON_STRING || value[2].as_string()!="")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse string []");
		return -1;
	}
	return 0;
}

int parseEmcStatusData( JSONNode interpretNode,const string &interpretNames)
{

	  HWInterpretation interpretation;
	  JSONNode::iterator foundItem = interpretNode.find("ValueID");
	  if (foundItem==interpretNode.end() || foundItem->type()!=JSON_STRING)
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse ValueID");
			return -1;
	  }

	  if (foundItem->as_string()!=interpretNames)
	  {
		  mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse ValueID, received %s",foundItem->as_string().c_str());
		  return -1; // if we got not wished interpretation just skip it and continue
	  }
	  interpretation.name = foundItem->as_string();
	  foundItem = interpretNode.find("explanation");
	  if (foundItem==interpretNode.end() || foundItem->type()!=JSON_STRING ||
			  (foundItem->as_string() != "VALUE FOUND" && foundItem->as_string() != "Port Not Enabled" && foundItem->as_string() != "Data Not Received"))
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse explanation, received %s",foundItem->as_string().c_str());
			return -1;
	  }
	  if (foundItem->as_string()=="VALUE FOUND")
	  {
		  interpretation.valid = true;
		  interpretation.enabled = true;
	  }
	  if (foundItem->as_string()=="Port Not Enabled")
	  {
		  interpretation.valid = false;
		  interpretation.enabled = false;
		  interpretation.value = 0;
	  }
	  if (foundItem->as_string()=="Data Not Received")
	  {
		  interpretation.valid = false;
		  interpretation.enabled = true;
	  }
	  foundItem = interpretNode.find("value");
	  if (foundItem==interpretNode.end() || foundItem->type()!=JSON_NODE )
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error Scaled Value");
			return -1;
	  }
	  JSONNode::iterator scalVal = foundItem->find("Scaled_value");
	  if (scalVal==foundItem->end() || scalVal->type()!=JSON_NUMBER )
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error Scaled Value");
			return -1;
	  }
	  interpretation.value = scalVal->as_int();
	  IWellEntities iwellEntities = IWellEntities::getInstance();
	  HWInterpretation EntityToUpdate = iwellEntities.getEntity(interpretation.name);
	  if (iwellEntities.hasEntity(interpretation.name))
   	  {
		mon.Puts(CMonitor::NOTICE,"JSON_PARSER:EMC_REQ","Entity [%s] found, updating it: value [%f]->[%f], valid [%d]->[%d], enabled [%d]->[%d]",
				interpretation.name.c_str(),EntityToUpdate.value,interpretation.value,EntityToUpdate.valid,interpretation.valid,EntityToUpdate.enabled,interpretation.enabled);
	  }
	  else
 	  {
		mon.Puts(CMonitor::NOTICE,"JSON_PARSER:EMC_REQ","Entity [%s] not found, creating it, value [%f], valid [%d],enabled [%d]",
	  			interpretation.name.c_str(),interpretation.value,interpretation.valid,interpretation.enabled);
	  }
	  EntityToUpdate.name = interpretation.name;
	  EntityToUpdate.valid =  interpretation.valid;
	  EntityToUpdate.value =  interpretation.value;
	  EntityToUpdate.enabled =  interpretation.enabled;
	  iwellEntities.setEntity(EntityToUpdate.name,EntityToUpdate);
	return 0;
}

int parseEmcIoData( JSONNode interpretNode,const string &interpretNames)
{
	  HWInterpretation interpretation;
	  JSONNode::iterator foundItem = interpretNode.find("interpretation_uuid");
	  if (foundItem==interpretNode.end() || foundItem->type()!=JSON_STRING)
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse ValueID, received %s",foundItem->as_string().c_str());
			return -1;
	  }
	  if (foundItem->as_string()!=interpretNames)
	  {
		  mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse ValueID, received %s",foundItem->as_string().c_str());
		  return -1; // if we got not wished interpretation just skip it and continue
	  }
	  interpretation.name = foundItem->as_string();
	  foundItem = interpretNode.find("port_type");
	  if (foundItem==interpretNode.end() || foundItem->type()!=JSON_STRING || foundItem->as_string()=="")
	  {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:EMC_REQ","Error parse port_type");
			return -1;
	  }

	  interpretation.port_type =foundItem->as_string();
	  IWellEntities iwellEntities = IWellEntities::getInstance();
	  HWInterpretation EntityToUpdate = iwellEntities.getEntity(interpretation.name);
	  if (iwellEntities.hasEntity(interpretation.name))
   	  {
		mon.Puts(CMonitor::NOTICE,"JSON_PARSER:EMC_REQ","Entity [%s] found, updating it: value [%f], valid [%d], enabled [%d], port_type [%s]->[%s]",
				interpretation.name.c_str(),EntityToUpdate.value,EntityToUpdate.valid,
				EntityToUpdate.enabled,EntityToUpdate.port_type.c_str(),interpretation.port_type.c_str());
	  }
	  else
 	  {
		mon.Puts(CMonitor::NOTICE,"JSON_PARSER:EMC_REQ","Entity [%s] not found, creating it, port_type [%s]",interpretation.port_type.c_str());
	  }
	  EntityToUpdate.name = interpretation.name;
	  EntityToUpdate.port_type = interpretation.port_type;
	  iwellEntities.setEntity(EntityToUpdate.name,EntityToUpdate);
	return 0;
}

int processSubscrJson(const string& msg,string &interprName,double &val)
{
	mon.Puts(CMonitor::NOTICE,"JSON_PARSER:SUBSCRIBE","Start processing msg");
	JSONNode root;
	try {
		root = libjson::parse(msg);
	}
	catch (invalid_argument&)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse JSON");
		return -1;
	}
	if (root.type() !=JSON_ARRAY  || root.size()!=3)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse root object");
		return -1;
	}
	if (root[0].type() !=JSON_ARRAY)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse [0]\"data\" item in root");
		return -1;
	}
	if (root[2].type() !=JSON_NODE)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse [2]\"data\" item in root");
		return -1;
	}

	JSONNode thirdMsgPart = root[2];
	JSONNode::const_iterator name = thirdMsgPart.find("name");
	if (name == thirdMsgPart.end() || name->type() != JSON_STRING)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse name");
		return -1;
	}

	JSONNode::const_iterator value = thirdMsgPart.find("value");
	if (root[0][0].type() !=JSON_STRING)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse 'data' or 'alarm' item");
		return -1;
	}
	if (root[0][0] =="alarm")
	{
		if (value == thirdMsgPart.end() || value->type() != JSON_STRING || value->as_string() =="" )
		{
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse alarm value");
			return -1;
		}

		if (value->as_string() == "active")
		{
			val = 1;
		}
		else
		{
			val = 0;
		}
	}
	else
	{
		if (value == thirdMsgPart.end() || (value->type() != JSON_BOOL && value->type() != JSON_NUMBER))
		{
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:SUBSCRIBE","Error parse value");
			return -1;
		}

		if (value->type() == JSON_BOOL)
		{
			val = value->as_bool();
		}
		else
		{
			val = value->as_float();
		}
	}
	interprName = name->as_string();
	mon.Puts(CMonitor::NOTICE,"JSON_PARSER:SUBSCRIBE","Finish processing msg");
	return 0;
}
string createDalSettingsReq()
{
	SentRequest request("data-request", "get_application_configuration");

	request.paramsPush(jsonObject("application_name","lea_settings"));

	return request.write();
}
int ParseDalSettingsData(const JSONNode &configNode)
{
	JSONNode::const_iterator field = configNode.find("application_name");
	if (field == configNode.end() || field->type() != JSON_STRING || field->as_string()!="lea_settings")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse application_name");
		return -1;
	}
	field = configNode.find("configuration");
	if (field == configNode.end() || field->type() != JSON_NODE)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse configuration");
		return -1;
	}
	JSONNode::const_iterator configuration = field->find("EmcEndpoint");
	if (configuration == field->end() || configuration->type() != JSON_STRING || configuration->as_string()=="")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse EmcEndpoint, it will not update!");
	}
	else
	{
		LeaSettings::ZmqEmcEndpoint = configuration->as_string();
	}

	configuration = field->find("PublisherEndpoint");
	if (configuration == field->end() || configuration->type() != JSON_STRING || configuration->as_string()=="")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse PublisherEndpoint, it will not update!");
	}
	else
	{
		LeaSettings::ZmqPublisherEndpoint = configuration->as_string();
	}

	configuration = field->find("LeaEndpoint");
	if (configuration == field->end() || configuration->type() != JSON_STRING || configuration->as_string()=="")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse LeaEndpoint, it will not update!");
	}
	else
	{
		LeaSettings::ZmqLeaEndpoint = configuration->as_string();
	}

	configuration = field->find("SubscriberEndpoint");
	if (configuration == field->end() || configuration->type() != JSON_STRING || configuration->as_string()=="")
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse SubscriberEndpoint, it will not update!");
	}
	else
	{
		LeaSettings::ZmqSubscriberEndpoint = configuration->as_string();
	}

	configuration = field->find("PollTimeOut");
	if (configuration == field->end() || configuration->type() != JSON_NUMBER || configuration->as_int()<0 || configuration->as_int()>65535)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse PollTimeOut, it will not update!");
	}
	else
	{
		LeaSettings::ZmqPollTimeOut = configuration->as_int();
	}

	configuration = field->find("RetryCount");
	if (configuration == field->end() || configuration->type() != JSON_NUMBER || configuration->as_int()<0 || configuration->as_int()>65535)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse RetryCount, it will not update!");
	}
	else
	{
		LeaSettings::ZmqRetryCount = configuration->as_int();
	}
	return 0;
}
int ParseDalSettingsReply(const string &msg)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse reply %s", e.text.c_str());
		return -1;
	}

	if (!reply.checkValueFormat("get_application_configuration"))
		return -1;

	JSONNode value = reply.values[0];
	if (value[1].type() != JSON_NODE)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse config node");
		return -1;
	}
	if (ParseDalSettingsData(value[1].as_node()))
	{
		return -1;
	}
	return 0;
}

int ParseDalWriteEquationsReply(const string &msg)
{
	return parseStandartOkReply(msg, "add_logic_equation");
}

int ParseDalDeleteEquationsReply(const string &msg)
{
	return parseStandartOkReply(msg, "delete_logic_equation");
}
int ParseDalReadEquationsReply(const string &msg, list<LogicalEquation> &lstEquations)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parse reply %s", e.text.c_str());
		return -1;
	}

	JSONNode value = reply.values[0];
	if (value.size()<2 || value[0].as_string() != "get_list_of_logic_equations" || value[1].type() != JSON_ARRAY) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parsing of get_list_of_logic_equations");
		return -2;
	}

	lstEquations.clear();
	JSONNode eqs = value[1].as_array();
	for (JSONNode::iterator it = eqs.begin(); it != eqs.end(); it++) {
		LogicalEquation eq;
		try {
			getLogicalEquationFromJSON(*it, eq);
			lstEquations.push_back(eq);
		}
		catch (CJSONException& e) {
			mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_SETTINGS","Error parsing of get_list_of_logic_equations %s", e.text.c_str());
			return -2;
		}
	}

	return 0;
}

string createReadDalEquationReq()
{
	SentRequest request("data-request", "get_list_of_logic_equations");

	request.paramsPush(JSONNode(JSON_NODE));

	return request.write();
}
string createWriteDalEquationReq(const JSONNode &param)
{
	SentRequest request("data-commit", "add_logic_equation");

	request.paramsPush(param);

	return request.write();
}
string createDeleteDalEquationReq(int equationID)
{
	SentRequest request("data-delete", "delete_logic_equation");

	request.paramsPush(jsonObject("equation_ID",equationID));

	return request.write();
}


SentRequest::SentRequest(const string& method, const string& value)
{
	if (! CmdLineSettings::noJsonDynData) {
		req.push_back(JSONNode("uniqueReqID", formUniqueReqID()));
		req.push_back(JSONNode("ts", time(NULL)));
	}
	req.push_back(JSONNode("method", method));

	JSONNode values(JSON_ARRAY);
	values.set_name("values");
	JSONNode value0(JSON_ARRAY);
	value0.push_back(JSONNode("",value));
	values.push_back(value0);
	req.push_back(values);

	JSONNode params(JSON_ARRAY);
	params.set_name("params");
	req.push_back(params);

	JSONNode version;
	version.set_name("version");
	version.push_back(JSONNode("major", 1));
	version.push_back(JSONNode("minor", 0));
	req.push_back(version);
}

EMCSentRequest::EMCSentRequest(const string& method)
{
	if (! CmdLineSettings::noJsonDynData) {
		req.push_back(JSONNode("uniqueReqID", formUniqueReqID()));
		req.push_back(JSONNode("ts", time(NULL)));
	}
	req.push_back(JSONNode("method", method));

	JSONNode values(JSON_ARRAY);
	values.set_name("values");
	JSONNode value0(JSON_ARRAY);
	values.push_back(value0);
	req.push_back(values);

}

bool parseUpdateCapabilityStatus(const Request& request, bool& status)
{
	return findJSONBool(request.getParam(), "status", status);
}

bool parseUpdateDownTime(const Request& request, string &mode,int &time)
{
	if (!findJSONStr(request.getParam(), "mode", mode))
	{
		return false;
	}
	return findJSONInt(request.getParam(), "downtime", time);
}

string createUpdateCapabilityStatusToDalReq(bool status)
{
	SentRequest request("data-commit", "update_logical_expression_capability_status");

	request.paramsPush(jsonObject("status", status));

	return request.write();
}

string createUpdateDownTimeToDalReq(const string &mode,const int &time)
{
	SentRequest request("data-commit", "update_downtime");

	request.paramsPush(jsonObject("mode", mode));
	request.paramsPush(jsonObject("downtime", time));
	return request.write();
}

string createGetCapabilityStatusFromDalReq()
{
	SentRequest request("data-request", "get_logical_expression_capability_status");

	request.paramsPush(JSONNode(JSON_NODE));

	return request.write();
}

string createGetDownTimeFromDalReq()
{
	SentRequest request("data-request", "get_downtime_mode");

	request.paramsPush(JSONNode(JSON_NODE));

	return request.write();
}

int parseGetCapabilityStatusFromDalReply(const string &msg, bool& status)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_ACTION","Error parse reply %s", e.text.c_str());
		return -1;
	}

	if (!reply.checkValueFormat("get_logical_expression_capability_status"))
		return -1;

	if (findJSONBool(reply.values[0][1], "status", status))	// existence of reply.values[0][1] was checked into reply.checkValueFormat
		return 0;

	return -1;
}

int parseGetDownTimeFromDalReply(const string &msg, string &mode, int &time)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_DOWNTIME","Error parse reply %s", e.text.c_str());
		return -1;
	}

	if (!reply.checkValueFormat("get_downtime_mode"))
		return -1;

	if (!findJSONStr(reply.values[0][1], "mode", mode))	// existence of reply.values[0][1] was checked into reply.checkValueFormat
		return -1;

	if (!findJSONInt(reply.values[0][1], "downtime", time))
		return -1;

	return 0;
}

int parseStandartOkReply(const string &msg, const string &type)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR, "JSON_PARSER","Error parse standart reply for %s", type.c_str());
		return -1;
	}

	if (reply.checkOk(type))
		return 0;
	return -1;
}
