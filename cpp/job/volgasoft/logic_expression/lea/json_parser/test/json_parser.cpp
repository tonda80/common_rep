#include <stdio.h>
#include "../JsonParser.h"
#include "../../utilities/CMonitor.h"

#include <string>
#include <iostream>


using namespace std;

int main(int argc, char **argv)
{
	mon.openFile("/tmp/json_parser.log");
	mon.Puts(CMonitor::NOTICE,"JSON_PARSER","\n\n\n\n\nStart parser");
	string f_name = "./test.json";
	FILE * f = fopen(f_name.c_str(), "r");
	if (f==NULL)
	{
		mon.Puts(CMonitor::ERROR,"JSON_PARSER","Error open test file %s",f_name.c_str());
		printf("Error open test file %s\n",f_name.c_str());
		return -1;
	}

	string strInput;
	char buff[1024];
	while (!feof(f)) {
		if (fgets(buff, sizeof(buff), f) != NULL) strInput += buff;
	}
	//cout << "_debug " + strInput;

	list<Request*> reqs;
	try {
		Request::deserialize(strInput, reqs);
	}
	catch (CJSONException& e) {
		cout << "Error " << e.type << ". " << e.text << endl;
	}
	cout << "Total " << reqs.size() << endl;
	for (list<Request*>::iterator it = reqs.begin(); it != reqs.end(); it++) {
		Request* &req = *it;
		cout << req->uniqueReqID << " " << req->ts << " " << req->method << endl;
		if (req->value == "create_logical_equation") {
			RequestCreateLogicalEquation* crReq = static_cast<RequestCreateLogicalEquation*>(req);
			cout << "\t" << crReq->value << " " << crReq->le_id << " " << crReq->resultant << endl;
			int n = 0;
			for (list<eqElement>::iterator itEl = crReq->equation_array.begin(); itEl != crReq->equation_array.end(); itEl++)
				cout << "\t " << n++ << " " << itEl->component_type << " " << itEl->hw_interp_uuid << endl;
		}
	}

	//CReply repl;
	//repl.deserializer(st);


	return 0;
}


