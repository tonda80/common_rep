
#ifndef JSONPARSER_H_
#define JSONPARSER_H_

#include <string>
#include <list>

#define NDEBUG
#include <libjson/libjson.h>

using namespace std;

class CJSONException
{
public:
	enum {
		None	= 0,
		INVALID_JSON,
		INVALID_STRUCTURE,
		UNKNOWN_REQUEST
	};
	CJSONException() : type(None) {}
	CJSONException(int t) : type(t) {}
	CJSONException(int t, const string& txt) : type(t), text(txt) {}
	int type;
	string text;
};

// it creates JSON_NODE object from 1 element
template <typename T>
inline JSONNode jsonObject(const string& name, const T& el)
{
	JSONNode obj(JSON_NODE);
	obj.push_back(JSONNode(name, el));
	return obj;
}

bool findJSONInt(const JSONNode& nd, const char* name, int& obj);
bool findJSONStr(const JSONNode& nd, const char* name, string& obj);
bool findJSONNode(const JSONNode& nd, const char* name, JSONNode& obj);
bool findJSONArray(const JSONNode& nd, const char* name, JSONNode& obj);
bool findJSONBool(const JSONNode& nd, const char* name, bool& obj);

class Reply
{
public:
	static void deserialize(const string& st, Reply& reply);
	bool checkValueFormat(const string& method);	// check an ordinary reply format (methodName, Something, "")
	bool checkOk(const string& method);				// check an ordinary reply Ok (methodName, "Ok", "")
	const JSONNode& payload() {return values[0][1];}

	string uniqueReqID;
	int ts;
	JSONNode values;
};

class SentRequest
{
public:
	SentRequest(const string& method, const string& value);
	void paramsPush(const JSONNode& p) {req.at("params").push_back(p);}
	string write() {return req.write();}
	JSONNode req;
};

class EMCSentRequest
{
public:
	EMCSentRequest(const string& method);
	void pushInValue0(const JSONNode& p) {req.at("values").at(0).push_back(p);}
	string write() {return req.write();}
	JSONNode req;
};

class Request
{
public:
	static void deserialize(const string& st, list<Request>& lstReq);

	string uniqueReqID;
	int ts;
	string method;
	string value;
	const JSONNode& getParam() const {return param;}
private:
	JSONNode param;
};

// element of "equation_array" in "create_logical_equation" request
struct eqElement
{
	string component_type;
	string hw_interp_uuid;
};

struct LogicalEquation
{
	LogicalEquation() : le_id(-1), logicDelay(0) {}
	int le_id;
	string resultant;
	list<eqElement> equation_array;
	int logicDelay;
};

class RequestCreateLogicalEquation : public Request, public LogicalEquation
{
public:
	RequestCreateLogicalEquation(const Request& r) : Request(r) {}
	RequestCreateLogicalEquation(const LogicalEquation& e) : LogicalEquation(e) {}

	static RequestCreateLogicalEquation deserialize(const Request& baseReq);
};

class RequestDeleteLogicalEquation : public Request
{
public:
	RequestDeleteLogicalEquation(const Request& r) : Request(r), le_id(-1) {}

	static RequestDeleteLogicalEquation deserialize(const Request& baseReq);

	int le_id;
};


void setReply(JSONNode& reply, const string &p1, const string &p2);
string createReply(const list<Request> &lstReqs, list<JSONNode> &replyParts);

int parseStandartOkReply(const string &msg, const string &type);

void createPublishCalculationResultMsg(int id, const string& resultant, const string& action, bool value, list<string> &msg);
void createPublishErrorResultMsg(int id, const string& resultant, list<string> &msg);
void createDisabledInterpretationsMsg(int id, const list<string>& invalidObjs, list<string> &msg);

bool checkItemInList(const string &item,list<string> &interpretations);
int parseEmcStatusData( JSONNode interpretNode,const string &interpretNames);
int parseEmcIoData( JSONNode interpretNode,const string &interpretNames);
int parseEmcHeader(const string &msg,const string &req_type,JSONNode& value);
int processSubscrJson(const string& msg,string &interprName,double &val);
string createDalSettingsReq();
string createEmcStatusReq(const string &interpretation);
string createEmcIoReq(const string &interpretation);
int ParseDalSettingsReply(const string &msg);
string createWriteDalEquationReq(const JSONNode &params);
int ParseDalWriteEquationsReply(const string &msg);
string createReadDalEquationReq();
int ParseDalReadEquationsReply(const string &msg, list<LogicalEquation> &lstEquations);
string createDeleteDalEquationReq(int equationID);
int ParseDalDeleteEquationsReply(const string &msg);

// capability status
bool parseUpdateCapabilityStatus(const Request& request, bool& status);
string createUpdateCapabilityStatusToDalReq(bool status);
string createGetCapabilityStatusFromDalReq();
int parseGetCapabilityStatusFromDalReply(const string &msg, bool& status);
// downtime
string createUpdateDownTimeToDalReq(const string &mode,const int &time);
string createGetDownTimeFromDalReq();
int parseGetDownTimeFromDalReply(const string &msg, string &mode, int &time);
bool parseUpdateDownTime(const Request& request, string &mode,int &time);
#endif /* JSONPARSER_H_ */
