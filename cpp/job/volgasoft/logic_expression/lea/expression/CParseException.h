#ifndef CPARSEEXCEPTION_H_INCLUDED
#define CPARSEEXCEPTION_H_INCLUDED

#include <string>

using namespace std;

class CParseException
{
	public:

	enum {
		None	= 0,
		UnknownSymbol,
		NotNumber,
		NoCloseBracket,
		NoOneObjAfterEval,
		EmptyEval,
		NotBool,
		NotDouble,
		NotValue,
		UnknownIWellObj
	};
	CParseException() : type(None) {}
	//CParseException(int t) : type(t) {}
	CParseException(int t, const string& txt) : type(t), text(txt) {}

	int type;
	string text;

};

#endif // CPARSEEXCEPTION_H_INCLUDED
