#ifndef CLEXEME_H_INCLUDED
#define CLEXEME_H_INCLUDED

#include <string>
#include <sstream>


#include <cfloat>
#include <cstdio>

#include "CParseException.h"

using namespace std;

class CLexeme;

#include <list>
#define LEX_CONTAINER_TYPE list
typedef LEX_CONTAINER_TYPE<CLexeme> TLexemeContainer;
typedef LEX_CONTAINER_TYPE<CLexeme>::iterator TIterLexemeContainer;
typedef LEX_CONTAINER_TYPE<CLexeme>::const_iterator TConstIterLexemeContainer;

class CLexeme
{
public:
	enum {
		None	= 0,
		Number,
		OpenBracket,
		CloseBracket,
		OpPlus,
		OpMinus,
		OpMult,
		OpDivide,
		OpLess,
		OpGreate,
		OpEqual,
		OpLogAnd,
		OpLogOr,
		OpLogNot,
		IWellEntity,
		FunctionName
	};

	CLexeme() : type(None), pos(-1) {}
	CLexeme(int t, string txt, int p) : type(t), text(txt), pos(p) {}
	CLexeme(int t, char sym, int p) : type(t), text(string(1, sym)), pos(p) {}

	CLexeme(int t, string txt) : type(t), text(txt), pos(-1) {}
	CLexeme(int t, char sym) : type(t), text(string(1, sym)), pos(-1) {}

	const string& getText() const {return text;}
	int getType() const {return type;}
	int getPos() const {return pos;}

private:
	int type;
	string text;
	int pos;
};


class CSplitter
{
public:
	CSplitter() : currPos(0) {}
	int splitToLexemes(const string& srcStr, TLexemeContainer& lexLst);
	//const TLexemeContainer& getList() {return lexLst;}
private:
	//TLexemeContainer lexLst;
	int currPos;
	istringstream src;

	bool parseOneSymbolLexeme(char c, TLexemeContainer& lexLst);	// true it was taken
	bool getSymbol(char& c);			// cover for istringstream::get
	void putSymbol(char c);				// cover for istringstream::putback

};

#endif // CLEXEME_H_INCLUDED
