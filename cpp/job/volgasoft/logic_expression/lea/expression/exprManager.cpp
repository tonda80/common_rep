#include "exprManager.h"

#include <time.h>
#include <string.h>
#include <sstream>

#include "../iwell/IWellEntities.h"

#include "../../common/utilities/CMonitor.h"

#include "../expression/CCalculator.h"

#include "../iwell/communicator.h"

#include "../iwell/LeaSettings.h"

#include "../json_parser/JsonParser.h"


const string M02 = "digital_output-Interpretation-2";

unsigned int cntSec = 0;	// global counter of second


map<int, Expression> ExpressionManager::expressions;

Expression::Expression(const string& st)
{
	CSplitter splitter;
	splitter.splitToLexemes(st, lexCont);

	static int cnt = 100;
	id = cnt++;	// fake id
}

ExpressionManager& ExpressionManager::getInstance() {
	static ExpressionManager instance;

	return instance;
}

int ExpressionManager::startProcess()
{
	static bool started = false;
	if (started) {
		mon.Puts(CMonitor::ERROR,"CExpressionManager","Process is started already");
		return 3;
	}

	timer_t timerid;

	struct sigevent sevp;
    memset (&sevp, 0, sizeof (struct sigevent));
	sevp.sigev_notify = SIGEV_THREAD;
	sevp.sigev_value.sival_ptr = &timerid;
	sevp.sigev_notify_function = timerFunction;
	sevp.sigev_value.sival_int = 0;
	sevp.sigev_notify_attributes = NULL;

	if (timer_create(CLOCK_REALTIME, &sevp, &timerid) == -1) {
		mon.Puts(CMonitor::ERROR,"CExpressionManager","Error of timer_create");
		return 1;
	}

	struct itimerspec its = { { 1, 0 }, { 1, 0 } };

	if (timer_settime(timerid, 0, &its, NULL) == -1) {
		mon.Puts(CMonitor::ERROR,"CExpressionManager","Error of timer_settime");
		return 2;
	}

	started = true;

	return 0;
}


void ExpressionManager::timerFunction(sigval_t s)
{
	if (! ExpressionManager::getInstance().capabilityStatus)
		return;

	int size = expressions.size();
	if (size != 0)
		mon.Puts(CMonitor::DEBUG,"CExpressionManager","Calculation of %d expressions", size);

	for (map<int, Expression>::iterator it = expressions.begin(); it != expressions.end(); it++) {
		it->second.calculate();

		it->second.decrementTimers();
	}

	if (++cntSec%60==0)
	{
		IWellEntities::checkEntitiesValidity();
	}
}

int ExpressionManager::addExpression(const Expression& expr)
{
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Add expression [%d]", expr.id);

	for (list<string>::const_iterator it = expr.iWellObjs.begin(); it != expr.iWellObjs.end(); it++) {
		if (IWellEntities::getInstance().subscribe(*it) != 0) {
			mon.Puts(CMonitor::ERROR, "ExpressionManager", "Cannot subscribe. No addition");
			return 1;
		}
	}

	expressions.insert(pair<int, Expression>(expr.id, expr));
	return 0;
}

int ExpressionManager::removeExpression(int id)
{
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Remove expression [%d]", id);

	map<int, Expression>::iterator itExpr;
	if ((itExpr = expressions.find(id)) == expressions.end())
		return 1;

	for (list<string>::iterator it = itExpr->second.iWellObjs.begin(); it != itExpr->second.iWellObjs.end(); it++) {
		IWellEntities::getInstance().unsubscribe(*it,"data");
		// maybe we should try to remove object from map here
	}

	expressions.erase(id);
	return 0;
}

bool ExpressionManager::hasId(int id)
{
	map<int, Expression>::iterator itExpr;
	if ((itExpr = expressions.find(id)) == expressions.end())
		return false;
	return true;
}

void Expression::calculate()
{
	if (action.actionType == "" ) {
		mon.Puts(CMonitor::NOTICE, "Expression", "le_id %d. Action is not set. Don't calculate", id);
		return;
	}

	if (!checkIWellObjs()) {
		Communicator::publishCalculationError(id, resultant);
		Communicator::reportDisabledInterpretations(id, invalidIWellObjs);
		return;
	}

	try {
		CCalculator calc(lexCont);
		CExprObject res = calc.evalAll();

		Communicator::publishCalculationResult(*this, res.getBoolValue());

		beforeExecuteAction(res.getBoolValue());
	}
	catch (CParseException& e) {
		mon.Puts(CMonitor::ERROR, "Expression", "Exception when calculate %d [%s]", e.type, e.text.c_str());

		Communicator::publishCalculationError(id, resultant);
	}
}

string Expression::fromStruct(const LogicalEquation& req)
{
	mon.Puts(CMonitor::DEBUG, "Expression", "Making of new expression");

	id = req.le_id;

	resultant = req.resultant;

	logicDelay = req.logicDelay;

	for (list<eqElement>::const_iterator it = req.equation_array.begin(); it != req.equation_array.end(); it++) {
		mon.Puts(CMonitor::DEBUG, "Expression", "Adding [%s]", it->component_type.c_str());
		if (it->component_type == "OPEN")
			lexCont.push_back(CLexeme(CLexeme::OpenBracket, '('));
		else if (it->component_type == "CLOSE")
			lexCont.push_back(CLexeme(CLexeme::CloseBracket, ')'));
		else if (it->component_type == "NOT")
			lexCont.push_back(CLexeme(CLexeme::OpLogNot, '\''));
		else if (it->component_type == "OR")
			lexCont.push_back(CLexeme(CLexeme::OpLogOr, '+'));
		else if (it->component_type == "AND")
			lexCont.push_back(CLexeme(CLexeme::OpLogAnd, '*'));
		else if (it->component_type == "HIGH" || it->component_type == "LOW" || it->component_type == "END")
			lexCont.push_back(CLexeme(CLexeme::FunctionName, it->component_type));
		else if (it->component_type == "HW_INTERP") {
			list<eqElement>::const_iterator nextIt(it); nextIt++;
			if (nextIt != req.equation_array.end() && (nextIt->component_type == "HIGH" || nextIt->component_type == "LOW")) {
				string alName = alarmName(it->hw_interp_uuid, nextIt->component_type);
				lexCont.push_back(CLexeme(CLexeme::IWellEntity, alName));
				iWellObjs.push_back(alName);
			}
			else {
				lexCont.push_back(CLexeme(CLexeme::IWellEntity, it->hw_interp_uuid));
				iWellObjs.push_back(it->hw_interp_uuid);
			}
		}
		else {
			return "Unknown component "+it->component_type;
			mon.Puts(CMonitor::ERROR, "Expression", "Unknown lexeme [%s]", it->component_type.c_str());
		}
		mon.Puts(CMonitor::DEBUG, "Expression", "New lexeme [%s]", lexCont.back().getText().c_str());
	}
	return "";
}



// auxiliary functions for syntaxValidate()
bool isNotEnd(TConstIterLexemeContainer& i) {return !(i->getType()==CLexeme::FunctionName && i->getText()=="END");}
bool isNotHiLow(TConstIterLexemeContainer& i) {return !(i->getType()==CLexeme::FunctionName && (i->getText()=="HIGH" || i->getText()=="LOW"));}
bool isNotAndOr(TConstIterLexemeContainer& i) {return !(i->getType()==CLexeme::OpLogAnd || i->getType()==CLexeme::OpLogOr);}

string Expression::syntaxValidate()
{
	if (lexCont.size() < 1 || lexCont.size() > LeaSettings::maxNumExprComponents)
		return "Error of number components";

	int typeOf1 = lexCont.front().getType();
	if (typeOf1 != CLexeme::IWellEntity && typeOf1 != CLexeme::OpenBracket)
		return "Validating Initial State error";

	int cntNesting = 0;	// counter of open/close brackets

	for (TConstIterLexemeContainer it = lexCont.begin(); it != lexCont.end(); it++) {

		TConstIterLexemeContainer nextIt = it; nextIt++;
		if (nextIt == lexCont.end() && isNotEnd(it))
			return "Last component is not 'END'";

		switch (it->getType()) {
		case CLexeme::IWellEntity:
			if (nextIt->getType() != CLexeme::CloseBracket && isNotEnd(nextIt) && nextIt->getType() != CLexeme::OpLogNot && isNotAndOr(nextIt) && isNotHiLow(nextIt))
				return "Validating Hardware Interpretation Value error";
			break;
		case CLexeme::OpenBracket:
			cntNesting++;
			if (nextIt->getType() != CLexeme::OpenBracket && nextIt->getType() != CLexeme::IWellEntity)
				return "Validating Nesting Open error";
			break;
		case CLexeme::OpLogNot:
			if (nextIt->getType() != CLexeme::CloseBracket && isNotEnd(nextIt) && isNotAndOr(nextIt))
				return "Validating Negation error";
			break;
		case CLexeme::CloseBracket:
			if (cntNesting <= 0)
				return "Validating Nesting Close error";
			cntNesting--;
			if (nextIt->getType() != CLexeme::CloseBracket && isNotEnd(nextIt) && nextIt->getType() != CLexeme::OpLogNot && isNotAndOr(nextIt))	// maybe NOT is unnecessary
				return "Validating Nesting Close error";
			break;
		case CLexeme::OpLogAnd:
		case CLexeme::OpLogOr:
			if (nextIt->getType() != CLexeme::OpenBracket && nextIt->getType() != CLexeme::IWellEntity)
				return "Validating Logical OR_AND error";
			break;
		case CLexeme::FunctionName:
			if (it->getText() == "END") {
				if (nextIt != lexCont.end())
					return "'END' isn't last component";
				if (cntNesting != 0)
					return "Nesting error";
			}
			else if (it->getText() == "HIGH" || it->getText() == "LOW") {
				if (nextIt->getType() != CLexeme::CloseBracket && isNotEnd(nextIt) && nextIt->getType() != CLexeme::OpLogNot && isNotAndOr(nextIt))
					return "Validating High/Low Threshold error";
			}
			break;
		default:
			return "Unknown component";
			break;
		}

	}
	return "";
}

bool Expression::toJSON(JSONNode& jExpression) const
{
	jExpression.clear();

	jExpression.push_back(JSONNode("le_id", id));
	jExpression.push_back(JSONNode("resultant", resultant));
	jExpression.push_back(JSONNode("logic_delay", logicDelay));

	JSONNode jeqArray(JSON_ARRAY); jeqArray.set_name("equation_array");
	for (TConstIterLexemeContainer it = lexCont.begin(); it != lexCont.end(); it++) {
		JSONNode jEl(JSON_NODE);
		switch (it->getType()) {
		case CLexeme::OpenBracket:
			jEl.push_back(JSONNode("component_type", "OPEN")); break;
		case CLexeme::CloseBracket:
			jEl.push_back(JSONNode("component_type", "CLOSE")); break;
		case CLexeme::OpLogAnd:
			jEl.push_back(JSONNode("component_type", "AND")); break;
		case CLexeme::OpLogOr:
			jEl.push_back(JSONNode("component_type", "OR")); break;
		case CLexeme::OpLogNot:
			jEl.push_back(JSONNode("component_type", "NOT")); break;
		case CLexeme::IWellEntity:
			jEl.push_back(JSONNode("component_type", "HW_INTERP"));
			jEl.push_back(JSONNode("hw_interp_uuid", it->getText()));
			break;
		case CLexeme::FunctionName:
			jEl.push_back(JSONNode("component_type", it->getText())); break;
		default:
			mon.Puts(CMonitor::ERROR, "Expression", "Cannot serialize component expression %d, %s %d ", id, it->getText().c_str(), it->getType());
			return false;
		}
		jeqArray.push_back(jEl);
	}
	jExpression.push_back(jeqArray);

	return true;
}

void ExpressionManager::reactionTo(const string& name)
{
	if (!capabilityStatus)
		return;

	for (map<int, Expression>::iterator it = expressions.begin(); it != expressions.end(); it++) {
		it->second.reactionTo(name);
	}
}

void Expression::reactionTo(const string& name)
{
	for (list<string>::const_iterator it = iWellObjs.begin(); it != iWellObjs.end(); it++) {
		if (*it == name) {
			mon.Puts(CMonitor::NOTICE, "Expression", "Element [%s] from LE %d was changed. Expression will be recalculated", name.c_str(), id);
			calculate();
			return;
		}
	}
}

bool Expression::checkIWellObjs()
{
	invalidIWellObjs.clear();

	for (list<string>::const_iterator it = iWellObjs.begin(); it != iWellObjs.end(); it++) {
		if (! IWellEntities::getInstance().getEntity(*it).ready()) {
			invalidIWellObjs.push_back(*it);
		}
	}

	if (invalidIWellObjs.size() > 0)
		return false;

	return true;
}

string ExpressionManager::checkAction(const CAction& act)
{
	if (! hasId(act.equationId))
		return "No equation";

	const char* possActions[] = {"START", "MALF", "STOP", "FLAG", "TEMP", "LATCH", "TIMER"};
	const int possActionsSize = sizeof(possActions)/sizeof(possActions[0]);
	for (int i = 0; ; i++) {
		if (i == possActionsSize)
			return "Wrong action";
		if (act.actionType == possActions[i])
			break;
	}

	if (act.actionType == "TIMER" && (act.timerPeriod < 1 || act.timerPeriod > 9999))
		return "Wrong timer period";

	if ( expressions[act.equationId].resultant == M02
			&&	!(act.actionType == "START" || act.actionType == "MALF" || act.actionType == "STOP") )
		return act.actionType + " action is not allowed for M02";

	if ( expressions[act.equationId].resultant != M02
			&&	(act.actionType == "START" || act.actionType == "MALF" || act.actionType == "STOP") )
		return act.actionType + " action is not allowed for General Purpose Digital Output";

	return "";
}

void Expression::beforeExecuteAction(bool evResult)
{
	mon.Puts(CMonitor::DEBUG, "Expression", "Action [%s] for [%s]. Result %d, delay %d", action.actionType.c_str(), resultant.c_str(), evResult, logicDelay);

	if (logicDelay == 0 || evResult == false) {
		executeAction(evResult);
		timerLogicDelay = 0;
	}
	else if (timerLogicDelay == 0) {
		timerLogicDelay = logicDelay;
		mon.Puts(CMonitor::NOTICE, "Expression", "Action is postponed on %d sec", timerLogicDelay);
	}
}


void Expression::executeAction(bool evResult)
{

	if ((action.actionType == "START" || action.actionType == "MALF" || action.actionType == "STOP") && resultant == M02)
		; // for the present do nothing
	else if (action.actionType == "FLAG" && resultant != M02)
		; // for the present do nothing
	else if (action.actionType == "TEMP" && resultant != M02)
		executeTempAction(evResult);
	else if (action.actionType == "LATCH" && resultant != M02)
		executeLatchAction(evResult);
	else if (action.actionType == "TIMER" && resultant != M02)
		executeTimerAction(evResult);
	else
		mon.Puts(CMonitor::ERROR, "Expression", "Do nothing");
}

void Expression::executeTempAction(bool evResult)
{
	Communicator::EMCWriteOutput(resultant, evResult);
}

void Expression::executeLatchAction(bool evResult)
{
	if (evResult) {
		Communicator::EMCWriteOutput(resultant, true);
		latched = true;
	}
}

bool Expression::clearLatched() {
	if (! latched) {
		return false;
	}
	if (! ExpressionManager::getInstance().capabilityStatus) {
		return false; // requirement [0056]
	}

	Communicator::EMCWriteOutput(resultant, false);
	latched = false;

	return true;
}

void Expression::executeTimerAction(bool evResult)
{
	if (evResult) {
		if (timerActionTimer != 0)
			return;		// we are doing a pulse already

		Communicator::EMCWriteOutput(resultant, true);
		timerActionTimer = 60*action.timerPeriod;
	}
}

void Expression::decrementTimers()
{
	if (timerActionTimer != 0) {
		timerActionTimer--;

		if (timerActionTimer == 0) {
			Communicator::EMCWriteOutput(resultant, false);
		}
	}

	if (timerLogicDelay != 0) {
		timerLogicDelay--;

		if (timerLogicDelay == 0) {
			mon.Puts(CMonitor::NOTICE, "Expression", "Start postponed action");
			executeAction(true);	// Timer is running when the evaluation result is true only
		}
	}
}
int Expression::checkPortType(const string &interpretName,const string &portType)
{
	if (! IWellEntities::getInstance().hasEntity(interpretName))	{
		mon.Puts(CMonitor::ERROR, "Expression:Check_IO_Direction", "Error check port Type, no interpretation with name %s",interpretName.c_str());
		return -1;
	}

	HWInterpretation interpretation = IWellEntities::getInstance().getEntity(interpretName);

	if (interpretation.port_type!=portType)
	{
		mon.Puts(CMonitor::ERROR, "Expression:Check_IO_Direction", "Error check port Type for %s, mismatch [%s]!=[%s]"
				,interpretName.c_str(),portType.c_str(),interpretation.port_type.c_str());
		return -2;
	}
	return 0;
}
int Expression::validateIoDirection()
{
	mon.Puts(CMonitor::NOTICE, "Expression:Check_IO_Direction","Start IO validation");

	if (checkPortType(resultant, "digital_output") !=0 )
	{
		mon.Puts(CMonitor::ERROR, "Expression:Check_IO_Direction","Failed IO validation");
		return -1;
	}

	for (list<string>::iterator iWellobject = iWellObjs.begin(); iWellobject!=iWellObjs.end(); iWellobject++)
	{
		if (checkPortType(*iWellobject, "digital_input") != 0)
		{
			mon.Puts(CMonitor::ERROR, "Expression:Check_IO_Direction","Failed IO validation");
			return -1;
		}
	}
	mon.Puts(CMonitor::NOTICE, "Expression:Check_IO_Direction","IO validation OK");
	return 0;
}
