#ifndef CCALCULATOR_H_INCLUDED
#define CCALCULATOR_H_INCLUDED

#include "CLexeme.h"

// a text label for calculated objects
#define OBJ_LABEL "__calc"

class CExprObject : public CLexeme
{
public:
	CExprObject() : CLexeme(CLexeme::None, OBJ_LABEL, 0), validBValue(false), validDValue(false) {}
	CExprObject(const CLexeme& l);
	CExprObject(bool v) : CLexeme(CLexeme::Number, OBJ_LABEL, 0), bValue(v), validBValue(true), validDValue(false) {}
	CExprObject(double v) : CLexeme(CLexeme::Number, OBJ_LABEL, 0), dValue(v), validBValue(false), validDValue(true) {}


	bool getBoolValue() const;
	double getDoubleValue() const;
	bool isBool() {return validBValue;}
	bool isDouble() {return validDValue;}


private:
	bool bValue;
	double dValue;
	bool validBValue;
	bool validDValue;
};

double add(const CExprObject& op1, const CExprObject& op2);
double sub(const CExprObject& op1, const CExprObject& op2);
double mul(const CExprObject& op1, const CExprObject& op2);
double div(const CExprObject& op1, const CExprObject& op2);
bool lt(const CExprObject& op1, const CExprObject& op2);
bool gt(const CExprObject& op1, const CExprObject& op2);
bool eq(const CExprObject& op1, const CExprObject& op2);
bool and_(const CExprObject& op1, const CExprObject& op2);
bool or_(const CExprObject& op1, const CExprObject& op2);
bool not_(const CExprObject& op1);

typedef LEX_CONTAINER_TYPE<CExprObject> TExprObjectContainer;
typedef TExprObjectContainer::iterator TIterExprObjectContainer;

class CCalculator
{
	TExprObjectContainer expression;

public:
	CCalculator(const TLexemeContainer& lexCont);
	CExprObject evalAll() {return eval(expression.begin(), expression.end());}

	void printAll(const char* title = NULL);

private:
	CExprObject eval(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);

	TIterExprObjectContainer cutAndPaste(const TIterExprObjectContainer& from, const TIterExprObjectContainer& to, const CExprObject& newObj, TIterExprObjectContainer& evalBegin);

	void openBrackets(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	TIterExprObjectContainer findCloseBracket(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);

	void doFunc(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doNot(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doUnMinus(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doMulDiv(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doAddSub(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doLtGt(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doEq(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doAnd(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
	void doOr(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);

	void findBinOperands(const TIterExprObjectContainer& curr, TIterExprObjectContainer& first, TIterExprObjectContainer& last);

	void print(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end, const char* title = NULL);
	string objTostring(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end);
};

#endif // CCALCULATOR_H_INCLUDED
