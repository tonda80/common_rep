#ifndef EXPRMANAGER_H_
#define EXPRMANAGER_H_

#include <signal.h>
#include "CLexeme.h"

#include <map>

#include "../iwell/CAction.h"

#define NDEBUG
#include <libjson/libjson.h>

using namespace std;

class LogicalEquation;

class Expression
{
	friend class ExpressionManager;
public:
	Expression(const string& st);	// this constructor is useless. For debug
	Expression() : id(-1), latched(false), timerActionTimer(0), logicDelay(0), timerLogicDelay(0) {};

	bool isEmpty() {return lexCont.size() == 0;}

	int id;
	string resultant;

	list<string> iWellObjs;			// list of names of used input IWell object
	list<string> invalidIWellObjs;	// list of names of invalid IWell object
	bool checkIWellObjs();			// it check what each from iWellObjs is enabled and valid

	string fromStruct(const LogicalEquation& req);	// create from request
	void calculate();
	string syntaxValidate();
	bool toJSON(JSONNode& jExpression) const;

	void reactionTo(const string& name);	// callback for events of subscription message

	void setAction(const CAction& a) {if (a.equationId == id) action = a;}
	const CAction getAction() const {return action;}
	void deleteAction() {action.actionType = "";}
	int validateIoDirection();
	bool clearLatched();

private:
	TLexemeContainer lexCont;

	CAction action;
	void beforeExecuteAction(bool evResult);
	void executeAction(bool evResult);
	void executeTempAction(bool evResult);
	void executeLatchAction(bool evResult);
	void executeTimerAction(bool evResult);
	int checkPortType(const string &interpretName,const string &portType);
	// for action
	bool latched;
	int timerActionTimer;
	int logicDelay;
	int timerLogicDelay;

	void decrementTimers();


	// TODO mutex for protection expressions when calculate
};

class ExpressionManager
{
public:
	static int startProcess();
	static ExpressionManager& getInstance();
	int addExpression(const Expression& expr);
	int removeExpression(int id);
	bool hasId(int id);
	unsigned int expressionNumber() {return expressions.size();}

	Expression& operator[](int i) {return expressions[i];}

	void reactionTo(const string& name);	// callback for events of subscription message

	bool capabilityStatus;

	string DownTimeMode;
	int DownTime;

	string checkAction(const CAction& act);

private:
	ExpressionManager() : capabilityStatus(false),DownTime(-1) {}
	static void timerFunction(sigval_t s);

	static map<int, Expression> expressions;
};

#endif
