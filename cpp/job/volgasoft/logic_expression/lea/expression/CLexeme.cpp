#include "CLexeme.h"

#include <cstdio>

int CSplitter::splitToLexemes(const string& srcStr, TLexemeContainer& lexLst)
{
	src.clear();
	src.str(srcStr);
	currPos = 0;

	lexLst.clear();

	char c;
  	while (getSymbol(c)) {

		if (isspace(c)) {
			continue;
		}
		else if (parseOneSymbolLexeme(c, lexLst)) {
			continue;
		}
		else if (isdigit(c)) {
			string st(1, c);
			int p = currPos - 1;
			while (getSymbol(c) && (isdigit(c) || c=='.')) {
				st.push_back(c);
			}
			putSymbol(c);
			lexLst.push_back(CLexeme(CLexeme::Number, st, p));
		}
		else if (isalpha(c)) {
			string st(1, c);
			int p = currPos - 1;
			while (getSymbol(c) && isalnum(c)) {
				st.push_back(c);
			}
			putSymbol(c);
			lexLst.push_back(CLexeme(CLexeme::None, st, p));	// Detection object supposed to be here
		}
		else {	// Unknown symbol
			char errorText[100];
			snprintf(errorText, sizeof(errorText)-1, "Error symbol [%c] [%d]", c, c);
			throw CParseException(CParseException::UnknownSymbol, errorText);
		}
	}

	return 0;
}

bool CSplitter::parseOneSymbolLexeme(char c, TLexemeContainer& lexLst)
{
	// One symbol lexemes
	switch (c) {
		case '(':
			lexLst.push_back(CLexeme(CLexeme::OpenBracket, c, currPos-1)); return true;
		case ')':
			lexLst.push_back(CLexeme(CLexeme::CloseBracket, c, currPos-1)); return true;
		case '+':
			lexLst.push_back(CLexeme(CLexeme::OpPlus, c, currPos-1)); return true;
		case '-':
			lexLst.push_back(CLexeme(CLexeme::OpMinus, c, currPos-1)); return true;
		case '*':
			lexLst.push_back(CLexeme(CLexeme::OpMult, c, currPos-1)); return true;
		case '/':
			lexLst.push_back(CLexeme(CLexeme::OpDivide, c, currPos-1)); return true;
		case '<':
			lexLst.push_back(CLexeme(CLexeme::OpLess, c, currPos-1)); return true;
		case '>':
			lexLst.push_back(CLexeme(CLexeme::OpGreate, c, currPos-1)); return true;
		case '=':
			lexLst.push_back(CLexeme(CLexeme::OpEqual, c, currPos-1)); return true;
		case '&':
			lexLst.push_back(CLexeme(CLexeme::OpLogAnd, c, currPos-1)); return true;
		case '|':
			lexLst.push_back(CLexeme(CLexeme::OpLogOr, c, currPos-1)); return true;
		case '!':
			lexLst.push_back(CLexeme(CLexeme::OpLogNot, c, currPos-1)); return true;
	}
	return false;
}

bool CSplitter::getSymbol(char& c)
{
	currPos++;
	return src.get(c);
}

void CSplitter::putSymbol(char c)
{
	currPos--;
	src.putback(c);
}

