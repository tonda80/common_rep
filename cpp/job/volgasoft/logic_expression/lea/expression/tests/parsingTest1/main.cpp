#include <iostream>
#include <cstdio>
#include <string>
#include "../../CLexeme.h"
#include "../../CCalculator.h"

using namespace std;

int main()
{
try
{
	cout << "**************************" << endl;

	CLexeme l1(CLexeme::Number, "3.2", 0);
	CLexeme l2(CLexeme::Number, "2.2", 0);
	CLexeme l3(CLexeme::Number, "1", 0);
	CLexeme l4(CLexeme::Number, "1", 0);
	CLexeme l5(CLexeme::Number, "32", 0);
	CLexeme l6(CLexeme::Number, "22", 0);

	cout << add(CExprObject(l1), CExprObject(l2)) << endl;
	cout << mul(l1, l2) << endl;
	//cout << div<double>(l1, l2) << endl;
	//cout << sub<double>(l1, l2) << endl;

	cout << "not " << not_(CExprObject(l3)) << endl;
	cout << "gt " << gt(l5, l6) << endl;

	cout << "and " << and_(l3, l4) << endl;
	cout << "or " << or_(l3, l4) << endl;

	// it is exception cout << add<int>(l1, l2) << endl;

	cout << "**************************" << endl;

    string s = "45>312 | 22<0";
    cout << s << endl;

	CSplitter splitter;
	splitter.splitToLexemes(s);

	for (TConstIterLexemeContainer it = splitter.getList().begin(); it != splitter.getList().end(); it++) {
		printf("Type %2d:  %6s \t (%2d) (%3d)\n", it->getType(), it->getText().c_str(), it->getText().size(), it->getPos());
    }
    cout << endl;

   cout << "**************************" << endl;

	CCalculator calc(splitter.getList());

	cout << "Result " << calc.evalAll().getBoolValue() << endl;
	//calc.printAll();

}

catch (CParseException& e) {
		cout << "Error " << e.type << ". " << e.text << endl;
	}

    return 0;
}
