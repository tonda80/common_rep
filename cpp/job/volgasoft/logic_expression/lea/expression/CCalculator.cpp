#include "CCalculator.h"

#include "../iwell/IWellEntities.h"

#include <iostream>

struct CIWellEntity
{
	operator bool() {return true;}
	operator double() {return 7.77;}
};


template <typename TV>
TV getValue_(const CLexeme& obj)
{
	char errorText[100];

	if (obj.getType() == CLexeme::IWellEntity) {

		HWInterpretation hw = IWellEntities::getInstance().getEntity(obj.getText());
		if (!hw.ready())
			throw CParseException(CParseException::UnknownIWellObj, "Invalid iWell Object "+obj.getText());
		return static_cast<TV>(hw);
	}

	if (obj.getType() == CLexeme::Number) {
		istringstream buffer(obj.getText());
		TV value;
		buffer >> value;
		if ( !(!buffer.fail() && buffer.eof()) ) {
			snprintf(errorText, sizeof(errorText)-1, "[%s] is not valid a value type", obj.getText().c_str());
			throw CParseException(CParseException::NotNumber, errorText);
		}
		return value;
	}

	snprintf(errorText, sizeof(errorText)-1, "[%s] doesn't have the value", obj.getText().c_str());
	throw CParseException(CParseException::NotValue, errorText);
}

double add(const CExprObject& op1, const CExprObject& op2) { return op1.getDoubleValue() + op2.getDoubleValue();}
double sub(const CExprObject& op1, const CExprObject& op2) { return op1.getDoubleValue() - op2.getDoubleValue();}
double mul(const CExprObject& op1, const CExprObject& op2) { return (op1.getDoubleValue() * op2.getDoubleValue());}
double div(const CExprObject& op1, const CExprObject& op2) { return op1.getDoubleValue() / op2.getDoubleValue();}
bool lt(const CExprObject& op1, const CExprObject& op2) {return op1.getDoubleValue() < op2.getDoubleValue();}
bool gt(const CExprObject& op1, const CExprObject& op2) {return op1.getDoubleValue() > op2.getDoubleValue();}
bool eq(const CExprObject& op1, const CExprObject& op2) {return op1.getDoubleValue() == op2.getDoubleValue();}
bool and_(const CExprObject& op1, const CExprObject& op2) {return op1.getBoolValue() && op2.getBoolValue();}
bool or_(const CExprObject& op1, const CExprObject& op2) {return op1.getBoolValue() || op2.getBoolValue();}
bool not_(const CExprObject& op1) {return !op1.getBoolValue();}

CExprObject::CExprObject(const CLexeme& l) : CLexeme(l), validBValue(false), validDValue(false)
{
	if (l.getType()==CLexeme::IWellEntity || l.getType()==CLexeme::Number) {
		dValue = getValue_<double>(l);
		validDValue = true;
	}
}


bool CExprObject::getBoolValue() const
{
	if (validBValue)
		return bValue;
	if (validDValue) {
		if (dValue==0) return false;
		return true;
	}
	throw CParseException(CParseException::NotBool, getText()+" isn't bool");
	return false;
}

double CExprObject::getDoubleValue() const
{
	if (validDValue)
		return dValue;
//	if (validBValue) {
//		if (bValue==0) return 0;
//		return 1;
//	}
	throw CParseException(CParseException::NotDouble, getText()+" isn't double");
	return 0.0;
}


CCalculator::CCalculator(const TLexemeContainer& lexCont)
{
	for (TConstIterLexemeContainer it = lexCont.begin(); it != lexCont.end(); it++) {
		expression.push_back(*it);
	}
}

CExprObject CCalculator::eval(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	TIterExprObjectContainer beginEval = beg;

	if (beginEval == end) {	// empty
		throw CParseException(CParseException::EmptyEval, "Empty expression");
	}

	openBrackets(beginEval, end);
	doFunc(beginEval, end);
//	print(beginEval, end);
	doNot(beginEval, end);
//	print(beginEval, end);
	doUnMinus(beginEval, end);
//	print(beginEval, end);
	doMulDiv(beginEval, end);
//	print(beginEval, end);
	doAddSub(beginEval, end);
//	print(beginEval, end);
	doLtGt(beginEval, end);
//	print(beginEval, end);
	doEq(beginEval, end);
//	print(beginEval, end);
	doAnd(beginEval, end);
//	print(beginEval, end);
	doOr(beginEval, end);
//	print(beginEval, end);

	TIterExprObjectContainer result = beginEval;

	if (++beginEval != end) {	// we have more than one object in rest
		//cout << "__debug "; printAll();
		throw CParseException(CParseException::NoOneObjAfterEval, "Cannot eval an expression "+objTostring(beginEval, end));
	}

	//cout << "__debug "; printAll();

	return *result;
}

void CCalculator::doFunc(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if (currPos->getType() == CLexeme::FunctionName) {
			if (currPos->getText() == "END") {
				currPos = expression.erase(currPos);
			}
			else if (currPos->getText() == "HIGH" || currPos->getText() == "LOW") {
				currPos = expression.erase(currPos);	// previous object should contain an alarm value
			}
		}
	}
}

void CCalculator::doNot(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if (currPos->getType() == CLexeme::OpLogNot) {
			TIterExprObjectContainer first = currPos; first--;
			CExprObject newObj(not_(*first));
			currPos = cutAndPaste(first, ++currPos, newObj, beg);
		}
	}
}

void CCalculator::doUnMinus(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		TIterExprObjectContainer posBefore = currPos; posBefore--;
		if (currPos->getType() == CLexeme::OpMinus && (currPos==beg || (posBefore->getType()!=CLexeme::Number)) ) {
			TIterExprObjectContainer first = currPos++;
			CExprObject newObj(currPos->getDoubleValue()*-1);
			currPos = cutAndPaste(first, ++currPos, newObj, beg);
		}
	}
}

void CCalculator::doMulDiv(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	TIterExprObjectContainer currPos;
	for ( currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpMult) || (currPos->getType() == CLexeme::OpDivide)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj;
			if (currPos->getType() == CLexeme::OpMult)
				newObj = CExprObject(mul(*first, *last));
			else
				newObj = CExprObject(div(*first, *last));

			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::doAddSub(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpPlus) || (currPos->getType() == CLexeme::OpMinus)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj;
			if (currPos->getType() == CLexeme::OpPlus)
				newObj = CExprObject(add(*first, *last));
			else
				newObj = CExprObject(sub(*first, *last));
			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::doLtGt(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpLess) || (currPos->getType() == CLexeme::OpGreate)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj;
			if (currPos->getType() == CLexeme::OpLess)
				newObj = CExprObject(lt(*first, *last));
			else
				newObj = CExprObject(gt(*first, *last));
			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::doEq(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpEqual)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj(eq(*first, *last));
			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::doAnd(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpLogAnd)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj(and_(*first, *last));
			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::doOr(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if ((currPos->getType() == CLexeme::OpLogOr)) {
			TIterExprObjectContainer first, last;
			findBinOperands(currPos, first, last);
			CExprObject newObj(or_(*first, *last));
			currPos = cutAndPaste(first, ++last, newObj, beg);
		}
	}
}

void CCalculator::findBinOperands(const TIterExprObjectContainer& curr, TIterExprObjectContainer& first, TIterExprObjectContainer& last)
{
	first = curr; first--;
	last = curr; last++;
}

void CCalculator::openBrackets(TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	for (TIterExprObjectContainer currPos = beg; currPos != end; currPos++) {
		if (currPos->getType() == CLexeme::OpenBracket) {
			TIterExprObjectContainer first = currPos;
			TIterExprObjectContainer last = findCloseBracket(first, end);

			CExprObject newObj = eval(++first, last);
			currPos = cutAndPaste(currPos, ++last, newObj, beg);
		}
		else if (currPos->getType() == CLexeme::CloseBracket) {
			char errorText[100];
			snprintf(errorText, sizeof(errorText)-1, "There is a unnecessary close bracket (%d)", currPos->getPos());
			throw CParseException(CParseException::NoCloseBracket, errorText);
		}
	}
}

TIterExprObjectContainer CCalculator::findCloseBracket(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	int cntOpenBrackets = 0;

	TIterExprObjectContainer currPos = beg; currPos++;		// beg points to "("
	for (; currPos != end; currPos++) {
		switch (currPos->getType()) {
		case CLexeme::OpenBracket:
			cntOpenBrackets++;
			break;
		case CLexeme::CloseBracket:
			if (cntOpenBrackets == 0)
				return currPos;
			else
				cntOpenBrackets--;
			break;
		}
	}

	char errorText[100];
	snprintf(errorText, sizeof(errorText)-1, "There is not a close bracket for the open bracket (%d)", beg->getPos());
	throw CParseException(CParseException::NoCloseBracket, errorText);

	return expression.end();	// we will be not here. For compilier
}

void CCalculator::printAll(const char* title)
{
	cout << "Total " << expression.size() << endl;
	print(expression.begin(), expression.end(), title);
}

void CCalculator::print(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end, const char* title)
{
	if (title != NULL)
		cout << title << endl;
	for (TIterExprObjectContainer it = beg; it != end; it++) {
		cout << it->getText();
		if (it->getText() == OBJ_LABEL ) {
			cout << "[";
			if (it->isBool()) cout << it->getBoolValue() << " ";
			if (it->isDouble()) cout << it->getDoubleValue() << " ";
			cout << "]";
		}
		cout << " ";
	}
	cout << endl;
}

string CCalculator::objTostring(const TIterExprObjectContainer& beg, const TIterExprObjectContainer& end)
{
	stringstream ss;
	ss << "[";
	for (TIterExprObjectContainer it = beg; it != end; it++) {
		if (OBJ_LABEL == it->getText()) {
			if (it->isBool()) ss << it->getBoolValue() << " ";
			if (it->isDouble()) ss << it->getDoubleValue() << " ";
		}
		else
			ss << it->getText() << " ";
	}
	ss << "]";
	return ss.str();
}

TIterExprObjectContainer CCalculator::cutAndPaste(const TIterExprObjectContainer& from_, const TIterExprObjectContainer& to_, const CExprObject& newObj, TIterExprObjectContainer& evalBegin)
{
	TIterExprObjectContainer pos1 = expression.erase(from_, to_);

	TIterExprObjectContainer pos2 = expression.insert(pos1, newObj);

	if (evalBegin == from_)
		evalBegin = pos2;

	return pos2;
}
