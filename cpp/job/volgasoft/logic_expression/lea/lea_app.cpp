#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include "zmq_manager/ZmqManager.h"
#include "../common/utilities/CMonitor.h"
#include "iwell/LeaSettings.h"
#include "iwell/DalEquation.h"

#include "expression/exprManager.h"


int shutdownFlag=0;
void SigPipeHdlr(int signum)
{
	mon.Puts(CMonitor::NOTICE,"MAIN","-----------------SIGPIPE----------------");
}
void SigQuitHdlr(int signum)
{
	mon.Puts(CMonitor::NOTICE,"MAIN","-----------------SIGQUIT----------------");
	shutdownFlag=1;
}
void SigTermHdlr(int signum)
{
	mon.Puts(CMonitor::NOTICE,"MAIN","-----------------SIGTERM----------------");
	shutdownFlag=1;

}
void SigIntHdlr(int signum)
{
	mon.Puts(CMonitor::NOTICE,"MAIN","-----------------SIGINT----------------");
	shutdownFlag=1;

}
int main(int argc, char **argv)
{
	CmdLineSettings::init(argc, argv);

	mon.openFile(CmdLineSettings::logFileName.c_str());
	mon.readSettings(".monitor.ini");

	mon.Puts(CMonitor::NOTICE,"","\n\n\n\n\n\n\n\n\n--------------------------Start LEA-----------------------------------");
	signal(SIGPIPE, SigPipeHdlr);
	signal(SIGQUIT, SigQuitHdlr);
	signal(SIGTERM, SigTermHdlr);
	signal(SIGINT, SigIntHdlr);

	while (LeaSettings::GetSettings() && shutdownFlag==0)
	{
		sleep(1);
	}
	LeaSettings::PrintSettings();

	ZmqManager zmq;
	zmq.CreateListenerThrd();
	zmq.CreateSubscriberThrd();
	sleep(1);

	//zmq.SetSubscribeFilter(""); //subscribe on all

	ExpressionManager::startProcess();

	DalEquation::getCapabilityStatusFromDal();
	DalEquation::readEquationsFromDal();

	zmq.JoinListenerThrd();
	zmq.JoinSubscriberThrd();
	mon.Puts(CMonitor::NOTICE,"","--------------------------Stop LEA-----------------------------------");
	return 0;
}
