#include <unistd.h>
#include <signal.h>
#include "ZmqManager.h"
#include "../../common/utilities/CMonitor.h"
#include "../iwell/communicator.h"
#include "../iwell/LeaSettings.h"


void *ZmqManager::ZmqContext=NULL;

void *ZmqManager::ZmqDALSock=NULL;
void *ZmqManager::ZmqEMCSock=NULL;
void *ZmqManager::ZmqLEASock=NULL;

pthread_mutex_t DALSockMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t EMCSockMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t LeaSockMutex = PTHREAD_MUTEX_INITIALIZER;

void *ZmqManager::ZmqSubscribeSock=NULL;
void *ZmqManager::ZmqPublishSock=NULL;

pthread_mutex_t ZmqManager::ZmqPublishMutex = PTHREAD_MUTEX_INITIALIZER;

pthread_t ZmqManager::ZmqListenerThread=0;
pthread_t ZmqManager::ZmqSubscriberThread=0;


extern int shutdownFlag;

class MutexLocker
{
public:
	MutexLocker(pthread_mutex_t *m) : mutex(m) {pthread_mutex_lock(mutex);}
	~MutexLocker() {pthread_mutex_unlock(mutex);}
private:
	pthread_mutex_t *mutex;
};

pthread_mutex_t* getSockMutex(int type)
{
	switch (type)
	{
		case ZMQ_DAL_REQ:
			return &DALSockMutex;
		case ZMQ_EMC_REQ:
			return &EMCSockMutex;
		case ZMQ_LEA_REP:
			return &LeaSockMutex;
	}
	return NULL;
}

void **ZmqManager::getSocket(int type)
{
	switch (type)
	{
		case ZMQ_DAL_REQ:
			return &ZmqDALSock;
		case ZMQ_EMC_REQ:
			return &ZmqEMCSock;
		case ZMQ_PUBLISHER:
			return &ZmqPublishSock;
		case ZMQ_LEA_REP:
			return &ZmqLEASock;
		case ZMQ_SUBSCRIBER:
			return &ZmqSubscribeSock;

	}
	return NULL;
}

string ZmqManager::getEndPoint(int type)
{
	string path;
	switch (type)
	{
		case ZMQ_DAL_REQ:
			path=LeaSettings::ZmqDalEndpoint;
			break;
		case ZMQ_EMC_REQ:
			path=LeaSettings::ZmqEmcEndpoint;
			break;
		case ZMQ_PUBLISHER:
			path=LeaSettings::ZmqPublisherEndpoint;
			break;
		case ZMQ_LEA_REP:
			path=LeaSettings::ZmqLeaEndpoint;
			break;
		case ZMQ_SUBSCRIBER:
			path=LeaSettings::ZmqSubscriberEndpoint;
			break;
	}
	return path;
}
ZmqManager::ZmqManager()
{
	if (ZmqContext==NULL)
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER","Create ZMQ Context");
		ZmqContext = zmq_ctx_new();
		if (ZmqContext==NULL)
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Create ZMQ Context");
		}
	}
	else
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER","ZMQ Context is already created");
	}
}
int  ZmqManager::SetSubscribeFilter(string filter)
{
	if (ZmqSubscribeSock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error Subscribe on filter [%s], sock is NULL",filter.c_str());
		return -1;
	}
	if (zmq_setsockopt(ZmqSubscribeSock,ZMQ_SUBSCRIBE,filter.c_str(),filter.size()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error Subscribe on filter [%s], size %d",filter.c_str(),filter.size());
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Subscribe is done on filter [%s], size %d",filter.c_str(),filter.size());
	return 0;
}
int  ZmqManager::UnSetSubscribeFilter(string filter)
{
	if (ZmqSubscribeSock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error UnSubscribe on filter [%s], sock is NULL",filter.c_str());
		return -1;
	}
	if (zmq_setsockopt(ZmqSubscribeSock,ZMQ_UNSUBSCRIBE,filter.c_str(),filter.size()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error UnSubscribe on filter [%s], size %d",filter.c_str(),filter.size());
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","UnSubscribe is done on filter [%s], size %d",filter.c_str(),filter.size());
	return 0;
}
void *ZmqManager::CreateSubscribeSock(string endPoint,void **sock)
{
	if (ZmqContext==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error Create ZMQ Socket, context is NULL, path [%s]",endPoint.c_str());
		return NULL;
	}
	*sock = zmq_socket(ZmqContext,ZMQ_SUB);
	if (*sock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error Create ZMQ Socket, path [%s]",endPoint.c_str());
		return *sock;
	}
	if (zmq_connect(*sock,endPoint.c_str()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error Connect to ZMQ Socket, path [%s]",endPoint.c_str());
		zmq_close(*sock);
		*sock=NULL;
		return *sock;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Subscribed to ZMQ , path [%s]",endPoint.c_str());
	return *sock;
}
void *ZmqManager::CreatePublishSock(string endPoint,void **sock)
{
	if (ZmqContext==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Create ZMQ Publish Socket, context is NULL, path [%s]",endPoint.c_str());
		return NULL;
	}
	*sock = zmq_socket(ZmqContext,ZMQ_PUB);
	if (*sock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Create ZMQ Publish Socket, path [%s]",endPoint.c_str());
		return *sock;
	}
	if (zmq_bind(*sock,endPoint.c_str()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Bind ZMQ Publish Socket, path [%s]",endPoint.c_str());
		zmq_close(*sock);
		*sock=NULL;
		return *sock;
	}
	return *sock;
}
void *ZmqManager::CreateRequestSock(string endPoint,void **sock)
{
	if (ZmqContext==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Create ZMQ Request Socket, context is NULL, path [%s]",endPoint.c_str());
		return NULL;
	}
	*sock = zmq_socket(ZmqContext,ZMQ_REQ);
	if (*sock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Create ZMQ Request Socket, path [%s]",endPoint.c_str());
		return *sock;
	}
	if (zmq_connect(*sock,endPoint.c_str()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER","Error Connect to ZMQ Request Socket, path [%s]",endPoint.c_str());
		zmq_close(*sock);
		*sock=NULL;
		return *sock;
	}
	return *sock;
}
void *ZmqManager::CreateReplySock(string endPoint,void **sock)
{
	if (ZmqContext==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error Create ZMQ Reply Socket, context is NULL, path [%s]",endPoint.c_str());
		return NULL;
	}
	*sock = zmq_socket(ZmqContext,ZMQ_REP);
	if (*sock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error Create ZMQ Reply Socket, path [%s]",endPoint.c_str());
		return *sock;
	}
	if (zmq_bind(*sock,endPoint.c_str()))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error Bind ZMQ Reply Socket, path [%s]",endPoint.c_str());
		zmq_close(*sock);
		*sock=NULL;
		return *sock;
	}
	return *sock;
}
void *ZmqManager::ConnectTo(int endPoint,int connType,void **sock)
{
	switch (connType)
	{
		case REQ:
				return CreateRequestSock(getEndPoint(endPoint),sock);
		case REP:
				return CreateReplySock(getEndPoint(endPoint),sock);
		case PUBLISH:
				return CreatePublishSock(getEndPoint(endPoint),sock);
		case SUBSCRIBE:
				return CreateSubscribeSock(getEndPoint(endPoint),sock);
	}
	return NULL;
}
int ZmqManager::SendReq(string &msg,int endpoint)
{
	if (msg.size()==0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Msg is empty");
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Start Request");
	void ** Sock = getSocket(endpoint);
	if (Sock==NULL)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error get socket, NULL is returned");
		return -1;
	}

	pthread_mutex_t* mut = getSockMutex(endpoint);
	if (mut == NULL) {
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error get mutex, NULL is returned");
		return -1;
	}
	MutexLocker mutLocker(mut);

	if (*Sock==NULL)
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Try to connect [%s]",getEndPoint(endpoint).c_str());
		if (ConnectTo(endpoint,REQ,Sock)==NULL)
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error connect");
			return -1;
		}
	}
	int send_bytes = zmq_send(*Sock,msg.c_str(),msg.size(),0);
	if (send_bytes==0)
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Send %d bytes, msg is empty",send_bytes);
		return -1;
	}
	if (send_bytes<0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error send msg, error #%d",send_bytes);
		if (zmq_close(*Sock))
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error close ZmqLEASock");
		}
		*Sock=NULL;
		return -1;
	}
	char buf[ZMQ_MSG_SIZE];
	zmq_pollitem_t items[ZMQ_ONE_POLL_ITEM];
	items[0].socket = *Sock;
	items[0].events = ZMQ_POLLIN;
	while (zmq_poll(items,ZMQ_ONE_POLL_ITEM,ZMQ_POLL_TIMEOUT)==0)
	{
		//mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Wait for response");
		sleep(ZMQ_ATTEMPT_DELAY);
		if (shutdownFlag)
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Shutdown signal");
			return -1;
		}
	}
	if (shutdownFlag)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Shutdown signal");
		return -1;
	}
	mon.Puts(CMonitor::DEBUG,"ZMQ_MANAGER:REQUEST","Waiting zmq_recv()");
	int recv_bytes = zmq_recv(*Sock,buf,ZMQ_MSG_SIZE,0);
	if (recv_bytes==0)
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Read %d bytes, msg is empty",recv_bytes);
		return 0;
	}
	if (recv_bytes<0)
	{

		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error read msg",recv_bytes);
		if (zmq_close(*Sock))
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error close ZmqLEASock");
		}
		*Sock=NULL;
		return -1;
	}
	if (recv_bytes>ZMQ_MSG_SIZE)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:REQUEST","Error recv %d bytes more than ZMQ_MSG_SIZE",recv_bytes);
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Read %d bytes",recv_bytes);
	msg.erase();
	msg.append(buf,recv_bytes);
	mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST",msg.c_str(),msg.size());
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:REQUEST","Finish Reply Req");
	return recv_bytes;
}


int ZmqManager::Listener()
{
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Start Replying");
	while (!shutdownFlag)
	{
		if (ZmqLEASock==NULL)
		{
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Try to connect");
			if (ConnectTo(ZMQ_LEA_REP,REP,&ZmqLEASock)==NULL)
			{
				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error connect, wait %d sec between new attempt",ZMQ_ATTEMPT_DELAY);
				sleep(ZMQ_ATTEMPT_DELAY);
			}
		}
		zmq_pollitem_t items[ZMQ_ONE_POLL_ITEM];
		items[0].socket = ZmqLEASock;
		items[0].events = ZMQ_POLLIN;
		while (!shutdownFlag)
		{
			if (zmq_poll(items,ZMQ_ONE_POLL_ITEM,ZMQ_POLL_TIMEOUT)==0)
			{
				//mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Timeout read recv, timeout=%dms",ZMQ_POLL_TIMEOUT);
				continue;
			}

			char buf[ZMQ_MSG_SIZE];
			int recv_bytes = zmq_recv(ZmqLEASock,buf,ZMQ_MSG_SIZE,0);
			if (recv_bytes==0)
			{
				mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Read %d bytes, msg is empty",recv_bytes);
				continue;
			}
			if (recv_bytes<0)
			{

				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error read msg , error #%d, try reconnect to socket",recv_bytes);
				break;
			}
			if (recv_bytes>ZMQ_MSG_SIZE)
			{
				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error recv %d bytes more than ZMQ_MSG_SIZE",recv_bytes);
				continue;
			}
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Read %d[%d] bytes",recv_bytes, ZMQ_MSG_SIZE);
			string recv_buf(buf,recv_bytes);
			mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER",recv_buf.c_str(),recv_buf.size());
			string reply_buf;
			Communicator::processRequest(recv_buf, reply_buf);
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Send reply %d bytes",reply_buf.size());
			mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER",reply_buf.c_str(),reply_buf.size());
			int send_bytes = zmq_send(ZmqLEASock,reply_buf.c_str(),reply_buf.size(),0);
			if (send_bytes==0)
			{
				mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Send %d bytes, msg is empty",send_bytes);
				continue;
			}
			if (send_bytes<0)
			{

				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error send msg , error #%d, try reconnect to socket",send_bytes);
				break;
			}

		}
		if (zmq_close(ZmqLEASock))
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error close ZmqLEASock");
		}
		ZmqLEASock=NULL;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Finish LEA Listener");
	return 0;
}
void ZmqManager::Subscriber()
{
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Start Subscribing");
	while (!shutdownFlag)
	{
		if (ZmqSubscribeSock==NULL)
		{
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Try to connect");
			if (ConnectTo(ZMQ_SUBSCRIBER,SUBSCRIBE,&ZmqSubscribeSock)==NULL)
			{
				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error connect, wait %d sec between new attempt",ZMQ_ATTEMPT_DELAY);
				sleep(ZMQ_ATTEMPT_DELAY);
			}
		}
		zmq_pollitem_t items[ZMQ_ONE_POLL_ITEM];
		items[0].socket = ZmqSubscribeSock;
		items[0].events = ZMQ_POLLIN;
		while (!shutdownFlag)
		{
			int ret = zmq_poll(items,ZMQ_ONE_POLL_ITEM,ZMQ_POLL_TIMEOUT);
			if (ret==0)
			{
				//mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Timeout read recv, timeout=%dms",ZMQ_POLL_TIMEOUT);
				continue;
			}
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","ZMQ_POLL Done, ret=%d",ret);
			bool MorePartsComming=true;
			string recv_buf="[";
			while (MorePartsComming)
			{
				zmq_msg_t recv_msg;
				if (zmq_msg_init(&recv_msg))
				{
					mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error init msg, wait %d sec between new attempt",ZMQ_ATTEMPT_DELAY);
					sleep(ZMQ_ATTEMPT_DELAY); // wait before next try
					continue;
				}
				if (zmq_msg_init_size(&recv_msg,ZMQ_MSG_SIZE))
				{
					mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error alloc msg, size %d, wait %d sec between new attempt",ZMQ_MSG_SIZE,ZMQ_ATTEMPT_DELAY);
					zmq_msg_close(&recv_msg);
					sleep(ZMQ_ATTEMPT_DELAY); // wait before next try
					continue;
				}
				int recv_bytes = zmq_msg_recv(&recv_msg,ZmqSubscribeSock,0);
				if (recv_bytes==0)
				{
					mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Read %d bytes, msg is empty",recv_bytes);
					continue;
				}
				if (recv_bytes<0)
				{
					mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error read msg , error #%d, try reconnect to socket",recv_bytes);
					break;
				}
				mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Read part of msg %d bytes",recv_bytes);
				mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE",(const char*)zmq_msg_data(&recv_msg),zmq_msg_size(&recv_msg));
				recv_buf.append((const char*)zmq_msg_data(&recv_msg),recv_bytes);
				if (zmq_msg_close(&recv_msg))
				{
					mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error close zmq msg");
				}
				if (!zmq_msg_more(&recv_msg))
				{
					MorePartsComming=false;
					recv_buf.append("]");
				}
				else
				{
					recv_buf.append(",");
				}
			}
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Json of readed msg %d bytes",recv_buf.size());
			mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE",recv_buf.c_str(),recv_buf.size());
			Communicator::processSubscrMsg(recv_buf);
		}
		if (zmq_close(ZmqSubscribeSock))
		{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBE","Error close ZmqSubscribeSock");
		}
		ZmqSubscribeSock=NULL;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBE","Finish Subscribing");
}
int ZmqManager::PublishMulti(const list<string> &msg)
{
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Start Publish");

	MutexLocker mut(&ZmqPublishMutex);
	if (ZmqPublishSock==NULL)
	{
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Try to connect");
		if (ConnectTo(ZMQ_PUBLISHER,PUBLISH,&ZmqPublishSock)==NULL)	{
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:PUBLISH","Error connect");
			return -1;
		}
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Send multipart message %d", msg.size());

	for (list<string>::const_iterator it = msg.begin(); it != msg.end(); it++) {
		mon.DumpText(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH", it->c_str(), it->size());

		list<string>::const_iterator itCheck = it;
		itCheck++;
		int send_bytes;
		if (itCheck != msg.end())
			send_bytes = zmq_send(ZmqPublishSock, it->c_str(), it->size(), ZMQ_SNDMORE);
		else {	// last
			send_bytes = zmq_send(ZmqPublishSock, it->c_str(), it->size(), 0);
			//mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Last part");
		}

		if (send_bytes==0) {
			mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Write %d bytes, msg is empty",send_bytes);
			return -1;
		}
		if (send_bytes<0) {
			mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:PUBLISH","Error write msg , error #%d, try reconnect to socket",send_bytes);
			if (zmq_close(ZmqPublishSock)) {
				mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:PUBLISH","Error close ZmqPublishSock");
			}
			ZmqPublishSock=NULL;
			return -1;
		}
		mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","There was sent %d from %d", send_bytes, it->size());

	}

	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:PUBLISH","Finish Publishing");
	return 0;
}

void *SubscriberThrd(void *)
{
	ZmqManager zmq;
	zmq.Subscriber();
	return NULL;
}
void *ListenerThrd(void *)
{
	ZmqManager zmq;
	zmq.Listener();
	return NULL;
}
int ZmqManager::JoinSubscriberThrd()
{
	if (ZmqSubscriberThread==0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBER","Error join subscriber thread, thread handle is zero");
		return -1;
	}
	int rc = pthread_join(ZmqSubscriberThread,NULL);
	if (rc!=0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:SUBSCRIBER","Error join subscriber thread,ret= %d",rc);
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:SUBSCRIBER","Subscriber Join done");
	return 0;
}
int ZmqManager::JoinListenerThrd()
{
	if (ZmqListenerThread==0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error join Listener thread, thread handle is zero");
		return -1;
	}
	int rc = pthread_join(ZmqListenerThread,NULL);
	if (rc!=0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:LISTENER","Error join Listener thread, ret= %d",rc);
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:LISTENER","Listener Join done");
	return 0;
}
int  ZmqManager::CreateSubscriberThrd()
{
	pthread_attr_t thr_attr;
	if (pthread_attr_init(&thr_attr)!=0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error init pthread_attr for Subscriber");
		return -1;
	}
	if (pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_JOINABLE))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error set DETACHED state for Subscriber");
		return -1;
	}
	if (pthread_create(&ZmqSubscriberThread,&thr_attr,SubscriberThrd,NULL))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error Subscriber thread");
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:THREAD","Created Subscriber thread");
	return 0;
}

int  ZmqManager::CreateListenerThrd()
{
	pthread_attr_t thr_attr;
	if (pthread_attr_init(&thr_attr)!=0)
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error init pthread_attr for Listener");
		return -1;
	}
	if (pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_JOINABLE))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error set DETACHED state for Listener");
		return -1;
	}
	if (pthread_create(&ZmqListenerThread,&thr_attr,ListenerThrd,NULL))
	{
		mon.Puts(CMonitor::ERROR,"ZMQ_MANAGER:THREAD","Error Listener thread");
		return -1;
	}
	mon.Puts(CMonitor::NOTICE,"ZMQ_MANAGER:THREAD","Created Listener thread ");
	return 0;
}
