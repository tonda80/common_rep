#ifndef ZMQMANAGER_H_
#define ZMQMANAGER_H_

#include <zmq.h>
#include <string>
#include <list>

#define ZMQ_DAL_REQ 	1
#define ZMQ_EMC_REQ 	2
#define ZMQ_PUBLISHER 	3
#define ZMQ_LEA_REP 	4
#define ZMQ_SUBSCRIBER 	5

#define ZMQ_DAL_ENDPOINT        "ipc:///tmp/IWELL_DAL"
#define ZMQ_EMC_ENDPOINT		"ipc:///tmp/IWELL_EMC"
#define ZMQ_PUBLISHER_ENDPOINT	"ipc:///tmp/publisher"
#define ZMQ_LEA_ENDPOINT		"ipc:///tmp/IWELL_LEA"
#define ZMQ_SUBSCRIBER_ENDPOINT	"ipc:///tmp/Subscriber"


#define ZMQ_MSG_SIZE 16384
#define ZMQ_ATTEMPT_DELAY 1

#define REQ 		1
#define REP 		2
#define PUBLISH 	3
#define SUBSCRIBE 	4

#define ZMQ_RETRY_COUNT 3
#define ZMQ_POLL_TIMEOUT 5000   // timeout in ms
#define ZMQ_ONE_POLL_ITEM 1

using namespace std;

class ZmqManager
{
public:
	ZmqManager();
	int SendReq(string &msg,int endpoint);
	int Listener();
	int PublishMulti(const list<string> &msg);
	void Subscriber();
	int  SetSubscribeFilter(string filter);
	int  UnSetSubscribeFilter(string filter);
	int  CreateSubscriberThrd();
	int  CreateListenerThrd();
	int JoinListenerThrd();
	int JoinSubscriberThrd();
private:
	static void *ZmqContext;
	static void *ZmqDALSock;
	static void *ZmqEMCSock;
	static void *ZmqPublishSock;
	static void *ZmqLEASock;
	static void *ZmqSCADASock;
	static void *ZmqMotorSock;
	static void *ZmqSubscribeSock;
	static pthread_mutex_t ZmqPublishMutex;
	static pthread_t ZmqListenerThread;
	static pthread_t ZmqSubscriberThread;
	void *ConnectTo(int zmqPath,int connType,void **sock);
	void *CreateSubscribeSock(string connectionPath,void **sock);
	void *CreatePublishSock(string connectionPath,void **sock);
	void *CreateRequestSock(string connectionPath,void **sock);
	void *CreateReplySock(string connectionPath,void **sock);
	void **getSocket(int type);
	string getEndPoint(int type);
	void ProcessSubscrMsg(string recv_buf);
};

void *CreateSubscriberThrd(void *);

#endif /* ZMQMANAGER_H_ */
