#include "LeaSettings.h"
#include "../zmq_manager/ZmqManager.h"
#include "../../common/utilities/CMonitor.h"
#include "../json_parser/JsonParser.h"

#include <iostream>
#include <cstring>

string CmdLineSettings::logFileName = "/tmp/lea.log";

string LeaSettings::ZmqDalEndpoint=ZMQ_DAL_ENDPOINT;
string LeaSettings::ZmqEmcEndpoint=ZMQ_EMC_ENDPOINT;
string LeaSettings::ZmqPublisherEndpoint=ZMQ_PUBLISHER_ENDPOINT;
string LeaSettings::ZmqLeaEndpoint=ZMQ_LEA_ENDPOINT;
string LeaSettings::ZmqSubscriberEndpoint=ZMQ_SUBSCRIBER_ENDPOINT;
int LeaSettings::ZmqPollTimeOut=ZMQ_POLL_TIMEOUT;  //timeout in ms
int LeaSettings::ZmqRetryCount=ZMQ_RETRY_COUNT;

unsigned int LeaSettings::maxNumExprComponents = 40;
unsigned int LeaSettings::maxNumExpressions = 8;

int LeaSettings::GetSettings()
{
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","Start getting LEA settings");
	string msg = createDalSettingsReq();
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"LEA_SETTINGS","Error get data from zmq");
		return -1;
	}
	if (ParseDalSettingsReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"LEA_SETTINGS","Error parse dal reply");
		return -2;
	}
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","Finish getting LEA settings");
	return 0;
}
void LeaSettings::PrintSettings()
{
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqDalEndpoint        = [%s]",LeaSettings::ZmqDalEndpoint.c_str());
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqEmcEndpoint        = [%s]",LeaSettings::ZmqEmcEndpoint.c_str());
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqPublisherEndpoint  = [%s]",LeaSettings::ZmqPublisherEndpoint.c_str());
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqLeaEndpoint        = [%s]",LeaSettings::ZmqLeaEndpoint.c_str());
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqSubscriberEndpoint = [%s]",LeaSettings::ZmqSubscriberEndpoint.c_str());
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqPollTimeOut, ms    = [%d]",LeaSettings::ZmqPollTimeOut);
	mon.Puts(CMonitor::NOTICE,"LEA_SETTINGS","ZmqRetryCount         = [%d]",LeaSettings::ZmqRetryCount);
}

bool CmdLineSettings::noJsonDynData(false);

void CmdLineSettings::init(int argc, char **argv)
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--no-json-dyn-data") == 0) {
			noJsonDynData = true;
			cout << "Start test mode no-json-dyn-data" << endl;
		}
		else if (strcmp(argv[i], "-l") == 0) {
			if (i+1 >= argc)
			{
				cerr << "Empty name for log file, using default " << logFileName << endl;
			}
			else
			{
				logFileName = argv[i+1];
				i++;
			}
		}
		else if (strcmp(argv[i], "-h") == 0) {
			printHelp();
		}

		//else if (strcmp(argv[i], "") == 0)
		else {
		 cerr << "Unknown option \"" << argv[i] << "\"" << endl;
		}
	}
}

void CmdLineSettings::printHelp()
{
	cout <<	"Options:\n"
			" -h \t\t\tShow this message\n"
			" -l file \t\tSet the log file name\n"
			" --no-json-dyn-data \tTest option. Lea doesn't past a dynamic data in JSON messages\n"
			;

	exit(0);
}
