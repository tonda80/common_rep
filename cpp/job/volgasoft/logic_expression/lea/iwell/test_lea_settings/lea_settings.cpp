#include <stdio.h>
#include "../../json_parser/JsonParser.h"
#include "../../utilities/CMonitor.h"
#include "../LeaSettings.h"

using namespace std;
unsigned char shutdownFlag=0;

int main(int argc, char **argv)
{
	mon.openFile("/tmp/dal_settings.log");
	while (LeaSettings::GetSettings());
	LeaSettings::PrintSettings();
	return 0;
}
