#ifndef LEASETTINGS_H_
#define LEASETTINGS_H_

#include <string>

using namespace std;

class LeaSettings
{
public:
	static string ZmqEmcEndpoint;
	static string ZmqDalEndpoint;
	static string ZmqPublisherEndpoint;
	static string ZmqLeaEndpoint;
	static string ZmqSubscriberEndpoint;
	static int ZmqPollTimeOut;  //timeout in ms
	static int ZmqRetryCount;
	static int GetSettings();
	static void PrintSettings();
	static int AddEquationInDal();

	static unsigned int maxNumExprComponents;
	static unsigned int maxNumExpressions;
};

class CmdLineSettings
{
public:
	static void init(int argc, char **argv);
	static void printHelp();	// print help and exit(0)

	static bool noJsonDynData;	// sent json data don't have dynamic data (uniqueReqId, timestamp ..)
	static string logFileName;
};

#endif
