#include "CAction.h"
#include "../../common/utilities/CMonitor.h"
#include "../json_parser/JsonParser.h"
#include "../zmq_manager/ZmqManager.h"



string createWriteDalActionReq(const CAction& action);
int ParseDalReadActionReply(const string &msg, CAction& action);
int ParseDalWriteActionReply(const string &msg);
string createReadDalActionReq(int equationId);
string createDeleteDalActionReq(int equationId);
int ParseDalDeleteActionReply(const string &msg);
string createUpdateDalActionReq(const CAction& action);
int ParseDalUpdateActionReply(const string &msg);


int CAction::writeToDal()
{
	mon.Puts(CMonitor::NOTICE,"DalAction","Start writing action to dal");
	string msg = createWriteDalActionReq(*this);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error send data");
		return -1;
	}
	if (ParseDalWriteActionReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error parse dal reply");
		return -2;
	}
	return 0;
}
int CAction::readFromDal()
{
	mon.Puts(CMonitor::NOTICE,"DalAction","Start read action from dal");
	string msg = createReadDalActionReq(equationId);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error send data");
		return -1;
	}

	int eqId = equationId;	//  desire to play safe

	if (ParseDalReadActionReply(msg, *this))
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error parse dal read action reply");
		return -2;
	}

	if (eqId != equationId)
		return -4;

	return 0;
}
int CAction::updateInDal()
{
	mon.Puts(CMonitor::NOTICE,"DalAction","Start update action to dal");
	string msg = createUpdateDalActionReq(*this);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error send data");
		return -1;
	}
	if (ParseDalUpdateActionReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error  parse update dal reply");
		return -2;
	}
	return 0;
}
int deleteActionFromDal(int le_id)
{
	mon.Puts(CMonitor::NOTICE,"DalAction","Start read action from dal");
	string msg = createDeleteDalActionReq(le_id);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error send data");
		return -1;
	}
	if (ParseDalDeleteActionReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"DalAction","Error parse dal delete action reply");
		return -2;
	}
	return 0;
}

JSONNode actionToJSON(const CAction& act)
{
	JSONNode jAction(JSON_NODE);
	jAction.push_back(JSONNode("le_id", act.equationId));
	jAction.push_back(JSONNode("action", act.actionType));
	if (act.actionType == "TIMER") {
		jAction.push_back(JSONNode("timer_period", act.timerPeriod));
	}
	return jAction;
}

bool actionFromJSON(CAction& act, const JSONNode& jAction)
{
	if (! findJSONInt(jAction, "le_id", act.equationId)) {
		mon.Puts(CMonitor::ERROR, "CAction::fromJSON", "Error le_id");
		return false;
	}

	if (! findJSONStr(jAction, "action", act.actionType)) {
		mon.Puts(CMonitor::ERROR, "CAction::fromJSON" ,"Error action");
		return false;
	}

	if (act.actionType=="TIMER") {
		if (! findJSONInt(jAction, "timer_period", act.timerPeriod)) {
			mon.Puts(CMonitor::ERROR, "CAction::fromJSON" ,"Error timer period");
			return false;
		}
	}

	return true;
}

string createWriteDalActionReq(const CAction& action)
{
	SentRequest request("data-commit", "create_action");
	request.paramsPush(actionToJSON(action));

	return request.write();
}
string createUpdateDalActionReq(const CAction& action)
{
	SentRequest request("data-commit", "update_action");
	request.paramsPush(actionToJSON(action));

	return request.write();
}
string createDeleteDalActionReq(int equationId)
{
	SentRequest request("data-delete", "delete_action");

	request.paramsPush(jsonObject("le_id",equationId));

	return request.write();
}
string createReadDalActionReq(int equationId)
{
	SentRequest request("data-request", "get_action");

	request.paramsPush(jsonObject("le_id",equationId));

	return request.write();
}
int ParseDalWriteActionReply(const string &msg)
{
	return parseStandartOkReply(msg, "create_action");
}
int ParseDalUpdateActionReply(const string &msg)
{
	return parseStandartOkReply(msg, "update_action");
}
int ParseDalReadActionReply(const string &msg, CAction& action)
{
	Reply reply;
	try {
		Reply::deserialize(msg, reply);
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR,"JSON_PARSER:DAL_ACTION","Error parse reply %s", e.text.c_str());
		return -1;
	}

	if (!reply.checkValueFormat("get_action"))
		return -1;

	if (! actionFromJSON(action, reply.payload()))
		return -1;

	return 0;
}
int ParseDalDeleteActionReply(const string &msg)
{
	return parseStandartOkReply(msg, "delete_action");
}

