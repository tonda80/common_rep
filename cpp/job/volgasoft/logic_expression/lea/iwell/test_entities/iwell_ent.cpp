#include <string>
#include <list>
#include "../IWellEntities.h"
#include "../../utilities/CMonitor.h"
#include "../communicator.h"

int shutdownFlag;
int main(int argc, char **argv)
{
	mon.openFile("/tmp/emc_request.log");
	std::string st;
	std::list<string> lst;
	st="digital_input-Interpretation-1";
	lst.push_back(st);
	st="digital_input-Interpretation-2";
	lst.push_back(st);
	st="digital_input-Interpretation-3";
	lst.push_back(st);
	st="digital_input-Interpretation-4";
	lst.push_back(st);
	st="digital_input-Interpretation-5";
	lst.push_back(st);
	Communicator::requestEmcData(lst);
  return 0;
}
