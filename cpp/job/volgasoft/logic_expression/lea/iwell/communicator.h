/*
 * communicator.h
 *
 *  Created on: Dec 11, 2013
 *      Author: ant
 */

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include <string>
#include <list>

#define NDEBUG
#include <libjson/libjson.h>


using namespace std;

class Request;
class RequestCreateLogicalEquation;
class Expression;

class Communicator
{
public:
	static void processRequest(const string& stReq, string& stReply);
	static int requestEmcData(list<string> &interpretations);
	static int requestOneEmcInterpretation(const string &interpretName);
	static void publishCalculationResult(const Expression& expr, bool value);
	static void publishCalculationError(int id, const string& resultant);
	static void reportDisabledInterpretations(int id, const list<string>& invalidObjs);
	static int processSubscrMsg(const string &msg);

	static void addLogicalEquation(const RequestCreateLogicalEquation& crReq, JSONNode &reply, bool fromReq = true);

	static void EMCWriteOutput(const string name, bool value);
private:
	static int processEmcStatusReply(const string &msg,const string &interpretation);
	static int processEmcIoReply(const string &msg,const string &interpretations);
	static int writeRealTimeData(const string& name,double val);
};


#endif /* COMMUNICATOR_H_ */
