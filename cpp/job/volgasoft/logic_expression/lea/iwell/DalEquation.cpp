#include <stdio.h>
#include "DalEquation.h"
#include "../zmq_manager/ZmqManager.h"
#include "../../common/utilities/CMonitor.h"
#include "communicator.h"
#include "../expression/exprManager.h"

#include "CAction.h"


int DalEquation::writeEquationToDal(const RequestCreateLogicalEquation &equation)
{
	mon.Puts(CMonitor::NOTICE,"DalEquation","Start writing equation to dal");
	string msg = createWriteDalEquationReq(equation.getParam());
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error send data");
		return -1;
	}
	if (ParseDalWriteEquationsReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error parse dal reply");
		return -2;
	}
	return 0;
}
int DalEquation::readEquationsFromDal()
{
	mon.Puts(CMonitor::NOTICE,"DalEquation","Start reading equation from dal");
	string msg = createReadDalEquationReq();
	mon.Puts(CMonitor::NOTICE,"DalEquation","ZMQ msg to read equations %s",msg.c_str());
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error send data");
		return -1;
	}

	list<LogicalEquation> lstEquations;
	if (ParseDalReadEquationsReply(msg ,lstEquations))
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error parse dal reply");
		return -2;
	}

	for (list<LogicalEquation>::iterator it = lstEquations.begin(); it != lstEquations.end(); it++) {
		JSONNode reply;	// fake reply. For using one function of addition
		Communicator::addLogicalEquation((*it), reply, false);

		CAction act(it->le_id);
		if (act.readFromDal() == 0 && ExpressionManager::getInstance().hasId(act.equationId)) {
			ExpressionManager::getInstance()[act.equationId].setAction(act);
		}
	}

	return 0;
}

int DalEquation::deleteEquationFromDal(int equationId)
{
	mon.Puts(CMonitor::NOTICE,"DalEquation","Start delete equation from dal");
	string msg = createDeleteDalEquationReq(equationId);
	mon.Puts(CMonitor::NOTICE,"DalEquation","ZMQ msg to delete %d equation %s",equationId,msg.c_str());
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error send data");
		return -1;
	}
	if (ParseDalDeleteEquationsReply(msg))
	{
		mon.Puts(CMonitor::ERROR,"DalEquation","Error parse dal reply");
		return -2;
	}
	return 0;
}

int DalEquation::updateCapabilityStatusToDal(bool status)
{
	mon.Puts(CMonitor::NOTICE, "DalEquation","writeCapabilityStatusToDal()");
	string msg = createUpdateCapabilityStatusToDalReq(status);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error send data");
		return -1;
	}
	if (parseStandartOkReply(msg, "update_logical_expression_capability_status") != 0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error parse dal reply");
		return -2;
	}
	return 0;
}

int DalEquation::getCapabilityStatusFromDal()
{
	mon.Puts(CMonitor::NOTICE, "DalEquation","getCapabilityStatusFromDal()");
	string msg = createGetCapabilityStatusFromDalReq();
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error send data");
		return -1;
	}
	if (parseGetCapabilityStatusFromDalReply(msg, ExpressionManager::getInstance().capabilityStatus) != 0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error parse dal reply");
		return -2;
	}

	mon.Puts(CMonitor::NOTICE, "DalEquation", "capability status was updated from DAL %d", ExpressionManager::getInstance().capabilityStatus);
	return 0;
}

int DalEquation::updateDownTimeToDal(const string &mode,const int &time)
{
	mon.Puts(CMonitor::NOTICE, "DalEquation","writedowntimeToDal()");
	string msg = createUpdateDownTimeToDalReq(mode,time);
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0)
	{
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error send data");
		return -1;
	}
	if (parseStandartOkReply(msg, "update_downtime") != 0)
	{
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error parse dal reply");
		return -2;
	}
	return 0;
}

int DalEquation::getDownTimeFromDal()
{
	mon.Puts(CMonitor::NOTICE, "DalEquation","getDownTimeFromDal()");
	string msg = createGetDownTimeFromDalReq();
	ZmqManager zmq;
	if (zmq.SendReq(msg,ZMQ_DAL_REQ)<=0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error send data");
		return -1;
	}
	if (parseGetDownTimeFromDalReply(msg,ExpressionManager::getInstance().DownTimeMode,ExpressionManager::getInstance().DownTime) != 0) {
		mon.Puts(CMonitor::ERROR, "DalEquation", "Error parse dal reply");
		return -2;
	}

	mon.Puts(CMonitor::NOTICE, "DalEquation", "downtime was updated from DAL mode=%s,time=%d",
			ExpressionManager::getInstance().DownTimeMode.c_str(),ExpressionManager::getInstance().DownTime);
	return 0;
}
