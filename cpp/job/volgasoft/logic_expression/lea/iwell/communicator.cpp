/*
 * communicator.cpp
 *
 *  Created on: Dec 11, 2013
 *      Author: ant
 */
#include "communicator.h"
#include "../expression/exprManager.h"
#include "../zmq_manager/ZmqManager.h"
#include "../../common/utilities/CMonitor.h"
#include "../json_parser/JsonParser.h"
#include "DalEquation.h"
#include "LeaSettings.h"
#include "IWellEntities.h"
#include "CAction.h"


typedef bool(*TRequestProcessers)(const Request&, JSONNode&);

bool processVerifyLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processCreateLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processUpdateLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processDeleteLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processGetLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processUpdateCapabilityStatusRequest(const Request& request, JSONNode &reply);
bool processGetCapabilityStatusRequest(const Request& request, JSONNode &reply);
bool processCreateActionRequest(const Request& request, JSONNode &reply);
bool processUpdateActionRequest(const Request& request, JSONNode &reply);
bool processDeleteActionRequest(const Request& request, JSONNode &reply);
bool processGetActionRequest(const Request& request, JSONNode &reply);
bool processClearLogicalEquationRequest(const Request& request, JSONNode &reply);
bool processGetDownTimeRequest(const Request& request, JSONNode &reply);
bool processUpdateDownTimeRequest(const Request& request, JSONNode &reply);


TRequestProcessers requestProcessers[] = {
		processVerifyLogicalEquationRequest,
		processCreateLogicalEquationRequest,
		processUpdateLogicalEquationRequest,
		processDeleteLogicalEquationRequest,
		processGetLogicalEquationRequest,
		processClearLogicalEquationRequest,
		processUpdateCapabilityStatusRequest,
		processGetCapabilityStatusRequest,
		processCreateActionRequest,
		processUpdateActionRequest,
		processDeleteActionRequest,
		processGetActionRequest,
		processUpdateDownTimeRequest,
		processGetDownTimeRequest
};
int numProcessers = sizeof(requestProcessers)/sizeof(TRequestProcessers);

const string errorReply = "Unsuccessful";
const string okReply = "Ok";

namespace {
	ZmqManager zmqMgr;
}

// function tries to create Expression object from RequestCreateLogicalEquation and does its verification
// If ok returns the correct Expression, if not returns the empty Expression and set reply
Expression createCorrectExpression(const RequestCreateLogicalEquation& crReq, JSONNode &reply);

void Communicator::processRequest(const string& stReq, string& stReply)
{
	mon.Puts(CMonitor::NOTICE,"Communicator::processRequest", "Start processing request");

	list<Request> lstReqs;
	list<JSONNode> replyParts;

	try {
		Request::deserialize(stReq, lstReqs);

		for (list<Request>::iterator itReq = lstReqs.begin(); itReq != lstReqs.end(); itReq++) {
			JSONNode reply(JSON_ARRAY);

			mon.Puts(CMonitor::NOTICE, "Communicator::processRequest", "Process request [%s %s]", itReq->method.c_str(), itReq->value.c_str());

			bool unknownReq = true;
			for (int i = 0; i < numProcessers; i++) {
				if (requestProcessers[i](*itReq, reply)) {
					unknownReq = false;
					break;	// request was processed
				}
			}

			if (unknownReq)
				setReply(reply, errorReply, "Unknown request");		// form error about unknown request

			replyParts.push_back(reply);
		}
	}
	catch (CJSONException& e) {
		mon.Puts(CMonitor::ERROR, "Communicator::processRequest", "CJSONException. %s. %d", e.text.c_str(), e.type);

		stringstream ss;
		ss << "{\"values\":[[\"\",\"Unsuccessful\",\"";
		ss << e.text << " [" << e.type << "]";
		ss << "\"]]}";

		stReply = ss.str();
		return;
	}

	// To form a common reply
	stReply = createReply(lstReqs, replyParts);
	mon.Puts(CMonitor::BLAB, "Communicator::processRequest", "Reply is ready\n%s", stReply.c_str());

}
bool processCreateLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "create_logical_equation")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	RequestCreateLogicalEquation crReq = RequestCreateLogicalEquation::deserialize(request);

	// Check existence
	if (ExpressionManager::getInstance().hasId(crReq.le_id))
		setReply(reply, errorReply, "Id exists");
	else
		Communicator::addLogicalEquation(crReq, reply);

	return true;
}

bool processVerifyLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-verify" || request.value != "verify_logical_equation")
		return false;

	RequestCreateLogicalEquation vReq = RequestCreateLogicalEquation::deserialize(request);

	// Check existence
	if (ExpressionManager::getInstance().hasId(vReq.le_id)) {
		setReply(reply, errorReply, "Id exists");
		return true;
	}

	Expression newExpr = createCorrectExpression(vReq, reply);
	if (! newExpr.isEmpty() ) {			// error log and reply into createCorrectExpression() already
		setReply(reply, okReply, "");
	}

	return true;
}

bool processDeleteLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-delete" || request.value != "delete_logical_equation")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	RequestDeleteLogicalEquation dReq = RequestDeleteLogicalEquation::deserialize(request);

	if (ExpressionManager::getInstance().removeExpression(dReq.le_id) == 0)
		setReply(reply, okReply, "");
	else
		setReply(reply, errorReply, "Cannot remove");

	if (DalEquation::deleteEquationFromDal(dReq.le_id) != 0 || deleteActionFromDal(dReq.le_id) != 0) {
		setReply(reply, errorReply, "DAL error");
	}

	return true;
}

bool processUpdateLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "update_logical_equation")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	RequestCreateLogicalEquation updReq = RequestCreateLogicalEquation::deserialize(request);	// update and create requests are identical
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Update expression [%d]", updReq.le_id);

	// Check existence
	if (! ExpressionManager::getInstance().hasId(updReq.le_id)) {
		setReply(reply, errorReply, "No id");
		return true;
	}

	// Test of possibility of addition. It prevents a removal of existing expression.
	Expression tstExpr = createCorrectExpression(updReq, reply);
	if ( tstExpr.isEmpty() ) {
		// error log and reply into createCorrectExpression() already
		return true;
	}

	CAction tmpAction =  ExpressionManager::getInstance()[updReq.le_id].getAction();

	if (ExpressionManager::getInstance().removeExpression(updReq.le_id) != 0) {
		setReply(reply, errorReply, "Cannot remove");
		return true;
	}

	if (DalEquation::deleteEquationFromDal(updReq.le_id) != 0) {
		setReply(reply, errorReply, "No removal from DAL");
		return true;
	}

	Communicator::addLogicalEquation(updReq, reply);

	ExpressionManager::getInstance()[updReq.le_id].setAction(tmpAction);

	return true;
}

Expression createCorrectExpression(const RequestCreateLogicalEquation& crReq, JSONNode &reply)
{
	Expression newExpr;
	string retStr = newExpr.fromStruct(crReq);
	if (retStr != "") {
		mon.Puts(CMonitor::ERROR, "ExpressionManager", "Cannot form expression");
		setReply(reply, errorReply, retStr);
		return Expression();
	}

	// Check syntax of created expression
	retStr = newExpr.syntaxValidate();
	if (retStr != "") {
		mon.Puts(CMonitor::ERROR, "ExpressionManager", "Syntax validate error %s", retStr.c_str());
		setReply(reply, errorReply, retStr);
		return Expression();
	}

	// Hardware validation
	;
	if (Communicator::requestEmcData(newExpr.iWellObjs) != 0 || Communicator::requestOneEmcInterpretation(newExpr.resultant) != 0) {
		mon.Puts(CMonitor::ERROR, "ExpressionManager", "No EMC validation");
		setReply(reply, errorReply, "No EMC validation");
		return Expression();
	}

	// IO Direction validation
	int res = newExpr.validateIoDirection();
	if (res != 0)
	{
		mon.Puts(CMonitor::ERROR, "ExpressionManager", "No EMC IO validation");
		setReply(reply, errorReply, "No EMC IO validation");
		return Expression();
	}

	return newExpr;
}



void Communicator::addLogicalEquation(const RequestCreateLogicalEquation& crReq, JSONNode &reply, bool fromReq/* = true*/)
{
	ExpressionManager& exprMgr = ExpressionManager::getInstance();

	// idiocy requirement
	if (exprMgr.expressionNumber() >= LeaSettings::maxNumExpressions) {
		mon.Puts(CMonitor::ERROR, "ExpressionManager", "Expressions are too much already (%d)", exprMgr.expressionNumber());
		setReply(reply, errorReply, "Expressions are too much already");
		return;
	}

	Expression newExpr = createCorrectExpression(crReq, reply);
	if (newExpr.isEmpty() ) {
		// log and reply into createCorrectExpression() already
		return;
	}

	// check of interpretations
	if (!newExpr.checkIWellObjs()) {
		string stInvalidObjs;
		for (list<string>::iterator it = newExpr.invalidIWellObjs.begin(); it != newExpr.invalidIWellObjs.begin(); it++)
			stInvalidObjs += *it + " ";
		setReply(reply, errorReply, "Disabled_interpretations "+stInvalidObjs);
		return;
	}

	// Send to DAL
	if (fromReq) {
		if (DalEquation::writeEquationToDal(crReq) != 0) {
			setReply(reply, errorReply, "No addition to DAL");
			return;
		}
	}

	if (exprMgr.addExpression(newExpr) == 0)
		setReply(reply, okReply, "");
	else
		setReply(reply, errorReply, "No addition to exprMgr");
}

void Communicator::publishCalculationResult(const Expression& expr, bool value)
{
	list<string> publMsg;
	createPublishCalculationResultMsg(expr.id, expr.resultant, expr.getAction().actionType, value, publMsg);
	zmqMgr.PublishMulti(publMsg);
}
void Communicator::publishCalculationError(int id, const string& resultant)
{
	list<string> publMsg;
	createPublishErrorResultMsg(id, resultant, publMsg);
	zmqMgr.PublishMulti(publMsg);
}

void Communicator::reportDisabledInterpretations(int id, const list<string>& invalidObjs)
{
	list<string> publMsg;
	createDisabledInterpretationsMsg(id, invalidObjs, publMsg);
	zmqMgr.PublishMulti(publMsg);
}

int Communicator::processEmcStatusReply(const string &msg,const string &interpretation)
{
	JSONNode value;
	if (parseEmcHeader(msg,"Status",value)==-1)
	{
		return -1;
	}
	if (parseEmcStatusData(value[1].as_array()[0].as_node(),interpretation)==-1)
	{
		return -1;
	}
	return 0;
}
int Communicator::processEmcIoReply(const string &msg,const string &interpretation)
{
	JSONNode value;
	if (parseEmcHeader(msg,"Get_EMC_Interpretations",value)==-1)
	{
		return -1;
	}
	if (parseEmcIoData(value.as_array()[1],interpretation)==-1)
	{
		return -2;
	}
	return 0;
}
int Communicator::requestOneEmcInterpretation(const string &interpretName)
{
//-------- read io status----------
	  string msg = createEmcStatusReq(interpretName);
	  mon.Puts(CMonitor::NOTICE,"COMMUNICATOR:EMC_REQ","Created emc status request");
	  mon.DumpText(CMonitor::NOTICE,"COMMUNICATOR:EMC_REQ",msg.c_str(),msg.size());
	  ZmqManager zmq;
	  if (zmq.SendReq(msg,ZMQ_EMC_REQ)<=0)
	  {
		mon.Puts(CMonitor::ERROR,"COMMUNICATOR:EMC_REQ","Error get status data from zmq");
		return -1;
	  }
	  if (processEmcStatusReply(msg,interpretName))
	  {
		mon.Puts(CMonitor::ERROR,"COMMUNICATOR:EMC_REQ","Error process emc status reply");
		return -1;
	  }
//-------- read io state----------
	  msg = createEmcIoReq(interpretName);
	  mon.Puts(CMonitor::NOTICE,"COMMUNICATOR:EMC_REQ","Created emc IO request");
	  mon.DumpText(CMonitor::NOTICE,"COMMUNICATOR:EMC_REQ",msg.c_str(),msg.size());
	  if (zmq.SendReq(msg,ZMQ_EMC_REQ)<=0)
	  {
		mon.Puts(CMonitor::ERROR,"COMMUNICATOR:EMC_REQ","Error get io data from zmq");
		return -1;
	  }
	  if (processEmcIoReply(msg,interpretName))
	  {
		mon.Puts(CMonitor::ERROR,"COMMUNICATOR:EMC_REQ","Error process emc io reply");
		return -1;
	  }
	return 0;
}
int Communicator::requestEmcData(list<string> &interpretations)
{
	list<string>::iterator interprItem = interpretations.begin();
	for (;interprItem!=interpretations.end();interprItem++)
	{
		if (requestOneEmcInterpretation(*interprItem)!=0)
		{
			return -1;
		}
	}
	return 0;
}
int Communicator::writeRealTimeData(const string& name,double val)
{
	mon.Puts(CMonitor::NOTICE,"COMMUNICATOR:SUBSCRIBE","Write [%s]=[%f] to map",name.c_str(),val);
	IWellEntities iwellEntities = IWellEntities::getInstance();
	if (! iwellEntities.hasEntity(name))	// maybe it's unnecessary
	{
		mon.Puts(CMonitor::ERROR,"COMMUNICATOR:SUBSCRIBE","Entity [%s] not found in map",name.c_str());
		return -1;
	}
	HWInterpretation entity;
	entity.value = val;
	entity.enabled=true;
	entity.valid=true;
	iwellEntities.setEntity(name,entity);
	return 0;
}

int Communicator::processSubscrMsg(const string &msg)
{
	string name;
	double val;
	if (processSubscrJson(msg,name,val))
	{
		return -1;
	}
	if (writeRealTimeData(name,val))
	{
		return -1;
	}

	ExpressionManager::getInstance().reactionTo(name);

	return 0;
}

bool processGetLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-request" || request.value != "get_logical_equation")
		return false;

	RequestDeleteLogicalEquation gReq = RequestDeleteLogicalEquation::deserialize(request);		// delete and get requests are identical
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Get expression [%d]", gReq.le_id);

	ExpressionManager& exprMgr = ExpressionManager::getInstance();

	if (! exprMgr.hasId(gReq.le_id)) {
		setReply(reply, errorReply, "No id");
		return true;
	}

	JSONNode jExpr;
	if (exprMgr[gReq.le_id].toJSON(jExpr)) {
		reply.push_back(jExpr);
		reply.push_back(JSONNode("", ""));
	}
	else {
		setReply(reply, errorReply, "Error of serialization");
	}

	return true;
}

bool processClearLogicalEquationRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "clear_logical_equation")
		return false;

	RequestDeleteLogicalEquation gReq = RequestDeleteLogicalEquation::deserialize(request);		// delete and clear requests are identical
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Clear expression [%d]", gReq.le_id);

	ExpressionManager& exprMgr = ExpressionManager::getInstance();

	if (! exprMgr.hasId(gReq.le_id)) {
		setReply(reply, errorReply, "No id");
		return true;
	}

//	if (! exprMgr[gReq.le_id].clearLatched()) {
//		setReply(reply, errorReply, "");
//		return true;
//	}
	exprMgr[gReq.le_id].clearLatched();		// no error (Question 15)


	setReply(reply, okReply, "");
	return true;
}

bool processUpdateCapabilityStatusRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "update_logical_expression_capability_status")
		return false;

	bool status;
	if (parseUpdateCapabilityStatus(request, status)) {
		ExpressionManager::getInstance().capabilityStatus = status;
		mon.Puts(CMonitor::NOTICE, "ExpressionManager", "Capability status is updated %d", ExpressionManager::getInstance().capabilityStatus);

		if (DalEquation::updateCapabilityStatusToDal(status) != 0) {
			setReply(reply, errorReply, "No DAL update");
		}
		else
			setReply(reply, okReply, "");

	}
	else {
		setReply(reply, errorReply, "");
	}

	return true;
}

bool processGetCapabilityStatusRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-request" || request.value != "get_logical_expression_capability_status")
		return false;

	reply.push_back(jsonObject("status", ExpressionManager::getInstance().capabilityStatus));
	reply.push_back(JSONNode("", ""));

	return true;
}

bool processCreateActionRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "create_action")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	CAction newAction;
	if (! actionFromJSON(newAction, request.getParam())) {
		setReply(reply, errorReply, "JSON error");
		return true;
	}

	ExpressionManager& exprMgr = ExpressionManager::getInstance();
	string ret = exprMgr.checkAction(newAction);
	if (ret != "") {
		setReply(reply, errorReply, ret);
		return true;
	}

	if (newAction.writeToDal() != 0) {
		setReply(reply, errorReply, "DAL error");
		return true;
	}

	exprMgr[newAction.equationId].setAction(newAction);
	setReply(reply, okReply, "");

	return true;
}

bool processUpdateActionRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "update_action")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}

	CAction updAction;
	if (! actionFromJSON(updAction, request.getParam())) {
		setReply(reply, errorReply, "JSON error");
		return true;
	}
	
	ExpressionManager& exprMgr = ExpressionManager::getInstance();
	string ret = exprMgr.checkAction(updAction);
	if (ret != "") {
		setReply(reply, errorReply, "No equation");
		return true;
	}

	if (updAction.updateInDal() != 0) {
		setReply(reply, errorReply, "DAL error");
		return true;
	}

	exprMgr[updAction.equationId].setAction(updAction);
	setReply(reply, okReply, "");

	return true;
}

bool processDeleteActionRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-delete" || request.value != "delete_action")
		return false;

	if (! ExpressionManager::getInstance().capabilityStatus) {
		setReply(reply, errorReply, "Capability status is disabled");
		return true;
	}


	int delId;
	if (! findJSONInt(request.getParam(), "le_id", delId)) {
		setReply(reply, errorReply, "JSON error");
		return true;
	}

	ExpressionManager& exprMgr = ExpressionManager::getInstance();
	if (! exprMgr.hasId(delId)) {
		setReply(reply, errorReply, "No equation");
		return true;
	}

	exprMgr[delId].deleteAction();
	setReply(reply, okReply, "");

	if (deleteActionFromDal(delId) != 0) {
		setReply(reply, errorReply, "DAL error");
	}

	return true;
}

bool processGetActionRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-request" || request.value != "get_action")
		return false;

	int gotId;
	if (! findJSONInt(request.getParam(), "le_id", gotId)) {
		setReply(reply, errorReply, "JSON error");
		return true;
	}

	ExpressionManager& exprMgr = ExpressionManager::getInstance();
	if (! exprMgr.hasId(gotId)) {
		setReply(reply, errorReply, "No equation");
		return true;
	}

	CAction action = exprMgr[gotId].getAction();
	if (action.actionType == "") {
		setReply(reply, errorReply, "No action");
	}
	else {
		reply.push_back(actionToJSON(action));
		reply.push_back(JSONNode("", ""));
	}

	return true;
}

bool processUpdateDownTimeRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-commit" || request.value != "update_downtime")
		return false;

	int  time;
	string mode;
	if (!parseUpdateDownTime(request,mode,time))
	{
		setReply(reply, errorReply, "");
		return true;
	}

	if (DalEquation::updateDownTimeToDal(mode,time) != 0)
	{
		setReply(reply, errorReply, "No DAL update");
		return true;
	}

	ExpressionManager::getInstance().DownTimeMode = mode;
	ExpressionManager::getInstance().DownTime = time;
	mon.Puts(CMonitor::NOTICE, "ExpressionManager", "DownTime updated mode=%s, time=%d",
			ExpressionManager::getInstance().DownTimeMode.c_str(),ExpressionManager::getInstance().DownTime);
	setReply(reply, okReply, "");
	return true;
}

bool processGetDownTimeRequest(const Request& request, JSONNode &reply)
{
	if (request.method != "data-request" || request.value != "get_downtime_mode")
		return false;

	reply.push_back(jsonObject("mode", ExpressionManager::getInstance().DownTimeMode));
	reply.push_back(jsonObject("downtime", ExpressionManager::getInstance().DownTime));

	reply.push_back(JSONNode("", ""));

	return true;
}

void Communicator::EMCWriteOutput(const string name, bool value)
{
	mon.Puts(CMonitor::NOTICE, "EMC", "EMCWriteOutput %s %d", name.c_str(), value);

	EMCSentRequest request("Port_write");
	request.pushInValue0(JSONNode("", name));
	request.pushInValue0(JSONNode("", value));
	string msg = request.write();

	ZmqManager zmq;
	if (zmq.SendReq(msg, ZMQ_EMC_REQ)<=0) {
		mon.Puts(CMonitor::ERROR, "EMC", "Error send data");
	}

	// reply doesn't interest us
}
