
#ifndef IWELLENTITIES_H_
#define IWELLENTITIES_H_

#include <string>
#include <map>

using namespace std;

class HWInterpretation
{
	friend class IWellEntities;
public:
	HWInterpretation() : value(0), enabled(false), valid(false), subscrCount(0) {}
	string name;
	string port_type;
	double value;

	operator bool() {return value;}
	operator double() {return value;}

	bool ready() {return enabled && valid;}

	bool enabled;
	bool valid;
private:
	unsigned int subscrCount;
};

class IWellEntities
{

public:
	static IWellEntities& getInstance();
	int setEntity(const string &name, const HWInterpretation &interpretation);
	HWInterpretation getEntity(const string &name);
	bool hasEntity(const string &name);

	int subscribe(const string &name);		//, const string &type = ""
	int unsubscribe(const string &name,const string &type);

	static int checkEntitiesValidity();

	void printAll();	// debug function
private:
	IWellEntities(){};
	static map<string, HWInterpretation> entities;
};

string alarmName(const string& interpName, const string& alarmType);
bool isAlarmName(const string& interpName);


#endif /* IWELLENTITIES_H_ */

