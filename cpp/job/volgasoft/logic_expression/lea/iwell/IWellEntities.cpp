#include "../../common/utilities/CMonitor.h"
#include "IWellEntities.h"

#include "../zmq_manager/ZmqManager.h"
#include "../iwell/communicator.h"


map<string, HWInterpretation> IWellEntities::entities;

IWellEntities& IWellEntities::getInstance()
{
	static IWellEntities instance;
	return instance;
}


int IWellEntities::setEntity(const string &name, const HWInterpretation &interpretation)
{
	entities[name] = interpretation;
	mon.Puts(CMonitor::NOTICE,"IWELL_ENTITY"," [%s] Successfully added in the map",name.c_str());
	return 0;
}
HWInterpretation IWellEntities::getEntity(const string &name)
{
	map<string, HWInterpretation>::iterator it = entities.find(name);

	if (it ==entities.end()) {
		return HWInterpretation();	// return disabled and invalid object
	}

	return it->second;
}

bool IWellEntities::hasEntity(const string &name)
{
	if (entities.find(name) ==entities.end()) {
		return false;
	}
	return true;
}

void IWellEntities::printAll()
{
	mon.Puts(CMonitor::NOTICE, "IWellEntities", "IWellEntities::printAll() %d", entities.size());
	for (map<string, HWInterpretation>::iterator it = entities.begin(); it!=entities.end(); it++)
		mon.Puts(CMonitor::NOTICE, "IWellEntities", "%s %f",it->first.c_str(), it->second.value);
}

int IWellEntities::subscribe(const string &name)
{
	// TODO check existence
	entities[name].subscrCount++;

	string type;
	if (isAlarmName(name)) {
		type = "alarm";
	}
	else {
		type = "data";
	}
	string pref = "[\"" + type + "\",\"" + name;

	ZmqManager zmq;
	return zmq.SetSubscribeFilter(pref);
}

int IWellEntities::unsubscribe(const string &name,const string &type)
{
	entities[name].subscrCount--;
	if (entities[name].subscrCount > 0)
		return 100;
	ZmqManager zmq;
	// TODO bug, alarm unsubscription
	string pref = "[\""+type+"\",\""+name;
	return zmq.UnSetSubscribeFilter(pref);
}

const string highAlarmSuffix("-high_alarm");
const string lowAlarmSuffix("-low_alarm");

string alarmName(const string& interpName, const string& alarmType)
{
	if (alarmType == "LOW") {
		return interpName + lowAlarmSuffix;
	}
	else if (alarmType == "HIGH") {
		return interpName + highAlarmSuffix;
	}

	return ""; // error
}

// helper function
bool strEndsAt(const string& src, const string& tail)
{
	return src.find(tail, src.size() - tail.size()) != string::npos;
}

bool isAlarmName(const string& interpName)
{
	return strEndsAt(interpName, lowAlarmSuffix) || strEndsAt(interpName, highAlarmSuffix);
}

int IWellEntities::checkEntitiesValidity()
{
	mon.Puts(CMonitor::NOTICE, "IWellEntities:EMC_PERIOD_REQ", "Start update entities");
	map<string, HWInterpretation>::iterator entity = entities.begin();
	for (; entity!=entities.end(); entity++)
	{
		if (Communicator::requestOneEmcInterpretation(entity->first)!=0)
		{
			mon.Puts(CMonitor::NOTICE, "IWellEntities:EMC_PERIOD_REQ", "Error update entities");
			return -1;
		}
	}
	mon.Puts(CMonitor::NOTICE, "IWellEntities:EMC_PERIOD_REQ", "Finished update entities");
	return 0;
}
