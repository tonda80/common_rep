#ifndef DALEQUATION_H_
#define DALEQUATION_H_

#include <list>
#include "../json_parser/JsonParser.h"
using namespace std;

class DalEquation
{
public:
	static int writeEquationToDal(const RequestCreateLogicalEquation &equation);
	static int readEquationsFromDal();
	static int deleteEquationFromDal(int equationId);

	static int updateCapabilityStatusToDal(bool status);
	static int getCapabilityStatusFromDal();
	static int updateDownTimeToDal(const string &mode,const int &time);
	static int getDownTimeFromDal();
};

#endif
