#ifndef CACTION_H_
#define CACTION_H_
#include <string>

using namespace std;

class JSONNode;

class CAction
{
public:
	CAction() : equationId(-1), timerPeriod(-1) {}
	CAction(int id) : equationId(id), timerPeriod(-1) {}
	CAction(int id, string type) : actionType(type), equationId(id), timerPeriod(-1) {}
	CAction(int id, int period)  : actionType("TIMER"), equationId(id), timerPeriod(period) {}

	string actionType;
	int equationId;
	int timerPeriod;

	int writeToDal();
	int readFromDal();
	int updateInDal();
};

JSONNode actionToJSON(const CAction& act);
bool actionFromJSON(CAction& act, const JSONNode& jAction);

int deleteActionFromDal(int le_id);

#endif /* CACTION_H_ */
