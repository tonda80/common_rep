/*
 * rcaEmc.h
 *
 *  Created on: Mar 4, 2014
 *      Author: ant
 */

#ifndef RCAEMC_H_
#define RCAEMC_H_

#include <string>

namespace RcaEmc
{

void checkAllObjects();

double getValue(const std::string& name);		// Function may throw AppException
void setValue(const std::string& name, double value);

bool has(const std::string& name);

bool isBad(const std::string& name);

void subscribe(const std::string& name);
void unsubscribe(const std::string& name);

}// end of namespace RcaEmc


#endif /* RCAEMC_H_ */
