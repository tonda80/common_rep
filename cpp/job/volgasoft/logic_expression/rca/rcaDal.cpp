/*
 * rcaDal.cpp
 *
 *  Created on: Feb 21, 2014
 *      Author: ant
 */

#include "rcaDal.h"
#include "regCalculation.h"
#include "calculationManager.h"
#include "rcaSettings.h"
#include "../common/json/iWellJSON.h"
#include "../common/zmq/ZmqManager.h"
#include "../common/utilities/CMonitor.h"
#include "../common/appException.h"

namespace RcaDal
{

ZmqManager zmq;
inline void zmqSendToDal(string& msg)
{
	if (zmq.SendReq(msg, ZMQ_DAL_REQ) <= 0)
		throw AppException("DAL ZMQ send error");
}

JSONNode calculationKey(int id)
{
	JSONNode key;
	key.push_back(JSONNode("type", "register_calculation"));
	key.push_back(JSONNode("id", id));

	return key;
}

void addCalculation(const RegCalculation& calc)
{
	mon.Puts(CMonitor::NOTICE, "RCA_DAL", "Add calculation [%d]", calc.calculation_id);

	DalSentRequest req("data-insert");

	JSONNode key = calculationKey(calc.calculation_id);
	JSONNode jCalc;	calc.toJSON(jCalc);
	req.pushParam("rca_calculations", key, jCalc);

	string msg = req.write();
	zmqSendToDal(msg);

	DalReply rep(msg);
}

void removeCalculation(int id)
{
	mon.Puts(CMonitor::NOTICE, "RCA_DAL", "Remove calculation [%d]", id);

	DalSentRequest req("data-delete");

	JSONNode key = calculationKey(id);
	req.pushParam("rca_calculations", key);

	string msg = req.write();
	zmqSendToDal(msg);

	DalReply rep(msg);
}

void getAllCalculations()
{
	mon.Puts(CMonitor::NOTICE, "RCA_DAL", "Get all calculations");

	try {

		DalSentRequest req("data-request");

		JSONNode empty;
		req.pushParam("rca_calculations", empty);

		string msg = req.write();
		zmqSendToDal(msg);

		DalReply rep(msg);

		const JSONNode calcList = rep.values();

		for (JSONNode::const_iterator it=calcList.begin(); it!=calcList.end(); it++) {
			try {
				JSONNodeExt jCalc(*it);
				CalculationManager::getInstance().add(RegCalculation(jCalc["content"]), false);
			}
			catch (AppException& e) {
				mon.Puts(CMonitor::ERROR, "RCA_DAL", "Cannot add some calculation [%s]", e.text.c_str());
			}
		}
	}
	catch (AppException& e) {
		mon.Puts(CMonitor::ERROR, "RCA_DAL", "Cannot get some calculation registers from DAL [%s]", e.text.c_str());
	}
}

void getSettings(ZMQEnvironment& zmqEnv)
{
	mon.Puts(CMonitor::NOTICE, "RCA_DAL", "Get settings");

	try {
		DalSentRequest req("data-request");

		JSONNode name;
		name.push_back(JSONNode("type", "rca_settings"));
		req.pushParam("rca_settings", name);

		string msg = req.write();
		zmqSendToDal(msg);

		DalReply rep(msg);

		RcaSettings::import(rep.value0()["content"], zmqEnv);
	}
	catch (AppException& e) {
		mon.Puts(CMonitor::ERROR, "RCA_DAL", "Cannot get settings from DAL [%s]. Default settings will be used.", e.text.c_str());
	}
}


}	// end of namespace RcaDal
