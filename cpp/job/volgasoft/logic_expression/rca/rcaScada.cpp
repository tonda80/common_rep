/*
 * rcaScada.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: ant
 */

#include "rcaScada.h"
#include "../common/json/iWellJSON.h"
#include "../common/communicator/scada.h"
#include "../common/zmq/ZmqManager.h"
#include "../common/utilities/CMonitor.h"
#include "../common/utilities/mapExt.h"
#include "../common/utilities/strHelper.h"

using namespace std;

namespace RcaScada
{

struct Register
{
	double value;
	int cntSubscr;
	Register() : value(0), cntSubscr(0) {}
};

// key is string NUMBER+TYPE ("30001_float")
struct RegisterMap : public MapExt<string, Register>
{
	const string keyErrorMessage(const string& key) const {return "SCADA. Unknown object " + key;}
};

// global module objects
RegisterMap scadaRegisters;	// object needed for calculation and subscribed
ZmqManager zmq;

// internal functions
double traceRegister(int regNum, const string& regType);
void untraceRegister(int regNum, const string& regType);
JSONNode createNodeForRequest(int regNum, const string& regType);
inline string registerKey(int regNum, const string& regType) {return toString(regNum) + "_" + regType;}

// ----------------

string publicationSign()
{
	return "Register_";
}


void subscribe(int regNum, const string& regType)
{
	string name = registerKey(regNum, regType);

	if (! scadaRegisters.has(name))	{
		// if tracking is possible
		double value = traceRegister(regNum, regType);

		// subscription
		mon.Puts(CMonitor::NOTICE, "RCA SCADA", "Subscription to %s", name.c_str());
		if (zmq.SetSubscribeFilter("[\"data\",\"Register_" + name) != 0)
			throw AppException("SCADA subscription error");

		// addition to map
		mon.Puts(CMonitor::NOTICE, "RCA SCADA", "Register %s value is %f", name.c_str(), value);
		scadaRegisters[name].value = value;
	}

	++scadaRegisters.at(name).cntSubscr;
}

void unsubscribe(int regNum, const string& regType)
{
	string name = registerKey(regNum, regType);

	if (--scadaRegisters.at(name).cntSubscr > 0) { // don't unsubscribe yet
		return;
	}

	mon.Puts(CMonitor::NOTICE, "RCA SCADA", "Unsubscription from %s", name.c_str());

	untraceRegister(regNum, regType);

	if (zmq.UnSetSubscribeFilter("[\"data\",\"Register_" + name) != 0)
		throw AppException("SCADA unsubscription error");

	scadaRegisters.erase(name);
}

double getValue(int regNum, const string& regType)
{
	return scadaRegisters.at(registerKey(regNum, regType)).value;
}

void setValue(int regNum, const string& regType, double value)
{
	setValue(registerKey(regNum, regType), value);
}

void setValue(const string& name, double value)
{
	if (scadaRegisters.has(name)) {
		mon.Puts(CMonitor::NOTICE, "RCA SCADA", "Register %s value is %f", name.c_str(), value);
		scadaRegisters[name].value = value;
	}
}

JSONNode createNodeForRequest(int regNum, const string& regType)
{
	JSONNode arrReg(JSON_ARRAY);
	arrReg.push_back(JSONNode("", regNum));
	arrReg.push_back(JSONNode("", regType));

	JSONNode arrRegisters(JSON_ARRAY);
	arrRegisters.push_back(arrReg);
	arrRegisters.set_name("registers");

	JSONNode reqParam;
	reqParam.push_back(JSONNode("application", "register_calculation"));
	reqParam.push_back(arrRegisters);

	return reqParam;
}

double traceRegister(int regNum, const string& regType)
{
	JSONNodeExt answer = Scada::requestUpdatePublishList(createNodeForRequest(regNum, regType));
	int answerRegNum = answer[0][0].toInt();
	string answerRegType = answer[0][1].toStr();
	double answerRegValue = answer[0][2].toDouble();

	if (answerRegNum != regNum && answerRegType != regType)
		throw AppException("Scada. Update publish list error");

	return answerRegValue;
}

void untraceRegister(int regNum, const string& regType)
{
	Scada::requestDeletePublishList(createNodeForRequest(regNum, regType));
}

}	// end of namespace RcaScada

