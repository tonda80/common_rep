/*
 * rcaDal.h
 *
 *  Created on: Feb 21, 2014
 *      Author: ant
 */

#ifndef RCADAL_H_
#define RCADAL_H_

#include <set>
#include <string>

using namespace std;

class RegCalculation;
struct ZMQEnvironment;

namespace RcaDal
{
	// DAL API
	void addCalculation(const RegCalculation& calc);
	void removeCalculation(int id);
	// the client calls
	void getAllCalculations();	// noexcept
	void getSettings(ZMQEnvironment& zmqEnv);	// noexcept

}	// end of namespace RcaDal


#endif /* RCADAL_H_ */
