/*
 * rcaModbusMaster.h
 *
 *  Created on: Mar 11, 2014
 *      Author: ant
 */

#ifndef RCAMODBUSMASTER_H_
#define RCAMODBUSMASTER_H_

#include <string>
#include <stdint.h>

namespace RcaModbusMaster
{

std::string publicationSign();			// string sign of a publication message from Modbus Master

void subscribe(int slaveNum, int regNum);
void unsubscribe(int slaveNum, int regNum);

uint16_t getValue(int slaveNum, int regNum);
double getValue(int slaveNum, int regNum, std::string regType);
void setValue(int slaveNum, int regNum, uint16_t value);

}	// end of namespace RcaModbusMaster


#endif /* RCAMODBUSMASTER_H_ */
