/*
 * regCalculation.cpp
 *
 *  Created on: Feb 13, 2014
 *      Author: ant
 */

#include "regCalculation.h"
#include "../common/utilities/CMonitor.h"
#include "../common/appException.h"
#include "../common/json/iWellJSON.h"
#include "rcaEmc.h"
#include "rcaModbusMaster.h"
#include "rcaScada.h"
#include "calculationManager.h"
#include "../common/utilities/strHelper.h"
#include "rcaSettings.h"

#include "../common/zmq/ZmqManager.h"
ZmqManager zmqMgr;

BaseRegister* registerFactory(const string& category)
{
	BaseRegister* ret = NULL;

	if (category == "analog")
		ret = new Analog;
	else if (category == "accumulator")
		ret = new Accumulator;
	else if (category == "register")
		ret = new Register;
	else if (category == "slave")
		ret = new Slave;
	else if (category == "constant")
		ret = new Constant;
	else if (category == "calc_result")
		ret = new CalcResult;

	if (ret != NULL)
		ret->category = category;

	return ret;
}


RegCalculation::RegCalculation(const JSONNode& jsonCalculation)
{
	if (! findJSONInt(jsonCalculation, "calculation_id", calculation_id))
		throw CJSONException("No calculation_id");
	if (calculation_id < 1 || calculation_id > RcaSettings::maxCalculationNumber)
		throw CJSONException("Wrong calculation_id");

	if (! findJSONStr(jsonCalculation, "arithmetic_operator", arithmetic_operator))
		throw CJSONException("No arithmetic_operator");
	if (arithmetic_operator != "plus" && arithmetic_operator != "minus" && arithmetic_operator != "multiply" && arithmetic_operator != "divide")
		throw CJSONException("Wrong arithmetic_operator");

	input1 = tr1::shared_ptr<BaseRegister>(setupInput(jsonCalculation, "input1_"));
	input2 = tr1::shared_ptr<BaseRegister>(setupInput(jsonCalculation, "input2_"));
}

void RegCalculation::toJSON(JSONNode& jsonCalculation) const
{
	if (input1.get() == NULL || input2.get() == NULL)
		throw AppException("Internal error. Wrong calculation into CalculationManager.");

	jsonCalculation = JSONNode(JSON_NODE);
	jsonCalculation.push_back(JSONNode("arithmetic_operator", arithmetic_operator));
	jsonCalculation.push_back(JSONNode("calculation_id", calculation_id));
	jsonCalculation.push_back(JSONNode("input1_register_category", input1->category));
	jsonCalculation.push_back(JSONNode("input2_register_category", input2->category));
	input1->getFields(jsonCalculation);
	input2->getFields(jsonCalculation);
}

BaseRegister* RegCalculation::setupInput(const JSONNode& jsonCalculation, const string& prefix)
{
	string input_register_category;
	string name_register_category = prefix+"register_category";
	if (! findJSONStr(jsonCalculation, name_register_category.c_str(), input_register_category))
		throw CJSONException("No "+name_register_category);

	BaseRegister* input = registerFactory(input_register_category);
	if (input == NULL)
		throw CJSONException("Wrong "+name_register_category);
	input->prefix = prefix;
	input->setFields(jsonCalculation, prefix);

	return input;
}

void Analog::setFields(const JSONNode& json, const string& prefix)
{
	string name_interpretation_uuid = prefix + "interpretation_uuid";
	if (! findJSONStr(json, name_interpretation_uuid.c_str(), interpretation_uuid))
		throw CJSONException("No "+name_interpretation_uuid);

	string name_interpretation_data_to_log = prefix + "interpretation_data_to_log";
	if (! findJSONStr(json, name_interpretation_data_to_log.c_str(), interpretation_data_to_log))
		throw CJSONException("No "+name_interpretation_data_to_log);
	if (interpretation_data_to_log != "scaled" && interpretation_data_to_log != "raw")
		throw CJSONException("Wrong "+name_interpretation_data_to_log);
}

void Analog::getFields(JSONNode& json)
{
	json.push_back(JSONNode(prefix + "interpretation_uuid", interpretation_uuid));
	json.push_back(JSONNode(prefix + "interpretation_data_to_log", interpretation_data_to_log));
}

void Register::setFields(const JSONNode& json, const string& prefix)
{
	string name_register_number = prefix + "register_number";
	if (! findJSONInt(json, name_register_number.c_str(), register_number))
		throw CJSONException("No "+name_register_number);
	if (register_number < 30000 || register_number > 39999)
		throw CJSONException("Wrong "+name_register_number);

	string name_register_type = prefix + "register_type";
	if (! findJSONStr(json, name_register_type.c_str(), register_type))
		throw CJSONException("No "+name_register_type);
	if (register_type != "word" && register_type != "long" && register_type != "float")		//  && register_type != "coil"
		throw CJSONException("Wrong "+name_register_type+" "+name_register_type);
}

void Register::getFields(JSONNode& json)
{
	json.push_back(JSONNode(prefix + "register_number", register_number));
	json.push_back(JSONNode(prefix + "register_type", register_type));
}

void Slave::setFields(const JSONNode& json, const string& prefix)
{
	string name_slave_device_number = prefix + "slave_device_number";
	if (! findJSONInt(json, name_slave_device_number.c_str(), slave_device_number))
		throw CJSONException("No "+name_slave_device_number);
	if (slave_device_number < 1 || slave_device_number > 10)
		throw CJSONException("Wrong "+name_slave_device_number);

	string name_slave_register_type = prefix + "slave_register_type";
	if (! findJSONStr(json, name_slave_register_type.c_str(), slave_register_type))
		throw CJSONException("No "+name_slave_register_type);
	if (slave_register_type != "word" && slave_register_type != "long" && slave_register_type != "float")	//  && slave_register_type != "coil"
		throw CJSONException("Wrong "+name_slave_register_type);

	string name_slave_register_index = prefix + "slave_register_index";
	if (! findJSONInt(json, name_slave_register_index.c_str(), slave_register_index))
		throw CJSONException("No "+name_slave_register_index);
	if (slave_register_index < 0 || slave_register_index > 49999)
		throw CJSONException("Wrong "+name_slave_register_index);

}

void Slave::getFields(JSONNode& json)
{
	json.push_back(JSONNode(prefix + "slave_device_number", slave_device_number));
	json.push_back(JSONNode(prefix + "slave_register_type", slave_register_type));
	json.push_back(JSONNode(prefix + "slave_register_index", slave_register_index));
}

void Constant::setFields(const JSONNode& json, const string& prefix)
{
	string name_constant_value = prefix + "constant_value";
	if (! findJSONDouble(json, name_constant_value.c_str(), constant_value))
		throw CJSONException("No "+name_constant_value);
}

void Constant::getFields(JSONNode& json)
{
	json.push_back(JSONNode(prefix + "constant_value", constant_value));
}

#include "rcaSettings.h"

void CalcResult::setFields(const JSONNode& json, const string& prefix)
{
	string name_calculated_equation_number = prefix + "calculated_equation_number";
	if (! findJSONInt(json, name_calculated_equation_number.c_str(), calculated_equation_number))
		throw CJSONException("No "+name_calculated_equation_number);
	if (calculated_equation_number < 1 || calculated_equation_number > RcaSettings::maxCalculationNumber)
		throw CJSONException("Wrong "+name_calculated_equation_number);

	// check a reference to itself
	int calculation_id;
	if (! findJSONInt(json, "calculation_id", calculation_id))
		throw CJSONException("No calculation_id");
	if (calculation_id == calculated_equation_number)
		throw AppException("calculated_equation_number == calculation_id");

	// check an existence of such calculation
	if (! CalculationManager::getInstance().hasId(calculated_equation_number))
		throw AppException("Calculation " + toString(calculated_equation_number) + " is absent");
}

void CalcResult::getFields(JSONNode& json)
{
	json.push_back(JSONNode(prefix + "calculated_equation_number", calculated_equation_number));
}

double RegCalculation::calculate() const
{
	mon.Puts(CMonitor::BLAB, "RegCalculation", "Calculation %d", calculation_id);

	double result;
	if (arithmetic_operator == "plus") {
		result = input1->value() + input2->value();
	}
	else if (arithmetic_operator == "minus") {
		result = input1->value() - input2->value();
	}
	else if (arithmetic_operator == "multiply") {
		result = input1->value() * input2->value();
	}
	else if (arithmetic_operator == "divide") {
		if (input2->value() == 0)
			throw AppException("Zero division error");
		result = input1->value() / input2->value();
	}
	else
		throw AppException("Bad arithmetic_operator " + arithmetic_operator);

	return result;
}

void RegCalculation::publishResult(double result) const
{
	JSONNode msgBody(JSON_NODE);
	msgBody.push_back(JSONNode("value", result));
	msgBody.push_back(JSONNode("ts", time(NULL)));

	publish("data", msgBody);
}

void RegCalculation::publishError(const string& what) const
{
	list<string> disObjs = disabledObjects();

	mon.Puts(CMonitor::DEBUG, "RegCalculation", "PublishError %d. Disabled objects %d", calculation_id, disObjs.size());

	if (disObjs.size() > 0) {
		publishAboutDisabled(disObjs);
	}
	else {
		JSONNode msgBody(JSON_NODE);
		msgBody.push_back(JSONNode("reason", what));

		publish("exception", msgBody);
	}
}

void RegCalculation::publishAboutDisabled(const list<string>& disabledObjects) const
{
	JSONNode jDisabledObjects(JSON_ARRAY);
	jDisabledObjects.set_name("disabled_interpretations");

	for(list<string>::const_iterator it = disabledObjects.begin(); it != disabledObjects.end(); it++)
		jDisabledObjects.push_back(JSONNode("", *it));

	JSONNode msgBody(JSON_NODE);
	msgBody.push_back(jDisabledObjects);
	publish("exception", msgBody);
}

list<string> RegCalculation::disabledObjects() const
{
	list<string> disabledIntepr;

	string obj;
	if (input1->checkDisabled(obj))
		disabledIntepr.push_back(obj);
	if (input2->checkDisabled(obj))
		disabledIntepr.push_back(obj);

	return disabledIntepr;
}

void RegCalculation::publish(const string& msgType, const JSONNode& msgBody) const
{
	list<string> publMsg;

	JSONNode p1(JSON_ARRAY);
	p1.push_back(JSONNode("", msgType));
	p1.push_back(JSONNode("", "register_calculation-" + toString(calculation_id)));

	JSONNode p2(JSON_NODE);
	p2.push_back(JSONNode("source", "register_calculations"));
	p2.push_back(JSONNode("destination", "all"));

	publMsg.push_back(p1.write()); publMsg.push_back(p2.write()); publMsg.push_back(msgBody.write());
	zmqMgr.PublishMulti(publMsg);
}

double Analog::value()
{
	return RcaEmc::getValue(interpretation_uuid);
}

bool Analog::checkDisabled(string &name)
{
	if (RcaEmc::isBad(interpretation_uuid)) {
		name = interpretation_uuid;
		return true;
	}
	return false;
}

double Register::value()
{
	return RcaScada::getValue(register_number, register_type);
}

double Slave::value()
{
	return RcaModbusMaster::getValue(slave_device_number, slave_register_index, slave_register_type);
}

double CalcResult::value()
{
	if (! CalculationManager::getInstance().hasId(calculated_equation_number))
		throw AppException("Necessary calculation " + toString(calculated_equation_number) + " is absent");

	double result;
	try {
		result = CalculationManager::getInstance()[calculated_equation_number].calculate();
	}
	catch (AppException& e) {
		throw AppException("Calculation " + toString(calculated_equation_number) + " cannot be calculated");
	}

	return result;
}

void Analog::addCallback()
{
	RcaEmc::subscribe(interpretation_uuid);
}
void Analog::removeCallback()
{
	RcaEmc::unsubscribe(interpretation_uuid);
}

void Register::addCallback()
{
	RcaScada::subscribe(register_number, register_type);
}
void Register::removeCallback()
{
	RcaScada::unsubscribe(register_number, register_type);
}

void Slave::addCallback()
{
	RcaModbusMaster::subscribe(slave_device_number, slave_register_index);

	if (slave_register_type == "float" || slave_register_type == "long")
		RcaModbusMaster::subscribe(slave_device_number, slave_register_index + 1);
}
void Slave::removeCallback()
{
	RcaModbusMaster::unsubscribe(slave_device_number, slave_register_index);

	if (slave_register_type == "float" || slave_register_type == "long")
		RcaModbusMaster::unsubscribe(slave_device_number, slave_register_index + 1);
}
