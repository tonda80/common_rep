/*
 * timeIntervals.cpp
 *
 *  Created on: Mar 11, 2014
 *      Author: ant
 */

#include <pthread.h>
#include <stdexcept>
#include <unistd.h>

#include "timeIntervals.h"
#include "rcaSettings.h"
#include "rcaEmc.h"
#include "../common/utilities/CMonitor.h"

extern int shutdownFlag;

namespace TimeIntervals
{

pthread_t intervalThread;
void* measureIntervals(void* );

void startThread()
{
	if (pthread_create(&intervalThread, NULL, measureIntervals, NULL) != 0)
		throw std::runtime_error("pthread_create error");
}

int joinThread()
{
	return pthread_join(intervalThread, NULL);
}

void* measureIntervals(void* )
{
	static int cntSec = 0;

	while (shutdownFlag == 0) {
		cntSec++;

		if (cntSec % RcaSettings::emcStatusRequestInterval == 0) {
			RcaEmc::checkAllObjects();
		}

		sleep(1);
	}

	return NULL;
}

} // end of namespace TimeIntervals
