/*
 * rcaScada.h
 *
 *  Created on: Mar 14, 2014
 *      Author: ant
 */

#ifndef RCASCADA_H_
#define RCASCADA_H_

#include <string>

using namespace std;

namespace RcaScada
{

std::string publicationSign();			// string sign of a publication message from Scada

void subscribe(int regNum, const string& regType);
void unsubscribe(int regNum, const string& regType);

double getValue(int regNum, const string& regType);
void setValue(int regNum, const string& regType, double value);
void setValue(const string& name, double value);

}	// end of namespace RcaScada



#endif /* RCASCADA_H_ */
