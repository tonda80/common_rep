/*
 * timeIntervals.h
 *
 *  Created on: Mar 11, 2014
 *      Author: ant
 */

#ifndef TIMEINTERVALS_H_
#define TIMEINTERVALS_H_


namespace TimeIntervals
{
	void startThread();
	int joinThread();
};


#endif /* TIMEINTERVALS_H_ */
