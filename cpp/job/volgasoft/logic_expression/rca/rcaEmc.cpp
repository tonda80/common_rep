/*
 * rcaEmc.cpp
 *
 *  Created on: Mar 4, 2014
 *      Author: ant
 */
#include "rcaEmc.h"
#include "../common/communicator/emc.h"
#include "../common/appException.h"
#include "../common/zmq/ZmqManager.h"
#include "../common/json/iWellJSON.h"
#include "../common/utilities/CMonitor.h"
#include "../common/utilities/mapExt.h"
#include "../common/utilities/mutexWrapper.h"

//#include "unistd.h"

namespace RcaEmc
{

struct EmcObject
{
	bool valid;		// if false, an object state is unknown
	bool enabled;	// object is enabled
	double value;
	int cntSubscr;	// subscribe counter

	EmcObject() : valid(false), enabled(false), value(0), cntSubscr(0) {}
	bool isGood() {return valid && enabled;}
	void setGood() {valid = true; enabled = true;}
};

// internal functions
void doStatusRequest(const string& name, EmcObject& emcObj);

// --------------------------------------------
class CEmcObjects : public MapExt<string, EmcObject>
{
	const string keyErrorMessage(const string& key) const {return "EMC. Unknown object " + key;}
};

// --------------------------------------------
// global module objects

CEmcObjects emcObjects;	// object needed for calculation and subscribed
Mutex emcObjectsMutex;

ZmqManager zmq;

// --------------------------------------------

void checkAllObjects()
{
	mon.Puts(CMonitor::NOTICE, "RCA EMC", "Emc Status Request for all EMC objects");

	MutexLocker mLocker(emcObjectsMutex);

	for (map<string, EmcObject>::iterator it = emcObjects.begin(); it != emcObjects.end(); it++) {

		try {
			doStatusRequest(it->first, it->second);
		}
		catch (AppException& e) {
			;	// error log output was already
		}
	}
}

double getValue(const string& name)
{
	if (! emcObjects.has(name))
		throw AppException("No object");
	if (! emcObjects.at(name).valid)
		throw AppException("Object is not valid");
	if (! emcObjects.at(name).enabled)
		throw AppException("Object is not enabled");

	return emcObjects.at(name).value;
}

void setValue(const string& name, double value)
{
	emcObjects[name].value = value;
}

bool has(const string& name)
{
	return emcObjects.has(name);
}

bool isBad(const std::string& name)
{
	if (!emcObjects.has(name) || !emcObjects.at(name).isGood())
		return true;
	return false;
}

void subscribe(const string& name)
{
	if (! emcObjects.has(name))	{
		// check UUID
		EmcObject emcObj;
		doStatusRequest(name, emcObj);

		// subscription
		mon.Puts(CMonitor::NOTICE, "RCA EMC", "Subscription to %s", name.c_str());
		if (zmq.SetSubscribeFilter("[\"data\",\"" + name) != 0)
			throw AppException("EMC subscription error");

		// addition to map
		emcObjects[name] = emcObj;
	}

	++emcObjects.at(name).cntSubscr;
}

void unsubscribe(const string& name)
{
	if (--emcObjects.at(name).cntSubscr > 0) { // don't unsubscribe yet
		return;
	}

	mon.Puts(CMonitor::NOTICE, "RCA EMC", "Unsubscription from %s", name.c_str());

	if (zmq.UnSetSubscribeFilter("[\"data\",\"" + name) != 0)
		throw AppException("EMC unsubscription error");

	MutexLocker mLocker(emcObjectsMutex);
	emcObjects.erase(name);
}

void doStatusRequest(const string& name, EmcObject& emcObj)
{
	mon.Puts(CMonitor::DEBUG, "RCA EMC", "EMC Status Request for %s", name.c_str());

	try {
		JSONNodeExt status = Emc::requestStatus(name);

		string explanation = status["explanation"].toStr();

		if (explanation == "VALUE FOUND") {
			emcObj.value = status["value"]["Scaled_value"].toDouble();
			emcObj.setGood();
		}
		else if (explanation == "Port Not Enabled") {
			emcObj.valid = true;
			emcObj.enabled = false;
		}
		else if (explanation == "Data Not Received") {
			emcObj.valid = false;
		}
		else {
			mon.Puts(CMonitor::ERROR, "RCA EMC", "Unknown \"explanation into EMC reply\"");
			emcObj.valid = false;
		}
	}
	catch (AppException& e) {
		mon.Puts(CMonitor::ERROR, "RCA EMC", "Cannot do EMC Status Request [%s]", e.c_str());
		emcObj.valid = false;
		throw;
	}
}

}// end of namespace RcaEmc

