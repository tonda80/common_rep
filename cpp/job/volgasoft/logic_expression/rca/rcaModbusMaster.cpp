/*
 * rcaModbusMaster.cpp
 *
 *  Created on: Mar 11, 2014
 *      Author: ant
 */

#include "rcaModbusMaster.h"

#include "../common/zmq/ZmqManager.h"
#include "../common/json/iWellJSON.h"
#include "../common/utilities/CMonitor.h"
#include "../common/utilities/mapExt.h"
#include "../common/utilities/strHelper.h"

using namespace std;

namespace RcaModbusMaster
{

struct Register
{
	int cntSubscr;
	uint16_t value;
	bool valid;			// value was set at least once
	Register() : cntSubscr(0), value(0), valid(false) {}
};

struct Slave : public MapExt<int, Register>
{
	const string keyErrorMessage(const int& key) const {return "Modbus master. Unknown register " + toString(key);}

	void traceRegister(int addr);
	void untraceRegister(int addr);
};

void Slave::traceRegister(int addr)
{
	if (! has(addr)) {
		insert(pair<int, Register>(addr, Register())); // addition of new register
	}
	++at(addr).cntSubscr;
}

void Slave::untraceRegister(int addr)
{
	if (--at(addr).cntSubscr <= 0)
		erase(addr);
}

struct Slaves : public MapExt<int, Slave>
{
	const string keyErrorMessage(const int& key) const {return "Modbus master. Unknown slave " + toString(key);}
};

// --------------------------------------------
// global module objects

Slaves slaves;
ZmqManager zmq;

// --------------------------------------------
// internal functions

string slaveName(int n)
{
	return publicationSign() + toString(n);
}

// --------------------------------------------
std::string publicationSign()
{
	return "Modbus_Master-Slave";
}

void subscribe(int slaveNum, int regNum)
{
	if (! slaves.has(slaveNum)) {
		mon.Puts(CMonitor::NOTICE, "RCA Modbus Master", "Subscription to %d slave", slaveNum);
		if (zmq.SetSubscribeFilter("[\"data\",\"" + slaveName(slaveNum)) != 0)
			throw AppException("Modbus Master subscription error");

		slaves[slaveNum];	// addition to map here
	}

	slaves.at(slaveNum).traceRegister(regNum);
}

void unsubscribe(int slaveNum, int regNum)
{
	Slave& slave =  slaves.at(slaveNum);

	slave.untraceRegister(regNum);

	if ( slave.empty() ) {
		mon.Puts(CMonitor::NOTICE, "RCA Modbus Master", "Unsubscription from %d slave", slaveNum);
		if (zmq.UnSetSubscribeFilter("[\"data\",\"" + slaveName(slaveNum)) != 0)
			throw AppException("Modbus Master unsubscription error");

		slaves.erase(slaveNum);		// remove from map here
	}
}

uint16_t getValue(int slaveNum, int regNum)
{
	if (slaves.has(slaveNum) && slaves[slaveNum].has(regNum) && slaves[slaveNum][regNum].valid) {
		return slaves[slaveNum][regNum].value;
	}
	mon.Puts(CMonitor::NOTICE, "RCA Modbus Master", "Slave %d register %d was requested but its value unknown (returned 0)", slaveNum, regNum);
	return 0;
}

void setValue(int slaveNum, int regNum, uint16_t value)
{
	if (slaves.has(slaveNum) && slaves[slaveNum].has(regNum)) {
		mon.Puts(CMonitor::NOTICE, "RCA Modbus Master", "Slave %d register %d set %d", slaveNum, regNum, value);
		Register& reg = slaves[slaveNum][regNum];
		reg.value = value;
		reg.valid = true;
	}
}

/// Compilation error is float is not 8 bit
bool TEST[sizeof(float) == 4 ? 1 : -1];

double getValue(int slaveNum, int regNum, std::string regType)
{
	union {
		uint16_t word_[2];
		uint32_t long_;
		float float_;
	} buff;

	if (regType == "word") {	// 16 bit
		return getValue(slaveNum, regNum);
	}
	else if (regType == "long") {	// 32 bit
		buff.word_[1] = getValue(slaveNum, regNum);
		buff.word_[0] = getValue(slaveNum, regNum + 1);
		return buff.long_;
	}
	else if (regType == "float") {	// 32 bit
		buff.word_[1] = getValue(slaveNum, regNum);
		buff.word_[0] = getValue(slaveNum, regNum + 1);
		if (buff.float_ != buff.float_)
			throw AppException("Slave" + toString(slaveNum) + ", " + toString(regNum) + " float is NAN");
		return buff.float_;
	}

	mon.Puts(CMonitor::NOTICE, "RCA Modbus Master", "%s register type is unknown Slave %d register %d set %d", regType.c_str());
	throw AppException("Unknown register type " + regType);
	return 0;
}

}	// end of namespace RcaModbusMaster


