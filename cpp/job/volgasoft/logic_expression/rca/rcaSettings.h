/*
 * RCASettings.h
 *
 *  Created on: Feb 6, 2014
 *      Author: ant
 */

#ifndef RCASETTINGS_H_
#define RCASETTINGS_H_

#include <string>

class JSONNodeExt;
struct ZMQEnvironment;

using namespace std;

namespace RcaSettings
{
	extern int maxCalculationNumber;
	extern int emcStatusRequestInterval;		// sec

	void import(const JSONNodeExt&, ZMQEnvironment& zmqEnv);
};	// end of namespace RcaSettings

namespace CmdLineSettings
{
	void init(int argc, char **argv);

	extern bool noJsonDynData;	// sent json data don't have dynamic data (uniqueReqId, timestamp ..)
	extern string logFileName;
};	// end of namespace CmdLineSettings




#endif /* RCASETTINGS_H_ */
