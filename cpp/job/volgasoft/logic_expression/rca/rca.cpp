/*
 * rca.cpp
 *
 *  Created on: Feb 6, 2014
 *      Author: ant
 */

#include <signal.h>
#include <unistd.h>

#include "../common/utilities/CMonitor.h"
#include "../common/zmq/ZmqManager.h"
#include "../common/communicator/communicator.h"
#include "communicator/subscriptionProcessers.h"
#include "rcaSettings.h"
#include "rcaDal.h"
#include "timeIntervals.h"
#include "calculationManager.h"


// global variables
int shutdownFlag = 0;

string appName("rca");

bool noJsonDynData;
// --------------------------

void signalHandler(int signum)
{
	mon.Puts(CMonitor::NOTICE, "Signal", "Signal %d was received", signum);
	shutdownFlag = 1;
}

int main(int argc, char **argv)
{
	CmdLineSettings::init(argc, argv);
	noJsonDynData = CmdLineSettings::noJsonDynData;

	mon.openFile(CmdLineSettings::logFileName.c_str());
	mon.readSettings(".monitor.ini");

	mon.Puts(CMonitor::NOTICE, "main", "\n\n\n\n-------------------------- Start RCA --------------------------");
	signal(SIGPIPE, signalHandler);
	signal(SIGQUIT, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGINT, signalHandler);

	// get settings (zmq and other)
	RcaDal::getSettings(ZmqManager::env);

	ZmqManager::init(Communicator::processRequest, Communicator::processSubscribeMsg);
	ZmqManager zmq;
	zmq.CreateListenerThrd("ipc:///tmp/IWELL_RCA");
	zmq.CreateSubscriberThrd();
	sleep(1);
	//zmq.SetSubscribeFilter(""); //Debug. Subscribe on all

	CalculationManager::startProcessCalculation();

	TimeIntervals::startThread();

	RcaDal::getAllCalculations();

	zmq.JoinListenerThrd();
	zmq.JoinSubscriberThrd();
	CalculationManager::joinProcessCalculation();
	TimeIntervals::joinThread();
	mon.Puts(CMonitor::NOTICE, "main", "\n\n-------------------------- Stop RCA --------------------------");

	return 0;
}

