/*
 * calculationManager.h
 *
 *  Created on: Feb 13, 2014
 *      Author: ant
 */

#ifndef CALCULATIONMANAGER_H_
#define CALCULATIONMANAGER_H_

#include "regCalculation.h"

#include <map>
#include <list>

#include "../common/utilities/mutexWrapper.h"

using namespace std;

class CalculationManager
{
public:
	static CalculationManager& getInstance() {static CalculationManager instance; return instance;}
	static int startProcessCalculation();
	static int joinProcessCalculation();

	void add(const RegCalculation& calc, bool addToDal = true);
	void update(const RegCalculation& calc);
	void remove(int id);
	bool hasId(int id);
	unsigned int calculationNumber() {return calculations.size();}

	const RegCalculation& operator[](int i) {return calculations[i];}
	list<int> idList();

private:
	CalculationManager() {}
	static void* timerFunction(void*);

	static map<int, RegCalculation> calculations;
	static map<int, Mutex> calcMutexes;
};


#endif /* CALCULATIONMANAGER_H_ */
