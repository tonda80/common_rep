/*
 * subscriptionProcessers.h
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#ifndef SUBSCRIPTIONPROCESSERS_H_
#define SUBSCRIPTIONPROCESSERS_H_

#include <string>

namespace Communicator
{
	void processSubscribeMsg(const std::string &msg);
}



#endif /* SUBSCRIPTIONPROCESSERS_H_ */
