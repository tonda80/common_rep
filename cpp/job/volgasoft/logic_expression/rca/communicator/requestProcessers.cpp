/*
 * requestProcessers.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#include "../../common/communicator/communicator.h"

#include "../calculationManager.h"

namespace Communicator
{

int create_register_calculation(const Request&, JSONNode&);
int get_register_calculation(const Request&, JSONNode&);
int delete_register_calculation(const Request&, JSONNode&);
int list_of_register_calculations(const Request&, JSONNode&);

TRequestProcesser requestProcessers[] = {
	create_register_calculation,
	get_register_calculation,
	delete_register_calculation,
	list_of_register_calculations
};
int numProcessers = sizeof(requestProcessers)/sizeof(TRequestProcesser);


int create_register_calculation(const Request& request, JSONNode&)
{
	if (request.method != "data-commit" || request.value != "create_register_calculation")
		return 0;

	RegCalculation regCalc(request.getParam());

	// TODO Checking (max number etc)

	if (CalculationManager::getInstance().hasId(regCalc.calculation_id)) {
		CalculationManager::getInstance().update(regCalc);
	}
	else {
		CalculationManager::getInstance().add(regCalc);
	}

	return 1;
}

int get_register_calculation(const Request& request, JSONNode& answer)
{
	if (request.method != "data-request" || request.value != "get_register_calculation")
		return 0;

	int calculation_id;
	if (! findJSONInt(request.getParam(), "calculation_id", calculation_id))
		throw CJSONException("No calculation_id");

	if (! CalculationManager::getInstance().hasId(calculation_id))
		throw AppException("No such calculation_id");

	CalculationManager::getInstance()[calculation_id].toJSON(answer);

	return 2;
}

int delete_register_calculation(const Request& request, JSONNode&)
{
	if (request.method != "data-delete" || request.value != "delete_register_calculation")
		return 0;

	int calculation_id;
	if (! findJSONInt(request.getParam(), "calculation_id", calculation_id))
		throw CJSONException("No calculation_id");

	CalculationManager::getInstance().remove(calculation_id);

	return 1;
}

int list_of_register_calculations(const Request& request, JSONNode& answer)
{
	if (request.method != "data-request" || request.value != "list_of_register_calculations")
		return 0;

	answer = JSONNode(JSON_ARRAY);
	list<int> idList = CalculationManager::getInstance().idList();
	for (list<int>::iterator it = idList.begin(); it != idList.end(); it++) {
		answer.push_back(JSONNode("", *it));
	}

	return 2;
}

}; // end of namespace Communicator
