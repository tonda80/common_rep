/*
 * subscriptionProcessers.cpp
 *
 *  Created on: Feb 7, 2014
 *      Author: ant
 */

#include "subscriptionProcessers.h"
#include "../../common/json/iWellJSON.h"
#include "../../common/utilities/CMonitor.h"
#include "../../common/utilities/splitter.h"
#include "../../common/utilities/strHelper.h"
#include "../rcaEmc.h"
#include "../rcaModbusMaster.h"
#include "../rcaScada.h"

namespace Communicator
{

void processDataSubscribeMsg(const SubscribeMsg &msg);
void processEMCMsg(const string& , const SubscribeMsg &msg);
void processModbusMasterMsg(const string&, const SubscribeMsg &msg);
void processScadaMsg(const string&, const SubscribeMsg &msg);

void processSubscribeMsg(const string &msg)
{
	try {
		SubscribeMsg subscribeMsg(msg);

		if (subscribeMsg.root[0][0] == "data")
			processDataSubscribeMsg(subscribeMsg);
		else
			mon.Puts(CMonitor::ERROR, "Subscription", "Unprocessed subscription message %s", msg.c_str());
	}
	catch (AppException& e) {
		mon.Puts(CMonitor::ERROR, "Subscription", "Subscribe message handling error. %s", e.c_str());
	}
}

void processDataSubscribeMsg(const SubscribeMsg &msg)
{
	string title = msg.root[0][1].toStr();

	if (title.find(RcaModbusMaster::publicationSign()) == 0) {
		processModbusMasterMsg(title, msg);
	}
	else if (title.find(RcaScada::publicationSign()) == 0) {
		processScadaMsg(title, msg);
	}
	else {	// EMC message
		processEMCMsg(title, msg);
	}


}

void processEMCMsg(const string& name, const SubscribeMsg &msg)
{
	mon.Puts(CMonitor::NOTICE, "Subscription", "EMC publication message");

	if (RcaEmc::has(name)) {
		double value = msg.root[2]["value"][0][1].toDouble();

		mon.Puts(CMonitor::NOTICE, "Subscription", "EMC new value %s %f", name.c_str(), value);

		RcaEmc::setValue(name, value);
	}
}


void processModbusMasterMsg(const string& name, const SubscribeMsg &msg)
{
	mon.Puts(CMonitor::NOTICE, "Subscription", "Modbus master publication message");

	Splitter spl(name, RcaModbusMaster::publicationSign());
	if (spl.size() != 2)
		throw AppException("Bad MODBUS slave name " + name);

	int slaveNum;
	bool ok = stringTo(spl[1], slaveNum);
	if (! ok)
		throw AppException("Bad MODBUS slave name " + name);

	JSONNode value = msg.root[2]["value"];
	if (value.type() != JSON_ARRAY)
		throw AppException("Bad value");

	for (JSONNode::iterator it = value.begin(); it != value.end(); it++) {
		if (it->type() != JSON_ARRAY || it->size() != 2 || (*it)[0].type() != JSON_NUMBER || (*it)[1].type() != JSON_NUMBER) {
			mon.Puts(CMonitor::ERROR, "Subscription", "Bad a value element %s", it->write().c_str());
		}
		int regNum = (*it)[0].as_int();
		int regValue = (*it)[1].as_int();
		mon.Puts(CMonitor::BLAB, "Subscription", "Slave %d, register %d, value %d", slaveNum, regNum, regValue);
		RcaModbusMaster::setValue(slaveNum, regNum, regValue);
	}

}

void processScadaMsg(const string& name, const SubscribeMsg &msg)
{
	mon.Puts(CMonitor::NOTICE, "Subscription", "SCADA publication message");

	Splitter spl(name, RcaScada::publicationSign());
	if (spl.size() != 2)
		throw AppException("Bad SCADA register name " + name);

	RcaScada::setValue(spl[1], msg.root[2]["value"][0][1].toDouble());
}

} // end of namespace Communicator


