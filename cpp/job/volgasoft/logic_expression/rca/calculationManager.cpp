/*
 * calculationManager.cpp
 *
 *  Created on: Feb 13, 2014
 *      Author: ant
 */

#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "calculationManager.h"
#include "../common/utilities/CMonitor.h"
#include "../common/appException.h"
#include "rcaDal.h"

pthread_t calculationThread;

extern int shutdownFlag;

int CalculationManager::startProcessCalculation()
{
	static bool started = false;
	if (started) {
		mon.Puts(CMonitor::ERROR, "CalculationManager", "Process is started already");
		return 3;
	}

	if (pthread_create(&calculationThread, NULL, timerFunction, NULL) != 0)
		throw runtime_error("pthread_create error");

	started = true;

	return 0;
}

int CalculationManager::joinProcessCalculation()
{
	return pthread_join(calculationThread, NULL);
}

map<int, RegCalculation> CalculationManager::calculations;
map<int, Mutex> CalculationManager::calcMutexes;

void* CalculationManager::timerFunction(void*)
{
	while (shutdownFlag == 0) {

		for (map<int, RegCalculation>::iterator it = calculations.begin(); it != calculations.end(); it++) {
			if (calcMutexes[it->first].tryLock()) {
				RegCalculation& calc = it->second;

				try {
					calc.publishResult(calc.calculate());
				}
				catch (AppException& e) {
					mon.Puts(CMonitor::DEBUG, "CalculationManager", "Error of calculation %s", e.c_str());
					calc.publishError(e.text);
				}

				calcMutexes[it->first].unlock();
			}
		}

		sleep(1);
	}

	return NULL;
}

bool CalculationManager::hasId(int id)
{
	map<int, RegCalculation>::iterator itExpr;
	if ((itExpr = calculations.find(id)) == calculations.end())
		return false;
	return true;
}

list<int> CalculationManager::idList()
{
	list<int> ret;
	for (map<int, RegCalculation>::iterator it = calculations.begin(); it != calculations.end(); it++) {
		ret.push_back(it->first);
	}
	return ret;
}

void CalculationManager::add(const RegCalculation& calc, bool addToDal/* = true*/)
{
	mon.Puts(CMonitor::NOTICE, "CalculationManager", "Add calculation [%d]", calc.calculation_id);

	if ( hasId(calc.calculation_id))
		throw AppException("Id exists");

	calc.addCallback();

	// DAL addition
	if (addToDal) {
		try {
			RcaDal::addCalculation(calc);
		}
		catch (AppException& e) {
			calc.removeCallback();	// cannot add
			throw;
		}
	}

	calculations.insert(pair<int, RegCalculation>(calc.calculation_id, calc));
	calcMutexes.insert(pair<int, Mutex>(calc.calculation_id, Mutex()));
}

void CalculationManager::update(const RegCalculation& calc)
{
	mon.Puts(CMonitor::NOTICE, "CalculationManager", "Update calculation [%d]", calc.calculation_id);

	remove(calc.calculation_id);
	add(calc);
}

void CalculationManager::remove(int id)
{
	mon.Puts(CMonitor::NOTICE, "CalculationManager", "Remove calculation [%d]", id);

	if (! hasId(id))
		throw AppException("No such id");

	MutexLocker mLocker(calcMutexes[id]);
	calculations[id].removeCallback();
	calculations.erase(id);
	calcMutexes.erase(id);

	// DAL removal
	RcaDal::removeCalculation(id);
}
