/*
 * RCASettings.cpp
 *
 *  Created on: Feb 6, 2014
 *      Author: ant
 */

#include "rcaSettings.h"

#include <iostream>
#include <cstring>
#include <cstdlib>

#include "../common/utilities/CMonitor.h"
#include "../common/zmq/ZmqManager.h"
#include "../common/json/libjsonHelper.h"

namespace RcaSettings
{
int maxCalculationNumber = 10;
int emcStatusRequestInterval = 60;		// sec

// internal
void setStringSetting(const JSONNodeExt& jSrc, const char* settingName, string& setting);
void setIntSetting(const JSONNodeExt& jSrc, const char* settingName, int& setting);

void import(const JSONNodeExt& jConf, ZMQEnvironment& zmqEnv)
{
	setStringSetting(jConf, "EmcEndpoint", zmqEnv.endpoints[ZMQ_EMC_REQ]);
	setStringSetting(jConf, "PublisherEndpoint", zmqEnv.endpoints[ZMQ_PUBLISHER]);
	setStringSetting(jConf, "RcaEndpoint", zmqEnv.endpoints[ZMQ_LISTENER]);
	setStringSetting(jConf, "SubscriberEndpoint", zmqEnv.endpoints[ZMQ_SUBSCRIBER]);
	setStringSetting(jConf, "ScadaEndpoint", zmqEnv.endpoints[ZMQ_SCADA_REQ]);

	setIntSetting(jConf, "maxCalculationNumber", maxCalculationNumber);
	setIntSetting(jConf, "emcStatusRequestInterval", emcStatusRequestInterval);
	setIntSetting(jConf, "PollTimeOut", zmqEnv.pollTimeout);
	setIntSetting(jConf, "RetryCount", zmqEnv.retryCount);
	if (zmqEnv.retryCount <= 0)	// correction
		zmqEnv.retryCount = 1;
}

void setStringSetting(const JSONNodeExt& jSrc, const char* settingName, string& setting)
{
	if (! findJSONStr(jSrc, settingName, setting)) {
		mon.Puts(CMonitor::NOTICE, "RcaSettings", "%s is not found. Default value %s will be used", settingName, setting.c_str());
	}
	else {
		mon.Puts(CMonitor::NOTICE, "RcaSettings", "%s is is set to %s", settingName, setting.c_str());
	}
}
void setIntSetting(const JSONNodeExt& jSrc, const char* settingName, int& setting)
{
	if (! findJSONInt(jSrc, settingName, setting)) {
		mon.Puts(CMonitor::NOTICE, "RcaSettings", "%s is not found. Default value %d will be used", settingName, setting);
	}
	else {
		mon.Puts(CMonitor::NOTICE, "RcaSettings", "%s is set to %d", settingName, setting);
	}
}

};	// end of namespace RcaSettings


namespace CmdLineSettings
{
	bool noJsonDynData = false;
	string logFileName = "/tmp/rca.log";

	static void printHelp();	// print help and exit(0)


void init(int argc, char **argv)
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--no-json-dyn-data") == 0) {
			noJsonDynData = true;
			cout << "Start test mode no-json-dyn-data" << endl;
		}
		else if (strcmp(argv[i], "-l") == 0) {
			if (i+1 >= argc) {
				cerr << "Empty name for log file, using default " << logFileName << endl;
			}
			else {
				logFileName = argv[i+1];
				i++;
			}
		}
		else if (strcmp(argv[i], "-h") == 0) {
			printHelp();
		}

		//else if (strcmp(argv[i], "") == 0)
		else {
			cerr << "Unknown option \"" << argv[i] << "\"" << endl;
		}
	}
}

void printHelp()
{
	cout <<	"Options:\n"
			" -h \t\t\tShow this message\n"
			" -l file \t\tSet the log file name\n"
			" --no-json-dyn-data \tTest option. Program doesn't past a dynamic data in JSON messages\n"
			;

	exit(0);
}

};	// end of namespace CmdLineSettings

