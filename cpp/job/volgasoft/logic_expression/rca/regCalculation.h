/*
 * regCalculation.h
 *
 *  Created on: Feb 13, 2014
 *      Author: ant
 */

#ifndef REGCALCULATION_H_
#define REGCALCULATION_H_

#include <string>
#include <list>

#include <tr1/memory>
#include <tr1/shared_ptr.h>

using namespace std;

class JSONNode;
class AppException;

struct BaseRegister
{
	friend class RegCalculation;

	string category;
	string prefix;

	virtual double value() = 0;
	virtual ~BaseRegister(){}

	// callbacks for an addition/removal of register
	virtual void addCallback() {}
	virtual void removeCallback() {}

	virtual bool checkDisabled(string &name) {return false;}	// maybe it's a bad decision. For analog and accumulator only

private:
	virtual void setFields(const JSONNode& json, const string& prefix) = 0;
	virtual void getFields(JSONNode& json) = 0;
};

struct Analog : public BaseRegister
{
	string interpretation_uuid;
	string interpretation_data_to_log;

	double value();

	void addCallback();
	void removeCallback();

	bool checkDisabled(string &name);
private:
	void setFields(const JSONNode& json, const string& prefix);
	void getFields(JSONNode& json);
};

struct Accumulator : public Analog
{
};

struct Register : public BaseRegister
{
	int register_number;
	string register_type;

	double value();

	void addCallback();
	void removeCallback();
private:
	void setFields(const JSONNode& json, const string& prefix);
	void getFields(JSONNode& json);
};

struct Slave : public BaseRegister
{
	int slave_device_number;
	string slave_register_type;
	int slave_register_index;

	double value();

	void addCallback();
	void removeCallback();
private:
	void setFields(const JSONNode& json, const string& prefix);
	void getFields(JSONNode& json);
};

struct Constant : public BaseRegister
{
	double constant_value;

	double value() {return constant_value;}
private:
	void setFields(const JSONNode& json, const string& prefix);
	void getFields(JSONNode& json);
};

struct CalcResult : public BaseRegister
{
	int calculated_equation_number;

	double value();
private:
	void setFields(const JSONNode& json, const string& prefix);
	void getFields(JSONNode& json);
};

BaseRegister* registerFactory(const string& category);

class RegCalculation
{
public:
	int calculation_id;
	string arithmetic_operator;

	tr1::shared_ptr<BaseRegister> input1;
	tr1::shared_ptr<BaseRegister> input2;

	RegCalculation() : calculation_id(-1){}
	RegCalculation(const JSONNode& jsonCalculation);

	double calculate() const;

	// callbacks for an addition/removal of register
	void addCallback() const {input1->addCallback(); input2->addCallback();}
	void removeCallback() const {input1->removeCallback(); input2->removeCallback();}

	void toJSON(JSONNode& jsonCalculation) const;

	void publishResult(double result) const;
	void publishError(const string& what) const;

private:
	BaseRegister* setupInput(const JSONNode& jsonCalculation, const string& prefix);

	void publish(const string& msgType, const JSONNode& msgBody) const;

	void publishAboutDisabled(const list<string>& disabledObjects) const;
	list<string> disabledObjects() const;
};



#endif /* REGCALCULATION_H_ */
