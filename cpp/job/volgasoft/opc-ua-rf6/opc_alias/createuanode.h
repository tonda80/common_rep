#ifndef CREATEUANODE_H
#define CREATEUANODE_H
#include "nodemanageruanode.h"
class CreateOpcUaNode
{
public:
    CreateOpcUaNode();
    UaStatus createUaNode(UaNodeId requestNodeId,
                          UaQualifiedName browseName,
                          UaNodeId typeDefinition,
                          UaNode  * &addedNode);
    UaStatus createAliasVariable(const UaNodeId &requestNodeId, const UaQualifiedName &browseName, UaNode *&addedNode);
    UaStatus createFolder(const UaNodeId &requestNodeId, const UaQualifiedName &browseName, UaNode *&addedNode);
    UaStatus createAliasObject(const UaNodeId &requestNodeId,
                         const UaQualifiedName &browseName,
                          UaNode *&addedNode);
    void setNodeManagerUaNode(NodeManagerUaNode *managerUaNode);

protected:

    NodeManagerUaNode *mNodeManagerUaNode;




};

#endif // CREATEUANODE_H
