INCLUDEPATH += \
    opc_alias/generated \
    opc_alias
HEADERS += \
    opc_alias/opcalias_nodemanageropcalias.h \
    opc_alias/generated/opcalias_identifiers.h \
    opc_alias/generated/opcalias_datatypes.h \
    opc_alias/createuanode.h

SOURCES += \
    opc_alias/opcalias_nodemanageropcalias.cpp \
    opc_alias/createuanode.cpp
