/******************************************************************************
** opcalias_opcaliasvariabletypebase.cpp
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/opcalias
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#include "opcalias_opcaliasvariabletypebase.h"
#include "uagenericnodes.h"
#include "nodemanagerroot.h"
#include "opcalias_nodemanageropcalias.h"

// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/opcalias
namespace OpcAlias {

bool OpcAliasVariableTypeBase::s_typeNodesCreated = false;

/** Constructs an instance of the class OpcAliasVariableTypeBase with all components
*/
OpcAliasVariableTypeBase::OpcAliasVariableTypeBase(
    UaNode*            pParentNode,  //!< [in] Parent node of the new variable
    UaVariable*        pInstanceDeclarationVariable, //!< [in] UaVariable interface of the instance declaration node used to provide attribute values other than NodeId and Value 
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the variable. Can be NULL if no shared mutex is provided
: OpcUa::BaseDataVariableType(pParentNode, pInstanceDeclarationVariable, pNodeConfig, pSharedMutex)
{
    initialize(pNodeConfig);
}

/** Constructs an instance of the class OpcAliasVariableTypeBase with all components
*/
OpcAliasVariableTypeBase::OpcAliasVariableTypeBase(
    const UaNodeId&    nodeId,       //!< [in] NodeId of the new variable
    const UaString&    name,         //!< [in] Name of the new variable. Used as browse name and also as display name if no additional language specific names are set.
    OpcUa_UInt16       browseNameNameSpaceIndex, //!< [in] Namespace index used for the browse name
    const UaVariant&   initialValue, //!< [in] Initial value for the Variable
    OpcUa_Byte         accessLevel,  //!< [in] Access level for the Variable
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the variable. Can be NULL if no shared mutex is provided
: OpcUa::BaseDataVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex)
{
    initialize(pNodeConfig);
}

/** Initialize the variable with all member nodes 
*/
void OpcAliasVariableTypeBase::initialize(NodeManagerConfig* pNodeConfig)
{
    OpcUa_ReferenceParameter(pNodeConfig);

    if ( s_typeNodesCreated == false )
    {
        createTypes();
    }
}

/** Destruction
*/
OpcAliasVariableTypeBase::~OpcAliasVariableTypeBase()
{
}

/** Create the related type nodes
*/
void OpcAliasVariableTypeBase::createTypes()
{
    if ( s_typeNodesCreated == false )
    {
        s_typeNodesCreated = true;
        NodeManagerRoot* pNodeManagerRoot = NodeManagerRoot::CreateRootNodeManager();
        OpcUa_Int16 nsTypeIdx = NodeManagerOpcAlias::getTypeNamespace();
        NodeManagerConfig* pTypeNodeConfig = pNodeManagerRoot->getNodeManagerByNamespace(nsTypeIdx)->getNodeManagerConfig();

        UaVariant                   nullValue;
        OpcUa::GenericVariableType* pVariableType;
        pVariableType = new OpcUa::GenericVariableType(
            UaNodeId(OpcAliasId_OpcAliasVariableType, nsTypeIdx), 
            UaQualifiedName("OpcAliasVariableType", nsTypeIdx), 
            UaLocalizedText("", "OpcAliasVariableType"), 
            UaLocalizedText("", ""), 
            nullValue,
            UaNodeId(24, NodeManagerRoot::getTypeNamespace()),
            -1,
            OpcUa_False,
            &clearStaticMembers);
        pTypeNodeConfig->addNodeAndReference(UaNodeId(OpcUaId_BaseDataVariableType, NodeManagerRoot::getTypeNamespace()), pVariableType, OpcUaId_HasSubtype);

    }
}

/** Clear the static members of the class
*/
void OpcAliasVariableTypeBase::clearStaticMembers()
{
    s_typeNodesCreated = false;
}

/** Returns the type definition NodeId for the OpcAliasVariableType
*/
UaNodeId OpcAliasVariableTypeBase::typeDefinitionId() const
{
    UaNodeId ret(OpcAliasId_OpcAliasVariableType, NodeManagerOpcAlias::getTypeNamespace());
    return ret;
}

} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/opcalias 


