#include "createuanode.h"

#include "opc_rf6/opcrf6_rf6aliasobject.h"
#include "opc_rf6/opcrf6_rf6aliasvariable.h"
#include "opcua_foldertype.h"
#include "helper.h"

CreateOpcUaNode::CreateOpcUaNode()
{
}

UaStatus CreateOpcUaNode::createAliasObject(const UaNodeId &requestNodeId, const UaQualifiedName &browseName, UaNode *&addedNode)
{
    addedNode  = new OpcRf6::Rf6AliasObject(requestNodeId,
                                    browseName.toString(),
                                    mNodeManagerUaNode->getNameSpaceIndex(),
                                    mNodeManagerUaNode);
    if (NULL == addedNode){
        return OpcUa_BadUnknownResponse;
    }
    return OpcUa_Good;

}
UaStatus CreateOpcUaNode::createAliasVariable(const UaNodeId &requestNodeId, const UaQualifiedName &browseName, UaNode *&addedNode)
{
    addedNode  =  new OpcRf6::Rf6AliasVariable(requestNodeId,
                                       browseName.toString(),
                                       mNodeManagerUaNode->getNameSpaceIndex(),
                                       UaVariant(30),
                                       3,
                                       mNodeManagerUaNode);
    if (NULL == addedNode){
        return OpcUa_BadUnknownResponse;
    }
    return OpcUa_Good;

}
UaStatus CreateOpcUaNode::createFolder(const UaNodeId &requestNodeId, const UaQualifiedName &browseName, UaNode *&addedNode)
{
    addedNode  = new OpcUa::FolderType(requestNodeId, browseName.toString(), mNodeManagerUaNode->getNameSpaceIndex(), mNodeManagerUaNode);
    if (NULL == addedNode){
        return OpcUa_BadUnknownResponse;
    }
    return OpcUa_Good;

}


UaStatus CreateOpcUaNode::createUaNode( UaNodeId requestNodeId,
                                        UaQualifiedName browseName,
                                        UaNodeId typeDefinition,
                                       UaNode * &addedNode)
{
    if (addedNode = mNodeManagerUaNode->getNode(requestNodeId)){
        return OpcUa_BadNodeIdExists;
    }
    if (requestNodeId.namespaceIndex() != mNodeManagerUaNode->getNameSpaceIndex() ){
        return OpcUa_BadNodeIdInvalid;
    }

    //create object
    switch (typeDefinition.identifierNumeric()){
        case OpcRf6Id_Rf6AliasObject:
            return createAliasObject(requestNodeId,browseName,addedNode);
        break;
        case OpcRf6Id_Rf6AliasVariable:
            return createAliasVariable(requestNodeId,browseName,addedNode);
        break;
        case OpcUaId_FolderType:
            return createFolder(requestNodeId,browseName,addedNode);
        break;
        default:
            return OpcUa_BadTypeDefinitionInvalid;
    }

    return OpcUa_BadNotImplemented;
}
void CreateOpcUaNode::setNodeManagerUaNode(NodeManagerUaNode *managerUaNode)
{
    mNodeManagerUaNode = managerUaNode;
}

