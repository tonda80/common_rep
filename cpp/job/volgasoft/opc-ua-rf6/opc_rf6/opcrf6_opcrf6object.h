/******************************************************************************
** opcrf6_opcrf6object.h
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#ifndef __OPCRF6_OPCRF6OBJECT_H__
#define __OPCRF6_OPCRF6OBJECT_H__

#include "opcua_baseobjecttype.h"
#include "opcrf6_identifiers.h"
#include "opcrf6_opcrf6objectbase.h"
#include <QSharedPointer>
#ifdef    RF6_COMPILED
#include "Rf6/rf6dbobject.h"
using namespace Rf6;
    typedef QSharedPointer<CRf6DbObject> pCRf6DbObject;
#endif
// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {


/** @brief Class implementing the UaObject interface for the OpcRf6Object.

 OPC UA Objects are used to represent systems, system components, real-world objects and software
 objects. They have the NodeClass @ref L3UaNodeClassObject. The detailed description of Objects and their attributes 
 can be found in the general description of the @ref L3UaNodeClassObject node class.<br>
 */
class OpcRf6_EXPORT OpcRf6Object: 
    public OpcRf6ObjectBase
{
    UA_DISABLE_COPY(OpcRf6Object);
protected:
    // destruction
    virtual ~OpcRf6Object();
public:
    // construction
    OpcRf6Object(const UaNodeId& nodeId, UaObject* pInstanceDeclarationObject, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex = NULL);
    OpcRf6Object(const UaNodeId& nodeId, const UaString& name, OpcUa_UInt16 browseNameNameSpaceIndex, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex = NULL);
    static void createTypes();
    static void clearStaticMembers();
#ifdef    RF6_COMPILED
    virtual void updateFromRf6(pCRf6DbObject rf6obj);
    virtual void toRf6(CRf6DbObject &ob);
#endif

 
protected:
    
private:
};

} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 

#endif // #ifndef __OPCRF6OPCRF6OBJECT_H__


