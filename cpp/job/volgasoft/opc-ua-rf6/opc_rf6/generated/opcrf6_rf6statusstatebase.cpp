/******************************************************************************
** opcrf6_rf6statusstatebase.cpp
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#include "opcrf6_rf6statusstatebase.h"
#include "uagenericnodes.h"
#include "nodemanagerroot.h"
#include "opcrf6_nodemanageropcrf6.h"

// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {

bool Rf6StatusStateBase::s_typeNodesCreated = false;
OpcUa::PropertyType* Rf6StatusStateBase::s_palarmAction = NULL;
OpcUa::PropertyType* Rf6StatusStateBase::s_palarmClass = NULL;
OpcUa::PropertyType* Rf6StatusStateBase::s_palarmPriority = NULL;
OpcUa::PropertyType* Rf6StatusStateBase::s_pcolor = NULL;
OpcUa::PropertyType* Rf6StatusStateBase::s_pcontrolId_ = NULL;

/** Constructs an instance of the class Rf6StatusStateBase with all components
*/
Rf6StatusStateBase::Rf6StatusStateBase(
    UaNode*            pParentNode,  //!< [in] Parent node of the new variable
    UaVariable*        pInstanceDeclarationVariable, //!< [in] UaVariable interface of the instance declaration node used to provide attribute values other than NodeId and Value 
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the variable. Can be NULL if no shared mutex is provided
: OpcRf6::OpcRf6DatabaseType(pParentNode, pInstanceDeclarationVariable, pNodeConfig, pSharedMutex)
{
    initialize(pNodeConfig);
}

/** Constructs an instance of the class Rf6StatusStateBase with all components
*/
Rf6StatusStateBase::Rf6StatusStateBase(
    const UaNodeId&    nodeId,       //!< [in] NodeId of the new variable
    const UaString&    name,         //!< [in] Name of the new variable. Used as browse name and also as display name if no additional language specific names are set.
    OpcUa_UInt16       browseNameNameSpaceIndex, //!< [in] Namespace index used for the browse name
    const UaVariant&   initialValue, //!< [in] Initial value for the Variable
    OpcUa_Byte         accessLevel,  //!< [in] Access level for the Variable
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the variable. Can be NULL if no shared mutex is provided
: OpcRf6::OpcRf6DatabaseType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex)
{
    initialize(pNodeConfig);
}

/** Initialize the variable with all member nodes 
*/
void Rf6StatusStateBase::initialize(NodeManagerConfig* pNodeConfig)
{
    UaStatus      addStatus;

    if ( s_typeNodesCreated == false )
    {
        createTypes();
    }

    // Mandatory variable alarmAction
    m_palarmAction = new OpcUa::PropertyType(this, s_palarmAction, pNodeConfig, getSharedMutex());
    addStatus = pNodeConfig->addNodeAndReference(this, m_palarmAction, OpcUaId_HasProperty);
    UA_ASSERT(addStatus.isGood());

    // Mandatory variable alarmClass
    m_palarmClass = new OpcUa::PropertyType(this, s_palarmClass, pNodeConfig, getSharedMutex());
    addStatus = pNodeConfig->addNodeAndReference(this, m_palarmClass, OpcUaId_HasProperty);
    UA_ASSERT(addStatus.isGood());

    // Mandatory variable alarmPriority
    m_palarmPriority = new OpcUa::PropertyType(this, s_palarmPriority, pNodeConfig, getSharedMutex());
    addStatus = pNodeConfig->addNodeAndReference(this, m_palarmPriority, OpcUaId_HasProperty);
    UA_ASSERT(addStatus.isGood());

    // Mandatory variable color
    m_pcolor = new OpcUa::PropertyType(this, s_pcolor, pNodeConfig, getSharedMutex());
    addStatus = pNodeConfig->addNodeAndReference(this, m_pcolor, OpcUaId_HasProperty);
    UA_ASSERT(addStatus.isGood());

    // Mandatory variable controlId_
    m_pcontrolId_ = new OpcUa::PropertyType(this, s_pcontrolId_, pNodeConfig, getSharedMutex());
    addStatus = pNodeConfig->addNodeAndReference(this, m_pcontrolId_, OpcUaId_HasProperty);
    UA_ASSERT(addStatus.isGood());
}

/** Destruction
*/
Rf6StatusStateBase::~Rf6StatusStateBase()
{
}

/** Create the related type nodes
*/
void Rf6StatusStateBase::createTypes()
{
    if ( s_typeNodesCreated == false )
    {
        s_typeNodesCreated = true;
        UaStatus      addStatus;
        UaVariant     defaultValue;
        NodeManagerRoot* pNodeManagerRoot = NodeManagerRoot::CreateRootNodeManager();
        OpcUa_Int16 nsTypeIdx = NodeManagerOpcRf6::getTypeNamespace();
        NodeManagerConfig* pTypeNodeConfig = pNodeManagerRoot->getNodeManagerByNamespace(nsTypeIdx)->getNodeManagerConfig();

        UaVariant                   nullValue;
        OpcUa::GenericVariableType* pVariableType;
        pVariableType = new OpcUa::GenericVariableType(
            UaNodeId(OpcRf6Id_Rf6StatusState, nsTypeIdx), 
            UaQualifiedName("Rf6StatusState", nsTypeIdx), 
            UaLocalizedText("", "Rf6StatusState"), 
            UaLocalizedText("", ""), 
            nullValue,
            UaNodeId(24, NodeManagerRoot::getTypeNamespace()),
            -1,
            OpcUa_False,
            &clearStaticMembers);
        pTypeNodeConfig->addNodeAndReference(UaNodeId(OpcRf6Id_OpcRf6DatabaseType, NodeManagerOpcRf6::getTypeNamespace()), pVariableType, OpcUaId_HasSubtype);

        // Mandatory variable alarmAction
        defaultValue.setByte(0);
        s_palarmAction = new OpcUa::PropertyType(UaNodeId(OpcRf6Id_Rf6StatusState_alarmAction, nsTypeIdx), "alarmAction", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_palarmAction->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pVariableType, s_palarmAction, OpcUaId_HasProperty);
        UA_ASSERT(addStatus.isGood());

        // Mandatory variable alarmClass
        defaultValue.setByte(0);
        s_palarmClass = new OpcUa::PropertyType(UaNodeId(OpcRf6Id_Rf6StatusState_alarmClass, nsTypeIdx), "alarmClass", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_palarmClass->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pVariableType, s_palarmClass, OpcUaId_HasProperty);
        UA_ASSERT(addStatus.isGood());

        // Mandatory variable alarmPriority
        defaultValue.setByte(0);
        s_palarmPriority = new OpcUa::PropertyType(UaNodeId(OpcRf6Id_Rf6StatusState_alarmPriority, nsTypeIdx), "alarmPriority", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_palarmPriority->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pVariableType, s_palarmPriority, OpcUaId_HasProperty);
        UA_ASSERT(addStatus.isGood());

        // Mandatory variable color
        defaultValue.setUInt32(0);
        s_pcolor = new OpcUa::PropertyType(UaNodeId(OpcRf6Id_Rf6StatusState_color, nsTypeIdx), "color", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_pcolor->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pVariableType, s_pcolor, OpcUaId_HasProperty);
        UA_ASSERT(addStatus.isGood());

        // Mandatory variable controlId_
        defaultValue.setByte(0);
        s_pcontrolId_ = new OpcUa::PropertyType(UaNodeId(OpcRf6Id_Rf6StatusState_controlId_, nsTypeIdx), "controlId_", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_pcontrolId_->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pVariableType, s_pcontrolId_, OpcUaId_HasProperty);
        UA_ASSERT(addStatus.isGood());

    }
}

/** Clear the static members of the class
*/
void Rf6StatusStateBase::clearStaticMembers()
{
    s_typeNodesCreated = false;
}

/** Returns the type definition NodeId for the Rf6StatusState
*/
UaNodeId Rf6StatusStateBase::typeDefinitionId() const
{
    UaNodeId ret(OpcRf6Id_Rf6StatusState, NodeManagerOpcRf6::getTypeNamespace());
    return ret;
}

// alarmAction
void Rf6StatusStateBase::setalarmAction(OpcUa_Byte alarmAction)
{
    UaVariant value;
    value.setByte(alarmAction);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_palarmAction->setValue(NULL, dataValue, OpcUa_False);
}
OpcUa_Byte Rf6StatusStateBase::getalarmAction()
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    OpcUa_Byte ret;
    if ( m_palarmAction == NULL )
    {
        defaultValue.setByte(0);
    }
    else
    {
        UaDataValue dataValue(m_palarmAction->value(NULL));
        defaultValue = *dataValue.value();
    }
    defaultValue.toByte(ret);
    return ret;
}

// alarmClass
void Rf6StatusStateBase::setalarmClass(OpcUa_Byte alarmClass)
{
    UaVariant value;
    value.setByte(alarmClass);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_palarmClass->setValue(NULL, dataValue, OpcUa_False);
}
OpcUa_Byte Rf6StatusStateBase::getalarmClass()
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    OpcUa_Byte ret;
    if ( m_palarmClass == NULL )
    {
        defaultValue.setByte(0);
    }
    else
    {
        UaDataValue dataValue(m_palarmClass->value(NULL));
        defaultValue = *dataValue.value();
    }
    defaultValue.toByte(ret);
    return ret;
}

// alarmPriority
void Rf6StatusStateBase::setalarmPriority(OpcUa_Byte alarmPriority)
{
    UaVariant value;
    value.setByte(alarmPriority);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_palarmPriority->setValue(NULL, dataValue, OpcUa_False);
}
OpcUa_Byte Rf6StatusStateBase::getalarmPriority()
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    OpcUa_Byte ret;
    if ( m_palarmPriority == NULL )
    {
        defaultValue.setByte(0);
    }
    else
    {
        UaDataValue dataValue(m_palarmPriority->value(NULL));
        defaultValue = *dataValue.value();
    }
    defaultValue.toByte(ret);
    return ret;
}

// color
void Rf6StatusStateBase::setcolor(OpcUa_UInt32 color)
{
    UaVariant value;
    value.setUInt32(color);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_pcolor->setValue(NULL, dataValue, OpcUa_False);
}
OpcUa_UInt32 Rf6StatusStateBase::getcolor()
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    OpcUa_UInt32 ret;
    if ( m_pcolor == NULL )
    {
        defaultValue.setUInt32(0);
    }
    else
    {
        UaDataValue dataValue(m_pcolor->value(NULL));
        defaultValue = *dataValue.value();
    }
    defaultValue.toUInt32(ret);
    return ret;
}

// controlId_
void Rf6StatusStateBase::setcontrolId_(OpcUa_Byte controlId_)
{
    UaVariant value;
    value.setByte(controlId_);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_pcontrolId_->setValue(NULL, dataValue, OpcUa_False);
}
OpcUa_Byte Rf6StatusStateBase::getcontrolId_()
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    OpcUa_Byte ret;
    if ( m_pcontrolId_ == NULL )
    {
        defaultValue.setByte(0);
    }
    else
    {
        UaDataValue dataValue(m_pcontrolId_->value(NULL));
        defaultValue = *dataValue.value();
    }
    defaultValue.toByte(ret);
    return ret;
}

} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 


