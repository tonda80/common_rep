/******************************************************************************
** opcrf6_rf6projectbase.cpp
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#include "opcrf6_rf6projectbase.h"
#include "opcrf6_rf6project.h"
#include "uagenericnodes.h"
#include "nodemanagerroot.h"
#include "methodhandleuanode.h"
#include "opcrf6_nodemanageropcrf6.h"

// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {

bool Rf6ProjectBase::s_typeNodesCreated = false;
OpcUa::BaseDataVariableType* Rf6ProjectBase::s_pip = NULL;
OpcUa::BaseDataVariableType* Rf6ProjectBase::s_pqnxName = NULL;
OpcUa::BaseDataVariableType* Rf6ProjectBase::s_prfVersion = NULL;
OpcUa::FolderType* Rf6ProjectBase::s_pChannels = NULL;
OpcUa::FolderType* Rf6ProjectBase::s_pPCUs = NULL;

/** Constructs an Rf6Project object using an instance declaration node as base
*/
Rf6ProjectBase::Rf6ProjectBase(const UaNodeId& nodeId, UaObject* pInstanceDeclarationObject, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex)
: OpcRf6::OpcRf6Object(nodeId, pInstanceDeclarationObject, pNodeConfig, pSharedMutex)
{
    initialize();
}

/** Constructs an Rf6Project object
*/
Rf6ProjectBase::Rf6ProjectBase(const UaNodeId& nodeId, const UaString& name, OpcUa_UInt16 browseNameNameSpaceIndex, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex)
: OpcRf6::OpcRf6Object(nodeId, name, browseNameNameSpaceIndex, pNodeConfig, pSharedMutex)
{
    initialize();
}

/** Initialize the object with all member nodes 
*/
void Rf6ProjectBase::initialize()
{
    OpcUa_Int16 nsIdx = m_pNodeConfig->getNameSpaceIndex();
    UaString      defaultLocaleId  = ""; // ToDo Get from Model
    UaStatus      addStatus;
    UaVariant     defaultValue;

    if ( s_typeNodesCreated == false )
    {
        createTypes();
    }

    // Object Channels
    m_pChannels = new OpcUa::FolderType(UaNodeId(UaString("%1.Channels").arg(nodeId().toString()), nsIdx), s_pChannels, m_pNodeConfig, m_pSharedMutex);
    m_pNodeConfig->addNodeAndReference(this, m_pChannels, OpcUaId_Organizes);
    // Object PCUs
    m_pPCUs = new OpcUa::FolderType(UaNodeId(UaString("%1.PCUs").arg(nodeId().toString()), nsIdx), s_pPCUs, m_pNodeConfig, m_pSharedMutex);
    m_pNodeConfig->addNodeAndReference(this, m_pPCUs, OpcUaId_Organizes);



    // Mandatory variable ip
    m_pip = new OpcUa::BaseDataVariableType(this, s_pip, m_pNodeConfig, m_pSharedMutex);
    addStatus = m_pNodeConfig->addNodeAndReference(this, m_pip, OpcUaId_HasComponent);
    UA_ASSERT(addStatus.isGood());
    // ToDoDataType variable mode
    // ToDo DataType not supported yet    
    // Mandatory variable qnxName
    m_pqnxName = new OpcUa::BaseDataVariableType(this, s_pqnxName, m_pNodeConfig, m_pSharedMutex);
    addStatus = m_pNodeConfig->addNodeAndReference(this, m_pqnxName, OpcUaId_HasComponent);
    UA_ASSERT(addStatus.isGood());
    // Mandatory variable rfVersion
    m_prfVersion = new OpcUa::BaseDataVariableType(this, s_prfVersion, m_pNodeConfig, m_pSharedMutex);
    addStatus = m_pNodeConfig->addNodeAndReference(this, m_prfVersion, OpcUaId_HasComponent);
    UA_ASSERT(addStatus.isGood());

}

/** Destruction
*/
Rf6ProjectBase::~Rf6ProjectBase()
{
}

/** Create the related type nodes
*/
void Rf6ProjectBase::createTypes()
{
    if ( s_typeNodesCreated == false )
    {
        s_typeNodesCreated = true;

        // Check if supertype is already created
        OpcRf6::OpcRf6Object::createTypes();

        UaStatus      addStatus;
        UaVariant     defaultValue;
        NodeManagerRoot* pNodeManagerRoot = NodeManagerRoot::CreateRootNodeManager();
        OpcUa_Int16 nsTypeIdx = NodeManagerOpcRf6::getTypeNamespace();
        OpcUa_Int16 nsSuperTypeIdx = OpcRf6::NodeManagerOpcRf6::getTypeNamespace();
        NodeManagerConfig* pTypeNodeConfig = pNodeManagerRoot->getNodeManagerByNamespace(nsTypeIdx)->getNodeManagerConfig();

        UaString           sOpcFLocale  = "";
        OpcUa::GenericObjectType* pObjectType;
        pObjectType = new OpcUa::GenericObjectType(
            UaNodeId(OpcRf6Id_Rf6Project, nsTypeIdx), 
            UaQualifiedName("Rf6Project", nsTypeIdx), 
            UaLocalizedText("", "Rf6Project"), 
            UaLocalizedText("", ""), 
            OpcUa_False,
            &Rf6Project::clearStaticMembers);
        pTypeNodeConfig->addNodeAndReference(UaNodeId(OpcRf6Id_OpcRf6Object, nsSuperTypeIdx), pObjectType, OpcUaId_HasSubtype);

        // Object Channels
        s_pChannels = new OpcUa::FolderType(UaNodeId(OpcRf6Id_Rf6Project_Channels, nsTypeIdx), "Channels", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), pTypeNodeConfig);
        s_pChannels->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        pTypeNodeConfig->addNodeAndReference(pObjectType, s_pChannels, OpcUaId_Organizes);
        // Object PCUs
        s_pPCUs = new OpcUa::FolderType(UaNodeId(OpcRf6Id_Rf6Project_PCUs, nsTypeIdx), "PCUs", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), pTypeNodeConfig);
        s_pPCUs->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        pTypeNodeConfig->addNodeAndReference(pObjectType, s_pPCUs, OpcUaId_Organizes);


        // Mandatory variable ip
        defaultValue.setString("");
        s_pip = new OpcUa::BaseDataVariableType(UaNodeId(OpcRf6Id_Rf6Project_ip, nsTypeIdx), "ip", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_pip->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pObjectType, s_pip, OpcUaId_HasComponent);
        UA_ASSERT(addStatus.isGood());
        
        // ToDoDataType variable mode
        // ToDo DataType not supported yet    
        // Mandatory variable qnxName
        defaultValue.setString("");
        s_pqnxName = new OpcUa::BaseDataVariableType(UaNodeId(OpcRf6Id_Rf6Project_qnxName, nsTypeIdx), "qnxName", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_pqnxName->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pObjectType, s_pqnxName, OpcUaId_HasComponent);
        UA_ASSERT(addStatus.isGood());
        
        // Mandatory variable rfVersion
        defaultValue.setString("");
        s_prfVersion = new OpcUa::BaseDataVariableType(UaNodeId(OpcRf6Id_Rf6Project_rfVersion, nsTypeIdx), "rfVersion", OpcRf6::NodeManagerOpcRf6::getTypeNamespace(), defaultValue, 3, pTypeNodeConfig);
        s_prfVersion->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
        addStatus = pTypeNodeConfig->addNodeAndReference(pObjectType, s_prfVersion, OpcUaId_HasComponent);
        UA_ASSERT(addStatus.isGood());
        
    }
}

/** Clear the static members of the class
*/
void Rf6ProjectBase::clearStaticMembers()
{
    s_typeNodesCreated = false;

    s_pip = NULL;
    s_pqnxName = NULL;
    s_prfVersion = NULL;
    s_pChannels = NULL;
    s_pPCUs = NULL;
}

/** Returns the type definition NodeId for the Rf6Project
*/
UaNodeId Rf6ProjectBase::typeDefinitionId() const
{
    UaNodeId ret(OpcRf6Id_Rf6Project, NodeManagerOpcRf6::getTypeNamespace());
    return ret;
}

/** Implementation of the MethodManager interface method beginCall
*/
UaStatus Rf6ProjectBase::beginCall(
    MethodManagerCallback* pCallback,
    const ServiceContext&  serviceContext,
    OpcUa_UInt32           callbackHandle,
    MethodHandle*          pMethodHandle,
    const UaVariantArray&  inputArguments)
{
    UaStatus            ret;
    UaVariantArray      outputArguments;
    UaStatusCodeArray   inputArgumentResults;
    UaDiagnosticInfos   inputArgumentDiag;
    MethodHandleUaNode* pMethodHandleUaNode = (MethodHandleUaNode*)pMethodHandle;
    UaMethod*           pMethod             = NULL; 

    if(pMethodHandleUaNode)
    {
        pMethod = pMethodHandleUaNode->pUaMethod();

        if(pMethod)
        {
            return OpcRf6Object::beginCall(pCallback, serviceContext, callbackHandle, pMethodHandle, inputArguments);
        }
        else
        {
            assert(false);
            ret = OpcUa_BadInvalidArgument;
        }
    }
    else
    {
        assert(false);
        ret = OpcUa_BadInvalidArgument;
    }

    return ret;
}

// ip
void Rf6ProjectBase::setip(const UaString& ip)
{
    UaVariant value;
    value.setString(ip);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_pip->setValue(NULL, dataValue, OpcUa_False);
}
UaString Rf6ProjectBase::getip() const
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    UaString ret;
    if ( m_pip == NULL )
    {
        defaultValue.setString("");
    }
    else
    {
        UaDataValue dataValue(m_pip->value(NULL));
        defaultValue = *dataValue.value();
    }
    ret = defaultValue.toString();
    return ret;
}

// mode
// ToDo DataType not supported yet    

// qnxName
void Rf6ProjectBase::setqnxName(const UaString& qnxName)
{
    UaVariant value;
    value.setString(qnxName);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_pqnxName->setValue(NULL, dataValue, OpcUa_False);
}
UaString Rf6ProjectBase::getqnxName() const
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    UaString ret;
    if ( m_pqnxName == NULL )
    {
        defaultValue.setString("");
    }
    else
    {
        UaDataValue dataValue(m_pqnxName->value(NULL));
        defaultValue = *dataValue.value();
    }
    ret = defaultValue.toString();
    return ret;
}

// rfVersion
void Rf6ProjectBase::setrfVersion(const UaString& rfVersion)
{
    UaVariant value;
    value.setString(rfVersion);
    UaDataValue dataValue;
    dataValue.setValue(value, OpcUa_True, OpcUa_True);
    m_prfVersion->setValue(NULL, dataValue, OpcUa_False);
}
UaString Rf6ProjectBase::getrfVersion() const
{
    UaDataValue dataValue;
    UaVariant defaultValue;
    UaString ret;
    if ( m_prfVersion == NULL )
    {
        defaultValue.setString("");
    }
    else
    {
        UaDataValue dataValue(m_prfVersion->value(NULL));
        defaultValue = *dataValue.value();
    }
    ret = defaultValue.toString();
    return ret;
}
OpcUa::FolderType* Rf6ProjectBase::getChannels()
{
    return m_pChannels;
}

OpcUa::FolderType* Rf6ProjectBase::getPCUs()
{
    return m_pPCUs;
}


} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 


