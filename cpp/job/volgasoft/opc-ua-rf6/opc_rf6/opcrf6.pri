INCLUDEPATH += $$UASDK_HOME/include/uaserver \
    opc_rf6/generated \
    opc_rf6 \
	opc_sql

HEADERS += opc_rf6/opcrf6_rf6tanktag.h \
    opc_rf6/opcrf6_rf6statustag.h \
    opc_rf6/opcrf6_rf6statusstate.h \
    opc_rf6/opcrf6_rf6roc.h \
    opc_rf6/opcrf6_rf6pcu.h \
    opc_rf6/opcrf6_rf6metertag.h \
    opc_rf6/opcrf6_rf6header.h \
    opc_rf6/opcrf6_rf6communicationstatistics.h \
    opc_rf6/opcrf6_rf6analogtag.h \
    opc_rf6/opcrf6_rf6analoglimit.h \
    opc_rf6/opcrf6_opcrf6object.h \
    opc_rf6/opcrf6_opcrf6databasetype.h \
    opc_rf6/opcrf6_nodemanageropcrf6.h \
    opc_rf6/generated/opcrf6_rf6tanktagbase.h \
    opc_rf6/generated/opcrf6_rf6statustagbase.h \
    opc_rf6/generated/opcrf6_rf6statusstatebase.h \
    opc_rf6/generated/opcrf6_rf6rocbase.h \
    opc_rf6/generated/opcrf6_rf6pcubase.h \
    opc_rf6/generated/opcrf6_rf6metertagbase.h \
    opc_rf6/generated/opcrf6_rf6headerbase.h \
    opc_rf6/generated/opcrf6_rf6communicationstatisticsbase.h \
    opc_rf6/generated/opcrf6_rf6analogtagbase.h \
    opc_rf6/generated/opcrf6_rf6analoglimitbase.h \
    opc_rf6/generated/opcrf6_opcrf6objectbase.h \
    opc_rf6/generated/opcrf6_opcrf6databasetypebase.h \
    opc_rf6/generated/opcrf6_identifiers.h \
    opc_rf6/generated/opcrf6_datatypes.h \
    opc_rf6/generated/opcrf6_rf6projectbase.h \
    opc_rf6/opcrf6_rf6project.h \
    opc_rf6/opcrf6_rf6channel.h \
    opc_rf6/generated/opcrf6_rf6channelbase.h \
    opc_rf6/opcrf6_rf6telemetry.h \
    opc_rf6/generated/opcrf6_rf6telemetrybase.h \
    opc_rf6/opcrf6_rf6aliasvariable.h \
    opc_rf6/generated/opcrf6_rf6aliasvariablebase.h\
    opc_rf6/opcrf6_rf6aliasobject.h \
    opc_rf6/generated/opcrf6_rf6aliasobjectbase.h


SOURCES +=  opc_rf6/opcrf6_rf6tanktag.cpp \
    opc_rf6/opcrf6_rf6statustag.cpp \
    opc_rf6/opcrf6_rf6statusstate.cpp \
    opc_rf6/opcrf6_rf6roc.cpp \
    opc_rf6/opcrf6_rf6pcu.cpp \
    opc_rf6/opcrf6_rf6metertag.cpp \
    opc_rf6/opcrf6_rf6header.cpp \
    opc_rf6/opcrf6_rf6communicationstatistics.cpp \
    opc_rf6/opcrf6_rf6analogtag.cpp \
    opc_rf6/opcrf6_rf6analoglimit.cpp \
    opc_rf6/opcrf6_opcrf6object.cpp \
    opc_rf6/opcrf6_opcrf6databasetype.cpp \
    opc_rf6/opcrf6_nodemanageropcrf6.cpp \
    opc_rf6/generated/opcrf6_rf6tanktagbase.cpp \
    opc_rf6/generated/opcrf6_rf6statustagbase.cpp \
    opc_rf6/generated/opcrf6_rf6statusstatebase.cpp \
    opc_rf6/generated/opcrf6_rf6rocbase.cpp \
    opc_rf6/generated/opcrf6_rf6pcubase.cpp \
    opc_rf6/generated/opcrf6_rf6metertagbase.cpp \
    opc_rf6/generated/opcrf6_rf6headerbase.cpp \
    opc_rf6/generated/opcrf6_rf6communicationstatisticsbase.cpp \
    opc_rf6/generated/opcrf6_rf6analogtagbase.cpp \
    opc_rf6/generated/opcrf6_rf6analoglimitbase.cpp \
    opc_rf6/generated/opcrf6_opcrf6objectbase.cpp \
    opc_rf6/generated/opcrf6_opcrf6databasetypebase.cpp \
    opc_rf6/generated/opcrf6_rf6projectbase.cpp \
    opc_rf6/opcrf6_rf6project.cpp \
    opc_rf6/opcrf6_rf6channel.cpp \
    opc_rf6/generated/opcrf6_rf6channelbase.cpp \
    opc_rf6/opcrf6_rf6telemetry.cpp \
    opc_rf6/generated/opcrf6_rf6telemetrybase.cpp \
    opc_rf6/opcrf6_rf6aliasvariable.cpp \
    opc_rf6/generated/opcrf6_rf6aliasvariablebase.cpp\
    opc_rf6/opcrf6_rf6aliasobject.cpp \
    opc_rf6/generated/opcrf6_rf6aliasobjectbase.cpp





