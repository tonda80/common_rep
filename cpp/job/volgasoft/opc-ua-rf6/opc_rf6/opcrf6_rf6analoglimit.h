/******************************************************************************
** opcrf6_rf6analoglimit.h
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#ifndef __OPCRF6_RF6ANALOGLIMIT_H__
#define __OPCRF6_RF6ANALOGLIMIT_H__

#include "opcrf6_rf6analoglimitbase.h"

#include"dbftype.h"

// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {

/** Class representing OPC UA Variable instances of the type Rf6AnalogLimit 
*/
class OpcRf6_EXPORT Rf6AnalogLimit: 
    public Rf6AnalogLimitBase
{
    UA_DISABLE_COPY(Rf6AnalogLimit);
public:

    // construction / destruction
    Rf6AnalogLimit(
        UaNode*            pParentNode,
        UaVariable*        pInstanceDeclarationVariable, 
        NodeManagerConfig* pNodeConfig,
        UaMutexRefCounted* pSharedMutex = NULL);
    Rf6AnalogLimit(
        const UaNodeId&    nodeId, 
        const UaString&    name, 
        OpcUa_UInt16       browseNameNameSpaceIndex, 
        const UaVariant&   initialValue,
        OpcUa_Byte         accessLevel,
        NodeManagerConfig* pNodeConfig,
        UaMutexRefCounted* pSharedMutex = NULL);
    void fromRf6Structure(NANALOG_LIMIT structure);
    NANALOG_LIMIT toRf6Structure();

    virtual ~Rf6AnalogLimit();

protected:
    
private:

};

} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 

#endif // #ifndef __OPCRF6RF6ANALOGLIMIT_H__


