/******************************************************************************
** opcrf6_rf6communicationstatistics.h
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#ifndef __OPCRF6_RF6COMMUNICATIONSTATISTICS_H__
#define __OPCRF6_RF6COMMUNICATIONSTATISTICS_H__

#include "opcrf6_opcrf6object.h"
#include "opcrf6_identifiers.h"
#include "opcrf6_rf6communicationstatisticsbase.h"

#include "dbftype.h"

// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {


/** @brief Class implementing the UaObject interface for the Rf6CommunicationStatistics.

 OPC UA Objects are used to represent systems, system components, real-world objects and software
 objects. They have the NodeClass @ref L3UaNodeClassObject. The detailed description of Objects and their attributes 
 can be found in the general description of the @ref L3UaNodeClassObject node class.<br>
 */
class OpcRf6_EXPORT Rf6CommunicationStatistics: 
    public Rf6CommunicationStatisticsBase
{
    UA_DISABLE_COPY(Rf6CommunicationStatistics);
protected:
    // destruction
    virtual ~Rf6CommunicationStatistics();
public:
    // construction
    Rf6CommunicationStatistics(const UaNodeId& nodeId, UaObject* pInstanceDeclarationObject, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex = NULL);
    Rf6CommunicationStatistics(const UaNodeId& nodeId, const UaString& name, OpcUa_UInt16 browseNameNameSpaceIndex, NodeManagerConfig* pNodeConfig, UaMutexRefCounted* pSharedMutex = NULL);
    static void createTypes();
    static void clearStaticMembers();
    void fromRf6Structure(NR_COMM_STATS structure);
    NR_COMM_STATS toRf6Structure();
 
protected:
    
private:
};

} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 

#endif // #ifndef __OPCRF6RF6COMMUNICATIONSTATISTICS_H__


