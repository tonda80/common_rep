/******************************************************************************
** opcrf6_rf6metertag.cpp
**
** Copyright (C) 2008-2011 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK information model for namespace http://schema.realflex.com/rf6opcserver/
**
** Description: File generated by UaModeler
**              Template C++ OPC UA SDK 1.3.1
**
******************************************************************************/

#include "opcrf6_rf6metertag.h"
#include "uagenericnodes.h"
#include "nodemanagerroot.h"
#include "opcrf6_nodemanageropcrf6.h"

#ifdef  RF6_COMPILED
    #include "Rf6/rf6dbobject.h"
#endif
// Namespace for the UA information model http://schema.realflex.com/rf6opcserver/
namespace OpcRf6 {

/** Construction of the class Rf6MeterTag.
 This constructor is used if the object is created based on an instance declaration of an object type. Since the only difference between the 
 instance declaration object and the instance object are the node id, the pointer to the instance declaration node is used to get the attribute
 values other than NodeId from the instance declaration node.<br>
 The object is reference counted and can not be deleted directly. The reference counter is set to one after the object is created. 
 If the object was successfully added to a NodeManager using the methods NodeManagerConfig::addUaNode or NodeManagerConfig::addNodeAndReference the 
 NodeManager will release this reference during shut down. If the object was not added to a NodeManager, it must be deleted using the releaseReference 
 method. If the interface pointer is used in other places, the availability of the object must be ensured by incrementing the reference counter with 
 addReference when the pointer is stored somewhere and decremented with releaseReference if the interface pointer is not longer needed.
 */
Rf6MeterTag::Rf6MeterTag(
    const UaNodeId& nodeId,          //!< [in] NodeId of the new object
    UaObject* pInstanceDeclarationObject, //!< [in] UaObject interface of the instance declaration node used to provide attribute values other than NodeId
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the object. Can be NULL if no shared mutex is provided
: Rf6MeterTagBase(nodeId, pInstanceDeclarationObject, pNodeConfig, pSharedMutex)
{
}

/** Construction of the class Rf6MeterTag.
 This constructor is used if the object is not created based on an instance declaration. It defines the name and node id of the object. Additional language specific
 names and other attribute values can be set with set methods provided by the class.
 The object is reference counted and can not be deleted directly. The reference counter is set to one after the object is created. 
 If the object was successfully added to a NodeManager using the methods NodeManagerConfig::addUaNode or NodeManagerConfig::addNodeAndReference the 
 NodeManager will release this reference during shut down. If the object was not added to a NodeManager, it must be deleted using the releaseReference 
 method. If the interface pointer is used in other places, the availability of the object must be ensured by incrementing the reference counter with 
 addReference when the pointer is stored somewhere and decremented with releaseReference if the interface pointer is not longer needed.
 */
Rf6MeterTag::Rf6MeterTag(
    const UaNodeId& nodeId,          //!< [in] NodeId of the new object
    const UaString& name,            //!< [in] Name of the new object. Used as browse name and also as display name if no additional language specific names are set.
    OpcUa_UInt16 browseNameNameSpaceIndex, //!< [in] Namespace index used for the browse name
    NodeManagerConfig* pNodeConfig,  //!< [in] Interface pointer to the NodeManagerConfig interface used to add and delete node and references in the address space
    UaMutexRefCounted* pSharedMutex) //!< [in] Shared mutex object used to synchronize access to the object. Can be NULL if no shared mutex is provided
: Rf6MeterTagBase(nodeId, name, browseNameNameSpaceIndex, pNodeConfig, pSharedMutex)
{
}

/** Destruction
*/
Rf6MeterTag::~Rf6MeterTag()
{
}

/** Create the type related static members of the class
*/
void Rf6MeterTag::createTypes()
{
    // Call base class
    Rf6MeterTagBase::createTypes();

    // Create your static members here
    // This method may be called several times
}

/** Clear the static members of the class
*/
void Rf6MeterTag::clearStaticMembers()
{
    // Clear your static members here
    // Call base class
    Rf6MeterTagBase::clearStaticMembers();
}

void Rf6MeterTag::fromRf6Structure(NMETER_REC structure)
{

    setcurrentGross(structure.curr_gross);
    setcurrentNet(structure.curr_value);
    setdaily(structure.daily);

   // setdbOffset(structure.);
    setfactor(structure.factor);
    setfrac(structure.frac);    
    sethourly(structure.hourly);
    setlastGood(structure.last_good);
    setlastHour(structure.last_hour);

    setmaxRaw(structure.max_raw);
    setmonthly(structure.monthly);
    setppunit(structure.ppunit);

    setrollover(structure.rollover);
    settype(structure.meter_type);
    setyearly(structure.yearly);
    setyesterday(structure.yesterday);
    // set header object
    getheader()->fromRf6Structure(structure.hdr);


}


#ifdef  RF6_COMPILED
void Rf6MeterTag::updateFromRf6(pCRf6DbObject rf6obj)
{
    if (NULL !=rf6obj->PtrMetData())
        fromRf6Structure(*rf6obj->PtrMetData());
}
void Rf6MeterTag::toRf6(CRf6DbObject &ob)
{
    if (NULL != ob.PtrMetData()){
       *(ob.PtrMetData()) = toRf6Structure();
    }
}
#endif

NMETER_REC Rf6MeterTag::toRf6Structure()
{
    NMETER_REC meterStructure;

    meterStructure.curr_gross = getcurrentGross();
    meterStructure.curr_value = getcurrentNet();
    meterStructure.daily = getdaily();


    meterStructure.factor = getfactor();
    meterStructure.frac = getfrac();

    meterStructure.hourly = gethourly();
    meterStructure.last_good = getlastGood();
    meterStructure.last_hour = getlastHour();

    meterStructure.max_raw = getmaxRaw();
    meterStructure.monthly = getmonthly();
    meterStructure.ppunit = getppunit();

    meterStructure.rollover = getrollover();
    meterStructure.meter_type = gettype();
    meterStructure.yearly = getyearly();

    meterStructure.yesterday = getyesterday();
    // set header
    meterStructure.hdr = getheader()->toRf6Structure();

    return meterStructure;

}



} // End namespace for the UA information model http://schema.realflex.com/rf6opcserver/ 


