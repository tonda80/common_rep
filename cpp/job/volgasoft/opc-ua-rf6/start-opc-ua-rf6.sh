#!/bin/sh

NEW_LIB_PATH="/usr/pkg/lib"
ST=`echo $LD_LIBRARY_PATH | grep $NEW_LIB_PATH`
if [ ! $ST ]
then
	echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"$NEW_LIB_PATH
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$NEW_LIB_PATH
fi

NEW_LIB_PATH="/usr/local/lib"
ST=`echo $LD_LIBRARY_PATH | grep $NEW_LIB_PATH`
if [ ! $ST ]
then
	echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"$NEW_LIB_PATH
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$NEW_LIB_PATH
fi

./opc-ua-rf6 $*
