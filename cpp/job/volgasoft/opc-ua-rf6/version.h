/*
 * version.h
 *
 *  Created on: 27.11.2012
 *      Author: aberezin
 */

#ifndef VERSION_H_
#define VERSION_H_

#define PROJECT_NAME "OPC-UA server"

#define MAJOR_VERSION_NUMBER 0
#define MINOR_VERSION_NUMBER 1

#endif /* VERSION_H_ */
