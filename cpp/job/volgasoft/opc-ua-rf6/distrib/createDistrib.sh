#!/bin/sh

# keep the current directory
BASEDIR=`pwd`/

# creation archive with necessary files
# --------------------------------------

# remove the old files
if [ -d tmpdir ]
then 
	rm -rf tmpdir
fi

# collection result
RESULT=0

# QT
cp -cp	\
	/usr/local/Trolltech/QtEmbedded-4.8.2-i386/lib/libQtCore.so.4.8.2	\
	/usr/local/Trolltech/QtEmbedded-4.8.2-i386/lib/libQtSql.so.4.8.2	\
	/usr/local/Trolltech/QtEmbedded-4.8.2-i386/lib/libQtNetwork.so.4.8.2 \
	tmpdir/usr/local/Trolltech/QtEmbedded-4.8.2-i386/lib/
let RESULT=$RESULT+$?	
cp -cp	\
	/usr/local/Trolltech/QtEmbedded-4.8.2-i386/plugins/sqldrivers/libqsqlite.so	\
	tmpdir/usr/local/Trolltech/QtEmbedded-4.8.2-i386/plugins/sqldrivers/
let RESULT=$RESULT+$?
# OPC-UA SDK
cp -cp	\
	/usr/local/lib/libuastack.so	\
	/usr/local/lib/libuastackd.so	\
	tmpdir/usr/local/lib
let RESULT=$RESULT+$?
# other things
cp -cp	\
	/usr/pkg/lib/libiconv.so.2.5.0	\
	/usr/pkg/lib/libxml2.so.2.7.8	\
	/usr/pkg/lib/libz.so.1.0.2	\
	tmpdir/usr/pkg/lib
let RESULT=$RESULT+$?
# creation of symbolic references	
cd tmpdir/usr/local/Trolltech/QtEmbedded-4.8.2-i386/lib/
ln -s libQtCore.so.4.8.2 libQtCore.so.4
let RESULT=$RESULT+$?
ln -s libQtSql.so.4.8.2 libQtSql.so.4
let RESULT=$RESULT+$?
ln -s libQtNetwork.so.4.8.2 libQtNetwork.so.4
let RESULT=$RESULT+$?
cd ${BASEDIR}tmpdir/usr/pkg/lib 
ln -s libiconv.so.2.5.0 libiconv.so.2
let RESULT=$RESULT+$?
ln -s libxml2.so.2.7.8 libxml2.so.2
let RESULT=$RESULT+$?
ln -s libz.so.1.0.2 libz.so.1
let RESULT=$RESULT+$?
cd ${BASEDIR}
# actually server
cp -cp ../opc-ua-rf6 ../ServerConfig.xml ../start-opc-ua-rf6.sh	\
	tmpdir/opc-ua-rf6/
let RESULT=$RESULT+$?

if [ RESULT -ne 0 ]
then 
	echo "Error. The necessary file(s) is missing"
	echo "Exiting"
	exit 1
fi

cd tmpdir
tar czf ../opc-ua-rf6-install.tgz *
if [ $? -ne 0 ]
then 
	echo "Error of a archive creation"
	echo "Exiting"
	exit 2
fi
cd ..

# Constructing install script
TGZ_SIZE=`ls -la opc-ua-rf6-install.tgz | awk '{print $5}'`

(cat << THE_END
#!/bin/sh

# Check if root starts the script
EUID=\`id -u\`
if [ \$EUID -ne 0 ]
then
	echo "You must be root user to install opc-ua-rf6 server"
	echo "Exiting"
	exit 1
fi

echo Installing opc-ua-rf6 server:
echo Installing Files...

# Extract
tail -c $TGZ_SIZE \$0 | tar xfz - -C /
if [ \$? -ne 0 ]
then 
	echo "Error of a archive extract"
	echo "Exiting"
	exit 2
fi

chown -R 0:realflex /opc-ua-rf6/opc-ua-rf6
chmod -R 775 /opc-ua-rf6/opc-ua-rf6

echo Installation complete
exit 0
THE_END
) > opc-ua-rf6.install
cat opc-ua-rf6-install.tgz >> opc-ua-rf6.install
chmod a+x opc-ua-rf6.install

# Clean up
rm opc-ua-rf6-install.tgz
rm -r tmpdir
