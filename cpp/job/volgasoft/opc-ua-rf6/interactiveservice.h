#ifndef INTERACTIVESERVICE_H
#define INTERACTIVESERVICE_H
#include <QCoreApplication>
#include "qtservice.h"

namespace Opc{
class OpcServer;
}
class InteractiveService : public QtService<QCoreApplication>
{
public:
    InteractiveService(int argc, char **argv);
    ~InteractiveService();

protected:

    void start();
    void stop();
    void pause();
    void resume();
    void processCommand(int code);

private:
    Opc::OpcServer* coreService;

};

#endif // INTERACTIVESERVICE_H
