#include "interactiveservice.h"
#include <QDebug>
#include <QtCore/QDir>
#include <QtCore/QSettings>
#include "uaplatformlayer.h"
#ifdef SUPPORT_XML_CONFIG
  #include "xmldocument.h"
#endif
#include "opcserver.h"
#include "shutdown.h"
#include "uaplatformlayer.h"
#include "uathread.h"

//for convert QString to UaString
#include "helper.h"

#include "opcua_basedatavariabletype.h"
#include "opcrf6_nodemanageropcrf6.h"
#include "opcalias_nodemanageropcalias.h"
#include "opcrf6_rf6pcu.h"

#include "version.h"

InteractiveService::InteractiveService(int argc, char **argv)
    : QtService<QCoreApplication>(argc, argv, PROJECT_NAME), coreService(NULL)
{
    setServiceDescription("OPC-UA server for realflex 6.");
    setServiceFlags(QtServiceBase::CanBeSuspended);
}

InteractiveService::~InteractiveService()
{
}

//starting opc_server
void InteractiveService::start()
{
    qDebug() << PROJECT_NAME << "is started";

    //- Initialize the environment --------------
#ifdef SUPPORT_XML_CONFIG
    // Initialize the XML Parser
    UaXmlDocument::initParser();
#endif
    //to do Extract application path char* szAppPath = Opc::getAppPath()
 //char* szAppPath;
    //-------------------------------------------
  // Initialize the UA Stack platform layer
    if ( 0 ==  UaPlatformLayer::init() )
    {
        //get path application for ServerConfig
        QString appPath = application()->applicationDirPath();
        QString sConfigFileName = appPath;

#ifdef SUPPORT_XML_CONFIG
        sConfigFileName += "/ServerConfig.xml";
#else
        sConfigFileName += "/ServerConfig.ini";
#endif

        //- Start up OPC server ---------------------
        //-------------------------------------------
        // Create and initialize server object
        coreService = new Opc::OpcServer;
        coreService->setServerConfig(Helper::QStringToUaString(sConfigFileName), Helper::QStringToUaString(appPath));
        //to do add nodemanager
       // BA::NodeManagerBA* pNodeManager = new BA::NodeManagerBA(OpcUa_True);
        //pServer->addNodeManager(pNodeManager);
        using namespace OpcRf6;
        OpcRf6::NodeManagerOpcRf6 *pNodeManager = new OpcRf6::NodeManagerOpcRf6(OpcUa_True);
        coreService->addNodeManager(pNodeManager);
        OpcAlias::NodeManagerOpcAlias  *pNodeManagerAlias = new OpcAlias::NodeManagerOpcAlias(OpcUa_True);
        coreService->addNodeManager(pNodeManagerAlias);

        pNodeManager->addAliasNodeManager(pNodeManagerAlias);

        // Start server object
        if ( 0 != coreService->start() )
            delete coreService;


    }

}

void InteractiveService::stop()
{
    qDebug() << PROJECT_NAME << "is stopped";


    coreService->stop(3, UaLocalizedText("", "User shut down"));
    delete coreService;
    coreService = NULL;

}

void InteractiveService::pause()
{
    qDebug() << PROJECT_NAME << "is paused";

    //coreService->pause();
}

void InteractiveService::resume()
{
    qDebug() << PROJECT_NAME << "is resumed";

    //coreService->resume();
}

void InteractiveService::processCommand(int code)
{
    qDebug() << "Command code " << QString::number(code);
}
