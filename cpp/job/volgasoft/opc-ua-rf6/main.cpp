#include "interactiveservice.h"
#ifdef RF6_COMPILED
	#include "Rf6/rf6projectinfo.h"
#endif
#include "helper.h"

int main(int argc, char **argv)
{
	debugLog.logToFile("\n\n*********** Start opc-ua-rf6 ***********\n");

	Rf6::CRf6ProjectInfo rf6Info;
	int licenseCheckResult = rf6Info.checkLicense();
	if (licenseCheckResult < 0)
	{
		debugLog.logToFile("Error of a license reading! Exiting");
		printf("Error of a license reading! Exiting.\n");
		exit(1);
	}
	else if (licenseCheckResult == 0)
	{
		debugLog.logToFile("OPC-UA license is absent! Exiting");
		printf("OPC-UA license is absent! Exiting.\n");
		exit(2);
	}

	InteractiveService service(argc, argv);
    return service.exec();
}
