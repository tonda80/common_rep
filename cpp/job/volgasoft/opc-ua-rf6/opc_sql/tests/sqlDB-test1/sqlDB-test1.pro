TEMPLATE = app
CONFIG += console qt debug
QT -= gui
QT += sql

DEPENDPATH += . ../..
INCLUDEPATH += . ../.. ../../../opc_rf6 ../../../opc_utilities /rf6/include 

SOURCES += main.cpp ../../../opc_utilities/helper.cpp

HEADERS += ../../opcsqlitedatabase.h main.h ../../opcserializer.h ../../../opc_utilities/helper.h
SOURCES += ../../opcsqlitedatabase.cpp ../../opcserializer.cpp

# OPC UA SDK
#--------------------------
UASDK_HOME = /usr/local
DEFINES += \
        SUPPORT_XML_CONFIG \
        _UA_STACK_USE_DLL \
        OPCUA_SUPPORT_SECURITYPOLICY_BASIC128RSA15=1 \
        OPCUA_SUPPORT_SECURITYPOLICY_BASIC256=1 \
        OPCUA_SUPPORT_SECURITYPOLICY_NONE=1 \
        OPCUA_SUPPORT_PKI=1
INCLUDEPATH += $$UASDK_HOME/include/uastack $$UASDK_HOME/include/uabase $$UASDK_HOME/include/uaserver
LIBS +=/usr/pkg/lib/libxml2.so
LIBS += \
     -L/usr/lib \
     -lcrypto \
     -L$$UASDK_HOME/lib
CONFIG(release, debug|release ) {
	message(Release build!)
    LIBS +=\
         -lcoremodule \
         -luamodule \
         -luastack \
         -luabase \
         -luapki \
         -lxmlparser \
         -luamodels
    }
CONFIG(debug, debug|release ) {
	message(Debug build!)
    LIBS +=\
         -lcoremoduled \
         -luamoduled \
         -luastackd \
         -luabased \
         -luapkid \
         -lxmlparserd \
         -luamodelsd
	}
#--------------------------   
