/*
 * main.h
 *
 *  Created on: 19.10.2012
 *      Author: aberezin
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <QObject>

#include "opcsqlitedatabase.h"

class CSigSlotTest : public QObject
{
	Q_OBJECT

	public slots:
		// COpcSqliteDatabase signals
		void giveObject(UaNodeId, QString);
		void giveObject(UaNodeId, COpcObjectData*);
		void finishGetAllObjects(int cntError);
		void giveReference(UaNodeId, UaNodeId, UaNodeId);
		void finishGetAllReferences(int cntError);
};

#endif /* MAIN_H_ */
