#include <QDebug>

#include "main.h"

#include "uanodeid.h"
#include "uaplatformlayer.h"

const char* DEFAULT_SQL_DB_NAME = "opcua.sqlite";

// helper functions
// string to UaNodeId
UaNodeId s2N(const char* str, int ns=0) {return UaNodeId(str, ns);}
// UaNodeId to string
#define n2S(node) (node.toXmlString().toUtf8())

void printRefList(QList<TSqlReference> &refList)
{
	int cnt = 0;
	foreach (TSqlReference ref, refList)
	{
		qDebug() << "Reference" << cnt++ << ":" << n2S(ref[0]) << n2S(ref[1]) << n2S(ref[2]);
	}
}

void printObjList(QList<TSqlObject> &objList)
{
	int cnt = 0;
	foreach (TSqlObject obj, objList)
	{
		qDebug() << "Object" << cnt++ << ":" << n2S(obj.first) << ":" << obj.second;
	}
}

int main()
{
	UaPlatformLayer::init();

	COpcSqliteDatabase sqldb;
	
	//sqldb.open("fakeDB.sqlite");
	int res = sqldb.open(DEFAULT_SQL_DB_NAME);
	if (res != 0)
	{
		qDebug() << "Error. sqldb.open() returns" << res;
		return 1;
	}

	QList<TSqlReference> refList;
	QList<TSqlObject> objList;

	qDebug() << "************ reference addition";
	sqldb.transaction();
	sqldb.addReference(s2N("obj3"), s2N("ref2"), s2N("trg3"));
	sqldb.addReference(s2N("src1"), s2N("ref2"), s2N("trg2"));
	sqldb.addReference(s2N("obj1"), s2N("ref3"), s2N("obj3"));
	sqldb.addReference(s2N("src2"), s2N("ref4"), s2N("trg1"));
	sqldb.addReference(s2N("src2"), s2N("ref1"), s2N("obj3"));
	sqldb.addReference(s2N("src2"), s2N("ref1"), s2N("trg3"));
	sqldb.commit();

	sqldb.transaction();
	sqldb.addReference(s2N("fuck1"), s2N("shit2"), s2N("cunt3"));
	sqldb.addReference(s2N("penis"), s2N("dumb"), s2N("trg2"));
	sqldb.rollback();		// this was very bad operations!
	
	sqldb.getAllReferences(refList);
	printRefList(refList);

	qDebug() << "************ findReferencesByTarget trg3";
	sqldb.findReferencesByNode(0, s2N("trg3"), refList);
	printRefList(refList);

	qDebug() << "************ findReferencesBySource src2";
	sqldb.findReferencesByNode(1, s2N("src2"), refList);
	printRefList(refList);

	qDebug() << "************ delete of references  [src1 ref3 trg3], [src2 ref1 trg3]";
	sqldb.deleteReference(s2N("src1"), s2N("ref3"), s2N("trg3"));
	sqldb.deleteReference(s2N("src2"), s2N("ref1"), s2N("trg3"));

	sqldb.getAllReferences(refList);
	printRefList(refList);

	qDebug() << "************ object addition";
	sqldb.addObject(s2N("obj1"), "Some description of the object 1");
	sqldb.addObject(s2N("obj2"), "Sasha was going on a road");
	qDebug() << "Now there will be error..";
	sqldb.addObject(s2N("obj1"), "This object is already!!");
	qDebug() << "I noticed..";
	sqldb.addObject(s2N("obj3"), "Some description of the object 3");

	sqldb.getAllObjects(objList);
	printObjList(objList);

	qDebug() << "************ object read/update";
	sqldb.updateObject(s2N("obj1"), "If bears will be bees");
	QString new_ser;
	sqldb.readObject(s2N("obj1"), new_ser);
	qDebug() << "New the object serialization" << new_ser;

	qDebug() << "************ delete of the object [obj3]";
	sqldb.deleteObject(s2N("obj3"));

	sqldb.getAllObjects(objList);
	printObjList(objList);

	sqldb.getAllReferences(refList);
	printRefList(refList);

	qDebug() << "************ Test signal/slot interface";
	sqldb.addObject(s2N("obj4"), "<?xml version=\"1.0\"?><storage version=\"32\"><ident>61</ident><browseName><name>someBrowseName</name><namespace>2</namespace></browseName><displayName><locale>en</locale><text>lalala name</text></displayName><description><locale>es</locale><text>lulululu description</text></description></storage>");
	CSigSlotTest oSt;
	QObject::connect(&sqldb, SIGNAL(giveObject(UaNodeId, COpcObjectData*)), &oSt, SLOT(giveObject(UaNodeId, COpcObjectData*)) );
	QObject::connect(&sqldb, SIGNAL(giveObject(UaNodeId, QString)), &oSt, SLOT(giveObject(UaNodeId, QString)) );
	QObject::connect(&sqldb, SIGNAL(finishGetAllObjects(int)), &oSt, SLOT(finishGetAllObjects(int)) );
	QObject::connect(&sqldb, SIGNAL(giveReference(UaNodeId, UaNodeId, UaNodeId)), &oSt, SLOT(giveReference(UaNodeId, UaNodeId, UaNodeId)) );
	QObject::connect(&sqldb, SIGNAL(finishGetAllReferences(int)), &oSt, SLOT(finishGetAllReferences(int)) );

	sqldb.getAllObjects();
	sqldb.getAllReferences();

	qDebug() << "************ End of tests. Check the result";

	return 0;
}

void CSigSlotTest::giveObject(UaNodeId objId, COpcObjectData* objData)
{
	qDebug() << "Signal giveObject()" << ":" << n2S(objId) << ":" << objData->browseName.toFullString().toUtf8();
}

void CSigSlotTest::giveObject(UaNodeId objId, QString objRawData)
{
	qDebug() << "Signal giveObject(), WTF???" << ":" << n2S(objId) << ":" << objRawData;
}

void CSigSlotTest::finishGetAllObjects(int cntError)
{
	qDebug() << "Signal objectsGiven()" << cntError;
}
void CSigSlotTest::giveReference(UaNodeId srcNodeId, UaNodeId refNodeId, UaNodeId trgNodeId)
{
	qDebug() << "Signal giveReference()" << ":" << n2S(srcNodeId) << n2S(refNodeId) << n2S(trgNodeId);
}
void CSigSlotTest::finishGetAllReferences(int cntError)
{
	qDebug() << "Signal referencesGiven()" << cntError;
}

