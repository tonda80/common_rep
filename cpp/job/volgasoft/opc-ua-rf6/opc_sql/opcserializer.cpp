/*
 * opcserializer.cpp
 *
 *  Created on: 22.10.2012
 *      Author: aberezin
 */

#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "opcserializer.h"

#include "generated/opcrf6_identifiers.h"

#include "helper.h"

int COpcSerializer::serialize(const UaNode *object, QString& serialization)
{
	if (serializationType == 0)		// xml
	{
		UaNodeId idNodeType = object->typeDefinitionId();
		if (idNodeType.identifierType() != OpcUa_IdentifierType_Numeric)
			return IDENTIFIERTYPENOTSUPPORTED;

		OpcUa_UInt32 nodeIdentifier = idNodeType.identifierNumeric();
		if ( 	nodeIdentifier == OpcUaId_FolderType
			||	nodeIdentifier == OpcRf6Id_Rf6AliasObject
			||	nodeIdentifier == OpcRf6Id_Rf6AliasVariable
		)
		{
			return storageToXml(nodeIdentifier, object, serialization);
		}
		else
			return IDENTIFIERNOTSUPPORTED;
	}
	else
	{
		return SERTYPENOTSUPPORTED;
	}
}

int COpcSerializer::storageToXml(OpcUa_UInt32 nodeIdentifier, const UaNode *object, QString& serialization)
{
	UaQualifiedName browseName = object->browseName();
	UaLocalizedText displayName = object->displayName(NULL);
	UaLocalizedText description = object->description(NULL);

	QXmlStreamWriter xmlWriter(&serialization);

	xmlWriter.writeStartDocument();
	xmlWriter.writeStartElement("storage");
	xmlWriter.writeAttribute("version", "1");

	xmlWriter.writeTextElement("ident", QString::number(static_cast<int>(nodeIdentifier)));

	xmlWriter.writeStartElement("browseName");
	xmlWriter.writeTextElement("name", browseName.toString().toUtf8());
	xmlWriter.writeTextElement("namespace", QString::number(static_cast<int>(browseName.namespaceIndex())));
	xmlWriter.writeEndElement();

	xmlWriter.writeStartElement("displayName");
	xmlWriter.writeTextElement("locale", UaString(displayName.locale()).toUtf8());
	xmlWriter.writeTextElement("text", displayName.toString().toUtf8());
	xmlWriter.writeEndElement();

	xmlWriter.writeStartElement("description");
	xmlWriter.writeTextElement("locale", UaString(description.locale()).toUtf8());
	xmlWriter.writeTextElement("text", description.toString().toUtf8());
	xmlWriter.writeEndElement();

	xmlWriter.writeEndElement(); 	// storage
	xmlWriter.writeEndDocument();

	return 0;
}

COpcObjectData* COpcSerializer::deserialize(const QString& serialization)
{
	if (serializationType == 0)		// xml
	{
		return xmlDeserializer.parseObject(serialization);
	}
	else
	{
		debugLog.logToFile("COpcSerializer::deserialize error. The serialization type is not supported");
		return NULL;
	}

}

COpcObjectData* CXmlDeserializer::parseObject(const QString& serialization)
{
	xmlReader.clear();
	xmlReader.addData(serialization);

	for (; continueParsing(); xmlReader.readNext())
	{
		if (isStartElement("storage"))
			storageFromXml();
		else if (xmlReader.isStartDocument() || xmlReader.tokenType()==0)
			continue;
		else
			xmlReader.raiseError(QString("CXmlDeserializer format error"));	// only one object!
	}

	if (xmlReader.hasError())
	{
		//qDebug() << "CXmlDeserializer. Error of parsing" << xmlReader.error() << xmlReader.errorString();
		debugLog.logToFile("CXmlDeserializer::parseObject error. %s [%d]", xmlReader.errorString().toUtf8().constData(), xmlReader.error());
		objectData = NULL;
	}

	//if (objectData) qDebug() << "__storageFromXml()" <<	objectData->nodeIdentifier << ":" << objectData->browseName.toFullString().toUtf8() << ":" << objectData->displayName.toFullString().toUtf8() << ":" << objectData->description.toFullString().toUtf8();
	return objectData;
}

void CXmlDeserializer::storageFromXml()
{
	objectData = &storageData;

	// if necessary version then uncomment it
	//QXmlStreamAttributes attr = xmlReader.attributes();
	//attr.value("version");

	xmlReader.readNext(); // next after StartElement

	for (; continueParsing(); xmlReader.readNext())
	{
		QString text;
		if (tryGetText("ident", text))
			objectData->nodeIdentifier = text.toInt();
		else if (isStartElement("browseName"))
			browseNameFromXml();
		else if (isStartElement("displayName"))
			displayNameFromXml();
		else if (isStartElement("description"))
			descriptionFromXml();
		else if (isEndElement("storage"))
			return;
		else
			break;	// everything else is impossible
	}

	xmlReader.raiseError(QString("storageFromXml"));
}

void CXmlDeserializer::browseNameFromXml()
{
	UaString name;
	OpcUa_UInt16 nameSpaceIdx;

	xmlReader.readNext(); // next after StartElement

	for (; continueParsing(); xmlReader.readNext())
	{
		QString text;
		if (tryGetText("name", text))
		{
			name = text.toUtf8().constData();
		}
		else if (tryGetText("namespace", text))
		{
			nameSpaceIdx = text.toInt();
		}
		else if (isEndElement("browseName"))
		{
			objectData->browseName.setQualifiedName(name, nameSpaceIdx);
			return;
		}
		else
			break;	// everything else is impossible
	}

	xmlReader.raiseError(QString("parseBrowseName"));
}

void CXmlDeserializer::displayNameFromXml()
{
	UaString locale;
	UaString locText;

	xmlReader.readNext(); // next after StartElement

	for (; continueParsing(); xmlReader.readNext())
	{
		QString text;
		if (tryGetText("locale", text))
		{
			locale = text.toUtf8().constData();
		}
		else if (tryGetText("text", text))
		{
			locText = text.toUtf8().constData();
		}
		else if (isEndElement("displayName"))
		{
			objectData->displayName.setLocalizedText(locale, locText);
			return;
		}
		else
			break;	// everything else is impossible
	}

	xmlReader.raiseError(QString("parseDisplayName"));
}

void CXmlDeserializer::descriptionFromXml()
{
	UaString locale;
	UaString locText;

	xmlReader.readNext(); // next after StartElement

	for (; continueParsing(); xmlReader.readNext())
	{
		QString text;
		if (tryGetText("locale", text))
		{
			locale = text.toUtf8().constData();
		}
		else if (tryGetText("text", text))
		{
			locText = text.toUtf8().constData();
		}
		else if (isEndElement("description"))
		{
			objectData->description.setLocalizedText(locale, locText);
			return;
		}
		else
			break;	// everything else is impossible
	}

	xmlReader.raiseError(QString("parseBrowseName"));
}

bool CXmlDeserializer::tryGetText(const QString& name, QString& text)
{
	if ( !(isStartElement(name)) )
		return false;	// it is not our

	xmlReader.readNext(); // next after StartElement

	for (; continueParsing(); xmlReader.readNext())
	{
		if (xmlReader.isCharacters())
			text = xmlReader.text().toString();
		else if (isEndElement(name))
			return true;
		else
			break;	// everything else is impossible
	}

	xmlReader.raiseError(QString("tryGetText:")+name);
	return false;
}

