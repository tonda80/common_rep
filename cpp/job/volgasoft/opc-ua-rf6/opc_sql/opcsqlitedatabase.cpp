/*
 * opcsqlitedatabase.cpp
 *
 *  Created on: 16.10.2012
 *      Author: aberezin
 */

#include "opcsqlitedatabase.h"

// helper functions
QString UaNodeIdToQstring(const UaNodeId& node){return QString(node.toXmlString().toUtf8());}
UaNodeId QstringToUaNodeId(const QString& st){return UaNodeId::fromXmlString(UaString(st.toUtf8()));}

int COpcSqliteDatabase::open(QString name)
{
	if (sqlDB.isOpen())
	{
		qDebug() << "Warning." << sqlDB.databaseName() << "will be closed!";
		sqlDB.close();
	}

	sqlDB.setDatabaseName(name);

	if (!sqlDB.open())
	{
		qDebug() << "SQLite database not open!";
		return 1;
	}

	// we are trying to create tables with refernces and objects
	QSqlQuery query(sqlDB);
	QString queryStr1 =
		"CREATE TABLE referenceTable ("
		"sourceNode TEXT, "
		"targetNode TEXT, "
		"refType TEXT"
		");";
	QString queryStr2 =
		"CREATE TABLE objectTable ("
		"object TEXT PRIMARY KEY NOT NULL, "
		"serialization TEXT"
		");";

	bool res1 = query.exec(queryStr1);
	QSqlError err1 = query.lastError();
	if (err1.type()==2 && err1.number()==1)	// table exists already. it's ok
		res1 = true;

	bool res2 = query.exec(queryStr2);
	QSqlError err2 = query.lastError();
	if (err2.type()==2 && err2.number()==1)	// table exists already. it's ok
		res2 = true;

	if (!res1 || !res2)
	{
		qDebug() << "COpcSqliteDatabase::open() error. CREATE returns" << err1.text() << err2.text();
		return 1;
	}

	return 0;
}

void COpcSqliteDatabase::close()
{
	sqlDB.close();
}

int COpcSqliteDatabase::execQuery(QString queryStr, QSqlQuery *query/*=NULL*/)
{
	if (!sqlDB.isOpen())
	{
		qDebug() << "COpcSqliteDatabase is not opened.";
		return 1;
	}

	//QSqlQuery query;
	static QSqlQuery localQuery(sqlDB);
	if (query == NULL)
		query = &localQuery;

	bool res = query->exec(queryStr);
	if (!res)
	{
		qDebug() << "COpcSqliteDatabase error!" << queryStr << query->lastError().text();
		return 2;
	}

	return 0;
}

int COpcSqliteDatabase::addReference(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId)
{
	QString templStr =
		"INSERT INTO referenceTable(sourceNode, targetNode, refType) "
		"VALUES ('%1', '%2', '%3');";
	QString queryStr = templStr.arg(UaNodeIdToQstring(sourceNodeId)).arg(UaNodeIdToQstring(targetNodeId)).arg(UaNodeIdToQstring(referenceTypeId));

	//qDebug() << "__debug" << queryStr;

	return execQuery(queryStr);
}

int COpcSqliteDatabase::deleteReference(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId)
{
	QString templStr =
		"DELETE FROM referenceTable WHERE "
		"sourceNode = '%1' AND targetNode = '%2' AND refType = '%3';";
	QString queryStr = templStr.arg(UaNodeIdToQstring(sourceNodeId)).arg(UaNodeIdToQstring(targetNodeId)).arg(UaNodeIdToQstring(referenceTypeId));

	return execQuery(queryStr);
}

int COpcSqliteDatabase::findReferencesByNode(int nodeType, const UaNodeId& nodeId, QList<TSqlReference> &refList)
{
	refList.clear();

	QString templStr;
	if (nodeType == 0)
		templStr = "SELECT * FROM referenceTable WHERE targetNode = '%1';";
	else
		templStr = "SELECT * FROM referenceTable WHERE sourceNode = '%1';";
	QString queryStr = templStr.arg(UaNodeIdToQstring(nodeId));

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
		return 2;

	QSqlRecord rec = query.record();
	while (query.next())
	{
		TSqlReference reply(3);
		reply[0] = QstringToUaNodeId(query.value(rec.indexOf("sourceNode")).toString());
		reply[1] = QstringToUaNodeId(query.value(rec.indexOf("refType")).toString());
		reply[2] = QstringToUaNodeId(query.value(rec.indexOf("targetNode")).toString());

		refList << reply;
	}

	return 0;
}

int COpcSqliteDatabase::addObject(const UaNodeId& obj, const QString& objSerialization)
{
	QString templStr =
		"INSERT INTO objectTable(object, serialization) "
		"VALUES ('%1', '%2');";
	QString queryStr = templStr.arg(UaNodeIdToQstring(obj)).arg(objSerialization);

	//qDebug() << "__debug" << queryStr;

	return execQuery(queryStr);
}

int COpcSqliteDatabase::addObject(const UaNode *obj)
{
	QString serialization;
	if (opcSerializer.serialize(obj, serialization) != 0)
		return 10;

	return addObject(obj->nodeId(), serialization);
}

int COpcSqliteDatabase::deleteObject(const UaNodeId& obj)
{
	QString removedObject = UaNodeIdToQstring(obj);

	QString templStr =
		"DELETE FROM objectTable WHERE object = '%1';";
	QString queryStr = templStr.arg(removedObject);

	if (execQuery(queryStr) != 0)
		return 1;

	templStr =
		"DELETE FROM referenceTable WHERE sourceNode = '%1' OR targetNode = '%1';";
	queryStr = templStr.arg(removedObject);

	if (execQuery(queryStr) != 0)
		return 2;

	return 0;
}

int COpcSqliteDatabase::readObject(const UaNodeId& obj, QString& objSerialization)
{
	objSerialization.clear();

	QString templStr = "SELECT * FROM objectTable WHERE object = '%1';";
	QString queryStr = templStr.arg(UaNodeIdToQstring(obj));

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
		return 2;

	QSqlRecord rec = query.record();
	if (query.next())	// one result
	{
		objSerialization = query.value(rec.indexOf("serialization")).toString();
	}

	return 0;
}

COpcObjectData* COpcSqliteDatabase::readObject(UaNode* obj)
{
	QString serialization;
	if (readObject(obj->nodeId(), serialization) != 0)
		return NULL;

	return opcSerializer.deserialize(serialization);
}

int COpcSqliteDatabase::updateObject(const UaNodeId& obj, const QString& objSerialization)
{
	QString templStr = "UPDATE objectTable SET serialization = '%1' WHERE object = '%2';";
	QString queryStr = templStr.arg(objSerialization).arg(UaNodeIdToQstring(obj));

	return execQuery(queryStr);
}

int COpcSqliteDatabase::updateObject(const UaNode* obj)
{
	QString serialization;
	if (opcSerializer.serialize(obj, serialization) != 0)
		return 10;

	return updateObject(obj->nodeId(), serialization);
}

int COpcSqliteDatabase::getAllReferences(QList<TSqlReference> &refList)
{
	refList.clear();

	QString queryStr = "SELECT * FROM referenceTable;";

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
		return 2;

	QSqlRecord rec = query.record();
	while (query.next())
	{
		TSqlReference reply(3);
		reply[0] = QstringToUaNodeId(query.value(rec.indexOf("sourceNode")).toString());
		reply[1] = QstringToUaNodeId(query.value(rec.indexOf("refType")).toString());
		reply[2] = QstringToUaNodeId(query.value(rec.indexOf("targetNode")).toString());

		refList << reply;
	}

	return 0;
}

void COpcSqliteDatabase::getAllObjects(bool raw/*=false*/)
{
	int cntError = 0;	// now in is not incremented (maybe it will be done later)

	QString queryStr = "SELECT * FROM objectTable;";

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
	{
		emit finishGetAllObjects(-1);
		return;
	}

	QSqlRecord rec = query.record();
	while (query.next())
	{
		UaNodeId nodeId = QstringToUaNodeId(query.value(rec.indexOf("object")).toString());
		QString ser = query.value(rec.indexOf("serialization")).toString();

		if (raw)
			emit giveObject(nodeId, ser);
		else
		{
			COpcObjectData* objData = opcSerializer.deserialize(ser);
			if (objData == NULL)
				emit giveObject(nodeId, ser);	// we cannot parse and send the raw string
			else
				emit giveObject(nodeId, objData);
		}
	}

	emit finishGetAllObjects(cntError);
	return ;
}

void COpcSqliteDatabase::getAllReferences()
{
	int cntError = 0;	// now in is not incremented (maybe it will be done later)

	QString queryStr = "SELECT * FROM referenceTable;";

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
	{
		emit finishGetAllReferences(-1);
		return;
	}

	QSqlRecord rec = query.record();
	while (query.next())
	{
		UaNodeId srcNodeId = QstringToUaNodeId(query.value(rec.indexOf("sourceNode")).toString());
		UaNodeId refNodeId = QstringToUaNodeId(query.value(rec.indexOf("refType")).toString());
		UaNodeId trgNodeId =  QstringToUaNodeId(query.value(rec.indexOf("targetNode")).toString());

		emit giveReference(srcNodeId, refNodeId, trgNodeId);
	}

	emit finishGetAllReferences(cntError);
	return;
}

int COpcSqliteDatabase::getAllObjects(QList<TSqlObject> &objects)
{
	objects.clear();

	QString queryStr = "SELECT * FROM objectTable;";

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
		return 2;

	QSqlRecord rec = query.record();
	while (query.next())
	{
		TSqlObject reply(
			QstringToUaNodeId(query.value(rec.indexOf("object")).toString()),
			query.value(rec.indexOf("serialization")).toString());

		objects << reply;
	}

	return 0;
}

int COpcSqliteDatabase::isReferenceExist(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId)
{
	QString templStr =	"SELECT * FROM referenceTable WHERE "
						"sourceNode = '%1' AND targetNode = '%2' AND refType = '%3';";
	QString queryStr = templStr.arg(UaNodeIdToQstring(sourceNodeId)).arg(UaNodeIdToQstring(targetNodeId)).arg(UaNodeIdToQstring(referenceTypeId));

	QSqlQuery query(sqlDB);
	if (execQuery(queryStr, &query) != 0)
		return -1;

	QSqlRecord rec = query.record();
	if (query.next())
	{
		return 1;
	}

	return 0;
}
