/*
 * opcserializer.h
 *
 *  Created on: 22.10.2012
 *      Author: aberezin
 */

#ifndef OPCSERIALIZER_H_
#define OPCSERIALIZER_H_

#include <QDebug>
#include <QString>
#include <QSharedPointer>

#include "uabasenodes.h"

class COpcObjectData
{
public:
	COpcObjectData() : nodeIdentifier(0) {}
	OpcUa_UInt32 nodeIdentifier;
	UaQualifiedName browseName;
	UaLocalizedText displayName;
	UaLocalizedText description;
};

class COpcStorageData : public COpcObjectData
{
};

typedef QSharedPointer<COpcObjectData> TSqlParseObject;

class CXmlDeserializer
{
public:
	CXmlDeserializer() : objectData(NULL) {}
	COpcObjectData* parseObject(const QString& serialization);

private:
	QXmlStreamReader xmlReader;
	COpcObjectData *objectData;

	bool isStartElement(const QString& name){return (xmlReader.isStartElement() && xmlReader.name()==name);}
	bool isEndElement(const QString& name) {return (xmlReader.isEndElement() && xmlReader.name()==name);}
	bool continueParsing() {return (!xmlReader.atEnd() && !xmlReader.hasError());}


	// storage parsing
	void storageFromXml();
	COpcStorageData storageData;
	bool tryGetText(const QString& name, QString& text);

	void browseNameFromXml();
	void displayNameFromXml();
	void descriptionFromXml();
};

class COpcSerializer
{
public:
	enum SerializerError
	{
		NOERROR = 0,
		SERTYPENOTSUPPORTED,		// a serialization type is not supported
		IDENTIFIERTYPENOTSUPPORTED, // identifierType of nodeId is not supported
		IDENTIFIERNOTSUPPORTED,		// identifier of nodeId is not supported
	};

	COpcSerializer() : serializationType(0) {}
	int serialize(const UaNode *object, QString& serialization);
	COpcObjectData* deserialize(const QString& serialization);

	void setSerializationType(int type) {serializationType = type;}

private:
	int serializationType;	// serialization type (0 - xml (default))

	int storageToXml(OpcUa_UInt32 nodeIdentifier, const UaNode *object, QString& serialization);	// the function serializes folders, alias objects, alias variables

	CXmlDeserializer xmlDeserializer;

};


#endif /* OPCSERIALIZER_H_ */
