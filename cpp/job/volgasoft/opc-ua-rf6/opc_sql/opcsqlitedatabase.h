/*
 * opcsqlitedatabase.h
 *
 *  Created on: 16.10.2012
 *      Author: aberezin
 */

#ifndef OPCSQLITEDATABASE_H_
#define OPCSQLITEDATABASE_H_

#include <QObject>
#include <QtSql>

#include "uanodeid.h"

#include "opcserializer.h"

typedef QVector<UaNodeId> TSqlReference;
typedef QPair<UaNodeId, QString> TSqlObject;

class COpcSqliteDatabase : public QObject
{
	Q_OBJECT
public:
	QSqlDatabase sqlDB;
	COpcSqliteDatabase() {sqlDB = QSqlDatabase::addDatabase("QSQLITE");}
	int open(QString name);
	void close();

	int execQuery(QString queryStr, QSqlQuery *query=NULL);

	int addReference(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId);
	int deleteReference(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId);

	// function returns vector from 3 elements (source, reference, target). If nodeType==0, the search is performed by the target, else by source
	int findReferencesByNode(int nodeType, const UaNodeId& nodeId, QList<TSqlReference> &refList);

	int addObject(const UaNodeId& obj, const QString& objSerialization);
	int addObject(const UaNode *obj);
	int deleteObject(const UaNodeId& obj);
	int readObject(const UaNodeId& obj, QString& objSerialization);
	COpcObjectData* readObject(UaNode* obj);
	int updateObject(const UaNodeId& obj, const QString& objSerialization);
	int updateObject(const UaNode* obj);

	int getAllReferences(QList<TSqlReference> &refList);
	int getAllObjects(QList<TSqlObject> &objects);

public slots:
	void getAllObjects(bool raw=false);
	void getAllReferences();
signals:
	void giveObject(UaNodeId, QString);
	void giveObject(UaNodeId, COpcObjectData*);
	void finishGetAllObjects(int cntError);			// all objects were given (or error)
	void giveReference(UaNodeId, UaNodeId, UaNodeId);
	void finishGetAllReferences(int cntError);		// all references were given  (or error)

public:
	// transactions
	bool commit() {return sqlDB.commit();}
	bool rollback() {return sqlDB.rollback();}
	bool transaction() {return sqlDB.transaction();}

	COpcSerializer opcSerializer;

	// function returns 1 if such reference exists, 0 - it doesn't exist and the negative value when a error occurs
	int isReferenceExist(const UaNodeId& sourceNodeId, const UaNodeId& referenceTypeId, const UaNodeId& targetNodeId);
private:
};


#endif /* OPCSQLITEDATABASE_H_ */
