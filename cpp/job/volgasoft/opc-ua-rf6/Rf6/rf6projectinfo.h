#ifndef RF6PROJECTINFO_H
#define RF6PROJECTINFO_H

#include <QObject>
#include <QString>

#include <rfversions.h>
#include <rf6lib.hpp>
#include <syscfg.hpp>

namespace Rf6
{

	// class accumulates a info about the current realflex project
	class CRf6ProjectInfo : public QObject
	{
		Q_OBJECT

	public:
		QString getProjectName();
		int getRfState();	// function returns the current state of the project (main, standby etc) See hscproc.h

		QString getNodeName();
		QString getRfversion();

		CRFVersion rfversion;
		CConfiguration rfconfig;
		QString getProjectPath();

		// function returns 1 when the license exists, 0 - it doesn't exist, the negative value when an error occurred
		int checkLicense();
	};

}

#endif
