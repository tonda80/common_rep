#include "rf6database.h"

#include <QDebug>

#include <errcodes.h>
#include <trans/rfu.h>

namespace Rf6
{

	int GetDbType(const ULONG_64& index) {return static_cast<int>(index >> 56);}
	int GetNode(const ULONG_64& index) {return static_cast<int>( (index >> 46) & 0x3FF );}
	//unsigned int GetIndexFromUnIndex(const ULONG_64& index) {return static_cast<int>( index & 0x3FFFFFFFFFFFLL );}
	
	CRf6Database::CRf6Database()
		: mPcuDb(NULL), mAnaDb(NULL), mStaDb(NULL), mMetDb(NULL), mTnkDb(NULL)
	{
		Rf6DbOpen();
	}

	CRf6Database::~CRf6Database()
	{
		Rf6DbClose();
	}

	int CRf6Database::Rf6DbOpen()
	{
		Rf6DbClose();
		try
		{
			mPcuDb = new TPcuObj;
			mAnaDb = new TAnaObj;
			mStaDb = new TStaObj;
			mMetDb = new TMetObj;
			mTnkDb = new TTnkObj;
		}
		catch (std::bad_alloc xa)
		{
			qDebug() << "CRf6Database::Rf6DbOpen. Memory allocation error.";
			Rf6DbClose();
			return 1;
		}

		int errcode = 0;
		if (open_db(mPcuDb) != RF_SUCCESS) errcode = 2;
		if (open_db(mAnaDb) != RF_SUCCESS) errcode = 3;
		if (open_db(mStaDb) != RF_SUCCESS) errcode = 4;
		if (open_db(mMetDb) != RF_SUCCESS) errcode = 5;
		if (open_db(mTnkDb) != RF_SUCCESS) errcode = 6;

		if (errcode != 0)
		{
			qDebug() << "CRf6Database::Rf6DbOpen. Error of database opening. Code " << errcode;
			Rf6DbClose();
			return errcode;
		}
		return 0;
	}
	
	void CRf6Database::Rf6DbClose()
	{
		close_db(mPcuDb);
		close_db(mAnaDb);
		close_db(mStaDb);
		close_db(mMetDb);
		close_db(mTnkDb);
	}
	
	void CRf6Database::close_db(CMemObject*& pDb)
	{
		if (pDb != NULL)
		{
			pDb->Close();
			delete pDb;
			pDb = NULL;
		}
	}

	int CRf6Database::open_db(CMemObject* pDb)
	{
		if (pDb != NULL)
			return pDb->Open();
		return RF_FAILURE;
	}

	CMemObject* CRf6Database::GetMemObject( int dbType )
	{
		switch ( dbType )
		{
			case NANALOG_DB: return mAnaDb;
			case NSTATUS_DB: return mStaDb;
			case NMETER_DB: return mMetDb;
			case NTANK_DB: return mTnkDb;
			case NPCU_DB: return mPcuDb;
			default: return NULL;
		}
		return NULL;
	}
	
	unsigned int CRf6Database::GetTagIndex(int dbType, const char *szPcu, const char *szTag)
	{
		CMemObject* pMemObj = GetMemObject(dbType);

		if ( pMemObj == NULL)
			return 0;
		char mbsPcu[MB_LEN_MAX*(_pcuNameSize+1)]; mbsPcu[0] = 0;
		if (szPcu != NULL)
			rf_strtombs(mbsPcu, szPcu, sizeof(mbsPcu));

		if (dbType == NPCU_DB)
			return pMemObj->GetIndex(NULL, mbsPcu);
	
		char mbsTag[MB_LEN_MAX*(_tagNameSize+1)]; mbsTag[0] = 0;
		if (szTag != NULL)
			rf_strtombs(mbsTag, szTag, sizeof(mbsTag));

		return pMemObj->GetIndex(mbsTag, mbsPcu);
	}
	
	ULONG_64 CRf6Database::GetPcuUnIndex( const char* szPcu)
	{
		return GetTagUnIndex( NPCU_DB, szPcu, NULL );
	}
	
	ULONG_64 CRf6Database::GetTagUnIndex( int dbType, const char* szPcu, const char* szTag )
	{
		int recIndex = GetTagIndex( dbType, szPcu, szTag );

		return GetUnIndex(dbType, recIndex);
	}

	ULONG_64 CRf6Database::GetUnIndex(int dbType, unsigned int index)
	{
		CMemObject* pObj = GetMemObject( dbType );
		if( pObj == NULL )
			return 0;

		if( pObj->Read(index ) == RF_SUCCESS)
			return pObj->GetUniqueIndex();
		else
			return 0;
	}

	int CRf6Database::ReadDbObject(ULONG_64 un_ind, SimpleDbObject& ob)
	{
		CMemObject* pMemObj = getRdMemObject(un_ind);
		if (pMemObj == NULL)
			return 1;
		
		switch(GetDbType(un_ind))
		{
		case NANALOG_DB:
			ob.AnaValue = pMemObj->DbRec.a->eu_value;
			break;

		case NSTATUS_DB:
			ob.StaValue = pMemObj->DbRec.s->cur_val;
			break;
		}

		return 0;
	}
	
	int CRf6Database::UpdateDbObject(ULONG_64 un_ind, SimpleDbObject& ob)
	{
		unsigned int ind;
		int bDel;
		CMemObject* pMemObj = getRdMemObject(un_ind, &ind, &bDel);
		if (pMemObj == NULL)
			return 1;
		
		switch(GetDbType(un_ind))
		{
		case NANALOG_DB:
			pMemObj->DbRec.a->eu_value = ob.AnaValue;
			break;

		case NSTATUS_DB:
			pMemObj->DbRec.s->cur_val = ob.StaValue;
			break;
		}
		
		if (pMemObj->Write(ind) == RF_FAILURE)
			return 4;
		return 0;
	}
	
	int CRf6Database::ReadDbObject(int dbType, const char *szPcu, const char *szTag, SimpleDbObject& ob)
	{
		ULONG_64 un_ind = GetTagUnIndex(dbType, szPcu, szTag);
		return ReadDbObject(un_ind, ob);
	}

	int CRf6Database::UpdateDbObject(int dbType, const char *szPcu, const char *szTag, SimpleDbObject& ob)
	{
		ULONG_64 un_ind = GetTagUnIndex(dbType, szPcu, szTag);
		return UpdateDbObject(un_ind, ob);
	}

	ULONG_64 CRf6Database::CreatePcu(const char* PcuName, bool *existed/*=NULL*/)
	{
		if (existed != NULL)
			*existed = false;

		if (mPcuDb == NULL)
			return 0;

		ULONG_64 PcuUnIdx = GetPcuUnIndex(PcuName);
		if (PcuUnIdx == 0)	// pcu is not existed
		{
			NPCU_REC *prRec = mPcuDb->DbRec.p;
			memset( prRec, 0, sizeof(NPCU_REC) );
			mPcuDb->SetPcuName(PcuName);
			mPcuDb->DbRec.p->addr = 0;//AddrPCU;
			mPcuDb->DbRec.p->cs.port = 0;//NumChannel;
			mPcuDb->SetNoReply();
			if (mPcuDb->AddRec() != RF_SUCCESS)
				return 0;
			else
				return GetPcuUnIndex(PcuName);
		}
		else			// pcu is existed already
		{
			if (existed != NULL)
				*existed = true;
			return PcuUnIdx;
		}
	}

	ULONG_64 CRf6Database::CreateTag(int DbType, const char* PcuName, const char * TagName, bool *existed/*=NULL*/)
	{
		if (existed != NULL)
			*existed = false;

		ULONG_64 PcuUnIdx = GetPcuUnIndex(PcuName);
		if (PcuUnIdx == 0)
			return 0;

		CMemObject* pMemObj = GetMemObject(DbType);
		if (pMemObj == NULL)
			return 0;

		ULONG_64 UnIdx = GetTagUnIndex(DbType, PcuName, TagName);

		if (UnIdx == 0)		// tag is not existed
		{
			switch(DbType)
			{
				case NANALOG_DB:
					memset( pMemObj->DbRec.a, 0, sizeof(NANALOG_REC) );
					break;
				case NSTATUS_DB:
					memset( pMemObj->DbRec.s, 0, sizeof(NSTATUS_REC) );
					pMemObj->DbRec.s->num_bit = 2;
					break;
				case NMETER_DB:
					memset( pMemObj->DbRec.m, 0, sizeof(NMETER_DB) );
					break;
				case NTANK_DB:
					memset( pMemObj->DbRec.t, 0, sizeof(NTANK_DB) );
					break;
				default:
					return 0;
			}

			RF_TIMESPEC rCurTime;
			rCurTime.tv_sec = time(NULL);
			rCurTime.tv_nsec = 0;

			pMemObj->DbRec.h->rtu_nbr = PcuUnIdx;
			pMemObj->SetTagName(TagName);
			pMemObj->SetPcuName(PcuName);
			pMemObj->SetTimeOfChange( rCurTime );
			pMemObj ->SetSystemTimeOfChange( rCurTime );

			if (pMemObj->AddRec() != RF_SUCCESS)
				return 0;
			else
				return GetTagUnIndex(DbType, PcuName, TagName);
		}
		else				// tag is existed already
		{
			if (existed != NULL)
				*existed = true;
			return UnIdx;
		}
	}
	
	 int CRf6Database::DeleteDbObject(ULONG_64 un_ind)
	{
		unsigned int ind;
		CMemObject* pMemObj = getRdMemObject(un_ind, &ind);
		if (pMemObj == NULL)
			return 1;
		
		if (pMemObj->Remove(ind) != RF_SUCCESS)
			return 4;
		
		return 0;
	}

	int CRf6Database::DeletePcu(const char* szPcu)
	{
		return DeleteDbObject(GetPcuUnIndex(szPcu));
	}

	int CRf6Database::DeleteTag(int dbType, const char* szPcu, const char* szTag)
	{
		return DeleteDbObject(GetTagUnIndex(dbType, szPcu, szTag));
	}
	
	void CRf6Database::HandlerCreate(ULONG_64 parent, CRf6DbObject& ob)
	{
		Rf6DbError errcode = UNKNOWNERROR;

		do
		{
			int dbtype = ob.GetDbType();
			int parent_dbtype = GetDbType(parent);

			if ( !(dbtype==NANALOG_DB || dbtype==NSTATUS_DB || dbtype==NMETER_DB || dbtype==NTANK_DB || dbtype==NPCU_DB) )
			{
				errcode = UNKNOWNDBTYPE;
				break;
			}

			bool existed;
			if (dbtype == NPCU_DB)	// creating of new PCU
			{
				ob.unindex = CreatePcu(ob.name.toAscii().constData(), &existed);
			}
			else					// creating of tag
			{
				if (parent_dbtype != NPCU_DB)
				{
					errcode = PARENTISNOTPCU;
					break;
				}

				const char* pcuname = GetObjName(parent);
				//qDebug() << "PCU index - "<< parent << " PCU name - " << pcuname << "name" << ob.name.toAscii().constData();
				if (!strcmp(pcuname, ""))
				{
					errcode = PARENTNOTFOUND;
					break;
				}
				ob.unindex = CreateTag(GetDbType(ob.unindex), pcuname, ob.name.toAscii().constData(), &existed);
			}

			if (existed)
			{
				errcode = OBJECTISEXISTED;
				break;
			}

			errcode = NOERROR;
		} while (0);

		if (ob.unindex == 0)
			errcode = ERRCREATOBJ;
			
		emit CreateComplete(errcode, parent, ob);
	}
	
	void CRf6Database::HandlerRead(ULONG_64 uind)
	{
		QSharedPointer<CRf6DbObject>spOb (new CRf6DbObject(uind));	// creation of object

		Rf6DbError errcode = UNKNOWNERROR;

		do
		{
			unsigned int ind;
			int bDel;
			CMemObject* pMemObj = getRdMemObject(uind, &ind, &bDel);
			if (pMemObj == NULL)
			{
				errcode = RF6DBERROR;
				break;
			}

			if (spOb->ReadFromDbRec(pMemObj->DbRec) != 0)
			{
				errcode = RF6DBERROR;
				break;
			}

			errcode = NOERROR;
		} while (0);
		
		spOb->name = GetObjName(spOb->unindex);

		emit ReadComplete(errcode, spOb);	// return by value
	}

	void CRf6Database::HandlerUpdate(const CRf6DbObject& ob)
	{
		Rf6DbError errcode = UNKNOWNERROR;

		unsigned int ind;	// 32-bit index
		int bDel;
		CMemObject* pMemObj = getRdMemObject(ob.unindex, &ind, &bDel);

		do
		{
			if (pMemObj == NULL)
			{
				errcode = RF6DBERROR;
				break;
			}

			if (ob.WriteToDbRec(pMemObj->DbRec) != 0)
			{
				errcode = RF6DBERROR;
				break;
			}

			if (pMemObj->Write(ind) == RF_FAILURE)
				errcode = RF6DBERROR;
			else
				errcode = NOERROR;
		} while (0);

		emit UpdateComplete(errcode, ob.unindex);
	}

	void CRf6Database::HandlerDelete(ULONG_64 uind)
	{
		Rf6DbError errcode = UNKNOWNERROR;

		do
		{
			unsigned int ind;
			CMemObject* pMemObj = getRdMemObject(uind, &ind);
			if (pMemObj == NULL)
			{
				errcode = RF6DBERROR;
				break;
			}

			if (pMemObj->Remove(ind) != RF_SUCCESS)
			{
				errcode = RF6DBERROR;
				break;
			}

			errcode = NOERROR;
		} while (0);

		emit DeleteComplete(errcode, uind);
	}

	const char* CRf6Database::GetObjName(ULONG_64 uind)
	{
		int bDel;
		unsigned int ind;
		CMemObject* pMemObj = getRdMemObject(uind, &ind, &bDel);
		if (pMemObj == NULL)
			return "";

		if (bDel==1)
			return "";

		if (GetDbType(uind) == NPCU_DB)
			return pMemObj->GetPcuName();
		return pMemObj->GetTagName();
	}

	void CRf6Database::GetObjects()
	{
		cnt_error_getting = 0;

		int idx;	// index

		QList<CMemObject*> lstpMemObj;
		lstpMemObj << mPcuDb << mAnaDb << mStaDb << mMetDb << mTnkDb;
		foreach (CMemObject* pMemObj, lstpMemObj)
		{
			if (pMemObj == NULL)	// total error
			{
				emit ObjectsGiven(-1);
				return;
			}
			int n_ob;	// quantity of objects
			pMemObj->GetSortedIndexes(n_ob);
			idx = pMemObj->Current();
			while (idx != -1)
			{
				if( pMemObj->Read(idx) == RF_SUCCESS )
				{
					QSharedPointer<CRf6DbObject>spOb (new CRf6DbObject());	// creation of object
					spOb->unindex = pMemObj->GetUniqueIndex();
					spOb->ReadFromDbRec(pMemObj->DbRec);

					ULONG_64 parent;
					if (pMemObj == mPcuDb)
					{
						spOb->name = pMemObj->GetPcuName();
						parent = 0;
					}
					else
					{
						spOb->name = pMemObj->GetTagName();
						// Getting of pcu unique index
						if( mPcuDb->Read(pMemObj->DbRec.h->rtu_nbr) != RF_SUCCESS)
						{
							cnt_error_getting++;
							idx = pMemObj->Next();
							continue;
						}
						parent = mPcuDb->GetUniqueIndex();
					}

					emit GiveObject(parent, spOb);
				}
				else
				{
					cnt_error_getting++;
				}

				idx = pMemObj->Next();
			}
		}

		emit ObjectsGiven(cnt_error_getting);
	}

	CMemObject* CRf6Database::getRdMemObject(ULONG_64 unind, unsigned int* ind, int* del)
	{
		CMemObject* pMemObj = GetMemObject(GetDbType(unind));
		if (pMemObj == NULL)
			return NULL;

		int bDel;
		int index32 = pMemObj->SearchByUniqueIndex(unind, bDel);
		if (index32 == 0)
			return NULL;

		if (pMemObj->Read(index32) != RF_SUCCESS)
			return NULL;

		// pMemObject was got. If necessary we fill the index and the flag of delete
		if (ind != NULL)
			*ind = index32;
		if (del != NULL)
			*del = bDel;
		return pMemObj;
	}
}

