#include "rf6readcfgfile.h"

#include "helper.h"

char group_buff[512];


CRf6ReadCfgFile::CRf6ReadCfgFile()
{
    mFieldsList = new IArray< TelWidgetPtr_t >(10,10,1);
}

CRf6ReadCfgFile::~CRf6ReadCfgFile()
{
}

int CRf6ReadCfgFile::SectionError(const char *section)
{
    char buf[512];
    debugLog.logToFile("Error in configuration format in the %s section\n", section);

    return 0;
}

int CRf6ReadCfgFile::GetNextLine(char *buff1)
{

        char	buff[512];
        if(!TelFile)
            return 0;

        while( TelFile )
        {
            TelFile.getline( buff, 255 );
            if(!TelFile)
                return 0;

            CRf6Telemetry::strcompress((unsigned char *)buff);

            if(buff[0] == '#')
                continue;

            if(buff[0] == ';')
                continue;

            if(!strlen(buff) )
                continue;
            break;
        }


        char	temp_buff[512];
        char	*token=NULL;
        strcpy(temp_buff, buff);
        token = CRf6Telemetry::findToken1(temp_buff, ";\r\n");
        if(token != NULL)
        {
            strcpy(buff1, token);
        }
        else
        {
            strcpy(buff1, buff);
        }
        return 1;

}

int CRf6ReadCfgFile::MemoryError(void)
{
    debugLog.logToFile("Memory Error, Failed in allocating the memory\n");
    return 0;
}


TelWidgetPtr_t *CRf6ReadCfgFile::AllocPtr()
{
    TelWidgetPtr_t *ptr=NULL;
    ptr = new TelWidgetPtr_t;
    if(ptr == NULL)
    {
        MemoryError();
        return 0;
    }
    memset(ptr, 0x00, sizeof(TelWidgetPtr_t) );
    ptr->offset = -1;

    return ptr;
}

int CRf6ReadCfgFile::ReadOffsetField(TelWidgetPtr_t *ptr, char *section, char *buff)
{
    char temp_buff[512];
    char	token_str1[512], token_str2[512], token_str3[512];
    char	*token=NULL;

    if(buff[0] == '[' )
        return 0;

    //first token
    strcpy(temp_buff, buff);
    if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
    {
        return 0;
    }
    strcpy(token_str1, token);

    //second token
    strcpy(temp_buff, buff);
    if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
    {
        return 0;
    }
    strcpy(token_str2, token);

    //third token
    if((token = CRf6Telemetry::findToken2(token_str2, "+") )== NULL)
    {
        return 0;
    }
    strcpy(token_str3, token);

    strupr(token_str1);
    if(!strcmp(token_str1, "OFFSET"))
    {
        ptr->offset = atoi(token_str3);
    }
    else
    {
       debugLog.logToFile("Error in configuration format in the %s section Offset field\n", section);
       return 0;
    }
    return 1;
}

int CRf6ReadCfgFile::ReadTextField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token = NULL;

    for(int x=0; x<2; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);
        strupr(token_str1);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DEFAULT") )
            {
                SectionError(section);
                return 0;
            }
            else
            {
                strcpy(ptr->Tele.Text.cur_text, " ");
                strcpy(ptr->Tele.Text.prev_text, " ");
                continue;
            }
        }
        strcpy(token_str2, token);

        if(!strcmp(token_str1, "DEFAULT") || !strcmp(token_str1, "LENGTH") )
        {
            if(!strcmp(token_str1, "LENGTH") )
            {
                ptr->Tele.Text.text_len = atoi(token_str2);
            }
            else if(!strcmp(token_str1, "DEFAULT") )
            {
                CRf6Telemetry::strcompress((unsigned char *)token_str2);
                strcpy(ptr->Tele.Text.cur_text, token_str2);
                strcpy(ptr->Tele.Text.prev_text, token_str2);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}

int CRf6ReadCfgFile::ReadDateField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);
        strupr(token_str1);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DEFAULT") )
            {
                SectionError(section);
                return 0;
            }
            else
            {
                continue;
            }
        }
        strcpy(token_str2, token);

        if(!strcmp(token_str1, "DEFAULT") )
        {
            if(!strcmp(token_str1, "DEFAULT") )
            {
                CRf6Telemetry::strcompress((unsigned char *)token_str2);
                CRf6Telemetry::parseDate(token_str2, &ptr->Tele.Date);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}

int CRf6ReadCfgFile::ReadTimeField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);
        strupr(token_str1);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DEFAULT") )
            {
                SectionError(section);
                return 0;
            }
            else
            {
                continue;
            }
        }
        strcpy(token_str2, token);

        if(!strcmp(token_str1, "DEFAULT") )
        {
            if(!strcmp(token_str1, "DEFAULT") )
            {
                CRf6Telemetry::strcompress((unsigned char *)token_str2);
                CRf6Telemetry::parseTime(token_str2, &ptr->Tele.Time);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}


int CRf6ReadCfgFile::ReadGroupStart(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token=NULL;

    for(int x=0; x<2; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            debugLog.logToFile("Error in configuration format in the %s section\n", section);
            return 0;
            strcpy(group_buff, buff);
            return -2;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);
        strupr(token_str1);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DEPENDSON")||strcmp(token_str1, "DISPLAYIF") )
            {
                SectionError(section);
                return 0;
            }
            else
            {
                continue;
            }
        }
        strcpy(token_str2, token);

        if(!strcmp(token_str1, "DEPENDSON")||!strcmp(token_str1, "DISPLAYIF") )
        {
            CRf6Telemetry::strcompress((unsigned char *)token_str2);
            if(!strcmp(token_str1, "DEPENDSON") )
                ptr->Tele.GroupS.depend=atoi(token_str2);

            if(!strcmp(token_str1, "DISPLAYIF") )
                ptr->Tele.GroupS.display_val=(double)atoi(token_str2);
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}


int CRf6ReadCfgFile::ReadGroupEnd(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);
        strupr(token_str1);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DELIM") )
            {
                SectionError(section);
                return 0;
            }
            else
            {
                continue;
            }
        }
        strcpy(token_str2, token);

        if(!strcmp(token_str1, "DELIM") )
        {
            CRf6Telemetry::strcompress((unsigned char *)token_str2);
            strupr(token_str2);
            if(!strcmp(token_str2, "YES") )
                ptr->Tele.GroupE.delim=1;
            else
                ptr->Tele.GroupE.delim=0;
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}


int CRf6ReadCfgFile::ReadNumericField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512];
    char	*token=NULL;

    for(int x=0; x<3; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str2, token);

        strupr(token_str1);
        strupr(token_str2);
        if(!strcmp(token_str1, "DEFAULT") || !strcmp(token_str1, "MIN") || !strcmp(token_str1, "MAX") )
        {
            if(!strcmp(token_str1, "MIN") )
            {
                ptr->Tele.Numeric.min_val = atof(token_str2);
            }
            if(!strcmp(token_str1, "MAX") )
            {
                ptr->Tele.Numeric.max_val = atof(token_str2);
            }
            else if(!strcmp(token_str1, "DEFAULT") )
            {
                ptr->Tele.Numeric.cur_val = atof(token_str2);
                ptr->Tele.Numeric.prev_val = atof(token_str2);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}

int CRf6ReadCfgFile::ReadCheckBoxField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char token_str1[512], token_str2[512];
    char *token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str2, token);

        strupr(token_str1);
        strupr(token_str2);
        if(!strcmp(token_str1, "DEFAULT") )
        {
            if(!strcmp(token_str1, "DEFAULT") )
            {
                ptr->Tele.CheckBox.cur_state=atoi(token_str2);
                ptr->Tele.CheckBox.prev_state=atoi(token_str2);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}

int CRf6ReadCfgFile::ReadPcuList(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char token_str1[512], token_str2[512];
    char *token=NULL;
    char def_str[128];

    for(int x=0; x<2; x++)
    {
        memset(buff, 0x00, 512);
        memset(temp_buff, 0x00, 512);
        memset(token_str1, 0x00, 512);
        memset(token_str2, 0x00, 512);

        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        strupr(token_str1);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            if(strcmp(token_str1, "DEFAULT") )
            {
            SectionError(section);
            return 0;
        }
        }
        if(token != NULL)
        strcpy(token_str2, token);

        strupr(token_str2);
        if(!strcmp(token_str1, "DEFAULT"))
        {
            CRf6Telemetry::strcompress((unsigned char *)token_str2);
            strcpy(def_str, token_str2);
        }
        else if(!strcmp(token_str1, "LENGTH"))
        {
            CRf6Telemetry::strcompress((unsigned char *)token_str2);
            ptr->Tele.List.item_len=atoi(token_str2);
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    int pcu_count=0;
    int curIdx=0;
    if(PcuObj.GetSortedIndexes(pcu_count, 0) == RF_FAILURE)
        return RF_FAILURE;

    for(int x=0; x<pcu_count; x++)
    {
        curIdx = PcuObj.Current();
        if(curIdx <= 0)
            break;

        if(PcuObj.Read(curIdx) != RF_SUCCESS)
            break;

        if(PcuObj.Port() != nChannelNo)
        {
            PcuObj.Next();
            continue;
        }
        ptr->Tele.List.item_count++;
        PcuObj.Next();
    }
    ptr->Tele.List.item_count++; //increment by one to add a blank one

    ptr->Tele.List.list_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.list_items == NULL)
    {
        MemoryError();
        return 0;
    }

    ptr->Tele.List.value_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.value_items == NULL)
    {
        MemoryError();
        return 0;
    }

    int item_pos=0;
    ptr->Tele.List.cur_sel = 0;
    ptr->Tele.List.prev_sel 	= 0;

    if(PcuObj.GetSortedIndexes(pcu_count, 0) == RF_FAILURE)
        return RF_FAILURE;

    for(int x=0; x<pcu_count+1; x++)
    {
        if( x == 0)
    {
            ptr->Tele.List.list_items[ item_pos ] = new char[1];
            if(ptr->Tele.List.list_items[ item_pos ] == NULL)
            {
                MemoryError();
                return 0;
            }
            strcpy(ptr->Tele.List.list_items[ item_pos ], "");

            ptr->Tele.List.value_items[ item_pos ] = new char[1];
            if(ptr->Tele.List.value_items[ item_pos ] == NULL)
            {
                MemoryError();
                return 0;
            }
            strcpy(ptr->Tele.List.value_items[ item_pos ], "");

            item_pos++;
            continue;
        }

        curIdx = PcuObj.Current();
        if(curIdx <= 0)
            break;

        if(PcuObj.Read(curIdx) != RF_SUCCESS)
            break;

        if(PcuObj.Port() != nChannelNo)
        {
            PcuObj.Next();
            continue;
        }

        ptr->Tele.List.list_items[ item_pos ] = new char[PcuObj.GetPcuNameL()];
        if(ptr->Tele.List.list_items[ item_pos ] == NULL)
        {
            MemoryError();
            return Pt_CONTINUE;
        }
        strcpy(ptr->Tele.List.list_items[ item_pos ], PcuObj.GetPcuName());

        ptr->Tele.List.value_items[ item_pos ] = new char[PcuObj.GetPcuNameL()];
        if(ptr->Tele.List.value_items[ item_pos ] == NULL)
        {
            MemoryError();
            return Pt_CONTINUE;
        }
        strcpy(ptr->Tele.List.value_items[ item_pos ], PcuObj.GetPcuName());
        item_pos++;

        PcuObj.Next();
    }

    //set the default selections
    ptr->Tele.List.cur_sel 	= 1;
    ptr->Tele.List.prev_sel 	= 1;

    for(int x=0; x<ptr->Tele.List.item_count; x++)
    {
        if(!strlen(def_str) )
        {
            ptr->Tele.List.cur_sel 	= x+1;
            ptr->Tele.List.prev_sel 	= x+1;
            break;
        }

        //find out the current item..
        if(!strcmp(ptr->Tele.List.value_items[x], def_str) )
        {
            ptr->Tele.List.cur_sel 	= x+1;
            ptr->Tele.List.prev_sel 	= x+1;
            break;
        }
    }

    ptr->widget_type=WIDGET_TYPE_LIST;
    ptr->data_type=TYPE_STRING;
    return 0;
}

int compare( const void *op1, const void *op2 )
{
    const int *p1 = (const int *) op1;
    const int *p2 = (const int *) op2;
    if( *p1 < *p2 )
        return -1;
    else if(*p1==*p2)
        return 0;
  //  else if(*p1>*p2)
    return 1;
}

int CRf6ReadCfgFile::ReadPcuAddrList(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char token_str1[512], token_str2[512];
    char *token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str2, token);

        strupr(token_str1);
        strupr(token_str2);
        if(!strcmp(token_str1, "DEFAULT"))
        {
            CRf6Telemetry::strcompress((unsigned char *)token_str2);
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }
    int pcu_cnt=0;
    if(PcuObj.GetSortedIndexes(pcu_cnt, 0) == RF_FAILURE)
        return RF_FAILURE;

    int item_pos=0;
    int curIdx=0;
    ptr->Tele.List.cur_sel = 0;
    ptr->Tele.List.prev_sel 	= 0;

    int *addr_list = new int[pcu_cnt];
    if(addr_list == NULL)
    {
        MemoryError();
        return 0;
    }
    memset(addr_list, 0x00, sizeof(int)*pcu_cnt);
    int addr_cnt=0;

    for(int x=0; x<pcu_cnt; x++)
    {
        curIdx = PcuObj.Current();
        if(curIdx <= 0)
            break;

        if(PcuObj.Read(curIdx) != RF_SUCCESS)
            break;

        int addr=PcuObj.DbRec.p->addr;

        //check if this address is in the addr_list..
        int bFound=0;
        for(int i=0; i<addr_cnt; i++)
        {
            if(addr == addr_list[i] )
            {
                bFound=1;
                break;
            }
        }

        if(!bFound)
        {
            addr_list[addr_cnt++]=addr;
        }

        PcuObj.Next();
    }
    qsort( addr_list, addr_cnt, sizeof(int), compare );

    ptr->Tele.List.item_count = addr_cnt;
    ptr->Tele.List.list_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.list_items == NULL)
    {
        MemoryError();
        return 0;
    }

    ptr->Tele.List.value_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.value_items == NULL)
    {
        MemoryError();
        return 0;
    }

    for(int x=0; x<ptr->Tele.List.item_count; x++)
    {
        memset(err_msg, 0x00, sizeof(err_msg) );
        sprintf(err_msg, "%d", addr_list[x]);

        ptr->Tele.List.list_items[ item_pos ] = new char[strlen(err_msg)];
        if(ptr->Tele.List.list_items[ item_pos ] == NULL)
        {
            MemoryError();
            return Pt_CONTINUE;
        }

        strcpy(ptr->Tele.List.list_items[ item_pos ], err_msg);

        ptr->Tele.List.value_items[ item_pos ] = new char[strlen(err_msg)];
        if(ptr->Tele.List.value_items[ item_pos ] == NULL)
        {
            MemoryError();
            return Pt_CONTINUE;
        }
        strcpy(ptr->Tele.List.value_items[ item_pos ], err_msg);
        item_pos++;

        int cur_addr = atoi(token_str2);

        //find out the current item..
        if(addr_list[x] == cur_addr )
        {
            ptr->Tele.List.cur_sel 	= x+1;
            ptr->Tele.List.prev_sel	= x+1;
        }
    }
    ptr->widget_type = WIDGET_TYPE_LIST;
    return 0;
}


int CRf6ReadCfgFile::ReadListBoxField(TelWidgetPtr_t *ptr, char *section)
{
    char buff[512], temp_buff[512];
    char token_str1[512], token_str2[512], token_str3[512];
    char *token=NULL;

    for(int x=0; x<1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str2, token);

        strupr(token_str1);
        strupr(token_str2);
        if(!strcmp(token_str1, "COUNT"))
        {
            if(!strcmp(token_str1, "COUNT") )
            {
                ptr->Tele.List.item_count = atoi(token_str2);
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    ptr->Tele.List.list_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.list_items == NULL)
    {
        MemoryError();
        return 0;
    }

    ptr->Tele.List.value_items = (char **)malloc(ptr->Tele.List.item_count*sizeof(char *));
    if(ptr->Tele.List.value_items == NULL)
    {
        MemoryError();
        return 0;
    }

    int item_pos=0;
    for(int x=0; x<ptr->Tele.List.item_count+1; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str2, token);

        //third token
        strcpy(temp_buff, token_str2);
        token = CRf6Telemetry::findToken2(temp_buff, ",");
        if(token == NULL)
            strcpy(token_str3, token_str2);
        else
        {
            strcpy(token_str3, token);
            strcpy(temp_buff, token_str2);
            token = CRf6Telemetry::findToken1(temp_buff, ",");
            if(token != NULL)
                strcpy(token_str2, token);
        }

        strupr(token_str1);
        if(!strcmp(token_str1, "DEFAULT") || !strncmp(token_str1, "ITEM", 4))
        {
            if(!strcmp(token_str1, "DEFAULT") )
            {
                ptr->Tele.List.cur_sel 	= atoi(token_str2)+1;
                ptr->Tele.List.prev_sel	= atoi(token_str2)+1;
                if(ptr->Tele.List.cur_sel >= ptr->Tele.List.item_count)
                {
                    ptr->Tele.List.cur_sel 	= ptr->Tele.List.item_count;
                    ptr->Tele.List.prev_sel	= ptr->Tele.List.item_count;
                }
            }
            else
            {
                ptr->Tele.List.list_items[ item_pos ] = new char[strlen(token_str3)];
                if(ptr->Tele.List.list_items[ item_pos ] == NULL)
                {
                    MemoryError();
                    return Pt_CONTINUE;
                }
                CRf6Telemetry::strcompress((unsigned char *)token_str3);
                strcpy(ptr->Tele.List.list_items[ item_pos ], token_str3);

                ptr->Tele.List.value_items[ item_pos ] = new char[strlen(token_str2)];
                if(ptr->Tele.List.value_items[ item_pos ] == NULL)
                {
                    MemoryError();
                    return Pt_CONTINUE;
                }
                strcpy(ptr->Tele.List.value_items[ item_pos ], token_str2);
                item_pos++;
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }

    return 0;
}

TelWidgetPtr_t* CRf6ReadCfgFile::ReadHeader(char *section)
{
    //Read Parameters section
    TelWidgetPtr_t *ptr=NULL;
    char buff[512], temp_buff[512];
    char	token_str1[512], token_str2[512], token_str3[512];
    char	*token=NULL;
    int str_cnt=3;

    //Create a pointer
    ptr = AllocPtr();

    for(int x=0; x<str_cnt; x++)
    {
        if(!GetNextLine(buff) )
        {
            SectionError(section);
            return 0;
        }

        if(buff[0] == '[' )
        {
            SectionError(section);
            return 0;
        }

        //first token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken1(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }
        strcpy(token_str1, token);

        //second token
        strcpy(temp_buff, buff);
        if((token = CRf6Telemetry::findToken2(temp_buff, "=") )== NULL)
        {
            SectionError(section);
            return 0;
        }

        strcpy(token_str2, token);
        strcpy(token_str3, token);
        strupr(token_str1);
        strupr(token_str2);
        if(!strcmp(token_str1, "FORMAT") || !strcmp(token_str1, "TYPE") || !strcmp(token_str1, "LABEL")
           || !strncmp(token_str1, "STATE", 5) )
        {
            if(!strcmp(token_str1, "FORMAT") )
            {
                if(!strcmp(token_str2, "STRING") )
                {
                    ptr->widget_type=WIDGET_TYPE_TEXT;
                }
                else if(!strcmp(token_str2, "NUMERIC") )
                {
                    ptr->widget_type=WIDGET_TYPE_NUMERIC;
                }
                else if(!strcmp(token_str2, "LIST") )
                {
                    ptr->widget_type=WIDGET_TYPE_LIST;
                }
                else if(!strcmp(token_str2, "BITMASK") )
                {
                    ptr->widget_type=WIDGET_TYPE_CHECK;
                }
                else if(!strcmp(token_str2, "CHECKBOX") )
                {
                    ptr->widget_type=WIDGET_TYPE_CHECK;
                }
                else if(!strcmp(token_str2, "DATE") )
                {
                    ptr->widget_type=WIDGET_TYPE_DATE;
                }
                else if(!strcmp(token_str2, "TIME") )
                {
                    ptr->widget_type=WIDGET_TYPE_TIME;
                }
                else if(!strcmp(token_str2, "PCULIST") )
                {
                    ptr->widget_type=WIDGET_TYPE_PCU_LIST;
                }
                else if(!strcmp(token_str2, "PCUADDR") )
                {
                    ptr->widget_type=WIDGET_TYPE_ADDR_LIST;
                }
                else if(!strcmp(token_str2, "GROUP") )
                {
                    ptr->widget_type=WIDGET_TYPE_GROUP_START;
                    str_cnt=2;
                }
                else if(!strcmp(token_str2, "GROUPEND") )
                {
                    ptr->widget_type=WIDGET_TYPE_GROUP_END;
                    str_cnt=1;
                }
                else
                {
                    debugLog.logToFile("Error in configuration format in the %s section and format field\n", section);
                    return 0;
                }
                ptr->bwidget_type=1;
            }
            else if(!strcmp(token_str1, "TYPE") )
            {
                if(!strcmp(token_str2, "CHAR") )
                {
                    ptr->data_type = TYPE_CHAR;
                }
                else if(!strcmp(token_str2, "WCHAR") )
                {
                    ptr->data_type = TYPE_WCHAR;
                }
                else if(!strcmp(token_str2, "BYTE") )
                {
                    ptr->data_type = TYPE_BYTE;
                }
                else if(!strcmp(token_str2, "SHORT") )
                {
                    ptr->data_type = TYPE_SHORT;
                }
                else if(!strcmp(token_str2, "USHORT") )
                {
                    ptr->data_type = TYPE_USHORT;
                }
                else if(!strcmp(token_str2, "INT") )
                {
                    ptr->data_type = TYPE_INT;
                }
                else if(!strcmp(token_str2, "UINT") )
                {
                    ptr->data_type = TYPE_UINT;
                }
                else if(!strcmp(token_str2, "LONG") )
                {
                    ptr->data_type = TYPE_LONG;
                }
                else if(!strcmp(token_str2, "ULONG") )
                {
                    ptr->data_type = TYPE_ULONG;
                }
                else if(!strcmp(token_str2, "FLOAT") )
                {
                    ptr->data_type = TYPE_FLOAT;
                }
                else if(!strcmp(token_str2, "DOUBLE") )
                {
                    ptr->data_type = TYPE_DOUBLE;
                }
                else
                {
                    debugLog.logToFile("Error in configuration format in the %s section and type field\n", section);
                    return 0;
                }
                ptr->bdata_type=1;
            }
            else if(!strcmp(token_str1, "LABEL") || !strncmp(token_str1, "STATE", 5) )
            {
                strcpy(ptr->label, token_str3);
                ptr->blabel=1;
            }
        }
        else
        {
            SectionError(section);
            return 0;
        }
    }
    return ptr;
}



IArray<TelWidgetPtr_t> *CRf6ReadCfgFile::ReadConfigFile(char *cfg_file_name)
{
    //Open the Telemetry file

    TelFile.open( cfg_file_name, ios::in | ios::out );

    if(!TelFile)
    {
        TelFile.clear();
        debugLog.logToFile(" file not open %s \n",cfg_file_name);
        return 0;
    }

    //Read Parameters section
    TelWidgetPtr_t *ptr=NULL;
    char buff[512];
    char	section[32];

    while( 1 )
    {
        if(!GetNextLine(buff) )
        {
            break;
        }
        if( !strcmp( buff, "[PARAMETERS]"))
        {
            break;
        }
    }

    if(!GetNextLine(buff) )
    {
        debugLog.logToFile("No fields defined in the file\n");
        return 0;
    }

    int cur_no=-1;
    int prev_no=-1;
    char seq[8];
    while( 1 )
    {
        if(buff[0] == '[' && buff[strlen(buff)-1]==']')
        {
            strcpy(section, buff);
            ptr = ReadHeader(section);

            strncpy(seq, &section[1], 3);
            cur_no = atoi(seq);
            if(prev_no == -1)
                prev_no=cur_no;
            else
            {
                if(cur_no != prev_no+1)
                {
                   debugLog.logToFile("Error in section no %d and it should be %d\n", cur_no, prev_no+1);
                   return NULL;
                }
                prev_no=cur_no;
            }

            mFieldsList->Add(ptr);
            if(ptr->widget_type == WIDGET_TYPE_TEXT)
            {
                ReadTextField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_NUMERIC)
            {
                ReadNumericField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_CHECK)
            {
                ReadCheckBoxField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_LIST)
            {
                ReadListBoxField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_DATE)
            {
                ReadDateField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_TIME)
            {
                ReadTimeField(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_PCU_LIST)
            {
                ReadPcuList(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_ADDR_LIST)
            {
                ReadPcuAddrList(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_GROUP_START)
            {
                if(ReadGroupStart(ptr, section) == -2)
                {
                    strcpy(buff, group_buff);
                    continue;
                }
            }
            else if(ptr->widget_type == WIDGET_TYPE_GROUP_END)
            {
                ReadGroupEnd(ptr, section);
            }
            else if(ptr->widget_type == WIDGET_TYPE_SKIP)
            {
            }

            if(!GetNextLine(buff) )
            {
                break;
            }
            if( !ReadOffsetField(ptr, section, buff) )
                continue;
        }

        if(!GetNextLine(buff) )
        {
            break;
        }
    }

    TelFile.close( );
    TelFile.clear();
    return mFieldsList;
}

IArray<TelWidgetPtr_t> *CRf6ReadCfgFile::ReadDriverConfigFile(QString drivername)
{
    QString fullPath = QString(DataDir) + "/tel/"+ drivername +".cfg";
    return ReadConfigFile(fullPath.toAscii().data() );
}
IArray<TelWidgetPtr_t> *CRf6ReadCfgFile::ReadPcuConfigFile(QString drivername)
{
    QString fullPath = QString(DataDir) + "/tel/"+ drivername +".pcu.cfg";
    return ReadConfigFile(fullPath.toAscii().data() );
}
