#ifndef RF6DBUPDATES_H
#define RF6DBUPDATES_H

#include <QObject>
#include <QMetaType>

#include <QThread>

#include <QSharedPointer>

#include "rf6defines.h"
#include "rf6database.h"
#include "rf6dbobject.h"

#include "eventprocifc.h"

namespace Rf6
{
	// Event types used in subscription
	enum EventTypes
	{
		eventSystem = 0x01,
		eventDatabase = 0x02,
		eventAlarms = 0x04,
		eventUserdef = 0x08,
		eventAll = eventSystem | eventDatabase | eventAlarms | eventUserdef
	};

	// the subscriber thread. It get events from a eventproc queue
	class CRf6EventSubscriber : public QThread, CEventProcInterface
	{
		Q_OBJECT

	public:
		friend class CRtSender;
		enum Rf6DbSubscriberMsg
		{
			CANNOT_OPEN_QUEUE,
			CANNOT_CONNECT_EVPROC,
			SUBSCRIBE_ERROR,
			QUEUE_RECIEVE_ERROR,
			DB_SUBSCRIBER_THREAD_EXITED,
			UNSUPPORTED_EVENT_TYPE,
			RF6_DB_ERROR,
			RF6_TAG_PARENT_ERROR,

		};

		CRf6EventSubscriber(EventTypes _ev_mask)
			: mRunning(true), ev_mask(_ev_mask)
		{
			qRegisterMetaType<QSharedPointer<CRf6DbObject> >("QSharedPointer<CRf6DbObject>");
			qRegisterMetaType<ULONG_64>("ULONG_64");
		}

		void stop(void) {mRunning = false;}

		int subscribe(EventTypes mask);
		int unsubscribe();

	signals:
		void messageSend(int code);

		void tagAdded(const ULONG_64& parent, QSharedPointer<CRf6DbObject> ob);
		void tagDeleted(const ULONG_64& ind);
		void tagUpdated(QSharedPointer<CRf6DbObject> ob);

	protected:
		bool mRunning;
		mqd_t open_queue();
		EventTypes ev_mask;		// subscribed event type

		char eventbuf[MAX_EVENTSIZE];

		virtual void run() {workThread();}
		void workThread();				// thread which do a useful work

		void eventDbProcessing();				// handling of the new database event
		void eventTagDbProcessing(DEVENT* ev);	// handling of the new tag database event

		void tagAddProcessing(ULONG_64 unind);	// tag was added
		void tagUpdProcessing(ULONG_64 unind);	// tag was updated
		int updateObject(QSharedPointer<CRf6DbObject> spObj);

		CRf6Database rf6db;

	};

}

#endif
