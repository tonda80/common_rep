#ifndef RF6TELEMETRYACCESS_H
#define RF6TELEMETRYACCESS_H
#include "rf6readcfgfile.h"
#include <QString>

typedef IArray< TelWidgetPtr_t > ArrayTeleWidget;
class Rf6TelemetryAccess
{
public:
    Rf6TelemetryAccess();
    void requestChanels();
    ArrayTeleWidget *requestPcuTelemetry(int numberChanel);
    ArrayTeleWidget *requestChanel(int numberChanel);
};

#endif // RF6TELEMETRYACCESS_H
