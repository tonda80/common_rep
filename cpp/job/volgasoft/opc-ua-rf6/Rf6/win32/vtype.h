#ifndef __VTYPE_H
#define __VTYPE_H

#include <stdint.h>
#include <time.h>

#ifdef _WIN32
struct timespec 
{
	long tv_sec;
	long tv_nsec;
};

#endif


typedef unsigned short	    USHORT;      // u
typedef unsigned short		WORD;        // u
typedef short				SHORT;       // i
typedef int					INT;         // I
typedef unsigned            UINT;        // U
typedef int					BOOL;        // b
typedef char                CHAR;        // c
typedef unsigned char	    UCHAR;       // h
typedef unsigned char	    BYTE;        // y
typedef unsigned long 	    ULONG;       // l
typedef long				LONG;        // L
typedef unsigned            RF_COLOR;    // R
typedef struct timespec     RF_TIMESPEC; // S
typedef double				DOUBLE;      // D
typedef float               FLOAT;       // F
typedef unsigned            RF_BITMASK;  // M
typedef char                RF_ASCII;    // A
typedef unsigned short      RF_UNICODE;  // W
typedef BYTE                RF_BYTEARRAY;// B
typedef long                RF_TIME;     // T
typedef unsigned short      RF_WCHAR;    // W
typedef unsigned long long  ULONG_64;    // 6
typedef UINT				RF_PRJ;
typedef unsigned long long  UINDEX;


#ifndef TRUE
#define TRUE	1
#endif
#ifndef FALSE
#define FALSE	0
#endif


#define RFT_USHORT      0x75 // u
#define RFT_SHORT       0x69 // i
#define RFT_INT         0x49 // I
#define RFT_UINT        0x55 // U
#define RFT_BOOL        0x62 // b
#define RFT_CHAR        0x63 // c
#define RFT_UCHAR       0x68 // h
#define RFT_BYTE        0x79 // y
#define RFT_ULONG       0x6c // l
#define RFT_LONG        0x4c // L
#define RFT_COLOR       0x52 // R
#define RFT_TIMESPEC    0x53 // S
#define RFT_DOUBLE      0x44 // D
#define RFT_FLOAT       0x46 // F
#define RFT_BITMASK     0x4d // M
#define RFT_ASCII       0x41 // A
#define RFT_UNICODE     0x57 // W
#define RFT_BYTEARRAY   0x42 // B
#define RFT_TIME        0x54 // T
#define RFT_WCHAR		RFT_UNICODE
#define	RFT_I64			0x34 // 4
#define	RFT_U64			0x36 // 6
#endif
