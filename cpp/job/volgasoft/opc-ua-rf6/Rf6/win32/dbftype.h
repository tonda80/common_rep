#ifndef __DBFTYPE_H
#define __DBFTYPE_H

#include <time.h>
#include <dbconst.h>
#include <vtype.h>
#include <limits.h>
#include <_pack1.h>

//************************************
// Record definitions
//************************************

typedef struct _NFLAG_REC
{
	UINT fl0		: 1;  // active
	UINT fl1		: 1;  // SPARE
	UINT fl2		: 1;  // associated with deltax pump record
	UINT fl3		: 1;  // alarm enable/disable
	UINT fl4		: 1;  // information tagged
	UINT fl5		: 1;  // control tagged
	UINT fl6		: 1;  // used by RemFlex
	UINT fl7		: 1;  // user flags enable
	UINT fl8		: 1;  // DOT alarm configuration enabled
	UINT fl9		: 1;  // used by RemFlex
	UINT fl10		: 1;  // Reason code generation enabled
	UINT fl11		: 1;  // analog controllable
	UINT fl12		: 1;  // SPARE
	UINT fl13		: 1;  // meterproc delta exceeded
	UINT fl14		: 1;  // CONTROL READY FOR TIMEOUT (out of limbo)
	UINT fl15		: 1;  // alarmproc +a option mark for ack or
						  // acknowledge Generated  0 = NO - 1 = YES

	UINT an_roc		: 1;  // analog roc
	UINT an_high	: 1;  // analog high
	UINT an_low		: 1;  // analog low
	UINT an_hhigh	: 1;  // analog high high alarm
	UINT an_llow	: 1;  // analog low low alarm
	UINT an_user1	: 1;  // analog high
	UINT an_user2	: 1;  // analog low
	UINT an_user3 	: 1;  // analog high high alarm
	UINT an_user4	: 1;  // analog low low alarm

	UINT sta_a		:1;

	UINT b_no_reply	 : 1;  // no reply set if rtu did not reply to request
	UINT b_alarm	 : 1;  // alarm condition
	UINT b_unack	 : 1;  // unacknowledged alarm condition
	UINT b_invalid	 : 1;  // invalid data eg. instrument failure, for PCU indicates SCAN state if b_no_reply = 0
	UINT b_control	 : 1;  // control outstanding eg control sent but not cleared
	UINT un_aut		 : 1;  // unauthorized status change
	UINT cold_start  : 1;  // cold start bit, set by database builder and coldstart
	UINT over_write	 : 1;  // manual over write
}NFLAG_REC;

// default record header
typedef struct _NREC_HDR
{
	UINT		disk_record;
	ULONG_64	unique_index;
	UINT		remGroup;
	UINT		repGroup;
	UINT		zoneID;
	RF_PRJ		prjCount;
}NREC_HDR;

// realtime record header
typedef struct _NR_REC_HDR
{
	NREC_HDR		rechdr;
	NFLAG_REC		flags;
	short int		custom_flags;
	RF_TIMESPEC		system_tstamp;
	RF_TIMESPEC		tstamp;

	UINT			updateCounter;
	UINT			rtu_nbr;   // rtu index
	UINT			rtu_point; // point offset
	UINT			aux_addr;
	BYTE			sub_type;
	UINT			readPerm;
	UINT			writePerm;
	UINT			group;
}NR_REC_HDR;

// reference to historical database
typedef struct _HIST_FLAGS
{
	USHORT	fl0 : 1; // collection on/off
	USHORT	fl1 : 1; // active hist collection point
	USHORT	fl2 : 1; // sample
}HIST_FLAGS;

typedef struct _HIST_COLL
{
	DOUBLE		deadband; // normal operation
	ULONG		lifetime; // upper word = day, week, month year, lower word = number of...
	ULONG		scantime; // time sampleing
	HIST_FLAGS	flags;
	USHORT		dllIndex;
	time_t		startTime;
	union {
		DOUBLE	d;
		CHAR	c;
		LONG 	l;
		CHAR	t[3];
	};
}HIST_COLL;

// PCU communication statistic
typedef struct _NR_COMM_STATS
{
	UINT	total_req;
	UINT	good_req;
	UINT	retries;
	UINT	no_response;
	UINT	data_err;
	DOUBLE	comm_eff;
	SHORT	port;
	UINT	node;
	UINT 	baud;
	CHAR	parity;
	CHAR	stop_bits;
	CHAR	data_bits;
	CHAR	analog_scan;
	CHAR	status_scan;
	CHAR	meter_scan;
	CHAR	tank_scan;
	CHAR	aga_scan;
	CHAR	spare_scan_1;
	CHAR	spare_scan_2;
	CHAR	spare_scan_3;
}NR_COMM_STATS;

// PCU record
typedef struct _NPCU_REC
{
	NR_REC_HDR	hdr;
	UINT		repGroup;
	UINT		zoneID;

	NR_COMM_STATS cs;
	INT	 		addr;
	SHORT 		active;
	CHAR		retry_alarm;
	INT         num_status;
	INT         num_analogs;
	INT         num_tanks;
	INT			attr_indx;		/* dbindex into ATTR_DB for this PCU's definitions */
	INT         num_meters;
	CHAR        spare[64];
}NPCU_REC;

typedef NPCU_REC NR_RTU;

typedef struct _NSCAN_FIELDS
{
	SHORT	active;
	SHORT	scan_freq;
}NSCAN_FIELDS;

typedef struct _NANALOG_LIMIT
{
	DOUBLE		limit;
	RF_COLOR 	color;		  // color for alarm banner
	CHAR		prio;		  // alarm priority
	CHAR		direction;    // ALM_RISING upper limit/ALM_FALLING=lower limit
	CHAR		name_index;   // index for limit name LoLo, Lo, Hi, HiHi, ROC, Instr
	CHAR		alm_class;	  // alarm class: Ok, Ev, A, C, U, No Alarm
	UINT		alm_action;   // action processor code
	USHORT		action_number;
}NANALOG_LIMIT;

typedef struct _NANALOG_REC
{
	NR_REC_HDR		hdr;
	HIST_COLL		histc;
	NSCAN_FIELDS 	sf;
	UINT			flags; // fl16-fl24 - number of extra limits

	DOUBLE		eu_value, min_eu, max_eu;

	BYTE		eu_type; // eu conversion type 0=none 1=linear 2=sq
	INT			min_raw; // 16-Aug-2001 short -> int
	INT			max_raw; // 16-Aug-2001 short -> int
	CHAR		roc_type; //	rate of change limit checking type
						  //	0 = no roc performed
						  //	1 = relative roc/percent/second
						  //	2 = absolute roc eu/second

	CHAR		roc_inhibit; // time in seconds where the roc is
							 // inhibited since last roc was detected
	DOUBLE		roc_limit;
	LONG		roc_time;    // time in seconds when last roc performed
	LONG		delta;       // time until next roc check
	DOUBLE		roc_eu;      // last eu when roc was made

	BYTE 		alm_deadband; // alarm limit deadband percentage

	DOUBLE		control_min;
	DOUBLE		control_max;
	// 16-Aug-2001 limits defined using NANALOG_LIMIT
	// flags::fl16-fl24 defines how many alarms this record has
//	NANALOG_LIMIT alarm[8];
}NANALOG_REC;

typedef struct _NSTATUS_STATE
{
	RF_COLOR	color;
	CHAR		attr_a;
	CHAR		cntrl_id;
	CHAR		prio;
	CHAR		alm_class;  // Normal, Ev, Al, Ur, Cr, No Alarm
	UINT 		alm_action; // action processor code
}NSTATUS_STATE;

typedef struct _NSTATUS_REC
{
	NR_REC_HDR	 hdr;
	HIST_COLL	 histc;
	NSCAN_FIELDS sf;
	UINT		 flags;

	SHORT 		db_offset;
	CHAR		cntrl_type; // control req_type
	CHAR		start_bit;

	CHAR		num_bit;	// 1 to 5 max = 32 states
	BYTE		cur_val;
	BYTE		timer; 		// control time_out = (timer * 10) in seconds
	BYTE		oper_timer; // momentary control timer * 50 msec
	BYTE		norm_state;	
}NSTATUS_REC;

typedef struct _NMETER_REC
{
	NR_REC_HDR	 hdr;
	HIST_COLL	 histc[5];
	NSCAN_FIELDS sf;
	UINT		 flags;

	CHAR		group;
	UCHAR		meter_type;
	DOUBLE		factor;
	DOUBLE		ppunit;

	LONG		curr_value;
	LONG		curr_gross;
	LONG		rollover;
	LONG		yearly;
	LONG		monthly;
	LONG		daily;
	LONG		yesterday;
	LONG		hourly;
	LONG		last_hour;
	LONG		last_good;
	LONG		max_raw;

	USHORT		frac;
}NMETER_REC;

typedef struct NR_PRODUCT  /* Product Name structure	*/
{
	USHORT head;
	char   name[12];
	USHORT color;
	char   spare[2];
} NR_PRODUCT;


typedef struct _NTANK_REC
{
	NR_REC_HDR		hdr;
	HIST_COLL		histc[5];
	NSCAN_FIELDS	sf;
	UINT			flags;

	CHAR			product_code;  // A single digit reference

	struct                         // Alarm structure
	{
		USHORT
		SETPOINT:	1,
		TRANSMIT:	1,
		HIHI:		1,
		LOLO:		1,
		HI:			1,
		LO:			1;
	} alarms;

	// 16-Aug-2001 all floats changed to doubles
	struct
	{
		DOUBLE		open_vol;           // Opening Volume, before Trans
		DOUBLE		close_vol;          // Closing Volume, after Trans
		DOUBLE		trans_vol;          // Transaction Volume    (NET)
		LONG		open_time;          // Opening Time of Trans
		LONG		close_time;         // Closing Time of Trans
		LONG		open_meter;
		LONG		close_meter;
		LONG		trans_meter;
		LONG		over_short;
		CHAR		transaction_type;   // r: RCV  d: DLV  n: No Transaction
		SHORT		other_id;           // ID of the other entity in transaction
	} transaction;

	DOUBLE	max_vol;		// Maximum Tank Volume
	DOUBLE	avail_vol;		// Available Storage Volume
	DOUBLE	volume;			// curr net volume
	DOUBLE	last;			// last volume
	DOUBLE	flow_rate;      // Flow Rate eg: GPM
	DOUBLE	gravity;
	DOUBLE	temperature;

	BYTE	level[3];		// Current Level ft,inch,frac
	BYTE 	max_level[2];	// Maximum Level ft,inch
	BYTE 	lo[2];			// low level in ft,inch
	CHAR	lPrio;
	BYTE 	lolo[2];		// low low level ft, inch
	CHAR	llPrio;
	BYTE 	hi[2];			// high level in ft, inch
	CHAR	hPrio;
	BYTE 	hihi[2];		// high high level in ft, inchs
	CHAR	hhPrio;

	RF_COLOR	llm_color;
	RF_COLOR	lllm_color;
	RF_COLOR	hlm_color;
	RF_COLOR	hhlm_color;

	CHAR	fractions;		// Fraction conv 8ths, 16ths
	CHAR	level_conv;		// 0 - no, 1 - linear, 2 - strapping
	CHAR	level_type;		// 0 - FT/IN, 1 - METERS/CM
	CHAR	moving;			// 0 - no movement, 1 - up, 2 - down
	CHAR	t_type;			// transaction type see above
	CHAR	st_feet;
	CHAR	st_inch;
	CHAR	st_frac;
	CHAR	sp_feet;
	CHAR	sp_inch;
	CHAR	sp_frac;
	LONG	start_fracs;	//	start Fractions for lookup
	LONG	end_fracs;		// 	end Fractions for lookup
	DOUBLE	vol_per_fracs;	// 	Volume per fractional part
}NTANK_REC;

typedef struct _IOSCAN_DATA
{
	CHAR	data[64];
}IOSCAN_DATA;

typedef struct _NRECORD_HEAD_STRUCT
{
	NREC_HDR	rechdr;
	NFLAG_REC	flgs;
	// fl0..fl4 reserved for db use.
	// fl0 - record locked
	// fl1 - active
	// fl7 - used by Windows RemFlex
}NRECORD_HEAD_STRUCT;

typedef struct _NALM_REC
{
	RF_TIMESPEC	tstamp;
	
	SHORT	alm_state;
	SHORT	alm_type;
	UINT	dbtype;
	DOUBLE	alm_value;
	DOUBLE	alm_limit;
	UINT	aux_data; // 16-Aug-2001 ushort -> uint
}NALM_REC;

typedef struct _NAUTHORIZE_CNG_STRUCT
{
	NRECORD_HEAD_STRUCT head;
	/*
			head.flgs.fl1 - active record...i.e. outstanding control
			head.flgs.fl5 - failed
			head.flgs.fl6 - execute control valid
			fl7 .. fl13 - spares
			fl14.. Control ready for timeout test. (out of limbo)
			fl15.. Generated Acknowledge AK event
	*/
	UINT	rtu_nbr;
	UINT	db_index;
	time_t	time_out;   /* qnx time when a control will have timed out */
	CHAR	change1;
	CHAR	change2;
	UINT	dbtype;
	CHAR	SPARE[2];
}NAUTHORIZE_CNG_STRUCT;

typedef struct
{
	NREC_HDR	rechdr;
	ULONG_64	dbPoint_index;
	ULONG_64	console_index;
	ULONG_64	user_index;
	RF_TIMESPEC	infotstamp;
	ULONG_64	cconsole_index;
	ULONG_64	cuser_index;
	RF_TIMESPEC	ctltstamp;
} INFO_CTL, *PINFO_CTL;

typedef struct info_msg
{
	INFO_CTL	info;
	char 		data[ MAX_INFO_TAG*MB_LEN_MAX ];
}info_msg_t;

typedef struct  {
	NREC_HDR	rechdr;
	UINT		flags;
}URTFILE_REC, *PURTFILE_REC;

typedef struct {
	NREC_HDR	rechdr;
	UINT		flags;
	UINT		tcpAddress;
	RF_TIMESPEC	tsStart;
	RF_TIMESPEC	tsLogon;
	ULONG_64	user_index;
}CON_REC, *PCON_REC;

typedef struct {
	NREC_HDR rechdr;
	UINT	 flags;
	UINT	 groupID;
	UINT	 readZone;
	UINT	 ctlZone;
	RF_WCHAR passwd[16];
}USER_REC, *PUSER_REC;

typedef struct {
	NREC_HDR rechdr;
	ULONG_64 itemsMask;
	ULONG_64 passMask;
}USER_GROUP_REC, *PUSER_GROUP_REC;

// disk with memory types only
// when adding a new type, be sure it is defined in dbconst.h
// use dbtypes that have a NREC_HDR member!!!
typedef union ndbType{
	NR_REC_HDR		*h;
	NPCU_REC 		*p;
	NANALOG_REC		*a;
	NMETER_REC		*m;
	NSTATUS_REC		*s;
	NTANK_REC		*t;
	URTFILE_REC		*urt;
	CON_REC			*con;
	USER_REC		*user;
	USER_GROUP_REC	*ugroup;
// exception to the above rule. Needed for legacy scanners.
	NAUTHORIZE_CNG_STRUCT *acs;
}_DbType;

#include <_packpop.h>
#endif
