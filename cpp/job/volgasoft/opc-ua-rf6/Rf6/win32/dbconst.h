#ifndef _SCANKIT_DBCONST_H
#define _SCANKIT_DBCONST_H

#include <limits.h>
#ifdef __cplusplus
extern "C" {
#endif

#ifndef _RF_SWCHAR_T
#define _RF_SWCHAR_T
typedef unsigned short swchar_t;
#endif

#ifndef MAX_PATH
#define MAX_PATH PATH_MAX + 1
#endif

#define MAX_INFO_TAG		401	// includes NULL character

// RF4 Compatible DB Types --------------------------------------
#define ANALOG_DB          	0
#define METER_DB           	1
#define STATUS_DB          	2
#define TANK_DB            	3
#define CRT_DB             	4
#define PCU_DB             	5
#define AUTHORIZE_CNG_DB   	6
#define ACTIVE_ALM_DB      	7
#define EVENT_DB           	8
#define TRN_INDEX_DB       	9
#define TRN_REC_DB         	10
#define PAGING_DB          	11
#define SETPOINT_DB       	12
#define CRTALARM_DB			13
#define HISTCOLL_DB		 	14
#define PRODUCT_DB		   	15
#define SPC_DB			   	16
#define SYM_DB			   	17
#define PUMP_DB			  	18
#define ATTR_DB			   	19		/* User defined attribute flag definitions */
#define STATUS_TIME_DB 	    97
#define ANALOG_TIME_DB  	98
#define METER_TIME_DB   	99
#define ANALOG_ALM_HH	 	100
#define ANALOG_ALM_H		101
#define ANALOG_ALM_LL		102
#define ANALOG_ALM_L	 	103
#define ANALOG_ALM_HH_OK	104
#define ANALOG_ALM_H_OK	  	105
#define ANALOG_ALM_LL_OK   	106
#define ANALOG_ALM_L_OK	   	107
#define STATUS_ALM		   	108
#define STATUS_ALM_OK	 	109
#define STATUS_STUFF	   	110
#define ANALOG_STUFF	   	111
#define ANALOG_DB_STATUS	112
#define METER_DB_STATUS		113
#define STATUS_DB_STATUS	114
#define TANK_DB_STATUS		115
#define NULL_DB	-1
#define RTU_DB	PCU_DB
// End of RF4 Compatible DB Types -------------------------------

// Disk & Memory DB Types
// when adding a new disk/memory type, be sure to add in dbftype.h in union _DbType
#define NANALOG_DB      0
#define NANALOG_DB_FILE	"/analog"
#define NANALOG_DB_NAME	"NANALOG_DB"
#define NMETER_DB       1
#define NMETER_DB_FILE	"/meter" 
#define NSTATUS_DB      2
#define NSTATUS_DB_FILE	"/status"
#define NTANK_DB        3
#define NTANK_DB_FILE	"/tank"
// If adding a new disk type, add after tank if using a key pair
// ** add file name to syscfg.cpp

#define NPCU_DB         14
#define NRTU_DB_FILE	"/rtu"
// If adding a new disk type, add after rtu if using a single key

#define NACTIVE_ALM_DB	25
#define NALARM_DB_FILE	"/alarm"
#define NEVENT_DB       26
#define NEVENT_DB_FILE	"/event"
#define NCRT_DB         27 
#define NCRT_DB_FILE	"/crt"
#define NPAGING_DB      28 
#define NPAGING_DB_FILE	"/paging"
#define NPRODUCT_DB		29
#define NPRODUCT_DB_FILE "/product"
#define NSPC_DB			30
#define NSPC_DB_FILE	"/spc"
#define NATTR_DB		31	
#define NATTR_DB_FILE	"/attr"
#define URTFILE_DB		 32	
#define URTFILE_DB_FILE	"/urt"
#define CON_DB			 33	
#define CON_DB_FILE		"/console"
#define USER_DB			 34	
#define USER_DB_FILE	"/user"
#define USER_GROUP_DB	 35	
#define USER_GROUP_DB_FILE	"/userg"
// Non-Disk DB Types
#define NAUTHORIZE_CNG_DB	36
#define NAUTHORIZE_DB_FILE	"/authorize"

// Non-Memory types
#define INFO_CTL_TAG_DB			 81
#define INFO_CTL_TAG_DB_FILE	"/ict"

#define NSTATUS_TIME_DB 	101
#define NANALOG_TIME_DB  	102
#define NMETER_TIME_DB   	103

// used with que_raw_data_ex_t to pass pid of the source
#define NSTATUS_TIME_DB_EX 	111
#define NANALOG_TIME_DB_EX 	112
#define NMETER_TIME_DB_EX  	113

#define NANALOG_ALM_HH	 	200
#define NANALOG_ALM_H		201
#define NANALOG_ALM_LL		202
#define NANALOG_ALM_L	 	203
#define NANALOG_ALM_HH_OK	204
#define NANALOG_ALM_H_OK	205
#define NANALOG_ALM_LL_OK  	206
#define NANALOG_ALM_L_OK	207
#define NSTATUS_ALM		   	208
#define NSTATUS_ALM_OK	 	209
#define NSTATUS_STUFF	   	210
#define NANALOG_STUFF	   	211
#define NANALOG_DB_STATUS	212
#define NMETER_DB_STATUS	213
#define NSTATUS_DB_STATUS	214
#define NTANK_DB_STATUS		215

#define MIN_TYPES			NANALOG_DB
#define MAX_TYPES			NTANK_DB_STATUS
#define MAX_DB_DISK_TYPE	NAUTHORIZE_CNG_DB
//USER_GROUP_DB

#define ALLTAG_DB			(MAX_TYPES+1)

#define NULL_DB				-1
#define NRTU_DB				NPCU_DB
#define NSYM_DB			    1000

// report database type
#define NREPORT_DB			1001

// project file type. Using in events
#define NPROJECTFILE		1002

// Tag database properties
#define NDBPROP_EXTDESC		1003

// max size of pcu and tag name including null termination
#define MAX_KEY_LEN		200

// Macros to determine if unique index is of ?_DB type.
#define _DB_TYPE(x)		((unsigned int)((x >> 56) & 0xFF))
#define _IS_ANALOG(z)	(( z >> 56 ) == NANALOG_DB	? 1 : 0)
#define _IS_METER(z) 	(( z >> 56 ) == NMETER_DB	? 1 : 0)
#define _IS_STATUS(z) 	(( z >> 56 ) == NSTATUS_DB	? 1 : 0)
#define _IS_PCU(z) 		(( z >> 56 ) == NPCU_DB		? 1 : 0)
#define _IS_TANK(z) 	(( z >> 56 ) == NTANK_DB	? 1 : 0)

// default sizes for alarms
#define	_RF_DB_ALARM_LL		0
#define	_RF_DB_ALARM_L		_RF_DB_ALARM_LL + 1
#define	_RF_DB_ALARM_H		_RF_DB_ALARM_LL + 2
#define	_RF_DB_ALARM_HH		_RF_DB_ALARM_LL + 3
#define	_RF_DB_ALARM_U1		_RF_DB_ALARM_LL + 4
#define	_RF_DB_ALARM_U2		_RF_DB_ALARM_LL + 5
#define	_RF_DB_ALARM_U3		_RF_DB_ALARM_LL + 6
#define	_RF_DB_ALARM_U4		_RF_DB_ALARM_LL + 7
#define _RF_DB_ALARM_MAX	8

#define _RF_DB_ALARM_STATE_0	0
#define _RF_DB_ALARM_STATE_1	_RF_DB_ALARM_STATE_0 + 1
#define _RF_DB_ALARM_STATE_2	_RF_DB_ALARM_STATE_0 + 2
#define _RF_DB_ALARM_STATE_3	_RF_DB_ALARM_STATE_0 + 3
#define _RF_DB_ALARM_STATE_MAX	32

#define	_RF_DB_TAG 		0
#define	_RF_DB_DESC		_RF_DB_TAG + 1
#define	_RF_DB_PCU		_RF_DB_TAG + 2
#define	_RF_DB_UNITS	_RF_DB_TAG + 3
#define	_RF_DB_TIME		_RF_DB_TAG + 4
#define	_RF_DB_DATA		_RF_DB_TAG + 5
#define	_RF_DB_ALARM	_RF_DB_TAG + 6
#define	_RF_DB_SD1		_RF_DB_TAG + 7
#define	_RF_DB_SD2		_RF_DB_TAG + 8
#define	_RF_DB_SD3		_RF_DB_TAG + 9
#define	_RF_DB_SD4		_RF_DB_TAG + 10
#define	_RF_DB_SD_MAX	32

// Flags for meter manual overwrite, indicates which field is MOW'd
// 
#define	_RF_METER_FLAG_NET	 0x01 
#define	_RF_METER_FLAG_HOUR  0x02
#define	_RF_METER_FLAG_DAY 	 0x04
#define	_RF_METER_FLAG_MONTH 0x08
#define	_RF_METER_FLAG_YEAR  0x10

// Database subtypes
#define NMETER_CURR_VALUE	0
#define NMETER_CURR_GROSS	1
#define NMETER_HOURLY		2
#define NMETER_LAST_HOUR	3
#define NMETER_LAST_GOOD	4
#define NMETER_DAILY		5
#define NMETER_YESTERDAY	6
#define NMETER_MONTHLY		7
#define NMETER_YEARLY		8
#define NMETER_FACTOR		9
//
// Flags for UpdateDbRecord(int idx, unsigned flags)
//
#define WR_SHMEM		0x01  // write to the shared memory
#define WR_DISK			0x02  // write to disk 
#define WR_LOG			0x04  // write to log file for autoupdate
#define	WR_UPD			0x08  // send message rupdate
#define WR_STBY			0x10  // send to standby
#define WR_HIST			0x20  // write historical
#define WR_SETTIME		0x40  // set record update time
#define WR_SETHISTTIME	0x80  // set historical update time

#define MAX_WR_FLAG		10

// Meters subtypes historically collected
#define H_MNET		((_RF_METER_FLAG_NET)<<MAX_WR_FLAG)
#define H_MHOUR		((_RF_METER_FLAG_HOUR)<<MAX_WR_FLAG)
#define H_MDAY		((_RF_METER_FLAG_DAY)<<MAX_WR_FLAG)
#define H_MMONTH	((_RF_METER_FLAG_MONTH)<<MAX_WR_FLAG)
#define H_MYEAR		((_RF_METER_FLAG_YEAR)<<MAX_WR_FLAG)

// Flags for histcoll records
//#define _HC_FL_ACTIVE     0x00000001
//#define _HC_FL_STATEON    0x00000002

// Database subtypes
#define NMETER_CURR_VALUE	0
#define NMETER_CURR_GROSS	1
#define NMETER_HOURLY		2
#define NMETER_LAST_HOUR	3
#define NMETER_LAST_GOOD	4
#define NMETER_DAILY		5
#define NMETER_YESTERDAY	6
#define NMETER_MONTHLY		7
#define NMETER_YEARLY		8
#define NMETER_FACTOR		9

#define NTANK_TEMP			0
#define NTANK_LEVEL			1
#define NTANK_VOLUME		2
#define NTANK_GRAVITY		3
#define NTANK_FLOWRATE		4
#define NTANK_AVAIL_VOLUME	5
#define NTANK_PRODUCT_NAME	6
#define NTANK_ALARM_STATUS	7
#define NTANK_LAST_VOLUME	8

#ifdef __cplusplus
};
#endif


#endif

