#include "rf6projectinfo.h"

#include <unistd.h>
#include <stdio.h>

#include <rf_rsrc/api.h>
#include <rf6key/rf6key.h>

#define MAXHOSTNAMELEN	256
// MAXHOSTNAMELEN from <sys\param.h>

namespace Rf6
{
	QString CRf6ProjectInfo::getProjectName()
	{
		rfconfig.ReadCfg();
		return rfconfig.GetProjectName();
	}

	QString CRf6ProjectInfo::getProjectPath()
	{
		return "/opt/rf6/data/" + getProjectName() + "/";
	}

	int CRf6ProjectInfo::getRfState()
	{
		return RF_StateEx();
	}

	QString CRf6ProjectInfo::getNodeName()
	{
		char szNode[MAXHOSTNAMELEN];
		if (gethostname(szNode, MAXHOSTNAMELEN) == 0)
			return szNode;
		else
			return "";
	}

	QString CRf6ProjectInfo::getRfversion()
	{
		return rfversion.GetVersion();
	}

	int CRf6ProjectInfo::checkLicense()
	{
		RF6KEY_INFO rKeyInfo;
		memset(&rKeyInfo, 0, sizeof (rKeyInfo));
		RFKEY_HANDLE pHandle = RFKEY_Init();

		if (pHandle == NULL || RFKEY_GetRFKeyInfo(pHandle, &rKeyInfo) != 0)
			return -1;

		if (rKeyInfo.key_type & RFKEY_OPC_UA)
			return 1;
		else
			return 0;
	}
}
