#ifndef RF6LOADTELEMETRYFILE_H
#define RF6LOADTELEMETRYFILE_H
#include "rf6telemetry.h"
#include <QString>
class CRf6LoadTelemetryFile
{
public:
    CRf6LoadTelemetryFile();
    ~CRf6LoadTelemetryFile();
    int PcuError();
    int LoadTextField(TelWidgetPtr_t *node);
    int LoadNumericField(TelWidgetPtr_t *node);
    int LoadListField(TelWidgetPtr_t *node);
    int LoadCheckBoxField(TelWidgetPtr_t *node);
    int LoadTimeField(TelWidgetPtr_t *node);
    int LoadDateField(TelWidgetPtr_t *node);
    int LoadTeleFromFile(char *tel_file_name, IArray< TelWidgetPtr_t > *CfgArray);
    int loadTelemetryForChanel(int chanel,IArray< TelWidgetPtr_t > *CfgArray);


    int loadTelemetryForPcu(QString pcuName, IArray<TelWidgetPtr_t> *CfgArray);
private:
    TEL_HDR	 tel_hdr;
    TPcuObj PcuObj;
    int PcuIndex;
    char	err_msg[512];
    int nChannelNo;
    std::fstream  File;
};

#endif // RF6LOADTELEMETRYFILE_H
