#ifndef RF6DBOBJECT_H
#define RF6DBOBJECT_H

#include <QObject>
#include <QString>
#include <QDebug>

#include <rfbase.hpp>
#include <dbconst.h>

#include "rf6defines.h"


namespace Rf6
{
	class CRf6Database;

	// Type for a primary work (read/write of the value) with tags
	typedef union
	{
		char StaValue;
		double AnaValue;
	} SimpleDbObject;
	
	// class representative a object of rf6 database
	class CRf6DbObject : public QObject
	{
		Q_OBJECT
	public:
		friend class CRf6Database;
		friend class CRf6RtSubscriber;
		friend class CRf6EventSubscriber;

		CRf6DbObject(): unindex(0), index32(0) {}

		CRf6DbObject(int dbtype, QString name_): unindex(0), name(name_), index32(0) {SetDbType(dbtype);}	// the creation of object for creation
		CRf6DbObject(ULONG_64 ui): unindex(ui), index32(0) {}		// the creation of object for read/write/delete

		int GetDbType(void) const {return static_cast<int>(unindex >> 56);}
		const ULONG_64& GetIndex(void) const { return unindex;}
		const QString& GetName(void) const {return name;}

		NPCU_REC* PtrPcuData(void) {if (unindex==0 || GetDbType()!= NPCU_DB) return NULL; return &(data.p);}
		NANALOG_REC* PtrAnaData(void) {if (unindex==0 || GetDbType()!= NANALOG_DB) return NULL; return &(data.a);}
		NMETER_REC* PtrMetData(void) {if (unindex==0 || GetDbType()!= NMETER_DB) return NULL; return &(data.m);}
		NSTATUS_REC* PtrStaData(void) {if (unindex==0 || GetDbType()!= NSTATUS_DB) return NULL; return &(data.s);}
		NTANK_REC* PtrTnkData(void) {if (unindex==0 || GetDbType()!= NTANK_DB) return NULL; return &(data.t);}

		// access to CRF_Fields attributes. Use caution because a internal buffer of rf6 database may be changed by other code (i.e. from other thread)!
		CMemObject* GetRdMemObject(CRf6Database& db);		// the function reads the object and returns a pointer to MemObject or NULL when was a error.
		int WriteMemObject(char operation_type = CMemObject::_NO_DISK_WRITE);

		NANALOG_LIMIT* GetAlarmLimit(CRf6Database& db, int limit_type);		// limit_type is HIHI, HI, etc (see dbconst.h _RF_DB_ALARM_LL)
		NSTATUS_STATE* GetAlarmState(CRf6Database& db, int num);			// num is [0, 1, 2, 3]



        int  setStatusDesc(CRf6Database &db, int num, const char *desc);

        const char * getUnits(CRf6Database &db);
        int setUnits(CRf6Database &db, const char *desc);
    private:

		ULONG_64 unindex;
		QString name;	// the field is not necessarily filled

		CMemObject* rdMemobj;	// readed MemObject
		unsigned int index32;	// 32 bit index. Used into operations with pMemobject

		union
		{
			NPCU_REC 	p;
			NANALOG_REC	a;
			NMETER_REC	m;
			NSTATUS_REC	s;
			NTANK_REC	t;
		} data;

		void SetDbType(int type) {unindex = (unindex & 0xffffffffffffffll) | (static_cast<ULONG_64>(type) << 56); memset(&data, 0 ,sizeof(data));}

		int ReadFromDbRec(_DbType DbRec);
		int WriteToDbRec(_DbType DbRec) const;

	};
}

#endif
