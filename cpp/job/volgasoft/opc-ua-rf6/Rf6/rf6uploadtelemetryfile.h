#ifndef RF6UPLOADTELEMETRYFILE_H
#define RF6UPLOADTELEMETRYFILE_H
#include "rf6telemetry.h"
class QString;
class Rf6UploadTelemetryFile
{
public:
    Rf6UploadTelemetryFile();
    int SaveTeleInFile(char *tel_file_name, IArray<TelWidgetPtr_t> &CfgArray);
    int SaveDateField(TelWidgetPtr_t *node);
    int SaveTimeField(TelWidgetPtr_t *node);
    int SaveCheckBoxField(TelWidgetPtr_t *node);
    int SaveListField(TelWidgetPtr_t *node);
    int SaveNumericField(TelWidgetPtr_t *node);
    int SaveTextField(TelWidgetPtr_t *node);
    int PcuError(int bWrite = 0);
    int SaveTelemetryForPcu(QString pcuName, IArray<TelWidgetPtr_t> *CfgArray);
    int SaveTelemetryForPcu(int chanel, QString pcuName, IArray<TelWidgetPtr_t> *CfgArray);
    int SaveTelemetryForChanel(int chanel, IArray<TelWidgetPtr_t> *CfgArray);
    void setTelHdr(char *scanerName);
private:
    TPcuObj PcuObj;
    int PcuIndex;
    char	err_msg[255];
    TEL_HDR	 tel_hdr;
    int nChannelNo;
    char	scanner_name[128];
    std::fstream  File;

};

#endif // RF6UPLOADTELEMETRYFILE_H
