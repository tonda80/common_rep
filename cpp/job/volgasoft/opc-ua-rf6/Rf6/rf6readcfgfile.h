#ifndef RF6READCFGFILE_H
#define RF6READCFGFILE_H

#include "rf6telemetry.h"
#include <QString>

class CRf6ReadCfgFile
{
public:
    CRf6ReadCfgFile();
    ~CRf6ReadCfgFile();
    TelWidgetPtr_t *AllocPtr();
    int MemoryError(void);
    int SectionError(const char *section);
    int GetNextLine(char	*buff1);
    int ReadOffsetField(TelWidgetPtr_t *ptr, char *section, char *buff);
    int ReadTextField(TelWidgetPtr_t *ptr, char *section);
    int ReadDateField(TelWidgetPtr_t *ptr, char *section);
    int ReadTimeField(TelWidgetPtr_t *ptr, char *section);
    int ReadGroupStart(TelWidgetPtr_t *ptr, char *section);
    int ReadGroupEnd(TelWidgetPtr_t *ptr, char *section);
    int ReadNumericField(TelWidgetPtr_t *ptr, char *section);
    int ReadCheckBoxField(TelWidgetPtr_t *ptr, char *section);
    int ReadPcuList(TelWidgetPtr_t *ptr, char *section);
    int ReadPcuAddrList(TelWidgetPtr_t *ptr, char *section);
    int ReadListBoxField(TelWidgetPtr_t *ptr, char *section);
    TelWidgetPtr_t* ReadHeader(char *section);
    IArray< TelWidgetPtr_t > * ReadConfigFile(char *cfg_file_name);
    IArray< TelWidgetPtr_t > * ReadDriverConfigFile(QString drivername);

    IArray<TelWidgetPtr_t> *ReadPcuConfigFile(QString drivername);
private:

    IArray< TelWidgetPtr_t > *mFieldsList;
    std::ifstream  TelFile;
    TPcuObj PcuObj;
    char	err_msg[512];
    int nChannelNo;
};

#endif // RF6READCFGFILE_H
