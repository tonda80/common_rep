#include "rf6readchnfile.h"

#include "helper.h"


CRf6ReadChnFile::CRf6ReadChnFile()
{
}

CRf6ReadChnFile::~CRf6ReadChnFile()
{
}

int  CRf6ReadChnFile::GetNextLine(char	*buff1)
{
    char	buff[512];
    if(!chnFile)
        return 0;

    while( chnFile )
    {
        chnFile.getline( buff, 255 );
        if(!chnFile)
            return 0;

        CRf6Telemetry::strcompress((unsigned char *)buff);
        if(buff[0] == '#')
            continue;

        if(buff[0] == ';')
            continue;

        if(!strlen(buff) )
            continue;
        break;
    }

    char	temp_buff[512];
    char	*token=NULL;
    strcpy(temp_buff, buff);
    token = CRf6Telemetry::findToken1(temp_buff, "#");
    if(token != NULL)
    {
        strcpy(buff1, token);
    }
    else
    {
        strcpy(buff1, buff);
    }
    return RF_TRUE;
}

int CRf6ReadChnFile::readDriverChannelFile()
{
    sprintf(chnPath, "%s/tel/driver.chn", DataDir);

    //Open the Telemetry file

    chnFile.open( chnPath, ios::in );

    if(!chnFile)
    {
        debugLog.logToFile("Error in Opening the driver.chn file ChnPath = %s \n", chnPath);
        return RF_FAILURE;
    }

    char buff[512], temp_buff[512];
    int  channel_num;
    char *token1=NULL, *token2=NULL;

    while( 1 )
    {
        if(!GetNextLine(buff) )
            break;

        strcpy(temp_buff, buff);
        token1 = CRf6Telemetry::findToken1(temp_buff, " \r\t");
        if(token1 == NULL)
        {
            debugLog.logToFile("Error in in the %s file, in line %s\n", chnPath, buff);
            return RF_FAILURE;
        }

        strcpy(temp_buff, buff);
        token2 = CRf6Telemetry::findToken2(temp_buff, " \r\t");
        if(token2 == NULL)
        {
            debugLog.logToFile("Error in in the %s file, in line %s", chnPath, buff);
            return RF_FAILURE;
        }
        channel_num = atoi(token1);

        if(channel_num == nChannelNo)
        {
            strcpy(scanner_name, token2);
            chnFile.close( );
            return RF_SUCCESS;
            break;
        }
    }

    return RF_FAILURE;

}

int CRf6ReadChnFile::readDriverChannelFile(int nChannel, QString &nameDriver)
{
    nChannelNo = nChannel;
    int ret = readDriverChannelFile();

    nameDriver = getScanerName();
    return ret;
}

char * CRf6ReadChnFile::getScanerName()
{
     return scanner_name;
}
