#ifndef RF6DATABASE_H
#define RF6DATABASE_H

#include <QObject>
#include <QSharedPointer>

#include <rfbase.hpp>
#include <dbconst.h>
//#include <vtype.h>

#include "rf6defines.h"
#include "rf6dbobject.h"


namespace Rf6
{
	int GetDbType(const ULONG_64& index);				// function returns dbtype from the unique index
	int GetNode(const ULONG_64& index);					// function returns node from the unique index
	//unsigned int GetIndexFromUnIndex(const ULONG_64& index);		// function selects index from the unique index


	// class encapsulates the work of RealFlex database
	// 64-bit uniqie index is main key of database
	class CRf6Database : public QObject
	{
		Q_OBJECT
	public:
		enum Rf6DbError
		{
			NOERROR = 0,
			UNKNOWNERROR,
			PARENTISNOTPCU,		// parent uind is not PCU uind
			UNKNOWNDBTYPE,		// unknown dbtype of a created tag
			ERRCREATOBJ,		// a object cannot be created
			PARENTNOTFOUND,		// parent is not found
			RF6DBERROR,			// realflex database error
			OBJECTNOTFOUND,		// requested object not found (now this value not used. If need be getRdMemObject will return this error)
			OBJECTISEXISTED,	// object is existed already
		};

		CRf6Database();		// constructor calls Rf6DbOpen()
		~CRf6Database();	// destructor calls Rf6DbClose()

		CMemObject* GetMemObject(int dbType );		// it returns Memobject of the required type. Returns the valid pointer or NULL

		// Getting indexes (unique and casual) by name
		// They return the valid index or zero
		unsigned int GetTagIndex(int dbType, const char *szPcu, const char *szTag);
		ULONG_64 GetTagUnIndex( int dbType, const char* szPcu, const char* szTag );
		ULONG_64 GetPcuUnIndex( const char* szPcu);
		ULONG_64 GetUnIndex(int dbType, unsigned int index);

		// CRUD (create, read, update, delete) of database
		//----

		// If ok returns the valid unique index of the new (or existed) PCU/Tag, else returns zero
		// if the object exists already, "existed" will be set in "true"
		ULONG_64 CreatePcu(const char* PcuName, bool *existed=NULL);
		ULONG_64 CreateTag(int DbType, const char* PcuName, const char * TagName, bool *existed=NULL);

		// If ok functions return 0, else a code of error
		int ReadDbObject(ULONG_64 uind, SimpleDbObject& ob);
		int ReadDbObject(int dbType, const char *szPcu, const char *szTag, SimpleDbObject& ob);

		// If ok functions return 0, else a code of error
		int UpdateDbObject(ULONG_64 uind, SimpleDbObject& ob);
		int UpdateDbObject(int dbType, const char *szPcu, const char *szTag, SimpleDbObject& ob);

		// If ok functions return 0, else a code of error
		int DeleteDbObject(ULONG_64 un_ind);
		int DeletePcu(const char* szPcu);
		int DeleteTag(int dbType, const char* szPcu, const char* szTag);

		//----

	protected:
		int Rf6DbOpen();	// it initialises internal Memobjects. If ok returns 0, else a code of error
		void Rf6DbClose();	// it closes internal Memobjects

		// Objects representing of RealFlex databases
		CMemObject *mPcuDb;		// db of Pcu
		CMemObject *mAnaDb;		// db of analog
		CMemObject *mStaDb;		// db of status
		CMemObject *mMetDb;		// db of meter
		CMemObject *mTnkDb;		// db of tank
	
	private:
		void close_db(CMemObject*& pDb);
		int open_db(CMemObject* pDb);

	// asynchronous interface of database
	public slots:
        void Create(ULONG_64 parent, CRf6DbObject& ob) {HandlerCreate(parent, ob);}
		void Read(ULONG_64 uind) {HandlerRead(uind);}
		void Update(const CRf6DbObject& ob) {HandlerUpdate(ob);}
		void Delete(ULONG_64 uind) {HandlerDelete(uind);}
	signals:
		void CreateComplete(int errcode, ULONG_64 parent, CRf6DbObject& ob);
		void ReadComplete(int errcode, QSharedPointer<CRf6DbObject> ob);
		void UpdateComplete(int errcode, ULONG_64 uind);
		void DeleteComplete(int errcode, ULONG_64 uind);
	// Handlers of asynchronous interface. Now they are called from slots. Perhaps they will be called from other function later on.
	private:
		void HandlerCreate(ULONG_64 parent, CRf6DbObject& ob);
		void HandlerRead(ULONG_64 uind);
		void HandlerUpdate(const CRf6DbObject& ob);
		void HandlerDelete(ULONG_64 uind);

	// --------------------

	public:
		const char *GetObjName(ULONG_64 uind);	// this returns object name by unique index

		CMemObject* getRdMemObject(ULONG_64 unind, unsigned int* ind=NULL, int* del=NULL);

	// getting of objects
	public slots:
		void GetObjects();	// A request on a getting of a list all objects
	signals:
		void GiveObject(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob);	// give another object
		void ObjectsGiven(int cnt_error_getting);							// all objects were given
	private:
		int cnt_error_getting;	// a counter error of the object getting

	};
}

#endif
