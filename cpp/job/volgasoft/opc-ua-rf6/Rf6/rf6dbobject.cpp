#include "rf6dbobject.h"
#include "rf6database.h"

namespace Rf6
{
	int CRf6DbObject::ReadFromDbRec(_DbType DbRec)
	{
		if (unindex == 0) return 1;

		switch (GetDbType())
		{
		case NPCU_DB:
			data.p = *(DbRec.p);
			return 0;
		case NANALOG_DB:
			data.a = *(DbRec.a);
			return 0;
		case NSTATUS_DB:
			data.s = *(DbRec.s);
			return 0;
		case NMETER_DB:
			data.m = *(DbRec.m);
			return 0;
		case NTANK_DB:
			data.t = *(DbRec.t);
			return 0;
		default:
			return 2;
		}
	}

	int CRf6DbObject::WriteToDbRec(_DbType DbRec) const
	{
		if (unindex == 0) return 1;

		NR_REC_HDR *pDstHdr = NULL;			// pointer to header of destination
		const NR_REC_HDR *pSrcHdr = NULL;	// pointer to header of source

		switch (GetDbType())
		{
		case NPCU_DB:
			memcpy(&(DbRec.p->hdr) + 1, &(data.p.hdr)+1, sizeof(NPCU_REC)-sizeof(NR_REC_HDR));	// copies all except header
			pDstHdr = &(DbRec.p->hdr);
			pSrcHdr = &(data.p.hdr);
			break;
		case NANALOG_DB:
			memcpy(&(DbRec.a->hdr) + 1, &(data.a.hdr)+1, sizeof(NANALOG_REC)-sizeof(NR_REC_HDR));	// copies all except header
			pDstHdr = &(DbRec.a->hdr);
			pSrcHdr = &(data.a.hdr);
			break;
		case NSTATUS_DB:
			memcpy(&(DbRec.s->hdr) + 1, &(data.s.hdr)+1, sizeof(NSTATUS_REC)-sizeof(NR_REC_HDR));	// copies all except header
			pDstHdr = &(DbRec.s->hdr);
			pSrcHdr = &(data.s.hdr);
			break;
		case NMETER_DB:
			memcpy(&(DbRec.m->hdr) + 1, &(data.m.hdr)+1, sizeof(NMETER_REC)-sizeof(NR_REC_HDR));	// copies all except header
			pDstHdr = &(DbRec.m->hdr);
			pSrcHdr = &(data.m.hdr);
			break;
		case NTANK_DB:
			memcpy(&(DbRec.t->hdr) + 1, &(data.t.hdr)+1, sizeof(NTANK_REC)-sizeof(NR_REC_HDR));	// copies all except header
			pDstHdr = &(DbRec.t->hdr);
			pSrcHdr = &(data.t.hdr);
			break;
		default:
			return 2;
		}

		// header copied fields
		pDstHdr->rtu_point = pSrcHdr->rtu_point;	// point offset
		pDstHdr->aux_addr = pSrcHdr->aux_addr;
		pDstHdr->sub_type = pSrcHdr->sub_type;
        pDstHdr->flags.fl11 = pSrcHdr->flags.fl11;
		return 0;
	}

    CMemObject* CRf6DbObject::GetRdMemObject(CRf6Database& db)
	{
		int bDel;
		rdMemobj = db.getRdMemObject(unindex, &index32, &bDel);

		if (bDel==1)
			rdMemobj = NULL;	// ?

		return rdMemobj;
	}

	int CRf6DbObject::WriteMemObject(char operation_type)
	{
		if (rdMemobj == NULL)
			return RF_FAILURE;

		return rdMemobj->Write(index32, operation_type);
	}

    NANALOG_LIMIT* CRf6DbObject::GetAlarmLimit(CRf6Database& db, int limit_type)
	{
		if (GetDbType() != NANALOG_DB)
			return NULL;

		GetRdMemObject(db);
		if (rdMemobj == NULL)
			return NULL;

		return rdMemobj->AlarmLimit(limit_type);
	}

	NSTATUS_STATE* CRf6DbObject::GetAlarmState(CRf6Database& db, int num)
	{
		if (GetDbType() != NSTATUS_DB)
			return NULL;

		GetRdMemObject(db);
		if (rdMemobj == NULL)
			return NULL;

        return rdMemobj->AlarmState(num);
    }

    int CRf6DbObject::setStatusDesc(CRf6Database& db, int num, const char* desc)
    {
        if (GetDbType() != NSTATUS_DB)
            return 1;

        GetRdMemObject(db);
        if (rdMemobj == NULL)
            return 2;

        CMemObject *pMO = GetRdMemObject(db);
        if (pMO != NULL){
            //set value desc
            pMO->SetStatusDesc(desc, num );
            WriteMemObject();
            return 0;
        }

        return 3;



    }
    const char *CRf6DbObject::getUnits(CRf6Database &db)
    {
        if  ((GetDbType() != NANALOG_DB) && (GetDbType() != NMETER_DB))
            return NULL;

        GetRdMemObject(db);
        if (rdMemobj == NULL)
            return NULL;

        return rdMemobj->GetUnits();
    }

    int CRf6DbObject::setUnits(CRf6Database &db, const char *desc)
    {
        if ((GetDbType() != NANALOG_DB) && (GetDbType() != NMETER_DB))
            return 1;

        GetRdMemObject(db);
        if (rdMemobj == NULL)
            return 2;

        rdMemobj->SetUnits(desc);
        WriteMemObject();
        return 0;
    }



}


