#include "rf6eventsubscriber.h"

#include <eventmsg.h>

#include <string.h>
#include <mqueue.h>
#include <errno.h>

namespace Rf6
{
	// event queue parameters
	const char * EV_QUEUE_NAME = "/dev/mqueue/opcdbchevt";
	const int EV_QUEUE_MAXMSG = 10240;

	int CRf6EventSubscriber::subscribe(EventTypes mask)
	{
		event_msg_register_t rMsg;
		memset(&rMsg, 0, sizeof(rMsg));

		rMsg.hdr.io_hdr.type = _IO_MSG;
		rMsg.hdr.io_hdr.subtype = EVENTPROC_TYPE;
		rMsg.hdr.MsgType = EVENT_SUBTYPE_REGISTER;

		strncpy(rMsg.szEventQueue, EV_QUEUE_NAME, sizeof(rMsg.szEventQueue));
		rMsg.dwMask = mask;

		return MsgSend(m_nEventProcCoID, &rMsg, sizeof(rMsg), NULL, 0);
	}

	int CRf6EventSubscriber::unsubscribe()
	{
		rf_msg_hdr_t rMsg;
		memset(&rMsg, 0, sizeof(rMsg));

		rMsg.io_hdr.type = _IO_MSG;
		rMsg.io_hdr.subtype = EVENTPROC_TYPE;
		rMsg.MsgType = EVENT_SUBTYPE_UNREGISTER;

		return MsgSend(m_nEventProcCoID, &rMsg, sizeof(rMsg), NULL, 0);
	}

	mqd_t CRf6EventSubscriber::open_queue()
	{
		struct mq_attr attr;
		attr.mq_maxmsg = EV_QUEUE_MAXMSG;
		attr.mq_msgsize = MAX_EVENTSIZE;
		attr.mq_flags = 0;

		mq_unlink(EV_QUEUE_NAME);
		return mq_open(EV_QUEUE_NAME, O_RDONLY|O_CREAT, S_IRUSR|S_IRGRP|S_IROTH|S_IWUSR|S_IWGRP|S_IWOTH, &attr);
	}

	void CRf6EventSubscriber::workThread()
	{
		mqd_t queueId = open_queue();
		if (queueId == -1)
		{
			emit messageSend(CANNOT_OPEN_QUEUE);
			return;
		}

		int res = CEventProcInterface::Connect();
		if (res != RF_SUCCESS)
		{
			emit messageSend(CANNOT_CONNECT_EVPROC);
			return;
		}

		res = subscribe(ev_mask);	// default arguments (queueName, eventDatabase)
		if (res != RF_SUCCESS)
		{
			emit messageSend(SUBSCRIBE_ERROR);
			return;
		}

		struct mq_attr attr; mq_getattr(queueId, &attr);
		while (mRunning)
		{
			res = mq_receive(queueId, eventbuf, sizeof(eventbuf), NULL);
			if (res >= 0)
			{
				// message = event_header + payload
				EVENT_HEADER* ev_hdr = reinterpret_cast<EVENT_HEADER*>(eventbuf);
				switch (ev_hdr->byType)
				{
					case EVT_DBEDIT:
						eventDbProcessing();
						break;
					default:
						qDebug() << "Unsupported a event type" << ev_hdr->byType;
						emit messageSend(UNSUPPORTED_EVENT_TYPE);
						break;
				}
			}
			else
			{
				qDebug() << "CRf6DbUpdates. Error" << errno; //perror(NULL);
				emit messageSend(QUEUE_RECIEVE_ERROR);
			}
		}

		emit messageSend(DB_SUBSCRIBER_THREAD_EXITED);
		return;

	}

	void CRf6EventSubscriber::eventDbProcessing()
	{
		DEVENT* ev = reinterpret_cast<DEVENT*>(eventbuf);
		//qDebug() << "New event is received" << ev->hdr.ullIdx << ev->hdr.uiDataSize << ev->data.ullRecIndex;

		switch (ev->data.nDbType)
		{
			case NPCU_DB:
			case NANALOG_DB:
			case NMETER_DB:
			case NSTATUS_DB:
			case NTANK_DB:
				eventTagDbProcessing(ev);
				break;
			default:
				qDebug() << "Unsupported a database event type" << ev->data.nDbType;
				emit messageSend(UNSUPPORTED_EVENT_TYPE);
				break;
		}
	}

	void CRf6EventSubscriber::eventTagDbProcessing(DEVENT* ev)
	{
		DEVENT_DATA &evData = ev->data;

		ULONG_64 &unind = evData.ullRecIndex;
		unsigned char &operation = evData.byOperation;

		//qDebug() << unind << evData.nDbType << operation << evData.data.dbrec.szPcu << evData.data.dbrec.szTag;

		switch (operation)
		{
			case DBEDIT_TAG_ADD:
				tagAddProcessing(unind);
				break;
			case DBEDIT_TAG_UPD:
				tagUpdProcessing(unind);
				break;
			case DBEDIT_TAG_DEL:
				emit tagDeleted(unind);
				break;
			default:
				qDebug() << "Unsupported a operation for database event type" << ev->data.byOperation;
				emit messageSend(UNSUPPORTED_EVENT_TYPE);
				break;
		}
	}

	void CRf6EventSubscriber::tagAddProcessing(ULONG_64 unind)
	{
		QSharedPointer<CRf6DbObject>new_obj (new CRf6DbObject(unind));
		if (updateObject(new_obj) != 0)
			return;

		ULONG_64 parent;
		if (new_obj->GetDbType() == NPCU_DB)
		{
			parent = 0;
		}
		else
		{
			NR_REC_HDR hdr = new_obj->data.p.hdr;	// NR_REC_HDR is a common field for all records. p just to be specific!
			parent = rf6db.GetUnIndex(NPCU_DB, hdr.rtu_nbr);
			if (parent == 0)
			{
				emit messageSend(RF6_TAG_PARENT_ERROR);
				return;
			}
		}

		emit tagAdded(parent, new_obj);

	}

	void CRf6EventSubscriber::tagUpdProcessing(ULONG_64 unind)
	{
		QSharedPointer<CRf6DbObject>new_obj (new CRf6DbObject(unind));
		if (updateObject(new_obj) != 0)
			return;

		emit tagUpdated(new_obj);
	}

	int CRf6EventSubscriber::updateObject(QSharedPointer<CRf6DbObject> spObj)
	{
		CMemObject* pMemObj = spObj->GetRdMemObject(rf6db);
		if (pMemObj == NULL)
		{
			emit messageSend(RF6_DB_ERROR);
			return 1;
		}

		int res = spObj->ReadFromDbRec(pMemObj->DbRec);
		if (res != 0)
		{
			emit messageSend(RF6_DB_ERROR);
			return 2;
		}

		if (spObj->GetDbType() == NPCU_DB)
			spObj->name = pMemObj->GetPcuName();
		else
			spObj->name = pMemObj->GetTagName();

		return 0;
	}
}
