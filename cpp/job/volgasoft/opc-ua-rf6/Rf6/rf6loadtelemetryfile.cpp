#include "rf6loadtelemetryfile.h"
#include "rf6readchnfile.h"

#include "helper.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif




CRf6LoadTelemetryFile::CRf6LoadTelemetryFile()
{
}

CRf6LoadTelemetryFile::~CRf6LoadTelemetryFile()
{
}

int CRf6LoadTelemetryFile::PcuError()
{
    debugLog.logToFile("Pcu Read Error. Error in Reading the PCU Record");
    return 0;
}

int CRf6LoadTelemetryFile::LoadTextField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+node->Tele.Text.text_len) < 64)
        {
            memcpy(node->Tele.Text.cur_text, &PcuObj.DbRec.p->spare[node->offset], node->Tele.Text.text_len);
        }
        else
            return RF_FAILURE;
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        File.read((char *)node->Tele.Text.cur_text, node->Tele.Text.text_len);
    }
    memcpy(node->Tele.Text.prev_text, node->Tele.Text.cur_text,node->Tele.Text.text_len);
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadNumericField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if(node->data_type == TYPE_CHAR )
        {
            char	data_val;
            if((node->offset+sizeof(char) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(char));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val;
            if((node->offset+sizeof(wchar_t) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(wchar_t));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val;
            if((node->offset+sizeof(unsigned char) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned char));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val;
            if((node->offset+sizeof(unsigned short) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned short));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val;
            if((node->offset+sizeof(short) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(short));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val;
            if((node->offset+sizeof(int) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(int));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val;
            if((node->offset+sizeof(unsigned int) ) < 64)
                memcpy(&data_val,&PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned int));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val;
            if((node->offset+sizeof(unsigned long) ) < 64)
                memcpy(&data_val,&PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned long));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val;
            if((node->offset+sizeof(long) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(long));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val;
            if((node->offset+sizeof(float) ) < 64)
                memcpy(&data_val,&PcuObj.DbRec.p->spare[node->offset], sizeof(float));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val;
            if((node->offset+sizeof(double) ) < 64)
                memcpy(&data_val,&PcuObj.DbRec.p->spare[node->offset], sizeof(double));
            else
                return RF_FAILURE;
            node->Tele.Numeric.cur_val=(double)data_val;
        }
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        if(node->data_type == TYPE_CHAR )
        {
            char	data_val;
            File.read((char *)&data_val, sizeof(char));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val;
            File.read((char *)&data_val, sizeof(wchar_t));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val;
            File.read((char *)&data_val, sizeof(unsigned char));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val;
            File.read((char *)&data_val, sizeof(unsigned short));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val;
            File.read((char *)&data_val, sizeof(short));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val;
            File.read((char *)&data_val, sizeof(int));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val;
            File.read((char *)&data_val, sizeof(unsigned int));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val;
            File.read((char *)&data_val, sizeof(unsigned long));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val;
            File.read((char *)&data_val, sizeof(long));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val;
            File.read((char *)&data_val, sizeof(float));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val;
            File.read((char *)&data_val, sizeof(double));
            node->Tele.Numeric.cur_val=(double)data_val;
        }
    }
    node->Tele.Numeric.prev_val=node->Tele.Numeric.cur_val;
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadListField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );

    double list_value=0.0;
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if(node->data_type == TYPE_STRING )
        {
            if((node->offset+sizeof(char) ) < 64)
                memcpy(err_msg, &PcuObj.DbRec.p->spare[node->offset], node->Tele.List.item_len);
            else
                return RF_FAILURE;
        }
        else if(node->data_type == TYPE_CHAR )
        {
            char	data_val;
            if((node->offset+sizeof(char) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(char));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val;
            if((node->offset+sizeof(wchar_t) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(wchar_t));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val;
            if((node->offset+sizeof(unsigned char) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned char));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val;
            if((node->offset+sizeof(unsigned short) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned short));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val;
            if((node->offset+sizeof(short) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(short));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val;
            if((node->offset+sizeof(int) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(int));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val;
            if((node->offset+sizeof(unsigned int) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned int));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val;
            if((node->offset+sizeof(unsigned long) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(unsigned long));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val;
            if((node->offset+sizeof(long) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(long));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val;
            if((node->offset+sizeof(float) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(float));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val;
            if((node->offset+sizeof(double) ) < 64)
                memcpy(&data_val, &PcuObj.DbRec.p->spare[node->offset], sizeof(double));
            else
                return RF_FAILURE;
            list_value=(double)data_val;
        }
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        if(node->data_type == TYPE_STRING )
        {
            File.read((char *)err_msg, node->Tele.List.item_len);
        }
        else if(node->data_type == TYPE_CHAR )
        {
            char	data_val;
            File.read((char *)&data_val, sizeof(char));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val;
            File.read((char *)&data_val, sizeof(wchar_t));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val;
            File.read((char *)&data_val, sizeof(unsigned char));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val;
            File.read((char *)&data_val, sizeof(unsigned short));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val;
            File.read((char *)&data_val, sizeof(short));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val;
            File.read((char *)&data_val, sizeof(int));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val;
            File.read((char *)&data_val, sizeof(unsigned int));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val;
            File.read((char *)&data_val, sizeof(unsigned long));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val;
            File.read((char *)&data_val, sizeof(long));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val;
            File.read((char *)&data_val, sizeof(float));
            list_value=(double)data_val;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val;
            File.read((char *)&data_val, sizeof(double));
            list_value=(double)data_val;
        }
    }

    double val=0.0;
    for(int i=0; i<node->Tele.List.item_count; i++)
    {
        if(node->data_type != TYPE_STRING)
        {
            val = atof(node->Tele.List.value_items[ i ]);
            if(val == list_value)
            {
                node->Tele.List.cur_sel = i+1;
                node->Tele.List.prev_sel = i+1;
                return RF_SUCCESS;
            }
        }
        else
        {
            if(!strcmp(err_msg, node->Tele.List.value_items[ i ] ) )
            {
                node->Tele.List.cur_sel = i+1;
                node->Tele.List.prev_sel = i+1;
                return RF_SUCCESS;
            }
        }
    }
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadCheckBoxField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+sizeof(char)) < 64)
        {
            memcpy(&node->Tele.CheckBox.cur_state, &PcuObj.DbRec.p->spare[node->offset], sizeof(char) );
            memcpy(&node->Tele.CheckBox.prev_state, &PcuObj.DbRec.p->spare[node->offset], sizeof(char) );
        }
        else
            return RF_FAILURE;
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        File.read((char *)&node->Tele.CheckBox.cur_state, sizeof(char));
        memcpy(&node->Tele.CheckBox.prev_state, &node->Tele.CheckBox.cur_state, sizeof(char) );
    }
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadTimeField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if((node->offset+sizeof(char)) < 64)
        {
            memcpy(err_msg, &PcuObj.DbRec.p->spare[node->offset], 8 );
        }
        else
            return RF_FAILURE;
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        File.read(err_msg, 8);
    }
    CRf6Telemetry::parseTime(err_msg, &node->Tele.Time);
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadDateField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+sizeof(char)) < 64)
        {
            memcpy(err_msg, &PcuObj.DbRec.p->spare[node->offset], 10 );
        }
        else
            return RF_FAILURE;
    }
    else
    {
        if( !File )
            return RF_FAILURE;

        File.read(err_msg, 10);
    }
    CRf6Telemetry::parseDate(err_msg, &node->Tele.Date);
    return RF_SUCCESS;
}

int CRf6LoadTelemetryFile::LoadTeleFromFile(char *tel_file_name, IArray< TelWidgetPtr_t > *mCfgArray)
{
    File.clear();
#if (_RF_VERSION < _RF_QNX641)
    File.open( tel_file_name, ios::in | ios::nocreate );
#else
    File.open( tel_file_name, ios::in );
#endif
    if( !File )
    {
        return Pt_CONTINUE;
    }

    memset(&tel_hdr, 0x00, sizeof(TEL_HDR) );
    File.read((char *)&tel_hdr, sizeof(TEL_HDR) );

    TelWidgetPtr_t *node=NULL;
    int iRet=0;
    for(int i=0; i<mCfgArray->NumberOfItems(); i++)
    {
        node = mCfgArray->GetItem(i);
        if(node == NULL)
            continue;

        if(node->widget_type == WIDGET_TYPE_TEXT)
        {
            iRet = LoadTextField(node);

        }
        else if(node->widget_type == WIDGET_TYPE_NUMERIC)
        {
            iRet = LoadNumericField(node);

        }
        else if(node->widget_type == WIDGET_TYPE_LIST)
        {
            iRet = LoadListField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_CHECK)
        {
            iRet = LoadCheckBoxField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_DATE)
        {
            iRet = LoadDateField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_TIME)
        {
            iRet = LoadTimeField(node);
        }
        else {
            continue;
        }

        if(iRet != RF_SUCCESS)
        {
            debugLog.logToFile("Error in Reading %d field\n", i);
            return( Pt_CONTINUE );
        }
    }

    File.close();
    return Pt_CONTINUE;
}

int CRf6LoadTelemetryFile::loadTelemetryForChanel(int chanel, IArray<TelWidgetPtr_t> *CfgArray)
{

    CRf6ReadChnFile readerChn;
    QString nameDriver;
    readerChn.readDriverChannelFile(chanel,nameDriver);
    QString fullPath = QString(DataDir) + "/tel/"+ nameDriver +"."+QString::number(chanel);
    return LoadTeleFromFile(fullPath.toAscii().data() ,CfgArray);
}

int CRf6LoadTelemetryFile::loadTelemetryForPcu(QString pcuName, IArray<TelWidgetPtr_t> *CfgArray)
{
    QString fullPath = QString(DataDir) + "/tel/"+ pcuName +".tel";
    return LoadTeleFromFile(fullPath.toAscii().data() ,CfgArray);
}

/*
int CRf6LoadTelemetryFile::LoadTelemetry()
{
    LoadTeleFromFile(channel_tel_path, DriverCfgList);
    LoadTeleFromFile(pcu_tel_path, PcuCfgList);

    return 0;//( Pt_CONTINUE );
}*/
