#include "rf6telemetryaccess.h"
#include "rf6readchnfile.h"


Rf6TelemetryAccess::Rf6TelemetryAccess()
{
}

void Rf6TelemetryAccess::requestChanels()
{
    //read driver.chn
}

ArrayTeleWidget *Rf6TelemetryAccess::requestPcuTelemetry(int numberChanel)
{
    CRf6ReadChnFile readFileChn;
    QString nameDriver;
    if (RF_SUCCESS == readFileChn.readDriverChannelFile(numberChanel,nameDriver)) {
        //request telemetry
        CRf6ReadCfgFile readFileCfg;

        return readFileCfg.ReadPcuConfigFile(nameDriver);

    }
     return  NULL;

}

ArrayTeleWidget *Rf6TelemetryAccess::requestChanel(int numberChanel)
{
    CRf6ReadChnFile readFileChn;
    QString nameDriver;
    if (RF_SUCCESS == readFileChn.readDriverChannelFile(numberChanel,nameDriver)) {
        //request telemetry
        CRf6ReadCfgFile readFileCfg;
        return readFileCfg.ReadDriverConfigFile(nameDriver);

    }
    return  NULL;
}

