#include "rf6rtsubscriber.h"

namespace Rf6
{
	void CRf6RtSubscriber::workThread()
	{
		int res;	// functions result
		int channel;
		res = updater.UpdateConnect(channel);
		if (res == RF_FAILURE)
		{
			emit messageSend(CANNOT_CONNECT_RUPDATE);
			return;
		}

		// a creation of a thread (sender)
		rtEmpty = true;
		CRtSender sender(this);	// object for client communication
		sender.start();			// run new thread

		while (mRunning)
		{
			update_msg_t update;

			int rcvId = MsgReceive(channel, &update, sizeof(update_msg_t), NULL);

			if (rcvId > 0)
			{
				MsgReply(rcvId, 0, &update, sizeof(update_msg_t));
				//qDebug() << "__CRf6RtSubscriber::workThread(). Realtime is received!" << update.hdr.pntID.dbIndex << update.hdr.pntID.dbType;
				rtUpdateProcessing(update);
			}
			else if (rcvId == 0)
			{
			}
			else
			{
				emit messageSend(MSG_RECEIVE_ERROR);
			}
		}

		sender.stop();
		sender.wait();

		emit messageSend(SUBSCRIBER_THREAD_EXITED);
		return;

	}

	void CRf6RtSubscriber::rtUpdateProcessing(const update_msg_t& update)
	{
		unsigned int idx = update.hdr.pntID.dbIndex;
		int dbtype = update.hdr.pntID.dbType;
		int res;

		//emit rtUpdate(idx);

		CMemObject* pMemObj = rf6db.GetMemObject(dbtype);
		if (pMemObj == NULL)
		{
			emit messageSend(RF6_DB_ERROR);
			return;
		}

		res = pMemObj->Read(idx);
		if (res != RF_SUCCESS)
		{
			emit messageSend(RF6_DB_ERROR);
			return;
		}

		ULONG_64 unidx = pMemObj->GetUniqueIndex();
		QSharedPointer<CRf6DbObject>spOb (new CRf6DbObject(unidx));	// creation of object

		res = spOb->ReadFromDbRec(pMemObj->DbRec);
		if (res != 0)
		{
			emit messageSend(RF6_DB_ERROR);
			return;
		}

		//emit rtUpdate(spOb);
		mutex_rt.lock();
		realtimes.append(spOb);
		rtEmpty = false;
		wc_rtNotEmpty.wakeAll();
		mutex_rt.unlock();
	}

	//void CRf6RtSubscriber::stop(void)
	//{
	//	mRunning = false;
	//}

	int CRf6RtSubscriber::subscribe(ULONG_64 uind, bool subscribe/*=true*/)
	{
		CMemObject* pMemObj = rf6db.GetMemObject(GetDbType(uind));
		if (pMemObj == NULL)
		{
			emit messageSend(RF6_DB_ERROR);
			return 1;
		}

		int bDel;
		unsigned int ind = pMemObj->SearchByUniqueIndex(uind, bDel);	// 32-bit index
		if (ind == 0)
		{
			emit messageSend(RF6_DB_ERROR);
			return 2;
		}

		int res;
		if (subscribe)
			res = updater.UpdateAddPoint(pMemObj, ind);
		else
			res = updater.UpdateRemovePoint(GetDbType(uind), ind);
		if (res != RF_SUCCESS)
		{
			emit messageSend(RUPDATE_ERROR);
			return 3;
		}

		if (updater.UpdateInit() != RF_SUCCESS)
		{
			emit messageSend(RUPDATE_ERROR);
			return 4;
		}

		return 0;
	}


	void CRtSender::workThread()
	{
		while (mRunning)
		{
			subscriber->mutex_rt.lock();

			if (subscriber->rtEmpty)
				subscriber->wc_rtNotEmpty.wait(&(subscriber->mutex_rt));

			realtimes.append(subscriber->realtimes);	// move updates into a local list
			subscriber->realtimes.clear();
			subscriber->rtEmpty = true;

			subscriber->mutex_rt.unlock();

			rtUpdateProcessing();
		}
	}

	void CRtSender::rtUpdateProcessing()
	{
		foreach (QSharedPointer<CRf6DbObject> pRtUp, realtimes)
		{
			subscriber->send_update(pRtUp);
		}
		realtimes.clear();
	}
}
