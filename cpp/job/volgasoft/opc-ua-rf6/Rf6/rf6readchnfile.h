#ifndef RF6READCHNFILE_H
#define RF6READCHNFILE_H

#include "rf6telemetry.h"
#include <QString>

class CRf6ReadChnFile
{
public:
    CRf6ReadChnFile();
    ~CRf6ReadChnFile();
    char *getScanerName();
    int  GetNextLine(char	*buff1);

    int readDriverChannelFile();
    int readDriverChannelFile(int nChannel,QString &nameDriver);
private:
    std::ifstream  chnFile;
    char chnPath[PATH_MAX];
    char scanner_name[128];
    int nChannelNo;
};

#endif // RF6READCHNFILE_H
