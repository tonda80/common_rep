#ifndef RF6RTSUBSCRIBER_H
#define RF6RTSUBSCRIBER_H

#include <QObject>
#include <QMetaType>
#include <QList>

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include "rf6defines.h"
#include "rf6database.h"
#include "rf6dbobject.h"

#include <rupdate.hpp>

namespace Rf6
{
	class CRtSender;

	// the subscriber thread. It communicates with rupdate
	class CRf6RtSubscriber : public QThread
	{
		Q_OBJECT
	public:
		friend class CRtSender;

		enum Rf6RtSubscriberMsg
		{
			CANNOT_CONNECT_RUPDATE,
			SUBSCRIBER_THREAD_EXITED,
			MSG_RECEIVE_ERROR,
			RF6_DB_ERROR,
			RUPDATE_ERROR,
		};

		CRf6RtSubscriber(): mRunning(true) {qRegisterMetaType<QSharedPointer<CRf6DbObject> >("QSharedPointer<CRf6DbObject>");};

		void stop(void) {mRunning = false;}

		int subscribe(ULONG_64 uind, bool subscribe=true);	// if subscribe - false, then unsubscriptioning
		//int unsubscribe(ULONG_64 uind);

	signals:
		void messageSend(int code);
		void rtUpdate(QSharedPointer<CRf6DbObject>);

	protected:
		CRf6Database rf6db;	// object for rf6 database communication
		CUpdate updater;	// object for rupdate communication
		bool mRunning;

		// a link with CRtSender thread
		QList<QSharedPointer<CRf6DbObject> > realtimes;	// list of realtimes
		QMutex mutex_rt;		// mutex and condvar for protection of realtimes
		QWaitCondition wc_rtNotEmpty;
		bool rtEmpty;			// flag - buffer empty
		void send_update(QSharedPointer<CRf6DbObject> spOb) {emit rtUpdate(spOb);}

		virtual void run() {workThread();}
		void workThread();										// thread which do a useful work
		void rtUpdateProcessing(const update_msg_t& update);	// handling of the new update
	};

	// the auxiliary thread of the sending of realtime to client
	class CRtSender : public QThread
	{
		Q_OBJECT
	public:
		CRtSender(CRf6RtSubscriber* p): mRunning(true), subscriber(p) {}
		void stop(void) {mRunning = false;}
	protected:
		bool mRunning;

		CRf6RtSubscriber* subscriber;	// parent-subscriber
		QList<QSharedPointer<CRf6DbObject> > realtimes;	// list of realtimes

		virtual void run() {workThread();}
		void workThread();										// thread which do a useful work
		void rtUpdateProcessing();						// handling of the update list
	};

}

#endif
