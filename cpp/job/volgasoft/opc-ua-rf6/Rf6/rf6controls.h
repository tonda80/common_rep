#ifndef RF6CONTROLS_H
#define RF6CONTROLS_H

#include <QObject>

#include <msgtype.h>
#include <vtype.h>

#include "rf6database.h"

namespace Rf6
{

	class CRf6ControlsManager : public QObject
	{
		Q_OBJECT
    public:
		enum Rf6ControlsManagerMsg
		{
			CONTROL_SEND_OK,
			RF6_DB_ERROR,
			SEND_CONTROL_ERROR,
            CONTROL_NOT_FOR_PCU
		};

	signals:
		void sendControlComplete(int result, ULONG_64 uind, UCHAR ctltype);

	public:
		int sendControl(ULONG_64 uind, UCHAR ctltype, cntl_val_t *Data);
		int sendWarmStart(ULONG_64 uind);
		int sendDemandScan(ULONG_64 uind);

	private:
		CRf6Database rf6db;

	};


}

#endif
