
# RealFlex dependencies
# --------------------------
qnx {
        DEFINES += RF6_COMPILED
        HEADERS += $$PWD/rf6database.h \
                    $$PWD/rf6defines.h \
                    $$PWD/rf6dbobject.h \
                    $$PWD/rf6rtsubscriber.h \
                    $$PWD/rf6eventsubscriber.h \
                    $$PWD/rf6projectinfo.h \
                    $$PWD/rf6loadtelemetryfile.h \
                    $$PWD/rf6telemetry.h \
                    $$PWD/rf6readcfgfile.h \
                    $$PWD/rf6readchnfile.h \
                    $$PWD/rf6controls.h 

        SOURCES += $$PWD/rf6database.cpp \
                    $$PWD/rf6dbobject.cpp \
                    $$PWD/rf6rtsubscriber.cpp \
                    $$PWD/rf6eventsubscriber.cpp \
                    $$PWD/rf6projectinfo.cpp \
                    $$PWD/rf6loadtelemetryfile.cpp \
                    $$PWD/rf6telemetry.cpp \
                    $$PWD/rf6readcfgfile.cpp \
                    $$PWD/rf6readchnfile.cpp \
                    $$PWD/rf6controls.cpp

        # --------------------------
        RF6_INSTALL_PATH = /opt/rf6
        RF6_SOURCES_PATH = /rf6
        RF6_LIB_PATH = $${RF6_INSTALL_PATH}/lib
        RF6_INCLUDE_PATH = $${RF6_SOURCES_PATH}/include
        RF6_SCANKIT_INCLUDE_PATH = $${RF6_SOURCES_PATH}/rfscan/scankit/include



        #include path
        INCLUDEPATH += $${RF6_INCLUDE_PATH}

        # include scankit path
        INCLUDEPATH += $${RF6_SCANKIT_INCLUDE_PATH}

        # include path (flex)
        #INCLUDEPATH += $${RF6_SOURCES_PATH}/flex/inc

        # library path
        LIBS += -L$${RF6_LIB_PATH}

        LIBS += -lrf -lrftrans -lrf6key -lrsa

        QMAKE_LFLAGS += -Wl,-rpath,$${RF6_LIB_PATH}

}
win32 {
        HEADERS += \
            Rf6/win32/dbftype.h \
            Rf6/win32/dbconst.h \
            Rf6/win32/vtype.h \
            Rf6/win32/_pack1.h \
            Rf6/win32/_packpop.h

        INCLUDEPATH += Rf6/win32
}

HEADERS += \
    $$PWD/rf6telemetryaccess.h \
    $$PWD/rf6uploadtelemetryfile.h

SOURCES += \
    $$PWD/rf6telemetryaccess.cpp \
    $$PWD/rf6uploadtelemetryfile.cpp

