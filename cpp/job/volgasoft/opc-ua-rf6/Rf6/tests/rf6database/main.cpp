#include <QDebug>
#include "rf6database.h"
#include <QTextStream>
/*
This program demonstrates (and tests its correct work) class CRf6Database
*/

const char PCU_NAME[] = "NEWPCU";
const char ANA_TAG[] = "ANA_TAG";
const char STA_TAG[] = "STA_TAG";
const char MET_TAG[] = "MET_TAG";
const char TNK_TAG[] = "TNK_TAG";

const double ANA_INIT_VALUE = 12.34;
const char STA_INIT_VALUE = 1;

int main()
{
    Rf6::CRf6Database rf6db;
	QTextStream stream_in(stdin);
	//QTextStream stream_out(stdout);
	
	qDebug() << "Create new pcu " << PCU_NAME;
	rf6db.CreatePcu(PCU_NAME);
	
	qDebug() << "Create new tags: " << ANA_TAG << STA_TAG << MET_TAG << TNK_TAG;
	rf6db.CreateTag(NANALOG_DB, PCU_NAME, ANA_TAG);
	rf6db.CreateTag(NSTATUS_DB, PCU_NAME, STA_TAG);
	rf6db.CreateTag(NMETER_DB, PCU_NAME, MET_TAG);
	rf6db.CreateTag(NTANK_DB, PCU_NAME, TNK_TAG);
	
	qDebug() << "Write values to analog and status tags";
	Rf6::SimpleDbObject dbo;
	
	qDebug() << "Enter a value of analog tag";
	stream_in >> dbo.AnaValue;
	rf6db.UpdateDbObject(NANALOG_DB, PCU_NAME, ANA_TAG, dbo);
	
	qDebug() << "Enter a value of status tag";
	int v;
	stream_in >> v;
	dbo.StaValue = v & 0x3;
	rf6db.UpdateDbObject(NSTATUS_DB, PCU_NAME, STA_TAG, dbo);
	
	qDebug() << "Read values from analog and status tags";
	int res = rf6db.ReadDbObject(NANALOG_DB, PCU_NAME, ANA_TAG, dbo);
	if (res == 0)
		qDebug() << "Value of analog tag is " << dbo.AnaValue;
	else
		qDebug() << "Cannot read the analog value. Error " << res;
	
	res = rf6db.ReadDbObject(NSTATUS_DB, PCU_NAME, STA_TAG, dbo);
	if (res == 0)
		qDebug() << "Value of status tag is " << static_cast<int>(dbo.StaValue);
	else
		qDebug() << "Cannot read the analog value. Error " << res;
		
	qDebug() << "Test a result and press Enter. Thereafter the analog tag of the new Pcu will be remoted";
	stream_in.readLine();	// ?!
	stream_in.readLine();
	rf6db.DeleteTag(NANALOG_DB, PCU_NAME, ANA_TAG);
	
	qDebug() << "Test a result and press Enter. Thereafter the new Pcu will be remoted";
	stream_in.readLine();
	rf6db.DeletePcu(PCU_NAME);
	
	return 0;
}
