TEMPLATE = app
CONFIG += console qt
QT -= gui

DEPENDPATH += . ../..
INCLUDEPATH += . ../..

SOURCES += main.cpp

include(../../rf6.pri)

#HEADERS += rf6database.h rf6defines.h
#SOURCES += rf6database.cpp

QMAKE_CXXFLAGS += -g
