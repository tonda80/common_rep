TEMPLATE = app
CONFIG += console qt
QT -= gui
	
DEPENDPATH += . ../..
INCLUDEPATH += . ../..

SOURCES += main-rfdb-ai.cpp
HEADERS += main-rfdb-ai.h

include(../../rf6.pri)

#HEADERS += rf6database.h rf6defines.h
#SOURCES += rf6database.cpp

QMAKE_CXXFLAGS += -g
