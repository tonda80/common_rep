#include "main-rfdb-ai.h"

/*
This program demonstrates (and tests its correct work) a asynchronous interface of class CRf6Database
*/

QTextStream stream_in(stdin);
//QTextStream stream_out(stdout);


void CTestAIRfDb::StartTest()
{
	rf6db.GetObjects();

	TestCreate();
	TestUpdate();
	TestRead();
	TestDelete();
}

void CTestAIRfDb::TestCreate()
{
	qDebug() << "Create new pcu:" << PCU_NAME;

	CRf6DbObject pcu_ob(NPCU_DB, PCU_NAME);
	rf6db.Create(0, pcu_ob);

	qDebug() << "Create new tag:" << ANA_TAG;
	CRf6DbObject tag_ob_a(ANALOG_DB, ANA_TAG);
	rf6db.Create(pcu_uind, tag_ob_a);

	qDebug() << "Create new tag:" << STA_TAG;
	CRf6DbObject tag_ob_s(STATUS_DB, STA_TAG);
	rf6db.Create(pcu_uind, tag_ob_s);

	qDebug() << "Create new tag:" << MET_TAG;
	CRf6DbObject tag_ob_m(METER_DB, MET_TAG);
	rf6db.Create(pcu_uind, tag_ob_m);

	qDebug() << "Create new tag:" << TNK_TAG;
	CRf6DbObject tag_ob_t(TANK_DB, TNK_TAG);
	rf6db.Create(pcu_uind, tag_ob_t);
}

void CTestAIRfDb::CreateCompleted(int errcode, ULONG_64 parent, CRf6DbObject& ob)
{
	if (errcode==CRf6Database::NOERROR || errcode==CRf6Database::OBJECTISEXISTED)
	{
		QString title;
		if (errcode==CRf6Database::OBJECTISEXISTED)
			title = "Object already exists:";
		else
			title = "Object was created:";
		qDebug() << title << ob.GetName() << ob.GetIndex() << ob.GetDbType() << GetDbType(parent);

		if (ob.GetDbType()==NPCU_DB)	// pcu was created
			pcu_uind = ob.GetIndex();
		else if (ob.GetDbType()==NANALOG_DB)
			ana_uind = ob.GetIndex();
		if (ob.GetDbType()==NSTATUS_DB)
			sta_uind = ob.GetIndex();
	}
	else
	{
		qDebug() << "Error of the object creation:" << errcode << parent << ob.GetName();
	}
}

void CTestAIRfDb::TestUpdate()
{
	qDebug() << "Writing values to analog and status tags.";

	qDebug() << "> Enter a value of analog tag";
	CRf6DbObject ob_a(ana_uind);
	if (ob_a.PtrAnaData()==NULL){
		qDebug() << "Error of database"; return;
	}
	stream_in >> ob_a.PtrAnaData()->eu_value;
	ob_a.PtrAnaData()->hdr.rtu_point = 1;
	ob_a.PtrAnaData()->hdr.aux_addr = 2;
	ob_a.PtrAnaData()->hdr.sub_type = 3;
	rf6db.Update(ob_a);

	qDebug() << "> Enter a value of status tag";
	CRf6DbObject ob_s(sta_uind);
	int v;
	stream_in >> v;
	if (ob_s.PtrStaData()==NULL){
		qDebug() << "Error of database"; return;
	}
	ob_s.PtrStaData()->cur_val = v & 0x3;
	rf6db.Update(ob_s);
}

void CTestAIRfDb::UpdateComplete(int errcode, ULONG_64 uind)
{
	if (errcode == CRf6Database::NOERROR)
	{
		qDebug() << "Object was updated:" << rf6db.GetObjName(uind) << uind << GetDbType(uind);
	}
	else
	{
		qDebug() << "Error of the object update:" << errcode << uind << rf6db.GetObjName(uind) << GetDbType(uind);
	}
}

void CTestAIRfDb::TestRead()
{
	qDebug() << "Reading values from analog and status tags.";
	rf6db.Read(ana_uind);
	rf6db.Read(sta_uind);
}

void CTestAIRfDb::ReadComplete(int errcode, QSharedPointer<CRf6DbObject> spOb)
{
	if (errcode == CRf6Database::NOERROR)
	{
		if (spOb->GetDbType() == NANALOG_DB)
			qDebug() << "Value of analog tag is:" << spOb->PtrAnaData()->eu_value;
		if (spOb->GetDbType() == NSTATUS_DB)
			qDebug() << "Value of status tag is:" <<  spOb->PtrStaData()->cur_val;
	}
	else
	{
		qDebug() << "Error of the object read:" << errcode << spOb->GetIndex() << spOb->GetName() << spOb->GetDbType();
	}
}

void CTestAIRfDb::TestDelete()
{
	qDebug() << "> Test a result and press Enter. Thereafter the analog tag of the new Pcu will be remoted.";
	stream_in.readLine();	// ?!
	stream_in.readLine();
	rf6db.Delete(ana_uind);

	qDebug() << "> Test a result and press Enter. Thereafter the new Pcu will be remoted.";
	stream_in.readLine();
	rf6db.Delete(pcu_uind);

}

void CTestAIRfDb::DeleteComplete(int errcode, ULONG_64 uind)
{
	if (errcode == CRf6Database::NOERROR)
	{
		qDebug() << "Object was deleted:" << rf6db.GetObjName(uind) << GetDbType(uind) << uind;
	}
	else
	{
		qDebug() << "Error of the object delete:" << errcode << rf6db.GetObjName(uind) << GetDbType(uind) << uind;
	}
}

int main()
{
	CRf6Database rf6db;
	
	CTestAIRfDb oTest(rf6db);
	QObject::connect(&rf6db, SIGNAL(CreateComplete(int, ULONG_64, CRf6DbObject&)),
					&oTest, SLOT(CreateCompleted(int, ULONG_64, CRf6DbObject&)));

	QObject::connect(&rf6db, SIGNAL(ReadComplete(int, QSharedPointer<CRf6DbObject>)),
						&oTest, SLOT(ReadComplete(int, QSharedPointer<CRf6DbObject>)));

	QObject::connect(&rf6db, SIGNAL(UpdateComplete(int, ULONG_64)),
						&oTest, SLOT(UpdateComplete(int, ULONG_64)));

	QObject::connect(&rf6db, SIGNAL(DeleteComplete(int, ULONG_64)),
						&oTest, SLOT(DeleteComplete(int, ULONG_64)));

	oTest.StartTest();

	return 0;
}
