#ifndef MAINRF6DBAI_H
#define MAINRF6DBAI_H

#include <QDebug>
#include <QObject>
#include "rf6database.h"
#include <QTextStream>

const char PCU_NAME[] = "NEWPCU";
const char ANA_TAG[] = "ANA_TAG";
const char STA_TAG[] = "STA_TAG";
const char MET_TAG[] = "MET_TAG";
const char TNK_TAG[] = "TNK_TAG";

const double ANA_INIT_VALUE = 12.34;
const char STA_INIT_VALUE = 1;

using namespace Rf6;

class CTestAIRfDb : public QObject
{
Q_OBJECT

public:
	ULONG_64 pcu_uind, ana_uind, sta_uind, met_uind, tnk_uind;
	CRf6Database& rf6db;
	CTestAIRfDb(CRf6Database& db) : pcu_uind(0), ana_uind(0), sta_uind(0), met_uind(0), tnk_uind(0), rf6db(db) {}
	void StartTest();

	void TestCreate();
	void TestUpdate();
	void TestRead();
	void TestDelete();

	void TestListAll();

public slots:
	void CreateCompleted(int errcode, ULONG_64 parent, CRf6DbObject& ob);
	void ReadComplete(int errcode, QSharedPointer<CRf6DbObject> spOb);
	void UpdateComplete(int errcode, ULONG_64 uind);
	void DeleteComplete(int errcode, ULONG_64 uind);

};

#endif
