#ifndef MAINRF6DBAI_H
#define MAINRF6DBAI_H

#include <rf6rtsubscriber.h>
#include <rf6eventsubscriber.h>

#include <QObject>
#include <QTextStream>

using namespace Rf6;

class CTestRf6RtSubscriber : public QObject
{
 Q_OBJECT

public:
	CTestRf6RtSubscriber(CRf6RtSubscriber& s) : m_subscriber(s) {}

	void StartTest();

public slots:
	void GiveObject(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob);
	void messageSend(int code);
	void rtUpdate(QSharedPointer<CRf6DbObject>);

	void messageSend_dbu(int code);
	void tagAdded(const ULONG_64& parent, QSharedPointer<CRf6DbObject> ob);
	void tagDeleted(const ULONG_64& ind);
	void tagUpdated(QSharedPointer<CRf6DbObject> ob);

private:
	CRf6RtSubscriber& m_subscriber;
};

#endif
