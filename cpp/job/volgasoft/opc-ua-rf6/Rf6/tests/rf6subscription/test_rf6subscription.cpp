#include "test_rf6subscription.h"
#include <unistd.h>
#include <QCoreApplication>

/*
This program demonstrates (and tests its correct work) a class CRf6RtSubscriber
*/
using namespace Rf6;

QTextStream stream_in(stdin);
//QTextStream stream_out(stdout);

CRf6RtSubscriber subscriber;
CRf6Database rf6db;

CRf6EventSubscriber dbupdates(eventDatabase);

int main(int argc, char** argv)
{
	QCoreApplication app(argc, argv);
	CTestRf6RtSubscriber subscriber_test(subscriber);

	QObject::connect(&rf6db, SIGNAL(GiveObject(ULONG_64, QSharedPointer<CRf6DbObject>)),
					&subscriber_test, SLOT(GiveObject(ULONG_64, QSharedPointer<CRf6DbObject>)));
	rf6db.GetObjects();
	
	QObject::connect(&subscriber, SIGNAL(messageSend(int)),
					&subscriber_test, SLOT(messageSend(int)));

	QObject::connect(&subscriber, SIGNAL(rtUpdate(QSharedPointer<CRf6DbObject>)),
					&subscriber_test, SLOT(rtUpdate(QSharedPointer<CRf6DbObject>)));

	// --------------------------------------

	QObject::connect(&dbupdates, SIGNAL(messageSend(int)),
						&subscriber_test, SLOT(messageSend_dbu(int)));

	QObject::connect(&dbupdates, SIGNAL(tagAdded(const ULONG_64&, QSharedPointer<CRf6DbObject>)),
						&subscriber_test, SLOT(tagAdded(const ULONG_64&, QSharedPointer<CRf6DbObject>)));

	QObject::connect(&dbupdates, SIGNAL(tagUpdated(QSharedPointer<CRf6DbObject>)),
						&subscriber_test, SLOT(tagUpdated(QSharedPointer<CRf6DbObject>)));

	QObject::connect(&dbupdates, SIGNAL(tagDeleted(const ULONG_64&)),
						&subscriber_test, SLOT(tagDeleted(const ULONG_64&)));



	subscriber.start(QThread::HighPriority);

	dbupdates.start();

	return app.exec();
}

void CTestRf6RtSubscriber::GiveObject(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob)
{
	m_subscriber.subscribe(ob->GetIndex());
	qDebug() << "Subscribed at" << ob->GetName() << ob->GetIndex() << parent;
}

void CTestRf6RtSubscriber::messageSend(int code)
{
	qDebug() << "Received message from subscriber" << code;
}

void CTestRf6RtSubscriber::rtUpdate(QSharedPointer<CRf6DbObject> ob)
{
	int dbtype = ob->GetDbType();
	qDebug() << "rt update" << dbtype << ob->GetIndex() << rf6db.GetObjName(ob->GetIndex());
	switch (dbtype)
	{
	case NANALOG_DB:
		if (ob->PtrAnaData() == NULL) {qDebug() << "Error!";break;}
		qDebug() << "   analog" << ob->PtrAnaData()->eu_value;
		break;
	case NSTATUS_DB:
		if (ob->PtrStaData() == NULL) {qDebug() << "Error!";break;}
		qDebug() << "   status" << ob->PtrStaData()->cur_val;
		break;
	case NMETER_DB:
		if (ob->PtrMetData() == NULL) {qDebug() << "Error!";break;}
		qDebug() << "   meter" << ob->PtrMetData()->curr_value;
		break;
	case NTANK_DB:
		if (ob->PtrTnkData() == NULL) {qDebug() << "Error!";break;}
		qDebug() << "   tank" << ob->PtrTnkData()->avail_vol;
		break;
	case NPCU_DB:
		if (ob->PtrPcuData() == NULL) {qDebug() << "Error!";break;}
		qDebug() << "   pcu" << ob->PtrPcuData()->hdr.rtu_nbr;
		break;
	default:
		qDebug() << "Error dbtype";
		break;
	}
}

void CTestRf6RtSubscriber::messageSend_dbu(int code)
{
		qDebug() << "Db update sent" << code;
}

void CTestRf6RtSubscriber::tagAdded(const ULONG_64& parent, QSharedPointer<CRf6DbObject> ob)
{
	qDebug() << "Tag was added" << parent << ob->GetIndex() << ob->GetDbType() << ob->GetName();
}
void CTestRf6RtSubscriber::tagDeleted(const ULONG_64& ind)
{
	qDebug() << "Tag was deleted" << ind << GetDbType(ind) << rf6db.GetObjName(ind);
}
void CTestRf6RtSubscriber::tagUpdated(QSharedPointer<CRf6DbObject> ob)
{
	qDebug() << "Tag was updated" << ob->GetIndex() << ob->GetDbType() << ob->GetName();
}

