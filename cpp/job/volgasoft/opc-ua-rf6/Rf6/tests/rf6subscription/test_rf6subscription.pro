TEMPLATE = app
CONFIG += console qt
QT -= gui
	
DEPENDPATH += . ../..
INCLUDEPATH += . ../..

SOURCES += test_rf6subscription.cpp
HEADERS += test_rf6subscription.h

include(../../rf6.pri)

QMAKE_CXXFLAGS += -g
