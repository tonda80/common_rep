TEMPLATE = app
CONFIG += console qt
QT -= gui
	
DEPENDPATH += . ../..
INCLUDEPATH += . ../..

SOURCES += main-rfdb-t2.cpp
HEADERS += main-rfdb-t2.h

include(../../rf6.pri)

QMAKE_CXXFLAGS += -g
