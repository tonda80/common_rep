#ifndef MAINRF6DBAI_H
#define MAINRF6DBAI_H

#include <QDebug>
#include <QList>
#include <QObject>
#include "rf6database.h"
#include <QTextStream>
using namespace Rf6;

class CTestRfDb : public QObject
{
Q_OBJECT

public:
	ULONG_64 pcu_uind, ana_uind, sta_uind, met_uind, tnk_uind;
	CRf6Database& rf6db;
	CTestRfDb(CRf6Database& db) : rf6db(db) {}

	void TestListAll();

	void TestAnalogLimits(CRf6DbObject&);
	void TestStatusState(CRf6DbObject&);
	void TestWriteField(CRf6DbObject&);

public slots:
	void GiveObject(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob);	// give another object
	void ObjectsGiven(int cnt_error_getting);							// all objects were given

	void sendControlComplete(int result, ULONG_64 uind, UCHAR ctltype);	//	CRf6ControlsManager

public:
	//QList<QSharedPointer<CRf6DbObject> > AnaTags;
	//QList<QSharedPointer<CRf6DbObject> > StaTags;

};

#endif
