#include "main-rfdb-t2.h"
#include "rf6defines.h"
#include <QTextStream>
#include "rf6projectinfo.h"

#include "rf6controls.h"

/*
This program demonstrates (and tests its correct work) something of class CRf6Database
*/


//QTextStream stream_out(stdout);
class CInpOutp
{
public:
	QTextStream inp; QTextStream outp;
	CInpOutp():inp(stdin), outp(stdout){}
	template<typename T>
	T get(const char* inv=NULL)
	{
		if (inv!=NULL) {outp<<inv; outp.flush();}
		T v; inp>>v; return v;
	}
};

using namespace Rf6;

CRf6Database rf6db;
CTestRfDb oTest(rf6db);
CInpOutp console;
CRf6ControlsManager controlManager;

void CTestRfDb::GiveObject(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob)
{
	qDebug() << parent << ob->GetName() << ob->GetDbType() << ob->GetIndex();

}

void CTestRfDb::ObjectsGiven(int cnt_error_getting)
{
	qDebug() << "All objects were recieved. Errors -" << cnt_error_getting;
	qDebug() << "**********************************";
}

void CTestRfDb::TestAnalogLimits(CRf6DbObject& atag)
{
	qDebug() << "Analog limits test\n----";
	QList<int> rf6AnaLimits;
	rf6AnaLimits << _RF_DB_ALARM_LL <<_RF_DB_ALARM_L << _RF_DB_ALARM_H << _RF_DB_ALARM_HH;
	CMemObject* pMO = atag.GetRdMemObject(rf6db);
	foreach (int limit, rf6AnaLimits)
	{
		//NANALOG_LIMIT* pAL = atag->GetAlarmLimit(rf6db, limit);
		NANALOG_LIMIT* pAL = pMO->AlarmLimit(limit);
		qDebug() << pAL->limit;
	}

}

void CTestRfDb::TestStatusState(CRf6DbObject& stag)
{
	qDebug() << "Status state test\n----";

	CMemObject* pMO = stag.GetRdMemObject(rf6db);
	for (int i=0; i<4; i++)
	{
		// NSTATUS_STATE* pAS = stag->GetAlarmState(rf6db, i);
		qDebug() << pMO->GetStatusDesc(i) << pMO->AlarmState(i)->alm_class;
	}
}

void CTestRfDb::TestWriteField(CRf6DbObject& atag)
{
	qDebug() << "Test of writing to field\n----";
	NANALOG_LIMIT* pAL = atag.GetAlarmLimit(rf6db, _RF_DB_ALARM_L);
	qDebug() << "Low limit is" << pAL->limit;
	pAL->limit = console.get<double>("New value: ");
	atag.WriteMemObject();
}

void CTestRfDb::sendControlComplete(int result, ULONG_64 uind, UCHAR ctltype)
{
	qDebug() << "sendControlComplete" << result << uind << ctltype;
}

void test_limit_state()
{
	qDebug() << "\nTest analog limits or status state. Select tag\n*****";
	int dbtype = console.get<int>("dbtype: ");
	ULONG_64 uind = rf6db.GetTagUnIndex(dbtype, console.get<QString>("pcu: ").toAscii().constData(), console.get<QString>("tag: ").toAscii().constData());
	if (uind == 0)
	{
		qDebug() << "Tag not found."; return;
	}

	CRf6DbObject dbo(uind);
	qDebug() << dbo.GetRdMemObject(rf6db)->GetTagName() << uind;

	if (dbtype==RF6_ANALOG)
	{
		oTest.TestAnalogLimits(dbo);
		oTest.TestWriteField(dbo);
	}
	else if (dbtype==RF6_STATUS)
		oTest.TestStatusState(dbo);
}

void test_controls()
{
	qDebug() << "Test controls.\n*****";
	ULONG_64 uind = rf6db.GetPcuUnIndex(console.get<QString>("pcu: ").toAscii().constData());
	if (uind == 0)
	{
		qDebug() << "PCU not found."; return;
	}
	QString ct = console.get<QString>("control type: ");
	switch (ct.toAscii().constData()[0])
	{
		case 'w':
			controlManager.sendWarmStart(uind); break;
		case 'd':
			controlManager.sendDemandScan(uind); break;
		default:
			qDebug() << "Unknown control type" << ct; break;
	}
}

void test_info()
{
	qDebug() << "\nInfo. Select tag\n*****";
	int dbtype = console.get<int>("dbtype: ");
	ULONG_64 uind = rf6db.GetTagUnIndex(dbtype, console.get<QString>("pcu: ").toUpper().toAscii().constData(), console.get<QString>("tag: ").toUpper().toAscii().constData());
	if (uind == 0)
	{
		qDebug() << "Tag not found."; return;
	}

	CRf6DbObject dbo(uind);
	qDebug() << "Name: " << dbo.GetRdMemObject(rf6db)->GetTagName();
	qDebug() << "Description: " << dbo.GetRdMemObject(rf6db)->GetDesc();

}

int main()
{
	QObject::connect(&rf6db, SIGNAL(GiveObject(ULONG_64, QSharedPointer<CRf6DbObject>)),
						&oTest, SLOT(GiveObject(ULONG_64, QSharedPointer<CRf6DbObject>)));

	QObject::connect(&rf6db, SIGNAL(ObjectsGiven(int)),
						&oTest, SLOT(ObjectsGiven(int)));

	QObject::connect(&controlManager, SIGNAL(sendControlComplete(int, ULONG_64, UCHAR)),
							&oTest, SLOT(sendControlComplete(int, ULONG_64, UCHAR)));

	rf6db.GetObjects();

	CRf6ProjectInfo rfprjinfo;
	qDebug() << "NodeName" << rfprjinfo.getNodeName();
	qDebug() << "Rfversion" << rfprjinfo.getRfversion();
	qDebug() << "ProjectName" << rfprjinfo.getProjectName();
	qDebug() << "RfState" << rfprjinfo.getRfState();

	while (1)
	{
		QString act = console.get<QString>("\nSelect action: ");
		switch (act.toAscii().constData()[0])
		{
			case 'l':
				test_limit_state(); break;
			case 'c':
				test_controls(); break;
			case 'i':
				test_info(); break;
			default:
				qDebug() << "Unknown action" << act; break;
		}
	}

	return 0;
}
