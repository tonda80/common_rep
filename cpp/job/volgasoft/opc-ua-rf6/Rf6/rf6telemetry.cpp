#include "rf6telemetry.h"

CRf6Telemetry::CRf6Telemetry()
{
}

CRf6Telemetry::~CRf6Telemetry()
{
}

#if (_RF_VERSION < _RF_QNX641)
char* CRf6Telemetry::findToken1(char *temp_buf, char *token)
#else
char* CRf6Telemetry::findToken1(char *temp_buf, const char *token)
#endif
{
    return strtok(temp_buf, token);
}

#if (_RF_VERSION < _RF_QNX641)
char* CRf6Telemetry::findToken2(char *temp_buf, char *token)
#else
char* CRf6Telemetry::findToken2(char *temp_buf, const char *token)
#endif
{
    char *str=NULL;
    str = strtok(temp_buf, token);
    if(str != NULL)
        return strtok(NULL, token);
    else
        return NULL;
}

void CRf6Telemetry::parseTime(char *buffer, TimeWidget_t *ptr)
{
#if (_RF_VERSION < _RF_QNX641)
    char * delims2=":";
#else
    const char * delims2=":";
#endif
    char hour=0, min=0, sec=0;

    //hours parsing
    char *p = strtok( buffer, delims2 );
    if( p == NULL )
    {
        return;
    }

    hour = atoi(p);
    if(hour < 0 || hour > 23 )
        hour = 0;

    //mins parsing
    p = strtok( NULL, delims2 );
    if( p == NULL )
    {
        return;
    }

    min = atoi(p);
    if(min < 0 || min > 59 )
        min = 0;

    //sec parsing
    p = strtok( NULL, delims2 );
    if( p == NULL )
    {
        return;
    }

    sec = atoi(p);
    if(sec < 0 || sec > 59 )
        sec = 0;
    ptr->cur_hour=ptr->prev_hour=hour;
    ptr->cur_min=ptr->prev_min=min;
    ptr->cur_sec=ptr->prev_sec=sec;
}

void CRf6Telemetry::parseDate(char *buffer, DateWidget_t *ptr)
{
    char day=0, mon=0;
    int year=0;

    //copy the date
#if (_RF_VERSION < _RF_QNX641)
    char *delims1="/";
#else
    const char * delims1="/";
#endif

    //day parsing
    char *p = strtok( buffer, delims1 );
    if(p == NULL)
    {
        return;
    }

    day = atoi(p);
    if(day < 1 || day > 31 )
        day = 0;

    //months parsing
    p = strtok( NULL, delims1 );
    if( p == NULL )
    {
        return;
    }

    mon = atoi(p);
    if(mon < 1 || mon > 12 )
        mon = 1;

    //year parsing
    p = strtok( NULL, delims1 );
    if(p == NULL)
    {
        return;
    }

    year = atoi(p);
    if(year < 0 )
        year = 1970;

    ptr->cur_date=ptr->prev_date=day;
    ptr->cur_mon=ptr->prev_mon=mon;
    ptr->cur_year=ptr->prev_year=year;
}
unsigned char *CRf6Telemetry::strcompress (unsigned char * cbuf)
{
    size_t	i , x ;
    size_t  roff ;
    size_t	csize ;

    unsigned char previous ;

    /**********************************************************************
    *
    * Start of code
    *
    **********************************************************************/

    roff = 0 ;

    csize = strlen((char *)cbuf) ;		// Get the length of the string

    if (csize < 1)
    {
        goto exit_point ;
    }

    if (csize == 1)
    {
        if (cbuf[0] <= ' ')
        {
            goto exit_point ;
        }
        else
        {
            roff = 1 ;
            goto exit_point ;
        }
    }

    if (csize == 2)
    {
        if ((cbuf[0] <= ' ') && (cbuf[1] <= ' '))
        {
            goto exit_point ; // "  "
        }
        else if ((cbuf[0] <= ' ') && (cbuf[1]  > ' ')) // " ?"
        {
            cbuf[0] = cbuf[1] ;
            cbuf[1] = ' ' ;
            roff = 1 ; // set to start storing at the null point
        }
        else if ((cbuf[0] > ' ') && (cbuf[1]  > ' '))
        {
            roff = ++csize ; // "??" - set to start storing at the null point
        }
        else
        {
            roff = 1 ;		// "? "
        }
        goto exit_point ;
    }

    // The trivial cases have been performed.  Now process strings that are at least 3 characters
    // long.

    for (i = 0 , previous = ' ' , x = 0 ; i < (csize - 1) ; i++)
    {
        if (cbuf[i] <= ' ')
        {
            cbuf[i] = ' ' ;
            if (previous == ' ')
            {
                strtrimleft (&cbuf[i]) ;
                previous = 'X' ;
            }
            else
            {
                previous = ' ' ;
            }
        }
        else
        {
            previous = 'X' ;
        }
    }
    exit_point: ;

    //remove the leading and trailing blanks
    str_trim((char *)cbuf);
    str_trim_lf((char *)cbuf);
    strtrimleft(cbuf);
    return cbuf ;
}

void CRf6Telemetry::str_trim(char * s)
{

    /* Find end of String */
    while (*s)
        ++s;
    /* Decrement One */
    --s;
    /* While end character is a space or a tab,
        replace char with a null char */
    while ((*s == 32) || (*s == 9))
    {
        *s = 0;
        --s;
    }
    return;
}

void CRf6Telemetry::strtrimleft (unsigned char * cbuf)
{
    size_t i , k , limit ;

    if (cbuf[0] > ' ')
    {
        return ;
    }

    limit = strlen((char *)cbuf) ;

    for (i = 0 ; i < limit ; i++)
    {
        if (cbuf[i] > ' ')
        {
            break ;
        }
    }

    if (i >= limit)
    {
        return ;
    }

    k = limit - i ;
    memmove (cbuf , &cbuf[i] , k) ;
    for ( ; k < limit ; k++)
    {
        cbuf[k] = ' ' ;
    }

    return ;
}

