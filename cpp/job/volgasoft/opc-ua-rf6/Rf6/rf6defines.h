// This file contained defines (etc.) from rf6 which couldn't be defined from h files for some reason

#ifndef RF6DEFINES_H
#define RF6DEFINES_H

#include <rfversions.h>

namespace Rf6
{
#if _RF_VERSION<6600
	//old realflex database filed constants
	const int _tagDescSize	= 20;
	const int _pcuDescSize	= 20;
	const int _tagNameSize	= 12;
	const int _pcuNameSize	= 12;
	const int _unitsSize	= 10;
	const int _statDescSize = 6;
#else
#include <dbfield_const.h>
	const int _tagDescSize	= LONG_TAG_DESC_SIZE;
	const int _pcuDescSize	= LONG_PCU_DESC_SIZE;
	const int _tagNameSize	= LONG_TAG_NAME_SIZE;
	const int _pcuNameSize	= LONG_PCU_NAME_SIZE;
	const int _unitsSize	= LONG_STAT_DESC_SIZE;
	const int _statDescSize = LONG_UNITS_SIZE;
#endif

	// pseudonyms of the types of tags
	const int RF6_ANALOG = 0;	// NANALOG_DB
	const int RF6_STATUS = 2;	// NSTATUS_DB
	const int RF6_METER = 1;	// NMETER_DB
	const int RF6_TANK = 3;		// NTANK_DB
	const int RF6_PCU = 14;		// NPCU_DB

}

#endif
