#include "rf6controls.h"
#include "rf6database.h"

#include <rfbase.hpp>
#include <rf6lib.hpp>

namespace Rf6
{

	int CRf6ControlsManager::sendControl(ULONG_64 uind, UCHAR ctltype, cntl_val_t *data)
	{
		unsigned int ind32;
		int bDel;
		CMemObject* pMemObj = rf6db.getRdMemObject(uind, &ind32, &bDel);
		if (pMemObj==NULL || ind32==0 || bDel==1)
		{
			emit sendControlComplete(RF6_DB_ERROR, uind, ctltype); 	// error
			return 1;
		}

		int res = RF_Control_Ex(ind32, GetDbType(uind), uind, ctltype, data, 0);
		if (res == RF_FAILURE)
		{
			emit sendControlComplete(SEND_CONTROL_ERROR, uind, ctltype); 	// error
			return 2;
		}

		emit sendControlComplete(CONTROL_SEND_OK, uind, ctltype); // ok
		return 0;
	}

	int CRf6ControlsManager::sendWarmStart(ULONG_64 uind)
	{
		if (GetDbType(uind) != NPCU_DB)
		{
			emit sendControlComplete(CONTROL_NOT_FOR_PCU, uind, WARMSTART_CONTROL); 	// error
			return 1;
		}

		cntl_val_t empty_ctl_val;
		return sendControl(uind, WARMSTART_CONTROL, &empty_ctl_val);
	}

	int CRf6ControlsManager::sendDemandScan(ULONG_64 uind)
	{
		if (GetDbType(uind) != NPCU_DB)
		{
			emit sendControlComplete(CONTROL_NOT_FOR_PCU, uind, DEMAND_SCAN); 	// error
			return 1;
		}

		cntl_val_t empty_ctl_val;
		return sendControl(uind, DEMAND_SCAN, &empty_ctl_val);
	}


}
