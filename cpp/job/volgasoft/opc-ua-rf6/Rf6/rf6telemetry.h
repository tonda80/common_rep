#ifndef RF6TELEMETRY_H
#define RF6TELEMETRY_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <vtype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <syscfg.hpp>
#include <errcodes.h>
#include <rfversions.h>
#include <getargs.h>
#include <errno.h>
#include <array.hpp>
#include <rfbase.hpp>
#include <alarmmsg.hpp>
#include <rf6lib.hpp>
#include <rfph/rfph.h>
#include <math.h>
#include <trans/rfu.h>
#include <trans/rftrans.h>

#include <_pack1.h>
#define	MAX_LBL_WIDGETS			20
#define	WIDGET_TYPE_TEXT		 0
#define	WIDGET_TYPE_NUMERIC		 1
#define	WIDGET_TYPE_LIST		 2
#define	WIDGET_TYPE_CHECK		 3
#define	WIDGET_TYPE_SKIP		 4
#define	WIDGET_TYPE_DATE		 5
#define	WIDGET_TYPE_TIME		 6
#define	WIDGET_TYPE_PCU_LIST	 7
#define	WIDGET_TYPE_ADDR_LIST	 8
#define	WIDGET_TYPE_GROUP_START	 9
#define	WIDGET_TYPE_GROUP_END	10

typedef struct
{
    int count;
    int state;
    PtWidget_t *pane;
    PtWidget_t *group;
    PtWidget_t *group_widgets[255];
}GROUP_WIDGET;

#define	TYPE_CHAR		 0
#define	TYPE_BYTE		 1
#define	TYPE_WCHAR		 2
#define	TYPE_SHORT		 3
#define	TYPE_USHORT		 4
#define	TYPE_INT		 5
#define	TYPE_UINT		 6
#define	TYPE_LONG		 7
#define	TYPE_ULONG		 8
#define	TYPE_FLOAT		 9
#define	TYPE_DOUBLE		10
#define	TYPE_STRING		11


typedef union
{
    char	data_char;
    BYTE	data_byte;
    wchar_t data_wchar;
    short	data_short;
    USHORT	data_ushort;
    int		data_int;
    UINT	data_uint;
    long	data_long;
    ULONG	data_ulong;
    float	data_float;
    double	data_double;
    char	data_str[255];
}DATA_VAL;

typedef struct
{
    char	cur_text[1025];
    char	prev_text[1025];
    int		text_len;
}TextWidget_t;

typedef struct
{
    double	min_val;
    double	max_val;
    double	cur_val;
    double	prev_val;
}NumericWidget_t;

typedef struct
{
    char	cur_state;
    char	prev_state;
}CheckBoxWidget_t;

typedef struct
{
    int		item_len;
    int		item_count;
    int		cur_sel;
    int		prev_sel;
    char	**list_items;
    char	**value_items;
}ListWidget_t;

typedef struct
{
    char cur_mon;
    char cur_date;
    int  cur_year;

    char prev_mon;
    char prev_date;
    int  prev_year;
}DateWidget_t;

typedef struct
{
    char cur_hour;
    char cur_min;
    char cur_sec;

    char prev_hour;
    char prev_min;
    char prev_sec;
}TimeWidget_t;

typedef struct
{
    int depend; //depend on
    double display_val;
}GroupStart_t;

typedef struct
{
    char delim;
}GroupEnd_t;


typedef union
{
    TextWidget_t		Text;
    NumericWidget_t		Numeric;
    CheckBoxWidget_t	CheckBox;
    ListWidget_t		List;
    DateWidget_t		Date;
    TimeWidget_t		Time;
    GroupStart_t		GroupS;
    GroupEnd_t			GroupE;
}TeleWidget_t;

typedef struct
{
    char	 widget_type;
    char	 data_type;
    char	 blabel;
    char	 bwidget_type;
    char	 bdata_type;
    int		 offset;

    PtWidget_t		*lbl_widget;
    PtWidget_t		*widget;
    char	 		label[256];
    TeleWidget_t	Tele;

}TelWidgetPtr_t;

typedef struct
{
    char	scan_name[32];
    unsigned char	version[4];
    unsigned int	data_size;
}TEL_HDR, *pTEL_HDR;

extern char DataDir[MAX_PATH];

#include <_packpop.h>

class CRf6Telemetry
{
public:
    CRf6Telemetry();
    ~CRf6Telemetry();

    static char* findToken1(char *temp_buf, const char *token);
    static char* findToken2(char *temp_buf, const char *token);

    static void parseTime(char *buffer, TimeWidget_t *ptr);
    static void parseDate(char *buffer, DateWidget_t *ptr);


    static unsigned char *strcompress(unsigned char *cbuf);
    static void str_trim(char *s);
    static void strtrimleft(unsigned char *cbuf);
protected:

    char	pcu_tel_path[PATH_MAX];
    char	channel_tel_path[PATH_MAX];

};

#endif // RF6TELEMETRY_H
