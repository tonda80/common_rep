#include "rf6uploadtelemetryfile.h"
#include "rf6readchnfile.h"

#include "helper.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

Rf6UploadTelemetryFile::Rf6UploadTelemetryFile()
{
}


std::fstream  File;
int Rf6UploadTelemetryFile::PcuError(int bWrite)
{
    if (!bWrite)
        debugLog.logToFile("Pcu Read Error  in Reading the PCU Record");
    else
        debugLog.logToFile("Pcu Write Error  in writing the PCU record");

    return 0;
}

void Rf6UploadTelemetryFile::setTelHdr(char *scanerName)
{
    strcpy(scanner_name, scanerName);
}

int Rf6UploadTelemetryFile::SaveTextField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+node->Tele.Text.text_len) < 64)
            memcpy(&PcuObj.DbRec.p->spare[node->offset], node->Tele.Text.cur_text, node->Tele.Text.text_len);
        else
            return RF_FAILURE;

        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        File.write((char *)node->Tele.Text.cur_text, node->Tele.Text.text_len);
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}


int Rf6UploadTelemetryFile::SaveNumericField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if(node->data_type == TYPE_CHAR )
        {
            char	data_val = (char)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(char) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(char));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val = (wchar_t)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(wchar_t) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(wchar_t));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val = (unsigned char)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(unsigned char) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned char));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val = (unsigned short)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(unsigned short) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned short));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val = (short)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(short) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(short));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val = (int)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(int) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(int));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val = (unsigned int)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(unsigned int) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned int));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val = (unsigned long)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(unsigned long) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned long));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val = (long)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(long) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(long));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val = (float)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(float) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(float));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val = (double)node->Tele.Numeric.cur_val;
            if((node->offset+sizeof(double) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(double));
            else
                return RF_FAILURE;
        }

        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        if(node->data_type == TYPE_CHAR )
        {
            char	data_val = (char)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(char));
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val = (wchar_t)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(wchar_t));
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val = (unsigned char)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(unsigned char));
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val = (unsigned short)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(unsigned short));
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val = (short)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(short));
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val = (int)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(int));
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val = (unsigned int)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(unsigned int));
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val = (unsigned long)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(unsigned long));
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val = (long)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(long));
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val = (float)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(float));
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val = (double)node->Tele.Numeric.cur_val;
            File.write((char *)&data_val, sizeof(double));
        }
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}

int Rf6UploadTelemetryFile::SaveListField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );

    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if(node->data_type == TYPE_STRING )
        {
            strcpy(err_msg, node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(char) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], err_msg, node->Tele.List.item_len);
            else
                return RF_FAILURE;
        }
        else if(node->data_type == TYPE_CHAR )
        {
            char	data_val = (char)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(char) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(char));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val = (wchar_t)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(wchar_t) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(wchar_t));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val = (unsigned char)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(unsigned char) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned char));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val = (unsigned short)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(unsigned short) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned short));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val = (short)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(short) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(short));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val = (int)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(int) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(int));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val = (unsigned int)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(unsigned int) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned int));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val = (unsigned long)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(unsigned long) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(unsigned long));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val = (long)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(long) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(long));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val = (float)atof(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(float) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(float));
            else
                return RF_FAILURE;
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val = (double)atof(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            if((node->offset+sizeof(double) ) < 64)
                memcpy(&PcuObj.DbRec.p->spare[node->offset], &data_val, sizeof(double));
            else
                return RF_FAILURE;
        }

        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        if(node->data_type == TYPE_STRING )
        {
            strcpy(err_msg, node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write(err_msg, node->Tele.List.item_len);
        }
        else if(node->data_type == TYPE_CHAR )
        {
            char	data_val = (char)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(char));
        }
        else	if(node->data_type == TYPE_WCHAR )
        {
            wchar_t	data_val = (wchar_t)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(wchar_t));
        }
        else	if(node->data_type == TYPE_BYTE )
        {
            unsigned char	data_val = (unsigned char)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(unsigned char));
        }
        else	if(node->data_type == TYPE_USHORT )
        {
            unsigned short	data_val = (unsigned short)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(unsigned short));
        }
        else	if(node->data_type == TYPE_SHORT )
        {
            short	data_val = (short)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(short));
        }
        else	if(node->data_type == TYPE_INT )
        {
            int data_val = (int)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(int));
        }
        else	if(node->data_type == TYPE_UINT )
        {
            unsigned int data_val = (unsigned int)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(unsigned int));
        }
        else	if(node->data_type == TYPE_ULONG )
        {
            unsigned long data_val = (unsigned long)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(unsigned long));
        }
        else	if(node->data_type == TYPE_LONG )
        {
            long data_val = (long)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(long));
        }
        else	if(node->data_type == TYPE_FLOAT )
        {
            float data_val = (float)atof(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(float));
        }
        else	if(node->data_type == TYPE_DOUBLE )
        {
            double data_val = (double)atoi(node->Tele.List.value_items[node->Tele.List.cur_sel-1] );
            File.write((char *)&data_val, sizeof(double));
        }
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}


int Rf6UploadTelemetryFile::SaveCheckBoxField(TelWidgetPtr_t *node)
{
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+sizeof(char)) < 64)
            memcpy(&PcuObj.DbRec.p->spare[node->offset], &node->Tele.CheckBox.cur_state, sizeof(char) );
        else
            return RF_FAILURE;
        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        File.write((char *)&node->Tele.CheckBox.cur_state, sizeof(char));
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}




int Rf6UploadTelemetryFile::SaveTimeField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );
    sprintf(err_msg, "%02d:%02d:%02d", node->Tele.Time.cur_hour, node->Tele.Time.cur_min, node->Tele.Time.cur_sec);
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }

        if((node->offset+sizeof(char)) < 64)
            memcpy(&PcuObj.DbRec.p->spare[node->offset], err_msg, strlen(err_msg) );
        else
            return RF_FAILURE;
        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        File.write((char *)err_msg, strlen(err_msg));
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}



int Rf6UploadTelemetryFile::SaveDateField(TelWidgetPtr_t *node)
{
    memset(err_msg, 0x00, sizeof(err_msg) );
    sprintf(err_msg, "%02d:%02d:%02d", node->Tele.Date.cur_date, node->Tele.Date.cur_mon, node->Tele.Date.cur_year);
    if(node->offset >= 0)
    {
        if(PcuObj.Read(PcuIndex) != RF_SUCCESS)
        {
            PcuError();
            return RF_FAILURE;
        }
        if((node->offset+sizeof(char)) < 64)
            memcpy(&PcuObj.DbRec.p->spare[node->offset], err_msg, strlen(err_msg) );
        else
            return RF_FAILURE;

        if(PcuObj.Write(PcuIndex) != RF_SUCCESS)
        {
            PcuError(TRUE);
            return RF_FAILURE;
        }
    }
    else
    {
        File.write((char *)err_msg, strlen(err_msg));
        if( !File )
            return RF_FAILURE;
    }
    return RF_SUCCESS;
}


int Rf6UploadTelemetryFile::SaveTeleInFile(char *tel_file_name, IArray< TelWidgetPtr_t > &CfgArray)
{
    if(CfgArray.NumberOfItems() <= 0)
        return 0;

    File.clear();
    File.open( tel_file_name, ios::out | ios::trunc  ) ; //, 0660 );
    if( !File )
    {
        debugLog.logToFile("File Open Error in opening the Telemetry file");
        return Pt_CONTINUE;
    }

    memset(&tel_hdr, 0x00, sizeof(TEL_HDR) );
    strncpy(tel_hdr.scan_name, scanner_name, sizeof(tel_hdr.scan_name));
    File.write((char *)&tel_hdr, sizeof(TEL_HDR) );

    TelWidgetPtr_t *node=NULL;
    int iRet=0;
    for(int i=0; i<CfgArray.NumberOfItems(); i++)
    {
        node = CfgArray.GetItem(i);

        if(node == NULL)
            continue;

        if(node->widget_type == WIDGET_TYPE_TEXT)
        {
            iRet = SaveTextField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_NUMERIC)
        {
            iRet = SaveNumericField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_LIST)
        {
            iRet = SaveListField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_CHECK)
        {
            iRet = SaveCheckBoxField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_DATE)
        {
            iRet = SaveDateField(node);
        }
        else if(node->widget_type == WIDGET_TYPE_TIME)
        {             
            iRet = SaveTimeField(node);
        }
        else
            continue;

        if(iRet != RF_SUCCESS)
        {
            debugLog.logToFile("Error in Writing %d field", i);
            debugLog.logToFile("File Save Error %s \n", err_msg);
            return( Pt_CONTINUE );
        }

    }
    File.close();
    return 0;
}
int Rf6UploadTelemetryFile::SaveTelemetryForChanel(int chanel, IArray<TelWidgetPtr_t> *CfgArray)
{

    CRf6ReadChnFile readerChn;
    QString nameDriver;

    readerChn.readDriverChannelFile(chanel,nameDriver);
    setTelHdr(readerChn.getScanerName());
    QString fullPath = QString(DataDir) + "/tel/"+ nameDriver +"."+QString::number(chanel);
    return SaveTeleInFile( fullPath.toAscii().data() ,*CfgArray);
}

int Rf6UploadTelemetryFile::SaveTelemetryForPcu(QString pcuName, IArray<TelWidgetPtr_t> *CfgArray)
{

    QString fullPath = QString(DataDir) + "/tel/"+ pcuName +".tel";
    return SaveTeleInFile(fullPath.toAscii().data() ,*CfgArray);
}

int Rf6UploadTelemetryFile::SaveTelemetryForPcu(int chanel, QString pcuName, IArray<TelWidgetPtr_t> *CfgArray)
{
    CRf6ReadChnFile readerChn;
    QString nameDriver;
    readerChn.readDriverChannelFile(chanel,nameDriver);
    setTelHdr(readerChn.getScanerName());
    return  SaveTelemetryForPcu(pcuName, CfgArray);
}
