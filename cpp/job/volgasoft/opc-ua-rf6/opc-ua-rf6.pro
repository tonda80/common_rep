TEMPLATE = app
CONFIG += console qt
QT -= gui
#defined variables for win64 for vs2010 sp1
win32 {

OPENSSL_HOME = "C:/Program Files (x86)/UnifiedAutomation/UaSdkCppBundleEval/third-party/win32/vs2010sp1/openssl"
UASDK_HOME = "C:/Program Files (x86)/UnifiedAutomation/UaSdkCppBundleEval"
LIBXML2_HOME = "C:/Program Files (x86)/UnifiedAutomation/UaSdkCppBundleEval/third-party/win32/vs2010sp1/libxml2"
}
qnx {

UASDK_HOME = /usr/local
}

DEPENDPATH += .
INCLUDEPATH += .

# Definitions
DEFINES += \
        SUPPORT_XML_CONFIG \
        _UA_STACK_USE_DLL \
        OPCUA_SUPPORT_SECURITYPOLICY_BASIC128RSA15=1 \
        OPCUA_SUPPORT_SECURITYPOLICY_BASIC256=1 \
        OPCUA_SUPPORT_SECURITYPOLICY_NONE=1 \
        OPCUA_SUPPORT_PKI=1

#add Path to include
INCLUDEPATH += $$UASDK_HOME/include/uastack \
            $$UASDK_HOME/include/uabase \
            $$UASDK_HOME/include/uapki \
            $$UASDK_HOME/include/xmlparser \
            $$UASDK_HOME/include/uaserver \
            $$OPENSSL_HOME/inc32/ \
            opc_utilities


HEADERS += \
	version.h \
    opc_utilities/shutdown.h \
    opc_utilities/serverconfigxml.h \
    opc_utilities/opcserver.h \
    interactiveservice.h \
    opc_utilities/helper.h

SOURCES += main.cpp \
    opc_utilities/opcserver.cpp \
    opc_utilities/shutdown.cpp \
    opc_utilities/serverconfigxml.cpp \
    interactiveservice.cpp \
    opc_utilities/helper.cpp \

win32 {

    CONFIG(release, debug|release ) {
        message(Release build!)
        #add release lib
        LIBS += \
            -L$$UASDK_HOME/lib \
            -L$$OPENSSL_HOME/out32dll \
            -L$$LIBXML2_HOME/out32dll \
            -luastack \
            -luabase \
            -luapki \
            -lxmlparser \
            -lcoremodule \
            -luamodule \
            -luamodels \
            -llibeay32 \
            -llibxml2 \
            -lws2_32 \
            -lrpcrt4 \
            -lOleAut32 \
            -lOle32 \
            -lcrypt32


    }
    CONFIG(debug, debug|release )   {
        message(debug build!)
        #add debug lib
        LIBS += \
            -L$$UASDK_HOME/lib \
            -L$$OPENSSL_HOME/out32dll.dbg \
            -L$$LIBXML2_HOME/out32dll.dbg \
            -luastackd \
            -luabased \
            -luapkid \
            -lxmlparserd \
            -lcoremoduled \
            -luamoduled \
            -luamodelsd \
            -llibeay32d \
            -llibxml2d \
            -lws2_32 \
            -lrpcrt4 \
            -lOleAut32 \
            -lOle32 \
            -lcrypt32

     }
}
qnx {
    LIBS +=/usr/pkg/lib/libxml2.so
    LIBS += \
         -L/usr/lib \
         -lcrypto \
         -L$$UASDK_HOME/lib
         
    CONFIG += console debug 
    DEFINES += RF6_DEBUGLOG
    
    CONFIG(release, debug|release ) {
        message(Release build!)
    LIBS +=\
         -lcoremodule \
         -luamodule \
         -luastack \
         -luabase \
         -luapki \
         -lxmlparser \
         -luamodels
    }
    CONFIG(debug, debug|release ) {
        message(Debug build!)
    LIBS +=\
         -lcoremoduled \
         -luamoduled \
         -luastackd \
         -luabased \
         -luapkid \
         -lxmlparserd \
         -luamodelsd
    }




}


include(qtservice/src/qtservice.pri)
include(opc_rf6/opcrf6.pri)
include(Rf6/rf6.pri)
include(dao/dao.pri)
include(opc_sql/opc_sql.pri)
include(opc_alias/opcalias.pri)


