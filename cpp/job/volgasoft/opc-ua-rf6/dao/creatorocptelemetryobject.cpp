#include "creatorocptelemetryobject.h"
#include "opcua_basedatavariabletype.h"
#include "opcua_multistatediscretetype.h"
#include "opcua_foldertype.h"
#include "time.h"
#include "opcua_datetime.h"
#include "Rf6/rf6telemetry.h"
namespace Rf6{
CreatorOcpTelemetryObject::CreatorOcpTelemetryObject()
{
}


OpcRf6::Rf6Telemetry *CreatorOcpTelemetryObject::createAndUpdate( ArrayTeleWidget *telemetry)
{

    TelWidgetPtr_t * node;
    for (int i = 0;i<telemetry->NumberOfItems();i++){
        node = telemetry->GetItem(i);
        if(NULL != node){

            UaNode *nodeOpc = createField(i,node);

        }
    }
}

void CreatorOcpTelemetryObject::setNodeManagerUaNode(NodeManagerUaNode *mNodeManager)
{

    mNodeManagerUaNode = mNodeManager;
}

void CreatorOcpTelemetryObject::setParentTelemetry(OpcRf6::Rf6Telemetry *parent)
{
    parentTelemetry = parent;
    folderParent = parent->getfields();

}

UaNode *CreatorOcpTelemetryObject::createField(int numberchanel,TelWidgetPtr_t *feild)
{
    switch(feild->widget_type){
    case WIDGET_TYPE_GROUP_START:
        return createFieldGroupS(numberchanel,feild);
        break;
    case WIDGET_TYPE_GROUP_END:
        return createFieldGroupE(numberchanel,feild);
        break;
    case WIDGET_TYPE_TEXT:
        return createFieldText(numberchanel,feild);
        break;
    case WIDGET_TYPE_NUMERIC:
        return createFieldNumeric(numberchanel,feild);
        break;
    case WIDGET_TYPE_LIST:
        return createFieldList(numberchanel,feild);
        break;
    case WIDGET_TYPE_CHECK:
        return createFieldCheck(numberchanel,feild);
        break;
    case WIDGET_TYPE_SKIP:
        return createFieldSkip(numberchanel,feild);
        break;
    case WIDGET_TYPE_DATE:
        return createFieldDate(numberchanel,feild);
        break;
    case WIDGET_TYPE_TIME:
        return createFieldTime(numberchanel,feild);
        break;
    case WIDGET_TYPE_PCU_LIST:
        return createFieldList(numberchanel,feild);
        break;
    case WIDGET_TYPE_ADDR_LIST:
        return createFieldList(numberchanel,feild);
        break;
    }
    return NULL;
}

UaNode *CreatorOcpTelemetryObject::createFieldText(int numberchanel,TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *text;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
        text= new OpcUa::BaseDataVariableType(newNodeId,
                                             UaString(feild->label),
                                             mNodeManagerUaNode->getNameSpaceIndex(),
                                             UaVariant(feild->Tele.Text.cur_text),
                                             3,
                                             mNodeManagerUaNode);
         mNodeManagerUaNode->addNodeAndReference(folderParent,text,OpcUaId_HasComponent);
    }else {
        text  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != text){
            UaDataValue data;
            UaVariant value(feild->Tele.Text.cur_text);
            data.setValue(value,OpcUa_True,OpcUa_True);
            text->setValue(NULL,data,OpcUa_False);

        }
    }
    return text;
}

UaNode *CreatorOcpTelemetryObject::createFieldNumeric(int numberchanel,TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *numeric;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr+UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
    numeric= new OpcUa::BaseDataVariableType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          UaVariant(feild->Tele.Numeric.cur_val),
                                          3,
                                          mNodeManagerUaNode);
    mNodeManagerUaNode->addNodeAndReference(folderParent,numeric,OpcUaId_HasComponent);
    }else {
        numeric  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != numeric){
            UaDataValue data;
            UaVariant value(feild->Tele.Numeric.cur_val);
            data.setValue(value,OpcUa_True,OpcUa_True);
            numeric->setValue(NULL,data,OpcUa_False);
        }
    }
    return numeric;
}

UaNode *CreatorOcpTelemetryObject::createFieldList(int numberchanel,TelWidgetPtr_t *feild)
{
    OpcUa::MultiStateDiscreteType *list;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr+UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
    list= new OpcUa::MultiStateDiscreteType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          UaVariant(feild->Tele.List.cur_sel),
                                          3,
                                          mNodeManagerUaNode);
    UaLocalizedTextArray listSting;
    listSting.create(feild->Tele.List.item_count);
    for(int i = 0; i<feild->Tele.List.item_count; i++){
        UaString text = UaString(feild->Tele.List.list_items[i]);
        text.copyTo(&listSting[i].Text);
    }
    list->setEnumStrings(listSting);
    mNodeManagerUaNode->addNodeAndReference(folderParent,list,OpcUaId_HasComponent);
    }else {
        list  = dynamic_cast<OpcUa::MultiStateDiscreteType *>(fieldNode);
        if (NULL != list){
            UaDataValue data;
            UaVariant value(feild->Tele.List.cur_sel);
            data.setValue(value,OpcUa_True,OpcUa_True);
            list->setValue(NULL,data,OpcUa_False);
        }
    }
    return list;
}

UaNode *CreatorOcpTelemetryObject::createFieldCheck(int numberchanel,TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *check;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
    check= new OpcUa::BaseDataVariableType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          UaVariant(feild->Tele.CheckBox.cur_state),
                                          3,
                                          mNodeManagerUaNode);

    mNodeManagerUaNode->addNodeAndReference(folderParent,check,OpcUaId_HasComponent);
    }else {
        check  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != check){
            UaDataValue data;
            UaVariant value(feild->Tele.CheckBox.cur_state);
            data.setValue(value,OpcUa_True,OpcUa_True);
            check->setValue(NULL,data,OpcUa_False);
        }
    }
    return check;
}

UaNode *CreatorOcpTelemetryObject::createFieldSkip(int numberchanel,TelWidgetPtr_t *feild)
{
    return NULL;
}

UaNode *CreatorOcpTelemetryObject::createFieldDate(int numberchanel,TelWidgetPtr_t *feild)
{
    struct tm *val;
    val->tm_mday = feild->Tele.Date.cur_date;
    val->tm_mon = feild->Tele.Date.cur_mon;
    val->tm_year = feild->Tele.Date.cur_year;
    UaDateTime  valueDate = UaDateTime::fromTime_t(mktime(val));
    UaVariant value;
    value.setDateTime(valueDate);
    OpcUa::BaseDataVariableType *date;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
    date= new OpcUa::BaseDataVariableType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          UaVariant(value),
                                          3,
                                          mNodeManagerUaNode);
    mNodeManagerUaNode->addNodeAndReference(folderParent,date,OpcUaId_HasComponent);
    }else {
        date  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != date){
            val->tm_mday = feild->Tele.Date.cur_date;
            val->tm_mon = feild->Tele.Date.cur_mon;
            val->tm_year = feild->Tele.Date.cur_year;
            valueDate = UaDateTime::fromTime_t(mktime(val));
            UaDataValue data;
            value.setDateTime(valueDate);
            data.setValue(value,OpcUa_True,OpcUa_True);
            date->setValue(NULL,data,OpcUa_False);
        }
    }
    return date;
}

UaNode *CreatorOcpTelemetryObject::createFieldTime(int numberchanel,TelWidgetPtr_t *feild)
{
    struct tm *val;
    val->tm_hour = feild->Tele.Time.cur_hour;
    val->tm_min = feild->Tele.Time.cur_min;
    val->tm_sec = feild->Tele.Time.cur_sec;
    UaDateTime  valueTime = UaDateTime::fromTime_t(mktime(val));
    UaVariant value;
    value.setDateTime(valueTime);
    OpcUa::BaseDataVariableType *time;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
    time= new OpcUa::BaseDataVariableType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          UaVariant(value),
                                          3,
                                          mNodeManagerUaNode);
    mNodeManagerUaNode->addNodeAndReference(folderParent,time,OpcUaId_HasComponent);
    }else {
        time  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != time){
            val->tm_hour = feild->Tele.Time.cur_hour;
            val->tm_min = feild->Tele.Time.cur_min;
            val->tm_sec = feild->Tele.Time.cur_sec;
            valueTime = UaDateTime::fromTime_t(mktime(val));
            UaDataValue data;
            value.setDateTime(valueTime);
            data.setValue(value,OpcUa_True,OpcUa_True);
            time->setValue(NULL,data,OpcUa_False);
        }
    }
    return time;
}

UaNode *CreatorOcpTelemetryObject::createFieldGroupS(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::FolderType *grop;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    grop= new OpcUa::FolderType(newNodeId,
                                          UaString(feild->label),
                                         mNodeManagerUaNode->getNameSpaceIndex(),
                                          mNodeManagerUaNode->getNodeManagerConfig());

    mNodeManagerUaNode->addNodeAndReference(folderParent,grop,OpcUaId_HasComponent);
    folderParent = grop;
    return grop;
}

UaNode *CreatorOcpTelemetryObject::createFieldGroupE(int numberchanel, TelWidgetPtr_t *feild)
{
     folderParent = parentTelemetry->getfields();
}
}
