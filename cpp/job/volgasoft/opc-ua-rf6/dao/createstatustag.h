#ifndef CREATESTATUSTAG_H
#define CREATESTATUSTAG_H
#include "createopcobject.h"
#include "opcrf6_rf6statustag.h"
namespace OpcRf6 {
class CreateStatusTag: public CreateOpcObject
{
public:
    CreateStatusTag();
    virtual UaNode *createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);

    UaNode *createAndUpdateStatusState(Rf6StatusTag *Status, pCRf6DbObject &ob);
};
}
#endif // CREATESTATUSTAG_H
