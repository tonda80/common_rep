#include "writetelemetryfromopcobj.h"
#include "opcua_basedatavariabletype.h"
#include "opcua_multistatediscretetype.h"
#include "opcua_foldertype.h"
#include "time.h"
#include "opcua_datetime.h"
#include "Rf6/rf6telemetry.h"
namespace Rf6{
WriteTelemetryFromOpcObj::WriteTelemetryFromOpcObj()
{
}

OpcRf6::Rf6Telemetry *WriteTelemetryFromOpcObj::rewriteTelemetry( ArrayTeleWidget *telemetry)
{

    TelWidgetPtr_t * node;
    for (int i = 0;i<telemetry->NumberOfItems();i++){
        node = telemetry->GetItem(i);
        if(NULL != node){
            UaNode *nodeOpc = createField(i,node);
        }
    }
}

void WriteTelemetryFromOpcObj::setNodeManagerUaNode(NodeManagerUaNode *mNodeManager)
{

    mNodeManagerUaNode = mNodeManager;
}

void WriteTelemetryFromOpcObj::setParentTelemetry(OpcRf6::Rf6Telemetry *parent)
{
    parentTelemetry = parent;
    folderParent = parent->getfields();

}

UaNode *WriteTelemetryFromOpcObj::createField(int numberchanel,TelWidgetPtr_t *feild)
{
    switch(feild->widget_type){
    case WIDGET_TYPE_TEXT:
        return createFieldText(numberchanel,feild);
        break;
    case WIDGET_TYPE_NUMERIC:
        return createFieldNumeric(numberchanel,feild);
        break;
    case WIDGET_TYPE_LIST:
        return createFieldList(numberchanel,feild);
        break;
    case WIDGET_TYPE_CHECK:
        return createFieldCheck(numberchanel,feild);
        break;
    case WIDGET_TYPE_DATE:
        return createFieldDate(numberchanel,feild);
        break;
    case WIDGET_TYPE_TIME:
        return createFieldTime(numberchanel,feild);
        break;
    case WIDGET_TYPE_PCU_LIST:
        return createFieldList(numberchanel,feild);
        break;
    case WIDGET_TYPE_ADDR_LIST:
        return createFieldList(numberchanel,feild);
        break;
    }
    return NULL;
}

UaNode *WriteTelemetryFromOpcObj::createFieldText(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *text;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        text  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != text){

            UaDataValue dataValue(text->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();

            UaString str = defaultValue.toString();
            if(str.size() > feild->Tele.Text.text_len){
                return NULL;
            }else{
                memcpy(feild->Tele.Text.cur_text,str.toUtf8(),str.size());
            }
        }
    }
    return text;
}

UaNode *WriteTelemetryFromOpcObj::createFieldNumeric(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *numeric;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        numeric  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != numeric){
            UaDataValue dataValue(numeric->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();
            double currentValue;
            defaultValue.toDouble(currentValue);
            if( (currentValue < feild->Tele.Numeric.min_val) || (currentValue > feild->Tele.Numeric.max_val) ){
                return NULL;
            }else{
                feild->Tele.Numeric.cur_val = currentValue;
            }
        }
    }
    return numeric;
}

UaNode *WriteTelemetryFromOpcObj::createFieldList(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *list;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        list  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != list){
            UaDataValue dataValue(list->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();
            int currentSel;
            defaultValue.toInt32(currentSel);
            if((currentSel < 1) || (currentSel > feild->Tele.List.item_count)){
                return NULL;
            }else{
                feild->Tele.List.cur_sel = currentSel;
            }
        }
    }
    return list;
}

UaNode *WriteTelemetryFromOpcObj::createFieldCheck(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *check;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        check  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != check){
            UaDataValue dataValue(check->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();
            OpcUa_SByte currentState;
            defaultValue.toSByte(currentState);
            feild->Tele.CheckBox.cur_state= currentState;
        }
    }
    return check;
}

UaNode *WriteTelemetryFromOpcObj::createFieldDate(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *date;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        date  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != date){
            UaDataValue dataValue(date->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();
             UaDateTime  valueDate;
            defaultValue.toDateTime(valueDate);

            struct tm * tmp;
            tmp = localtime((time_t*)valueDate.toTime_t());
            feild->Tele.Date.cur_date = tmp->tm_mday;
            feild->Tele.Date.cur_mon = tmp->tm_mon;
            feild->Tele.Date.cur_year = tmp->tm_year;
        }
    }
    return date;
}

UaNode *WriteTelemetryFromOpcObj::createFieldTime(int numberchanel, TelWidgetPtr_t *feild)
{
    OpcUa::BaseDataVariableType *time;
    UaString idStr = parentTelemetry->nodeId().toString();
    idStr=idStr + UaString(".field%1").arg(numberchanel);
    UaNodeId newNodeId = UaNodeId(idStr, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *fieldNode =  mNodeManagerUaNode->getNode(newNodeId);
    if (NULL == fieldNode){
       return NULL;
    }else {
        time  = dynamic_cast<OpcUa::BaseDataVariableType *>(fieldNode);
        if (NULL != time){
            UaDataValue dataValue(time->value(NULL));
            UaVariant defaultValue;
            defaultValue = *dataValue.value();
             UaDateTime  valueDate;
            defaultValue.toDateTime(valueDate);

            struct tm * tmp;
            tmp = localtime((time_t*)valueDate.toTime_t());
            feild->Tele.Time.cur_hour = tmp->tm_hour;
            feild->Tele.Time.cur_min = tmp->tm_min;
            feild->Tele.Time.cur_sec = tmp->tm_sec;
        }
    }
    return time;
}


}
