#ifndef DAO_ORDER_H
#define DAO_ORDER_H

#include <QObject>
#include <uanodeid.h>
#include "opcua_baseobjecttype.h"


//interface
class dao_Order : public QObject
{
    Q_OBJECT
public:
    explicit dao_Order(QObject *parent = 0);
    virtual void requestAllDB();
    void setNamespaceIndex(qint16 index);
    virtual int deleteObject(const UaNodeId &deleteNodeId);
    void setNodeManagerUaNode(NodeManagerUaNode *managerUaNode);
    virtual UaStatus createObject(const UaNodeId& parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass nodeClass, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition, UaNodeId &addedNodeId);
    void setNodeManagerConfig(NodeManagerConfig *config);
    virtual UaStatus updateAttributes(UaNodeId &NodeId, UaLocalizedText &nodeAtrib);
    virtual UaStatus updateAttributes(OpcUa_WriteValue *writeNode);
    virtual UaStatus updateObject(UaNodeId& NodeId);


    
signals:
    void finishDownload();

    void newOpcObj(const UaNodeId& parentNodeId,
                       const UaNodeId &referenceTypeId,
                       const UaNodeId& requestedNewNodeId,
                       const UaQualifiedName &browseName,
                       const UaNodeId &typeDefinition
                       );
    void opcObject(UaNodeId& parentNodeId,UaReferenceLists *opcObj, UaNodeId&  referenceTypeId);
protected:
    qint16 namespaceIndex;

    NodeManagerUaNode *mNodeManagerUaNode;
    NodeManagerConfig *mNodeManagerConfig;

    

};

#endif // DAO_ORDER_H
