#include "createstatustag.h"
#include "opcrf6_rf6statustag.h"
#include "helper.h"
namespace OpcRf6 {
CreateStatusTag::CreateStatusTag()
{
}

UaNode *CreateStatusTag::createAndUpdate(ULONG_64 parent, pCRf6DbObject ob)
{

    //convert id
    QString QStrId = QString::number(ob->GetIndex(),16);
    UaString UaStrId = Helper::QStringToUaString(QStrId);
    //create node id
    UaNodeId requestNodeId = UaNodeId(UaStrId,mNodeManagerUaNode->getNameSpaceIndex());
    //request node
    UaNode *opcNode = mNodeManagerUaNode->getNode(requestNodeId);
    OpcRf6::Rf6StatusTag *Status;
    if (NULL == opcNode ){
        //node not exist
        //node name
        UaString nodeName =Helper::QStringToUaString(ob->GetName());
        //convert id parent
        QString QStrIdParent = QString::number(parent,16);
        UaString UaStrIdParent = Helper::QStringToUaString(QStrIdParent);
        // parent node id
        UaNodeId parentNodeId = UaNodeId(UaStrIdParent,mNodeManagerUaNode->getNameSpaceIndex());
        //get parent
        UaNode *parentNode = mNodeManagerUaNode->getNode(parentNodeId);

        if (NULL == parentNode ){
            return NULL; //parent node not exists
        }
        if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
             return NULL; //parent node not exists
        }

        Status = new OpcRf6::Rf6StatusTag(requestNodeId,nodeName,mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);

        //add reference
         mNodeManagerUaNode->addNodeAndReference(parentNodeId,Status,OpcUaId_Organizes);

    } else {
        //node exist
        Status  = dynamic_cast<OpcRf6::Rf6StatusTag *>(opcNode);
        if (NULL == Status){
            return NULL;
        }
    }
    if (NULL != ob->PtrStaData()){
        Status->fromRf6Structure(*ob->PtrStaData());
        createAndUpdateStatusState(Status,ob);
    }

     return Status;
}
UaNode *CreateStatusTag::createAndUpdateStatusState(OpcRf6::Rf6StatusTag *Status, pCRf6DbObject &ob)
{

    UaLocalizedTextArray listSting;
    listSting.create(4);

    CMemObject *pMO = ob->GetRdMemObject(*mDatabase);
    if (pMO != NULL){
        //set value descw
        for(int i = 0; i< 4; i++){
            UaString text(pMO->GetStatusDesc(i));
            text.copyTo(&listSting[i].Text);
        }
        Status->mValue()->setEnumStrings(listSting);
        Status->setDescription(UaLocalizedText(UaString("en"), UaString(ob->GetRdMemObject(*mDatabase)->GetDesc())));
    }

    //create or up4 status state
    for (int i = 0 ; i < 4; i++) {
        UaString nodeName =UaString("state%1").arg(i);
        UaString nodeId = Status->getKey().toString() +"."+nodeName;

        UaNode *statusStateNode = mNodeManagerUaNode->getNode(UaNodeId(nodeId, mNodeManagerUaNode->getNameSpaceIndex()));
        OpcRf6::Rf6StatusState * pStatusState;
        if (NULL  == statusStateNode) {
           pStatusState = new OpcRf6::Rf6StatusState(UaNodeId(nodeId, mNodeManagerUaNode->getNameSpaceIndex()),
                                                       nodeName,
                                                       mNodeManagerUaNode->getNameSpaceIndex(),
                                                       UaVariant(),
                                                       3,
                                                       mNodeManagerUaNode);
            mNodeManagerUaNode->addNodeAndReference(Status->getstates(),pStatusState,OpcUaId_HasComponent);

        } else {
             pStatusState  = dynamic_cast<OpcRf6::Rf6StatusState *>(statusStateNode);

        }

        if (NULL != pStatusState) {
             pStatusState->fromRf6Structure(*ob->GetAlarmState(*mDatabase,i));


             UaDataValue dataValue;
             UaLocalizedText Text(listSting[i]);
             UaVariant variant(Text);
             dataValue.setValue(variant,OpcUa_True);

             pStatusState->setValue(NULL,dataValue,OpcUa_False);


        }


    }
}
}
