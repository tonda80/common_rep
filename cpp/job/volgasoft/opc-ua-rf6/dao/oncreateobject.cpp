#include "oncreateobject.h"
#include "opcua_foldertype.h"
#include "opc_rf6/opcrf6_rf6aliasobject.h"
#include "opc_rf6/opcrf6_rf6aliasvariable.h"
#include "helper.h"
#include "createuanode.h"
#include "opc_alias/createuanode.h"

using namespace OpcRf6;
using namespace OpcUa;

UaStatus COnCreateObject::createObject(const UaNodeId &parentNodeId,
									   const UaNodeId &referenceTypeId,
									   const UaNodeId &requestNodeId,
									   const UaQualifiedName &browseName,
                                       OpcUa_NodeClass nodeClass,
                                       const UaExtensionObject &nodeAttributes,
                                       const UaNodeId &typeDefinition,
                                       UaNodeId &addedNodeId)
{
    CreateOpcUaNode creator;
    creator.setNodeManagerUaNode(mNodeManagerUaNode);
    UaNode * node = NULL;
    UaStatus ret = creator.createUaNode(requestNodeId,browseName,typeDefinition,node);	// result for return from function

    if (ret.statusCode() == OpcUa_BadNodeIdExists ) {
        addedNodeId = node->getKey();
        return ret;
    }
    else if (ret.isBad()) {
		return ret;
	}
    else if (node == NULL) {
    	return OpcUa_Bad;
    }

    ret = mNodeManagerUaNode->addUaNode(node);
    if (ret.isBad())
    {
    	debugLog.logToFile("COnCreateObject::createObject. Error of addUaNode() %s", ret.toString().toUtf8());
    	return ret;
    }

    //create reference
    ret = mNodeManagerUaNode->addUaReference(parentNodeId, node->getKey(), referenceTypeId);
    if (ret.isBad()) {
    	mNodeManagerUaNode->deleteUaNode(node, OpcUa_True);
    	debugLog.logToFile("COnCreateObject::createObject. Error of addUaReference() %s", ret.toString().toUtf8());
        return ret;
    }

    //set Dysplay name and decription
    bool flgAttrAdded = false;
    if ((nodeAttributes!= NULL) && (nodeAttributes.encoding() == UaExtensionObject::EncodeableObject) && (nodeAttributes.object() != NULL))
    {
    	//debugLog.logToFile("COnCreateObject::createObject. __deb1 %d %d", nodeClass, nodeAttributes.objectType()->TypeId);
    	if ((nodeClass==OpcUa_NodeClass_Object) && (nodeAttributes.objectType()->TypeId==OpcUaId_ObjectAttributes))
    	{
    		OpcUa_ObjectAttributes* objectAttributes = (OpcUa_ObjectAttributes*)nodeAttributes.object();
    		static_cast<OpcUa::BaseObjectType *>(node)->setDescription(objectAttributes->Description);
       		static_cast<OpcUa::BaseObjectType *>(node)->setDisplayName(objectAttributes->DisplayName);
       		flgAttrAdded = true;
    	}
    	else if ((nodeClass==OpcUa_NodeClass_Variable) && (nodeAttributes.objectType()->TypeId==OpcUaId_VariableAttributes))
    	{
    		OpcUa_VariableAttributes* variableAttributes = (OpcUa_VariableAttributes*)nodeAttributes.object();
    		static_cast<OpcUa::BaseVariableType *>(node)->setDisplayName(variableAttributes->DisplayName);
			static_cast<OpcUa::BaseVariableType *>(node)->setDescription(variableAttributes->Description);
			flgAttrAdded = true;
    	}
    }

	int res = addToDb(node, parentNodeId, referenceTypeId);
    if (res != 0)
	{
		debugLog.logToFile("COnCreateObject::createObject. Sql db error. addToDb() returns %d", res);
	}

    addedNodeId = node->getKey();

    if (flgAttrAdded)
    	return OpcUa_Good;
    else
    	return OpcUa_BadNodeAttributesInvalid;
}

UaStatus COnCreateObject::createObject(const UaNodeId &parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass nodeClass, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition)
{
    switch(typeDefinition.identifierNumeric()){
        case OpcUaId_FolderType:
            return createFolders(parentNodeId, browseName, nodeClass, nodeAttributes);
            break;
        case OpcRf6Id_Rf6AliasObject:
        case OpcRf6Id_Rf6AliasVariable:
            return createAlias(parentNodeId, browseName, nodeClass, nodeAttributes, typeDefinition);
            break;
        default:
        return OpcUa_BadTypeDefinitionInvalid;
    }

}

UaStatus COnCreateObject::createFolders(const UaNodeId &parentNodeId,
                                    const UaQualifiedName &browseName,
                                    OpcUa_NodeClass nodeClass,
                                    const UaExtensionObject &nodeAttributes)
{
	UaStatus ret(OpcUa_Bad);
    UaString folderId;
    QString str = Helper::UaStringToQString(parentNodeId.toString());

    if(str.contains("OpcRf6Project.")){
        return OpcUa_BadUserAccessDenied;
    }

    UaNode *parrentNode = mNodeManagerUaNode->getNode(parentNodeId);

    if (NULL != parrentNode){
        folderId = parentNodeId.toString() + "." + browseName.toString();
    }else{
        folderId = browseName.toString();
    }

    UaNodeId newNodeId = UaNodeId(folderId, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *foldersNode =  mNodeManagerUaNode->getNode(newNodeId);
    if( NULL == foldersNode){
         if (OpcUa_NodeClass_Object == nodeClass){
             if ( (nodeAttributes.encoding() != UaExtensionObject::EncodeableObject)
                ||(nodeAttributes.objectType()->TypeId != OpcUaId_ObjectAttributes)
                ||(nodeAttributes.object() == NULL) ){
                return OpcUa_BadNodeAttributesInvalid;
             }
             OpcUa_ObjectAttributes* objectAttributes = (OpcUa_ObjectAttributes*)nodeAttributes.object();
             OpcUa::FolderType * folder = new OpcUa::FolderType(newNodeId, browseName.toString(), mNodeManagerUaNode->getNameSpaceIndex(), mNodeManagerUaNode);

             folder->setDescription(objectAttributes->Description);
             folder->setDisplayName(objectAttributes->DisplayName);

             if (NULL != parrentNode){
                ret = mNodeManagerUaNode->addNodeAndReference(parentNodeId,folder,OpcUaId_Organizes);
             }else{
                ret = mNodeManagerUaNode->addNodeAndReference(OpcUaId_ObjectsFolder,folder,OpcUaId_Organizes);
             }

             // addition to database
			if (ret.isGood())
			{
			 	int res = addToDb(folder, parentNodeId, UaNodeId(OpcUaId_Organizes, 0));
			 	if (res != 0)
			 		debugLog.logToFile("COnCreateObject::createFolders error. addToDb() returns %d", res);
			}
			else
				debugLog.logToFile("COnCreateObject::createFolders. Error of addition %s", ret.toString().toUtf8());
         }
    }
    return ret;
}

int COnCreateObject::addToDb(const UaNode* obj, const UaNodeId &parentNodeId, const UaNodeId &refNodeId)
{
	if (mSqlDb ==NULL)
		return 1;

	if (mSqlDb->addObject(obj) != 0)
		return 2;

	if (mSqlDb->addReference(parentNodeId, refNodeId, obj->nodeId()) != 0)
		return 3;

	debugLog.logToFile("%s was added to sqlite database", obj->nodeId().toString().toUtf8());
	return 0;
}

int COnCreateObject::deleteObject(const UaNodeId &deleteNodeId)
{
    UaNode *deletNode =  mNodeManagerUaNode->getNode(deleteNodeId);
    if (NULL == deletNode){
       return OpcUa_BadNodeIdInvalid;
    }
    mNodeManagerUaNode->deleteUaNode(deletNode,OpcUa_True,OpcUa_True,OpcUa_True);
    return OpcUa_Good;
}

UaStatus COnCreateObject::createAlias(const UaNodeId &parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass /*nodeClass*/, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition)
{
    UaStatus ret;
    UaString aliasId;
    QString str = Helper::UaStringToQString(parentNodeId.toString());

    UaNode *parrentNode = mNodeManagerUaNode->getNode(parentNodeId);
    if (NULL != parrentNode){
        aliasId = parentNodeId.toString() + "." + browseName.toString();
    }else{
        aliasId = browseName.toString();
    }

    UaNodeId newNodeId = UaNodeId(aliasId, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode *aliasNode =  mNodeManagerUaNode->getNode(newNodeId);

    if( NULL == aliasNode){

        if(OpcRf6Id_Rf6AliasObject == typeDefinition.identifierNumeric()){
            ret = createAliasObject(parentNodeId, newNodeId, browseName, nodeAttributes);
        }else if(OpcRf6Id_Rf6AliasVariable == typeDefinition.identifierNumeric()){
            ret = createAliasVariable(parentNodeId, newNodeId, browseName, nodeAttributes);
        }
        return ret;
    }
    return OpcUa_BadTypeDefinitionInvalid;
}

UaStatus COnCreateObject::createAliasObject(const UaNodeId &parentNodeId, UaNodeId newNodeId, const UaQualifiedName &browseName, const UaExtensionObject &nodeAttributes)
{
    if ( (nodeAttributes.encoding() != UaExtensionObject::EncodeableObject)
         ||  (nodeAttributes.objectType()->TypeId != OpcUaId_ObjectAttributes)
         ||  (nodeAttributes.object() == NULL) ){
        return OpcUa_BadNodeAttributesInvalid;
    }

    OpcUa_ObjectAttributes* objectAttributes = (OpcUa_ObjectAttributes*)nodeAttributes.object();
    Rf6AliasObject *aliasObject = new Rf6AliasObject(newNodeId,
                                                     browseName.toString(),
                                                     mNodeManagerUaNode->getNameSpaceIndex(),
                                                     mNodeManagerUaNode
                                                     );
    aliasObject->setDisplayName(objectAttributes->DisplayName);
    aliasObject->setDescription(objectAttributes->Description);
    UaStatus ret = mNodeManagerUaNode->addNodeAndReference(parentNodeId,aliasObject,OpcUaId_Organizes);

    // addition to database
	if (ret.isGood())
	{
		int res = addToDb(aliasObject, parentNodeId, UaNodeId(OpcUaId_Organizes, 0));
		if (res != 0)
			debugLog.logToFile("COnCreateObject::createAliasObject error. addToDb() returns %d", res);
	}
	else
		debugLog.logToFile("COnCreateObject::createAliasObject. Error of addition %s", ret.toString().toUtf8());

	return ret;
}

UaStatus COnCreateObject::createAliasVariable(const UaNodeId &parentNodeId, UaNodeId newNodeId, const UaQualifiedName &browseName, const UaExtensionObject &nodeAttributes)
{
    if ( (nodeAttributes.encoding() != UaExtensionObject::EncodeableObject)
        ||  (nodeAttributes.objectType()->TypeId != OpcUaId_VariableAttributes)
        ||  (nodeAttributes.object() == NULL) ){
        return OpcUa_BadNodeAttributesInvalid;
    }

    OpcUa_VariableAttributes* variableAttributes = (OpcUa_VariableAttributes*)nodeAttributes.object();
    Rf6AliasVariable *aliasVariable = new Rf6AliasVariable(newNodeId,
                                                           browseName.toString(),
                                                           mNodeManagerUaNode->getNameSpaceIndex(),
                                                           UaVariant(30),
                                                           3,
                                                           mNodeManagerUaNode
                                                           );
    aliasVariable->setDisplayName(variableAttributes->DisplayName);
    aliasVariable->setDescription(variableAttributes->Description);
    UaStatus ret = mNodeManagerUaNode->addNodeAndReference(parentNodeId,aliasVariable,OpcUaId_Organizes);

    // addition to database
	if (ret.isGood())
	{
		int res = addToDb(aliasVariable, parentNodeId, UaNodeId(OpcUaId_Organizes, 0));
		if (res != 0)
			debugLog.logToFile("COnCreateObject::createAliasObject error. addToDb() returns %d", res);
	}
	else
		debugLog.logToFile("COnCreateObject::createAliasObject. Error of addition %s", ret.toString().toUtf8());

	return ret;
}

void COnCreateObject::setNodeManagerUaNode(NodeManagerUaNode *managerUaNode)
{
    mNodeManagerUaNode = managerUaNode;
}

UaStatus COnCreateObject::updateObject(OpcUa_WriteValue *writeNode)
{

    UaNode *node = mNodeManagerUaNode->getNode(UaNodeId(writeNode->NodeId));
    if (NULL == node ){
        return OpcUa_BadNodeIdUnknown;
    }
    //set handler for set description and dysplay name

    if (writeNode->AttributeId == OpcUa_Attributes_Description){
        UaLocalizedText text;
        UaVariant(writeNode->Value.Value).toLocalizedText(text);

        switch(node->typeDefinitionId().identifierNumeric())
        {
        case OpcUaId_FolderType:
            ((OpcUa::FolderType*)node)->setDescription(text);
            break;
        case OpcRf6Id_Rf6AliasObject:
            ((Rf6AliasObject*)node)->setDescription(text);
            break;
        case OpcRf6Id_Rf6AliasVariable:
            ((Rf6AliasVariable*)node)->setDescription(text);
            break;
        default:
            return OpcUa_BadTypeDefinitionInvalid;
        }
    }else if(writeNode->AttributeId == OpcUa_Attributes_DisplayName){
        UaLocalizedText text;
        UaVariant(writeNode->Value.Value).toLocalizedText(text);

        switch(node->typeDefinitionId().identifierNumeric())
        {
        case OpcUaId_FolderType:
            ((OpcUa::FolderType*)node)->setDisplayName(text);
            break;
        case OpcRf6Id_Rf6AliasObject:
            ((Rf6AliasObject*)node)->setDisplayName(text);
            break;
        case OpcRf6Id_Rf6AliasVariable:
            ((Rf6AliasVariable*)node)->setDisplayName(text);
            break;
        default:
            return OpcUa_BadTypeDefinitionInvalid;
        }
    } else if(writeNode->AttributeId == OpcUa_Attributes_Value){

        UaVariant value_(writeNode->Value.Value);
        UaDataValue dataValue;
        dataValue.setValue(value_, OpcUa_True, OpcUa_True);

        switch(node->typeDefinitionId().identifierNumeric())
        {

        case OpcRf6Id_Rf6AliasVariable:
            ((Rf6AliasVariable*)node)->setValue(NULL,dataValue ,OpcUa_False);
            break;
        default:
            return OpcUa_BadTypeDefinitionInvalid;
        }
    }
    if (mSqlDb != NULL)
    {
        int res = mSqlDb->updateObject(node);
        if (res != 0)
            debugLog.logToFile("COnCreateObject::updateObject. mSqlDb->updateObject() returns %d", res);
        else
            debugLog.logToFile("COnCreateObject::updateObject. Object %s was updated", node->nodeId().toFullString().toUtf8());
    }

    return OpcUa_Good;

}

