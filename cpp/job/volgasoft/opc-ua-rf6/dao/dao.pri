HEADERS += \
    dao/dao_order.h \
    dao/dao_accessdb.h \
    dao/opcobjectfactory.h \
    dao/createopcobject.h \
    dao/createpcu.h \
    dao/creatoranalogtag.h \
    dao/createstatustag.h \
    dao/creatormetertag.h \
    dao/creatorocptelemetryobject.h \
    dao/accessrf6telemetry.h \
    dao/writetelemetryfromopcobj.h \
    dao/opc_rf6_controls.h \
    dao/oncreateobject.h


SOURCES += \
    dao/dao_order.cpp \
    dao/dao_accessdb.cpp \
    dao/opcobjectfactory.cpp \
    dao/createopcobject.cpp \
    dao/createpcu.cpp \
    dao/creatoranalogtag.cpp \
    dao/createstatustag.cpp \
    dao/creatormetertag.cpp \
    dao/creatorocptelemetryobject.cpp \
    dao/accessrf6telemetry.cpp \
    dao/writetelemetryfromopcobj.cpp \
    dao/opc_rf6_controls.cpp \
    dao/oncreateobject.cpp

qnx {
HEADERS += \
    dao/dao_rf6order.h

SOURCES += \
    dao/dao_rf6order.cpp
INCLUDEPATH += ../Rf6
}
