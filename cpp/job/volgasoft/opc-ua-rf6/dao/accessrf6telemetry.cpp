#include "accessrf6telemetry.h"
#include "Rf6/rf6telemetryaccess.h"
#include "writetelemetryfromopcobj.h"
#include "creatorocptelemetryobject.h"
#include "Rf6/rf6loadtelemetryfile.h"
#include "Rf6/rf6uploadtelemetryfile.h"
#include "helper.h"

namespace OpcRf6 {
AccessRf6Telemetry::AccessRf6Telemetry(QObject *parent):
    QObject(parent)
{
    timer = new QTimer(this);
    timer->setInterval(600000);
    timer->start();
    connect(timer,SIGNAL(timeout()),this,SLOT(updateTelemetry()));
}

UaStatus AccessRf6Telemetry::createOrUpdateTelemetry(Rf6Telemetry *parent, ArrayTeleWidget *telemetry)
{
    Rf6::CreatorOcpTelemetryObject creator;
    creator.setNodeManagerUaNode(mNodeManagerUaNode);
    creator.setParentTelemetry(parent);
    creator.createAndUpdate(telemetry);
    return UaStatus();
}

UaStatus AccessRf6Telemetry::updatePcuTelemetry(int numberChanel, Rf6Pcu *pcuObj)
{
    ArrayTeleWidget * array = readTelemetryPcu(numberChanel,pcuObj->browseName().toString());
    if (NULL == array ){
      return OpcUa_BadFileNotFound;
    }
    UaStatus ret = createOrUpdateTelemetry(pcuObj->getpcuTelemetry(),array);
    delete array;
    return ret;
}

UaStatus AccessRf6Telemetry::updateChannelTelemetry(int numberChanel)
{
    ArrayTeleWidget * array = readTelemetryChannel(numberChanel);
    if (NULL == array ){
      return OpcUa_BadFileNotFound;
    }

    Rf6Channel * channel;
    UaStatus ret = getChannel(numberChanel,channel);
    if (OpcUa_Good == ret.statusCode() ){
        if (NULL == channel){

             return OpcUa_BadTypeDefinitionInvalid;
        }
        OpcRf6::Rf6Telemetry* telPar = channel->gettelemetry();
        ret = createOrUpdateTelemetry(telPar,array);

        return ret;
    }
    delete array;
    return ret;
}



void AccessRf6Telemetry::setNodeManagerUaNode(NodeManagerUaNode *mNodeManager)
{
    mNodeManagerUaNode = mNodeManager;
}

UaStatus AccessRf6Telemetry::createChannel(int numChanel, Rf6Pcu *pcuObj)
{

    Rf6Channel * channel;
    UaStatus ret = getChannel(numChanel,channel);
    if( OpcUa_BadNotFound == ret.statusCode()){
        //chanel not exist
        ArrayTeleWidget * array = readTelemetryChannel(numChanel);
        if (NULL == array){
            return OpcUa_BadFileNotFound;
        }
        //create channel
        UaString nameChannel = UaString("channel%1").arg(numChanel);
        UaNodeId requestNodeId = UaNodeId(nameChannel, mNodeManagerUaNode->getNameSpaceIndex());
        channel = new Rf6Channel(requestNodeId,nameChannel,mNodeManagerUaNode->getNameSpaceIndex(), mNodeManagerUaNode);
        channel->setnumber(numChanel);
        //link  in OpcRf6Project
        UaNodeId parentNodeId = UaNodeId("OpcRf6Project.Channels",mNodeManagerUaNode->getNameSpaceIndex());
        mNodeManagerUaNode->addNodeAndReference(parentNodeId, channel, OpcUaId_Organizes);
        createOrUpdateTelemetry(channel->gettelemetry(),array);
        //on auto update
        addPcuOnUpdate(pcuObj,numChanel);
    }  else if (OpcUa_BadTypeDefinitionInvalid ==  ret.statusCode() )  {
        //get channel
         return ret;
    }
    //link in pcu
    mNodeManagerUaNode->addUaReference(pcuObj, channel, OpcUaId_Organizes);
    return 0;
}

ArrayTeleWidget *AccessRf6Telemetry::readTelemetryChannel(int numChanel)
{
    ArrayTeleWidget * array =getTelemetryChannelFromFile(numChanel);
    CRf6LoadTelemetryFile loadTelemetry;
    if (NULL == array){
        return NULL;
    }
    loadTelemetry.loadTelemetryForChanel(numChanel,array);
    return array;
}

ArrayTeleWidget *AccessRf6Telemetry::readTelemetryPcu(int numChanel, UaString pcuName)
{
    Rf6TelemetryAccess Access;
    ArrayTeleWidget * array = Access.requestPcuTelemetry(numChanel);
    if (NULL == array){
        return NULL;
    }
    CRf6LoadTelemetryFile loadTelemetry;

    loadTelemetry.loadTelemetryForPcu(Helper::UaStringToQString(pcuName),array);
    return array;
}

ArrayTeleWidget *AccessRf6Telemetry::getTelemetryChannelFromFile(int numChanel)
{

    Rf6TelemetryAccess Access;

    return Access.requestChanel(numChanel);
}


void AccessRf6Telemetry::updateTelemetry()
{
   QList<Rf6Pcu*> listPcu = pcuListUpdate.keys();
   for(int i=0;i<listPcu.count();i++){
       Rf6Pcu* pcu = listPcu[i];
       if (NULL != pcu){
           int numchannel = pcuListUpdate[pcu];
           updateChannelTelemetry(numchannel);
           updatePcuTelemetry(numchannel,pcu);
       }
   }
}

void AccessRf6Telemetry::addPcuOnUpdate(Rf6Pcu *pcu, int numChannel)
{
    pcuListUpdate.insert(pcu,numChannel);
}



ArrayTeleWidget * AccessRf6Telemetry::getTelemetryPcuFromFile(int numChanel, Rf6Pcu *pcuObj)
{
    Rf6TelemetryAccess Access;

    return Access.requestPcuTelemetry(numChanel);
}

UaStatus AccessRf6Telemetry::getChannel(int numChanel,Rf6Channel* & channel)
{
    //request chanel
    UaString nameChannel = UaString("channel%1").arg(numChanel);
    UaNodeId requestNodeId = UaNodeId(nameChannel, mNodeManagerUaNode->getNameSpaceIndex());
    UaNode * nodeChannel = mNodeManagerUaNode->getNode(requestNodeId);
    if(NULL == nodeChannel){
        return OpcUa_BadNotFound;
    }
        //get channel
    channel =  dynamic_cast<Rf6Channel *>(nodeChannel);
    if (NULL == channel){
       return OpcUa_BadTypeDefinitionInvalid;
    }
    return OpcUa_Good;
}

UaStatus AccessRf6Telemetry::saveChannelTelemetry(int numChanel)
{
    //read structure
    ArrayTeleWidget * array = getTelemetryChannelFromFile(numChanel);
    if (NULL == array){
        return OpcUa_BadNotFound;
    }

    // read value for structure
    WriteTelemetryFromOpcObj readTelemetry;
    readTelemetry.setNodeManagerUaNode(mNodeManagerUaNode);
    Rf6Channel * channel;

    if ( getChannel(numChanel, channel).isBad() ){
        return OpcUa_BadNotWritable;
    }
    readTelemetry.setParentTelemetry(channel->gettelemetry());
    readTelemetry.rewriteTelemetry(array);
    //write in file
    Rf6UploadTelemetryFile uploader;
    uploader.SaveTelemetryForChanel(numChanel,array);
    delete array;
    updateTelemetry();
    return OpcUa_Good;
}

UaStatus AccessRf6Telemetry::savePcuTelemetry(int numChanel, Rf6Pcu *pcuObj)
{
    //read structure
    ArrayTeleWidget * array = getTelemetryPcuFromFile(numChanel, pcuObj);
    if (NULL == array){
        return OpcUa_BadNotFound;
    }

    // read value for structure
    WriteTelemetryFromOpcObj readTelemetry;
    readTelemetry.setNodeManagerUaNode(mNodeManagerUaNode);
    readTelemetry.setParentTelemetry(pcuObj->getpcuTelemetry());
    readTelemetry.rewriteTelemetry(array);
    //write in file

    Rf6UploadTelemetryFile uploader;
    uploader.SaveTelemetryForPcu(numChanel, Helper::UaStringToQString(pcuObj->browseName().toString()),array);
    delete array;
    updateTelemetry();
    return OpcUa_Good;
}


}
