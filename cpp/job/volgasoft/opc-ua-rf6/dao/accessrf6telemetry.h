#ifndef ACCESSRF6TELEMETRY_H
#define ACCESSRF6TELEMETRY_H
#include "QMap"
#include "QTimer"
#include <QObject>
#include "createopcobject.h"
#include "Rf6/rf6telemetryaccess.h"
#include "opcrf6_rf6pcu.h"
#include "helper.h"
#include "opcrf6_rf6channel.h"


namespace OpcRf6 {
class AccessRf6Telemetry :public QObject
{
    Q_OBJECT
public:
    AccessRf6Telemetry(QObject *parent = 0);
    UaStatus createOrUpdateTelemetry(OpcRf6::Rf6Telemetry *parent, ArrayTeleWidget *telemetry);
    UaStatus updatePcuTelemetry(int numberChanel, Rf6Pcu *pcuObj);
    UaStatus updateChannelTelemetry(int numberChanel);

    void setNodeManagerUaNode( NodeManagerUaNode *mNodeManager);
    UaStatus createChannel(int numChanel, Rf6Pcu * pcuObj);
    ArrayTeleWidget * readTelemetryChannel(int numChanel);
    ArrayTeleWidget * readTelemetryPcu(int numChanel,UaString pcuName);

    ArrayTeleWidget * getTelemetryChannelFromFile(int numChanel);
    ArrayTeleWidget * getTelemetryPcuFromFile(int numChanel, Rf6Pcu * pcuObj);
    UaStatus getChannel(int numChanel, OpcRf6::Rf6Channel* &channel);
    UaStatus saveChannelTelemetry(int numChanel);
    UaStatus savePcuTelemetry(int numChanel,Rf6Pcu * pcuObj);

private slots:
    void updateTelemetry();

private:
    void addPcuOnUpdate(Rf6Pcu* pcu,int numChannel);
    NodeManagerUaNode *mNodeManagerUaNode;
    QTimer *timer;
    QMap<Rf6Pcu*,int> pcuListUpdate;
};
}

#endif // ACCESSRF6TELEMETRY_H
