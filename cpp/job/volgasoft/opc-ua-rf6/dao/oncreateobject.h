#ifndef ONCREATEOBJECT_H
#define ONCREATEOBJECT_H

#include "dao_order.h"
#include "nodemanagerbase.h"
#include "nodemanageruanode.h"
#include <QObject>

#include "opcsqlitedatabase.h"

class COnCreateObject
{
public:
    COnCreateObject(NodeManagerUaNode *nodeManager, COpcSqliteDatabase *sqlDB) : mNodeManagerUaNode(nodeManager), mSqlDb(sqlDB) {}
    COnCreateObject() : mNodeManagerUaNode(NULL), mSqlDb(NULL) {}

    UaStatus createObject(const UaNodeId &parentNodeId,
    					  const UaNodeId &referenceTypeId,
    					  const UaNodeId &requestNodeId,
    					  const UaQualifiedName &browseName,
                          OpcUa_NodeClass nodeClass,
                          const UaExtensionObject &nodeAttributes,
                          const UaNodeId &typeDefinition,
                          UaNodeId &addedNodeId);


    UaStatus createObject(const UaNodeId &parentNodeId,
                          const UaQualifiedName &browseName,
                          OpcUa_NodeClass nodeClass,
                          const UaExtensionObject &nodeAttributes,
                          const UaNodeId &typeDefinition);
    UaStatus createFolders(const UaNodeId &parentNodeId,
                           const UaQualifiedName &browseName,
                           OpcUa_NodeClass nodeClass,
                           const UaExtensionObject &nodeAttributes);
    int deleteObject(const UaNodeId& deleteNodeId);
    UaStatus createAlias(const UaNodeId &parentNodeId,
                         const UaQualifiedName &browseName,
                         OpcUa_NodeClass nodeClass,
                         const UaExtensionObject &nodeAttributes,
                         const UaNodeId &typeDefinition);
    UaStatus createAliasObject(const UaNodeId &parentNodeId,
                               UaNodeId newNodeId,
                               const UaQualifiedName &browseName,
                               const UaExtensionObject &nodeAttributes);
    UaStatus createAliasVariable(const UaNodeId &parentNodeId,
                                 UaNodeId newNodeId,
                                 const UaQualifiedName &browseName,
                                 const UaExtensionObject &nodeAttributes);
    void setNodeManagerUaNode(NodeManagerUaNode *managerUaNode);
    UaStatus updateObject(OpcUa_WriteValue *writeNode);

    void setSqlDbManager(COpcSqliteDatabase *sqlDB) {mSqlDb = sqlDB;}

protected:

    NodeManagerUaNode *mNodeManagerUaNode;

    COpcSqliteDatabase *mSqlDb;
    int addToDb(const UaNode* obj, const UaNodeId &parentNodeId, const UaNodeId &refNodeId);	// save object with its parent reference to database
};

#endif // ONCREATEOBJECT_H
