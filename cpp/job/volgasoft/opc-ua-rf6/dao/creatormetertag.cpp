#include "creatormetertag.h"
#include "opcrf6_rf6metertag.h".h"
#include "helper.h"
namespace OpcRf6 {
CreatorMeterTag::CreatorMeterTag()
{
}

UaNode *CreatorMeterTag::createAndUpdate(ULONG_64 parent, pCRf6DbObject ob)
{
    //convert id
    QString QStrId = QString::number(ob->GetIndex(),16);
    UaString UaStrId = Helper::QStringToUaString(QStrId);
    //create node id
    UaNodeId requestNodeId = UaNodeId(UaStrId,mNodeManagerUaNode->getNameSpaceIndex());
    //request node
    UaNode *opcNode = mNodeManagerUaNode->getNode(requestNodeId);
    OpcRf6::Rf6MeterTag *meter ;
    if (NULL == opcNode ){
        //node not exist
        //node name
        UaString nodeName =Helper::QStringToUaString(ob->GetName());
        //convert id parent
        QString QStrIdParent = QString::number(parent,16);
        UaString UaStrIdParent = Helper::QStringToUaString(QStrIdParent);
        // parent node id
        UaNodeId parentNodeId = UaNodeId(UaStrIdParent,mNodeManagerUaNode->getNameSpaceIndex());
        //get parent
        UaNode *parentNode = mNodeManagerUaNode->getNode(parentNodeId);

        if (NULL == parentNode ){
            return NULL; //parent node not exists
        }
        if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
             return NULL; //parent node not exists
        }
        meter = new OpcRf6::Rf6MeterTag(requestNodeId,nodeName,mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);

        //add reference
        mNodeManagerUaNode->addNodeAndReference(parentNodeId,meter,OpcUaId_Organizes);
    } else {
        //node exist
        meter  = dynamic_cast<OpcRf6::Rf6MeterTag *>(opcNode);
        if (NULL == meter){
            return NULL;
        }
    }
     if (NULL != ob->PtrMetData()){
         meter->fromRf6Structure(*ob->PtrMetData());
     }

     CMemObject *pMO = ob->GetRdMemObject(*mDatabase);
      if (pMO != NULL)
		 meter->setDescription(UaLocalizedText(UaString("en"), UaString(ob->GetRdMemObject(*mDatabase)->GetDesc())));
         meter->setengineeringUnits(UaString(ob->getUnits(*mDatabase)));

	  return meter;
}
}
