#ifndef CREATORMETERTAG_H
#define CREATORMETERTAG_H
#include "createopcobject.h"
namespace OpcRf6 {
class CreatorMeterTag : public CreateOpcObject
{
public:
    CreatorMeterTag();
    virtual UaNode*createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);
};
}
#endif // CREATORMETERTAG_H
