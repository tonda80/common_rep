
#include "createpcu.h"

#include "Rf6/rf6telemetryaccess.h"
#include "opcrf6_rf6channel.h"
#include "creatorocptelemetryobject.h"
#include "Rf6/rf6loadtelemetryfile.h"
#include "helper.h"
namespace OpcRf6 {
CreatorPcu::CreatorPcu()
{
    mAccessRf6Telemetry = new AccessRf6Telemetry;
    mControlManager = new COpcRf6ControlManager;
}

UaNode * CreatorPcu::createAndUpdate(ULONG_64 parent, pCRf6DbObject ob)
{
	//convert id
    QString QStrId = QString::number(ob->GetIndex(),16);
    UaString UaStrId = Helper::QStringToUaString(QStrId);
    //create node id
    UaNodeId requestNodeId = UaNodeId(UaStrId,mNodeManagerUaNode->getNameSpaceIndex());
    //request node
    UaNode *opcNode = mNodeManagerUaNode->getNode(requestNodeId);
    OpcRf6::Rf6Pcu *pcu ;
    if (NULL == opcNode ){
        //node not exist
        //node name
        UaString nodeName =Helper::QStringToUaString(ob->GetName());
        //convert id parent
       // QString QStrIdParent = QString::number(parent,16);
        //UaString UaStrIdParent = Helper::QStringToUaString(QStrIdParent);
        // parent node id
        UaNodeId parentNodeId = UaNodeId("OpcRf6Project.PCUs",mNodeManagerUaNode->getNameSpaceIndex());
        UaNode *parentNode = mNodeManagerUaNode->getNode(parentNodeId);
        if (NULL == parentNode ){
            return NULL; //parent node not exists
        }
        pcu = new OpcRf6::Rf6Pcu(requestNodeId,nodeName,mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);
        pcu->setControlManager(mControlManager);
        //add reference
        mNodeManagerUaNode->addNodeAndReference(parentNodeId,pcu,OpcUaId_Organizes);
        if (NULL != ob->PtrPcuData()){
            mAccessRf6Telemetry->createChannel(ob->PtrPcuData()->cs.port, pcu);
            mAccessRf6Telemetry->updatePcuTelemetry(ob->PtrPcuData()->cs.port,pcu);
           // updatePcuTelemetry(ob->PtrPcuData()->cs.port,pcu);
        }

    } else {
    	//node exist
        pcu  = dynamic_cast<OpcRf6::Rf6Pcu *>(opcNode);
        if (NULL == pcu){
            return NULL;
        }

        if( ob->PtrPcuData()->cs.port != pcu->getcommunicationStatistics()->getport()){
        	debugLog.logToFile("CreatorPcu::createAndUpdate. Update of telemetry");

			pcu->getpcuTelemetry()->getfields()->deleteAllChilren(mNodeManagerUaNode);
		   UaReferenceLists* referenceLists = pcu->getUaReferenceLists();
		   UaReference * ref = (UaReference *) referenceLists->pTargetNodes();
		   while (NULL != ref){
				UaString str = ref->pTargetNode()->getKey().toString();
				QString qstr = Helper::UaStringToQString(str);
				if (qstr.contains("channel")){
					mNodeManagerUaNode->deleteUaReference(pcu->getKey(),ref->pTargetNode()->getKey(),OpcUaId_Organizes);
				}
				ref = ref->pNextForwardReference();
			 }
			int numChannel = ob->PtrPcuData()->cs.port;
			mAccessRf6Telemetry->createChannel(numChannel,pcu);
			mAccessRf6Telemetry->updateChannelTelemetry(numChannel);
			mAccessRf6Telemetry->updatePcuTelemetry(numChannel,pcu);
		}
    }
     if (NULL != ob->PtrPcuData()){

         pcu->fromRf6Structure(*ob->PtrPcuData());

     }

     CMemObject *pMO = ob->GetRdMemObject(*mDatabase);
     if (pMO != NULL)
    	 pcu->setDescription(UaLocalizedText(UaString("en"), UaString(pMO->GetDesc())));

     return pcu;
}

void CreatorPcu::setAccessRf6Telemetry(AccessRf6Telemetry *accessTelemetry)
{
    mAccessRf6Telemetry = accessTelemetry;
}

}
