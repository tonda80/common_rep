#include "creatoranalogtag.h"
#include "helper.h"


namespace OpcRf6 {
CreatorAnalogTag::CreatorAnalogTag()
{
}

UaNode *CreatorAnalogTag::createAndUpdate(ULONG_64 parent, pCRf6DbObject ob)
{
    //convert id
    QString QStrId = QString::number(ob->GetIndex(),16);
    UaString UaStrId = Helper::QStringToUaString(QStrId);
    //create node id
    UaNodeId requestNodeId = UaNodeId(UaStrId,mNodeManagerUaNode->getNameSpaceIndex());
    //request node
    UaNode *opcNode = mNodeManagerUaNode->getNode(requestNodeId);
    OpcRf6::Rf6AnalogTag *Analog;
    if (NULL == opcNode ){
        //node not exist
        //node name
        UaString nodeName =Helper::QStringToUaString(ob->GetName());
        //convert id parent
        QString QStrIdParent = QString::number(parent,16);
        UaString UaStrIdParent = Helper::QStringToUaString(QStrIdParent);
        // parent node id
        UaNodeId parentNodeId = UaNodeId(UaStrIdParent,mNodeManagerUaNode->getNameSpaceIndex());
        //get parent
        UaNode *parentNode = mNodeManagerUaNode->getNode(parentNodeId);

        if (NULL == parentNode ){
            return NULL; //parent node not exists
        }
        if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
             return NULL; //parent node not exists
        }

        Analog = new OpcRf6::Rf6AnalogTag(requestNodeId,nodeName,mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);

        //add reference
         mNodeManagerUaNode->addNodeAndReference(parentNodeId,Analog,OpcUaId_Organizes);
    } else {
        //node exist
        Analog  = dynamic_cast<OpcRf6::Rf6AnalogTag *>(opcNode);
        if (NULL == Analog){
            return NULL;
        }

    }
     if (NULL != ob->PtrAnaData()){
         Analog->fromRf6Structure(*ob->PtrAnaData());
     }
      //create analog limit Hi
     createAndUpdateAnalogLimits(UaString("limitHi"),Analog,ob,_RF_DB_ALARM_H);


     //create analog limit HiHi
     createAndUpdateAnalogLimits( UaString("limitHiHi"),Analog,ob,_RF_DB_ALARM_HH);


     //create analog limit Lo
     createAndUpdateAnalogLimits(UaString("limitLo"),Analog,ob,_RF_DB_ALARM_L);


     //create analog limit LoLo
      createAndUpdateAnalogLimits(UaString("limitLoLo"),Analog,ob,_RF_DB_ALARM_LL);

      CMemObject *pMO = ob->GetRdMemObject(*mDatabase);
	   if (pMO != NULL)
		 Analog->setDescription(UaLocalizedText(UaString("en"), UaString(ob->GetRdMemObject(*mDatabase)->GetDesc())));
       const  char * units = NULL;
       units = ob->getUnits(*mDatabase);
       if (NULL != units){
           Analog->setEngineeringUnits(UaString(units));
       }

     return Analog;
}

void CreatorAnalogTag::createAndUpdateAnalogLimits(UaString limitName,OpcRf6::Rf6AnalogTag *AnalogTag, pCRf6DbObject ob,int type)
{
   UaNode *AnalogLimitNode = mNodeManagerUaNode->getNode(UaNodeId(AnalogTag->getKey().toString()+"."+limitName,mNodeManagerUaNode->getNameSpaceIndex()));
   OpcRf6::Rf6AnalogLimit *AnalogLimit;
   if (NULL  == AnalogLimitNode) {
       //node not exist
       //create node
      AnalogLimit =  new OpcRf6::Rf6AnalogLimit(
                             UaNodeId(AnalogTag->getKey().toString()+"."+limitName,mNodeManagerUaNode->getNameSpaceIndex()),
                             limitName,
                             mNodeManagerUaNode->getNameSpaceIndex(),
                             UaVariant(),
                             3,
                             mNodeManagerUaNode);
      //add node in address space
       mNodeManagerUaNode->addNodeAndReference(AnalogTag->getalarm(),AnalogLimit,OpcUaId_Organizes);
   } else {
       //node exist
       AnalogLimit  = dynamic_cast<OpcRf6::Rf6AnalogLimit *>(AnalogLimitNode);
   }
   if (NULL != AnalogLimit) {
       //update value
       AnalogLimit->fromRf6Structure(*ob->GetAlarmLimit(*mDatabase,type));
   }
}

}
