#ifndef OPC_RF6_CONTROLS_H
#define OPC_RF6_CONTROLS_H

#include <QObject>

#include "Rf6/rf6controls.h"
#include "nodemanagerbase.h"
#include "nodemanageruanode.h"
#include "opcrf6_identifiers.h"
#include "opcrf6_datatypes.h"


namespace OpcRf6
{
	class COpcRf6ControlManager : public Rf6::CRf6ControlsManager
	{
		Q_OBJECT

	public:
		// The function checks whether the write value is a control
		bool writeValueIsControl(OpcUa_WriteValue *writeNode);
		UaStatus sendControl(OpcUa_WriteValue *writeNode);

	private:
		int fillAnalogControl(OpcUa_WriteValue *writeNode, cntl_val_t* controlValue);
		int fillStatusControl(OpcUa_WriteValue *writeNode, cntl_val_t* controlValue);

	};

}

#endif
