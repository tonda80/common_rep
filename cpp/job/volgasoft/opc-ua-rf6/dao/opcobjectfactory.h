#ifndef OPCOBJECTFACTORY_H
#define OPCOBJECTFACTORY_H
#include "QMap"
#include "createopcobject.h"
#include "Rf6/rf6database.h"
#include "Rf6/rf6dbobject.h"
#include "nodemanageruanode.h"

using namespace Rf6;

typedef QSharedPointer<CRf6DbObject> pCRf6DbObject;


namespace OpcRf6 {
class OpcObjectFactory
{

public:
    explicit OpcObjectFactory();
    UaNode *createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);
    void setNodeManagerUaNode( NodeManagerUaNode *mNodeManager);
    void setDatabase(CRf6Database *database);
    void addCreator(int type,CreateOpcObject *creator);


    

private:
    QMap<int,CreateOpcObject*> mCreators;
    CRf6Database *mDatabase;
    NodeManagerUaNode *mNodeManagerUaNode;
    
};

#endif // OPCOBJECTFACTORY_H
}
