#include "opc_rf6_controls.h"
#include "opc_utilities/helper.h"

namespace OpcRf6
{
	bool COpcRf6ControlManager::writeValueIsControl(OpcUa_WriteValue *writeNode)
	{
		UaString sNodeID = UaNodeId(writeNode->NodeId).toString();
		if (endsWith(sNodeID, ".value"))
		{
			debugLog.logToFile("Received control for %s", sNodeID.toUtf8());
			return true;
		}
		else
			return false;
	}

	UaStatus COpcRf6ControlManager::sendControl(OpcUa_WriteValue *writeNode)
	{
		UaString sNodeID = UaNodeId(writeNode->NodeId).toString();

		ULONG_64 parent_int = uastringToUlong64(sNodeID, 16);
		if (parent_int == 0)
		{
			debugLog.logToFile("ERROR. Node %s not found", sNodeID.toUtf8());
			return OpcUa_Bad;
		}

		cntl_val_t controlValue;
		int ret;

		switch (Rf6::GetDbType(parent_int))
		{
		case Rf6::RF6_ANALOG:
			ret = fillAnalogControl(writeNode, &controlValue);
			break;
		case Rf6::RF6_STATUS:
			ret = fillStatusControl(writeNode, &controlValue);
			break;
		default:
			debugLog.logToFile("WARNING. COpcRf6ControlManager::writeValueIsControl() don't support the control for the tag type %d", Rf6::GetDbType(parent_int));
			return OpcUa_BadNotImplemented;
		}

		if (ret != 0)
		{
			debugLog.logToFile("ERROR. fill__Control returns %d", ret);
			return OpcUa_Bad;
		}

		debugLog.logToFile("Sending of SETPOINT_CONTROL to %lld", parent_int);
		ret = Rf6::CRf6ControlsManager::sendControl(parent_int, SETPOINT_CONTROL, &controlValue);	//

		if (ret != 0)
		{
			debugLog.logToFile("ERROR. Rf6::CRf6ControlsManager::sendControl() returns %d", ret);
			return OpcUa_Bad;
		}

		return OpcUa_Good;
	}

	int COpcRf6ControlManager::fillAnalogControl(OpcUa_WriteValue *writeNode, cntl_val_t* controlValue)
	{
		OpcUa_Variant &value = writeNode->Value.Value;

		if (value.Datatype != OpcUaId_Double)
		{
			debugLog.logToFile("ERROR. fillAnalogControl has Datatype %d", value.Datatype);
			return 1;
		}

		controlValue->data_type = 'F';
		controlValue->un.dval = value.Value.Double;

		return 0;
	}

	int COpcRf6ControlManager::fillStatusControl(OpcUa_WriteValue *writeNode, cntl_val_t* controlValue)
	{
		OpcUa_Variant &value = writeNode->Value.Value;

		if (value.Datatype != OpcUaId_Byte)
		{
			debugLog.logToFile("ERROR. fillStatusControl has Datatype %d", value.Datatype);
			return 1;
		}

		controlValue->data_type = 'C';
		controlValue->un.cval = value.Value.Byte;

		return 0;
	}
}
