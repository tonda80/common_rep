#ifndef DAO_RF6ORDER_H
#define DAO_RF6ORDER_H
#include "dao_order.h"
#include "Rf6/rf6database.h"
#include "Rf6/rf6rtsubscriber.h"
#include "nodemanageruanode.h"
#include "opcobjectfactory.h"
#include "Rf6/rf6eventsubscriber.h"
#include "opcrf6_rf6analogtag.h"
#include "accessrf6telemetry.h"
using namespace  Rf6;
class dao_Rf6Order :
        public dao_Order
{
	Q_OBJECT
public:
    dao_Rf6Order();
    virtual void requestAllDB();

    virtual int deleteObject(const UaNodeId& deleteNodeId);
    virtual UaStatus updateObject(UaNodeId &NodeId);
    UaStatus updateAttributes(UaNodeId &NodeId, UaLocalizedText &nodeAtrib);
    virtual UaStatus updateAttributes(OpcUa_WriteValue *writeNode);
    virtual UaStatus createObject(const UaNodeId &parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass nodeClass, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition, UaNodeId &addedNodeId);
    UaStatus createUaNode(  const UaNodeId& parentNodeId,
                            const UaNodeId &referenceTypeId,
                            const UaNodeId& requestedNewNodeId,
                            const UaQualifiedName &browseName,
                            const UaNodeId &typeDefinition);    
    void createAndUpdateAnalogLimits(UaString limitName, OpcRf6::Rf6AnalogTag *AnalogTag, CRf6DbObject &ob, int type);

public slots:
    void recivedRf6Object(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob);
    void recivedRealtime(QSharedPointer<CRf6DbObject> ob);
    void recivedUpdateRf6object(QSharedPointer<CRf6DbObject> ob);
    void deleteOpcObject(ULONG_64 uid);
    void startEventSubscribe();
    void UpdateComplete(int errcode, ULONG_64 uind);

private:
    UaStatus updateRf6StatusTag(UaNode *opcNode, CRf6DbObject &ob);
    Rf6::CRf6Database *mDatabase;
    Rf6::CRf6RtSubscriber *mSubscribe;
    OpcRf6::OpcObjectFactory *factory;
    Rf6::CRf6EventSubscriber *mEventSubscribe;
    OpcRf6::AccessRf6Telemetry * mAccessRf6Telemetry;

};

#endif // DAO_RF6ORDER_H
