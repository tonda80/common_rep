#include "dao_accessdb.h"
using namespace dao;
dao_AccessDb::dao_AccessDb(QObject *parent) :
    QObject(parent)
{
}

void dao_AccessDb::setOrderDAO(dao_Order *order)
{
    mOrder = order;
    connect(mOrder,SIGNAL(opcObject(UaNodeId&,UaReferenceLists*,UaNodeId&)),this,SIGNAL(opcObject(UaNodeId&,UaReferenceLists*,UaNodeId&)));
    //translation signal finish download db
    connect(mOrder,SIGNAL(finishDownload()),this,SIGNAL(finishDownload()));
}

dao_Order *dao_AccessDb::getOrder()
{
    return mOrder;
}

void dao_AccessDb::requestAllDB()
{
    mOrder->requestAllDB();
}
