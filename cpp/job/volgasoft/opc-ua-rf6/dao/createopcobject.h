#ifndef CREATEOPCOBJECT_H
#define CREATEOPCOBJECT_H


#include "Rf6/rf6database.h"
#include "Rf6/rf6dbobject.h"
#include "nodemanageruanode.h"
using namespace Rf6;
typedef QSharedPointer<CRf6DbObject> pCRf6DbObject;
namespace OpcRf6 {
class CreateOpcObject
{

public:
    CreateOpcObject();
    virtual UaNode * createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);
    virtual void setNodeManagerUaNode( NodeManagerUaNode *mNodeManager);
    void setDatabase(CRf6Database *database);

protected:
     CRf6Database *mDatabase;
     NodeManagerUaNode *mNodeManagerUaNode;


};
}
#endif // CREATEOPCOBJECT_H
