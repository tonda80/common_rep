#ifndef CREATEPCU_H
#define CREATEPCU_H
#include "createopcobject.h"
#include "accessrf6telemetry.h"
#include "dao/opc_rf6_controls.h"
#include "opcrf6_rf6pcu.h"


namespace OpcRf6 {

class CreatorPcu : public CreateOpcObject
{
public:
    CreatorPcu();
    virtual UaNode * createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);
    virtual void setAccessRf6Telemetry(AccessRf6Telemetry * accessTelemetry);


private:
    AccessRf6Telemetry *mAccessRf6Telemetry;
    COpcRf6ControlManager *mControlManager;

};
}
#endif // CREATEPCU_H
