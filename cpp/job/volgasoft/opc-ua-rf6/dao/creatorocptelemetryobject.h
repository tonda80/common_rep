#ifndef CREATOROCPTELEMETRYOBJECT_H
#define CREATOROCPTELEMETRYOBJECT_H
#include "Rf6/rf6telemetryaccess.h"
#include "opcrf6_rf6telemetry.h"
#include "nodemanageruanode.h"

namespace Rf6{
class CreatorOcpTelemetryObject
{
public:
    CreatorOcpTelemetryObject();
    OpcRf6::Rf6Telemetry * createAndUpdate(ArrayTeleWidget *telemetry);
    void setNodeManagerUaNode( NodeManagerUaNode *mNodeManager);
    void setParentTelemetry( OpcRf6::Rf6Telemetry *parent);



private:
    OpcRf6::Rf6Telemetry *parentTelemetry;
    NodeManagerUaNode *mNodeManagerUaNode;
    OpcUa::FolderType* folderParent;
    UaNode * createField(int numberchanel,TelWidgetPtr_t *feild);
     UaNode * createFieldText(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldNumeric(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldList(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldRadio(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldCheck(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldSkip(int numberchanel,TelWidgetPtr_t *feild) ;
     UaNode *createFieldDate(int numberchanel, TelWidgetPtr_t *feild);
     UaNode *createFieldTime(int numberchanel, TelWidgetPtr_t *feild);
     UaNode *createFieldGroupS(int numberchanel,TelWidgetPtr_t *feild);
     UaNode *createFieldGroupE(int numberchanel, TelWidgetPtr_t *feild);
};
}
#endif // CREATOROCPTELEMETRYOBJECT_H
