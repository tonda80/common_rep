#ifndef DAO_ACCESSDB_H
#define DAO_ACCESSDB_H

#include <QObject>
#include "dao_order.h"
#include <uanodeid.h>
#include "opcua_baseobjecttype.h"

namespace dao{
class dao_AccessDb : public QObject
{
    Q_OBJECT
public:
    explicit dao_AccessDb(QObject *parent = 0);
    void setOrderDAO(dao_Order *order);
    void deleteObject(UaNodeId& deleteNodeId);
    dao_Order* getOrder();
signals:
    void finishDownload();
    void opcObject(UaNodeId& parentNodeId,UaReferenceLists *opcObj, UaNodeId&  referenceTypeId);

public slots:
    void requestAllDB();




private:
    dao_Order *mOrder;
};
}
#endif // DAO_ACCESSDB_H
