#ifndef CREATORANALOGTAG_H
#define CREATORANALOGTAG_H
#include "createopcobject.h"
#include "Rf6/rf6dbobject.h"
#include "opcrf6_rf6analogtag.h"
namespace OpcRf6 {
class CreatorAnalogTag : public CreateOpcObject
{
public:
    CreatorAnalogTag();
    virtual UaNode *createAndUpdate(ULONG_64 parent, pCRf6DbObject ob);
    void createAndUpdateAnalogLimits(UaString limitName,OpcRf6::Rf6AnalogTag *AnalogTag,pCRf6DbObject ob, int type);

};
}
#endif // CREATORANALOGTAG_H
