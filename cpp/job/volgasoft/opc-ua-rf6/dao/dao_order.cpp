#include "dao_order.h"

dao_Order::dao_Order(QObject *parent) :
    QObject(parent)
{
}

void dao_Order::requestAllDB()
{
}

void dao_Order::setNamespaceIndex(qint16 index)
{
    namespaceIndex = index;
}

int dao_Order::deleteObject(const UaNodeId &deleteNodeId)
{
    return OpcUa_BadNotImplemented;
}

void dao_Order::setNodeManagerUaNode(NodeManagerUaNode *managerUaNode)
{
    mNodeManagerUaNode  = managerUaNode;

}

UaStatus dao_Order::createObject(const UaNodeId &parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass nodeClass, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition, UaNodeId &addedNodeId)
{
    return OpcUa_BadNotImplemented;
}

void dao_Order::setNodeManagerConfig(NodeManagerConfig *config)
{
    mNodeManagerConfig = config;
}

UaStatus dao_Order::updateAttributes(UaNodeId& NodeId, UaLocalizedText &nodeAtrib)
{
    return OpcUa_BadNotImplemented;
}

UaStatus dao_Order::updateAttributes(OpcUa_WriteValue *writeNode)
{
    return OpcUa_BadNotImplemented;
}

UaStatus dao_Order::updateObject(UaNodeId& NodeId)
{
    return OpcUa_BadNotImplemented;
}
