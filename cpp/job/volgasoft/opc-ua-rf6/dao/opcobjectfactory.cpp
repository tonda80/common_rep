#include "opcobjectfactory.h"

namespace OpcRf6 {
OpcObjectFactory::OpcObjectFactory()
{

}

UaNode *OpcObjectFactory::createAndUpdate(ULONG_64 parent, pCRf6DbObject ob)
{

    if (!mCreators.contains(ob->GetDbType())){
        return NULL;
    }
   return mCreators[ob->GetDbType()]->createAndUpdate(parent,ob);


}

void OpcObjectFactory::setNodeManagerUaNode(NodeManagerUaNode *mNodeManager)
{
    mNodeManagerUaNode = mNodeManager;
}

void OpcObjectFactory::setDatabase(CRf6Database *database)
{
    mDatabase = database;
}

void OpcObjectFactory::addCreator(int type, CreateOpcObject * creator)
{
    //add new creator
    creator->setDatabase(mDatabase);
    creator->setNodeManagerUaNode(mNodeManagerUaNode);
    mCreators.insert(type,creator);
}
}
