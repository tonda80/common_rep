#include "dao_rf6order.h"

#include "opcrf6_rf6pcu.h"

#include "opcrf6_opcrf6object.h"
#include "opcrf6_rf6analoglimit.h"
#include "opcrf6_rf6analogtag.h"
#include "opcrf6_rf6metertag.h"
#include "opcrf6_rf6pcu.h"
#include "opcrf6_rf6roc.h"
#include "opcrf6_rf6statusstate.h"
#include "opcrf6_rf6statustag.h"
#include "opcrf6_rf6tanktag.h"
#include "opcrf6_rf6project.h"
#include "createpcu.h"
#include "creatoranalogtag.h"
#include "createstatustag.h"
#include "creatormetertag.h"
#include "opcrf6_rf6channel.h"
#include "opcrf6_rf6aliasvariable.h"
#include <QStringList>

#include "helper.h"
using namespace Rf6;
using namespace OpcUa;
dao_Rf6Order::dao_Rf6Order()
{

    mDatabase  =new Rf6::CRf6Database;
    mSubscribe = new Rf6::CRf6RtSubscriber();
    connect(mSubscribe,SIGNAL(rtUpdate(QSharedPointer<CRf6DbObject>)),this,SLOT(recivedRealtime(QSharedPointer<CRf6DbObject>)));
    connect(mDatabase,SIGNAL(GiveObject(ULONG_64,QSharedPointer<CRf6DbObject>)),this,SLOT(recivedRf6Object(ULONG_64,QSharedPointer<CRf6DbObject>)));
    mEventSubscribe = new Rf6::CRf6EventSubscriber(Rf6::eventDatabase);
    connect(mEventSubscribe,SIGNAL(tagAdded(ULONG_64,QSharedPointer<CRf6DbObject>)),this,SLOT(recivedRf6Object(ULONG_64,QSharedPointer<CRf6DbObject>)));
    connect(mEventSubscribe,SIGNAL(tagDeleted(ULONG_64)),this,SLOT(deleteOpcObject(ULONG_64)));
    connect(mEventSubscribe,SIGNAL(tagUpdated(QSharedPointer<CRf6DbObject>)),this,SLOT(recivedUpdateRf6object(QSharedPointer<CRf6DbObject>)));
    connect(mDatabase,SIGNAL(UpdateComplete(int,ULONG_64)),this,SLOT(UpdateComplete(int,ULONG_64)));
    mAccessRf6Telemetry = new OpcRf6::AccessRf6Telemetry;

    mSubscribe->start();

}

void dao_Rf6Order::requestAllDB()
{

    //initialization factory
    factory = new OpcRf6::OpcObjectFactory();
    factory->setDatabase(mDatabase);
    factory->setNodeManagerUaNode(mNodeManagerUaNode);

    mAccessRf6Telemetry->setNodeManagerUaNode(mNodeManagerUaNode);
    OpcRf6::CreatorPcu *creatorPcu = new OpcRf6::CreatorPcu();
    creatorPcu->setAccessRf6Telemetry(mAccessRf6Telemetry);
    factory->addCreator(NPCU_DB,creatorPcu);

    factory->addCreator(NANALOG_DB,new OpcRf6::CreatorAnalogTag());
    factory->addCreator(NSTATUS_DB,new OpcRf6::CreateStatusTag());
    factory->addCreator(NMETER_DB,new OpcRf6::CreatorMeterTag());
    //get database
    mDatabase->GetObjects();
    mEventSubscribe->start();

}


int dao_Rf6Order::deleteObject(const UaNodeId &deleteNodeId)
{

    if (deleteNodeId.identifierType() == OpcUa_IdentifierType_Numeric  ) {
        return 1;
    }
    UaNode * deleteNode = mNodeManagerUaNode->getNode(deleteNodeId);
    if (NULL == deleteNode){
        return 1;
    }

    ULONG_64 uid = QString(deleteNodeId.toString().toUtf8()).toLongLong(0,16);

    int ret =  mDatabase->DeleteDbObject(uid);
    if (ret == 0){
        return 0; //succes
    }
    return OpcUa_BadNodeIdInvalid;
}
UaStatus dao_Rf6Order::updateRf6StatusTag(UaNode *opcNode,CRf6DbObject &ob)
{
    if (NULL == opcNode){
        return OpcUa_BadNoData;
    }
    OpcRf6::Rf6StatusTag *opcRf6Status = dynamic_cast<OpcRf6::Rf6StatusTag *>(opcNode);
    if (NULL == opcRf6Status) {

        return OpcUa_BadNotWritable;
    }
    UaLocalizedTextArray enumString;
    opcRf6Status->mValue()->getEnumStrings(enumString);

    for (int i = 0 ; i < 4; i++) {
        //convert id

        UaString nodeId = opcRf6Status->getKey().toString() + UaString(".state%1").arg(i);
        //get node
        UaNode *statusState = mNodeManagerUaNode->getNode(UaNodeId(nodeId, mNodeManagerUaNode->getNameSpaceIndex()));

        if (NULL  != statusState) {
            OpcRf6::Rf6StatusState *pStatusState  = dynamic_cast<OpcRf6::Rf6StatusState *>(statusState);
            if(NULL != pStatusState){
                NSTATUS_STATE* pState = ob.GetAlarmState(*mDatabase,i);
                *pState = pStatusState->toRf6Structure();
                ob.WriteMemObject(CMemObject::_DISK_WRITE);
                ob.setStatusDesc(*mDatabase,i,UaVariant(*pStatusState->value(NULL).value()).toString().toUtf8() );
                if (i<enumString.length()) {
                    UaString text(UaVariant(*pStatusState->value(NULL).value()).toString().toUtf8());
                    text.copyTo(&enumString[i].Text);
                }
            }
        }
    }
    opcRf6Status->mValue()->setEnumStrings(enumString);

    return OpcUa_Good;
}

UaStatus dao_Rf6Order::updateObject(UaNodeId &NodeId)
{

    QString nodeId ;
    UaNode *opcNodeTarget = mNodeManagerUaNode->getNode(NodeId);
    if (NULL == opcNodeTarget){
        return OpcUa_BadNodeIdInvalid;
    }
    if (OpcRf6Id_Rf6AliasVariable == opcNodeTarget->typeDefinitionId().identifierNumeric()){
        if (NULL ==((OpcRf6::Rf6AliasVariable *)opcNodeTarget)->getTargetNode()){
            return OpcUa_BadTargetNodeIdInvalid;
        }
        nodeId= Helper::UaStringToQString(UaString(((OpcRf6::Rf6AliasVariable *)opcNodeTarget)->getTargetNode()->getKey().identifierString()));
    } else {
        nodeId= Helper::UaStringToQString(UaString(NodeId.identifierString() ));
    }


    //convert in qstring

    //parse nodeid
    QStringList listNods = nodeId.split(".");
    //get node
    UaNodeId updateNodeId = UaNodeId(Helper::QStringToUaString(listNods[0]),namespaceIndex);
    UaNode *opcNode =  mNodeManagerUaNode->getNode(updateNodeId);
    if (NULL == opcNode ){
        return OpcUa_BadNotWritable;
    }

    CRf6DbObject ob(listNods[0].toLongLong(0,16));

    OpcRf6::OpcRf6Object *opcRf6obj = dynamic_cast<OpcRf6::OpcRf6Object *>(opcNode);
    if (NULL != opcRf6obj) {
        opcRf6obj->toRf6(ob);
        mDatabase->Update(ob);
     } else {
        return OpcUa_Good;
    }

    if (OpcRf6Id_Rf6Pcu == opcNode->typeDefinitionId().identifierNumeric()){
        //update telemetry for pcu
        if (listNods.count() > 2){
            OpcRf6::Rf6Pcu *opcRf6Pcu = dynamic_cast<OpcRf6::Rf6Pcu *>(opcNode);
            if (NULL == opcRf6Pcu) {
               return OpcUa_BadNotWritable;
            }
            if( "port" == listNods[2]){
                opcRf6Pcu->getpcuTelemetry()->getfields()->deleteAllChilren(mNodeManagerUaNode);
               UaReferenceLists* referenceLists = opcRf6Pcu->getUaReferenceLists();
               UaReference * ref = (UaReference *) referenceLists->pTargetNodes();
               while (NULL != ref){
                    UaString str = ref->pTargetNode()->getKey().toString();
                    QString qstr = Helper::UaStringToQString(str);
                    if (qstr.contains("channel")){
                        mNodeManagerUaNode->deleteUaReference(opcRf6Pcu->getKey(),ref->pTargetNode()->getKey(),OpcUaId_Organizes);
                    }
                    ref = ref->pNextForwardReference();

                 }
                int numChannel = opcRf6Pcu->getcommunicationStatistics()->getport();
                mAccessRf6Telemetry->createChannel(numChannel,opcRf6Pcu);
                mAccessRf6Telemetry->updateChannelTelemetry(numChannel);
                return mAccessRf6Telemetry->updatePcuTelemetry(numChannel,opcRf6Pcu);
            }
            UaNodeId pcuChieldId = UaNodeId(Helper::QStringToUaString(listNods[0]+"."+listNods[1]),namespaceIndex);
            UaNode * pcuChieldNode=  mNodeManagerUaNode->getNode(pcuChieldId);
            OpcRf6::Rf6Telemetry *opcRf6Telemetry = dynamic_cast<OpcRf6::Rf6Telemetry *>(pcuChieldNode);
            if (NULL == opcRf6Telemetry) {

               return OpcUa_BadNotWritable;
            }
            return mAccessRf6Telemetry->savePcuTelemetry(opcRf6Pcu->getcommunicationStatistics()->getport(),opcRf6Pcu);
        }
    }
    if (OpcRf6Id_Rf6Channel == opcNode->typeDefinitionId().identifierNumeric()){
        //update telemetry

        OpcRf6::Rf6Channel *opcRRf6Channel = dynamic_cast<OpcRf6::Rf6Channel *>(opcNode);
        if (NULL == opcRRf6Channel) {

           return OpcUa_BadNotWritable;
        }

        return mAccessRf6Telemetry->saveChannelTelemetry(opcRRf6Channel->getnumber());
    }

    if (OpcRf6Id_Rf6MeterTag == opcNode->typeDefinitionId().identifierNumeric()){
        OpcRf6::Rf6MeterTag *opcRf6Meter = dynamic_cast<OpcRf6::Rf6MeterTag *>(opcNode);
        if (NULL == opcRf6Meter){
            return OpcUa_BadNotWritable;
        }
        ob.setUnits(*mDatabase,opcRf6Meter->getengineeringUnits().toUtf8());
        ob.WriteMemObject(CMemObject::_DISK_WRITE);

    }

    if (OpcRf6Id_Rf6StatusTag == opcNode->typeDefinitionId().identifierNumeric()){
        //get parent : status tag
        updateRf6StatusTag(opcNode,ob);
    }



    if (OpcRf6Id_Rf6AnalogTag == opcNode->typeDefinitionId().identifierNumeric()){
        //get parent : status tag
                OpcRf6::Rf6AnalogTag *Analog = dynamic_cast<OpcRf6::Rf6AnalogTag *>(opcNode);
        if (NULL == Analog) {

           return OpcUa_BadNotWritable;
        }
        //update units
        ob.setUnits(*mDatabase,Analog->getEngineeringUnits().toUtf8());
        //create analog limit Hi
        createAndUpdateAnalogLimits(Helper::QStringToUaString(listNods[0]) + UaString(".limitHi"),Analog,ob,_RF_DB_ALARM_H);


        //create analog limit HiHi
        createAndUpdateAnalogLimits(Helper::QStringToUaString(listNods[0]) + UaString(".limitHiHi"),Analog,ob,_RF_DB_ALARM_HH);


        //create analog limit Lo
        createAndUpdateAnalogLimits(Helper::QStringToUaString(listNods[0]) + UaString(".limitLo"),Analog,ob,_RF_DB_ALARM_L);


        //create analog limit LoLo
        createAndUpdateAnalogLimits(Helper::QStringToUaString(listNods[0]) + UaString(".limitLoLo"),Analog,ob,_RF_DB_ALARM_LL);

    }

    return OpcUa_Good;
}

UaStatus dao_Rf6Order::updateAttributes(UaNodeId &NodeId, UaLocalizedText &nodeAtrib)
{
    UaNode *opcNodeTarget = mNodeManagerUaNode->getNode(NodeId);
    if (NULL == opcNodeTarget){
        return OpcUa_BadNodeIdInvalid;
    }

    QString nodeId ;
    nodeId= Helper::UaStringToQString(UaString(opcNodeTarget->nodeId().identifierString() ));
    QStringList listNods = nodeId.split(".");

    unsigned int ind;
    CMemObject *pMO = mDatabase->getRdMemObject(listNods[0].toLongLong(0,16), &ind);
    if (pMO == NULL)
    {
        return OpcUa_BadIndexRangeNoData;
    }

    //set descriptiom in rf6 database

    bool flgPcu = opcNodeTarget->typeDefinitionId() == OpcRf6Id_Rf6Pcu ? true : false;
    UaString uastrDescr = nodeAtrib.toString();
    int descLen = uastrDescr.length();
    int descMaxLen;
    if (flgPcu)
        descMaxLen = _pcuDescSize;
    else
        descMaxLen = _tagDescSize;
    if (descLen > descMaxLen)
    {
        QString tempStr = uastrDescr.toUtf8();
        tempStr.truncate(descMaxLen);
        uastrDescr = UaString(tempStr.toUtf8().constData());
    }
    const char* description = uastrDescr.toUtf8();
    pMO->SetDesc(description);
    pMO->Write(ind, CMemObject::_DISK_WRITE);

    return OpcUa_Good;
}

UaStatus dao_Rf6Order::updateAttributes(OpcUa_WriteValue *writeNode)
{
    OpcUa_NodeId  nodeIdStruct = writeNode->NodeId;
    UaNodeId writeNodeId(nodeIdStruct);
    UaLocalizedText nodeAttrib;
    UaVariant(writeNode->Value.Value).toLocalizedText(nodeAttrib);
    if(writeNode->AttributeId == OpcUa_Attributes_Description){
        return updateAttributes(writeNodeId, nodeAttrib);
    }else if(writeNode->AttributeId == OpcUa_Attributes_DisplayName){
        UaNode * node = mNodeManagerUaNode->getNode(writeNodeId);
        if(QString::compare(QString(node->browseName().toString().toUtf8()),QString(nodeAttrib.toString().toUtf8()))){
            return OpcUa_BadNotWritable;
        }
        switch(node->typeDefinitionId().identifierNumeric())
        {
        case OpcRf6Id_Rf6Pcu:
            ((OpcRf6::Rf6Pcu*)node)->setDisplayName(nodeAttrib);
            break;
        case OpcRf6Id_Rf6AnalogTag:
            ((OpcRf6::Rf6AnalogTag*)node)->setDisplayName(nodeAttrib);
            break;
        case OpcRf6Id_Rf6MeterTag:
            ((OpcRf6::Rf6MeterTag*)node)->setDisplayName(nodeAttrib);
            break;
        case OpcRf6Id_Rf6StatusTag:
            ((OpcRf6::Rf6StatusTag*)node)->setDisplayName(nodeAttrib);
            break;
        default:
            return OpcUa_BadTypeDefinitionInvalid;
        }
        return OpcUa_Good;
    }else{
        return OpcUa_BadAttributeIdInvalid;
    }


}

void dao_Rf6Order::createAndUpdateAnalogLimits(UaString limitName,OpcRf6::Rf6AnalogTag *AnalogTag, CRf6DbObject &ob,int type)
{
    UaNode *AnalogLimitNode = mNodeManagerUaNode->getNode(UaNodeId(limitName,mNodeManagerUaNode->getNameSpaceIndex()));

    if (NULL  != AnalogLimitNode) {

      OpcRf6::Rf6AnalogLimit *pAnalogLimit  = dynamic_cast<OpcRf6::Rf6AnalogLimit *>(AnalogLimitNode);
      NANALOG_LIMIT* pLimit = ob.GetAlarmLimit(*mDatabase,type);

      *pLimit = pAnalogLimit->toRf6Structure();
      ob.WriteMemObject(CMemObject::_DISK_WRITE);


    }
}

UaStatus dao_Rf6Order::createObject(const UaNodeId &parentNodeId, const UaQualifiedName &browseName, OpcUa_NodeClass nodeClass, const UaExtensionObject &nodeAttributes, const UaNodeId &typeDefinition, UaNodeId &addedNodeId)
{
    bool rf6ObjWasExisted;	// flag of a rf6 object existence

    if ( (nodeAttributes.encoding() == UaExtensionObject::EncodeableObject)
         && (nodeAttributes.objectType()->TypeId == OpcUaId_ObjectAttributes)
         && (nodeAttributes.object() != NULL) ){

        OpcUa_ObjectAttributes* obAttr = (OpcUa_ObjectAttributes*)(nodeAttributes.object());
        UaString uastrDisplayName = UaLocalizedText(obAttr->DisplayName).toString();
        UaString uastrBrowsName = browseName.toString();
        if(QString::compare(QString(uastrDisplayName.toUtf8()),QString(uastrBrowsName.toUtf8()))){
            return OpcUa_BadNotWritable;
        }
    }

    ULONG_64 uid = 0;
    bool flgPcu = false;	// flag of PCU.
    if (typeDefinition.identifierNumeric() == OpcRf6Id_Rf6Pcu) {
        if (browseName.toString().length() > Rf6::_pcuNameSize)
            return OpcUa_BadBrowseNameInvalid;
        if (parentNodeId.toString() != UaString("OpcRf6Project.PCUs"))
            return OpcUa_BadParentNodeIdInvalid;
        uid = mDatabase->CreatePcu(browseName.toString().toUtf8(), &rf6ObjWasExisted);
        flgPcu = true;
    }

    //get pcu name
    UaNode *parrentNode = mNodeManagerUaNode->getNode(parentNodeId);
    if (NULL == parrentNode){
        return OpcUa_BadParentNodeIdInvalid;
    }

    if (typeDefinition.identifierNumeric() == OpcRf6Id_Rf6AnalogTag)  {
        if (browseName.toString().length() > Rf6::_tagNameSize)
            return OpcUa_BadBrowseNameInvalid;
        uid = mDatabase->CreateTag(NANALOG_DB,parrentNode->browseName().toString().toUtf8(),browseName.toString().toUtf8(), &rf6ObjWasExisted);

    }else if (typeDefinition.identifierNumeric() == OpcRf6Id_Rf6StatusTag)  {
        if (browseName.toString().length() > Rf6::_tagNameSize)
            return OpcUa_BadBrowseNameInvalid;
        uid = mDatabase->CreateTag(NSTATUS_DB,parrentNode->browseName().toString().toUtf8(),browseName.toString().toUtf8(), &rf6ObjWasExisted);
    }
    else if (typeDefinition.identifierNumeric() == OpcRf6Id_Rf6MeterTag)  {
        if (browseName.toString().length() > Rf6::_tagNameSize)
            return OpcUa_BadBrowseNameInvalid;
        uid = mDatabase->CreateTag(NMETER_DB,parrentNode->browseName().toString().toUtf8(),browseName.toString().toUtf8(), &rf6ObjWasExisted);
    }
    else if (typeDefinition.identifierNumeric() != OpcRf6Id_Rf6Pcu) {
          // it is not analog and status and meter and PCU
          return OpcUa_BadUserAccessDenied;
      }

    if (0 == uid){
        return OpcUa_BadNodeIdInvalid;
    }

    // description for PCUs and tags
    if ( (nodeAttributes.encoding() == UaExtensionObject::EncodeableObject)
         && (nodeAttributes.objectType()->TypeId == OpcUaId_ObjectAttributes)
         && (nodeAttributes.object() != NULL) )
    {
        unsigned int ind;
        CMemObject *pMO = mDatabase->getRdMemObject(uid, &ind);
        if (pMO != NULL)
        {
            OpcUa_ObjectAttributes* obAttr = (OpcUa_ObjectAttributes*)(nodeAttributes.object());

            UaString uastrDescr = UaLocalizedText(obAttr->Description).toString();
            int descLen = uastrDescr.length();
            int descMaxLen;
            if (flgPcu)
                descMaxLen = _pcuDescSize;
            else
                descMaxLen = _tagDescSize;
            if (descLen > descMaxLen)
            {
                QString tempStr = uastrDescr.toUtf8();
                tempStr.truncate(descMaxLen);
                uastrDescr = UaString(tempStr.toUtf8().constData());
            }
            const char* description = uastrDescr.toUtf8();
            pMO->SetDesc(description);
            //int ret = ; debugLog.logToFile("\tobject description \"%s\" [%d]", description, ret);
            pMO->Write(ind);

        }
    }

    QString QStrId;
    QStrId = QString::number(uid,16);

    UaString UaStrId = Helper::QStringToUaString(QStrId);

    addedNodeId = UaNodeId(UaStrId, namespaceIndex);
    UaStatus ret = createUaNode(parentNodeId,
                                OpcUaId_Organizes,
                                addedNodeId,
                                browseName,
                                typeDefinition);

    // workaround. object may be created by a update from realflex database
    if (rf6ObjWasExisted==false && ret.statusCode()==OpcUa_BadNodeIdExists)
    {
        debugLog.logToFile("The refundable status was changed.", uid);
        ret = OpcUa_Good;
    }

    return ret;
}



UaStatus dao_Rf6Order::createUaNode(const UaNodeId &parentNodeId,
                                    const UaNodeId &referenceTypeId,
                                    const UaNodeId &requestedNewNodeId,
                                    const UaQualifiedName &browseName,
                                    const UaNodeId &typeDefinition)
{

    //to do check nodemanagerUa
    //get parent Node

    UaNode *parentNode = mNodeManagerUaNode->getNode(parentNodeId);
    if (NULL == parentNode ){
        return OpcUa_BadParentNodeIdInvalid; //parent node not exists
    }

    if (NULL != mNodeManagerUaNode->getNode(requestedNewNodeId)) {
        return OpcUa_BadNodeIdExists; // node exists
    }

    if ( mNodeManagerUaNode->getNameSpaceIndex() != typeDefinition.namespaceIndex()  ) {
        return OpcUa_BadTypeDefinitionInvalid; //invalid type new node
    }
    UaReferenceLists *newOpcObject;

    switch (typeDefinition.identifierNumeric()){

        //#################################### create Rf6 PCU ######################################
        case OpcRf6Id_Rf6Pcu: {
            if (OpcUaId_FolderType != parentNode->typeDefinitionId().identifierNumeric()){
                 return OpcUa_BadParentNodeIdInvalid; //parent node not exists
            }
            //printf("new pCU \n");
            newOpcObject = new OpcRf6::Rf6Pcu(requestedNewNodeId,browseName.toString(),mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);
        }break;
        //###########################################################################################
        //################################## create Rf6 METER #######################################

        case OpcRf6Id_Rf6MeterTag:{
            if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
                return OpcUa_BadParentNodeIdInvalid; //parent node not exists
            }
            newOpcObject = new OpcRf6::Rf6MeterTag(requestedNewNodeId,browseName.toString(), mNodeManagerUaNode->getNameSpaceIndex(), mNodeManagerUaNode);
        }break;
        //############################################################################################
        //#################################### create RF6ANALOG ######################################
        case OpcRf6Id_Rf6AnalogTag:{
            if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
                 return OpcUa_BadParentNodeIdInvalid; //parent node not exists
            }

            OpcRf6::Rf6AnalogTag * pAnalogTag = new OpcRf6::Rf6AnalogTag(requestedNewNodeId,browseName.toString(),mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);
             //create 4 status state
            for (int i = 0 ; i < 4 ; i++) {
               UaString nodeName =requestedNewNodeId.toString();
               nodeName += UaString(".alarm.alarm.%1").arg(i);
               OpcRf6::Rf6AnalogLimit * pAnalogLimit = new OpcRf6::Rf6AnalogLimit(UaNodeId(nodeName, mNodeManagerUaNode->getNameSpaceIndex()),
                                                                                  nodeName,
                                                                                  mNodeManagerUaNode->getNameSpaceIndex(),
                                                                                  UaVariant(),
                                                                                  10,
                                                                                  mNodeManagerUaNode);
               mNodeManagerUaNode->addNodeAndReference(pAnalogTag->getalarm(),pAnalogLimit,OpcUaId_Organizes);
            }
            newOpcObject = pAnalogTag;
         }break;
          //##################################################################################################
            //################################ create Rf6 STATUS ########################################
           case OpcRf6Id_Rf6StatusTag:{
               if (OpcRf6Id_Rf6Pcu != parentNode->typeDefinitionId().identifierNumeric()){
                     return OpcUa_BadParentNodeIdInvalid; //parent node not exists
               }

               OpcRf6::Rf6StatusTag * pStatusTag = new OpcRf6::Rf6StatusTag(requestedNewNodeId,browseName.toString(),mNodeManagerUaNode->getNameSpaceIndex(),mNodeManagerUaNode);
                //create 4 status state
                for (int i = 0 ; i < 4 ; i++) {
                    UaString nodeName =requestedNewNodeId.toString();
                    nodeName += UaString(".states.state.%1").arg(i);
                    OpcRf6::Rf6StatusState * pStatusState = new OpcRf6::Rf6StatusState(UaNodeId(nodeName, mNodeManagerUaNode->getNameSpaceIndex()),
                                                                                       nodeName,
                                                                                       mNodeManagerUaNode->getNameSpaceIndex(),
                                                                                       UaVariant(),
                                                                                       10,
                                                                                       mNodeManagerUaNode);
                    mNodeManagerUaNode->addNodeAndReference(pStatusTag->getstates(),pStatusState,OpcUaId_Organizes);
                }
                newOpcObject = pStatusTag;
           }break;
           //##################################################################################################


    }

    if (NULL == newOpcObject ) {
        return OpcUa_BadNoData;
    }
    return mNodeManagerUaNode->addNodeAndReference(parentNodeId,newOpcObject,referenceTypeId);

}

void dao_Rf6Order::recivedRf6Object(ULONG_64 parent, QSharedPointer<CRf6DbObject> ob)
{

    //create and update or only update opcobject
    UaNode   *pNode =  factory->createAndUpdate(parent,ob);

    if (NULL != pNode){
        //subcribe realtime
        mSubscribe->subscribe(ob->GetIndex());
    }

}

void dao_Rf6Order::recivedRealtime(QSharedPointer<CRf6DbObject> ob)
{
    if (NULL == ob) return;
   UaNode *opcNode =  mNodeManagerUaNode->getNode(UaNodeId(Helper::QStringToUaString(QString::number(ob->GetIndex(),16)),namespaceIndex));
   OpcRf6::OpcRf6Object *opcRf6obj = dynamic_cast<OpcRf6::OpcRf6Object *>(opcNode);
   if (NULL != opcRf6obj) {
         opcRf6obj->updateFromRf6(ob);
   }
}
void dao_Rf6Order::deleteOpcObject(ULONG_64 uid)
{
    UaNodeId deleteNodeId = UaNodeId(Helper::QStringToUaString(QString::number(uid,16)),namespaceIndex);
    UaNode *deletNode =  mNodeManagerUaNode->getNode(deleteNodeId);
    if (NULL != deletNode){
       mNodeManagerUaNode->deleteUaNode(deletNode,OpcUa_True,OpcUa_True,OpcUa_True);
    }

}

void dao_Rf6Order::startEventSubscribe()
{
    mEventSubscribe->start();
}

void dao_Rf6Order::UpdateComplete(int errcode, ULONG_64 uind)
{
    //printf("%lld error = %d \n",uind,errcode);
}

void dao_Rf6Order::recivedUpdateRf6object(QSharedPointer<CRf6DbObject> ob)
{
   //parent not use for update if object not found then return null
    factory->createAndUpdate(0,ob);

}

