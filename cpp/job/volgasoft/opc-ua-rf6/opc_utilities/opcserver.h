/******************************************************************************
** opcserver.h
**
** Copyright (C) 2008-2009 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK sample code
**
** Description: Main OPC Server object class.
**
******************************************************************************/
#ifndef MAIN_OPCSERVER_H
#define MAIN_OPCSERVER_H

#include "serverconfig.h"
#include "nodemanager.h"

class OpcServerPrivate;
class OpcServerCallback;
class UaServer;
namespace Opc {
    /** Main OPC Server object.
      This class is a utility class managing all server SDK modules for common use cases in a simplified way.
      Enhanced users may replace this class with their own implementation to be able
      to use derived classes to overwrite SDK functionality.
      */
    class OpcServer
    {
        UA_DISABLE_COPY(OpcServer);
    public:
        // construction / destruction
        OpcServer();
        ~OpcServer();

        // Methods used to initialize the server
        int setServerConfig(const UaString& configurationFile, const UaString& applicationPath);
        int setServerConfig(ServerConfig* pServerConfig);
        int addNodeManager(NodeManager* pNodeManager);
        int setCallback(OpcServerCallback* pOpcServerCallback);

        // Methods used to start and stop the server
        int start();
        int stop(OpcUa_Int32 secondsTillShutdown, const UaLocalizedText& shutdownReason);

        // Access to default node manager
        NodeManagerConfig* getDefaultNodeManager();

    private:
        OpcServerPrivate* d;
    };
}

/** Callback interface for the main OPC Server object.
 This callback interface needs to be implemented if the application wants to implement user authentication.
 */
class OpcServerCallback
{
public:
    /** Construction */
    OpcServerCallback(){}
    /** Destruction */
    virtual ~OpcServerCallback(){}

    /** Creates a session object for the OPC server.
     This callback allows the application to create its own session class derived from UaSession to store 
     user specific information and to implement the user logon and user verfication.
     @param sessionID            Session Id created by the server application. 
     @param authenticationToken  Secret session Id created by the server application. 
     @return                     A pointer to the created session.
     */
    virtual Session* createSession(OpcUa_Int32 sessionID, const UaNodeId &authenticationToken) = 0;

    /** Validates the user identity token and sets the user for the session.
     *  @param pSession             Interface to the Session context for the method call
     *  @param pUserIdentityToken   Secret session Id created by the server application. 
     *  @return                     Error code.
     */
    virtual UaStatus logonSessionUser(Session* pSession, UaUserIdentityToken* pUserIdentityToken) = 0;

    /** Optional method used to create an application specific UaServer object used as main entry point for the UA communication.
     *  The SDK creates an instance of the UaServer class as default implementation.
     *  @return UaServer object or NULL if the SDK should use the default implementation.
     */
	virtual UaServer* createUaServer() { return NULL; }
};

#endif // MAIN_OPCSERVER_H


