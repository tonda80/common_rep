#include "helper.h"

#include <QString>
#include <QStringList>

#include <time.h>
#include <stdarg.h>

Helper::Helper(QObject *parent) :
    QObject(parent)
{
}

UaString Helper::QStringToUaString(QString str)
{
    return str.toAscii().data();
}

QString Helper::UaStringToQString(UaString str)
{
    return str.toUtf8();
}

bool endsWith(const UaString& checkedStr, const char* end)
{
	QString checked = checkedStr.toUtf8();
	return checked.endsWith(QString(end));
}

DebugLog::DebugLog(const char *fname)
{
	logfile = NULL;
	if (fname != NULL)
		logfile = fopen(fname,"a+");
}

DebugLog::~DebugLog()
{
	if(logfile != NULL)
		fclose(logfile);
}

void DebugLog::printTimestamp()
{
	if(logfile==NULL)
		return;
	struct timespec curr_time;
	clock_gettime(CLOCK_REALTIME, &curr_time);
	struct tm* ptm = localtime(&(curr_time.tv_sec));
	fprintf(logfile, "%d.%02d.%02d %02d:%02d:%02d.%03ld: ", ptm->tm_year+1900, ptm->tm_mon+1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, curr_time.tv_nsec/1000000 );
}

int DebugLog::logToFile(const char *fmt, ...)
{
	if(logfile==NULL)
		return 0;

	printTimestamp();

	va_list arglist;
	int retval;
	va_start(arglist, fmt);
	retval = vfprintf(logfile, fmt, arglist);
	va_end(arglist);

	fprintf(logfile, "\n");
	fflush(logfile);

	return retval;
}

#ifdef RF6_DEBUGLOG
DebugLog debugLog("/tmp/opc-ua-rf6.log");
#else
DebugLog debugLog(NULL);
#endif

ULONG_64 uastringToUlong64(const UaString& nodeID, int base/*=10*/)
{
	QString st = nodeID.toUtf8();
	bool ok;
	return st.split('.')[0].toULongLong(&ok, base);
}


