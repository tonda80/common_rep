#ifndef HELPER_H
#define HELPER_H
#include <opcua_proxystub.h>
#include <uastring.h>
#include <QObject>

#include <stdio.h>

#include <vtype.h>

class Helper : public QObject
{
    Q_OBJECT
public:
    explicit Helper(QObject *parent = 0);
  static  UaString QStringToUaString(QString str);
  static  QString UaStringToQString(UaString str);
signals:
    
public slots:
    
};

bool endsWith(const UaString& checkedStr, const char* end);

class DebugLog
{
public:
	DebugLog(const char *fname);
	~DebugLog();
	int logToFile(const char *fmt, ...);
	void printTimestamp();

private:
	FILE *logfile;
};

extern DebugLog debugLog;


// Function transfers UaString to U
ULONG_64 uastringToUlong64(const UaString& nodeID, int base=10);

#endif // HELPER_H
