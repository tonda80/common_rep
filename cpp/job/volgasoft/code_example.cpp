// ����������� �� rsrc
------------------------------------------------------------------------------
#include <rf_rsrc/api.h>

RSRC_HANDLE g_hHdl=NULL;

// register module on rsrc
g_hHdl = RSRC_Init();
if (g_hHdl == NULL)
{
	g_mon.Error("calcdo: rf_rsrc is not running, exiting...\n");
	rf6_exit(-1);
}
RSRC_Register( g_hHdl, "calcdo", argv, -1, 0);

RSRC_UnRegister( g_hHdl );


// �������� � ��������� �������
------------------------------------------------------------------------------
struct sigevent event;
SIGEV_PULSE_INIT(&event, m_iCCoid, getprio(0), PULSE_TIMER, 0);

timer_t timer;
timer_create(CLOCK_REALTIME, &event, &timer);

const int PERIOD = 2;	// sec
struct itimerspec itime;
itime.it_value.tv_sec = PERIOD;
itime.it_value.tv_nsec = 0;
itime.it_interval.tv_sec = PERIOD;
itime.it_interval.tv_nsec = 0;
timer_settime(timer, 0, &itime, NULL);

// ������ � ����������� ���� (��� ������� ����������)
------------------------------------------------------------------------------
#include <trans/rfstr.h>
#include <memobjmgr.hpp>

MemObjMgr  g_mom;
g_mom.InitObjMgr(NPCU_DB);

const char pcu_name_f[] = "COOL_PCU";
const char tag_name_f[] = "SUPER_TAG";
char szPcuName[MB_LEN_MAX*MAX_LEN_NAME], szTagName[MB_LEN_MAX*MAX_LEN_NAME];	
rf_strtombs(szPcuName, rf_strupr(pcu_name_f), sizeof(szPcuName));
rf_strtombs(szTagName, rf_strupr(tag_name_f), sizeof(szTagName));

CMemObject *pMemObj = g_mom.GetItem(NSTATUS_DB)->cobj;	// ! dbtype
int dbidx = pMemObj->GetIndex(szTagName, szPcuName);
if (dbidx < 1) ;// error
if (pMemObj->Read(dbidx) == RF_FAILURE) 
	; // error

_DbType &dbrec = pMemObj->DbRec;
// dbrec.s - ��� ��������� �� ������ ��������� �����
switch(dbtype)
{	case NSTATUS_DB:
	dbrec.s->cur_val &= 1;	// ������ ����� ��������� �������� ��� ������ � ������
	break;
}
	pMemObj->Write(dbidx, CMemObject::_DISK_WRITE);	// ���-�� �����, ����� ��������� ��������� � ����
------------------------------------------------------------------------------
//������ ������� ��������� �������
// ��������� ������ ��� CDbAccess ������ � recipe/tagwork.cpp
CDbAccess oDbAccess;
oDbAccess.RfDbOpen();

CMemObject* poMemPcu = oDbAccess.GetRfObject(FP_PCU_DB);
if (poMemPcu == NULL)
	return EXIT_FAILURE;

uPcuIdx = oDbAccess.GetRfIndex(poMemPcu,NamePCU, NULL);
if ( poMemPcu -> Read(uPcuIdx) != RF_SUCCESS )
	return EXIT_FAILURE;

CMemObject* poMemObj = oDbAccess.GetRfObject(DbType);
if (poMemObj == NULL)
	return EXIT_FAILURE;

uIdx = oDbAccess.GetRfIndex(poMemObj,NamePCU, NameTag);

------------------------------------------------------------------------------
// �������� ����������
int create_dir(const char *dirname)
{
	struct stat stat_buf;

	if (stat(dirname, &stat_buf)==0)
	{
		if (S_ISDIR(stat_buf.st_mode)) return 0;
		return -1;
	}

	if (errno != ENOENT) return -2;
	
	if (mkdir(dirname, S_IRWXU|S_IRWXG|S_IRWXO) == 0) return 0;
	return -3;
}

------------------------------------------------------------------------------
class CInpOutp
{
public:
	QTextStream inp; QTextStream outp;
	CInpOutp():inp(stdin), outp(stdout){}
	template<typename T>
	T get(const char* inv=NULL)
	{
		if (inv!=NULL) {outp<<inv; outp.flush();}
		T v; inp>>v; return v;
	}
};


