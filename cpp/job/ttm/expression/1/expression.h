#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <string>
#include <memory>


struct expression_error : public std::runtime_error {
    expression_error(const std::string& msg) : std::runtime_error("expression_error: "+msg) {}
};


class ExpressionImpl;


class Expression
{
public:
    Expression(const std::string& src_);
    ~Expression();

    std::string calculate();
    const std::string& source_string() {return src;}

private:
    std::unique_ptr<ExpressionImpl> impl;
    std::string src;
};

#endif // EXPRESSION_H
