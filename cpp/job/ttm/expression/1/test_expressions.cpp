#include <iostream>

#include "expression.h"


void test_expression(const std::string& s) {
    Expression e(s);
    std::cout << e.source_string() << " = " << e.calculate() << std::endl;
}

int main(int argc, const char** argv)
{
    test_expression("3");
    test_expression("1+2");
    test_expression("1+2+3+4");
    test_expression("1+ 2 +3   +4");

    test_expression("1+2*3");


    return 0;
}
