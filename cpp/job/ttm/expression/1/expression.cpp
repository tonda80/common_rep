#include "expression.h"

#include <vector>
#include <cctype>

#include "ignored_warnings.h"

#include <iostream> // for debug


struct CalculatedBase
{
    virtual ~CalculatedBase() = default;
    virtual int priority() {return 100;}
};

template<typename T>
struct Calculated : public CalculatedBase
{
    virtual T get_value() = 0;

    virtual void set_value(const T&) {throw expression_error("[Calculated::set_value] not implemented");}
    virtual void init(std::initializer_list<Calculated<T>*>) {throw expression_error("[Calculated::init] not implemented");}
};


enum class LexType {
    Argument,
    Number,
    BinOperator,
    Symbol,             // brackets, commas (?)
    //Function,
    //UnOperator        // prefix, postfix
};

struct Lex {
    virtual ~Lex() = default;
    LexType type;
    virtual CalculatedBase* get_calculated() { return nullptr; }
    std::string str_type() {return std::to_string(static_cast<int>(type));}
protected:
    Lex(LexType t) : type(t) {}
};

using LexPtr = std::unique_ptr<Lex>;
using StrIter = std::string::const_iterator;


// TODO CalculatedLex


template<typename T>
struct Argument : public Lex, public Calculated<T> {
    Argument(char n) : Lex(LexType::Argument), name(n) {}

    static LexPtr create(StrIter& it) {
        if (*it == 'x') {
            return LexPtr(new Argument(*it++));
        }
        return nullptr;
    }

    CalculatedBase* get_calculated() override { return this; }

    T get_value() override {
        if (!initialized) {
            throw expression_error("[Argument::get_value] not initialized: " + std::string(1, name));
        }
        return value;
    }

    void set_value(const T& v) override {
        value = v;
        initialized = true;
    }

    const char name;
private:
    T value;
    bool initialized = false;
};

template<typename T>
struct Number : public Lex, public Calculated<T> {
    Number(T v) : Lex(LexType::Number), value(v) {}

    static LexPtr create(StrIter& it, const StrIter end);

    CalculatedBase* get_calculated() override { return this; }

    T get_value() override {
        return value;
    }

private:
    T value;
};
// and only double specialization
template<>
LexPtr Number<double>::create(StrIter& it, const StrIter end) {
    std::size_t pos;
    try {
        double d = std::stod(std::string(it, end), &pos);
        it += pos;
        return LexPtr(new Number<double>(d));
    }
    catch (...) {
        return nullptr;
    }
}

struct SymbolLex : public Lex {
    SymbolLex(char c) : Lex(LexType::Symbol), symbol(c) {}

    static LexPtr create(StrIter& it) {
        for (auto c : ")(") {
            if (*it == c) {
                return LexPtr(new SymbolLex(*it++));
            }
        }
        return nullptr;
    }

    const char symbol;
};

template<typename T>
struct BinOperator : public Lex, public Calculated<T> {
    BinOperator(char c) : Lex(LexType::BinOperator), operator_(c) {}

    static LexPtr create(StrIter& it) {
        for (auto c : "+-*/") {
            if (*it == c) {
                return LexPtr(new BinOperator(*it++));
            }
        }
        return nullptr;
    }

    CalculatedBase* get_calculated() override { return this; }

    int priority() override {
        if (operator_ == '+' || operator_ == '-') {
            return 50;
        }
        return 100;
    }

    void init(std::initializer_list<Calculated<T>*> lst) override {
        if (lst.size() != 2) {
            throw expression_error("[BinOperator::init] wrong length " + std::to_string(lst.size()));
        }
        auto it = lst.begin();
        if (*it) {
            operand1 = *it;
        }
        if (*++it) {
            operand2 = *it;
        }
    }

    T get_value() override {
        if (!operand1 || !operand2) {
            throw expression_error("[BinOperator::get_value] not initialized");
        }
        if (operator_ == '+') {
            return operand1->get_value() + operand2->get_value();
        }
        if (operator_ == '-') {
            return operand1->get_value() - operand2->get_value();
        }
        if (operator_ == '*') {
            return operand1->get_value() * operand2->get_value();
        }
        if (operator_ == '/') {
            return operand1->get_value() / operand2->get_value();
        }
        throw expression_error("[BinOperator::get_value] unsupported operator: " + std::string(1, operator_));
    }

    const char operator_;

private:
    Calculated<T> *operand1 = nullptr;
    Calculated<T> *operand2 = nullptr;
};


using NumberT = double;

LexPtr create_from(StrIter& it, const StrIter& end)
{
    while (it < end) {
        if (std::isspace(*it)) {
            ++it;
            continue;
        }
        // TODO cycle
        if (auto l = Argument<NumberT>::create(it)) {
            return l;
        }
        if (auto l = SymbolLex::create(it)) {
            return l;
        }
        if (auto l = BinOperator<NumberT>::create(it)) {
            return l;
        }
        if (auto l = Number<NumberT>::create(it, end)) {    // строго после bin, чтоб не ловить +2 как число
            return l;
        }
        throw expression_error("cannot recognize lexeme: "+std::string(1, *it)); // TODO
    }

    throw expression_error("internal error");   // cannot be here
}

using lex_container = std::vector<LexPtr>;

lex_container split(const std::string& src) {
    lex_container res;
    auto it = src.begin();
    while (it < src.end()) {
        res.push_back(create_from(it, src.end()));
    }
    return res;
}


class ExpressionImpl
{
public:
    ExpressionImpl(const std::string& src) {
        lexemes = split(src);

        if (lexemes.size() == 0) {
            throw expression_error("[ExpressionImpl()] no lexemes");
        }
        for (size_t i = 0; i < lexemes.size(); ++i) {
            auto& l = lexemes[i];

            if (l->type == LexType::Number || l->type == LexType::Argument) {
                if (lexemes.size() == 1) {
                    root = get_calculated(i);
                }
                else if (i != 0) {
                    throw expression_error("[ExpressionImpl()] unexpected lexeme into " + std::to_string(i));
                }
            }
            else if (l->type == LexType::BinOperator) {
                if (i != 0 && i != lexemes.size()-1) {
                    auto calc = get_calculated(i);
                    auto op1 = get_calculated(i-1);
                    auto op2 = get_calculated(i+1);
                    ++i;

                    // TODO!!! тут переделать
                    if (!root) {
                        calc->init({op1, op2});
                        root = calc;
                    }
                    else if (calc->priority() > root->priority()) {
                        calc->init({op1, op2});
                        root->init({nullptr, calc});
                    }
                    else {
                        calc->init({op1, op2});
                        root = calc;
                    }
                }
                else {
                    throw expression_error("[ExpressionImpl()] unexpected BinOperator: " + std::to_string(i));
                }
            }
            else if (l->type == LexType::Symbol) {
                // TODO
            }
            //else if (l->type == LexType::Function) {  // TODO            }
            else {
                throw expression_error("[ExpressionImpl()] unsupported lexeme: " + l->str_type());
            }

        }
    }
    std::string calculate(NumberT arg) {
        return std::to_string(root->get_value());
    }
private:
    lex_container lexemes;
    Calculated<NumberT>* root = nullptr;
    Calculated<NumberT>* argument = nullptr;    // TODO? list

    Calculated<NumberT>* get_calculated(size_t i) {
        auto& lexeme = lexemes[i];
        auto calc = static_cast<Calculated<NumberT>*>(lexeme->get_calculated());
        if (!calc) {
            throw expression_error("[ExpressionImpl::get_calculated] expected calculated into " + std::to_string(i));
        }
        if (lexeme->type == LexType::Argument) {
            if (!argument) {
                argument = calc;
            }
            else {
                throw expression_error("[ExpressionImpl()] too many arguments: " + std::to_string(i));
            }
        }
        return calc;
    }
};

Expression::Expression(const std::string& src_)
{
    src = src_;
    impl = std::make_unique<ExpressionImpl>(src);
}

Expression::~Expression() = default;

std::string Expression::calculate()
{
    return impl->calculate(0);
}
