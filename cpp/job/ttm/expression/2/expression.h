#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <string>
#include <memory>
#include <functional>


struct expression_error : public std::runtime_error {
    expression_error(const std::string& msg) : std::runtime_error("expression_error: "+msg) {}
};


class ExpressionImpl;

class Expression
{
public:
    using NumT = double;

    Expression(const std::string& src);
    ~Expression();

    std::string calculate(NumT value, NumT* result=nullptr) const;
    const std::string& source_string() const;

    static std::function<void(const std::string&)> report_warn;
private:
    std::unique_ptr<ExpressionImpl> impl;
};

#endif // EXPRESSION_H
