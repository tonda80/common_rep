#include <spdlog/spdlog.h>

#include "sqlite/sqlite3.h"

#include "StorageSqlite.h"

using namespace std;


StorageSqlite::StorageSqlite(const std::shared_ptr<spdlog::logger>& logger_) :
      db(nullptr)
    , logger(logger_)
    , insert_stmt(nullptr)
    , select_stmt(nullptr)
    , update_stmt(nullptr)
    , delete_stmt(nullptr)
    , select_all_stmt(nullptr)
{
}

bool StorageSqlite::Open(const char* dbname)
{
    if (!logger) {
        return false;
    }

    if (sqlite3_open(dbname, &db) != SQLITE_OK) {
        logger->error("StorageSqlite. Opening error of {}", dbname);
        return false;
    }

    if (!ExecRequest("CREATE TABLE IF NOT EXISTS tbl(id INTEGER PRIMARY KEY ASC, hdr BLOB, msg BLOB);")) {
        return false;
    }

    if (!ExecRequest("PRAGMA synchronous = OFF;")) {
        return false;
    }

    if (!ExecRequest("PRAGMA journal_mode = TRUNCATE;")) {
        return false;
    }

    if (!PrepareStatement(insert_stmt, "INSERT INTO tbl VALUES (?1, ?2, ?3);")) {
        return false;
    }

    if (!PrepareStatement(select_stmt, "SELECT * FROM tbl WHERE id = ?1;")) {
        return false;
    }

    if (!PrepareStatement(update_stmt, "UPDATE tbl SET hdr = ?2, msg = ?3 WHERE id = ?1;")) {
        return false;
    }

    if (!PrepareStatement(delete_stmt, "DELETE FROM tbl WHERE id = ?1;")) {
        return false;
    }

    if (!PrepareStatement(select_all_stmt, "SELECT * FROM tbl;")) {
        return false;
    }

    return true;
}

StorageSqlite::~StorageSqlite() noexcept
{
    Close();
}

bool StorageSqlite::Close()
{
    if (!db) {
        return true;
    }

    sqlite3_finalize(insert_stmt);
    sqlite3_finalize(select_stmt);
    sqlite3_finalize(update_stmt);
    sqlite3_finalize(delete_stmt);
    sqlite3_finalize(select_all_stmt);

    int ret = sqlite3_close(db);
    if (ret != SQLITE_OK) {
        logger->error("StorageSqlite. Cannot close database. Returned {}", ret);
        return false;
    } else {
        db = nullptr;
    }

    return true;
}

bool StorageSqlite::ExecRequest(const std::string& req)
{
    char *err = nullptr;
    sqlite3_exec(db, req.c_str(), 0, 0, &err);
    if (err) {
        logger->error("StorageSqlite. Error of execution of request '{}'. {}.", req, err);
        sqlite3_free(err);
        return false;
    }
    return true;
}

bool StorageSqlite::PrepareStatement(sqlite3_stmt *&stmt, const std::string& stmt_str)
{
    int ret = sqlite3_prepare_v2(db, stmt_str.c_str(), static_cast<int>(stmt_str.size()), &stmt, nullptr);
    if (!stmt) {
        logger->error("StorageSqlite. Cannot prepare '{}' statement. Returned {}.", stmt_str, ret);
        return false;
    }
    return true;
}

bool StorageSqlite::Bind(sqlite3_stmt *stmt, int pos, int value)
{
    int ret;
    ret = sqlite3_bind_int(stmt, pos, value);
    if (ret != SQLITE_OK) {
        logger->error("StorageSqlite. Cannot bind int to statement. Pos {}, value {}. Returned {}.", pos, value, ret);
        return false;
    }
    return true;
}

bool StorageSqlite::Bind(sqlite3_stmt *stmt, int pos, const string& value)
{
    int ret;
    ret = sqlite3_bind_blob(stmt, pos, value.c_str(), static_cast<int>(value.size()), nullptr);
    if (ret != SQLITE_OK) {
        logger->error("StorageSqlite. Cannot bind blob to statement. Pos {}, value {}. Returned {}.", pos, value, ret);
        return false;
    }
    return true;
}

bool StorageSqlite::Create(unsigned int num, const std::string &hdr, const std::string &msg)
{
    sqlite3_reset(insert_stmt);

    if (!Bind(insert_stmt, 1, num) || !Bind(insert_stmt, 2, hdr) || !Bind(insert_stmt, 3, msg)) {
        logger->error("StorageSqlite. Cannot create {} record. Bind error.", num);
        return false;
    }

    int ret = sqlite3_step(insert_stmt);
    // TODO handle SQLITE_BUSY and so on
    if (ret != SQLITE_DONE) {
        logger->error("StorageSqlite. Creare error for {}, sqlite3_step returned {}.", num, ret);
        return false;
    }

    return true;
}

bool StorageSqlite::Read(unsigned int num, std::string &hdr, std::string &msg)
{
    sqlite3_reset(select_stmt);

    if (!Bind(select_stmt, 1, num)) {
        logger->error("StorageSqlite. Cannot read {} record. Bind error.", num);
        return false;
    }

    int ret = sqlite3_step(select_stmt);
    // TODO handle sqlite_busy and so on
    if (ret != SQLITE_ROW) {
        if (ret == SQLITE_DONE)
            logger->warn("StorageSqlite. Read, no records with key {}.", num);
        else
            logger->error("StorageSqlite. Read error for {}, sqlite3_step returned {}.", num, ret);
        return false;
    }

    int n_col = sqlite3_column_count(select_stmt);
    int type0 = sqlite3_column_type(select_stmt, 0);
    int type1 = sqlite3_column_type(select_stmt, 1);
    int type2 = sqlite3_column_type(select_stmt, 2);
    if (n_col != 3 ||
        type0 != SQLITE_INTEGER || type1 != SQLITE_BLOB || type2 != SQLITE_BLOB ||
        sqlite3_column_int(select_stmt, 0) != num)
    {
        logger->error("StorageSqlite. Read verification error {} {} {} {} {}.", n_col, type0, type1, type2, sqlite3_column_int(select_stmt, 0));
        return false;
    }

    hdr.assign(static_cast<const char*>(sqlite3_column_blob(select_stmt, 1)), sqlite3_column_bytes(select_stmt, 1));
    msg.assign(static_cast<const char*>(sqlite3_column_blob(select_stmt, 2)), sqlite3_column_bytes(select_stmt, 2));

    return true;
}

bool StorageSqlite::Update(unsigned int num, const std::string &hdr, const std::string &msg)
{
    sqlite3_reset(update_stmt);

    if (!Bind(update_stmt, 1, num) || !Bind(update_stmt, 2, hdr) || !Bind(update_stmt, 3, msg)) {
        logger->error("StorageSqlite. Cannot update {} record. Bind error.", num);
        return false;
    }

    int ret = sqlite3_step(update_stmt);
    // TODO handle SQLITE_BUSY and so on
    if (ret != SQLITE_DONE) {
        logger->error("StorageSqlite. Update error, sqlite3_step returned {}.", ret);
        return false;
    }

    if (sqlite3_changes(db) == 0) {  // no update
        logger->warn("StorageSqlite. Update, no records with key {}.", num);
        return false;
    }

    return true;
}

bool StorageSqlite::Delete(unsigned int num)
{
    sqlite3_reset(delete_stmt);

    if (!Bind(delete_stmt, 1, num)) {
        logger->error("StorageSqlite. Cannot delete {} record. Bind error.", num);
        return false;
    }

    int ret = sqlite3_step(delete_stmt);
    // TODO handle sqlite_busy and so on
    if (ret != SQLITE_DONE) {
        logger->error("StorageSqlite. Delete error, sqlite3_step returned {}.", ret);
        return false;
    }

    if (sqlite3_changes(db) == 0) {  // no delete
        logger->warn("StorageSqlite. Delete, no records with key {}.", num);
        return false;
    }

    return true;
}

int StorageSqlite::DeleteAll()
{
    if (ExecRequest("DELETE FROM tbl;")) {
        return sqlite3_changes(db);
    }

    return 0;
}

void StorageSqlite::GetAllKeys(const std::function<void(unsigned int)> &clb)
{
    sqlite3_reset(select_all_stmt);

    int ret;
    // TODO handle sqlite_busy and so on
    while (1) {
        ret = sqlite3_step(select_all_stmt);
        if (ret != SQLITE_ROW)
            break;

        if (sqlite3_column_type(select_all_stmt, 0) == SQLITE_INTEGER)
            clb(sqlite3_column_int(select_all_stmt, 0));
    }

    if (ret != SQLITE_DONE) {
        logger->error("StorageSqlite. GetAll error, sqlite3_step returned {}.", ret);
    }

}