#ifndef __STORAGE_SQLITE_H__
#define __STORAGE_SQLITE_H__

#include <functional>
#include <memory>

#include "tl_defines.h" // temporary

struct sqlite3;
struct sqlite3_stmt;
namespace spdlog { class logger; };

// TODO this class is exported for test/demo only
class TL_EXPORT StorageSqlite
{
public:
    StorageSqlite(const std::shared_ptr<spdlog::logger>&);
    bool Open(const char* dbname);
    ~StorageSqlite() noexcept;
    bool Close();       // it's called from destructor

    bool Create(unsigned int num, const std::string &hdr, const std::string &msg);
    bool Read(unsigned int num, std::string &hdr, std::string &msg);
    bool Update(unsigned int num, const std::string &hdr, const std::string &msg);
    bool Delete(unsigned int num);

    int DeleteAll();
    void GetAllKeys(const std::function<void(unsigned int)> &clb);

    StorageSqlite(const StorageSqlite&) = delete;
    const StorageSqlite& operator=(const StorageSqlite&) = delete;

private:
    bool ExecRequest(const std::string&);  // slow simple way
    bool PrepareStatement(sqlite3_stmt *&, const std::string&);
    inline bool Bind(sqlite3_stmt *stmt, int pos, int value);
    inline bool Bind(sqlite3_stmt *stmt, int pos, const std::string& value);
    
    sqlite3 *db;
    sqlite3_stmt *insert_stmt;
    sqlite3_stmt *select_stmt;
    sqlite3_stmt *update_stmt;
    sqlite3_stmt *delete_stmt;
    sqlite3_stmt *select_all_stmt;

    std::shared_ptr<spdlog::logger> logger;
};

#endif // __STORAGE_SQLITE_H__