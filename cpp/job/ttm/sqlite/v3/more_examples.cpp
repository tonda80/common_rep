// для теста стирать базу вместе с доп файлами -shm и -wal
rm tsdiag.db && rm tsdiag.db-* && cp bkp_db/tsdiag_no_raw.db tsdiag.db


// вот так предлагается читать неопределенное количество значений
auto s = statement("SELECT raw_value from diag WHERE id='PRTZ_6';");
int ret = s.exec();
while (ret == SQLITE_ROW) {
	logdebug << "__deb read " << std::hex << static_cast<uint64_t>(s.column<int64_t>(0));
	ret = s.step();
}
