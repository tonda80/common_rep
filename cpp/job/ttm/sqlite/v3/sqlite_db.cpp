#include "sqlite_db.h"

#include <lib/logger/logger.h>

#include "utils.h"


void SqliteDbBase::open(const char* dbname)
{
    if (sqlite3_open(dbname, &db) != SQLITE_OK) {
        throw sqlitedb_error(std::string(dbname) + " cannot be opened. " + sqlite3_errmsg(db));
    }
}

bool SqliteDbBase::close()
{
    if (!db) {
        return true;
    }

    int ret = sqlite3_close(db);
    if (ret == SQLITE_OK) {
        db = nullptr;
    }
    else {
        logerror << "[SqliteDbBase::close] Cannot close database, returned " << ret;
        return false;
    }
    return true;
}

bool SqliteDbBase::exec_request(const std::string& req, exec_callback_t callback/*=nullptr*/, void* callback_arg/*=nullptr*/)  const
{
    char *errmsg = nullptr;
    sqlite3_exec(db, req.c_str(), callback, callback_arg, &errmsg);
    if (errmsg) {
        logerror << "[SqliteDbBase::exec_request] " << req << " cannot be executed. " << errmsg;
        sqlite3_free(errmsg);
        return false;
    }
    return true;
}

void SqliteDbBase::Statement::init(SqliteDbBase* owner_)
{
    int ret = sqlite3_prepare_v2(owner_->db, statement_str.c_str(), -1, &statement, nullptr);
    if (ret != SQLITE_OK) {
        throw sqlitedb_error(utils::str() << "statement \"" << statement_str << "\" init error " << ret);
    }
    //owner = owner_;
}

bool SqliteDbBase::Statement::finalize()
{
    if (!statement) {
        return false;
    }
    if (int ret = sqlite3_finalize(statement) != SQLITE_OK) {
        logerror << "SqliteStatement::finalize error " << ret;
        return false;
    }
    return true;
}

void SqliteDbBase::Statement::reset()
{
    if (!statement) {
        throw sqlitedb_error("null statement reset");
    }
    if (int ret = sqlite3_reset(statement) != SQLITE_OK) {
        throw sqlitedb_error(utils::str() << "statement \"" + statement_str + "\" reset error " << ret);
    }
}

int SqliteDbBase::Statement::step()
{
    if (!statement) {
        throw sqlitedb_error("null statement step");
    }
    int ret = sqlite3_step(statement);
    if (ret != SQLITE_DONE && ret != SQLITE_ROW) {
        throw sqlitedb_error(utils::str() << "statement \"" + statement_str + "\" step error " << ret);
    }
    return ret;
}
