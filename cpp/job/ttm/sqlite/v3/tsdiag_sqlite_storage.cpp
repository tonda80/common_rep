#include "tsdiag_sqlite_storage.h"

#include <chrono>

#include "lib/logger/logger.h"



void TsdiagSqliteStorage::init(const std::string& dbpath, int db_limit_day)
{
    open(dbpath.c_str());
    init_tables();

    loginfo << "opened " << dbpath;

    if (db_limit_day > 0) {
        long limit_sec = utils::timestamp_sec() - db_limit_day*24*3600;

        loginfo << "records older than " << db_limit_day << " days will be removed from database ";
        logdebug << "limit_sec = " << limit_sec;

        Statement stm("DELETE FROM diag WHERE time < datetime(?1, 'unixepoch');", this);
        stm.exec_no_throw([](const char* what){
            logerror << "cannot remove old records into database: " << what;
        }, limit_sec);
    }
}

void TsdiagSqliteStorage::init_tables() {
    // -- diagnostic
    // id       id параметра
    // time     время записи
    // map      плейсхолдеры описания
    // type     тип (0 historical event, 1 state (похоже такое вообще не нужно), ?)
    // group_id идентификатор группы записей (нужен ui)
    // value
    // raw_value все 8 байт кан сообщения
    //
    if (!exec_request(R"(
            CREATE TABLE IF NOT EXISTS diag
            (
                id         TEXT        ,
                time        TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                map         TEXT        ,
                type        INTEGER     ,
                group_id    INTEGER     ,
                value       TEXT

            );
        )")) {
        throw sqlitedb_error("cannot create db table");
    }

    if (!exec_request("PRAGMA journal_mode=WAL;")) {
        logerror << "cannot open db in WAL mode";
    }

    auto version = statement("PRAGMA user_version;").exec_r().column<int64_t>(0);
    loginfo << "db version = " << version;
    if (version == 0) {
        statement("ALTER TABLE diag ADD raw_value INTEGER;").exec();
        statement("PRAGMA user_version = 1;").exec();
        loginfo << "changed db version to 1";
    }
}


void sqlite_err_clb(const char* what) {
    logerror << what;
}

void TsdiagSqliteStorage::add_event(std::string_view id, std::string value, std::string_view map, int group_id, uint64_t raw_value)
{
    static Statement stm(
        "INSERT INTO diag (id, map, group_id, value, type, raw_value) VALUES (?, ?, ?, ?, 0, ?);", this);

    stm.exec_no_throw(sqlite_err_clb, id, map, group_id, value, raw_value);
}

// похоже такое не надо
/*
void TsdiagSqliteStorage::upd_state(std::string_view id, std::string value, std::string_view map, int group_id)
{
    static Statement stm_insert(
        "INSERT INTO diag (id, map, group_id, value, type) SELECT ?1, ?2, ?3, ?4, 1 WHERE NOT EXISTS(SELECT 1 FROM diag WHERE id = ?1 AND type = 1);", this);

    static Statement stm_update("UPDATE diag SET value = ?1, time = datetime() WHERE id = ?2 AND type = 1;", this);

    stm_insert.exec_no_throw(sqlite_err_clb, id, map, group_id, value);
    stm_update.exec_no_throw(sqlite_err_clb, value, id);
}
*/

long get_time_point_ms() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}

void TsdiagSqliteStorage::begin_transaction_once() {
    static Statement stm("BEGIN TRANSACTION;", this);

    if (begin_transaction_time == 0) {
        stm.exec_no_throw(sqlite_err_clb);
        begin_transaction_time = get_time_point_ms();
    }

}

long TsdiagSqliteStorage::commit_if_need() {
    static Statement stm("COMMIT;", this);

    long res = 0;
    if (begin_transaction_time != 0) {
        stm.exec_no_throw(sqlite_err_clb);
        res = get_time_point_ms() - begin_transaction_time;
        begin_transaction_time = 0;
    }
    return res;
}
