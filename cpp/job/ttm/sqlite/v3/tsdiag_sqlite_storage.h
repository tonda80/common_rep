#ifndef TSDIAG_SQLITE_STORAGE_H
#define TSDIAG_SQLITE_STORAGE_H

#include <string_view>

#include "sqlite_db.h"

class TsdiagSqliteStorage : public SqliteDbBase
{
public:
    TsdiagSqliteStorage() = default;
    void init(const std::string& dbpath, int db_limit_day);

    void add_event(std::string_view id, std::string value, std::string_view map, int group_id, uint64_t raw_value);
    //void upd_state(std::string_view id, std::string value, std::string_view map, int group_id);


public:
    void begin_transaction_once();      // no thread-safe
    long commit_if_need();

private:
    void init_tables();

    long begin_transaction_time = 0;
};

#endif // TSDIAG_SQLITE_STORAGE_H
