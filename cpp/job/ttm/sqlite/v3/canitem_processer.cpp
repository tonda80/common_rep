#include "canitem_processer.h"

#include <map>
#include <algorithm>

#include "mnt2app.h"
#include "filesystem_wrapper.h"
#include "expression.h"


#include <zmqpb.h>
#include <global_conf.h>


// набор функций (и стат. данных для них) для проверки необходимости процессить item
namespace Filter {
    static std::map<std::string, std::string> last_values;      // key: last_value
    static std::map<std::string, long> last_pub_times;   // key: last_pub_time
    static int pub_timeout = -1;

    bool check_change_and_remember(const std::string& key, const std::string& value) {
        const auto it = last_values.find(key);
        bool res = it == last_values.end() || it->second != value;
        last_values[key] = value;
        //logdebug << "[check_change_and_remember] " << key << " " << value << " " << res;
        return res;
    }

    bool check_pub_time(const std::string& key) {
        const auto it = last_pub_times.find(key);
        bool res = it == last_pub_times.end() || it->second < (utils::timestamp_sec() - pub_timeout);
        //logdebug << "[check_pub_time_and_remember_if_true]" << pub_timeout << " " << key << " " << res << " " << last_pub_times[key];
        return res;
    }

    void remember_pub_time(const std::string& key) {
        last_pub_times[key] = utils::timestamp_sec();
    }
}


struct ConfigParameter {
    std::string id;
    //std::string desc;
    std::string flags;      // zZhd +t(костыль для шин)
    int group_id;
    // dtc
    std::string dm1_name;
    int spn;
    int fmi;
    //
    std::set<std::string> placeholders;
    //
    int base_spn;   // костыль для параметров шин

    ConfigParameter(const Json::Value& p) {
        id = p.get("id", "").asString();
        //desc = p.get("desc", "").asString();
        flags = p.get("flags", "").asString();
        group_id = p.get("groupId", -1).asInt();

        dm1_name = p.get("dm1Name", "").asString();
        spn = p.get("spn", 0).asInt();
        base_spn = spn;
        fmi = p.get("fmi", 0).asInt();

        init_placeholders(p);
    }

    bool is_dtc() const {
        return !dm1_name.empty();
    }

    bool has_flag(char f) const {
        return flags.find(f) != std::string::npos;
    }

    bool has_placeholder(std::string_view p) const {
        return std::find(placeholders.begin(), placeholders.end(), p) != placeholders.end();
    }

private:
    void init_placeholders(const Json::Value& par) {
        for (const auto& par_name : {"desc", "err_code", "advice"}) {
            std::string check_str = par.get(par_name, "").asString();
            std::string::size_type start = check_str.find("{");
            while (start != std::string::npos) {
                auto end = check_str.find("}", start+2);
                if (end == std::string::npos  ) {
                    throw mnt2app_error("[ConfigParameter.init_placeholders] bad description of "+id);
                }
                else {
                    placeholders.emplace(check_str.substr(start+1, end-start-1));
                    start = check_str.find("{", end+1);
                }
            }
        }
        //if (!placeholders.empty()) {auto l = logdebug; l << id << " placeholders: "; for (auto& p : placeholders) l << p << " ";}
    }
};


class ConfigDictionary {
private:
    std::map<std::string, ConfigParameter> parameters;              // id(canItemName) : ConfigParameter
    std::map<std::string, dm1_par_container> dm1_parameters;        // dm1CanItemName : {spn : ConfigParameter}

    std::map<uint32_t, std::set<uint32_t>> can_index_filter;        // can_addr : [can_indexes]

    std::map<std::string, Expression> expressions;                  // par.id : expression

    void add_dm1_parameter(ConfigParameter&& par) {
        if (par.spn < 0) {
            logerror << "parameter has dm1Name " << par.dm1_name << ", but has no spn, ignoring it";
            return;
        }
        auto range = dm1_parameters[par.dm1_name].equal_range(static_cast<uint>(par.spn));
        for (auto it = range.first; it != range.second; ++it) {
            if (it->first == static_cast<uint>(par.fmi)) {
                logerror << "spn " << par.spn << " fmi " << par.fmi << " for " << par.dm1_name << " repeated, ignoring it";
                return;
            }
        }
        if (par.has_flag('t')) {
            // костыль, особым образом обрабатываем параметры для шин, добавляем в конфиг 24 параметра вместо 1-го
            // так, чтобы не иметь в конфиге 10*24 одинаковых параметра
            // TODO придумать что-то покрасивее, может просто обобщить
            auto base_id = par.id;
            for(int i = 0; i < 24; ++i) {
                par.id = base_id + "_" + std::to_string(i);
                par.spn = par.base_spn + i;
                dm1_parameters[par.dm1_name].emplace(static_cast<uint>(par.spn), par);
            }
        }
        else {
            dm1_parameters[par.dm1_name].emplace(static_cast<uint>(par.spn), std::move(par));
        }
    }

    void can_index_filter_init(const Json::Value& json_obj) {
        for (const auto& k : json_obj.getMemberNames()) {
            uint32_t index = static_cast<uint32_t>(std::stoul(k));
            for (const auto& a : json_obj[k]) {
                uint32_t address = static_cast<uint32_t>(std::stoul(a.asString(), nullptr, 16));
                auto it = can_index_filter.find(address);
                if (it == can_index_filter.end()) {
                    it = can_index_filter.emplace(std::make_pair(address, decltype(can_index_filter)::mapped_type())).first;
                }
                it->second.insert(index);
            }
        }
        //for (const auto& e : can_index_filter) {auto log = logdebug; log << std::hex << e.first << ": " << std::dec; for (const auto& ee : e.second) log << ee << " ";}
    }

    void params_init(const Json::Value& json_params) {
        int cnt = 0;
        for (const auto& json_par : json_params) {
            ConfigParameter par(json_par);

            if (par.id.empty()) {
                logerror << "empty parameter id, ignoring it";
                continue;
            }
            if (parameters.count(par.id)) {
                logerror << "parameter id \"" << par.id << "\" repeated, ignoring it";
                continue;
            }

            try_add_expression(par, json_par.get("expr", "").asString());

            if (par.is_dtc()) {
                add_dm1_parameter(std::move(par));
            }
            else {
                parameters.emplace(par.id, std::move(par));
            }

            ++cnt;
        }

        auto q = parameters.size();
        for (const auto& p : dm1_parameters) {
            q += p.second.size();
        }
        loginfo << "readed " << cnt << " parameters; total " << q;
    }

    void try_add_expression(const ConfigParameter& par, const std::string& str) {
        if (str.empty()) {
            return;
        }
        auto [it, _] = expressions.emplace(par.id, str);

        auto res = it->second.calculate(1);       // тестируем, чтобы упасть тут а не во время работы
        logdebug << "added expression '" << it->second.source_string() << "' for " << par.id << " => " << res;
    }

public:
    void init(const std::string& cfg_path) {
        std::string err;
        auto dict_config = utils::read_json(cfg_path, err);
        if (dict_config.get() == nullptr) {
            throw mnt2app_error(err);
        }
        if (!dict_config) {
            throw mnt2app_error(err);
        }

        params_init((*dict_config)["params"]);

        try {
            can_index_filter_init((*dict_config)["can_index_filter"]);
        }
        catch (std::exception& e) {
            throw mnt2app_error(std::string("wrong json can_index_filter: ") + e.what());
        }
    }

    const dm1_par_container* dm1_parameters_ptr(const std::string& name) {
        const auto it = dm1_parameters.find(name);
        if (dm1_parameters.find(name) == dm1_parameters.end()) {
            return nullptr;
        }
        return &it->second;
    }

    const ConfigParameter* dm1_parameter_ptr(const dm1_par_container& par_map, uint spn, uint fmi) {
        auto range = par_map.equal_range(spn);
        for (auto it = range.first; it != range.second; ++it) {
            if (it->second.fmi == -1 || it->second.fmi == static_cast<int>(fmi)) {
                return &it->second;
            }
        }
        return nullptr;
    }

    const ConfigParameter* parameter_ptr(const std::string& name) {
        const auto it = parameters.find(name);
        if (parameters.find(name) == parameters.end()) {
            return nullptr;
        }
        return &it->second;
    }

    bool is_needed_canindex(const pb::CanItem& item) const {
        const auto it = can_index_filter.find(item.address());
        if (it == can_index_filter.end() || it->second.find(item.canindex()) != it->second.end()) {
            return true;
        }
        return false;
    }

    const Expression* get_expression(const ConfigParameter& par) {
        auto it = expressions.find(par.id);
        if (it == expressions.end()) {
            return nullptr;
        }
        return &(it->second);
    }

};

static ConfigDictionary config_dict;

CanItemHandler::T_storage_ptr CanItemHandler::diag_storage;


_filesystem::path db_dir() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbDirPath", ".").asString();
        path = _filesystem::canonical(name);
    }
    return path;
}


std::string CanItemHandler::db_path() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbName", "tsdiag.db").asString();
        path = db_dir()/name;
    }
    return path;
}

std::string CanItemHandler::db_old_path() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbOldName", "tsdiag_old.db").asString();
        path = db_dir()/name;
    }
    return path;
}

// helper
void remove_if_exists(std::string_view p) {
    if (_filesystem::exists(p)) {
        _filesystem::remove(p);
    }
}

void CanItemHandler::db_rotate() {
    uint64_t rotate_size = Mnt2App::instance().config().get("dbRotateSizeMb", 0).asUInt();
    if (rotate_size == 0) {
        return;
    }
    loginfo << "db rotate size " << rotate_size;

    rotate_size *= 1024*1024;
    uint64_t db_size = 0;
    try {
        db_size = _filesystem::file_size(db_path());
    }
    catch (...) {
        return;
    }

    if (db_size > rotate_size) {
        logwarning << "db will be rotated (" << db_size << ")";

        std::string tmp_path = db_path() + ".bkp";
        remove_if_exists(tmp_path);

        _filesystem::rename(db_path(), tmp_path);
        remove_if_exists(db_old_path());
        _filesystem::rename(tmp_path, db_old_path());

        logwarning << "db was rotated";
    }
}

std::string CanItemHandler::par_dict_path() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dictName", "par_dict").asString();
        int matrix_id = GlobalConf::instance().root().get("EnableCanMatrixId", -999).asInt();
        path = _filesystem::absolute(name + std::to_string(matrix_id) + ".json");
    }
    return path;
}

void CanItemHandler::init() {
    config_dict.init(par_dict_path());

    db_rotate();
    diag_storage = std::make_unique<TsdiagSqliteStorage>();
    int db_limit_day = Mnt2App::instance().config().get("dbRotateDay", 0).asInt();
    diag_storage->init(db_path(), db_limit_day);

    Filter::pub_timeout = Mnt2App::instance().config().get("pubTimeout", 5).asInt();
}


void CanItemHandler::process() {
    for (const auto& item : data.items()) {
        extra_data_container extra_data;

        if (const auto map_ptr = config_dict.dm1_parameters_ptr(item.name())) {
            process_dm_message(*map_ptr, item, extra_data);
        }
        else if (const auto par_ptr = config_dict.parameter_ptr(item.name())) {
            process_parameter(*par_ptr, item, extra_data);
        }
        else {
            //logdebug  << "[CanItemHandler::process] ignored: " << item.name() << " " << item.address() << " " << item.comment();
        }
    }

    auto transaction_time = diag_storage->commit_if_need();
    if (transaction_time > 0) {
        logdebug << "[CanItemHandler::process] canitems processing time " << transaction_time;
    }
}


std::tuple<std::string, std::string> get_value(const ConfigParameter& par, const pb::CanItem& item, extra_data_container& extra_data) {
    uint64_t parsed_value = item.parsedvalue();

    std::string value;
    std::string filter_key;
    if (par.id == "TIRE_PRESS") {   // для давления шин значение получаем сами, и учитываем что в одном кан сообщении приходят значения для разных колес
        auto value_num = parsed_value & 0xff;
        auto wheel_num = parsed_value >> 8 & 0xff;

        std::string wheel_num_str = utils::hex(wheel_num);
        extra_data["wheel_num"] = wheel_num_str;

        value = std::to_string(value_num/25.0);     // переводим в бары x*4000/100000
        filter_key = par.id + wheel_num_str;
    }
    else {
        if (par.is_dtc()) {
            value = "1";
        }
        else if (auto expr = config_dict.get_expression(par)) {
            try {
                value = expr->calculate(parsed_value);
            }
            catch (expression_error& e) {
                logerror << "cannot calculate value for " << par.id << " with value=" << parsed_value << ": " << e.what();
                value = std::to_string(parsed_value);
            }
        }
        else {
            value = std::to_string(parsed_value);
        }
        filter_key = par.id;
    }

    extra_data["value"] = value;

    return std::make_tuple(value, filter_key);
}


// helper
std::string extra_data_to_string(const extra_data_container& extra_data) {
    std::stringstream ss;
    Json::Value root;
    for (const auto& e : extra_data) {
        root[e.first] = e.second;
    }
    utils::json_fast_writer()->write(root, &ss);
    return ss.str();
}

void CanItemHandler::process_parameter(const ConfigParameter& par, const pb::CanItem& item, extra_data_container& extra_data) {
    if (!config_dict.is_needed_canindex(item)) {
        logdebug  << "[CanItemHandler::process_config_parameter] ignored by inappropriate canindex: " << item.canindex() << " " << std::hex << item.address();
        return;
    }

    if (par.has_placeholder("canid")) {
        extra_data["canid"] = utils::hex(item.address());
    }

    logdebug  << "[CanItemHandler::process_config_parameter] will process " << par.id << " " << par.flags;

    auto [value, filter_key] = get_value(par, item, extra_data);
    bool is_changed = Filter::check_change_and_remember(filter_key, value);

    std::string extra_data_str = extra_data_to_string(extra_data);      // прямо перед отправкой

    bool pub_flag = par.has_flag('Z');
    pub_flag = pub_flag || (par.has_flag('z') && (is_changed || Filter::check_pub_time(filter_key)));
    if (pub_flag) {
        publish_message(par, item, value, extra_data_str);
        Filter::remember_pub_time(filter_key);
    }

    if (par.has_flag('h') || (par.has_flag('d') && is_changed)) {
        diag_storage->begin_transaction_once();
        diag_storage->add_event(par.id, value, extra_data_str, par.group_id, item.rawvalue());
    }
    //diag_storage->upd_state(par.id, value, extra_data_str, par.group_id);
}

// helper. n в диапазоне [1, 8], считаем что байт 1 соответствует старшему байту нашего RawValue
uint _b(uint64_t src, uint8_t n, uint8_t mask=0xff) {
    return src >> 8*(8 - n) & mask;
}

void CanItemHandler::process_dm_message(const dm1_par_container& par_map, const pb::CanItem& item, extra_data_container& extra_data) {
    const uint64_t& v = item.rawvalue();
    uint spn = (_b(v, 5, 0xe0) << (2*8-5)) | (_b(v, 4) << 1*8) | _b(v, 3);
    uint fmi = _b(v, 5, 0x1f);

    auto par_ptr = config_dict.dm1_parameter_ptr(par_map, spn, fmi);
    if (!par_ptr) {
        logdebug << "unknown spn " << spn << " fmi " << fmi << " for CanItem " << item.name();
        return;
    }
    const ConfigParameter& par = *par_ptr;

    if (par.has_placeholder("spn")) {
        extra_data.emplace("spn", std::to_string(spn));
    }
    if (par.has_placeholder("fmi")) {
        extra_data.emplace("fmi", std::to_string(fmi));
    }
    if (par.has_flag('t')) {
        extra_data.emplace("N", std::to_string(par.spn - par.base_spn + 1));
    }

    process_parameter(par, item, extra_data);
}

void CanItemHandler::publish_message(const ConfigParameter& par, const pb::CanItem& /*item*/, const std::string& value, const std::string& extra_data_str) {
    std::stringstream ss;
    Json::Value root;

    root["id"] = par.id;
    root["map"] = extra_data_str;
    root["groupId"] = par.group_id;
    root["value"] = value;

    utils::json_fast_writer()->write(root, &ss);

    //logdebug  << "send_to_zmq " << ss.str();
    zmqpb::send_mnt_msg({"UI"}, "tsdiagevent", ss.str(), Mnt2App::instance().router_client());
}


