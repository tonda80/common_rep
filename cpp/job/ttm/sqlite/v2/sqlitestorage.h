#ifndef SQLITESTORAGE_H
#define SQLITESTORAGE_H

#include "sqlitedbbase.h"


class SqliteStorage : public SqliteDbBase
{
public:
	SqliteStorage();

	void add_event(const std::string& event_raw, const std::string& event_desc);

private:
	void prepare_statements();
	SqliteStatement add_event_stmt;
};

#endif // SQLITESTORAGE_H
