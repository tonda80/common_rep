#include "sqlitestorage.h"

#include "lib/logger/logger.h"


class Logger : public ILogger
{
	const std::string tag = "[SqliteStorage] ";
public:
	void debug(const std::string& msg) { logdebug << tag << msg; }
	void info(const std::string& msg) { loginfo << tag << msg; }
	void error(const std::string& msg) { logerror << tag << msg; }
	void fatal(const std::string& msg) {
		error(msg);
		throw std::runtime_error("fatal error: " + tag + msg);
	}
};


SqliteStorage::SqliteStorage()
	: SqliteDbBase(std::make_unique<Logger>())
{
	open("diagnostic.db");

	if (!exec_request(R"(CREATE TABLE IF NOT EXISTS tbl
			(
				id			INTEGER		PRIMARY KEY AUTOINCREMENT,
				timestamp	TIMESTAMP	NOT NULL DEFAULT CURRENT_TIMESTAMP,
				delivered	INTEGER		DEFAULT 0,
				event_raw 	BLOB,
				event_desc	BLOB
			);
		)")) {
        logger->fatal("cannot create db table");
    }

    prepare_statements();
}

void SqliteStorage::prepare_statements()
{
	add_event_stmt.init("INSERT INTO tbl (event_raw, event_desc) VALUES (?, ?);", this);
}

void SqliteStorage::add_event(const std::string& event_raw, const std::string& event_desc)
{
	if (add_event_stmt.reset() &&
		add_event_stmt.bind(1, event_raw) && add_event_stmt.bind(2, event_desc) &&
		add_event_stmt.step() == SQLITE_DONE
	) {
		logger->info("added event " + event_raw);
	}
	else {
		logger->error("cannot add event " + event_raw);
	}
}
