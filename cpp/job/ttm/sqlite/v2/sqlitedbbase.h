#ifndef SQLITEDBBASE_H
#define SQLITEDBBASE_H

#include <sqlite3.h>
#include <string>
#include <memory>
#include <vector>


struct Noncopyable {
	Noncopyable() = default;
    ~Noncopyable() = default;
	Noncopyable(const Noncopyable&) = delete;
    Noncopyable& operator=(const Noncopyable&) = delete;
};


// untying from app logging
class ILogger
{
public:
	virtual ~ILogger() = default;
	virtual void debug(const std::string& msg) = 0;
	virtual void info(const std::string& msg) = 0;
	virtual void error(const std::string& msg) = 0;
	virtual void fatal(const std::string& msg) = 0;
};
// and simple convenient way to make single log strings (c++17)
class logstr final : public std::string
{
public:
    template<typename T> logstr& operator<<(const T& t) {
		if constexpr(std::is_convertible<T, std::string>()) {
			*this += t;
		}
		else {
			*this += std::to_string(t);
		}
        return *this;
    }
};


class SqliteDbBase : private Noncopyable
{
public:
	~SqliteDbBase() {close();}	// virtual?
	bool open(const char* dbname);
	bool close();

protected:
	SqliteDbBase(std::unique_ptr<ILogger>&&);

	using exec_callback_t = int(*)(void*,int,char**,char**);
	bool exec_request(const std::string&, exec_callback_t callback = nullptr, void* callback_arg = nullptr);		// slow simple way to exec request

	sqlite3* db;

	std::unique_ptr<ILogger> logger;

protected:
	class SqliteStatement : private Noncopyable
	{
	public:
		SqliteStatement() = default;
		bool init(const std::string& str, SqliteDbBase* pOwner);

		~SqliteStatement() {finalize();}
		bool finalize();

		bool reset();

		int step();

		template<typename T> bool bind(int pos, T value)
		{
			if (!statement) {
				return false;
			}

			int ret;
			if constexpr(std::is_same<T, int>()) {
				ret =  sqlite3_bind_int(statement, pos, value);
			}
			else if (std::is_same<T, std::string>()) {
				ret = sqlite3_bind_text(statement, pos, value.c_str(), value.size(), SQLITE_TRANSIENT);	// TODO? customize last arg
			}
			// TODO double,
			else {
				owner->logger->fatal("bind error");	// TODO compile time assert
				return false;
			}
			if (ret != SQLITE_OK) {
				owner->logger->error(logstr() << "bind statement error. Pos " << pos << ", value " << value << ", returned " << ret);
				return false;
			}
			return true;
		}

	private:
		SqliteDbBase* owner = nullptr;;
		sqlite3_stmt* statement = nullptr;
	};
};


#endif // SQLITESTORAGE_H
