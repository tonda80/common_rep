#include "sqlitedbbase.h"


SqliteDbBase::SqliteDbBase(std::unique_ptr<ILogger>&& logger_)
	: logger(std::move(logger_))
{}

bool SqliteDbBase::open(const char* dbname)
{
	if (sqlite3_open(dbname, &db) != SQLITE_OK) {
        logger->fatal(logstr() << dbname << " cannot be opened. " << sqlite3_errmsg(db));
        return false;
    }
    return true;
}

bool SqliteDbBase::close()
{
	if (!db) {
        return true;
    }

	int ret = sqlite3_close(db);
    if (ret == SQLITE_OK) {
        db = nullptr;
    }
    else {
        logger->error(logstr() << "Cannot close database, returned " << ret);
        return false;
    }

    return true;
}

bool SqliteDbBase::exec_request(const std::string& req, exec_callback_t callback/*=nullptr*/, void* callback_arg/*=nullptr*/)
{
    char *errmsg = nullptr;
    sqlite3_exec(db, req.c_str(), callback, callback_arg, &errmsg);
    if (errmsg) {
        logger->error(logstr() << req << " cannot be executed. " << errmsg);
        sqlite3_free(errmsg);
        return false;
    }
    return true;
}

bool SqliteDbBase::SqliteStatement::init(const std::string& str,  SqliteDbBase* pOwner)
{
	if (statement) {
		owner->logger->error("not null statement init");
		return false;
	}
	int ret = sqlite3_prepare_v2(pOwner->db, str.c_str(), -1, &statement, nullptr);
    if (!statement) {
        pOwner->logger->error(logstr() << str << " cannot be prepared. Returned " << ret);
        return false;
    }

    owner = pOwner;
    return true;
}

bool SqliteDbBase::SqliteStatement::finalize()
{
	if (!statement) {
		return false;
	}
	if (int ret = sqlite3_finalize(statement) != SQLITE_OK) {
		owner->logger->error(logstr() << "finalize error " << ret);
		return false;
	}
	return true;
}

bool SqliteDbBase::SqliteStatement::reset()
{
	if (!statement) {
		return false;
	}
	if (int ret = sqlite3_reset(statement) != SQLITE_OK) {
		owner->logger->error(logstr() << "reset error" << ret);
		return false;
	}
	return true;
}

int SqliteDbBase::SqliteStatement::step()
{
	if (!statement) {
		return false;
	}
	int ret = sqlite3_step(statement);
	if (ret != SQLITE_DONE && ret != SQLITE_ROW) {	// SQLITE_ROW means there is data
		owner->logger->error(logstr() << "step error" << ret);
	}
	return ret;
}
