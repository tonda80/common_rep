#include "lib/logger/logger.h"
//#include "lib/process_monitor/pmon_observer.h"
#include "zmqpb.h"
#include "lib/global_conf.h"
#include "sqlitestorage.h"


const std::string ModuleName = "DIAGN";
const std::string AppTag = "[mnt_diag] ";


int main(int argc, char** argv)
{
	loginfo << AppTag << "started";

	//ProcessMonitor::Observer::instance().wait_started();
	zmqpb::RouterClient rt(ModuleName);

	SqliteStorage storage;
	storage.add_event("bla", "qwa");
	storage.add_event("1212", "23232");

	loginfo << AppTag << "exiting";
	return 0;
}
