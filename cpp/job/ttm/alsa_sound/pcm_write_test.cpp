// g++ pcm_write_test.cpp -lpthread -lasound -o pcm_write_test
// apt install libasound2-dev


#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <termios.h>
#include <cstring>
#include <fstream>

#include <alsa/asoundlib.h>

#include "../common.h"


static const char* device = "default";
static const auto pcm_format = SND_PCM_FORMAT_S16_LE;
static const unsigned int channels = 1;
static const int pcm_format_divider = 2 * channels;
static const unsigned int rate = 22050;


snd_pcm_t* pcm_playback_handle(void)
{
    snd_pcm_t* handle;
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_open error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }
    if ((err = snd_pcm_set_params(handle,
                                  pcm_format,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  channels,
                                  rate,
                                  1,        // allow resampling
                                  500000)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_set_params error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }

    std::cout << "[pcm_playback_handler] OK" << std::endl;
    return handle;
}


int pOpenVoicePort(const char * port)
{
    struct termios lOptions;

    int lFd = open(port, O_RDWR | O_NDELAY);
    if (lFd != -1) {
        /* read operations are set to blocking according to the VTIME value */
        fcntl(lFd, F_SETFL, FNDELAY);

        tcgetattr(lFd, &lOptions);

        /* Set to 115200 */
        cfsetispeed(&lOptions, B115200);
        cfsetospeed(&lOptions, B115200);

        /* set to 8N1 */
        lOptions.c_cflag &= ~PARENB;
        lOptions.c_cflag &= ~CSTOPB;
        lOptions.c_cflag &= ~CSIZE;
        lOptions.c_cflag |= CS8;
        lOptions.c_iflag &= ~(INPCK | BRKINT |PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
        lOptions.c_iflag |= IGNBRK;
        lOptions.c_iflag &= ~(IXON|IXOFF|IXANY);
        /* set to RAW mode for input */
        //lOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        /* set to RAW mode for output */
        //lOptions.c_oflag &= ~OPOST;
        lOptions.c_oflag=0;
        lOptions.c_lflag=0;
        lOptions.c_cc[VMIN] = 0;
        lOptions.c_cc[VTIME] = 20;

        tcsetattr(lFd, TCSANOW, &lOptions);

        return(lFd);
    }
    return -1;
}


class WavFile {
    typedef struct {
      char           chunkID[4];
      long           chunkSize;

      short          wFormatTag;
      unsigned short wChannels;
      unsigned long  dwSamplesPerSec;
      unsigned long  dwAvgBytesPerSec;
      unsigned short wBlockAlign;
      unsigned short wBitsPerSample;
    } __attribute__((packed)) formatChunk;

    typedef struct {
      char           chunkID[4];
      long           chunkSize;
    } __attribute__((packed)) chunkStart;

    typedef struct {
      char           RIFFId[4];
      long           size;
      char           WAVEId[4];
      formatChunk    fmt;
      chunkStart data;
    } __attribute__((packed)) wavFormat;

    std::fstream fs;

public:
    WavFile(const std::string& file_name) {
        fs.open(file_name, std::fstream::in | std::fstream::binary);

        wavFormat f;
        fs.read(reinterpret_cast<char*>(&f), sizeof(wavFormat));
        P2(f.RIFFId);
        P2(f.size);
        P2(f.WAVEId);
        P2(f.fmt.chunkID);
		P2(f.fmt.chunkSize);
		P2(f.fmt.wChannels);
		P2(f.fmt.wFormatTag);
		P2(f.data.chunkID);
		P2(f.data.chunkSize);
    }

    bool is_ok() {
        return fs.is_open() && fs.good();
    }

    ~WavFile() {
        fs.close();
    }

    long get_next(char* buf, int n) {
        if (fs) {
            fs.read(buf, n);
            return fs.gcount();
        }
        return 0;
    }
};


void play_in_device(WavFile& w, int dev_fd)
{
    const int buf_size = 1600;
    char buf[buf_size];

    while (w.is_ok()) {

        auto need_wr = buf_size;
        auto wr = write(dev_fd, buf, need_wr);
        fsync(dev_fd);
        //std::cout << "[capture_thread] tried to write " << need_wr << std::endl;

    }   //while (work_flag)

    std::cout << "[capture_thread] finished" << std::endl;
}


void sleep_ms(int ms) {
    auto check_point = std::chrono::steady_clock::now();
    while (std::chrono::steady_clock::now() < check_point + std::chrono::milliseconds(ms)) {
        std::this_thread::yield();
    }
}

void print_timestamp_ms(const char* pref)
{
    auto check_point = std::chrono::steady_clock::now();
    auto tstamp_ms = std::chrono::duration_cast<std::chrono::milliseconds>(check_point.time_since_epoch()).count();
    static auto old_tstamp_ms = tstamp_ms;
    std::cout << pref << " " << tstamp_ms - old_tstamp_ms << std::endl;
    old_tstamp_ms = tstamp_ms;
}

void play_in_alsa(WavFile& w, snd_pcm_t* handle)
{
    const int buf_size = 16000;
    char buf[buf_size];

    while (w.is_ok()) {
        auto rd = w.get_next(buf, buf_size);
        if (rd <= 0) {
            break;
        }
        auto wr = snd_pcm_writei(handle, buf, rd/pcm_format_divider);
        std::cout << "__deb " << rd  << " " << wr << std::endl;
        print_timestamp_ms("after write");

        if (wr < 0) {
            auto rec_res = snd_pcm_recover(handle, wr, 1);
            if (rec_res < 0) {
                std::cerr << "[playback_thread] snd_pcm_recover error: " << snd_strerror(rec_res) << " " << rec_res << " " << wr << std::endl;
                break;
            }
        }

        sleep_ms(100);
    }


    std::cout << "[playback_thread] finished" << std::endl;
}



int main(int argc, const char** argv) {
    if (argc < 2) {
        std::cerr << "usage: pcm_write_test file.wav [device], exiting\n";
        return 1;
    }
    const char* wav_name = argv[1];
    WavFile wav_file(wav_name);
    if (!wav_file.is_ok()) {
        std::cerr << "bad file " << wav_name << ", exiting\n";
        return 1;
    }

    snd_pcm_t* playback_pcm = pcm_playback_handle();
    if (!playback_pcm) {
        std::cerr << "cannot get playback handler\n";
        return -1;
    }
    play_in_alsa(wav_file, playback_pcm);

    return 0;


    const char* serial_device_name = "/dev/ttyUSB1";
    if (argc > 2) {
        serial_device_name = argv[2];
    }

    int dev_fd = pOpenVoicePort(serial_device_name);
    if (dev_fd < 0) {
        std::cerr << "cannot open " << serial_device_name << std::endl;
        return -1;
    }
    std::cout << "opened " << serial_device_name << std::endl;






    close(dev_fd);
    std::cout << "device closed\n";
    snd_pcm_close(playback_pcm);
    std::cout << "pcm handle closed\n";

    std::cout << "\nexiting" << std::endl;

    return 0;
}
