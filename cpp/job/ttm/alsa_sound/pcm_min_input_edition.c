// gcc pcm_min_input_edition.c -pthread -lasound

#include <unistd.h>
#include <termios.h>
#include <pthread.h>

#include <alsa/asoundlib.h>

static char *device = "default";			/* playback device */


int g_work = 1;

snd_pcm_t *handle;
FILE * pFile;

snd_pcm_format_t format = SND_PCM_FORMAT_S16;
int bufsize = 16 * 1024 * 4;
//static int channels = 2;
//static int rate = 48000;

void hex_dump(const unsigned char* buf) {
	for (int i=0; i < 8; ++i) {
		printf("%02x ", (*(buf+i)));
	}
	putchar('\n');
}

void* pcm_reader(void* data) {
	//buf = calloc(1, snd_pcm_format_size(format, bufsize) * channels);
	const int buf_size = 1024;
	char buf[buf_size*2];	// todo
	int err;

	snd_pcm_start(handle);
	while (g_work) {

		err = snd_pcm_readi(handle, buf, buf_size);
		if (err < 0) {
			printf("snd_pcm_readi error: %s (%d)\n", snd_strerror(err), err);
			err = snd_pcm_recover(handle, err, 0);
			if (err < 0) {
				printf("snd_pcm_recover error: %s (%d), exiting\n", snd_strerror(err), err);
				break;
			}
			snd_pcm_start(handle);
		}
		else if (err > 0) {
			//printf("readed %d frames\n", err);
			hex_dump(buf);
			fwrite (buf, 2, err, pFile);

		}
		//putchar('*');
		//usleep(1000);
	}
}


void wait_escape() {
	struct termios initial_settings, new_settings;

	tcgetattr(0,&initial_settings);

	new_settings = initial_settings;
	new_settings.c_lflag &= ~ICANON;
	new_settings.c_lflag &= ~ECHO;
	new_settings.c_lflag &= ~ISIG;
	new_settings.c_cc[VMIN] = 0;
	new_settings.c_cc[VTIME] = 0;

	tcsetattr(0, TCSANOW, &new_settings);

	while(getchar() != 27) {
		usleep(100000);
	}

	tcsetattr(0, TCSANOW, &initial_settings);
}


int main(void)
{
	int err;

	printf("SND_PCM_NONBLOCK=%d; SND_PCM_ASYNC=%d\n", SND_PCM_NONBLOCK, SND_PCM_ASYNC);

	if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {		//SND_PCM_NONBLOCK
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}
	if ((err = snd_pcm_set_params(handle,
	                              SND_PCM_FORMAT_S16,
	                              SND_PCM_ACCESS_RW_INTERLEAVED,
	                              1,
	                              8000,
	                              1,
	                              500000)) < 0) {	/* 0.5sec */
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	pFile = fopen ("my_pcm.wav", "wb");

	//printf("__deb %d\n", snd_pcm_format_size(format, bufsize));


	pthread_t thread;
	if (pthread_create(&thread, NULL, pcm_reader, NULL)) {
		fprintf(stderr, "pthread_create error\n");
		return 1;
	}
	//pcm_reader(NULL);

	puts("thread started");
	wait_escape();

	g_work = 0;
	pthread_join(thread, NULL);

	puts("thread finished");
	snd_pcm_close(handle);
	fclose (pFile);

	return 0;
}
