#!/usr/bin/env bash

set -e
set -x

function efunc {
    local LOCAL_ARG="ll"
    echo "my arg quantity $#, args $1 $2 ..."
    echo "LOCAL_ARG=$LOCAL_ARG"
    return 0	# код завершения
}

efunc
efunc 1 2
echo "efunc returns $?, LOCAL_ARG=$LOCAL_ARG"

unused_res=`efunc YOU WILL NOT SEE THIS ARGUMENTS WITHOUT -x OPTION!`		# можно это назвать lazy call?
used_res=`efunc 3 4`
echo "such result is all echoed! <${used_res}>"
