import java.io.File;
import java.util.Set;
import java.util.HashSet;

// export PATH=$PATH:{DIR c javac}
// javac Main.java && java Main

public class Main
{
	public static void main (String[] args) throws Exception
	{
		C c = new C();

		int a = 12345;
		int b = 100;
		int cc = a/b;
		U.p(cc);

		c.f();

		// error -> c.ss = new HashSet<Integer>();
		// error too Set<Integer> ss = c.ss;
		// but
		Set<Integer> ss = c.get_ss();
		c.check(ss);
		ss.add(33);
		c.f();	//!

		U.p("---");
		C c2 = new C();
		U.p(c.get_i());
		c2.set_obj(c, 77);
		U.p(c2.ff());
		U.p(c.get_i());
	}
}

class C
{
	private Set<Integer> ss;
	Set<Integer> get_ss() {return ss;}
	void check(Set<Integer> oth) {U.p(oth == ss);}

	public C() {
		ss = new HashSet<Integer>();
		ss.add(1);
	}
	int a = 8;

	public void f() {
		for (Integer i : ss) {
			System.out.print(i);
			System.out.print(' ');
		}
		System.out.println();
	}

	// --
	private C obj = null;

	private int i = 0;
	public int get_i() {
		return i;
	}

	public void set_obj(C o, int i) {
		obj = o;
		obj.i = i;
	}

	public int ff() {
		return obj.i;
	}
}

class U
{
	public static <T> void p(T obj) {
		System.out.println(obj);
	}

	public static void sleep(int delay) {
		try {
			Thread.sleep(delay);
		}
		catch (InterruptedException e)
		{}
	}
}
