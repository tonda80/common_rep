import java.io.File;
import java.util.Set;
import java.util.HashSet;

public class Main
{
	public static void main (String[] args) throws Exception
	{
		C c = new C();

		c.f();

		// error -> c.ss = new HashSet<Integer>();
		// error too Set<Integer> ss = c.ss;
		// but
		Set<Integer> ss = c.get_ss();
		c.check(ss);
		ss.add(33);
		c.f();	// !
	}
}

class C
{
	private Set<Integer> ss;
	Set<Integer> get_ss() {return ss;}
	void check(Set<Integer> oth) {U.p(oth == ss);}

	public C() {
		ss = new HashSet<Integer>();
		ss.add(1);
	}
	int a = 8;

	public void f() {
		for (Integer i : ss) {
			System.out.print(i);
			System.out.print(' ');
		}
		System.out.println();
	}
}

class U
{
	public static <T> void p(T obj) {
		System.out.println(obj);
	}

	public static void sleep(int delay) {
		try {
			Thread.sleep(delay);
		}
		catch (InterruptedException e)
		{}
	}
}

