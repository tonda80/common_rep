import java.util.Collection;
import java.io.File;
import java.util.Iterator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


public class reflection
{
	
	public static void main (String[] args) throws Exception
	{
		C c = new C();
		
		U.p("c.getClass()==C.class is "+String.valueOf(c.getClass()==C.class));
		
		//C.static_method();
		C.class.getDeclaredMethod("static_method").invoke(null);
		//C.static_method2(77);
		C.class.getDeclaredMethod("static_method2", int.class).invoke(null, 77);
		
		//c.method();
		C.class.getDeclaredMethod("method").invoke(c);
		//c.method2(12);
		C.class.getDeclaredMethod("method2", float.class).invoke(c, 12);
		
		//Ifc impl = new Impl();
		//impl.sayHello();
		//impl.sayBye();
		InvocationHandler handler = new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				String mName = method.getName();
				if (mName.equals("sayHello")) {
					U.p("hello from reflection!");
				} else if (mName.equals("sayBye")) {
					U.p("bye from reflection!");
				}
				return null;
			}
		};
		Ifc o = (Ifc) Proxy.newProxyInstance(Ifc.class.getClassLoader(), new Class[] { Ifc.class }, handler);
		o.sayHello();
		o.sayBye();
	}
		
}

interface Ifc
{
	void sayHello();
	void sayBye();
}

class Impl implements Ifc
{
	@Override
	public void sayHello() {U.p("Hello, my dear friend!!!");}
	@Override
	public void sayBye() {U.p("Goodbye!!!");}
}

class C
{
	public C() {}
	public static void static_method() {U.p("U.static_method");}
	public static void static_method2(int i) {U.p("U.static_method2 "+String.valueOf(i));}
	public void method() {U.p("U.method");}
	public void method2(float f) {U.p("U.method2 "+String.valueOf(f));}
}

// ещё пример из андроид
/*
		if (api == 26) {
			try {
				Class lstClass = Class.forName("android.os.StrictMode$ViolationListener");
				StrictMode.class.getDeclaredMethod("setViolationListener", lstClass).invoke(null,
					Proxy.newProxyInstance(lstClass.getClassLoader(), new Class[] { lstClass }, new InvocationHandler() {
						@Override
						public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
							if (method.getName().equals("onViolation")) {
								Log.e("HsStrictMode", (String)args[0]);
							}
							return null;
						}
					}));
			}
			catch (Exception e) {
				Log.e("HsStrictMode", "setViolationListener error: "+e.toString());
			}
		}
*/

class U
{
	public static <T> void p(T obj) {
		System.out.println(obj);
	}
	
	public static <T> void pAll(Collection<T> coll) {pAll(coll, ", ");}
	public static <T> void pAll(Collection<T> coll, String dl) {
		Iterator<T> i = coll.iterator();
		while ( i.hasNext()) {
			System.out.print(i.next());		
			if ( i.hasNext()) {
				System.out.print(dl);				
			}
		}
		System.out.println();
	}

	public static void sleep(int delay) {
		try {
			Thread.sleep(delay);
		}
		catch (InterruptedException e)
		{}
	}
	
	public static void deleteDirectory(File dir) {
		if (dir != null) {
			if (!dir.isDirectory()) {
				U.p("deleteDirectory error: " + dir.getAbsolutePath() + " is not a directory");
				return;
			}

			try {
				for (File f : dir.listFiles()) {
					if (f.isDirectory()) {
						deleteDirectory(f);
					}
					else {
						f.delete();
					}
				}
				dir.delete();
			}
			catch (Exception e) {
				p("Cannot delete a directory " + dir.getName() + ": " + e.getMessage());
			}
		}
	}
	
	public static void deleteFile(File f) {
		if (f != null) {
			try {
				f.delete();
			}
			catch (Exception e) {
				p("Cannot delete a file " + f.getName() + ": " + e.getMessage());
			}
		}
	}
}
