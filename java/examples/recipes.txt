private static JSONObject readJsonFile(final String path) {
    try {
	File file = new File(path);
	BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
	byte[] buffer = new byte[(int) file.length()];
	stream.read(buffer);
	return new JSONObject(new String(buffer));
    } catch (Exception e) {
	return null;
    }
}
