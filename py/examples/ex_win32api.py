import win32gui
import win32process
import win32pdh

def findWndHndl(pid):
	def _clb(whndl, testPid):
		tid, pid = win32process.GetWindowThreadProcessId(whndl)
		#print tid, pid, whndl			
		if pid == testPid:
			res.append((whndl, win32gui.IsWindowVisible(whndl), win32gui.GetWindow(whndl, 4)))
			# from stackoverflow
			# BOOL is_main_window(HWND handle) { return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);}
			# It stops the search
			#return False
			
	res = []
	win32gui.EnumWindows(_clb, pid)
	return res
	
# copypaste	from http://code.activestate.com/recipes/303339/
def procids():
	#each instance is a process, you can have multiple processes w/same name
	junk, instances = win32pdh.EnumObjectItems(None,None,'process', win32pdh.PERF_DETAIL_WIZARD)
	#print '__deb', instances
	proc_ids = []
	for instance in set(instances):
		for inum in xrange(instances.count(instance)):
			#print '__deb', instance, inum
			hq = win32pdh.OpenQuery() # initializes the query handle 
			path = win32pdh.MakeCounterPath( (None,'process',instance, None, inum,'ID Process') )
			counter_handle=win32pdh.AddCounter(hq, path) 
			win32pdh.CollectQueryData(hq) #collects data for the counter 
			type, val = win32pdh.GetFormattedCounterValue(counter_handle, win32pdqh.PDH_FMT_LONG)
			proc_ids.append((instance,str(val)))
			win32pdh.CloseQuery(hq) 

	proc_ids.sort()
	return proc_ids

pss = procids()
for e in pss:
	print e
print 'Total', len(pss)

while 1:	
	try:
		pid = int(raw_input('pid: '))
	except ValueError, KeyboardInterrupt:
		exit(0)
	for e in findWndHndl(pid): print e
	