# A minimal SQLite shell for experiments

import sqlite3

con = sqlite3.connect(":memory:")
con.isolation_level = None
cur = con.cursor()

buffer = ""

print "Enter your SQL commands to execute in sqlite3."
print "Enter a blank line to exit."

while True:
	line = raw_input()
	if line == '!':
		break
	buffer += line
	if 1:#sqlite3.complete_statement(buffer):
		#print 'Go!'
		try:
			buffer = buffer.strip()
			cur.execute(buffer)
			#if buffer.lstrip().upper().startswith("SELECT"):
			#	print cur.fetchall()
		except sqlite3.Error, e:
			print "An error occurred:", e.args[0]
		
		print cur.fetchall()
		buffer = ""

con.close()

#'create table vsoft (name, prof, marr)'