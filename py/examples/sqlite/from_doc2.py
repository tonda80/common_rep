# A minimal SQLite shell for experiments

import sqlite3

con = sqlite3.connect(":memory:")
con.isolation_level = None
cur = con.cursor()

buffer = ""

print "Enter your SQL commands to execute in sqlite3."

cur.execute('create table tt (name TEXT, age INTEGER)')
while True:
	line = raw_input()
	if line == '!':
		print 'Exiting'
		break
	elif line == '!a':
		line = 'select * from tt'
		print '>> ', line
	
	line += ';'
	try:
		line = line.strip()
		cur.execute(line)
	except sqlite3.Error, e:
		print "An error occurred:", e.args[0]
	
	print cur.fetchall()
	
con.close()

#'create table vsoft (name, prof, marr)'