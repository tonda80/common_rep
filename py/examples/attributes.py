class J():
    def __init__(self, num=3):
        self.number = num
    def attr_add(self, suf):
        setattr(self, suf, 0)
        setattr(self, 'get'+suf, lambda : getattr(self, suf))
        setattr(self, 'set'+suf, lambda v: setattr(self, suf, v))
    def ext_func_add(self, ext_func):
        def f():
            return ext_func(self.number)
        setattr(self, ext_func.__name__, f)
        