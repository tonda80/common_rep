# coding=utf8

# possible runs
# python e_unittest.py FooTest.test_2
# python e_unittest.py FooTest

import unittest
import logging

logging.basicConfig(format='%(levelname)s|%(name)s \t%(message)s')
log = logging.getLogger('tests')
log.setLevel(logging.DEBUG)

def setUpModule():
	log.debug('setUpModule')

def tearDownModule():
	log.debug('tearDownModule')

class FooTest(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		log.debug('FooTest.setUpClass')
	@classmethod
	def tearDownClass(cls):
		log.debug('FooTest.tearDownClass')
	def setUp(self):
		log.debug('FooTest.setUp')
	def tearDown(self):
		log.debug('FooTest.tearDown')
	def test_1(self):
		log.debug('FooTest.test_1')
	def test_2(self):
		log.debug('FooTest.test_2')

class BarTest(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		log.debug('BarTest.setUpClass')
	@classmethod
	def tearDownClass(cls):
		log.debug('BarTest.tearDownClass')
	def setUp(self):
		log.debug('BarTest.setUp')
	def tearDown(self):
		log.debug('BarTest.tearDown')
	def test_1(self):
		log.debug('BarTest.test_1')
		#raise RuntimeError
	def test_2(self):
		log.debug('BarTest.test_2')

if __name__ == '__main__':
	unittest.main()