from Tkinter import *


class DemoPlace:
	xArea = 4
	yArea = 4
	xVelocityCoef = 1
	yVelocityCoef = 1
	
	def __init__(self):
		self.root = Tk()
		
		self.x = self.y = 100
		
		self.testObject = Label(text = 'Hello')
		self.testObject.place(x = self.x, y  = self.y, anchor = CENTER)
		
		#~ Label(text = 'Pack').pack()
		
		self.root.bind_class('Tk', "<Motion>", self.motionCallback)
		self.oldEventX = self.oldEventY = 1e6
		
						
	def motionCallback(self, event):
		testDX = DemoPlace.xArea + self.testObject.winfo_width()/2
		testDY = DemoPlace.yArea + self.testObject.winfo_height()/2
		
		if abs(self.x - event.x) < testDX and abs(self.y - event.y) < testDY:
			self.x += (event.x - self.oldEventX)*DemoPlace.xVelocityCoef
			self.y += (event.y - self.oldEventY)*DemoPlace.yVelocityCoef
			self.testObject.place(x = self.x, y  = self.y, anchor = CENTER)
			
		self.oldEventX, self.oldEventY = event.x, event.y



DemoPlace().root.mainloop()
