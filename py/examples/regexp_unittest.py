#coding=utf8

import re
import unittest

# Docs
# https://docs.python.org/2/library/re.html
# http://habrahabr.ru/post/115825/
# https://docs.python.org/2/library/unittest.html
# http://www.cs.tut.fi/~jkorpela/perl/regexp.html
# https://regex101.com/		cool interactive tool

# Notes regexp
# ------------------
# . ^ $ * + ? { [ ] \ | ( )		all meta symbols
# [a-c] [^12] [\s,]				class examples
#	внутри классов экранировать нужно лишь 4 символа ]\^-
# ^ $		start, end of string
# \d \D 	digit, non-digit
# \s \S 	whitespace, non-whitespace
# \w \W		letter or digit [a-zA-Z0-9_], [^a-zA-Z0-9_]
# \b \B		border of words (word is letters and ciphers)
# [^\W\d_]	только символы
# .			any (a new line with re.DOTALL only)
# *			any times. E.g. ca*t => ct, cat, caat, .. caaaaaaaaat
# +			more 0 times.
# ?			0 or 1 times. E.g. ca?t => ct, cat
# {m}{m,n}	m and from m to n times (if {,} m==0, n==inf). E.g. a/{1,3}b => a/b, a//b and a///b
# ?			after a quantifier is sign of lazy (a lazy regexp catches the shortest line)
# (group)	(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) it's corresponding any string with one month
# 			(?!const)+.*FIXMessage\b 		all non const FIXMessage
# 			(.)\1{7,}						all repeated symbols (more 8 times)

# compilation flags allow: ignore case, use the locale e.t.c
# match, search		=> MatchObject
# split				=> паттерном задаем разделитель
# findall			=> string list
# finfiter			=> MatchObject list
# MatchObject methods
# --
# group				=> string is corresponding re
# group(N)			=> value of N group, where N from [1,]
# start, end, span	=> begin, end, tuple of the coincidence
#


# Notes unitest
# ------------------
# assertEqual(a, b)			a == b
# assertNotEqual(a, b)		a != b
# assertTrue(x)				bool(x) is True
# assertFalse(x)			bool(x) is False
# assertIs(a, b)			a is b 					2.7
# assertIsNot(a, b)			a is not b 				2.7
# assertIsNone(x)			x is None 				2.7
# assertIsNotNone(x)		x is not None 			2.7
# assertIn(a, b)			a in b 					2.7
# assertNotIn(a, b)			a not in b 				2.7
# assertIsInstance(a, b)	isinstance(a, b) 		2.7
# assertNotIsInstance(a, b)	not isinstance(a, b) 	2.7

# assertRaises(exc, fun, *args, **kwds) 			fun(*args, **kwds) raises exc
# assertRaisesRegexp(exc, r, fun, *args, **kwds) 	fun(*args, **kwds) raises exc and the message matches regex r 	2.7
# assertAlmostEqual, assertNotAlmostEqual			round(a-b, 7) == 0
# assertGreater, assertGreaterEqual, assertLess, assertLessEqual
# assertRegexpMatches, assertNotRegexpMatches		r.search(s)
# assertItemsEqual(a, b) 							sorted(a) == sorted(b) and works with unhashable objs 	2.7
# assertDictContainsSubset(a, b) 					all the key/value pairs in a exist in b
# assertDictEqual, assertListEqual, assertMultiLineEqual, assertSequenceEqual, assertSetEqual, assertTupleEqual
#		The list of type-specific methods automatically used by assertEqual() are summarized in the following table.
#		Note that it’s usually not necessary to invoke these methods directly

# --------------------------------------

class RegExpDemo(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		print 'setUpClass'
	@classmethod
	def tearDownClass(cls):
		print 'tearDownClass'
	def setUp(self):
		print 'setUp for:', self.id()
	def tearDown(self):
		print 'tearDown for:', self.id()

	def test_match(self):
		reg_templ = re.compile(r'ca{2,4}t\d{2,2}')
		self.assertIsNotNone(reg_templ.match('caaat35'))
		self.assertIsNone(reg_templ.match('ccaaat35'))
		# or
		self.assertIsNotNone(re.match('cat', 'CAt', re.IGNORECASE))
		self.assertIsNone(re.match('cat', 'cAt'))

	def test_search(self):
		self.assertEqual((2,4), re.search(r'\d[a-d]', 'qw1cdv').span())

	def test_findall(self):
		self.assertEqual(['e4', 'g8'], re.findall(r'[a-h]\d', 'A pawn is standing on e4, the king has gone to g8'))

	def test_finditer(self):
		for i, mo in zip(xrange(1,9,2), re.finditer(r'\d', 'b1x5z7n4')):
			self.assertEqual(i, mo.start())

def setUpModule():
    print 'setUpModule'

def tearDownModule():
    print 'tearDownModule'


unittest.main()
