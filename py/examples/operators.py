
class A():
	def __init__(self, n=0):
		self.value = n
	def __add__(self, n=0):
		self.value += n
		return self.value
	def __radd__(self, n=0):
		self.value += n
		#return self.value
		
	def __gt__(self, n):
		return self.value>n	

a = A()

print a.value
print a + 3
print a.value
print 4 + a
print a.value

print a>0
print a>7
print a>10

print a, type(a)
a += 3		
print a, type(a)