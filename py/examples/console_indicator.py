# coding=utf8

from time import sleep
from sys import stdout

class ConsoleIndicator:
	def __init__(self, max_step, min_step=0, len_=30, sym='#', pref='Done [', postf=']'):
		self.k = float(len_)/(max_step - min_step)
		self.len = len_
		self.min_step = min_step
		self.sym = sym
		self.pref = pref
		self.postf = postf

	def update(self, step):
		q_sym = int(round(self.k*(step - self.min_step)))
		if q_sym < 0:
			q_sym = 0
		ind_str = '%%-%ds'%self.len%(q_sym*self.sym)
		print '\r%s%s%s'%(self.pref, ind_str, self.postf),
		stdout.flush()	# special for Linux

	def finish(self):
		print

if __name__ == '__main__':
	def indicator_test(max_step, min_step=0, **kw):
		ind = ConsoleIndicator(max_step, min_step=min_step, **kw)
		for i in xrange(min_step, max_step+1):
			ind.update(i)
			sleep(0.1)
		ind.finish()

	indicator_test(10)
	indicator_test(30, sym='*')
	indicator_test(100, sym='%', pref='how are things => |', postf='|')
