import argparse

# Using
# python args.py -h
# python args.py --foo ee --l q -o --l 1 --foo2 zzz

# Docs
# https://docs.python.org/2/library/argparse.html#module-argparse
# https://docs.python.org/2/howto/argparse.html
# http://habrahabr.ru/post/144416/

# Notes
# "--" or "-" is sign of optionality (without "required=True")
# --arg -a => args.arg ie "--" is stronger, also dest parameter may be defined, it evidently sets the atrribute name
# nargs - control of quantity of catched arguments (+ or * to catch all)

# print 'args: %s'%args.__dict__		simple and more or less pretty output of all arguments

# --------------------------------------

# common part for 2 methods of getting arguments (see below)
def create_parser():
	parser = argparse.ArgumentParser()

	parser.add_argument('--foo', '-f', help='Optional argument WITH a VALUE or None')
	parser.add_argument('--foo3', help='Optional argument may have DEFAULT VALUE', default='foo3')
	parser.add_argument('--foo2', action='store_const', const=42, help='Optional argument may have CONST VALUE or None')
	parser.add_argument('-o', '--opt', action='store_true', help='Optional BOOLEAN argument (FALSE is DEFAULT)')
	parser.add_argument('--p', '-pt', action='store_false', help='Optional BOOLEAN argument (TRUE is DEFAULT)')
	parser.add_argument('bar', help='Positional REQUIRED argument')
	parser.add_argument('-f4', required=True, help='Named REQUIRED argument')
	parser.add_argument('--l', action='append', help='It adds all such argumets into list')

	parser.add_argument('--delay', '-d', type = int, default = 2, help='How often to look')
	#parser.add_argument('enc_files', nargs='*',  help='Encoded files')

	return parser

# method 1
def get_args():
	return create_parser().parse_args()

# method 2
class MyArgs():
	def __init__(self):
		create_parser().parse_args(namespace = self)


# args = get_args()
args = MyArgs()

print 'Print demo only'
print '---------------'

def print_arg(name):
	try:
		print '%s \t\t== %s'%(name, args.__dict__[name])
	except KeyError:
		print 'No argument %s'%name

print_arg('bar')
print_arg('f')		# long name access only!
print_arg('foo')
print_arg('foo2')
print_arg('foo3')
print_arg('opt')
print_arg('p')
print_arg('l')
print_arg('f4')

#print_arg('')
