﻿from Tkinter import *
from random import randrange

width = 1300
height = 800
max_d = 100

def add_ov():
	x = randrange(width)
	y = randrange(height)
	d= randrange(max_d)
	color = '#%06x'%randrange(0x1000000)
	cnv.create_oval(x,y,x+d,y+d,fill = color)
	cnv.config(scrollregion=cnv.bbox(ALL))


root = Tk()
root.geometry('%dx%d+%d+%d'%(width+max_d, height+max_d+50, 20, 10))

sry = Scrollbar(root)
sry.pack(side=RIGHT, fill=Y)
srx = Scrollbar(root, orient=HORIZONTAL)
srx.pack(side=BOTTOM, fill=X)

cnv = Canvas(root, yscrollcommand=sry.set, xscrollcommand=srx.set, bg='white')
cnv.pack(fill=BOTH, expand=YES)

sry.config(command=cnv.yview)
srx.config(command=cnv.xview)

Button(text='Add',command=add_ov).pack(side=TOP)

for i in xrange(1000):
	root.after_idle(add_ov)

mainloop()
