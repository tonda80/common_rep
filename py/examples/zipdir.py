# coding=utf8

import zipfile
import os
import argparse

def zip_dir(dir_path, zf_path):
    with zipfile.ZipFile(zf_path, 'w', zipfile.ZIP_DEFLATED) as zf:
        # точка отсчета ФС внутри архива,
        #  если взять dir_path то в архиве как-бы будет содержимое директории
        #  а так будет и сама директория тоже
        zf_root = os.path.dirname(dir_path)


        for root, _, files in os.walk(dir_path):
            for f in files:
                file_path = os.path.join(root, f)
                zf.write(file_path, os.path.relpath(file_path, zf_root))

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help='Input dir path')
    parser.add_argument('zip', help='Output zip file path')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    zip_dir(args.dir, args.zip)
