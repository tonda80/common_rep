# coding=utf8

class B:
	def f(self):
		print 'B1.f'

class B1(object):
	def f(self):
		print 'B1.f'

class B2(object):
	def f(self):
		print 'B2.f'

#class C(B): 		# it'not working - TypeError: must be type, not classobj
#class C(B1):		# it's ok
class C(B2):		# it's ok
	def f(self):
		print 'C.f'
		super(C, self).f()

# --
c = C()
c.f()
