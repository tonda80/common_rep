class C:
	var = 5
	def m(self):
		print 'Usual method', self
	
	@classmethod
	def cm(cls):
		print 'Class method', cls, cls.var
	
	@staticmethod
	def sm():
		print 'Static method'
		
	def nsm():
		print 'Method from the class namespace. It can be called through class.__dict__ only!'
		
c = C()

c.m()
c.sm()
c.cm()
#c.nsm()	--> TypeError: nsm() takes no arguments (1 given)

#C.m()	--> TypeError: unbound method m() must be called with C instance as first argument (got nothing instead)
C.sm()
C.cm()
#C.nsm()	--> TypeError: unbound method nsm() must be called with C instance as first argument (got nothing instead)
#C.nsm(c)	--> TypeError: nsm() takes no arguments (1 given)
C.__dict__['nsm']()
