# coding=utf8

import math

# py3 скриптец для проверки cycle_clamp
# см fmod https://docs.python.org/3/library/math.html#math.fmod и его разницу с % (чье поведение как в сmath remainder)

def clamp(l, r, v):
	if v < l:
		return l
	elif v > r:
		return r
	return v

# g - расстояние между l и r при склейке
def cycle_clamp(l, r, v, g = 1):
	# не работает, не учитывает g при переходе
	# d = int(math.fmod(v - l, r - l + 1))
	# if d < 0:
		# return r + d
	# return l + d
	# а вот это подсмотренное - работает
	m = r - l + g
	return math.fmod(math.fmod(v - l, m) + m, m) + l

if __name__ == '__main__':
	while 1:
		l, r, v = map(float, input('left, right, value: ').split())
		print ('  cycle_clamp value = ', cycle_clamp(l, r, v))
		print ('  clamp value = ', clamp(l, r, v))
