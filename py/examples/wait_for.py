# coding=utf8

import time
import threading
import sys

def wait_for(obj, attr, state, timeout, time_step = 0.2):
	end_time = time.time() + timeout
	while 1:
		if obj.__dict__[attr] == state:
			return True
		if time.time() > end_time:
			return False
		time.sleep(time_step)

class CtrlCWaiter:
	def __init__(self):
		self.old_excepthook = sys.excepthook
		sys.excepthook = self.hook
		self.trigger = False

	def hook(self, exctype, value, traceback):
		if exctype == KeyboardInterrupt:
			print 'Ctrl-c is pressed'
		else:
			self.old_excepthook(exctype, value, traceback)
		self.trigger = True

	def unHook(self):
		sys.excepthook = self.old_excepthook

	def wait(self):
		while not self.trigger:
			raw_input()		# other way to catch the event is time.sleep
			# TODO send around the input string

if __name__ == '__main__':

	class C:
		def __init__(self): self.i = 0

	class TestThread(threading.Thread):
		def __init__(self, c):
			threading.Thread.__init__(self)
			self.c = c
			self.start()
		def run(self):
			while not g_ctrlcWaiter.trigger:
				self.c.i += 1
				print self.c.i
				time.sleep(1)

	g_ctrlcWaiter = CtrlCWaiter()
	c = C()
	TestThread(c)

	print wait_for(c, 'i', 3, 10)
	#raise RuntimeError
	print wait_for(c, 'i', 17, 3)

	g_ctrlcWaiter.wait()
