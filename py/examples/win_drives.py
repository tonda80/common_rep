#coding=utf8

import win32api
import win32file

# cut-and-pasted from MSDN
DRIVE_TYPES = """
0 	Unknown
1 	No Root Directory
2 	Removable Disk
3 	Local Disk
4 	Network Drive
5 	Compact Disc
6 	RAM Disk
"""
print DRIVE_TYPES

for drive in win32api.GetLogicalDriveStrings().split ("\000"):
	if drive:
		print drive, "=>", win32file.GetDriveType(drive)
		