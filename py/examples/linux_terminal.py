# coding=utf8

import select
import sys
import tty
import termios

def extra_read():
	while select.select((in_,), (), (), 0)[0]:
		sys.stdout.write('_%d'%sys.stdin.read(1))

in_ = sys.stdin.fileno()
old_term_attr = termios.tcgetattr(in_)
try:
	tty.setraw(in_)
	while 1:
		if select.select((in_,), (), (), 3)[0]:
			c = sys.stdin.read(1)
			sys.stdout.write('-> %s (%d)\n\r'%(c, ord(c)))
			if c == '\x03':
				raise KeyboardInterrupt
		else:
			sys.stdout.write('not pressed\n\r')
finally:
	termios.tcsetattr(in_, termios.TCSANOW, old_term_attr)
