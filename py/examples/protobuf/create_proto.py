#coding=utf8

import lsh

ARGS = {
	'protoc' : r'D:\rep-s\ttrt\3rd\build\debug\Protobuf\bin\protoc.exe',
	'SRC_DIR' : r'D:\rep-s\ttrt\project\data-model-all\src\main\proto',
	'DST_DIR' : '.'
}

for in_proto_file in lsh.open_glob(lsh.path_join(ARGS['SRC_DIR'],'*.proto')):
	cmd = '{protoc} -I={SRC_DIR} --python_out={DST_DIR} {in_proto_file}'.format(in_proto_file = in_proto_file, **ARGS)
	lsh.run_tee(cmd.split())