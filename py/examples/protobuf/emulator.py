#coding=utf8

import zmq

import MessageHeaders_pb2
import MessageType_pb2
import MessageDictionary_pb2

from time import sleep
from sys import stdout

class Publisher:
	def __init__(self, zmq_context, topic_name, pub_endpoint):
		self.pub_socket = zmq_context.socket(zmq.PUB)
		self.pub_socket.bind(pub_endpoint)

		self.topic_name = topic_name

	def publishUpdate(self, message_type, body):
		header = MessageHeaders_pb2.MessageHeader()
		header.seqNumber = 0
		header.messageType = message_type

		self.pub_socket.send_multipart((self.topic_name, header.SerializeToString(), body))

class Omms:
	def __init__(self, zmq_context, pub_endpoint):
		self.publisher = Publisher(zmq_context, 'I.G', pub_endpoint)
		self.orderId = 0

	def send_ER(self):
		message_type = MessageType_pb2.EXECUTION_REPORT
		er = MessageDictionary_pb2.ExecutionReport()

		self.orderId += 1

		er.OrderID = self.orderId
		er.ClOrdID = '1'
		er.ExecID = 1
		er.TransactTime = 1
		er.ExecType = MessageDictionary_pb2.EXEC_TYPE_NEW
		er.OrderStatus = MessageDictionary_pb2.ORD_STATUS_NEW
		er.AccountID = 1
		er.UserID = 1
		er.OrderCreationTime = 1

		self.publisher.publishUpdate(message_type, er.SerializeToString())


zmq_context = zmq.Context()
omms = Omms(zmq_context, 'tcp://*:9834')
while 1:
	omms.send_ER()
	sleep(1)
	print '.',
	stdout.flush()
