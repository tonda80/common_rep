import threading
import time

gWork = True

def ff1():
	while gWork:
		time.sleep(1)
		e.wait()
		print 'ff1',

def ff2():
	cnt = 0
	while gWork:
		time.sleep(1)
		print 'ff2',
		cnt+=1
		if cnt == 4:
			e.set()
			print 'e.set()'

e = threading.Event()
tt1 = threading.Thread(target=ff1, args=())
tt2 = threading.Thread(target=ff2, args=())

print tt1.name, tt1.ident
print tt2.name, tt2.ident

print threading.enumerate()
tt1.start()
tt2.start()
print threading.enumerate()
print tt1.name, tt1.ident
print tt2.name, tt2.ident

try:
	while 1:
		if tt1.isAlive():
			tt1.join(1)
		if tt2.isAlive():
			tt2.join(1)
except KeyboardInterrupt:
	print 'KeyboardInterrupt'
	gWork = False
	e.set()
