﻿from Tkinter import *

def comm1():
	print 'command1'
def comm2():
	print 'command2'

def hndl_btn(ev):
	menu_test.post(ev.x_root, ev.y_root)#

root = Tk()
root.geometry('640x200+200+100')
root.bind('<Button-3>', hndl_btn)

menu_test = Menu(root) 
menu_test.add_command(label = 'command1', command = comm1)
submenu = Menu(root)
menu_test.add_cascade(label = 'submenu', menu = submenu)
submenu.add_command(label = 'command1', command = comm1)
submenu.add_command(label = 'command2', command = comm2)
menu_test.add_separator()
menu_test.add_command(label = u'Выход', command = root.quit)

root.mainloop()
