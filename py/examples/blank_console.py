# coding=utf8

import argparse

class App:
	def __init__(self, args=None):
		self.args = self.parse_args(args)

	def parse_args(self, args):
		parser = argparse.ArgumentParser()
		parser.add_argument('-v', action='store_true', help='Verbose')
		#parser.add_argument('-t', '--target', required=True, choices=('generate', 'parse', 'both'), help='Target of launching')
		#parser.add_argument('--disable', nargs=2, help='Disable/enable device.', metavar=('ID','[1|0]'))
		parser.add_argument('-s', '--something', help='Something')
		return parser.parse_args(args)

		def verbose_print(self, msg):
			if self.args.v:
				print msg

	def main(self):
		print self.args.__dict__

if __name__ == '__main__':
	App().main()
