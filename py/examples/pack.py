from Tkinter import *

root = Tk()
root.title('Pack test'); root.geometry('640x480')

i = 0
for i_side in (TOP, LEFT, RIGHT, BOTTOM):
	for i_anch in (N, E, S, W, CENTER):
		Label(root, text = '%d_%s_%s'%(i, i_anch, i_side)).pack(side = i_side, anchor = i_anch)
		i += 1

w = Toplevel()
w.title('Pack test'); w.geometry('640x480')

i = 0
for i_anch in (N, E, S, W, CENTER):
	for i_side in (TOP, LEFT, RIGHT, BOTTOM):
		Label(w, text = '%d_%s_%s'%(i, i_side, i_anch)).pack(side = i_side, anchor = i_anch)
		i += 1


root.mainloop()