# coding=utf8

import time
import multiprocessing

def proc_func():
	cnt = 0
	while 1:
		print cnt
		cnt += 1
		time.sleep(1)
		if cnt > 10:
			break


if __name__ == '__main__':

	p0 = multiprocessing.Process(target = proc_func)
	p0.start()

	raw_input('Enter to stop')
