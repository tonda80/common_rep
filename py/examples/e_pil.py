# coding=1251

from PIL import Image, ImageTk, ImageDraw
import uTkinter

# modes: 1, L, P, RGB, ...
w1,h1 = (500, 300)
img1 = Image.new('RGB', (w1, h1), 'white')
img2 = Image.new('1', (w1, h1))	# only '#_____' works here to set black bg

draw = ImageDraw.Draw(img1)
draw.line((0, 0, w1, h1), fill='#ff0000')
draw.line((0, h1, w1, 0), fill='#00ff00')
draw.arc((0, 0, w1/2, h1/2), 0, 90, fill='#0000ff')
draw.rectangle((w1/10,h1/10,w1/3,h1/3), outline='#ff0000')

draw = ImageDraw.Draw(img2)
draw.arc((0, 0, w1/2, h1/2), 0, 90, '#00000f')	# only '#_____' works here, default is white
draw.line((0, h1/5, w1, 2*h1/3), fill='#00000f')
draw.rectangle((w1/10,h1/10,w1/3,h1/3), fill='#ff0000')


del draw


# placing
w,h = (1024, 768)
wnd = uTkinter.Tk()
uTkinter.setWindowSize(wnd, w, h)

cnv1 = uTkinter.uCanvas(wnd)
pImg = ImageTk.PhotoImage(img1)
cnv1.create_image((w/4,h/4), image=pImg)


cnv2 = uTkinter.uCanvas(wnd)
bImg = ImageTk.BitmapImage(img2)
cnv2.create_image((w/4, h/4), image=bImg)

wnd.mainloop()