# coding=utf8

import websocket
import threading
from time import sleep

WS_ADDR = 'ws://10.17.131.24/fds/ws'

def debug(fmt, *a, **kw):
	pref = '__debug:'
	if a or kw:
		print pref, fmt.format(*a, **kw)
	else:
		print pref, fmt

def receivedThread(ws, stop):
	ws.settimeout(1)
	while not stop.is_set():
		try:
			recv_msg = ws.recv()
		except websocket.WebSocketTimeoutException:
			continue
		print 'Received: %s'%recv_msg

ws = websocket.create_connection(WS_ADDR)

stop = threading.Event();
threading.Thread(target=receivedThread, args=(ws,stop)).start()

msg = 'SEND /login\n{"LoginRequest":{"UserName":"TEST_PARTICIPANT_1","Password":"TEST_PARTICIPANT_1"}}\n'
debug(msg)
ws.send(msg)
#msg = "SEND " + "/login" + "\n" + '"{"LoginRequest":{"Password":"TEST_P…' + "\n"

try:
	while 1: sleep(1)
except KeyboardInterrupt:
	pass
stop.set()
print 'exit'