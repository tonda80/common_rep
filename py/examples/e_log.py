class Log:
	def __init__(self, out, level):
		self.out = out
		self.level = level
	def output(self, lvl, fmt, *a, **kw):
		if lvl > self.level:
			return
		self.out.write((fmt+'\n').format(*a, **kw))
	def error(self, fmt, *a, **kw):
		self.output(0, fmt, *a, **kw)
	def warn(self, fmt, *a, **kw):
		self.output(10, fmt, *a, **kw)
	def info(self, fmt, *a, **kw):
		self.output(20, fmt, *a, **kw)
	def debug(self, fmt, *a, **kw):
		self.output(30, fmt, *a, **kw)
	def trace(self, fmt, *a, **kw):
		self.output(40, fmt, *a, **kw)
	def blame(self, fmt, *a, **kw):
		if not isinstance(fmt, str):
			fmt = str(fmt)
		self.output(50, '__debug ' + fmt, *a, **kw)

log = Log(sys.stdout, 100)