from Tkinter import *

class Entrylimint(Entry):
	def __init__(self, root=None, min, max):
		Entry.__init__(self,root)
		
		self.__min = min
		self.__max = max
		
		self.num = None
		self.__corr_v = ''
		
		self.__var = StringVar()
		self.config(textvariable = self.__var)
		self.__var.trace('w', self.__limvar)
		
	def __limvar(self,n,i,m):
		text = self.__var.get()
		
		if text[0:2] == '0x': base = 16
		else: base = 10
		
		try: num = int(text, base)
		except ValueError:
			if text=='' or text=='0x':
				self.num = 0
				self.__corr_v = text
			else:	
				self.__var.set(self.__corr_v)
		else:
			if num<self.__min or num>self.__max:
				self.__var.set(self.__corr_v)
			else:
				self.__corr_v = text
				self.num = num
		
		
		
				
		