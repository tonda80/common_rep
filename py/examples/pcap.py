# coding=utf8

# pip install dpkt
# https://jon.oberheide.org/blog/2008/10/15/dpkt-tutorial-2-parsing-a-pcap-file/

import dpkt
import struct
import cPickle
import time

InpFile = r'D:\aberezin\job_issues\EPMBFIXSUP-628-micex_adapter\_user\logs\experiment00.pcap'
OutpFile0 = r'D:\aberezin\job_issues\EPMBFIXSUP-628-micex_adapter\_user\all_packets.txt'
InpFileDataLink = 113
UDP_ports = (17043,)#(17003, 17043, 16003,16043)
PickleFile = 'pcap.pckl'

init_time = time.time()

pcap = dpkt.pcap.Reader(open(InpFile, 'rb'))
assert pcap.datalink()==InpFileDataLink, 'This input file must have such datalink'		# http://www.tcpdump.org/linktypes.html


try:
	with open(PickleFile, 'rb') as f:
		unpck = cPickle.Unpickler(f)
		pcap_seq_nums = unpck.load()
except:
	# we want to find all seq_nums into the cap file
	pcap_seq_nums = []

	for ts, buf in pcap:
		ts0 = ts
		break

	pck_cnt = 0
	for ts, buf in pcap:
		pck_cnt += 1
		packet = dpkt.sll.SLL(buf)
		try:
			udp_packet = packet.ip.udp
		except AttributeError:
			continue
		if udp_packet.dport not in UDP_ports:
			continue

		seq_num = struct.unpack('<I', udp_packet.data[0:4])[0]
		pcap_seq_nums.append((seq_num, udp_packet.dport, pck_cnt, ts-ts0))

	with open(PickleFile, 'wb') as f:
		pck = cPickle.Pickler(f)
		pck.dump(pcap_seq_nums)
