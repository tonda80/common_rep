# coding=utf8

import click

@click.group()
def grp():
    pass


@grp.command('act1')
@click.argument('arg1')
@click.option('--opt1', help='Helpful description')
@click.option('--opt2', help='Another helpful description')
def action1(*a, **kw):
    print 'act1', a, kw

@grp.command('act2')
@click.argument('arg1')
@click.option('--opt1', help='Helpful description')
@click.option('--opt2', help='Another helpful description')
def action1(*a, **kw):
    print 'act2', a, kw

@grp.command('act3')
@click.option('--df', default='defval', show_default=True, help='some default value')
@click.option('--oo')
@click.option('--mo', multiple=True, help='some help')
@click.option('--no', nargs=2)
def act3(*a, **kw):
    print a, kw

if __name__ == '__main__':
    grp()
