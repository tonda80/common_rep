print '-- Example 1 --'

class ContextxMgr:
	def __init__(self):
		print 'ContextxMgr __init__'
	def __enter__(self):
		print 'ContextxMgr __enter__'
		try:
			return self.m
		except AttributeError:
			pass	# we can return nothing!
	def __exit__(self, *a):	# a == exc_type, exc_value, traceback
		print 'ContextxMgr __exit__'
	def open(self, *args):
		print 'open',args
		self.m = 'controlled object'
		return self


ctx = ContextxMgr()
with ctx.open(1,2,'any arguments') as smth:
	print 'with actions over', smth
	#raise RuntimeError('!')

print '-- Example 1.1 --'
with ContextxMgr() as smth:
	print 'type(smth)==', type(smth)

print '-- Example 2 --'

class C:
	def __init__(self, i):
		self.i = i
	def info(self):
		print 'i ==', self.i

class CHolder:
	def __init__(self, c):
		self.c = c

	def __enter__(self):
		self.c.i += 1
		#return self.c	# for as

	def __exit__(self, *a):
		print 'CH.__exit__', a
		self.c.i -= 1

c = C(1)
c.info()

with CHolder(c):
	print 'with',
	c.info()

c.info()
