# coding=utf8

import bisect

class C:
	def __init__(self, i, j):
		self.i=i
		self.j=j

	def __str__ (self):
		return 'C(%d)'%self.i
	def __repr__(self):
		return self.__str__()

	# one attribute
	def __cmp__(self, oth):
		return self.i - oth.i

	# another attribute and not object at all
	def __eq__(self, j):
		return self.j == j	#oth.


cont = []

bisect.insort(cont, C(19, 20))
bisect.insort(cont, C(10, 200))
bisect.insort(cont, C(12, 20))
bisect.insort(cont, C(14, 20))
bisect.insort(cont, C(17, 20))
bisect.insort(cont, C(13, 20))
bisect.insort(cont, C(11, 20))
bisect.insort(cont, C(10, 201))

print "__cmp__ is used for insertion"
print cont
print
print "__eq__ is used for checking"
for i in (20, 199, 0):
	print i in cont,
print
