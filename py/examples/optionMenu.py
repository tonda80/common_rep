from Tkinter import *

w = Tk()

options = ('red', 'green', 'blue')

sVar = StringVar(value = options[0])

def cmd(a):
	print 'cmd', a, type(a)

optMenu = OptionMenu(w, sVar, *options, command=cmd)
optMenu.pack()

w.mainloop()
