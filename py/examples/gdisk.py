# coding=utf8
'''
Доступ к файлам гугл-диска
https://developers.google.com/drive/v3/web/quickstart/python
https://habr.com/post/328248/

Пример получает список файлов и скачивает файл по id.

Нужно создать проект и получить oauth json (имя этого json файла указывается в flow_from_clientsecrets)
Далее при первом запуск скрипта подтверждаем права (задаются в SCOPES).
Впоследствии авторизуется через созданный credentials.json
'''

from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

import io
from apiclient.http import MediaIoBaseDownload

# Setup the Drive v3 API
SCOPES = 'https://www.googleapis.com/auth/drive.readonly'
		#'https://www.googleapis.com/auth/drive.metadata.readonly'
store = file.Storage('credentials.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('client_id.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('drive', 'v3', http=creds.authorize(Http()))

# Call the Drive v3 API
results = service.files().list(
    pageSize=10, fields="nextPageToken, files(id, name)").execute()

items = results.get('files', [])
if not items:
    print 'No files found.'
else:
    print 'Files: (%d)'%len(items)
    for item in items:
        #if item['name'].startswith('test'):
		#	print(item)
        print u'{0} ({1})'.format(item['name'], item['id'])

f_id = u'1kFIt37LbVhTT1nvLwmQi1awaipj-fWam290mklh8kn0'

#request = service.files().get_media(fileId=f_id)	# так можно скачать обычный файл
# гугл док же скачивается только через экспорт (application/pdf)
request = service.files().export_media(fileId=f_id, mimeType='text/csv')

fh = io.BytesIO()	#io.FileIO('downloaded_test.txt', 'wb')
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print 'Download %d%%.'%int(status.progress() * 100)
