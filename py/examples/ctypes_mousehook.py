import ctypes

# https://docs.python.org/2/library/ctypes.html
# http://www.unknowncheats.me/forum/c-and-c/83707-setwindowshookex-example.html						c example
# https://msdn.microsoft.com/en-us/library/windows/desktop/ms644990%28v=vs.85%29.aspx				SetWindowsHookEx function
# https://msdn.microsoft.com/en-us/library/windows/desktop/aa383751%28v=vs.85%29.aspx#INT_PTR		Windows Data Types

# typedef struct tagPOINT {
 # LONG x;
 # LONG y;
# } POINT, *PPOINT;
class POINT(ctypes.Structure):
	_fields_ = [
		('x', ctypes.c_long),
		('y', ctypes.c_long)
	]

# typedef struct tagMSLLHOOKSTRUCT {
  # POINT     pt;
  # DWORD     mouseData;
  # DWORD     flags;
  # DWORD     time;
  # ULONG_PTR dwExtraInfo;
# } MSLLHOOKSTRUCT, *PMSLLHOOKSTRUCT, *LPMSLLHOOKSTRUCT;
class MSLLHOOKSTRUCT(ctypes.Structure):
	_fields_ = [
		('pt', POINT),
		('mouseData', ctypes.c_int),
		('flags', ctypes.c_int),
		('time', ctypes.c_int),
		('dwExtraInfo', ctypes.POINTER(ctypes.c_ulong))
		]
PMSLLHOOKSTRUCT = ctypes.POINTER(MSLLHOOKSTRUCT)

user32dll = ctypes.windll.user32
kernel32dll = ctypes.windll.kernel32

# LRESULT __stdcall HookCallback(int nCode, WPARAM wParam, LPARAM lParam)
CLBFUNC = ctypes.WINFUNCTYPE(ctypes.c_longlong, ctypes.c_int, ctypes.c_long, ctypes.c_long)

def py_clb_func(nCode, wParam, lParam):
	print 'py_clb_func', nCode, wParam, lParam

	lParam =  ctypes.cast(lParam, PMSLLHOOKSTRUCT)
	print '\t', lParam.contents.pt.x, lParam.contents.pt.y, lParam.contents.mouseData

	if wParam == 0x207:	# middle button
		user32dll.UnhookWindowsHookEx(hook)
		print user32dll.PostMessageW(None, 0x0012, 0, 0)	# 0x12 WM_QUIT, generally any messages stops script

	# CallNextHookEx(_hook, nCode, wParam, lParam);
	return user32dll.CallNextHookEx(ctypes.c_void_p(), nCode, wParam, lParam)


clb_func = CLBFUNC(py_clb_func)

hook = user32dll.SetWindowsHookExW(14, clb_func, None, 0)
if not hook:
	print 'SetWindowsHookExW error', kernel32dll.GetLastError()


# while (GetMessage(&msg, NULL, 0, 0)) ;
Tarr = ctypes.c_int * 1000	# type for dummy
dummy = Tarr()
user32dll.GetMessageW(ctypes.pointer(dummy), None, 0, 0)	# any message stops the script

# it is possible, but it works strange
#import Tkinter
#Tkinter.Tk().mainloop()
