# coding=utf8

# echo "echo 1 > /proc/sys/vm/drop_caches" | sudo sh
# полезная команда для очистки кеша в линуксе, без неё файлы читаются повторно влет, что мешает эксперименту)
# https://unix.stackexchange.com/questions/87908/how-do-you-empty-the-buffers-and-cache-on-a-linux-system
# https://www.thomas-krenn.com/en/wiki/Linux_Page_Cache_Basics

import argparse
import os
import time

class TimeMeas:
	def __enter__(self):
		self.t = time.time()
	def __exit__(self, *a):
		print '%f seconds was spent'%(time.time()-self.t)

def parse_args():
	parser = argparse.ArgumentParser()

	parser.add_argument('-a', '--action', required=True, choices=('g', 't1', 't2', 't3', 't4'), help='Required actions: g(generate), t_(some reading test)')
	parser.add_argument('-s', '--size', type=int, help='Size of generated file in Mb')
	parser.add_argument('-p', '--path', help='Path to a file/dir')

	return parser.parse_args()

def create_giant_file(path, size_mb):
	with open(path, 'wb') as f:
		line_cnt = 0
		maxline = size_mb*1024*8
		while line_cnt < maxline:
			line_128b = '%025d: %s\n'%(line_cnt, 'x'*100)
			line_cnt += 1
			f.write(line_128b)

args = parse_args()

if args.action == 'g':
	path = os.path.join(args.path, 'py_test_giant_file_%d.txt'%args.size)
	create_giant_file(path, args.size)
elif args.action.startswith('t'):
	with TimeMeas():
		with open(args.path) as f:
			if args.action == 't1':
				bf = f.read()
			elif args.action == 't2':
				bfl = f.readlines()
				# нет просадки во времени (от t1),
				# но заметно растет потребление памяти (i.e. 500М > 720M)
			elif args.action == 't3':
				for l in f: s = l
				# ожидаемо, нет выигрыша по времени (от t1-t2), но очевидно экономим память
			elif args.action == 't4':
				for n, l in enumerate(f):
					test_n = 9999
					if n == test_n:	print test_n, ': ', l
				# enumerate не тратит лишнего времени по сравнению с t3
	raw_input('press enter to continue..')
