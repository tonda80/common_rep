import time

def testPerfomance(f, *a, **aa):
	t = time.time()
	f(*a, **aa)
	print 'Time:', time.time() - t

def lTest1(s):
	l1 = l2 = range(s)
	l3 = [n for n in l1 if n in l2]
	l4 = [n for n in l1 if n not in l2]
	return (l3, l4)

def lTest2(s):
	l1 = l2 = range(s)
	l3=[]
	l4=[]
	for n in l1:
		if n in l2:
			l3.append(n)
		else:
			l4.append(n)
	return (l3, l4)

Q = 20000
testPerfomance(lTest1, Q)
testPerfomance(lTest2, Q)
