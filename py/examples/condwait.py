import threading
from time import sleep

cnd = threading.Condition()

def f():
    print 'f begin'
    sleep(10)
    print 's'
    cnd.acquire()
    print 'a'
    cnd.notify()
    print 'n'
    cnd.release()
    print 'f end'
threading.Thread(target=f).start()

cnd.acquire()
print 'wait'
cnd.wait()
print 'end wait' 

sleep(100)

