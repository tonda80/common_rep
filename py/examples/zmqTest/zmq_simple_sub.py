#!/usr/bin/python

import zmq
from sys import argv

if len(argv) > 1:
	EP = argv[1]
else:
	EP = 'epgm://eth0;239.192.1.1:5555'
print 'Subscription to %s'%EP

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt(zmq.SUBSCRIBE, '')

#socket.bind(EP)
socket.connect(EP)
socket.RCVTIMEO = 1000

print 'SUB ready..'
while True:
	try:
		msg_in = socket.recv()
		print '[%s] was received'%msg_in
	except zmq.error.Again:
		pass

