#!/usr/bin/python

import zmq
import json

with open('config.json') as f:
	config = json.loads(f.read())

context = zmq.Context()
socket = context.socket(zmq.DEALER)
socket.connect(config['requestQueue'])

print config['requestQueue'], 'DEALER ready..'
while True:
	msg = raw_input('>')
	socket.send(msg)
	print '[%s] was sent'%msg
	#msg_in = socket.recv()
	#print '[%s] was received'%msg_in
