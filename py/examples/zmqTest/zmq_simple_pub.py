#!/usr/bin/python

import zmq
from time import sleep
from sys import argv

if len(argv) > 1:
	EP = argv[1]
else:
	EP = 'epgm://eth0;239.192.1.1:5555'
print 'Publishing to %s'%EP

context = zmq.Context()

socket = context.socket(zmq.PUB)

socket.bind(EP)
#socket.connect(EP)

print 'PUB ready..'
while 1:
	msg = raw_input('>')
	socket.send(msg)
	print '[%s] was sent'%msg

