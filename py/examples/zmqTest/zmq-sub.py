#!/usr/bin/python

import zmq
import json

with open('config.json') as f:
	config = json.loads(f.read())

context = zmq.Context()

socket = context.socket(zmq.SUB)

if config['one-to-some']:		# usual way
	socket.connect(config['publicationQueue'])		
else:
	socket.bind(config['publicationQueue'])	
		
socket.setsockopt(zmq.SUBSCRIBE, '')

if config['one-to-some']:
	stScheme = 'one-to-some'
else:
	stScheme = 'some-to-one'
	
print stScheme, config['publicationQueue'], 'SUB ready..'
while True:
	msg_in = socket.recv()
	print '[%s] was received'%msg_in

	
