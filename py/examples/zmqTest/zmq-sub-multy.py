#!/usr/bin/python

import zmq
import json

with open('config.json') as f:
	config = json.loads(f.read())

context = zmq.Context()

socket = context.socket(zmq.SUB)
socket.connect(config['publicationQueue'])
socket.setsockopt(zmq.SUBSCRIBE, '')

print config['publicationQueue'], 'SUB ready..'
while True:
	msg_in = socket.recv_multipart()
	print '[%s] was received'%msg_in

	
