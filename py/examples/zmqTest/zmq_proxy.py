# coding=utf8

import threading
import zmq
import sys

class Global:
	flag_work = True
	zmq_context = zmq.Context()

class ZmqSubPubProxy:
	def __init__(self, sub_endpoint, pub_endpoint):
		self.pub_socket = Global.zmq_context.socket(zmq.PUB)
		self.pub_socket.bind(pub_endpoint)

		self.sub_socket = Global.zmq_context.socket(zmq.SUB)
		self.sub_socket.connect(sub_endpoint)
		self.sub_socket.setsockopt(zmq.SUBSCRIBE, '')

		threading.Thread(target=self.receivedThread).start()

	def receivedThread(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while Global.flag_work:
			socks = dict(poller.poll(1000))
			if self.sub_socket in socks:
				msg = self.sub_socket.recv_multipart()
				self.process_message(msg)

	def process_message(self, in_msg):
		self.pub_socket.send_multipart(in_msg)

# python zmq_proxy.py tcp://127.0.0.1:12347 tcp://127.0.0.1:12348
sub_ep, pub_ep = sys.argv[1:3]
print '%s => %s'%(sub_ep, pub_ep)
zmq_proxy = ZmqSubPubProxy(sub_ep, pub_ep)
try:
	raw_input('Press Enter to stop')
except KeyboardInterrupt:
	pass
Global.flag_work = False
