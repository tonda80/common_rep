#!/usr/bin/python

import zmq
import thread
from time import sleep

addr = 'inproc://ep1'

def receiver(ctx, addr, suf):
    sck = ctx.socket(zmq.PAIR)
    sck.connect(addr)
    while 1:
        msg = sck.recv()
        print msg, suf

# -----------------------------

ctx = zmq.Context()

thread.start_new_thread(receiver, (ctx, addr, '!!!!'))
sck = ctx.socket(zmq.PAIR)
sck.bind(addr)
cnt = 0
while 1:
    sck.send('message %d'%cnt)
    cnt += 1
    sleep(1)





