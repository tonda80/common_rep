#!/usr/bin/python

import zmq
import json
from time import sleep

with open('config.json') as f:
	config = json.loads(f.read())

context = zmq.Context()

socket = context.socket(zmq.PUB)

if config['one-to-some']:		# usual way
	socket.bind(config['publicationQueue'])
else:
	socket.connect(config['publicationQueue'])

if config['one-to-some']:
	stScheme = 'one-to-some'
else:
	stScheme = 'some-to-one'

print stScheme, config['publicationQueue'], 'PUB ready..'

for i in xrange(5):
	msg = 'msg%d'%i
	socket.send(msg)
	print '[%s] was sent'%msg

while 1:
	msg = raw_input('>')
	socket.send(msg)
	print '[%s] was sent'%msg

