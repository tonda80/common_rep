#!/usr/bin/python

import zmq
import json

with open('config.json') as f:
	config = json.loads(f.read())
	
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind(config['requestQueue'])

print config['requestQueue'], 'REP ready..'
while True:
	msg_in = socket.recv()
	print '[%s] was received'%msg_in
	msg = 'Answer to [%s]'%msg_in
	socket.send(msg)
	print '[%s] was sent'%msg

