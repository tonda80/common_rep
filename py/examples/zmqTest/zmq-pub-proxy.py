#!/usr/bin/python

import zmq
import json
import os
import thread
import sys

context = zmq.Context()

def proxy():
	socketSub = context.socket(zmq.SUB)
	socketSub.bind(config['proxyQueue'])
	socketSub.setsockopt(zmq.SUBSCRIBE, '')

	socketPub = context.socket(zmq.PUB)
	socketPub.bind(config['publicationQueue'])
	
	while True:
		msg = socketSub.recv()
		print 'proxy [%s] was received'%msg
		socketPub.send(msg)
		print 'proxy [%s] was sent'%msg

with open('config.json') as f:
	config = json.loads(f.read())

lockFile = 	config['lockFile']
flagProxy = False
if not os.access(lockFile, os.F_OK):
	print 'proxy works here'
	thread.start_new_thread(proxy, ())
	flagProxy = True
	open(lockFile, 'w').close()
	
try:	
	socket = context.socket(zmq.PUB)
	socket.connect(config['proxyQueue'])		# !!! connect

	print config['publicationQueue'], 'PUB ready..'
	while 1:
		msg = raw_input('>')
		socket.send(msg)
		print '[%s] was sent'%msg
		if not os.access(lockFile, os.F_OK):
			print '\n\n----------------------'
			print 'proxy was stopped. Exiting..'
			print '----------------------\n\n'
			sys.exit(0)
					
finally:
	if flagProxy:
		os.remove(lockFile)
		print '\n\n----------------------'
		print 'proxy doesn\'t work more'
		print '------------------------\n\n'
			
