from Tkinter import *

root = Tk()
root.title('Frames test'); root.geometry('640x480')

#frames = []

for rel in (SUNKEN, RAISED, GROOVE, RIDGE, FLAT):
	fr = Frame(root, borderwidth=2 , relief=rel)
	fr.pack(side=TOP, expand=YES, fill=BOTH)
	Label(fr, text = str(rel)).pack(anchor = S)
	#frames.append(fr)

root.mainloop()