﻿var0 = 0 

ll = []

def f0():
	var0 = 1
	ll = []
	def f11():
		print 'f11', var0	# we can get a top var
	def f12():
		print var0	# 
		var0 += 111  # error! local variable 'var0' referenced before assignment
	def f13():
		global var0
		print 'f13', var0	# 
		var0 = 13  # we can change a var of unchanged type if it's global only
	def f14():
		ll.append(14)  # we can change a var of changed type
	def f15():
		global ll
		ll.append(15)
		
	f11()
	#f12()
	f13()
	f14()
	f15()
	print 'f0', var0
	print 'f0', ll
	
f0()
print 'global', var0
print 'global', ll
