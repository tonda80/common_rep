from Tkinter import *

root = Tk()
root.geometry('640x480')

Frame(root, bd=3, bg='red').grid(row=0,column=0,sticky=NSEW)
Frame(root, bd=3, bg='green').grid(row=0,column=1,sticky=NSEW)
Frame(root, bd=3, bg='blue').grid(row=1,column=0,sticky=NSEW)
Frame(root, bd=3, bg='black').grid(row=1,column=1,sticky=NSEW)
Frame(root, bd=3, bg='yellow').grid(row=1,column=2,sticky=NSEW)

size = root.grid_size()

for i in xrange(size[0]):
	root.grid_columnconfigure(i,weight=i+1)#, minsize=512)
for i in xrange(size[1]):
	root.grid_rowconfigure(i,weight=i+1)

root.title('Grid test %s'%str(size))

root.mainloop()
