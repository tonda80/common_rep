# coding=utf8
# в 2024 году пишем на 2 питоне тестовый http сервер для dssl

import BaseHTTPServer
import json


class HTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	server_version = 'HTTPExample/0.1'

	def do_GET(self):
		print 'Received get', self.path
		if self.path == '/plain':
			self._reply_plain('ok')
		elif self.path == '/json':
			self._reply_json({'status': 'ok', 'code': 777})
		else:
			self.send_error(404, "No such resource")

	def _reply_plain(self, data):
		self.send_response(200)
		self.send_header('Content-type', 'text/plain')
		self.send_header('Content-Length', str(len(data)))
		self.end_headers()
		self.wfile.write(data)

	def _reply_json(self, data):
		self.send_response(200)
		self.send_header('Content-type', 'application/json')
		sdata = json.dumps(data, separators=(',', ':'))
		self.send_header('Content-Length', str(len(sdata)))
		self.end_headers()
		self.wfile.write(sdata)



def run_http_server(serv_name = '127.0.0.1', port = 8080):
	# empty serv_name bind to 0.0.0.0
	server_address = (serv_name, port)
	httpd = BaseHTTPServer.HTTPServer(server_address, HTTPRequestHandler)
	httpd.serve_forever()


if __name__ == '__main__':
	try:
		run_http_server()
	except KeyboardInterrupt:
		pass
