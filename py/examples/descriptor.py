class FooDescriptor(object):

	value = 1

	def __get__(self, instance, owner):

		print "FooDescriptor.__get__ called"

		return self.value

	def __set__(self, instance, value):

		print "FooDescriptor.__set__ called with value %d" % value

		self.value = value

class Bar(object):

	foo = FooDescriptor()

bar = Bar()

print bar.foo
