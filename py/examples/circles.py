﻿from Tkinter import *
from random import randrange

def add_ov():
	x = randrange(1000)
	y = randrange(700)
	d= randrange(100)
	color = '#%06x'%randrange(0x1000000)
	cnv.create_oval(x,y,x+d,y+d,fill = color)
	cnv.config(scrollregion=cnv.bbox(ALL))
	

root = Tk()

sry = Scrollbar(root)
sry.pack(side=RIGHT, fill=Y)
srx = Scrollbar(root, orient=HORIZONTAL)
srx.pack(side=BOTTOM, fill=X)

cnv = Canvas(root, yscrollcommand=sry.set, xscrollcommand=srx.set, bg='white')
cnv.pack(fill=BOTH, expand=YES)

sry.config(command=cnv.yview)
srx.config(command=cnv.xview)

Button(text='Add',command=add_ov).pack(side=TOP)

for i in xrange(1000):
	root.after_idle(add_ov)

mainloop()
