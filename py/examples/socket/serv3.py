﻿import socket
import select

import sock_utils

HOST = ''
PORT = 50007

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)

f_exit = False
while 1:
	if f_exit:
		break

	conn, addr=s.accept()
	print 'Connect from %s [%s]'%(addr,conn)
	conn.setblocking(0)

	while 1:
		cmd = raw_input('> ')
		if cmd == 'r':
			data = sock_utils.recv(conn, 1024)
			print len(data), ':', data
		elif cmd == 's':
			print 'conn.send()'
			conn.send('x'*1000)
		elif cmd == 'q':
			f_exit = True
			break
		elif cmd == 'c':
			sock_utils.check(conn)
		elif cmd == 'n':
			conn.close()
			break
		else:
			print 'unknown command:', cmd

conn.close()