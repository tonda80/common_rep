import socket
import select
import time

def connect(sck, *a, **kw):
	while 1:
		try:
			sck.connect(*a, **kw)
		except socket.error as e:
			print 'Connect error:', e
			time.sleep(2)
		else:
			break

def recv(sck, *a, **kw):
	data = ''
	try:
		data = sck.recv(*a, **kw)
	except socket.error as e:
		print 'recv error:', e
	return data

def check(sck):
	print map(bool, select.select((sck,), (sck,), (sck,), 0))