import socket
import select
from time import sleep
from os import remove

SOCKETFILE = "/var/tmp/pysocket"

s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
#s.setblocking(10)	it works strangely

s.bind(SOCKETFILE)
s.listen(1)	# listen is not blocking !!!!
try:
	conn, addr = s.accept()
	print type(conn), type(addr)
	print 'Connected by', addr

	while 1:
		if select.select((conn,), (), (), 5) [0]:
			#sleep(10)
			data = conn.recv(1024)
			print data, '/', len(data)
			if not data: break
			conn.send(data.upper())
			
except KeyboardInterrupt:
	pass
	
print "Exiting"
conn.close()
remove(SOCKETFILE)
