import socket
import time
import threading

HOST = '127.0.0.1'
PORT = 20000
NUM = 440

lck = threading.Lock()
def clear_print(msg):
	lck.acquire()
	print msg
	lck.release()

f_exit = False

def server(port):
	s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.bind((HOST,port))
	s.listen(1)

	while not f_exit:
		conn, addr=s.accept()
		#print 'Connect from %s [%s]'%(addr,conn)

		while not f_exit:
			data = conn.recv(1024)
			if not data:
				#print 'Connection was closed'
				break
			#print data
			conn.send('answer '+data)

	conn.close()

def client(port):
	s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.connect((HOST,port))
	#print 'Connected'
	cnt = 0
	while not f_exit:
		cnt += 1
		msg = 'B%d-%dE'%(port-PORT, cnt)

		s.send(msg)
		answ = s.recv(1024)
		clear_print(answ)

		time.sleep(2)

	s.close()

threads = []
for i in xrange(PORT, PORT+NUM):
	try:
		t1 = threading.Thread(target=server, args=(i,))
		t2 = threading.Thread(target=client, args=(i,))
		t1.start();
		t2.start()
	except Exception as e:
		print e
		print 'Cannot create thread', i-PORT
		# this situation breaks script

	threads.append(t1)
	threads.append(t2)

# waiting
while 1:
	try:
		time.sleep(0.5)
	except KeyboardInterrupt:
		print '---------------------------'
		print 'Application will be stopped'
		break

f_exit = True
for t in threads:
	t.join()

