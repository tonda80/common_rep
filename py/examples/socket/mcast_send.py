#coding=utf8

import socket

MCAST_GRP = '224.1.1.8'
MCAST_PORT = 50017

MCAST_GRP = '239.195.1.137'
MCAST_PORT = 17009

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 50)

# linux only
#ifreq = struct.pack('16sH14s', IF_NAME, socket.AF_INET, '\x00'*14)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, ifreq)

while 1:
	msg=raw_input()
	sock.sendto(msg, (MCAST_GRP, MCAST_PORT))
