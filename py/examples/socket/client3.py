import socket
import select
import time

import sock_utils

HOST = 'localhost'
PORT=50007

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock_utils.connect(s, (HOST,PORT))
s.setblocking(0)

print 'Connected'
msg = 'x'*1000

while 1:
	cmd = raw_input('> ')

	if cmd == 'q':
		s.close()
		break
	elif cmd.startswith('s'):
		n = 1
		try: n = int(cmd[1:])
		except: pass
		smsg = msg*n
		print 's.send()', len(smsg)
		s.send(smsg)
	elif cmd == 'c':
		sock_utils.check(s)
	elif cmd == 'r':
		data = sock_utils.recv(s, 1024)
		print len(data), ':', data
	else:
		print 'unknown command:', cmd

	if msg == 'exit':
		print 'Exiting..'
		break

s.close()
