# Echo client program
import socket
import time
import select

SOCKETFILE = "/var/tmp/pysocket"

s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

while 1:
	try:
		s.connect(SOCKETFILE)
	except socket.error:
		print 'Error of connection. Try again..'
		time.sleep(2)
	else:
		print 'Connected'
		break
		
while 1:
	st = raw_input()
	s.send(st)
	if select.select((s,), (), (), 5) [0]:
		data = s.recv(1024)
		print 'Received', repr(data)
s.close()
	