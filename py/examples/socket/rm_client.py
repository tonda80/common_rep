import socket

HOST = 'localhost'
PORT=50007

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST,PORT))
print 'Connected'
while 1:
	msg = raw_input()
	
	if msg == 'test':
		print 'Test a socket closing'
		s.close()		
	
	if msg:
		s.send(msg)
		print s.recv(1024)
		
	if msg == 'exit':
		print 'Exiting..'
		break

s.close()
