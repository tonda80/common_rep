PATH=%PATH%;D:\aberezin\programs\swig;C:\Program Files (x86)\CodeBlocks\MinGW\bin

swig -python try.i

gcc -c try_wrap.c try.c  -DHAVE_CONFIG_H -IC:\Python27\include
gcc -shared try_wrap.o try.o -o _mytry.pyd -LC:\Python27\libs -lpython27

python test_try.py

del *mytry* *.o *wrap*
