# coding=utf8

import time

# parses string like "2016-02-05 09:49:58" + 211556 + "+00";
def timestamp_to_time(s_ts, mks, s_tz):
	tt = time.mktime(time.strptime(s_ts, '%Y-%m-%d %H:%M:%S')) + time.timezone
