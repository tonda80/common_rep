import os

class RawCounter:
	def __init__(self):
		self.cnt = 0
	def check_pause(self):
		self.cnt += 1
		if self.cnt%40 == 0:
			raw_input('more..')
			
def remove_if_exists(ll, e):
	try:
		ll.remove(e)
	except ValueError:
		pass
		
rc = RawCounter()
for r,d,f in os.walk('C:\\'):	# root, dirs, files
	#print r,d,f
	
	#When topdown is true (default), the caller can modify the dirnames list in-place
	#(e.g., via del or slice assignment ll[s:e]), and walk will only recurse into the
	#subdirectories whose names remain in dirnames
	remove_if_exists(d, 'Boot')
		
	for e in f+d:
		print os.path.join(r, e)
		rc.check_pause()
