#import xml.dom.minidom as MD
import json
import os
import path_getter

rxml = path_getter.get_file_name_no_throw()
if not rxml:
	exit(1)
d,f  = os.path.split(rxml)
wxml = os.path.join(d, 'pretty_'+f)

with open(rxml) as rf, open(wxml, 'w') as wf:
	#wf.write(MD.parseString(rf.read()).toprettyxml())
	json.dump(json.load(rf), wf, indent=4, separators=(',', ' : '), sort_keys=True)
