# coding=utf8

import cv2
import time
import sys

def open_image(path):
	return cv2.imread(path)

# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_image_display/py_image_display.html#py-display-image
def show_window(image, name=None):
	if name is None:
		name = 'image%s'%time.time()
	cv2.imshow(name, image)
	d = 30
	while 1:
		key = cv2.waitKey(0)
		if key == 27:
			cv2.destroyWindow(name)
			break
		elif key == ord('r'):
			r1, r2 = (10, 40), (10 + d, 40 + d)
			print 'Test rectangle', r1, r2
			d += 30
			cv2.rectangle(image, r1, r2, (0, 0, 255), 4)
			cv2.imshow(name, image)
			d += 30
		#else: print 'Pressed', key
	# cv2.destroyAllWindows() чтоб закрыть все

if __name__ == '__main__':
	img=open_image(sys.argv[1])
	show_window(img)
