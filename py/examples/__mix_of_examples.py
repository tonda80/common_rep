def hexdump(s, delim=' '):
    return delim.join('%02x'%ord(c) for c in s)