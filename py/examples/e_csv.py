import csv
import os

InpF = r'D:\aberezin\job_issues\BBP-3563_reuters_bridge\SBRR1_p.csv'
OutpF= InpF+'.out.csv'

with open(InpF, 'rb') as fI, open(OutpF, 'wb') as fO:
	reader = csv.reader(fI, delimiter=',')
	writer = csv.writer(fO, delimiter=',', quoting=csv.QUOTE_ALL)
	cnt_empty = 10
	for row in reader:
		test = (row[10] + row[11]).strip()
		if test or cnt_empty:
			writer.writerow(row)
			if not test:
				cnt_empty -= 1
