# coding=utf8
# в py, потому что тестил на 2-м питоне, но по идее в 3-м должно быть все то же самое

def setup():
    print ("basic setup into module")

def teardown():
    print ("basic teardown into module")

def setup_module(module):
    print ("module setup")

def teardown_module(module):
    print ("module teardown")

def setup_function(function):
    print ("function setup")

def teardown_function(function):
    print ("function teardown")


def test_224():
    print ("test_224")
    assert 2*2 == 4

def test_22p():
    print ("test_22p")
    assert 2**2 == 42
