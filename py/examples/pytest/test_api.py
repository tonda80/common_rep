# coding=utf8

import BaseHTTPServer
import json
import logging
import pytest
import re
import threading
from collections import namedtuple

import sys
sys.path.append('..')
sys.path.append('../../../..')      # host
from vizlabs.unpacked.resources.backend import viz_http_client
import host


log = logging.getLogger('tests')    # --log-cli-level=DEBUG|INFO|etc


VIZLABS_SERVER_HOST = '127.0.0.1'
VIZLABS_SERVER_PORT = 8088
VIZLABS_SERVER_USER = 'dssl'
VIZLABS_SERVER_PASSWORD = '12345'
VIZLABS_SERVER_NEED_PRESET = 'need_preset'
VIZLABS_SERVER_ACCESS_TOKEN = 'accesstoken'
VIZLABS_SERVER_EXPIRED_ACCESS_TOKEN = 'expiredaccesstoken'
VIZLABS_SERVER_REFRESH_TOKEN = 'refreshtoken'


class VizlabsData:
    objects = []
    cameras = []
    presets = []

    @classmethod
    def clear(cls, objects=None, cameras=None, presets=None):
        if objects is None:
            cls.objects = []
        else:
            cls.objects = objects

        if cameras is None:
            cls.cameras = []
        else:
            cls.cameras = cameras

        if presets is None:
            cls.presets = []
        else:
            cls.presets = presets


class VizlabsRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    server_version = 'HTTPExample/0.1'

    refresh_token_cnt = 0

    def do_GET(self):
        #print 'Received get', isinstance(self, VizlabsRequestHandler), self.path, self.headers
        if not self._check_authorized():
            return
        {
            '/any/': lambda: self._reply_json({'answer': 42}),
            '/api/statistics/camera-storage/objects/': lambda: self._reply_json(VizlabsData.objects),
            '/api/statistics/camera-storage/optical-cameras/': lambda: self._reply_json(VizlabsData.cameras),
            '/api/statistics/camera-storage/categories-preset/': lambda: self._reply_json(VizlabsData.presets),
        }.get(self.path, self._do_unknown_request)()

    def do_POST(self):
        #print 'Received post', self.path, self.headers
        if not self._check_authorized():
            return

        handler = {
            '/api/statistics/auth/login/': self._do_post_login,
            '/api/statistics/auth/refresh/': self._do_post_refresh,
            '/api/statistics/camera-storage/objects/': self._do_post_objects,
        }.get(self.path)
        if handler:
            return handler()

        param_urls = {
            r'/api/statistics/camera-storage/(\d+)/optical-cameras/': self._do_post_cameras,
        }
        for patten, handler in param_urls.iteritems():
            mo = re.match(patten, self.path)
            if mo:
                return handler(*mo.groups())

        self._do_unknown_request()

    def _do_unknown_request(self):
        log.warning('VizlabsRequestHandler get unknown {} request {}'.format(self.command, self.path))
        self.send_error(404, 'No resource')

    def _get_post_data_as_dict(self):
        content_length = int(self.headers['Content-Length'])
        raw_data = self.rfile.read(content_length)
        #print 'post raw_data: ', raw_data
        content_type = self.headers.get('Content-Type', '')
        if content_type == 'application/x-www-form-urlencoded':
            kv_pairs = [e.partition('=') for e in raw_data.split('&')]
            dict_ = {e[0]:e[2] for e in kv_pairs}
        elif not content_type or content_type == 'application/json':
            if not content_type:
                log.info('Post request have no Content-Type, we treat it as json')
            dict_ = json.loads(raw_data)
        else:
            raise RuntimeError('Unknown content type: ' + content_type)

        return dict_

    def _reply_json(self, data, code=200):
        self.send_response(code)
        self.send_header('Content-type', 'application/json')
        sdata = json.dumps(data, separators=(',', ':'))
        self.send_header('Content-Length', str(len(sdata)))
        self.end_headers()
        self.wfile.write(sdata)

    def _check_authorized(self):
        if self.path in ('/api/statistics/auth/login/', '/api/statistics/auth/refresh/'):
            return True

        if self.headers['Authorization'].startswith('Bearer ' + VIZLABS_SERVER_ACCESS_TOKEN):
            return True

        self.send_error(401, 'Unauthorized')
        return False

    def _do_post_login(self):
        post_data = self._get_post_data_as_dict()
        if post_data.get('username') == VIZLABS_SERVER_USER and post_data.get('password') == VIZLABS_SERVER_PASSWORD:
            refresh_token_cnt = 0
            reply_data = {
                'access_token': VIZLABS_SERVER_ACCESS_TOKEN,
                'refresh_token': VIZLABS_SERVER_REFRESH_TOKEN,
            }
            self._reply_json(reply_data)
        else:
            self.send_error(403, 'No user')

    def _do_post_refresh(self):
        post_data = self._get_post_data_as_dict()
        access_token = post_data.get('access_token', '')
        refresh_token = post_data.get('refresh_token', '')

        if refresh_token.startswith(VIZLABS_SERVER_REFRESH_TOKEN) and \
              (access_token == VIZLABS_SERVER_EXPIRED_ACCESS_TOKEN or \
               access_token.startswith(VIZLABS_SERVER_ACCESS_TOKEN)):
            self.refresh_token_cnt += 1
            reply_data = {
                'access_token': VIZLABS_SERVER_ACCESS_TOKEN + str(self.refresh_token_cnt),
                'refresh_token': VIZLABS_SERVER_REFRESH_TOKEN + str(self.refresh_token_cnt),
            }
            self._reply_json(reply_data)
        else:
            self.send_error(401, 'Unauthorized')

    def _do_post_objects(self):
        post_data = self._get_post_data_as_dict()
        new_item = {
            'obj_id': 11,   # TODO?
        }
        new_item.update(post_data)
        VizlabsData.objects.append({'info': new_item})
        self._reply_json(new_item)

    def _do_post_cameras(self, str_obj_id):
        obj_id = int(str_obj_id)
        post_data = self._get_post_data_as_dict()
        new_item = {
            'obj_id': obj_id,
            'description': post_data.pop('description'),
            'camera': {
                'cam_id': 22,   # TODO?
            }
        }
        new_item['camera'].update(post_data)
        VizlabsData.cameras.append(new_item)
        self._reply_json(new_item)



@pytest.fixture(autouse=True, scope='module')
def vizlabs_mock_server():
    server_addr = (VIZLABS_SERVER_HOST, VIZLABS_SERVER_PORT)
    httpd = BaseHTTPServer.HTTPServer(server_addr, VizlabsRequestHandler)

    thread = threading.Thread(target=httpd.serve_forever)
    thread.start()

    yield httpd

    httpd.shutdown()
    thread.join()


@pytest.fixture()
def script_args():
    return {
        'URL': 'http://{}:{}'.format(VIZLABS_SERVER_HOST, VIZLABS_SERVER_PORT),
        'LOGIN': VIZLABS_SERVER_USER,
        'PASSWORD': VIZLABS_SERVER_PASSWORD,
        'PRESET_NAME' : VIZLABS_SERVER_NEED_PRESET
    }


@pytest.fixture()
def viz_client(script_args):
    authorized_client = viz_http_client.VizHttpClient(log, **script_args)
    assert authorized_client._access_token == VIZLABS_SERVER_ACCESS_TOKEN
    assert authorized_client._refresh_token == VIZLABS_SERVER_REFRESH_TOKEN
    return authorized_client


def test_autorization_failed(script_args, mocker):
    script_args['LOGIN'] = 'wrong login'
    script_args['PASSWORD'] = 'wrong password'
    spy = mocker.spy(VizlabsRequestHandler, '_do_post_login')

    with pytest.raises(RuntimeError) as exc_info:
        viz_client = viz_http_client.VizHttpClient(log, **script_args)

    assert 'Unsuccessful authorization' in str(exc_info.value)
    spy.assert_called_once()


def test_autorization_ok(script_args, mocker):
    spy = mocker.spy(VizlabsRequestHandler, '_do_post_login')

    viz_client = viz_http_client.VizHttpClient(log, **script_args)

    spy.assert_called_once()
    assert viz_client._access_token == VIZLABS_SERVER_ACCESS_TOKEN
    assert viz_client._refresh_token == VIZLABS_SERVER_REFRESH_TOKEN

def test_refresh_expired_token(viz_client, mocker):
    viz_client._access_token = VIZLABS_SERVER_EXPIRED_ACCESS_TOKEN
    spy = mocker.spy(VizlabsRequestHandler, '_do_post_refresh')

    viz_client.get('/any/')

    spy.assert_called_once()
    assert viz_client._access_token == VIZLABS_SERVER_ACCESS_TOKEN+'1'  # apropos '1' see refresh_token_cnt
    assert viz_client._refresh_token == VIZLABS_SERVER_REFRESH_TOKEN+'1'

def test_refresh_stolen_token(viz_client, mocker):
    viz_client._access_token = 'wrong'
    viz_client._refresh_token = 'wrong'
    spy_login = mocker.spy(VizlabsRequestHandler, '_do_post_login')
    spy_refresh = mocker.spy(VizlabsRequestHandler, '_do_post_refresh')

    viz_client.get('/any/')

    assert spy_refresh.call_count > 1
    spy_login.assert_called_once()
    assert viz_client._access_token == VIZLABS_SERVER_ACCESS_TOKEN
    assert viz_client._refresh_token == VIZLABS_SERVER_REFRESH_TOKEN

def test_add_new_cameras(viz_client):
    VizlabsData.clear()
    new_cameras = ['chan1', 'chan2']
    viz_client.add_new_cameras(new_cameras)

    assert len(VizlabsData.objects) == 1
    assert len(VizlabsData.cameras) == len(new_cameras)

def test_add_new_cameras_to_existing(viz_client, mocker):
    server_guid = host.settings("").guid

    VizlabsData.clear(
        objects = [
            {'info': {'o_type': server_guid, 'obj_id': 1}}
        ],
        cameras = [
            {'camera': {'cam_id': 1, 'name': 'chan1'}, 'description': 'chan1_guid', 'obj_id': 1},
            {'camera': {'cam_id': 2, 'name': 'chan2'}, 'description': 'chan2_guid', 'obj_id': 1}],
    )

    orig_host_settings = host.settings
    def patched_host_settings(path):
        #print 'Called host.settings({})'.format(path)
        FakeSetting = namedtuple('FakeSetting', ['guid', 'name'])
        if path.startswith('channels/'):
            chan = path.partition('/')[2]
            assert '/' not in chan
            return FakeSetting(chan+'_guid', chan+'_name')
        return orig_host_settings(path)

    mocker.patch('host.settings', side_effect=patched_host_settings)

    viz_client.add_new_cameras(['chan1', 'chan2', 'chan3'])

    assert len(VizlabsData.objects) == 1
    assert len(VizlabsData.cameras) == 3

def test_no_need_preset(viz_client):
    VizlabsData.clear()

    with pytest.raises(RuntimeError) as exc_info:
        viz_client.get_preset()

    assert "Preset '{}' not found".format(VIZLABS_SERVER_NEED_PRESET) == str(exc_info.value)

def test_preset_ok(viz_client):
    VizlabsData.clear(
        presets = [
            {'name': 'other_preset', 'any_other_data': 0},
            {'name': VIZLABS_SERVER_NEED_PRESET, 'any_other_data': 777},
            {'name': 'also_preset', 'any_other_data': 1},
        ]
    )

    preset = viz_client.get_preset()

    assert preset.get('name') == VIZLABS_SERVER_NEED_PRESET
