import sys 

print sys.argv

class ChattyType(type):
    def __new__(cls, name, bases, dct):
        print "Allocating memory for class", name
        return type.__new__(cls, name, bases, dct)
    def __init__(cls, name, bases, dct):
        print "Initing (configuring) class", name
        super(ChattyType, cls).__init__(name, bases, dct)


chtype = ChattyType('MyMetaclass', (), {})

ob = chtype()

print type(ob)
    


    
