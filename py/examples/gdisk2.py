# coding=utf8

# Доступ к файлам гугл-диска
# Пробуем сделать более внятный пример чем gdisk.py

# https://developers.google.com/api-client-library/python/start/get_started
# https://developers.google.com/drive/v3/web/quickstart/python
# https://habr.com/post/328248/
# https://medium.com/@ashokyogi5/a-beginners-guide-to-google-oauth-and-google-apis-450f36389184

# pip install google-api-python-client		именно так во избежание дурацких проблем импорта
# заходишь в гугл консоль, заводишь проект и в нем oauth авторизацию, скачиваешь client_secret.json
# конкретное имя файла передается в flow_from_clientsecrets параметром --secret_path
# права доступа к гугл документам см в C.SCOPES
# oauth2client.tools.run_flow открывает в браузере окно авторизации и после одобрения оной генерится другой истинно секретный credential.json
# который собственно и используется для авторизации
# по ссылке выше (medium.com) сказано что refresh_token (его значение есть в credential.json) бессрочный в отличие от (?)

import os
import argparse

import apiclient
import oauth2client
import oauth2client.tools
import oauth2client.file

import baseapp


class C:
	SCOPES = 'https://www.googleapis.com/auth/drive.readonly'


class AppError(RuntimeError):
	pass


class App(baseapp.BaseConsoleApp):
	def _get_parent_parsers(self):
		return [oauth2client.tools.argparser]

	def add_arguments(self, parser):
		parser.add_argument('--cred_path', help=u'Путь к файлу с credentials, если опущено генерим из secret_path, см App.credential_path')
		parser.add_argument('--secret_path', help=u'Путь к client_secret.json')

	def main(self):
		client = self.create_drive_service()
		print client

	def credential_path(self):
		if self.args.cred_path:
			return self.args.cred_path
		if not self.args.secret_path:
			raise AppError('no secret_path')
		return os.path.splitext(self.args.secret_path)[0] + '_credential.json'		# возможно по secret.json уже был сгенерен secret_credential.json

	def get_credentials(self):
		cred_path = self.credential_path()
		store = oauth2client.file.Storage(cred_path)
		creds = store.get()
		if not creds or creds.invalid:
			self.log.info('new credentials file will be generated: {}'.format(cred_path))
			flow = oauth2client.client.flow_from_clientsecrets(self.args.secret_path, C.SCOPES)		# https://developers.google.com/api-client-library/python/guide/aaa_oauth#flow_from_clientsecrets
			creds = oauth2client.tools.run_flow(flow, store, flags=self.args)						# https://oauth2client.readthedocs.io/en/latest/source/oauth2client.tools.html
		return creds

	# список имен, версий
	# https://developers.google.com/api-client-library/python/apis/
	def create_service(self, api_name, api_version, credentials):
		if credentials is None:
			credentials = self.get_credentials()
		print credentials
		return apiclient.discovery.build(api_name, api_version, credentials=credentials)	# http://googleapis.github.io/google-api-python-client/docs/epy/googleapiclient.discovery-module.html#build

	def create_drive_service(self):
		return self.create_service('drive', 'v3', None)

	def create_sheets_service(self):
		return self.create_service('sheets', 'v4', None)

# Пример чтения содержимого таблицы
# sheet = sheets_service.spreadsheets()
# sheet.values().get(spreadsheetId='1p1p6Ry7S2Clfw8bYUwckEnXnelfRJiuhka3e1NheLGA', range=u'Лист1').execute()
# https://developers.google.com/resources/api-libraries/documentation/sheets/v4/python/latest/sheets_v4.spreadsheets.values.html#get
# тут вот есть инфа о листах
# sheet.get(spreadsheetId='1p1p6Ry7S2Clfw8bYUwckEnXnelfRJiuhka3e1NheLGA').execute()

if __name__ == '__main__':
	App().main()

# а вот так можно запускать под ipython, или там будет выполняться __main__ код
# g = {'__name__': 'ipython_test1'}; execfile('/home/ant/common_rep/py/examples/gdisk2.py', g); s = g['sheets']
# g = {'__name__': 'ipython_test2'}; execfile(r'D:\common_rep\py\examples\gdisk2.py', g); s = g['sheets']
elif __name__ == 'ipython_test1':
	app = App(('--cred_path', '/home/ant/job/gd/client_secret_credential.json'))
	sheets_service = app.create_sheets_service()
	sheets = sheets_service.spreadsheets()
elif __name__ == 'ipython_test2':
	app = App(('--cred_path', r'D:\job\playrix\gd\client_secret_65361___credential.json'))
	sheets_service = app.create_sheets_service()
	sheets = sheets_service.spreadsheets()
