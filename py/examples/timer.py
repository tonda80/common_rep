# coding=1251
from time import sleep, time
from threading import Timer
from Tkinter import *

class CTrackingTimer:
	def __init__(self, callback=None):
		self.callback = callback
		self.value = 0
	
	def start(self, time, discr=1, callback=None):
		if (callback==None):
			callback = self.callback	# we will use a common callback
		self.value = time
		Timer(discr, self.__counting, (discr, callback)).start()
		
	def __counting(self, discr, callback):
		if self.value>0:
			self.value = self.value - discr		# �� -=, ����� ����� ����������� ������������ ���������������� IntVar (�� ������)
			Timer(discr, self.__counting, (discr, callback)).start()
		else:
			if (callback != None):
				callback()
		
if __name__=="__main__":
	
	def pp():
		print 'Time is up'
		
	timer = CTrackingTimer(pp)

	class Wnd():		
		def __init__(self):
			self.root = Tk()
			Button(self.root, text='Send',width=10,command=self.start).pack()	#
			self.tmr = mIntVar();self.tmr.set(30)
			Entry(textvariable=self.tmr).pack()
			self.root.mainloop()
		def start(self):
			timer.start(self.tmr)
	
	class mIntVar(IntVar):
		def __init__(self):
			IntVar.__init__(self)
		def __sub__(self, s):
			self.set(self.get() - s)
			return self
		def __gt__(self, n):
			return self.get()>n	
			
	Wnd()	