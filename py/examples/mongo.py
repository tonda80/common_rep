# coding=utf8

from pymongo import MongoClient

import datetime
import random


# туториал
# https://api.mongodb.com/python/current/tutorial.html
# док
# https://api.mongodb.com/python/current/api/pymongo/index.html
# тестовая база (tonda mail - тнд17мнг!)
# https://cloud.mongodb.com/v2/5c17ad63014b7673f61ebab7#clusters

# строчка для экспериментов в ipython
collection_name='c1';import pymongo;mcl=pymongo.MongoClient(r'mongodb://mongo_guru:uehfghk@cluster0-shard-00-00-adg3l.mongodb.net:27017,cluster0-shard-00-01-adg3l.mongodb.net:27017,cluster0-shard-00-02-adg3l.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true').get_database('test').get_collection(collection_name)


conn_string = r'mongodb://mongo_guru:uehfghk@cluster0-shard-00-00-adg3l.mongodb.net:27017,cluster0-shard-00-01-adg3l.mongodb.net:27017,cluster0-shard-00-02-adg3l.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
mongo_client = MongoClient(conn_string)
# или можно так
# mongo_client = MongoClient(host, username, password)

db_name = 'hello_db'
db = mongo_client.get_database(db_name)

print db.collection_names(include_system_collections=False)

collection_name = 'hello_collection'
collection = db.get_collection(collection_name)

# documents must have only string keys
certain_doc = {
	'time': datetime.datetime.utcnow().isoformat(),
	'very_important_number': random.randrange(0,100),
	'float': 1.234,
	'dict' : {'1': 'one', '2': 'two', '3':'three'},
	'rhymes' : ['eniky','ate',5,'vareniks']
}
res = collection.insert_one(certain_doc)	# у res есть acknowledged, inserted_id
# collection.insert_one(certain_doc) 		ОШИБКА! в certain_doc появился _id

# СМ bson.objectid.ObjectId()

collection.insert_one({'somekey1':228})
collection.insert_one({'somekey1':'somevalue11'})

collection.find_one({'float': 1.234})	# возвращает документ в виде словаря

cursor = collection.find({'float': 1.234})
for e in cursor:
	print e

# отсутсвующие ключи имеют значение None
print collection.count_documents({'any_absent_key':None}) == collection.count()

# более хитрые запросы https://docs.mongodb.com/manual/reference/operator/query/
# например имеющие поле somekey1
for e in collection.find({'somekey1': {'$ne' : None }  }): print e
# поле большее 70
for e in collection.find({'very_important_number': {'$gt' : 70 }  }): print e

# можно добавить индекс на коллекцию

# delete_many, delete_one для удаления
res=collection.delete_many({'somekey1':138})
# res.deleted_count чтоб узнать удалилось ли

# update_one, update_many для обновления
# выражения для обновления https://docs.mongodb.com/manual/reference/operator/update/
res = collection.update_one({'_id':2}, {'$set':{'f1':13}})
res.matched_count == 0: print u'нифига не обновилось'
collection.update_many({}, {'$inc': {'very_important_number' : 1}})
# ВНЕЗАПНО! коллекции с осутствующим значением (по идее None) после такого инкремента равны 1
# во избежание надо указывать фильтр для обновляемого
