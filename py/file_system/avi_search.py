# coding=utf8
import os, shutil
import lsh

Find_dir = r'D:\photos'

# -----------------------------

f_dict = {}
for dp,ds,fs in os.walk(Find_dir):
	for f in fs:
		if lsh.extention(f) == '.avi':
			f_dict.setdefault(dp, []).append(f)

# -----------------------------

cmn_f_cnt = 0
cmn_size = 0
for d, fs in f_dict.iteritems():
	dir_size = 0
	for f in fs:
		sz = os.path.getsize(os.path.join(d, f))
		cmn_f_cnt += 1
		cmn_size += sz
		dir_size += sz
	if dir_size > 200e6:
		print '%-50s \t%s'%(lsh.unicode_file_name(d), lsh.human_size(dir_size))

print 'Total %s, %d files in %d dirs'%(lsh.human_size(cmn_size), cmn_f_cnt, len(f_dict))
