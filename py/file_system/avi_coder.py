# coding=utf8
import os
import lsh

In_dir = r'D:\temp\v'
Out_dir = os.path.join(In_dir, 'out')
Rm_out_dir = True
Test = 1

Media_info = r'D:\programs\MediaInfo\MediaInfo.exe'
Handbrake = r'C:\Program Files\Handbrake\HandBrakeCLI.exe'
Handbrake_options = '-e x264 --x264-preset medium --x264-tune film --quality 20 --two-pass'
# quality 0-51;

# ------------------------

def get_avi_date(avi_f):
	for s in lsh.run((Media_info, avi_f)).split('\n'):
		try:
			name, value = map(lambda s: s.strip(), s.strip().split(':', 1))
		except ValueError:
			continue
		if name == 'Mastered date':
			return value.replace(' ', '_').replace(':', '-')

	raise RuntimeError('No data for %s'%avi_f)


def recode(in_f, out_f):
	global cnt_recode
	cnt_recode += 1
	print in_f, '=>', out_f
	opts = tuple(Handbrake_options.split()) + ('-i', '%s'%in_f, '-o', '%s'%out_f)
	args = (Handbrake,) + opts

	print args
	if Test:
		return
	lsh.run(args)

def filter_(pth):
	return lsh.test_file(pth)

# main ------------------------

if Rm_out_dir and lsh.test_dir(Out_dir):
	lsh.rm(Out_dir, '-r')
lsh.mkdir(Out_dir)

cnt_recode = 0
file_list = filter(filter_, map(lambda f: os.path.join(In_dir, f), os.listdir(In_dir)))
total = len(file_list)

for in_f in file_list:
	date_label = get_avi_date(in_f)

	nm, ext = os.path.splitext(os.path.split(in_f)[1])
	nm += '_' + date_label
	out_f = os.path.join(Out_dir, nm + ext)

	print '%d(%d):'%(cnt_recode+1, total),
	recode(in_f, out_f)
print '\nTotal %d'%cnt_recode

