# coding=cp1251
import os, shutil

WORK_DIR = 'F:\\'			# ������� ����������
FILE_OUT = r'out_file.txt'	# ����� ����������
MAX_REC = 0					# ������� ������ � ����������� (0 -�� ������� � �����)

file_out = open(FILE_OUT, 'w')


def print_files(path, n_tab):
	'''�������� ����� � ����� � path � �������� n_tab'''
	try:
		all_files = os.listdir(path)	# �������� ������ ������ � �������� 
	except WindowsError:
		all_files = []
		
	all_files = map(lambda f: os.path.join(path, f), all_files)	# ������ ����� ����� �����������
	
	all_files.sort(cmp_path)	# ���������
	
	for n in all_files:	# �� ���� ������ ������
		file_out.write('\t'*n_tab + base_ext(n)[0] + '\n')
		if n_tab < MAX_REC and os.path.isdir(n): print_files(n, n_tab+1)
	

def base_ext(path):
	'''��������� �������� ��� � ����������'''
	''' �������� ���� ���, ��� �������� ������������ ����������, ���� �� �������������))
	���������� ������� os.splitext, �� ����� ����� rfind, �� ����� �� �������� ����	
	'''
	name = os.path.basename(path)
	num = -1
	for i,c in enumerate(name):
		if c == '.': num = i
	if num == -1: return (name, '')
	else: return (name[:num], name[num+1:])
	
	
def cmp_path(px, py):
	'''������� ��� ���������� �����.'''
	if os.path.isdir(px):
		if os.path.isdir(py):	return cmp(px, py)
		else:					return -1
	else:
		if os.path.isdir(py):	return 1
		else:					return cmp(px, py)


print_files(WORK_DIR, 0)
file_out.close()

