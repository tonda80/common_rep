import lsh
import sys

SCRIPT = sys.argv[0]
DIR = sys.argv[1]

lsh.mkdir(DIR, '-p')     

for f in lsh.ls():
	if f not in (DIR, SCRIPT, '.git'):
		#print f, '->', DIR
		lsh.mv(f, DIR)
