﻿# coding=utf-8
from Tkinter import *

root = Tk()
root.title(u'Делитель')

def get_float(entry):
	try:
		num = float(entry.get())
	except ValueError:
		num = None
		entry['bg'] = 'gray70'
	else:
		entry['bg'] = 'white'
	return num

def set_float(entry, num):
	entry.delete(0, END); entry.insert(0, '%.3g'%num)
	entry['bg'] = 'yellow'
	
def calc_uin():
	r1 = get_float(entr_r1)
	r2 = get_float(entr_r2)
	u_out = get_float(entr_uout)
	if None in (r1,r2,u_out) or r1==0: return 1
	set_float(entr_uin, u_out*(r1+r2)/r1)
	return 1
	
def calc_r1():
	u_in = get_float(entr_uin)
	r2 = get_float(entr_r2)
	u_out = get_float(entr_uout)
	if None in (u_in,r2,u_out) or u_in==u_out: return 1
	set_float(entr_r1, u_out*r2/(u_in-u_out))	
	return 1
	
def calc_r2():
	u_in = get_float(entr_uin)
	r1 = get_float(entr_r1)
	u_out = get_float(entr_uout)
	if None in (u_in,r1,u_out) or u_out==0: return 1
	set_float(entr_r2, r1*(u_in-u_out)/u_out)
	return 1

def calc_uout():
	u_in = get_float(entr_uin)
	r1 = get_float(entr_r1)
	r2 = get_float(entr_r2)
	if None in (u_in,r1,r2) or r1+r2==0: return 1
	set_float(entr_uout, u_in*r1/(r1+r2))
	return 1
	
# --------------------------------------------------------------------------------------------------------------------------------------------
fr1 = Frame(root, borderwidth=2, relief=GROOVE); fr1.pack(side=TOP, expand=YES, fill=BOTH)
fr2 = Frame(root, borderwidth=2, relief=GROOVE); fr2.pack(side=TOP, expand=YES, fill=BOTH)
fr3 = Frame(root, borderwidth=2, relief=GROOVE); fr3.pack(side=TOP, expand=YES, fill=BOTH)
fr4 = Frame(root, borderwidth=2, relief=GROOVE); fr4.pack(side=TOP, expand=YES, fill=BOTH)
	
Label(fr1, text='Uin').pack(side=RIGHT,padx=35,pady=5)	
entr_uin = Entry(fr1, validate='focusin', validatecommand=calc_uin); entr_uin.pack(side=LEFT,padx=1,pady=5)

Label(fr2, text='R2').pack(side=RIGHT,padx=35,pady=5)	
entr_r2 = Entry(fr2, validate='focusin', validatecommand=calc_r2); entr_r2.pack(side=LEFT,padx=1,pady=5)

Label(fr3, text='R1').pack(side=RIGHT,padx=35,pady=5)	
entr_r1 = Entry(fr3, validate='focusin', validatecommand=calc_r1); entr_r1.pack(side=LEFT,padx=1,pady=5)

Label(fr4, text='Uout').pack(side=RIGHT,padx=35,pady=5)	
entr_uout = Entry(fr4, validate='focusin', validatecommand=calc_uout); entr_uout.pack(side=LEFT,padx=1,pady=5)

root.mainloop()
