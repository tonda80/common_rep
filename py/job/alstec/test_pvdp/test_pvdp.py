# coding=cp1251
from Tkinter import *
import serial
import string
#from thread import start_new_thread

PATH = r'/mspu/bin/./'	# ���� � test

Ri = 47.0/2					# ������������� ������������������ ���������
Ua = 5.0					# ���������� ������� ���
DIV = 15.0/(1020.0+15.0)	# �������� ��� ��������� ����������
DIVDP = 13.0/(1020.0+13.0)	# �������� ��� ��������� ���������� ��

# ��� 1 ��������
i_1d = (Ua/Ri)/0xfff
u_1d = (Ua/DIV)/0xfff
p_1d = 16*16*i_1d*u_1d	# p_1d = p/p(����������)
u_1d_dp = (Ua/DIVDP)/0xfff

class Wnd_test_pvdp():
	def test(self):
		print self.send_uart('')
		
	
	def __init__(self):
		self.root = Tk(); self.root.title(u'���� ����');
		self.root.bind('<Key>', self.hndl_key)
				
		fr_info = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	#�������������� ����
		fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		self.info = StringVar()
		Label(fr_info, textvariable = self.info).pack(side = LEFT)
		
		self.flg_init_uart = 1
		
		fr1 = Frame(self.root, borderwidth=2, relief=GROOVE); fr1.pack(side=TOP, expand=YES, fill=BOTH)
		Button(fr1,text='Raw',width=10,command=self.raw_cmd,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.raw_str = StringVar(value=r'insmod /lib/modules/2.6.22.1/misc/drv_cgc_eb.ko major_number=253 debug_level=4  ndsl_ext=1');
		Entry(fr1,textvariable = self.raw_str,takefocus=OFF).pack(side=LEFT,expand=YES,fill=X,padx=5,pady=5)
		
		#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr2_4 = Frame(self.root, borderwidth=2, relief=GROOVE); fr2_4.pack(side=TOP, expand=YES, fill=BOTH)
		
		fr2 = Frame(fr2_4); fr2.pack(side=TOP, expand=YES, fill=BOTH)
		Button(fr2,text='Out',width=10,command=self.dt_out_pvdp,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.str_dt_out = []
		for i in xrange(4):
			self.str_dt_out.append(StringVar(value=0))
			Entry(fr2,width=7,textvariable = self.str_dt_out[i]).pack(side=LEFT,padx=5,pady=5)
		
		fr3 = Frame(fr2_4); fr3.pack(side=TOP, expand=YES, fill=BOTH)
		Button(fr3,text='Read out',width=10,command=self.read_out_pvdp,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.str_read_dt_out = []
		for i in xrange(4):
			self.str_read_dt_out.append(StringVar())
			Entry(fr3,width=7,textvariable = self.str_read_dt_out[i],state=DISABLED).pack(side=LEFT,padx=5,pady=5)
			
		fr4 = Frame(fr2_4); fr4.pack(side=TOP, expand=YES, fill=BOTH)
		Button(fr4,text='Read in',width=10,command=self.read_in_pvdp,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.str_read_dt_in = []
		for i in xrange(4):
			self.str_read_dt_in.append(StringVar())
			Entry(fr4,width=7,textvariable = self.str_read_dt_in[i],state=DISABLED).pack(side=LEFT,padx=5,pady=5)
		self.flg_cont_read_in = BooleanVar()
		Checkbutton(fr4,variable=self.flg_cont_read_in,takefocus=OFF).pack(side=LEFT)
		
		#Button(fr2_4,text='Out + Read out + Read in',command=self.write_read,takefocus=OFF).pack(side=TOP,padx=5,pady=5,expand=YES, fill=X)
		
		# UFM --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr5 = Frame(self.root, borderwidth=2, relief=GROOVE); fr5.pack(side=TOP, expand=YES, fill=BOTH)
		Label(fr5,text=u'UFM ����').pack(pady=2)
		self.ufm_addr = StringVar()
		self.ufm_dtwr = StringVar()
		self.ufm_dtrd = StringVar()
		Label(fr5,text=u'ad').pack(side=LEFT)
		Entry(fr5,width=5,textvariable=self.ufm_addr).pack(side=LEFT,padx=5,pady=5)
		Label(fr5,text=u'dt').pack(side=LEFT)
		Entry(fr5,width=5,textvariable=self.ufm_dtwr).pack(side=LEFT,padx=5,pady=5)
		Button(fr5,text='write',width=5,command=self.ufm_write,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr5,text='read',width=5,command=self.ufm_read,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Entry(fr5,width=7,textvariable=self.ufm_dtrd,state=DISABLED).pack(side=LEFT,padx=5,pady=5)
		Button(fr5,text='erase',width=5,command=self.ufm_erase,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		
		# ������ --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr_ch = Frame(self.root, borderwidth=2, relief=GROOVE); fr_ch.pack(side=TOP, expand=YES, fill=BOTH)
		Label(fr_ch,text=u'������').pack(pady=2)
		self.n_channels = 0	# ����� ��� �������� � ����
		def click_channel(i):
			self.n_channels ^= 1 << i
			n2, n1 = divmod(self.n_channels,256)
			self.exec_cmd_pvdp((0x10, 0xd5, n2, n1))
		def en_all():
			self.exec_cmd_pvdp((0x10, 0xd5, 0xff, 0xff))
		def dis_all():
			self.exec_cmd_pvdp((0x10, 0xd5, 0, 0))
		for i in xrange(16):
			callback = SimpleCallback(click_channel, i)
			Checkbutton(fr_ch,command=callback,takefocus=OFF,indicatoron=0,text=str(i)).pack(side=LEFT,padx=3,pady=2)#
		Button(fr_ch,command=en_all,takefocus=OFF,text='Enable All').pack(side=RIGHT,padx=3,pady=2)
		Button(fr_ch,command=dis_all,takefocus=OFF,text='Disable All').pack(side=RIGHT,padx=3,pady=2)
		
		# ���� ����� --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr_rel_l = Frame(self.root, borderwidth=2, relief=GROOVE); fr_rel_l.pack(side=TOP, expand=YES, fill=BOTH)
		Label(fr_rel_l,text=u'���� �����').pack(pady=2)
		n_rel_l = IntVar()	# ����� ��� �������� � ����
		def click_rel_l():
			n2, n1 = divmod(n_rel_l.get(),256)
			#print n2, n1
			self.exec_cmd_pvdp((0xa0, 0xd5, n2, n1))
		Radiobutton(fr_rel_l,variable=n_rel_l,value=0,command=click_rel_l,takefocus=OFF,text='Off').pack(side=TOP)
		for i in xrange(16):
			Radiobutton(fr_rel_l,variable=n_rel_l,value=1<<i,command=click_rel_l,takefocus=OFF,text=str(i)).pack(side=LEFT,pady=2)
		
		
		# ���� DSL --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr_rel_d = Frame(self.root, borderwidth=2, relief=GROOVE); fr_rel_d.pack(side=TOP, expand=YES, fill=BOTH)
		Label(fr_rel_d,text=u'���� DSL').pack(pady=2)
		n_rel_d = IntVar()	# ����� ��� �������� � ����
		def click_rel_d():
			n2, n1 = divmod(n_rel_d.get(),256)
			#print n2, n1
			self.exec_cmd_pvdp((0x30, 0xd5, n2, n1))
		Radiobutton(fr_rel_d,variable=n_rel_d,value=0,command=click_rel_d,takefocus=OFF,text='Off').pack(side=TOP)
		for i in xrange(16):
			Radiobutton(fr_rel_d,variable=n_rel_d,value=1<<i,command=click_rel_d,takefocus=OFF,text=str(i)).pack(side=LEFT,pady=2)
		
		# ����� ����� � ���������� --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		fr6 = Frame(self.root, borderwidth=2, relief=GROOVE); fr6.pack(side=TOP, expand=YES, fill=BOTH)
		self.u_i = StringVar(value='---')
		Label(fr6,textvariable=self.u_i).pack()
		
		self.count = 0	# �������, �������������� � �������
		self.timer()
		self.root.mainloop()
	
	# �������� ������� �� ������	
	def raw_cmd(self):	
		ans = self.send_uart(self.raw_str.get())
		print '>>',
		for s in ans:
			if s[-4:]!='\x1b[6n': print s
	# ���������� �������� � �������
	def write(self, addr, val):
		#start_new_thread(self.send_uart, (r'%stest -d 6 -w 8 -s %x -x %x'%(PATH, addr, val), ) )
		self.send_uart(r'%stest -d 6 -w 8 -s %x -x %x'%(PATH, addr, val))
	# ������ �������� �� ���������	
	def read(self, addr, q):
		ans = self.send_uart(r'%stest -d 6 -w 8 -q %x -l %d'%(PATH, addr, q))
		try: rd_val = map(lambda x: int(x, 16), string.split(ans[3])[1:])
		except (IndexError, ValueError): return
		return rd_val		
	# ���������� � ������������, ��� �������� ���������
	def wr_rd_test(self, addr, val):
		self.write(addr, val)
		if val == self.read(addr,1)[0]: return 1
		else: return 0
	
	# ������ � ����
	def dt_out_pvdp(self):
		self.write(0xf, 0x5)
		addr = 0x10
		for s in self.str_dt_out:
			try: val = int(s.get(), 16)
			except ValueError: print u'������ � ���� ����� %d'%(addr-0xf); return 0
			self.write(addr, val)
			addr += 1
		#self.read_out_pvdp()
		self.write(0xf, 0x1)		
	# ������, ��� ������ �� ����
	def read_out_pvdp(self):
		rv = self.read(0x10, 4)
		for i in xrange(4):
			try: self.str_read_dt_out[i].set('%#.2x'%rv[i])
			except (TypeError,IndexError): self.info.set(u'������ ������ �����'); print rv,
			else: self.info.set(u'Ok')
	# ������, ��� ������ ��� ����
	def read_in_pvdp(self):
		rv = self.read(0x30, 4)
		for i in xrange(4):
			try: self.str_read_dt_in[i].set('%#.2x'%rv[i])
			except (TypeError,IndexError): self.info.set(u'������ ������ �����'); print rv,
			else: self.info.set(u'Ok')
		self.calc_u_i()
		#self.test_tdm()
		
	# ����� � ������ ����
	def write_read(self):
		self.dt_out_pvdp()
		self.read_in_pvdp()
		
	def exec_cmd_pvdp(self, tpl):	# ��������� ������� ����. � tpl[0:3] ��� �������
		self.write(0xf, 0x5)	
		addr = 0x10
		for i in xrange(4):
			self.write(addr, tpl[i])
			#print(addr, tpl[i])
			addr += 1
		self.write(0xf, 0x1)
		self.dt_out_pvdp()		# ���������� ������ �������� �� ������
		
	# UFM --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	def ufm_write(self):
		try: addr = int(self.ufm_addr.get(), 16); data = int(self.ufm_dtwr.get(), 16)
		except ValueError: self.info.set(u'������ ����� ������'); return
		else: self.info.set(u'Ok')
		self.write(0xf, 0x5)
		self.write(0x10, 0xc5); self.write(0x11, 0x1) ;self.write(0x12, addr>>8); self.write(0x13, addr&0xff)
		self.write(0xf, 0x1)
		print '%x %x %x %x'%tuple(self.read(0x10, 4))	# ����� ��� �����
		self.write(0xf, 0x5)
		self.write(0x11, 0x2) ;self.write(0x12, data>>8); self.write(0x13, data&0xff)
		self.write(0xf, 0x1)
		print '%x %x %x %x'%tuple(self.read(0x10, 4))	# ����� ��� �����
		self.write(0x11, 0x6)
	def	ufm_read(self):
		try: addr = int(self.ufm_addr.get(), 16)
		except ValueError: self.info.set(u'������ ����� ������'); return
		else: self.info.set(u'Ok')
		self.write(0xf, 0x5)
		self.write(0x10, 0xc5); self.write(0x11, 0x1) ;self.write(0x12, addr>>8); self.write(0x13, addr&0xff)
		self.write(0xf, 0x1)
		print '%x %x %x %x'%tuple(self.read(0x10, 4))	# ����� ��� �����
		self.write(0x11, 0x5)		
		data = self.read(0x32, 2)
		self.ufm_dtrd.set('%#.2x%.2x'%tuple(data))
	def	ufm_erase(self):
		self.write(0xf, 0x5)
		self.write(0x10, 0xc5); self.write(0x11, 0x7)
		self.write(0xf, 0x1)

	# ������ ����� ��� ����������
	def calc_u_i(self):
		in0 = self.str_read_dt_in[0].get()
		if in0[:-1]=='0x6':	# ������ ����
			i_top = self.str_read_dt_in[1].get()[2:] + self.str_read_dt_in[2].get()[2:3]
			i_bot = self.str_read_dt_in[2].get()[-1:] + self.str_read_dt_in[3].get()[2:]
			#if in0=='0x64': k = 50/23.5	# ��� ��� 1 ����� �� ������� ��� 4 ��������� ������� ����������������� ��������
			#else: 
			k = 1
			n_i_t = int(i_top, 16)*i_1d*k*1000	# ��� � ��
			n_i_b = int(i_bot, 16)*i_1d*k*1000	# ��� � ��
			self.u_i.set('%s    Itop = %.2f    Ibot = %.2f  (mA)'%(self.str_read_dt_in[0].get()[3:],n_i_t, n_i_b))
		elif in0[:-1]=='0x7':	# ������ ����������
			u_top = self.str_read_dt_in[1].get()[2:] + self.str_read_dt_in[2].get()[2:3]
			u_bot = self.str_read_dt_in[2].get()[-1:] + self.str_read_dt_in[3].get()[2:]
			n_u_t = int(u_top, 16)*u_1d	# ���������� � �������
			n_u_b = int(u_bot, 16)*u_1d	# ���������� � �������
			self.u_i.set('%s    Utop = %.2f    Ubot = %.2f  (V)'%(self.str_read_dt_in[0].get()[3:],n_u_t, n_u_b))
		elif in0[:-1]=='0xb':	# ������ ���������� ��
			udp = self.str_read_dt_in[1].get()[2:] + self.str_read_dt_in[2].get()[2:3]
			n_udp = int(udp, 16)*u_1d_dp	# ���� � �������
			self.u_i.set('Udp = %.2f (V)'%(n_udp))
		else:
			self.u_i.set(u'---')
			
	# ���� ���
	def test_tdm(self):
		if (self.str_read_dt_in[0].get()[:-1] != '0x0' or
			self.str_read_dt_in[1].get() != '0x01' or
			self.str_read_dt_in[2].get() != '0xaa' or
			self.str_read_dt_in[3].get() != '0xaa'
			):
			for st in self.str_read_dt_in: print st.get(),
			print
			
			
		
			
	# ������ � ���-������ -------------------------------------------------------------------------------------------------		
	def init_uart(self):	# ������������� �����
		try: self.uart = serial.Serial(0, baudrate=115200, timeout=0.08)	
		except serial.SerialException: self.info.set(u'������ �������� COM0'); self.flg_init_uart = 1
		else:
			self.info.set(u'COM0')
			self.flg_init_uart = 0
			
	def test_uart(self):	# ���� ������� �����
		#print self.send_uart('')
		try:
			if self.send_uart('')[1][-4:]=='\x1b[6n': self.info.set(u'EFM ������'); return 1
			else: self.info.set(u'EFM �� ������'); return 0
		except IndexError: self.info.set(u'EFM �� ������'); return 0
	def send_uart(self, cmd):	# �������� �������
		if self.flg_init_uart: return ()
		self.uart.write(cmd+'\r')
		#start_new_thread(self.uart.write, (cmd+'\r', ))
		answer = []	# ������ �������� �����
		line = self.my_readline()
		while (line != '' and line[-4:]!=['\x1b','[','6','n']):
			answer.append(string.strip(line))
			line = self.my_readline()
		#print answer
		return tuple(answer)	# � 0 ��-�� ����� ������������ �������, ����� ��������� ������
	def my_readline(self):	# ������ ������ �� uart, ������� readline � ������� ����� �� ������ ���������� ������
		line = []
		sym = '1'	# ����� ����� � ����
		while sym!='' and line[-2:]!=['\r','\n'] and line[-4:]!=['\x1b','[','6','n']:
			sym = self.uart.read(size=1); line.append(sym)
		return string.join(line,'')
		
	#-----------------------------------------------------------------------------------------------------------------------------	
	def timer(self):	
		if self.flg_init_uart: self.init_uart()
		if self.flg_cont_read_in.get(): self.read_in_pvdp()
		#self.count += 1
		'''try: self.en_u_i4.set(self.str_read_dt_in[1].get()+self.str_read_dt_in[2].get()[2]+'\t'
						+ '0x' + self.str_read_dt_in[2].get()[3] + self.str_read_dt_in[3].get()[2:])
		except IndexError: pass
		'''
		self.root.after(500, self.timer)
	
	def hndl_key(self, ev):
		#print ev.keysym
		if ev.keysym=='Control_R': self.dt_out_pvdp() #print 'Data out'
		if ev.keysym=='Shift_R': self.read_in_pvdp() #print 'Read in' 
		if ev.keysym=='Return': self.dt_out_pvdp();self.write_read() #print 'Data out + Read in'
		
		
class SimpleCallback:
	'''��������� ��������� ��������� ������, ���������� �� ����,
	 ������������ ������� ������� ��������� (Scott David Daniels),
	 ������� ����� �������� � � ��������� �������-�����������.'''

	def __init__(self, callback, *firstArgs):
			self.__callback = callback
			self.__firstArgs = firstArgs

	def __call__(self, *args):
			return self.__callback (*(self.__firstArgs + args))		

Wnd_test_pvdp()
