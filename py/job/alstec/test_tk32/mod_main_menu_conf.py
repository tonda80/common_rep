﻿from Tkinter import *
import mod_uart
import mod_rw_spi
import mod_rw_tdm

# обработчик выбора меню COM
def hndl_menu_com():
	mod_uart.sel_com(main_menu)	

# для тестов	
def hndl_menu_test():
	print('menu test')
	#mod_uart.sel_com(main_menu)

# функция создающая главное меню
def main_menu_conf(m):
	global main_menu
	
	main_menu = m
	main_menu.add_command(label = 'COM1', command = hndl_menu_com)	# команда смены COM порта
	
	menu_test = Menu(main_menu) 
	main_menu.add_cascade(label = u'Тесты', menu = menu_test)		# меню Тесты
	menu_test.add_command(label = u'Чтение\\запись SPI', command = mod_rw_spi.create_wnd_rw_spi)	# тест чтения\записи SPI
	menu_test.add_command(label = u'Чтение\\запись TDM', command = mod_rw_tdm.create_wnd_rw_tdm)	# чтение\запись теста ТДМ
	
	# для упрощения тестовой отладки модулей
	#mod_rw_tdm.create_wnd_rw_tdm()	
