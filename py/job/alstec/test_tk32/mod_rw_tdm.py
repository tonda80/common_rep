﻿from Tkinter import *
import mod_uart
from my_mod import EntryLimInt, window_center, set_same_cell
from table import Table
from string import split

# для упрощения отладки
def close_main():
	w.root.master.destroy()

# кодируем/декодируем строку по правилам кодирования ТЧ.
def code_tf(num):
	if (num & 0x80): return num^0x55	# lsb -1		
	else: return num^0x2a				# lsb -0
			
# класс окна теста чтения\записи служебных TDM
class Wnd_test_tdm:
	
	def __init__(self):
		self.root = Toplevel()
		self.root.title('Чтение\\запись TDM');
		
		#window_center(self.root,500,300);
		# для упрощения отладки		self.root.protocol("WM_DELETE_WINDOW", close_main)
		
		# вход
		fr_in = Frame(self.root, borderwidth=2, relief=GROOVE)
		fr_in.pack(side=TOP, expand=1, fill = BOTH)
		
		Label(fr_in, text = u'Входные служебные тайм-слоты').pack(side = TOP, pady=4)
		self.tbl_srv_ts_in = Table(fr_in, 4, 8, noedit=1, takefocus=0)
		self.tbl_srv_ts_in.pack(side = TOP, expand=1, fill = BOTH)
		
		Label(fr_in, text = u'Входные тайм-слоты с ТЧ').pack(side = TOP, pady=4)
		self.tbl_tf_ts_in = Table(fr_in, 4, 8, noedit=1, takefocus=0)
		self.tbl_tf_ts_in.pack(side = TOP, expand=1, fill = BOTH)
		
		self.btn_rd = Button(fr_in, text=u'Чтение', activebackground='gray70', bd=3, command=self.rd_tdm_all)
		self.btn_rd.pack(side = LEFT, pady=5, padx=5)
		
		self.flg_const_rd = BooleanVar(self.root)	# признак постоянного чтения
		Checkbutton(fr_in, text='Читать непреывно', variable=self.flg_const_rd, command = self.check__const_rd, indicatoron = 0).pack(side = LEFT, pady=3, padx=10)
		
		self.flg_decode_tf = BooleanVar(self.root, value=1)	# признак необходимости декодирования входного ТЧ
		Checkbutton(fr_in, text='Декодировать ТЧ', variable=self.flg_decode_tf).pack(side = RIGHT, pady=3, padx=5)
		
		# выход
		fr_out = Frame(self.root, borderwidth=2, relief=GROOVE)
		fr_out.pack(side=TOP, expand=1, fill = BOTH)
		
		Label(fr_out, text = u'Выходные служебные тайм-слоты').pack(side = TOP, pady=4)
		self.tbl_srv_ts_out = Table(fr_out, 4, 8, noedit = 0, callback = self.change_tbl_ts_out)	# callback общий для 2 таблиц
		self.tbl_srv_ts_out.pack(side = TOP, expand=1, fill = BOTH)

		Label(fr_out, text = u'Выходные тайм-слоты с ТЧ').pack(side = TOP, pady=4)
		self.tbl_tf_ts_out = Table(fr_out, 4, 8, noedit = 0, callback = self.change_tbl_ts_out)		# callback общий для 2 таблиц
		self.tbl_tf_ts_out.pack(side = TOP, expand=1, fill = BOTH)

		Button(fr_out, text=u'Получить с платы',command=self.query_tdm_out, bd=5).pack(side = LEFT, pady=3, padx=5)
		
		self.flg_code_tf = BooleanVar(self.root, value=1)	# признак необходимости кодирования выходного ТЧ
		Checkbutton(fr_out, text='Кодировать ТЧ', variable=self.flg_code_tf).pack(side = RIGHT, pady=3, padx=5)
		
		self.flg_alw_hex  = BooleanVar(self.root, value=1)	# признак 16 ричности вводимых чисел
		Checkbutton(fr_out, text='Всегда hex', variable=self.flg_alw_hex).pack(side = RIGHT, pady=3, padx=5)
		
		# информационная рамка
		fr_info = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	# Информ. рамка внизу окна
		fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		
		self.info = StringVar(self.root)
		Label(fr_info, textvariable=self.info).pack(side=LEFT)
		self.__last_task = 1	# идент. запущенного after, чтобы не ругался в 1 раз
		
		self.query_tdm_out()		
		self.rd_tdm_all()
		#set_same_cell(fr_main)
		
	# запись в выходной ТДМ. Функция вызывается когда меняется элемент в таблицах tbl_srv_ts_out или tbl_tf_ts_out
	def change_tbl_ts_out(self, num, value, who_call):
		def inform_error(num):# сообщить об ошибке ввода в num ячейку
			message = 'Error write to %d time slot'% num
			print(message); self.blink_info(message)
			who_call.set_marker(num)
		
		try:
			if self.flg_alw_hex.get(): num_value = int(value, 16)
			else:
				try: num_value = int(value, 10)
				except ValueError: num_value = int(value, 16)
		except ValueError:
			if value != '':	inform_error(num)	# когда пустая строка, просто выходим					
			return
		if num_value<0 or num_value>255: inform_error(num); return	
		# проверка пройдена
		who_call.del_marker(num)
		
		if who_call==self.tbl_srv_ts_out: cmd = 'tdmw,%d,%d'%(2*num+1, num_value)	# таблица служебных ТС
		else:
			if self.flg_code_tf.get(): num_value = code_tf(num_value)
			cmd = 'tdmw,%d,%d'%(2*num, num_value)									# таблица ТС с ТЧ
		
		print cmd
		answer = mod_uart.send_cmd(cmd)
		for i in answer[1:]: print('> '+ i)
	
	# опрашиваем все входные тайм-слоты	
	def rd_tdm_all(self):
		flg_const_rd = self.flg_const_rd.get()
		cmd = 'tdmr'
		if not flg_const_rd: print(cmd)
		answer = mod_uart.send_cmd(cmd)
		if answer == ():
			self.blink_info(u'Плата не готова')
			for i in xrange(32): self.tbl_srv_ts_in.set_elem(i, ''); self.tbl_tf_ts_in.set_elem(i, '')
		elif answer[0] != cmd: print 'Error of answer tdmr'
		else:
			for i in xrange(8):
				if not flg_const_rd: print '> ', answer[i+1]
				list_values = split(answer[i+1], ' ')
				for j in xrange(4):
					self.tbl_srv_ts_in.set_elem(4*i+j, list_values[2*j+1])	# таблица служебных ТС
					ts_tf = list_values[2*j]
					if self.flg_decode_tf.get(): ts_tf = '%X'%code_tf(int(ts_tf, 16))
					self.tbl_tf_ts_in.set_elem(4*i+j, '0x'+ts_tf)		# таблица ТС с ТЧ
		if flg_const_rd: self.root.after_idle(self.rd_tdm_all)
		
	# нажали на флажок пост. чтения
	def check__const_rd(self):
		if self.flg_const_rd.get(): self.btn_rd.invoke(); self.btn_rd.config(state = DISABLED)
		else: self.btn_rd.config(state = NORMAL)
			
	# опрашиваем, какие тайм-слоты выводим
	def query_tdm_out(self):
		cmd = 'tdmw'
		print(cmd)
		answer = mod_uart.send_cmd(cmd)
		if answer == ():
			self.blink_info(u'Плата не готова')
			for i in xrange(32): self.tbl_srv_ts_out.set_elem(i, ''); self.tbl_tf_ts_out.set_elem(i, '')
		elif answer[0] != cmd: print 'Error of answer tdmw'
		else:
			for i in xrange(8):
				print '> ', answer[i+1]
				list_values = split(answer[i+1],' ')
				for j in xrange(4):
					self.tbl_srv_ts_out.set_elem(4*i+j, '0x'+list_values[2*j+1])	# таблица служебных ТС
					ts_tf = list_values[2*j]
					if self.flg_code_tf.get(): ts_tf = '%X'%code_tf(int(ts_tf, 16))
					self.tbl_tf_ts_out.set_elem(4*i+j, '0x'+ts_tf)					# таблица ТС с ТЧ
	
	# выводит сообщение на delay секунд
	def blink_info(self, str, delay=2):
		self.info.set(str)
		self.root.after_cancel(self.__last_task)
		self.__last_task = self.root.after(delay*1000, self.clear_info)
	# стирает сообщение
	def clear_info(self):
		self.info.set('')
	
	# обработчик нажатия на клавишу
	#def hndl_key(self, event):
	
# создает окно w тестов чтения\записи TDM
def create_wnd_rw_tdm():
	global w
	try: w.root.deiconify()
	except (NameError, TclError): w = Wnd_test_tdm(); w.root.deiconify()	#  если окна нет или закрыто, то создаем

