﻿from Tkinter import *
import mod_uart
from my_mod import EntryLimInt, window_center

# для упрощения отладки
def close_main():
	w.root.master.destroy()
	
	
def tst():
	print test
	
	

# класс окна теста чтения\записи SPI
class Wnd_test_spi_rw:
	
	def __init__(self):
		self.root = Toplevel()
		self.root.title('Чтение\\запись SPI');
		self.root.bind('<Key>', self.hndl_key_enter)
		window_center(self.root);
		# для упрощения отладки 
		#self.root.protocol("WM_DELETE_WINDOW", close_main)
		
		fr1 = Frame(self.root, borderwidth=2, relief=GROOVE)
		fr1.pack(side=TOP, expand=YES, fill=BOTH)
		
		# номер микросхемы
		Label(fr1, text=u'Номер микросхемы (0-31)').grid(row=0,column=0,pady=2,padx=10)
		self.entr_n_chip = EntryLimInt(fr1, 0, 31)
		self.entr_n_chip.grid(row=1,column=0,pady=2,padx=10)
		
		self.flg_broadcast = BooleanVar(self.root)	# признак широковещательной команды записи
		Checkbutton(fr1, text='Broadcast', variable=self.flg_broadcast, takefocus=OFF, command=self.hndl_chk_br).grid(row=2,column=0,pady=2,padx=10, sticky = W)
		
		# номер регистра
		Label(fr1, text=u'Номер регистра (0-255)').grid(row=0,column=1,pady=2,padx=10)
		self.entr_n_reg = EntryLimInt(fr1, 0, 255)
		self.entr_n_reg.grid(row=1,column=1,pady=2,padx=10)
		
		fr2 = Frame(self.root, borderwidth=2, relief=GROOVE)
		fr2.pack(side=TOP, expand=YES, fill=BOTH)
		
		# записываемое значение
		Label(fr2, text=u'Записываемое значение').grid(row=0,column=0,pady=2,padx=10)
		self.entr_wr_val = EntryLimInt(fr2, 0, 255)
		self.entr_wr_val.grid(row=1,column=0,pady=2,padx=10)
		
		# считываемое значение
		Label(fr2, text=u'Считываемое значение').grid(row=0,column=1,pady=2,padx=10)
		self.rd_val = StringVar(self.root)
		Entry(fr2, state = 'disabled', textvariable = self.rd_val).grid(row=1,column=1,pady=2,padx=10)
		
		# кнопки		
		self.btn_wr = Button(fr2, text=u'Запись (W)',command=self.wr_spi, takefocus=0, activebackground='gray70',bd=3)
		self.btn_wr.grid(row=2,column=0,pady=10,padx=10)
		
		self.btn_rd = Button(fr2, text=u'Чтение (R)',command=self.rd_spi, takefocus=0, activebackground='gray70',bd=3)
		self.btn_rd.grid(row=2,column=1,pady=10,padx=10)
		
		fr_info = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	# Информ. рамка внизу окна
		fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		
		self.info = StringVar(self.root)
		Label(fr_info, textvariable=self.info).pack(side=LEFT)
		self.__last_task = 1	# идент. запущенного after, чтобы не ругался в 1 раз
		
		#set_same_cell(fr1); set_same_cell(fr2)
	
	# обработчик нажатия на клавишу
	def hndl_key_enter(self, event):
		if event.keysym == 'w' or event.keysym == 'W' or event.keysym == 'F2':
			self.btn_wr.invoke(); self.btn_wr.flash()
		elif event.keysym == 'r' or event.keysym == 'R' or event.keysym == 'F1':
			self.btn_rd.invoke(); self.btn_rd.flash()
	
	# обработчик смены состояния флажка broadcast
	def hndl_chk_br(self):
		if self.flg_broadcast.get(): self.entr_n_chip.config(state = 'disabled')
		else: self.entr_n_chip.config(state = 'normal')
		
	# выводит сообщение на delay секунд
	def blink_info(self, str, delay=2):
		self.info.set(str)
		self.root.after_cancel(self.__last_task)
		self.__last_task = self.root.after(delay*1000, self.clear_info)
	# стирает сообщение
	def clear_info(self):
		self.info.set('')
		
	# запись SPI
	def wr_spi(self):
		if self.flg_broadcast.get(): n_chip = 32	# широковещательная передача
		else: n_chip = self.entr_n_chip.num
		cmd = 'spiw,%d,%d,%d'%(n_chip, self.entr_n_reg.num, self.entr_wr_val.num)
		print(cmd)
		answer = mod_uart.send_cmd(cmd)
		if answer == ():
			self.blink_info(u'Плата не готова')
			return
		for i in answer[1:]: print('> '+ i)
	
	# чтение SPI
	def rd_spi(self):
		cmd = 'spir,%d,%d'%(self.entr_n_chip.num, self.entr_n_reg.num)
		print(cmd)
		answer = mod_uart.send_cmd(cmd)
		if answer == ():
			self.blink_info(u'Плата не готова')
			self.rd_val.set('')
			return
		for i in answer[1:]: print('> '+ i)
		try: self.rd_val.set(answer[2][15:])
		except IndexError: self.rd_val.set('Error!')
		except: self.rd_val.set('Unknown error!')
	
	
	# для теста
	def tst(self):
		print ('Test!')
	
# создает окно w тестов чтения\записи SPI
def create_wnd_rw_spi():
	global w
	try: w.root.deiconify()
	except (NameError, TclError): w = Wnd_test_spi_rw(); w.root.deiconify()	#  если окна нет или закрыто, то создаем
