﻿from Tkinter import *
import mod_uart
import mod_main_menu_conf

root = Tk()
root.title('TK-32 test'); root.geometry('640x200+200+100')

main_menu = Menu(root, borderwidth=20, relief=SUNKEN)
root.config(menu = main_menu)
mod_main_menu_conf.main_menu_conf(main_menu)

fr_info = Frame(root, height=20, borderwidth=2, relief=GROOVE)	# Информ. рамка внизу окна
fr_info.pack(side=BOTTOM, expand=NO, fill=X)

fr_main = Frame(root, borderwidth=2, relief=GROOVE)	# основной фрейм
fr_main.pack(side=BOTTOM, expand=YES, fill=BOTH)

mod_uart.create_uart(fr_info)	# создаем uart и его метку

mod_uart.timer1()		# запускаем таймер

'''
def btn_tst_handler():
	pass

btn_tst = Button(fr_main, text = u'Тест', command=btn_tst_handler).pack()
'''

root.mainloop()