﻿from Tkinter import *

# см __doc__
class EntryLimInt(Entry):
	'''
	# потомок класса Entry. Отслеживает вводимые значения и не дает ввести число болье max и меньше min.
	# Фактически min может быть лишь 0, тк класс недоделан, зато всегда дает корректное число
	# 
	# возвращает введенное число, как self.num
	# textvariable объекта  - self.text, если назначить другую переменную, то потеряет ограничительные свойства
	'''
	def __init__(self, root, min, max, **kw):
		Entry.__init__(self,root, **kw)
		
		self.__min = min
		self.__max = max
		
		self.num = min		# инициализируем минимумом
		self.__corr_v = str(min)		
		self.text = StringVar(value = str(min))
		
		self.config(textvariable = self.text)
		self.text.trace('w', self.__limvar)
		
	def __limvar(self,n,i,m):
		text = self.text.get()
		
		if text[0:2] == '0x': base = 16
		else: base = 10
		
		try: num = int(text, base)
		except ValueError:
			if text=='' or text=='0x':
				self.num = 0
				self.__corr_v = text
			else:	
				self.text.set(self.__corr_v)
		else:
			if num<self.__min or num>self.__max:
				self.text.set(self.__corr_v)
			else:
				self.__corr_v = text
				self.num = num
		

# помещает окно заданных размеров (или без них) в центр		
def window_center(window, w = None, h = None):
	if w==None or h==None:
		window.geometry('+%d+%d'%(window.winfo_screenwidth()/2,window.winfo_screenheight()/2))
	else:
		window.geometry('%dx%d+%d+%d'%(w,h,window.winfo_screenwidth()/2-w/2,window.winfo_screenheight()/2-h/2))
	

# устанавливает одинаковые настройки для всех ячеек виджета в котором используется метод grid	
def set_same_cell(wdg):
	size = wdg.grid_size()
	for i in xrange(size[0]): wdg.grid_columnconfigure(i,weight=1)#, minsize=512)
	for i in xrange(size[1]): wdg.grid_rowconfigure(i,weight=1)
		
		
				
		