﻿import serial
from Tkinter import *
import string

global uart, n_com_port, flg_cycle_init_test, state_uart, info_uart, lbl_uart

# номер порта с которым работаем
n_com_port = 0

# флаги выполнения функций в таймере
flg_cycle_init_test = (0, 0)

# состояние сом - порта
state_uart = 0

# выбираем текущий com-порт
def sel_com(main_menu):
	global n_com_port
	n_com_port = int(not n_com_port)
	main_menu.entryconfigure(1, label = 'COM%d'%(n_com_port+1))
	try: uart.close()
	except: pass
	init_uart()
	

#  Создаем метку отражающую состояние COM-порта. info_uart - textvariable метки
def create_uart(fr_info):
	global info_uart, lbl_uart
	
	info_uart = StringVar(value=u'Инициализация COM-порта')
	lbl_uart = Label(fr_info, text=info_uart.get(), textvariable=info_uart)	# Метка отражающая состояние com-порта
	lbl_uart.pack(side=LEFT)
		
	lbl_uart.after_idle(init_uart)

# Инициализируем COM-порт 
def init_uart():
	global uart, n_com_port, flg_cycle_init_test, state_uart
	try:
		uart = serial.Serial(n_com_port, baudrate=57600, timeout=0.05)	
	except serial.SerialException:
		info_uart.set(u'Ошибка открытия COM%d'%(n_com_port+1))
		flg_cycle_init_test = (1, 0)
		state_uart = 0
		return
	info_uart.set(uart.portstr)
	flg_cycle_init_test = (0, 1)
	state_uart = 1
	
# посылает команду и возвращает ответ
def send_cmd(cmd):
	if state_uart == 0:
		return ()
	uart.write(cmd + '\r')
	answer = []	# список читаемых строк
	line = uart.readline()
	while (line != ''):
		answer.append(string.strip(line))
		line = uart.readline()
	return tuple(answer)	# в 0 эл-те будет отправляемая команда, далее считанные строки
		
# посылает команду test и  выводит ответ в lbl_uart
def test_uart():
	answer = send_cmd('test')
	try:
		info_uart.set('%s: %s' % (uart.portstr, answer[1]))
	except IndexError:
		info_uart.set('%s: %s' % (uart.portstr, 'Device not found'))
	
		
# таймер на 1 секунду
def timer1():
	if flg_cycle_init_test[0]: lbl_uart.after_idle(init_uart)
	if flg_cycle_init_test[1]: lbl_uart.after_idle(test_uart)
	lbl_uart.after(1000, timer1)
	