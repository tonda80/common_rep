#!/usr/bin/python

import zmq
from threading import Thread
from time import sleep
import json

import os

ZMQ_DAL_REQ = "ipc:///tmp/IWELL_DAL"
ZMQ_EMC_REQ = "ipc:///tmp/IWELL_EMC"
ZMQ_PUBLISHER = "ipc:///tmp/IWELL_Publisher"
ZMQ_LEA_REP = "ipc:///tmp/IWELL_LEA"
ZMQ_SCADA_REP = "ipc:///tmp/IWELL_SCADA"
ZMQ_SUBSCRIBER = "ipc:///tmp/Subscriber"
ZMQ_MOTOR_REQ = "ipc:///tmp/IWELL_MOTOR"

RequestCreateExprFile = 'create-req.json'
ReplyEMCFile = 'emc-reply.json'

context = zmq.Context()

gWork = True

class etThread(Thread):
	def etWait(self):
		if self.isAlive():
			self.join(1)
	
def sendCreateReq():
	hmi_socket = context.socket(zmq.REQ)
	hmi_socket.connect(ZMQ_LEA_REP)
	msgOut = json.dumps(json.load(open(RequestCreateExprFile)))
	print 'Sending: %s'%msgOut
	hmi_socket.send(msgOut)		
	msgIn = hmi_socket.recv()
	print 'Recieved: %s'%msgIn
th0 = etThread(target=sendCreateReq, args=())
	
subscr_socket = context.socket(zmq.SUB)
subscr_socket.connect(ZMQ_PUBLISHER)
subscr_socket.setsockopt(zmq.SUBSCRIBE, '')
def recvPubl():
	while gWork:
		rec = subscr_socket.recv()
		print 'Publ. data:\n%s'%rec
th1 = etThread(target=recvPubl, args=())

emc_socket = context.socket(zmq.REP)
emc_socket.bind(ZMQ_EMC_REQ)
def recvEMC():
	while gWork:
		req = emc_socket.recv()
		print 'EMC req:\n%s'%req
		emc_socket.send(json.dumps(json.load(open(ReplyEMCFile))))
th2 = etThread(target=recvEMC, args=())

th0.start()
th1.start()
th2.start()

try:
	while (1):
		th0.etWait()
		th1.etWait()
		th2.etWait()		
except KeyboardInterrupt:
	gWork = False
	print 'KeyboardInterrupt'

sleep(3)
print 'Self-murder..'
os.kill(os.getpid(), 9)
