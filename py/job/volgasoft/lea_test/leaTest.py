#!/usr/bin/python

import zmq
from Tkinter import *
import tkFileDialog
import tkMessageBox
import thread
from time import sleep, time as _time
import json
import Queue
import os
import copy

ZMQ_DAL_REQ = "ipc:///tmp/IWELL_DAL"
ZMQ_EMC_REQ = "ipc:///tmp/IWELL_EMC"
ZMQ_PUBLISHER = "ipc:///tmp/IWELL_Publisher"
ZMQ_LEA_REP = "ipc:///tmp/IWELL_LEA"
ZMQ_SCADA_REP = "ipc:///tmp/IWELL_SCADA"
ZMQ_SUBSCRIBER = "ipc:///tmp/Subscriber"
ZMQ_MOTOR_REQ = "ipc:///tmp/IWELL_MOTOR"

def time():
	return int(_time())

def readSettings(st):
	global ZMQ_DAL_REQ, ZMQ_EMC_REQ, ZMQ_PUBLISHER, ZMQ_LEA_REP, ZMQ_SCADA_REP, ZMQ_SUBSCRIBER, ZMQ_MOTOR_REQ

	conf = json.loads(st)['configuration']
	ZMQ_EMC_REQ = conf["EmcEndpoint"]
	ZMQ_PUBLISHER = conf["PublisherEndpoint"]
	ZMQ_LEA_REP = conf["LeaEndpoint"]
	ZMQ_SUBSCRIBER = conf["SubscriberEndpoint"]
	

# settings
setAutoCreateOnStart = 	False
setPublDataChangeInterpretations = False

DalDir = 'dalStore'
def dalFile(key, prefix='le'):
	if (type(key)==int):
		return os.path.join(DalDir, '%s-%d.json'%(prefix,key))
	return os.path.join(DalDir, '%s'%key)
	
X_WIN_POS = 1000

class Wnd_():
	
	def __init__(self):
		self.root = Tk(); self.root.title(u'Lea test');
		#self.root.bind('<Key>', self.hndl_key)
		self.root.minsize(500, 200)
		self.root.maxsize(600, 800)
		#w=500; h=600;
		#x=self.root.winfo_screenwidth()/2-w/2; y=self.root.winfo_screenheight()/2-h/2
		#self.root.geometry('%dx%d+%d+%d'%(w,h,0,0))
		self.root.geometry('+%d+%d'%(0,0))
		#self.root.iconify()
		
		readSettings(open(dalFile('application_configuration.json')).read())
				
		#~ fr_info = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	
		#~ fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		#~ self.info = StringVar()
		#~ Label(fr_info, textvariable = self.info).pack(side = LEFT)
		
		fr1 = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	
		fr1.pack(side=TOP, expand=YES, fill=X)
		
		Button(fr1, text='Send From File',width=10,command=self.sendFromFile,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.strFileName = StringVar(value='create-req.json');
		Entry(fr1,textvariable = self.strFileName).pack(side=LEFT,expand=YES,fill=X,padx=5,pady=5)
				
		fr2 = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	
		fr2.pack(side=TOP, expand=YES, fill=BOTH)
		fr3 = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	
		fr3.pack(side=TOP, expand=YES, fill=BOTH)
		
		Button(fr2, text='Send',width=10,command=self.sendFromText,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr2, text='Cleare',width=10,command=lambda:self.jsonText.delete('1.0', END),takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)
		Button(fr2, text='Save',width=10,command=self.saveJsonToFile,takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)
		Button(fr2, text='Load JSON',width=10,command=self.loadJsonFromFile,takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)
		self.jsonText = Text(fr3, font=10)
		self.jsonText.pack(side=BOTTOM, expand=YES, fill=BOTH)	
		self.jsonText.insert('1.0', open('test.out').read())
		
		fr4 = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)	
		fr4.pack(side=TOP, expand=YES, fill=X)
		self.logRecSubscrData = BooleanVar(value=False)
		Checkbutton(fr4, text='Show subscribed data', variable=self.logRecSubscrData).pack(side=TOP, anchor='w', expand=NO, pady=0, ipady=0)
		self.logEmcReq = BooleanVar(value=False)
		Checkbutton(fr4, text='Show Emc requests', variable=self.logEmcReq).pack(side=TOP, anchor='w', expand=NO, pady=0, ipady=0)
		self.logPublishedData = BooleanVar(value=False)
		Checkbutton(fr4, text='Show published data', variable=self.logPublishedData).pack(side=TOP, anchor='w', expand=NO, pady=0, ipady=0)
				
		
		w1 = Toplevel(); w1.title('iWell objects')
		w1.protocol("WM_DELETE_WINDOW", self.root.destroy)
		w1.geometry('+%d+%d'%(X_WIN_POS, 0))
		#w1.minsize(50, 50)
		self.interpInpFr = Frame(w1,relief=GROOVE)	
		Label(self.interpInpFr, text='Inputs',font=('Arial', 14, 'bold')).pack(side=TOP,padx=25,pady=5)
		self.interpOutpFr = Frame(w1, relief=GROOVE)	
		Label(self.interpOutpFr, text='Outputs',font=('Arial', 14, 'bold')).pack(side=TOP,padx=25,pady=5)
		self.interpInpFr.pack(side=TOP, expand=YES, fill=BOTH)
		self.interpOutpFr.pack(side=TOP, expand=YES, fill=BOTH)		
		self.iwellObjs = {}				
		self.queueReqObj = Queue.Queue()	
		self.queueEMCReply = Queue.Queue()	
		self.updateEMCReq()
		
		self.w2 = Toplevel(); self.w2.title('Expressions')
		self.w2.protocol("WM_DELETE_WINDOW", self.root.destroy)
		self.w2.geometry('+%d+%d'%(X_WIN_POS, 400))
		fr21 = Frame(self.w2, height=20, borderwidth=2, relief=GROOVE)	
		fr21.pack(side=TOP, expand=YES, fill=X)
		Button(fr21, text='Create',width=10,command=self.createExpression,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr21, text='Update',width=10,command=self.updateExpression,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr21, text='Delete',width=10,command=self.deleteExpression,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr21, text='Get',width=10,command=self.getExpression,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr21, text='Verify',width=10,command=self.verifyExpression,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		self.capabilityStatus = BooleanVar()
		self.capabilityStatus.set(json.loads(open(dalFile('capabilityStatus.json')).read())['status'])
		Checkbutton(fr21, indicatoron=False,text='Capability', variable=self.capabilityStatus,width=10,height=1,command=self.capabilityStatusChanged).pack(side=LEFT,padx=5,pady=5,ipady=4)
		fr22 = Frame(self.w2, height=20, borderwidth=2, relief=GROOVE)	
		fr22.pack(side=TOP, expand=YES, fill=X)
		self.strExprId = StringVar(value='1');
		Entry(fr22,textvariable = self.strExprId,width=10).pack(side=TOP,expand=YES,fill=X,padx=5,pady=5)
		self.strExprRes = StringVar(value='output-1');
		Entry(fr22,textvariable = self.strExprRes,width=40).pack(side=TOP,expand=YES,fill=X,padx=5,pady=5)
		self.strExpr = StringVar(value='di1 and di2');
		Entry(fr22,textvariable = self.strExpr,width=40).pack(side=TOP,expand=YES,fill=X,padx=5,pady=5)
		self.logicDelay = IntVar(value=0);
		Entry(fr22,textvariable = self.logicDelay,width=40).pack(side=TOP,expand=YES,fill=X,padx=5,pady=5)
		fr23 = Frame(self.w2, height=20, borderwidth=2, relief=GROOVE)	
		fr23.pack(side=TOP, expand=YES, fill=X)
		Button(fr23, text='Create',width=10,command=self.createAction,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr23, text='Update',width=10,command=self.updateAction,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr23, text='Delete',width=10,command=self.deleteAction,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr23, text='Get',width=10,command=self.getAction,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		Button(fr23, text='Clear Latched',width=10,command=self.clearLatched,takefocus=OFF).pack(side=LEFT,padx=5,pady=5)
		fr24 = Frame(self.w2, height=20, borderwidth=2, relief=GROOVE)	
		fr24.pack(side=TOP, expand=YES, fill=X)
		self.strAction = StringVar(value = 'flag');
		Entry(fr24,textvariable = self.strAction,width=10).pack(side=TOP,expand=YES,fill=X,padx=5,pady=5)
		
		self.zmqInit()	
		
		self.reqIdCnt = 0	
				
	def getReqId(self):
		self.reqIdCnt += 1
		return 'HMI%d'%self.reqIdCnt
		
	#def hndl_key(self, ev):
		#if ev.keysym=='Control_R': pass
	
	def updateEMCReq(self):
		try:
			while 1:
				name = self.queueReqObj.get_nowait()
				self.addObject(name)				
		except Queue.Empty:
			self.root.after(300, self.updateEMCReq)
				
	def recvEMC(self):
		while True:			
			req = self.emc_socket.recv()
			if self.logEmcReq.get():
				print 'EMC req:\n%s'%req
			self.reqEMCJson = json.loads(req)
			if self.reqEMCJson['method'] == 'data-request': 
				for v in self.reqEMCJson['values']:			# Vova's EMC status request
					interp = v[0]['ValueID']
					self.queueReqObj.put(interp)
				#sleep(0.5)
				self.EMCReply()

				reply = self.queueEMCReply.get()
				print 'EMC reply:\n%s'%reply
				self.emc_socket.send(reply)
			elif self.reqEMCJson['method'] == 'Port_write':
				v0 = self.reqEMCJson['values'][0]
				self.queueChEmcData.put((v0[0], v0[1]))
				self.emc_socket.send('Indifferent reply')
			
	def addObject(self, name):
		if (self.iwellObjs.has_key(name)): return
		if (name.find('output') != -1):
			state = DISABLED
			frame = self.interpOutpFr
		else:
			state = NORMAL
			frame = self.interpInpFr
		def handler():
			self.publishObjState(name)
		var = BooleanVar()
		self.iwellObjs[name] = var
		Checkbutton(frame, text=name, variable=var, state = state, command=handler).pack(side=TOP, anchor='w', expand=NO, pady=0, ipady=0)
		
		
	def zmqInit(self):
		context = zmq.Context()
		
		self.hmi_socket = context.socket(zmq.REQ)
		self.hmi_socket.connect(ZMQ_LEA_REP)
		self.hmiSocketBusy = False
		self.hmiLastReply = ''
		
		subscr_socket = context.socket(zmq.SUB)
		subscr_socket.connect(ZMQ_PUBLISHER)
		subscr_socket.setsockopt(zmq.SUBSCRIBE, '')
		self.queueChEmcData = Queue.Queue()		# queue for changing EMC data. It waits tuple (name, value)
		def recvPubl():
			while 1:
				rec = subscr_socket.recv_multipart()
				if self.logRecSubscrData.get():
					print 'Published data:\n%s'%rec
				if setPublDataChangeInterpretations:
					bodyMsg = json.loads(rec[2])
					if bodyMsg['evalutation_status']=='successful':
						self.queueChEmcData.put((bodyMsg['resultant'], bodyMsg['value']))
					else:
						print 'Error published message [%s]'%bodyMsg			
		thread.start_new_thread(recvPubl, ())
		self.processPubl()
		
		self.emc_socket = context.socket(zmq.REP)
		self.emc_socket.bind(ZMQ_EMC_REQ)
		thread.start_new_thread(self.recvEMC, ())
		
		self.publ_socket = context.socket(zmq.PUB)
		self.publ_socket.bind(ZMQ_SUBSCRIBER)
		
		self.dal_socket = context.socket(zmq.REP)
		self.dal_socket.bind(ZMQ_DAL_REQ)
		thread.start_new_thread(self.recvDAL, ())
					
	def sendFromFile(self):
		msgOut = open(self.strFileName.get()).read()
		self.sendHMIRequest(msgOut)
		
	def sendHMIRequest(self, msgOut):
		if not msgOut or self.hmiSocketBusy: return
		print 'Sending: %s'%msgOut
		def _send(msg):
			self.hmiSocketBusy = True
			self.hmi_socket.send(msgOut)		
			msgIn = self.hmi_socket.recv()
			print 'Recieved: %s'%msgIn
			self.hmiSocketBusy = False
			self.hmiLastReply = msgIn
		thread.start_new_thread(_send, (msgOut,))
		
	def loadJsonFromFile(self):
		jF = tkFileDialog.askopenfile()
		if jF:
			self.loadJsonObjToText(json.load(jF))
	
	def loadJsonObjToText(self, jObj):		
		self.jsonText.delete('1.0', END)
		self.jsonText.insert('1.0', json.dumps(jObj, indent=4, separators=(',', ' : '), sort_keys=True))
	
	def addJsonObjToText(self, jObj):		
		self.jsonText.insert(END, '\n\n')
		self.jsonText.insert(END, json.dumps(jObj, indent=4, separators=(',', ' : '), sort_keys=True))
	
	
	def saveJsonToFile(self):
		jF = tkFileDialog.asksaveasfile(defaultextension='.json')
		if jF:
			jF.write(self.jsonText.get(1.0, END)),
		
	def sendFromText(self):
		msgOut = str(self.jsonText.get(1.0, END))   #json.dumps(json.loads(self.jsonText.get(1.0, END)))
		self.sendHMIRequest(msgOut)
	
	def EMCReply(self):
		reply = {'uniqueReqID':self.reqEMCJson['uniqueReqID'], 'ts':time(), 'method':'data-reply'}
		
		# next sheer nonsense... remake!!
		if self.reqEMCJson['values'][0][0]['parameter'] == 'Status':
			arr = []
			for v in self.reqEMCJson['values']:
				name = v[0]['ValueID']
				value = 0
				if self.iwellObjs.has_key(name) and self.iwellObjs[name].get():
					value = 1
				arr.append({'explanation':'VALUE FOUND', 'ValueID':name, 'value': {'Scaled_value' : value}})
			reply['values'] = [['Status', arr, '']]
		elif self.reqEMCJson['values'][0][0]['parameter'] == 'Get_EMC_Interpretations':
			interprName = self.reqEMCJson['values'][0][0]['ValueID']
			if interprName.find('output') == -1:
				portType = 'digital_input'
			else:
				portType = 'digital_output'
			dd = {	"interpretation_uuid" : interprName,
					"port_type" : portType}
			reply['values'] = [['Get_EMC_Interpretations', dd, '']]
		
		self.queueEMCReply.put_nowait(json.dumps(reply))

	def processPubl(self):
		try:
			while 1:
				msg = self.queueChEmcData.get_nowait()
				self.addObject(msg[0])
				self.iwellObjs[msg[0]].set(msg[1])
		except Queue.Empty:
			self.root.after(300, self.processPubl)
			
	def publishObjState(self, name):
		pbl = [None, {'origin_app':'leaTest', 'destination_app':'all'}, {"name": name, 'ts': time(), 'str_ts': 'Blabla'}]
		if (name.endswith('alarm')):
			pbl[0] = ('alarm', name)
			if self.iwellObjs[name].get(): pbl[2]['value'] = 'active'
			else: pbl[2]['value'] = 'inactive'
		else:
			pbl[0] = ('data', name)
			pbl[2]['value'] = self.iwellObjs[name].get()
			
		pbl = map(lambda p:json.dumps(p, separators=(',', ':')), pbl)
		
		if self.logPublishedData.get():
			print 'Publishing: %s'%pbl
		self.publ_socket.send_multipart(pbl)
		
		
	def createExpression(self):
		if self.iwellObjs.has_key(self.strExprRes.get()) or self.strExprRes.get().find('output')==-1:
			if not tkMessageBox.askyesno('','Are you sure?', parent = self.w2):
				return
		
		crReq = self.createUpdateObj()
		crReq["values"] = [["create_logical_equation"]]
				
		self.loadJsonObjToText(crReq)
		self.sendHMIRequest(json.dumps(crReq))		
	
	def updateExpression(self):
		updReq = self.createUpdateObj()
		updReq["values"] = [["update_logical_equation"]]
		
		self.loadJsonObjToText(updReq)
		self.sendHMIRequest(json.dumps(updReq))	
		
	def verifyExpression(self):
		vReq = self.createUpdateObj()
		vReq["values"] = [["verify_logical_equation"]]
		vReq['method'] = 'data-verify'
		
		self.loadJsonObjToText(vReq)
		self.sendHMIRequest(json.dumps(vReq))		
	
	def createUpdateObj(self):
		cUObj = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-commit"};
		lstLex =  self.strExpr.get().split()
		eqArr = []
		for e in lstLex:	
			if e=='and' or e=='or' or e=='not' or e=='AND' or e=='OR' or e=='NOT' or e=='low' or e=='LOW' or e=='high' or e=='HIGH': 
				eqArr.append({"component_type" : e.upper()})
			elif e=='(': 
				eqArr.append({"component_type" : 'OPEN'})
			elif e==')': 
				eqArr.append({"component_type" : 'CLOSE'})
			else:
				eqArr.append({"component_type" : "HW_INTERP", "hw_interp_uuid" : e})	
		eqArr.append({"component_type" : "END"})
		cUObj['params'] = [{"le_id" : int(self.strExprId.get()), "resultant" : self.strExprRes.get(), "equation_array" : eqArr, 'logic_delay' : self.logicDelay.get()}]
		return cUObj
		
	
	def deleteExpression(self):
		dReq = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-delete", "values" : [["delete_logical_equation"]], 'params' : [{"le_id" : int(self.strExprId.get())}] }
		self.loadJsonObjToText(dReq)
		self.sendHMIRequest(json.dumps(dReq))
		
	def waitHMIReply(self):
		self.hmiLastReply = ''	
		while self.hmiLastReply=='':
			sleep(0.1)
			print '.'
		rpl = json.loads(self.hmiLastReply)
		self.addJsonObjToText(rpl)
		return rpl				
		
	def getExpression(self):
		dReq = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-request", "values" : [["get_logical_equation"]], 'params' : [{"le_id" : int(self.strExprId.get())}] }
		self.loadJsonObjToText(dReq)
		self.sendHMIRequest(json.dumps(dReq))
		rpl = self.waitHMIReply()
		if rpl['values'][0][0]	!= "get_logical_equation":
			self.strExprRes.set('')
			print 'Something is wrong'; return
		gottenEq = rpl['values'][0][1]
		if gottenEq == 'Unsuccessful':
			print 'No such id'
			self.strExprRes.set('')
			self.strExpr.set('')
			self.logicDelay.set('')
			return
		print 'Gotten equation ', gottenEq
		expr = ''
		for c in gottenEq['equation_array']:
			cType = c['component_type']
			if cType == 'HW_INTERP':
				expr += c['hw_interp_uuid']
			elif cType=='AND' or cType=='OR' or cType=='NOT' or cType=='HIGH' or cType=='LOW':
				expr += cType
			elif cType=='OPEN':
				expr += '('
			elif cType=='CLOSE':
				expr += ')'				
			expr += ' '
		self.strExprRes.set(gottenEq['resultant'])
		self.strExpr.set(expr)
		self.logicDelay.set(gottenEq.get('logic_delay', ''))	
			
	
	def recvDAL(self):
		while True:			
			stReq = self.dal_socket.recv()
			print 'DAL req:\n%s'%stReq
			reqDAL = json.loads(stReq)
			
			reqKey = reqDAL['values'][0][0]
			
			reply = copy.deepcopy(reqDAL)
			del reply['method']
			del reply['params']
			del reply['version']
						
			if reqKey == 'add_logic_equation' or reqKey == 'create_action' or reqKey == 'update_action':
				if reqKey == 'add_logic_equation': fPrefix = 'le'
				else: fPrefix = 'act'
				fName = dalFile(reqDAL['params'][0]['le_id'], fPrefix)
				if os.access(fName, os.F_OK) and reqKey == 'add_logic_equation':
					reply['values'][0].append('unsuccessful')
					reply['values'][0].append('Le id exists')
				else:
					open(fName, 'w').write(json.dumps(reqDAL['params'][0]))
					reply['values'][0].append('Ok')
					reply['values'][0].append('')
			elif reqKey == 'delete_logic_equation' or reqKey == 'delete_action':
				if reqKey == 'delete_logic_equation': fPrefix = 'le'; key = 'equation_ID'
				else: fPrefix = 'act'; key = 'le_id'
				try: os.remove(dalFile(reqDAL['params'][0][key], fPrefix))
				except OSError: reply['values'][0].append('unsuccessful')
				else: reply['values'][0].append('Ok')
				reply['values'][0].append('')
			elif reqKey == 'get_list_of_logic_equations':
				lEqs = []
				for f in filter(lambda s:s.find('le')!=-1, os.listdir(DalDir)):
					lEqs.append(json.loads(open(dalFile(f)).read()))
				reply['values'][0].append(lEqs)
				reply['values'][0].append('')
			elif reqKey == 'get_application_configuration':
				reply['values'][0].append(json.loads(open(dalFile('application_configuration.json')).read()))
				reply['values'][0].append('')
			elif reqKey == 'update_logical_expression_capability_status':
				open(dalFile('capabilityStatus.json'), 'w').write(json.dumps(reqDAL['params'][0]))
				reply['values'][0].append('Ok')
				reply['values'][0].append('')			
			elif reqKey == 'get_logical_expression_capability_status':
				reply['values'][0].append(json.loads(open(dalFile('capabilityStatus.json')).read()))
				reply['values'][0].append('')
			elif reqKey == 'get_action':
				try:
					reply['values'][0].append(json.loads(open(dalFile(reqDAL['params'][0]['le_id'], 'act')).read()))
					reply['values'][0].append('')
				except IOError:
					reply['values'][0].append('unsuccessful')
					reply['values'][0].append('No action')
			#elif reqKey == '':
			else:
				reply['values'][0].append('unsuccessful')
				reply['values'][0].append('Unknown request')			
				
			stRep = json.dumps(reply)
			print 'DAL reply %s\n'%stRep
			self.dal_socket.send(stRep)
		
	def capabilityStatusChanged(self):
		self.capabilityStatus.get()
		dReq = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-commit",
				"values" : [["update_logical_expression_capability_status"]], 'params' : [{"status" : self.capabilityStatus.get()}] }
		self.loadJsonObjToText(dReq)
		self.sendHMIRequest(json.dumps(dReq))
		
	def createAction(self):
		actionType = self.strAction.get().upper()
		if actionType.find('TIMER') == 0:
			actionType, period = actionType.split()
		req = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-commit", "values" : [["create_action"]], 'params' : [{"le_id" : int(self.strExprId.get()), 'action':actionType}] }
		if actionType == 'TIMER':
			req['params'][0]['timer_period'] = int(period)
		self.loadJsonObjToText(req)
		self.sendHMIRequest(json.dumps(req))	
		
	def updateAction(self):
		actionType = self.strAction.get().upper()
		if actionType.find('TIMER') == 0:
			actionType, period = actionType.split()
		req = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-commit", "values" : [["update_action"]], 'params' : [{"le_id" : int(self.strExprId.get()), 'action':actionType}] }
		if actionType == 'TIMER':
			req['params'][0]['timer_period'] = int(period)
		self.loadJsonObjToText(req)
		self.sendHMIRequest(json.dumps(req))	
	def	deleteAction(self):
		req = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-delete", "values" : [["delete_action"]], 'params' : [{"le_id" : int(self.strExprId.get())}] }
		self.loadJsonObjToText(req)
		self.sendHMIRequest(json.dumps(req))
	def	getAction(self):
		req = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-request", "values" : [["get_action"]], 'params' : [{"le_id" : int(self.strExprId.get())}] }
		self.loadJsonObjToText(req)
		self.sendHMIRequest(json.dumps(req))
		rpl = self.waitHMIReply()
		if rpl['values'][0][0]	!= "get_action":
			self.strAction.set('')
			print 'Something is wrong'; return
		gottenAct = rpl['values'][0][1]
		if gottenAct == 'Unsuccessful':
			self.strAction.set('')
		elif gottenAct['action'] == 'TIMER':
			self.strAction.set('TIMER %d'%gottenAct['timer_period'])
		else:
			self.strAction.set(gottenAct['action'])
			
	def clearLatched(self):
		req = {"uniqueReqID" : self.getReqId(), "ts" : time(), "method" : "data-commit", "values" : [["clear_logical_equation"]], 'params' : [{"le_id" : int(self.strExprId.get())}] }
		self.loadJsonObjToText(req)
		self.sendHMIRequest(json.dumps(req))
			
if __name__=='__main__': 
	w=Wnd_()
	
	if not os.access(DalDir, os.F_OK):
		os.mkdir(DalDir)
	
	if setAutoCreateOnStart:
		w.createExpression()
	
	w.root.mainloop()
	
