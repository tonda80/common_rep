import os
import pickle

def get_prefix(path):
	return os.path.normpath(path)+os.path.sep

#get recursive list  of file  from root
#returns list of full path to file
def file_list(root_path):
	files = []
	prefix = get_prefix(root_path)
	for item in os.listdir(root_path):
		path = prefix+item
		if os.path.isdir(path):
			files += file_list(path)
 		else:
			files.append(path)
	return files

#prepare relation pasth, exclude root from path. if root not found return npath
def relation_path(root, path):
	nroot = os.path.normpath(root)
	npath = os.path.normpath(path)
	try:
		start = npath.index(nroot)+len(nroot)+len(os.path.sep)
		if(start < len(npath)):
			return npath[start:]
	except ValueError:
		pass
	return  npath

#check changes in files by time modification
#returns list of changes files
def check_changes(root_path, files_time):
	transfer = []

	start_root = len(get_prefix(root_path))
	for file_ in file_list(root_path):
		try:
			if os.stat(file_).st_mtime > files_time[file_]:
				transfer.append(file_[start_root:])
				files_time[file_] = os.stat(file_).st_mtime
		except KeyError:
			files_time[file_] = os.stat(file_).st_mtime
	return transfer


#create initial dictionary with file and according times modificatin
#will called when  prj_state.pkl not exist
def init_project_list(root_path):
	files_time = {}
	for file in file_list(root_path):
		files_time[file] = os.stat(file).st_mtime

	archive = open('prj_state.pkl','wb')
	pickle.dump(files_time, archive)
	archive.close()


#test method  write list of changed file to file
def test_save_changes_list(transfer):
    print transfer;
#	tr_list = open(r'transfer_list.txt','wt')
#	for file in transfer:
#		tr_list.write(file+'\n')
#		print file+'\n'


#Root Api method should be  called for get list of changes files
#parameter: root path of project
#load  and update dictionary when changes in file was detected.
def get_list_of_changed_file(root):
	try:
		files_time = {}
		archive = open('prj_state.pkl','rb')
	except IOError:
		init_project_list(root)
		return []

	files_time = pickle.load(archive)
	archive.close()

	transfer = check_changes(root, files_time)

	archive = open('prj_state.pkl','wb')
	pickle.dump(files_time, archive)
	archive.close()

	return transfer


if __name__ == "__main__":
    test_save_changes_list(get_list_of_changed_file(r'D:\Volgasoft\RealflexUnlimDB\rf6unlimDB'))

