#coding=1251

from ftplib import *
from sys import argv
from ffgit import *
from time_transfer import get_list_of_changed_file
import wndchecks

DEFAULT_CONF_FILE = 'ftpconf.cfg'

# functions for parsing of config file	
def str_not_filename(st):
	if len(st)==0 or st[0]=='#' or st[0]=='!':	# empty string; # - comment; ! - config
		return True
def str_not_conf(st):
	if len(st)==0 or st[0]!='!':	# config string is begun with !
		return True
		
def get_settings(file_conf):
	# # - comment; ! - config info
	# valid config parameters - !HOST !LOGIN, !PASSWD, !SRC_ROOT, !DST_ROOT, !GIT (bool), !TIME (bool), !CONF (bool), !GUI (bool)
	config = {}
	# try of a reading settings of the connection from the file
	file_conf.seek(0, 0)
	for st in file_conf:
		if str_not_conf(st): continue
		lst_st = st[1:].split('=')
		if (len(lst_st) == 1):
			config[lst_st[0].strip()] = 1	# a bool parameter
		elif len(lst_st) == 2:
			config[lst_st[0].strip()] = lst_st[1].strip()	
		else:
			print 'Warning!\t Can\'t parse string "%s"'%st

	# default settings of connection
	config.setdefault('HOST', '192.168.9.28')
	config.setdefault('LOGIN', 'root')
	config.setdefault('PASSWD', 'tuscan')
	config.setdefault('SRC_ROOT', 'D:/rep-s/rf6/')
	config.setdefault('DST_ROOT', '/rf6/')
	config.setdefault('GIT', None)	
	config.setdefault('CONF', None)
	config.setdefault('TIME', None)
	config.setdefault('GUI', None)

	if config['SRC_ROOT'][-1] != '/': config['SRC_ROOT'] += '/'
	if config['DST_ROOT'][-1] != '/': config['DST_ROOT'] += '/'

	return config

def get_list_from_conf():
	global file_conf_name
	file_conf = open(file_conf_name, 'rb')
	result = []
	for st in file_conf:
		file = st.strip()
		if str_not_filename(file): continue
		result.append(file)
	file_conf.close()
	return result
	
def get_file_list():
	global config
	file_list = []
	str_sources = 'Files are gotten from: '
	if config['GIT']:
		file_list += get_list_from_git(config['SRC_ROOT'])
		str_sources += 'git, '
	if config['CONF']:	
		file_list += get_list_from_conf()
		str_sources += 'config file, '
	if config['TIME']:	
		file_list += get_list_of_changed_file(config['SRC_ROOT'])
		str_sources +=  'changing of time, '
	print str_sources[:-2]+'.'
	return file_list
	
def qnx_name(path):
	return path.replace('\\', '/')
def win_name(path):
	return path.replace('/', '\\')
	
def get_ftp_connect():
	print 'Trying of connection with FTP server on %s'%(config['HOST'],)
	try:
		ftp_client = FTP(config['HOST'], config['LOGIN'], config['PASSWD'])
	# except error_perm:
		# print 'Error! Error of logging'
		# exit()
	except Exception, exc:
		print 'Error!\t Something bad is occured by connection'
		print exc
		return
	return ftp_client

def transfer_file_list(file_list):
	ftp_client = get_ftp_connect()
	if ftp_client == None:
		return "Cannot connect to ftp server"

	cnt_file = 0
	cnt_error = 0
	for file in file_list:
		try:
			src_name = win_name(config['SRC_ROOT']+file)
			dst_name = qnx_name(config['DST_ROOT']+file)
			ftp_client.storbinary('STOR '+dst_name, open(src_name, 'rb'))
			cnt_file += 1
		except IOError:
			print 'Error!\t File %s not found'%src_name
			cnt_error += 1
			continue
		except Exception, exc:
			print 'Error!\t Something bad is occured by file sending'
			print exc
			cnt_error += 1
			continue
		else:
			print 'Ok.\t File %s is sended'%file
			
	result = 'Total %d files sended to %s'%(cnt_file, config['HOST'])
	print result
	
	ftp_client.quit()
	
	return result + ". %d errors"%cnt_error


# =============================================================================
# main

# opening of the config file
try:
	file_conf = open(argv[1], 'rb')
except (IndexError, IOError):
	try:
		file_conf = open(DEFAULT_CONF_FILE, 'rb')
	except IOError:
		print 'Error!\t Opening of the config file'
		exit()
	
config = get_settings(file_conf)
file_conf.close()	
file_conf_name = file_conf.name

print 'The configuration file %s'%file_conf_name 

if config['GUI']:
	wnd = wndchecks.WndCheck(get_file_list, transfer_file_list)
	wnd.mainloop()
else:
	transfer_file_list(get_file_list())
