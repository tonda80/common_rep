# coding=cp1251
from Tkinter import *
from datetime import datetime

class WndCheck():
	
	def __init__(self, clb_get_list, clb_send, n_ch_col=20):
		self.root = Tk(); self.root.title(u'');
		w=440; h=240;
		#x=self.root.winfo_screenwidth()/2-w/2; y=self.root.winfo_screenheight()/2-h/2
		#self.root.geometry('%dx%d+%d+%d'%(w,h,x,y))
		self.root.minsize(w, h)
		#self.root.bind('<Key>', self.hndl_key)
		
		self.clb_send = clb_send
		self.clb_get_list = clb_get_list
			
		# info frame
		fr_info = Frame(self.root, height=10, borderwidth=2, relief=GROOVE)
		fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		self.info = StringVar()
		Label(fr_info, textvariable = self.info).pack(side = LEFT)
				
		# frame with checks
		self.fr_checks = Frame(self.root, borderwidth=2, relief=GROOVE)
		self.fr_checks.pack(side=TOP, expand=YES, fill=BOTH)
		self.dict_checks = {}	# {path:boolvar, }
		self.N_CHECKS_COL = n_ch_col	# a number of checkbuttons to a columns
		self.get_list()
		
		# button frame
		self.fr_btn = Frame(self.root, height=30, borderwidth=2, relief=GROOVE)
		self.fr_btn.pack(side=BOTTOM, expand=NO, fill=X)
		b = Button(self.fr_btn, text='Send',width=10,command=self.apply_checks)
		b.pack(side=LEFT,padx=5,pady=5); b.focus_set()
		Button(self.fr_btn, text='Update',width=10,command=self.get_list).pack(side=LEFT,padx=5,pady=5)
		Button(self.fr_btn, text='Uncheck all',width=10,command=self.uncheck_all).pack(side=RIGHT,padx=5,pady=5)
		Button(self.fr_btn, text='Check all',width=10,command=self.check_all).pack(side=RIGHT,padx=5,pady=5)
	
				
		#self.count = 0; self.timer()	# �������, �������������� � �������
		
	def mainloop(self):
		self.root.mainloop()
		
	#def timer(self):	
		#self.count += 1
		#self.root.after(500, self.timer)
	
	#def hndl_key(self, ev):
	#	if ev.keysym=='Control_R': pass
		
	def place_checks(self, seq):
		self.clear_all_checks()
		
		cnt_ch = 0
		def new_frame():
			fr = Frame(self.fr_checks)#, borderwidth=2, relief=GROOVE)
			fr.pack(side=LEFT, expand=YES, fill=BOTH)
			return fr
		fr = new_frame()
		for st in seq:
			if cnt_ch == self.N_CHECKS_COL:
				fr = new_frame()
				cnt_ch = 0
			var = BooleanVar(value=True)
			chb = Checkbutton(fr, text=st, variable=var)
			chb.pack(side=TOP, anchor='w', expand=NO, pady=0, ipady=0)
			self.dict_checks[st] = var	# {path:boolvar, }			
			cnt_ch += 1 
			
	def clear_all_checks(self):
		for c in self.fr_checks.children.values(): c.destroy()
		self.dict_checks = {}
		
	def get_selected(self):
		return [i for i in self.dict_checks.iterkeys() if self.dict_checks[i].get()]
	
	def check_all(self):
		for r in self.dict_checks.values(): r.set(True)
	def uncheck_all(self):
		for r in self.dict_checks.values(): r.set(False)
	
	def apply_checks(self):
		result = self.clb_send(self.get_selected())
		timestamp = datetime.now().strftime('%H:%M:%S')
		self.info.set('%s: %s'%(timestamp, result))
		
	def get_list(self):
		self.place_checks(self.clb_get_list())
			
if __name__=='__main__':
	
	def test_send(seq):
		print 'Checked:',
		for e in seq: print e,
		print
		
	n = 45
	def test_get_list():
		global n
		n += 1
		return range(n)
	
	lst = (u'����', u'����', 'Gregory', 'q'*1000)
	#w = WndCheck(lst)
	w = WndCheck(test_get_list, test_send)
	
	def test2():
		print 'Button is working!'
	Button(w.fr_btn, text='Test place',width=10,command=test2,takefocus=OFF, bg='yellow').pack(side=RIGHT,padx=25,pady=5)	
	
	w.mainloop()