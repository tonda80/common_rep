#coding=1251

# files from git

import os
import subprocess

def get_list_from_git(path):
	result = set()
	currdir = os.getcwd()
	os.chdir(path)
	pr = subprocess.Popen(('git', 'status'), stdout=subprocess.PIPE)
	(stdoutdata, stderrdata) = pr.communicate()
	crit_list = ('modified:', 'new file:')
	for crit in crit_list:
		for st in stdoutdata.split('\n'):
			#print st
			tp = st.partition(crit)
			name = tp[2].strip()
			if name:
				if '(new commits)' not in name:	# excluded submodules
					result.add(name)
	os.chdir(currdir)
	return result
	
if __name__ == "__main__":
	lst = get_list_from_git(r'd:/rep-s/rf6')
	for fn in lst:
		print fn