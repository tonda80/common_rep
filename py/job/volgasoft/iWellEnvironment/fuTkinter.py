#!/usr/bin/python

from Tkinter import *

		
def placeWindowTo(wnd, x, y):
	wnd.geometry('+%d+%d'%(x,y))		
		
def placeWindowToCenter(wnd, dX = 0, dY = 0):
	x = wnd.winfo_screenwidth()/2 - dX
	y = wnd.winfo_screenheight()/2 - dY
	wnd.geometry('+%d+%d'%(x,y))
	
def setWindowSize(wnd, w , h):
	wnd.geometry('%dx%d'%(w, h))	

def fuFrame(master, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	args.setdefault('borderwidth', 2)
	args.setdefault('relief', GROOVE)
	packArgs.setdefault('expand', YES)
	packArgs.setdefault('fill', BOTH)
	
	frame = Frame(master, **args)	
	frame.pack(**packArgs)
	return frame
	
def fuLabelFrame(master, label, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	args.setdefault('borderwidth', 2)
	args.setdefault('relief', GROOVE)
	packArgs.setdefault('expand', YES)
	packArgs.setdefault('fill', BOTH)
	
	labelFrame = LabelFrame(master, text = label, **args)	
	labelFrame.pack(**packArgs)
	return labelFrame
	
def fuEntry(master, defaultValue = '', ValueType = StringVar, callback = None, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	args.setdefault('width', 20)
	packArgs.setdefault('padx', 5)
	packArgs.setdefault('pady', 5)
	
	_var = ValueType(value=defaultValue)
	Entry(master, textvariable = _var, **args).pack(**packArgs)
	
	if callback is not None:			# callback[0](callback[1], _var) will react to a changes _var
		_var.trace('w', lambda *a: callback[0](callback[1], _var))
		
	return _var
	
def fuButton(master, command, text = 'Button', packArgs = None, **args):
	if packArgs == None: packArgs = {}
	args.setdefault('width', 10)
	args.setdefault('takefocus', OFF)
	packArgs.setdefault('padx', 5)
	packArgs.setdefault('pady', 5)
	
	button = Button(master, text = text, command = command, **args)
	button.pack(**packArgs)	
	return button
		
def fuCheckbutton(master, text = '', command = None, defaultValue = False, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	packArgs.setdefault('anchor', 'w')
	
	bVar = BooleanVar(value = defaultValue)
	Checkbutton(master, variable = bVar, command = command, text = text, **args).pack(**packArgs)
	
	return bVar
		
def fuText(master, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	args.setdefault('font', 'Courier 10')
	args.setdefault('height', 10)
	packArgs.setdefault('expand', YES)
	packArgs.setdefault('fill', BOTH)
	
	text = Text(master, **args)
	text.pack(**packArgs)
	
	return text 

def fuOptionMenu(master, options, command = None, ValueType = StringVar, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	packArgs.setdefault('padx', 5)
	packArgs.setdefault('pady', 5)
	
	if len(options) > 0:
		defaultValue = options[0]
	else:
		defaultValue = ''
	var = ValueType(value=defaultValue)	
	optionMenu = OptionMenu(master, var, *options, command = command, **args).pack(**packArgs)

	return var

def fuLabel(master, text, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	packArgs.setdefault('padx', 5)
	packArgs.setdefault('pady', 5)
		
	label = Label(master, text = text, **args)
	label.pack(**packArgs)
	
	return label
	
def fuMutableLabel(master, defaultValue = '', ValueType = StringVar, packArgs = None, **args):
	if packArgs == None: packArgs = {}
	packArgs.setdefault('padx', 5)
	packArgs.setdefault('pady', 5)
		
	textVar = ValueType(value=defaultValue)	
	Label(master, textvariable = textVar, **args).pack(**packArgs)
	
	return textVar
	
def createStatusBar(master, font = 'System 11'):
	frame = fuFrame(master, height=20, borderwidth=2, relief=GROOVE, packArgs={'side':BOTTOM, 'fill':X, 'expand':NO})
	# For some reason "expand - YES" places the bar into a window middle
	return fuMutableLabel(frame, font = font, packArgs={'side':LEFT, 'pady':0})

#~ def fu(master, packArgs = None):
	#~ if packArgs == None: packArgs = {}
	#~ args.setdefault('', )
	#~ packArgs.setdefault('', )
	#~ 
	 #~ = (master, **args)
	#~ .pack(**packArgs)
	#~ 
	#~ return 

	
if __name__ == '__main__':
	w = Tk()
	f = fuFrame(w, borderwidth=4)
	v=fuEntry(f, 12.34, DoubleVar)
	print v.get(), type(v.get())
	fuEntry(f, 1)
	fuLabelFrame(w, 'Label')
	def pp(a = None): print 'Something pressed', a, type(a)
	fuButton(w, pp, 'Press me')
	fuCheckbutton(w, 'Check me', pp)
	fuText(w)
	fuOptionMenu(w, ('red', 'green', 'yellow'), pp)
	fuOptionMenu(w, (1, 2, 3), pp)
	fuLabel(w, 'Label')
	
	ml = fuMutableLabel(w, 'MLabel', StringVar)
	ml.set(12)
	print type (ml.get())
	
	print '********************'
	w2 = Toplevel()
	fuButton(w2, pp)
	createStatusBar(w2)
	
	w.mainloop()

# Memo
# packArgs.setdefault('side', TOP)
# args.setdefault('indicatoron', True)
# wnd.geometry('%dx%d+%d+%d'%(w,h,x,y))	
# root.protocol("WM_DELETE_WINDOW", mainWindow.destroy)
# root.protocol("WM_TAKE_FOCUS", pp)
# root.iconify()
# colors 
#	RRGGBB tk_rgb = "#%02x%02x%02x" % (128, 192, 200)
#	Red, Green, Blue, ....   for Windows SystemActiveBorder, SystemActiveCaption, 
# font
# args.setdefault('font', 'Courier 10')
#	("Helvetica", 10, "bold italic"), ("Symbol", 8)			
#	tkFont.Font(family="Times", size=10, weight=tkFont.BOLD)	more options - slant, underline, overstrike 
#	system fonts
