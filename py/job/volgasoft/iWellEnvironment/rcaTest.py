#!/usr/bin/python

from iWellEnvironment import *
import os
import random
import struct

class RCA_HMI(iWellOrdinaryApplication):
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)
				
		self.zmqInit(requestEndPoint='ipc:///tmp/IWELL_RCA')
		
		frame0 = fuFrame(self.root)
		self.id = fuEntry(frame0, 1, IntVar, packArgs={'side':LEFT})
		fuButton(frame0, self.getCalculationList, 'Get list', packArgs={'side':RIGHT})
		fuButton(frame0, self.deleteCalculation, 'Delete', packArgs={'side':RIGHT})
		fuButton(frame0, self.getCalculation, 'Get', packArgs={'side':RIGHT})
		fuButton(frame0, self.createCalculation, 'Create', packArgs={'side':RIGHT})
		
		frame1 = fuLabelFrame(self.root, 'Calculation')		
		INPUTS = ('analog', 'accumulator', 'register', 'slave', 'constant', 'calc_result')
		OPERATIONS = ('plus', 'minus', 'multiply', 'divide')
		fuOptionMenu(frame1, INPUTS, lambda st: self.inputChanged(0, st), packArgs={'side':LEFT, 'padx':20})
		self.operation = fuOptionMenu(frame1, OPERATIONS, packArgs={'side':LEFT, 'padx':20})
		fuOptionMenu(frame1, INPUTS, lambda st: self.inputChanged(1, st), packArgs={'side':LEFT, 'padx':20})
		frame2 = fuFrame(self.root)	
		self.inputFrame = (fuLabelFrame(frame2, 'Input1', packArgs={'side':LEFT}), fuLabelFrame(frame2, 'Input2', packArgs={'side':RIGHT}))
		self.input = [None, None]
		self.inputParameters = ({}, {})
		self.inputChanged(0, INPUTS[0]); self.inputChanged(1, INPUTS[0])	# initialization
		
	def createCalculation(self):
		req = self.blankRequest('data-commit', 'create_register_calculation')
		req.setParam(calculation_id = self.id.get(), arithmetic_operator = self.operation.get(), input1_register_category = self.input[0], input2_register_category = self.input[1])
		for k, v in self.inputParameters[0].iteritems():
			req.param()['input1_' + k] = v.get()
		for k, v in self.inputParameters[1].iteritems():
			req.param()['input2_' + k] = v.get()
		#print objToPrettyJSON(req)
		self.sendRequest(req)
		
	def getCalculation(self):
		req = self.blankRequest('data-request', 'get_register_calculation')
		req.setParam(calculation_id = self.id.get())
		self.sendRequest(req)
		
	def getCalculationList(self):
		req = self.blankRequest('data-request', 'list_of_register_calculations')
		self.sendRequest(req)
		
	def deleteCalculation(self):
		req = self.blankRequest('data-delete', 'delete_register_calculation')
		req.setParam(calculation_id = self.id.get())
		self.sendRequest(req)
		
	
	def inputChanged(self, inpNum, inputType):
		if self.input[inpNum] == inputType:
			return
		self.input[inpNum] = inputType
		
		frame = self.inputFrame[inpNum]
		map(lambda c: c.destroy(), frame.winfo_children())
		parameters = self.inputParameters[inpNum]
		parameters.clear()
		
		def createParameterWidgets(parDict):
			row = 0
			for l, v in parDict.iteritems():
				Label(frame, text = l).grid(row = row, column = 0, padx = 3, pady = 2)
				Entry(frame, textvariable = v).grid(row = row, column = 1, padx = 3, pady = 2)
				row += 1
			
		if inputType == 'analog' or inputType == 'accumulator':
			parameters.update({'interpretation_uuid':StringVar(value='uuid%d'%inpNum), 'interpretation_data_to_log':StringVar(value='scaled')})
		elif inputType == 'register':
			parameters.update({'register_number':IntVar(value=33333), 'register_type':StringVar(value='long')})
		elif inputType == 'slave':
			parameters.update({'slave_device_number':IntVar(value=7), 'slave_register_type':StringVar(value='word'), 'slave_register_index':IntVar(value=111)})
		elif inputType == 'constant':
			parameters.update({'constant_value':DoubleVar(value=12.34)})
		elif inputType == 'calc_result':
			parameters.update({'calculated_equation_number':IntVar(value=2)})
		
		createParameterWidgets(parameters)
	
	def replyHandler(self, rep):
		rep = Reply(rep)
		iWellOrdinaryApplication.replyHandler(self, rep)
		if rep.valueIs('get_register_calculation'):
			print objToPrettyJSON(rep.answer())
		if rep.valueIs('list_of_register_calculations'):
			self.info.set(rep.answer())
			

class DAL(iWellOrdinaryApplication):
	Dir = 'rcaDalStore'
	
	@staticmethod
	def fileName(f):
		return os.path.join(DAL.Dir, '%s'%f)
	@staticmethod
	def calculationFileName(_id):
		return DAL.fileName('calculation-%03d.json'%_id)
	@staticmethod
	def configurationFileName():
		return DAL.fileName('application_configuration.json')
	@staticmethod
	def listCalculationFiles():
		return filter(lambda s: s.find('calculation-')==0 and not s.endswith('~'), os.listdir(DAL.Dir))
	@staticmethod
	def listEmcObjects():
		return DAL.findInAllCalculations('analog') + DAL.findInAllCalculations('accumulator')
	@staticmethod
	def listModbusMasterObjects():
		return DAL.findInAllCalculations('slave')
	@staticmethod	
	def listScadaObjects():
		return DAL.findInAllCalculations('register')
	@staticmethod
	def findInAllCalculations(reqCategory):
		ret = []
		for f in DAL.listCalculationFiles():
			with open(DAL.fileName(f)) as opF:
				calc = json.loads(opF.read())
				for i in (1, 2):
					category = calc['input%d_register_category'%i]
					if category == reqCategory == 'register':
						regNum = calc['input%d_register_number'%i]
						regType = calc['input%d_register_type'%i]
						ret.append((regNum, regType))
					elif category == reqCategory == 'slave':
						slave = calc['input%d_slave_device_number'%i]
						register = calc['input%d_slave_register_index'%i]
						type_ = calc['input%d_slave_register_type'%i]
						ret.append((slave, register, type_))
					elif category == reqCategory and (category == 'analog' or category == 'accumulator'):
						uuid = calc['input%d_interpretation_uuid'%i]
						ret.append(str(uuid))
		#print '__debug', reqCategory, ret
		return ret
	@staticmethod
	def calculationToStr(_id):
		try:
			with open(DAL.calculationFileName(_id)) as opF:
				return calculationJsonToStr(json.loads(opF.read()))
		except IOError:
			return ''
	@staticmethod		
	def printAllCalculations():
		print '******* Calculation list *******\n'
		for f in DAL.listCalculationFiles():
			with open(DAL.fileName(f)) as opF:
				#print opF.read()
				print calculationJsonToStr(json.loads(opF.read()))				
		print '\n*******************************\n'
					
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)
		self.root.iconify()
		self.zmqInit(listenerEndPoint='ipc:///tmp/IWELL_DAL')
		
		if not os.access(DAL.Dir, os.F_OK):
			os.mkdir(DAL.Dir)	
			
	def requestHandler(self, req):	# special DAL way
		req = DalRequest(req)		
		handlerName = 'requestHandler_'+req.method().replace('-', '_') + '_' + req.param()['location']['document_type']
		try:
			self.__class__.__dict__[handlerName](self, req)
		except KeyError, e:
			if e.message == handlerName:
				print 'ERROR! %s has not %s\n'%(self.name, handlerName), req
				self.sendReply(req.formReply('Error'))	# we should send something
			else:
				raise
	
	def requestHandler_data_insert_rca_calculations(self, req):
		_id = req.param()['name']['id']
		with open(DAL.calculationFileName(_id), 'w') as _file:
			_file.write(objToPrettyJSON(req.param()['content']))
		self.sendReply(req.formOkStatusReply())
		
	def requestHandler_data_delete_rca_calculations(self, req):
		_id = req.param()['name']['id']
		try: os.remove(DAL.calculationFileName(_id))
		except OSError: self.sendReply(req.formErrorStatusReply('error', 'no file'))
		else: self.sendReply(req.formOkStatusReply())
		
	def requestHandler_data_request_rca_calculations(self, req):
		answer = []
		for f in DAL.listCalculationFiles():
			with open(DAL.fileName(f)) as _f:
				answer.append(
					{	'location': {'document_type': 'rca_calculations', 'document_subtype': ''},
						'name': {'type': 'register_calculation', 'id': int(f[12:15])},
						'content': self.decode(_f.read())
					})			
		self.sendReply(req.formValuesReply(answer))
			
	def requestHandler_data_request_rca_settings(self, req):
		with open(DAL.configurationFileName()) as _file:
			answer = [
					{	'location': {'document_type': 'rca_settings', 'document_subtype': ''},
						'name': {'type': 'rca_settings'},
						'content': json.loads(_file.read())
					}]
		self.sendReply(req.formValuesReply(answer))
			
class EMC(iWellOrdinaryApplication):
	class Object:
		# StringVar value; BooleanVar enabled
		pass
		
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)
		setWindowSize(self.root, 250, 150)
		
		self.zmqInit(publicationEndPoint='ipc:///tmp/iWellEnvironmentProxy', listenerEndPoint='ipc:///tmp/IWELL_EMC')	# 
		
		self.objects = {}
		# settings		
						
	def requestHandler(self, req):	# special EMC way
		req = EmcRequest(req)		
		handlerName = 'requestHandler_'+req.method().replace('-', '_')+'_'+req.value()['parameter'] 
		try:
			self.__class__.__dict__[handlerName](self, req)
		except KeyError, e:
			if e.message == handlerName:
				print 'ERROR! %s has not %s\n'%(self.name, handlerName), req
				self.sendReply(req.formReply('Error'))	# we should send something
			else:
				raise
		
	def requestHandler_data_request_Status(self, req):
		name = req.value()['ValueID']
		self.addObject(name, random.randint(0, 1))
		
		emcObj = self.objects[name]
		if (emcObj.enabled.get()):			
			answer = [{'ValueID' : name, 'explanation' : 'VALUE FOUND', 'value' : {'Scaled_value' : emcObj.value.get()}}]
		else: 
			answer = [{'ValueID' : name, 'explanation' : 'Port Not Enabled'}]
		self.sendReply(req.formReply(answer))
			
	def addObject(self, name, enabled):
		if self.objects.has_key(name):
			return 
		newObj = EMC.Object()
		frame = fuFrame(self.root, packArgs = {'pady' : 1})
		fuLabel(frame, name, packArgs = {'side' : LEFT, 'pady' : 0})
		newObj.enabled = fuCheckbutton(frame, defaultValue = enabled, packArgs = {'side' : RIGHT, 'pady' : 0})
		newObj.value = fuEntry(frame, defaultValue = round(random.random()*100, 2), ValueType = DoubleVar, callback = (self.someValueChanged, name), width = 8, packArgs = {'side' : RIGHT, 'pady' : 0})	
		
		self.objects[name] = newObj
	
	def addObjects(self, objLst):
		for o in objLst:
			self.addObject(o, random.randint(0, 1))
		
	def someValueChanged(self, name, entryVar):
		try:
			value = entryVar.get()
		except ValueError:
			return
		#print '_debug', name, value
		pbl = self.blankPublication(name)
		pbl[2]['value'] = [['smth', value, 'units']]
		pbl[2]['name'] = name
		self.sendPublication(pbl)
				
	def publishAll(self):
		for k, v in self.objects.iteritems():
			self.someValueChanged(k, v.value)
		
class ModbusMaster(iWellOrdinaryApplication):
	
	assert struct.calcsize('f') == struct.calcsize('I') == 4 and struct.calcsize('H') == 2
	def helpCalculation(self, regToValue):
		if self.calcFlagUserDidntChange: return
		self.calcFlagUserDidntChange = True
		if regToValue:
			self.calcValue.set('')
			try:
				if self.calcType.get() == 'float':
					self.calcValue.set(struct.unpack('f', struct.pack('HH', int(self.calcReg[1].get()), int(self.calcReg[0].get()))))
				elif self.calcType.get() == 'long':
					self.calcValue.set(struct.unpack('I', struct.pack('HH', int(self.calcReg[1].get()), int(self.calcReg[0].get()))))
			except (ValueError, struct.error):
				print 'Helper calculation error for "%s", "%s"'%(self.calcReg[0].get(), self.calcReg[1].get())
		else:
			map(lambda v: v.set(''), self.calcReg)
			try:
				if self.calcType.get() == 'float':
					reg = struct.unpack('HH', struct.pack('f', float(self.calcValue.get())))
				elif self.calcType.get() == 'long':
					reg = struct.unpack('HH', struct.pack('I', int(self.calcValue.get())))
				self.calcReg[1].set(reg[0])	
				self.calcReg[0].set(reg[1])
			except (ValueError, struct.error):
				print 'Helper calculation error for "%s"'%(self.calcValue.get(),)
							
		self.calcFlagUserDidntChange = False
			
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)		
		setWindowSize(self.root, 300, 300)
	
		self.zmqInit(publicationEndPoint='ipc:///tmp/iWellEnvironmentProxy')
		
		self.slaves = {}
		
		# settings
		self.CreateRandomSlaves = False
		
		# helper frame
		calcFrame = fuLabelFrame(self.root, 'Helper'); cf1 = fuFrame(calcFrame, relief = FLAT); cf2 = fuFrame(calcFrame, relief = FLAT); cf3 = fuFrame(calcFrame, relief = FLAT); 
		self.calcReg = [fuEntry(cf1, 0, width = 10, packArgs={'side':LEFT, 'padx': 5, 'pady':1}) for i in xrange(2)]
		self.calcType = fuOptionMenu(cf2, ('float', 'long'), packArgs={'side':LEFT, 'padx':3, 'pady':1})
		fuButton(cf2, lambda: self.helpCalculation(False), 'To reg', width = 8, packArgs={'side':RIGHT, 'padx':3, 'pady':1})
		fuButton(cf2, lambda: self.helpCalculation(True), 'From reg', width = 8,  packArgs={'side':RIGHT, 'padx':3, 'pady':1})
		self.calcValue = fuEntry(cf3, 0, packArgs={'side':LEFT, 'pady':1, 'fill':X, 'expand':YES})
		self.calcFlagUserDidntChange = False
		
		slaveControlFrame = fuFrame(self.root)
		entryNewSlave = fuEntry(slaveControlFrame, 1, IntVar, packArgs={'side':LEFT})
		fuButton(slaveControlFrame, lambda:self.addSlave(entryNewSlave.get()), 'Add slave', packArgs={'side':RIGHT})
				
	def addSlave(self, slaveNum):
		if self.slaves.has_key(slaveNum):
			return
		newSlaveFrame = fuLabelFrame(self.root, 'Slave%d'%slaveNum, packArgs={'side':LEFT})
		registerControlFrame = fuFrame(newSlaveFrame, relief = FLAT, packArgs = {'side':TOP})
		entryNewRegister = fuEntry(registerControlFrame, 0, IntVar, width = 8, packArgs={'side':LEFT, 'padx':1})
		fuButton(registerControlFrame, lambda:self.addRegister(slaveNum, entryNewRegister.get()), 'Add', width = 5, packArgs={'side':RIGHT, 'padx':1})
		
		self.slaves[slaveNum] = [{}, newSlaveFrame]		# 0 - dict with numReg:entryValue, 1 - frame
		
		if self.CreateRandomSlaves:
			regQuantity = random.randrange(2,5)	#2..4
			for i in xrange(regQuantity):
				self.addRegister(slaveNum, random.randint(0, 49999))
		
		
	def addRegister(self, slaveNum, registerNum):
		regDict = self.slaves[slaveNum][0]
		frame = self.slaves[slaveNum][1]
		
		if regDict.has_key(registerNum):
			return
		newRegisterFrame = fuFrame(frame, relief = FLAT)
		fuLabel(newRegisterFrame, str(registerNum), packArgs={'side':LEFT, 'pady':0, 'padx':1})				
		regDict[registerNum] = fuEntry(newRegisterFrame, defaultValue = random.randint(0, 2**16-1), ValueType = IntVar, callback = (self.someValueChanged, (slaveNum, registerNum)), width = 8, packArgs = {'side' : RIGHT, 'pady':0, 'padx':1})
					
	def someValueChanged(self, tpl, entryVar):
		slaveNum = tpl[0]
		#registerNum = tpl[1]
		#value = entryVar.get()
		#print 'Slave %d register %d changed to %d'%(slaveNum, registerNum, value)
		values = []
		for n, e in self.slaves[slaveNum][0].iteritems():
			#print '  reg %8d %8d'%(n, e.get())
			try:
				values.append([n, e.get()])
			except ValueError:
				return
		title = 'Modbus_Master-Slave%d'%slaveNum
		pbl = self.blankPublication(title)
		pbl[2]['value'] = values
		pbl[2]['name'] = title
		self.sendPublication(pbl)
		
	def publishAll(self):
		for k, v in self.slaves.iteritems():
			self.someValueChanged((k, 'Dummy register'), 'Dummy entryVar')
	
	def addObject(self, obj):
		slave = obj[0]
		register = obj[1]
		type_ = obj[2]
		self.addSlave(slave)
		self.addRegister(slave, register)
		if (type_ == 'float' or type_ == 'long'):
			self.addRegister(slave, register + 1)
		
	def addObjects(self, objLst):
		for o in objLst:
			self.addObject(o)
						
			
class Scada(iWellOrdinaryApplication):
	@staticmethod
	def regName(regNum, regType):
		return '%d_%s'%(regNum, regType)
		
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)
		setWindowSize(self.root, 250, 150)
		
		self.zmqInit(publicationEndPoint='ipc:///tmp/iWellEnvironmentProxy', listenerEndPoint='ipc:///tmp/IWELL_SCADA')	# 
		
		self.objects = {}	# name : entryStringVar
		# settings
		self.StatusOkAlways = True
				
	def requestHandler_update_publish_list(self, req):
		reg = req.param()['registers'][0]
		regNum = reg[0]
		regType = reg[1]
		name = Scada.regName(regNum, regType)
		if (self.StatusOkAlways):
			self.addObject(regNum, regType)
			answer = [[regNum, regType, self.getValue(name, regType)]]
			self.sendReply(req.formReply(answer, 'OK'))
		else: 
			raise RuntimeError
			
	def requestHandler_delete_publish_list(self, req):
		reg = req.param()['registers'][0]
		regNum = reg[0]
		regType = reg[1]
		name = Scada.regName(regNum, regType)
		self.objects[name].set('')	# mark
		del self.objects[name]; self.sendReply(req.formReplyOk())
		#self.sendReply(req.formReplyError('Test error'))
		
	def getValue(self, name, regType):
		strValue = self.objects[name].get()
		if regType == 'float':
			return float(strValue)
		return int(strValue)			
			
	def addObject(self, regNum, regType):
		name = Scada.regName(regNum, regType)
		if self.objects.has_key(name):
			return
		frame = fuFrame(self.root, packArgs = {'pady' : 1})
		fuLabel(frame, name, packArgs = {'side' : LEFT, 'pady' : 0})
		if regType == 'float':
			defaultValue = round(random.random()*1000, 2)
		else:
			defaultValue = random.randint(0, 1000)
		self.objects[name] = fuEntry(frame, defaultValue = defaultValue, callback = (self.someValueChanged, (name, regType)), packArgs = {'side' : RIGHT, 'pady' : 0})	
	
	def addObjects(self, objLst):
		for n, t in objLst:
			self.addObject(n, t)
		
	def someValueChanged(self, tpl, entryVar):
		name, regType = tpl
		try:
			value = self.getValue(name, regType)
		except ValueError:
			return
		#print '_debug', name, value
		title = 'Register_' + name
		pbl = self.blankPublication(title)
		pbl[2]['value'] = [['smth', value, 'units']]
		pbl[2]['name'] = title
		self.sendPublication(pbl)
		
	def publishAll(self):
		for k, v in self.objects.iteritems():	# name : entryStringVar
			self.someValueChanged((k, k.split('_')[1]), v)
		
class Observer(iWellOrdinaryApplication):
	class Object:
		def indicate(self):
			states = ('-', '|', '+', '*')
			for i in xrange(-1, len(states) - 1):
				if self.indicator.get() == states[i]:
					self.indicator.set(states[i+1])
					break
			else:
				self.indicator.set(states[0])
					
	def __init__(self, mainWindow):
		iWellApplication.__init__(self, mainWindow)
		setWindowSize(self.root, 800, 150)
		
		self.zmqInit(subscriptionEndPoint="ipc:///tmp/publisher")
		
		self.calculations = {}	# number : Observer.Object (see addObject)
	
	def subscriptionHandler(self, msg):
		if msg[0][1].startswith('register_calculation-'):
			self.calculationHandler(msg)
	
	def calculationHandler(self, msg):
		msgType = msg[0][0]
		calcNum = int(msg[0][1].split('register_calculation-')[1])
		self.addObject(calcNum)
		obj = self.calculations[calcNum]
		obj.calc.set(DAL.calculationToStr(calcNum))
		obj.indicate()
		if msgType == 'data':
			obj.result.set(str(msg[2]['value']))
			obj.error.set('')
		elif msgType == 'exception':
			obj.result.set('')
			if msg[2].has_key('disabled_interpretations'):
				obj.error.set(msg[2]['disabled_interpretations'])
			else:
				obj.error.set(msg[2]['reason'])			
		
	def addObject(self, calcNum):
		if self.calculations.has_key(calcNum):
			return
		newObject = Observer.Object()
		frame = fuFrame(self.root, packArgs = {'pady':0})
		newObject.indicator = fuMutableLabel(frame, anchor = W, font = ("Courier", 12, "bold"), packArgs = {'side':LEFT, 'padx':2, 'pady':0})
		fuLabel(frame, calcNum, anchor = W, packArgs = {'side':LEFT, 'pady':0})
		newObject.calc = fuMutableLabel(frame, anchor = W, packArgs = {'side':LEFT, 'expand':YES,'fill':X, 'padx':2, 'pady':0})
		newObject.result = fuMutableLabel(frame, anchor = E, packArgs = {'side':RIGHT, 'expand':YES,'fill':X, 'padx':2, 'pady':0})	
		newObject.error = fuMutableLabel(frame, anchor = E, fg = 'red', packArgs = {'side':RIGHT, 'expand':YES, 'fill':X, 'padx':2, 'pady':0})	
		self.calculations[calcNum] = newObject

def calculationJsonToStr(jCalc):
	def __input(i):
		cat = jCalc['input%d_register_category'%i]
		if cat == 'analog' or cat == 'accumulator':
			ret = '%s[%s %s]'%(cat, jCalc['input%d_interpretation_uuid'%i], jCalc['input%d_interpretation_data_to_log'%i])
		elif cat == 'register':
			ret = '%s[%d %s]'%(cat, jCalc['input%d_register_number'%i], jCalc['input%d_register_type'%i])
		elif cat == 'slave':
			ret = '%s[%d %d %s]'%(cat, jCalc['input%d_slave_device_number'%i], jCalc['input%d_slave_register_index'%i], jCalc['input%d_slave_register_type'%i])	
		elif cat == 'constant':
			ret = str(jCalc['input%d_constant_value'%i])
		elif cat == 'calc_result':
			ret = 'calc[%d]'%(jCalc['input%d_calculated_equation_number'%i])
		return ret
			
	__operator = {'plus':' + ', 'minus':' - ', 'multiply':' * ', 'divide':' / '}[jCalc['arithmetic_operator']]
	
	return __input(1) + __operator + __input(2)
			
if __name__ == '__main__':
	wnd = windowApp()
	
	ZmqMultiProxy('ipc:///tmp/iWellEnvironmentProxy', 'ipc:///tmp/Subscriber')
	
	hmi = RCA_HMI(wnd.root)
	placeWindowTo(hmi.root, 1000, 0)
	
	DAL(wnd.root)
	
	emc = EMC(wnd.root)
	emc.addObjects(DAL.listEmcObjects())
	placeWindowTo(emc.root, 1250, 400)
	
	modbusMaster = ModbusMaster(wnd.root)
	modbusMaster.addObjects(DAL.listModbusMasterObjects())
	placeWindowTo(modbusMaster.root, 900, 400)
	
	scada = Scada(wnd.root)
	scada.addObjects(DAL.listScadaObjects())
	placeWindowTo(scada.root, 1600, 400)
	
	observer = Observer(wnd.root)
	placeWindowTo(observer.root, 0, 0)
	
	DAL.printAllCalculations()	
	
	sleep(0.5)
	emc.publishAll()
	sleep(0.5)
	modbusMaster.publishAll()
	sleep(0.5)
	scada.publishAll()
	
	wnd.mainloop()

