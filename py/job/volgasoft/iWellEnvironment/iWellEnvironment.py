#!/usr/bin/python

import zmq
from fuTkinter import *
import thread
import threading
import Queue
from time import sleep, time
import json

def time_():
	return int(time())
	
def toPrettyJSON(st):
	try:
		ret = json.dumps(json.loads(st), indent=4, separators=(',', ' : '), sort_keys=True)
	except ValueError:
		return st
	return ret
	
def objToPrettyJSON(obj):
	return json.dumps(obj, indent=4, separators=(',', ' : '), sort_keys=True)
	
class JSONCoder:
	errorResult = ''
	def errorOccured(self, smth):
		print 'Error of JSONCoder for', smth 
			
	def code(self, smth):
		return json.dumps(smth, separators=(',', ':'))
	
	def codeSequence(self, smth):
		return map(self.code, smth)
		
	def decode(self, smth):
		if type(smth) is list:
			return self.decodeSequence(smth)
		try:
			ret = json.loads(smth)
		except ValueError:
			ret = JSONCoder.errorResult
			self.errorOccured(smth)
		return ret
	
	def decodeSequence(self, smth):
		return map(self.decode, smth)		

class windowApp():
	def __init__(self):
		self.root = Tk()
		self.root.iconify()
		#self.root.geometry("600x400+100+100")	
		
		self.root.bind_all('<Escape>', lambda ev: self.root.quit())	
		
		frame1 = fuLabelFrame(self.root, 'Request')
		self.zmqReqAddr = fuEntry(frame1, 'ipc:///tmp/IWELL_RCA', packArgs={'fill':X})
		self.reqText = fuText(frame1)
		try:
			self.reqText.insert('1.0', toPrettyJSON(open('debugReq.txt').read()))	
		except IOError:
			pass
		self.repText = fuText(frame1)
		fuButton(frame1, self.sendZmqReq, 'Send request')
		
		frame2 = fuLabelFrame(self.root, 'Publication')
		self.publEntries = [fuEntry(frame2, packArgs={'fill':X}) for i in xrange(3)]
		try:
			for i, s in enumerate(open('debugPub.txt').readlines()):
				self.publEntries[i].set(s.strip())
		except IOError:
			pass
		fuButton(frame2, lambda: self.publSocket.send_multipart(map(lambda e: e.get(), self.publEntries)), 'Send publication')
		
		self.zmqContext = zmq.Context()
		self.publSocket = self.zmqContext.socket(zmq.PUB)
		self.publSocket.connect('ipc:///tmp/iWellEnvironmentProxy')
		
	def sendZmqReq(self):
		self.repText.delete('1.0', END)
		socket = self.zmqContext.socket(zmq.REQ)
		socket.connect(self.zmqReqAddr.get())
		socket.send(str(self.reqText.get(1.0, END)))
		self.repText.insert('1.0', toPrettyJSON(socket.recv()))
		socket.close()
		
	def mainloop(self):
		self.root.mainloop()
	
class ZmqMultiProxy:
	def __init__(self, proxySubEndPoint, proxyPubEndPoint):
		context = zmq.Context()
		
		socketSub = context.socket(zmq.SUB)
		socketSub.bind(proxySubEndPoint)
		socketSub.setsockopt(zmq.SUBSCRIBE, '')

		socketPub = context.socket(zmq.PUB)
		socketPub.bind(proxyPubEndPoint)
		
		self.work = True
		def __work():
			while self.work:
				msg = socketSub.recv_multipart()
				socketPub.send_multipart(msg)
		thread.start_new_thread(__work, ())
	
	def stop(self):
		self.work = False
	
	
class iWellApplication(JSONCoder):
	
	def __init__(self, mainWindow):
		self.name = self.__class__.__name__
		self.root = Toplevel(mainWindow)
		self.root.title(self.name)
		self.root.protocol("WM_DELETE_WINDOW", mainWindow.destroy)
		
		self.info = createStatusBar(self.root)
		
		self.cntReq = 0
		
	# maybe to do dictionaries of endpoints	
	def zmqInit(self, listenerEndPoint=None, requestEndPoint=None, publicationEndPoint=None, proxy=True, subscriptionEndPoint=None, subscriptionFilter=''):
		zmqContext = zmq.Context()
				
		if listenerEndPoint is not None:
			self.listenerRequestQueue = Queue.Queue()
			self.listenerReplyQueue = Queue.Queue()
			def listener():
				listenerSocket = zmqContext.socket(zmq.REP)
				listenerSocket.bind(listenerEndPoint)
				while 1:
					self.listenerRequestQueue.put(listenerSocket.recv())
					listenerSocket.send(self.listenerReplyQueue.get())
			self.queueGetter(self.requestHandler, self.listenerRequestQueue)
			thread.start_new_thread(listener, ())
			
		if subscriptionEndPoint is not None:
			self.subscriptionQueue = Queue.Queue()
			def subscription():
				subscriptionSocket = zmqContext.socket(zmq.SUB)
				subscriptionSocket.connect(subscriptionEndPoint)
				subscriptionSocket.setsockopt(zmq.SUBSCRIBE, subscriptionFilter)
				while 1:
					self.subscriptionQueue.put(subscriptionSocket.recv_multipart())
			self.queueGetter(self.subscriptionHandler, self.subscriptionQueue)
			thread.start_new_thread(subscription, ())
		
		if requestEndPoint is not None:
			self.requestSocket = zmqContext.socket(zmq.REQ)
			self.requestSocket.connect(requestEndPoint)
			self.requestLock = threading.Lock()
			self.requestReplyQueue = Queue.Queue()
			self.queueGetter(self.replyHandler, self.requestReplyQueue)
			
		if publicationEndPoint is not None:
			self.publicationSocket = zmqContext.socket(zmq.PUB)
			if proxy:
				self.publicationSocket.connect(publicationEndPoint)
			else:
				self.publicationSocket.bind(publicationEndPoint)
	
			
	def queueGetter(self, callback, queue):
		#print '__debug %s queueGetter with %s'%(self.name, callback.__name__)
		try:
			while 1:
				msg = queue.get_nowait()
				callback(self.decode(msg))				
		except Queue.Empty:
			self.root.after(200, self.queueGetter, callback, queue)
					
	def sendRequest(self, req):
		if self.requestLock.acquire(False) == False:
			return False
		self.requestSocket.send(self.code(req))
		def replier():
			reply = self.requestSocket.recv()
			self.requestLock.release()
			self.requestReplyQueue.put(reply)
		thread.start_new_thread(replier, ())
		return True
		
	def sendReply(self, rep):
		self.listenerReplyQueue.put(self.code(rep))
		
	def sendPublication(self, pub):
		self.publicationSocket.send_multipart(self.codeSequence(pub))
		
	# override
	def requestHandler(self, req):
		print '%s request:\n'%self.name, req
		self.sendReply('Reply dummy')
	
	# override	
	def replyHandler(self, rep):
		print '%s reply:\n'%self.name, rep
		
	# override
	def subscriptionHandler(self, msg):
		print '%s subcription message:\n'%self.name, msg

	def blankRequest(self, method, value):
		self.cntReq += 1
		return SentRequest('%s%d'%(self.name, self.cntReq), method, value)	
		
	def blankPublication(self, title, mode = 'data'):
		return [
			[mode, title],
			{'origin_app':self.name, 'destination_app':'third_party_2'},
			{'ts':time_(), 'str_ts':'str_ts'}
				]
		
OKSign = 'Ok'
ErrorSign = 'Unsuccessful'

class Reply(dict):
	def __init__(self, dd):
		dict.__init__(self, dd)
			
	def valueIs(self, value):
		return self.value() == value
		
	def value(self):
		return self['values'][0][0]
	
	def answer(self):	
		return self['values'][0][1]
		
	def error(self):
		return self['values'][0][2]

	def isOk(self):
		return self['values'][0][1] == OKSign
	def isError(self):
		return self['values'][0][1] == ErrorSign
		
class Request(dict):
	def __init__(self, dd):
		dict.__init__(self, dd)
		
	def param(self):
		return self['params'][0]
	
	def value(self):
		return self['values'][0][0]
	
	def valueIs(self, value):
		return self.value() == value
		
	def formReplyOk(self):
		return self.formReply(OKSign)
		
	def formReplyError(self, errorText = ''):
		return self.formReply(ErrorSign, errorText)
	
	def formReply(self, answer, answer2 = ''):	
		return {'uniqueReqID' : self['uniqueReqID'], 'ts' : time_(), 'values' : [[self.value(), answer, answer2]]}
		
	def method(self):
		return self['method']
		
class EmcRequest(Request):
	def __init__(self, dd):
		Request.__init__(self, dd)
		
	def formReply(self, answer):	
		return {'method' : 'data-reply', 'values' : [[self.value()['parameter'], answer, '']]}
	
	def formReplyOk(self):
		raise NotImplementedError		
	def formReplyError(self, errorText = ''):
		raise NotImplementedError
		
class DalRequest(dict):
	def __init__(self, dd):
		dict.__init__(self, dd)
		
	def param(self): return self['params'][0]	
	def method(self): return self['method']	
		
	def formBaseReply(self):	
		return {'uniqueReqID' : self['uniqueReqID'], 'ts' : time_(), 'application_name': self['application_name'], 'method': self['method']}
	def formStatusReply(self, answer):	
		return dict(self.formBaseReply(), **{'location': self['params'][0]['location'], 'name': self['params'][0]['name'], 'status': answer})
	def formOkStatusReply(self):
		return self.formStatusReply('Ok')
	def formErrorStatusReply(self, error, reason):
		return self.formStatusReply({'error' : error, 'reason' : reason})
	def formValuesReply(self, values):
		return dict(self.formBaseReply(), **{'values' : values, 'status': 'Ok'})
				
		
class SentRequest(Request):
	def __init__(self, uniqueReqID, method, value):	
		dict.__init__(self, uniqueReqID = uniqueReqID, ts = time_(), method = method, values = [[value]], params = [{}] )
	
	def setParam(self, **kw):
		self['params'][0].update(kw)
		
# This class realized some common approaches (status bar, request handlers)
class iWellOrdinaryApplication(iWellApplication):
	def sendRequest(self, req):
		if iWellApplication.sendRequest(self, req):
			self.info.set('Send request')
	
	def replyHandler(self, rep):
		rep = Reply(rep)
		if rep.isError():
			self.info.set('Error of %s. %s.'%(rep.value(), rep.error()))
		else:
			self.info.set('Ok')
		
	def requestHandler(self, req):
		req = Request(req)	
		handlerName = 'requestHandler_'+req.value()
		try:
			self.__class__.__dict__[handlerName](self, req)
		except KeyError, e:
			if e.message == handlerName:
				print 'ERROR! %s has not %s\n'%(self.name, handlerName), req
				self.sendReply(req.formReplyError('Unknown request'))
			else:
				raise
		
if __name__ == '__main__':
	wnd = windowApp()
	wnd.mainloop()	
