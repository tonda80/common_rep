# coding=utf8

import zmq				# pip install zmq
import sys
import json
import os
import time

curr_dir = os.path.realpath(os.path.dirname(__file__))
pb_dir = os.path.realpath(os.path.join(curr_dir, *'../../../python/pb'.split('/')))
sys.path.append(pb_dir)
import mntproto_pb2		# pip install protobuf


class Global:
	flag_work = True
	zmq_context = zmq.Context()


class ZmqChecker:	# TODO
	def __init__(self, filter_, connect = True, sub_endpoint = 'tcp://127.0.0.1:10125'):
		self.sub_socket = Global.zmq_context.socket(zmq.SUB)
		if connect:
			self.sub_socket.connect(sub_endpoint)
		else:
			self.sub_socket.bind(sub_endpoint)
		self.sub_socket.setsockopt(zmq.SUBSCRIBE, filter_)

		#threading.Thread(target=self.receivedThread).start()

	def recv_loop(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while Global.flag_work:
			print '__deb', 0
			socks = dict(poller.poll(1*1000))
			if self.sub_socket in socks:
				print '__deb', 1
				msg = self.sub_socket.recv_multipart()
				print '__deb', msg

	def recv_loop2(self):
		while True:
			msgs = self.sub_socket.recv_multipart()
			print '__deb', msgs

ZmqChecker('').recv_loop()
