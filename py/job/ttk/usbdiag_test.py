#!/usr/bin/env python
# coding=utf8

# тестируем usbdiag, более универсальное развитие в py3/.../mnt2_test.py

import zmq				# pip install zmq
import sys
import json
import os
import time
import threading
import traceback

#curr_dir = os.path.realpath(os.path.dirname(__file__))
#os.path.realpath(os.path.join(curr_dir, *'../../../python/pb'.split('/')))
pb_dir = '/home/anton/rep-s/mnt2/python/pb'
sys.path.append(pb_dir)
import mntproto_pb2		# pip install protobuf
import usbdiag_pb2
import canparser_pb2
import board_pb2

from baseapp import BaseConsoleApp

usbdiag = 'USBDIAG'			# router client name
CanItemName = 'USBCHRG'		# UCRGTST USBCHRG

class AppError(RuntimeError):
	pass

def app_assert(cond, message):
	if not cond:
		raise AssertionError(message)


def get_object_from_any(any_object, ProtoClass):
	app_assert(any_object.Is(ProtoClass.DESCRIPTOR), 'wrong {} object into Any'.format(ProtoClass.__name__))
	ret = ProtoClass()
	any_object.Unpack(ret)
	return ret


class G:
	flag_work = True
	zmq_context = zmq.Context()
	log = None
	router_mode = False		# если True то связываемся через роутер

class ZmqSender:
	def __init__(self, name, pub_endpoint, use_connect):
		self.pub_socket = G.zmq_context.socket(zmq.PUB)
		if use_connect:
			self.pub_socket.connect(pub_endpoint)
		else:
			self.pub_socket.bind(pub_endpoint)
		time.sleep(.2)			# без паузы не работает!

		self.name = name
		self.id = 0

	def initHeader(self, mntHeader):
		mntHeader.version = mntproto_pb2.V1_1
		mntHeader.type = mntproto_pb2.MESSAGE
		mntHeader.source = self.name;
		mntHeader.createTime = int(time.time()*1000)
		mntHeader.id = self.id
		self.id += 1
		mntHeader.replyTo = 0
		mntHeader.isReplyMandatory = False

	def publish(self, dest, name, data = None):
		mntCommand = mntproto_pb2.MntCommand()
		self.initHeader(mntCommand.header)
		mntCommand.header.destination.append(dest)
		mntCommand.header.name = name
		if data:
			mntCommand.data.Pack(data)

		if G.router_mode:
			msgs = ('ROUTER', dest, mntCommand.SerializeToString())
		else:
			msgs = (dest, mntCommand.SerializeToString())

		self.pub_socket.send_multipart(msgs)
		G.log.debug('published {}'.format(msgs))


class ZmqReceiver:
	def __init__(self, recv_handler, sub_endpoint, use_connect, filter_ = ''):
		self.sub_socket = G.zmq_context.socket(zmq.SUB)
		if use_connect:
			self.sub_socket.connect(sub_endpoint)
		else:
			self.sub_socket.bind(sub_endpoint)
		self.sub_socket.setsockopt(zmq.SUBSCRIBE, filter_)

		self.recv_handler = recv_handler
		threading.Thread(target=self.recv_loop).start()

	def recv_loop(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while G.flag_work:
			socks = dict(poller.poll(1*1000))
			if self.sub_socket in socks:
				msgs = self.sub_socket.recv_multipart()
				G.log.debug('received {}'.format(msgs))
				try:
					self.recv_handler(msgs)
				except:
					traceback.print_exc()
					G.log.error('recv_handler issue')


class Cmd(list):
	def __init__(self, str_cmd):
		list.__init__(self, str_cmd.split())
	def get(self, i, default = None):
		return self[i] if len(self) > i else default


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('--router', '-r', action='store_true', help=u'Взаимодействие через роутер')

	def main(self):
		G.log = self.log
		if self.args.router:
			G.router_mode = True
			snd_endpoint = 'tcp://127.0.0.1:10125'
			rcv_endpoint = 'tcp://127.0.0.1:10126'
			self.log.info('App started for communication with router')
		else:
			G.router_mode = False
			snd_endpoint = 'tcp://127.0.0.1:10126'
			rcv_endpoint = 'tcp://127.0.0.1:10125'

		self.sender_name = 'TestRequester'
		self.zsnd = ZmqSender(self.sender_name, snd_endpoint, G.router_mode)
		zrcv = ZmqReceiver(self.recv_handler, rcv_endpoint, G.router_mode)
		self.command_handler_loop()

	def recv_handler(self, msgs):
		if G.router_mode and len(msgs) < 3:
			return	# не знаю что там такое приходит и не интересно
		mntCommand = mntproto_pb2.MntCommand()
		mntCommand.ParseFromString(msgs[2])
		self.log.debug('received {} from {}'.format(mntCommand.header.name, mntCommand.header.source))
		if mntCommand.header.source == usbdiag:
			handler = {
				'GetConfig' : lambda *a: None,
				#'CanEncodeMessage' : self.process_usbdiag_CanEncodeMessage,
				'usbdiagState' : self.process_usbdiag_usbdiagState,
				'CanMessage' : self.process_usbdiag_CanMessage,
			}.get(mntCommand.header.name, lambda cmd: self.log.error('unknown message from usbdiag {}'.format(cmd)))
			handler(mntCommand)

	def process_usbdiag_usbdiagState(self, mntCommand):
		app_assert(mntCommand.data.Is(usbdiag_pb2.usbdiagState.DESCRIPTOR), 'wrong usbdiag_usbdiagControl data')
		state = usbdiag_pb2.usbdiagState()
		mntCommand.data.Unpack(state)
		self.log.info('received usbdiag usbdiagState: {}'.format(str(state).replace('\n', '; ')))

	def process_usbdiag_CanEncodeMessage(self, mntCommand):
		app_assert(mntCommand.data.Is(canparser_pb2.CanOutPacket.DESCRIPTOR), 'wrong usbdiag_CanEncodeMessage data')
		can_out_packet = canparser_pb2.CanOutPacket()
		mntCommand.data.Unpack(can_out_packet)
		app_assert(len(can_out_packet.Items) == 1, 'wrong usbdiag_CanEncodeMessage items len')
		value = can_out_packet.Items[0].Value
		self.log.info('received usbdiag CanEncodeMessage: command {:x}, param {:x}'.format(value & 0xf, value >> 4))

	def process_usbdiag_CanMessage(self, mntCommand):
		can_out_packet = get_object_from_any(mntCommand.data, board_pb2.CANData)
		self.log.info('received usbdiag CanMessage: {}'.format(can_out_packet.data))

	# эмулируем отправку управления usbchargers с UI
	def send_usb_control(self, s_cmd):
		cmd = Cmd(s_cmd)
		ctrl = usbdiag_pb2.usbdiagControl()
		ctrl.command = int(cmd.get(1, 1))
		ctrl.address = int(cmd.get(2, 5))
		self.log.info('send_usb_control [{}]'.format(str(ctrl).replace('\n', '; ')))
		self.zsnd.publish(usbdiag, 'usbdiagControl', ctrl)

	# эмулируем отправку coстояния usbchargers от кан декодера
	def send_usb_state_can_message(self, s_cmd):
		cmd = Cmd(s_cmd)
		raw_state = 0
		addr = int(cmd.get(1, 5))
		fail = int(cmd.get(2, 0)) != 0
		power = float(cmd.get(3, 12.34))
		d_power = int(power/0.1)
		for i in xrange(3):
			voltage = float(cmd.get(4+i, '{0}.{0}{0}{0}'.format(i+1)))
			d_voltage = int(voltage/0.05)
			raw_state |= d_voltage << 8*(3-i)
		raw_state |= addr    << 32 & 0x0f00000000
		raw_state |= fail    << 36 & 0x1000000000
		raw_state |= d_power << 29 & 0x2000000000
		raw_state |= d_power & 0xff
		self.log.info('send_usb_state_can_message {:x}'.format(raw_state))

		can_pck = canparser_pb2.CanPacket()
		can_item = can_pck.Items.add()
		can_item.Name = CanItemName
		can_item.ParsedValue = raw_state

		self.zsnd.publish(usbdiag, 'CanMessage', can_pck)

	def command_handler_loop(self):
		prev_cmd = cmd = None
		while G.flag_work:
			cmd = raw_input('> ') if not cmd else cmd
			if cmd == 't':	# send unknown test messages
				for i in xrange(2): self.zsnd.publish(usbdiag, 'TestRequest%d'%i)
			elif cmd.startswith('c'):
				self.send_usb_control(cmd)
			elif cmd.startswith('s'):
				self.send_usb_state_can_message(cmd)
			#elif cmd == '':
			elif cmd == 'q':
				G.flag_work = False
				self.log.warn('exiting')
			elif cmd == '' and prev_cmd:
				cmd = prev_cmd
				print '>>', cmd
				continue
			else:
				self.log.warn('unknown command: {}'.format(cmd))
			prev_cmd = cmd
			cmd = None
			time.sleep(0.1)		# чтоб не мешался вывод пришедших с приглашением

if __name__ == '__main__':
	try:
		ret = App().main()
	except SystemExit:
		ret = 0
	except:
		G.flag_work = False
		ret = -1
		traceback.print_exc()
	exit(ret)
