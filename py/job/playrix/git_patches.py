#!/usr/bin/python
# coding=utf8

# Проверяем необходимость выкладывания патчей из андроид ветки
# Ищем коммиты в android_branch base_mm которых нет в patch_branch
# Если надо, создаем временную ветку и переносим туда коммиты
# Пример использования: python git_patches.py --android_branch ver/260_android --patch_branch ver/260_patches
# и затем пуш новой ветки (ver/260_patches_android) и пул реквест нужно сделать вручную
# либо же скрипт предложит черрипикать руками


import os
import subprocess

from baseapp import BaseConsoleApp


class AppError(RuntimeError):
	pass


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('--android_branch', required=True)
		parser.add_argument('--patch_branch', required=True)

	# args - аргументы гита, kw - доп. кастомизация
	def run_git(self, *args, **kw):
		cmd = ['git']
		cmd.extend(args)
		cwd = kw.get('cwd')
		if not cwd:
			cwd = self.root_dir
		elif not os.path.isabs(cwd):
			cwd = os.path.join(self.root_dir, cwd)
		self.log.debug(cwd+' $ '+' '.join(cmd))
		try:
			return subprocess.check_output(cmd, cwd=cwd)
		except subprocess.CalledProcessError as e:
			if kw.get('ignore_error'):
				return ''
			raise

	def __init__(self):
		BaseConsoleApp.__init__(self)

		curr = os.path.realpath(os.path.dirname(__file__))
		self.root_dir = os.path.realpath(os.path.join(curr, *('..',)*5))

	def main(self):
		self.run_git('fetch')

		commits = list(self.get_commits_for_transfer())

		if not commits:
			self.log.info('No commits.')
			return 0
		commits.reverse()

		self.run_git('checkout', self.args.patch_branch)
		self.run_git('pull')
		new_branch = 'tmp/' + self.args.patch_branch + '_android'
		self.run_git('branch', '-D', new_branch, ignore_error=True)
		self.run_git('checkout', '-b', new_branch)
		try:
			self.run_git('cherry-pick', '--keep-redundant-commits', *[c.sha for c in commits])
		except subprocess.CalledProcessError:
			self.log.error('cherry-pick error, abort it')
			self.run_git('cherry-pick', '--abort')
			# не получается авто черрипик, надо делать руками
			self.log.error('\nCannot cherry-pick automatically. Try it manually:\n{}\n{}'.format(
				'git checkout {0} && git branch -D {1} && git checkout -b {1}    # to clean the current script work if needed'.format(self.args.patch_branch, new_branch),
				'git cherry-pick {}'.format(' '.join([c.sha for c in commits]))
			))
			return 1

		self.log.info('\nCheck result. Push. Make pull request.')
		return 0

	def get_commits_for_transfer(self):
		# смотрим удаленные ветки
		android_branch = 'origin/'+self.args.android_branch
		patch_branch = 'origin/'+self.args.patch_branch

		# список для определения уже черри-пикнутого в patch_branch
		ident_list = [commit.ident for commit in self.get_commits('^'+android_branch, patch_branch, '--', 'base_mm')]

		for commit in self.get_commits(android_branch, '^'+patch_branch, '--', 'base_mm'):
			if commit.subject.startswith('Merge branch ') or commit.subject.startswith('Merge remote-tracking branch '):
				continue
			if commit.ident in ident_list:
				continue
			self.log.info('{}    will be cherry-picked'.format(commit))
			yield commit

	def get_commits(self, *log_args):
		out = self.run_git(*(Commit.LOG_ARGS + log_args))
		commits = []
		for l in out.splitlines():
			yield Commit(l)

class Commit:
	__delimiter = '__<[{dlmtr}]>__'
	LOG_ARGS = ('log', '--format=%H{d}%ae{d}%at{d}%s'.format(d=__delimiter))

	def __init__(self, log_line):
		items = log_line.split(self.__delimiter)
		self.sha = items[0]
		self.ident = '{} {}'.format(items[1], items[2])		# для идентификации черрипик коммитов. у них отличается sha но одинаковы автор и дата. коммент может отличаться
		self.subject = items[3]

		self.__str__ = lambda: '%s %s %s'%(self.sha, self.ident, self.subject)

if __name__ == '__main__':
	exit(App().main())
