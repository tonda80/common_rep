# coding=utf8
#
# Скрипт пытается проверить что в текущей гит ветке, есть все нужные изменения
#
# https://app.asana.com/0/354325212918017/677120453843944/f
# - Проверять все ли смерджено из текущей ios ветки
# - Проверять есть ли все коммиты из ios сабмодулей текущей ветки
# - Проверять есть ли все коммиты сабмодулей из нашей предыдущей версии
# - Проверять в сабмодулях, наличие ветки xxx_android текущей версии и если существует, то на ней ли мы сейчас

import subprocess
import argparse
import os
import re

class GitCheckerError(RuntimeError):
	pass


class GitChecker:
	def __init__(self, args=None):
		curr = os.path.realpath(os.path.dirname(__file__))
		self.root_dir = os.path.realpath(os.path.join(curr, *('..',)*5))
		self.script_result = 0
		self.parse_args(args)

		# так получаем коммиты для анализа
		self.GIT_LOG_ARGS = ('log', '--pretty=oneline', '--format=%H %an %ae %ad')

	def parse_args(self, args):
		parser = argparse.ArgumentParser()
		parser.add_argument('-d', '--debug_log_enable', action='store_true')
		parser.parse_args(args, self)

	def debug_log(self, msg):
		if self.debug_log_enable:
			print msg

	def run_git(self, *args):
		return self.run_git_in(self.root_dir, *args)

	def run_git_in(self, path, *args):
		cmd = ['git']
		for a in args:
			if isinstance(a, tuple) or isinstance(a, list):
				cmd.extend(a)
			else:
				cmd.append(a)
		self.debug_log(path+' $ '+' '.join(cmd))
		return subprocess.check_output(cmd, cwd=os.path.join(self.root_dir, path))

	def commits_info(self, sha_list, path=None):
		if path is None:
			path = self.root_dir
		ret = []
		for sha in sha_list:
			ret.append(self.run_git_in(path, 'show', '--name-only', sha))
		return '---\n'.join(ret)

	def get_current_branch(self):
		for l in self.run_git('branch').splitlines():
			if l.startswith('* '):
				return l[2:]
		raise GitCheckerError('cannot get a current branch name')

	def init_branch_names(self):
		self.target_branch = self.get_current_branch()
		self.app_version = None
		trg_template = re.compile(r'ver\/(\d*)_android')
		try:
			self.app_version = trg_template.search(self.target_branch).group(1)
		except:
			raise GitCheckerError('a target branch name %s doesn\'t correspond to a pattern'%self.target_branch)
		self.source_branch = 'origin/ver/'+self.app_version			# сравниваем с серверным бранчем

		self.prev_branch = None
		curr_version = int(self.app_version)
		prev_version = 0
		for l in self.run_git('branch', '-a').splitlines():
			mo = trg_template.search(l)
			if mo:
				version = mo.group(1)
				if prev_version < int(version) < curr_version:
					self.prev_branch = 'origin/'+mo.group(0)	# сравниваем с серверным бранчем

		print 'Checked branch:  ', self.target_branch
		print 'Source branch:   ', self.source_branch
		print 'Previous branch: ', self.prev_branch

	def init_list_submodules(self):
		self.submodule_pathes = []
		for l in self.run_git('config', '--file', '.gitmodules', '--get-regexp', 'path').splitlines():
			self.submodule_pathes.append(l.split()[1])

	def print_test_title(self, title):
		print
		print title
		print '-'*len(title)

	def test_error(self, title, what):
		print
		print '[ERROR]', title
		for l in what.splitlines():
			print '\t%s'%l
		self.script_result = 1

	# все ли смерджено из текущей ios ветки
	def test_source_merge(self):
		self.print_test_title('Source branch test')
		not_merged_commits = self.run_git(self.GIT_LOG_ARGS, self.source_branch, '^'+self.target_branch)
		if not_merged_commits:
			new_target_commits = self.run_git(self.GIT_LOG_ARGS, '^'+self.source_branch, self.target_branch)
			sha_list = self.check_cherry_picked_commits(not_merged_commits, new_target_commits)
			if sha_list:
				what = self.commits_info(sha_list)
				self.test_error('The following commits from %s are NOT merged!'%self.source_branch, what)
		else:
			print 'OK'

	# есть ли все коммиты из ios сабмодулей текущей ветки
	# есть ли все коммиты сабмодулей из нашей предыдущей версии
	def test_submodules_merge(self, label, branch):
		self.print_test_title(label+' submodules test')
		ok = True
		for submodule_path in self.submodule_pathes:
			ok &= self.check_submodule_merge(submodule_path, branch)
		if ok:
			print 'OK'

	# проверяем, что все коммиты сабмодуля в ветке есть у нас
	def check_submodule_merge(self, submodule_path, branch):
		src_submodule_sha = self.get_submodule_sha(submodule_path, branch)
		trg_submodule_sha = self.get_submodule_sha(submodule_path, self.target_branch)
		if src_submodule_sha is None or src_submodule_sha == trg_submodule_sha:
			return True
		not_merged_commits = self.run_git_in(submodule_path, self.GIT_LOG_ARGS, src_submodule_sha, '^'+trg_submodule_sha)
		if not_merged_commits:
			new_target_commits = self.run_git_in(submodule_path, self.GIT_LOG_ARGS, '^'+src_submodule_sha, trg_submodule_sha)
			sha_list = self.check_cherry_picked_commits(not_merged_commits, new_target_commits)
			if sha_list:
				what = self.commits_info(sha_list, submodule_path)
				self.test_error('The following commits from submodule %s branch %s are NOT merged!'%(submodule_path, branch), what)
			return False

		return True

	def get_submodule_sha(self, submodule_path, branch):
		out = self.run_git('ls-tree', branch, submodule_path)
		if not out:
			print '[INFO] Branch %s doesn\'t have submodule %s'%(branch, submodule_path)
			return
		try:
			return re.search(r'\s*commit\s*(\w*)\s*'+submodule_path, out).group(1)
		except:
			raise GitCheckerError('unexpected output "git ls-tree %s %s":  "%s"'%(branch, submodule_path, out))

	# проверяем не перенесены ли коммиты из src_commit_towel в trg_commit_towel черри пиком
	# в этом случае у них отличается sha но одинаковы автор и дата. Коммент	может отличаться
	# возвращает список sha не перенесенных
	def check_cherry_picked_commits(self, src_commit_towel, trg_commit_towel):
		def split_commit_line(l):
			n = l.find(' ')
			return l[:n], l[n+1:]	# sha и то по чему можно идентифицировать коммит, см GIT_LOG_ARGS

		src_commits = {}
		for l in src_commit_towel.splitlines():
			sha, sign = split_commit_line(l)
			src_commits[sign] = sha
		if not src_commits: return []

		for l in trg_commit_towel.splitlines():
			sha, sign = split_commit_line(l)
			if sign in src_commits:
				sha = src_commits.pop(sign)
				print '[INFO] %s was cherry-picked'%sha
				if not src_commits:
					break
		return src_commits.values()

	# Проверять в сабмодулях, наличие ветки xxx_android текущей версии и если существует, то на ней ли мы сейчас
	def test_submodules_custom_branch(self):
		self.print_test_title('Submodule custom branch test')
		ok = True
		for submodule_path in self.submodule_pathes:
			ok &= self.check_submodule_custom_branch(submodule_path)
		if ok:
			print 'OK'

	def check_submodule_custom_branch(self, submodule_path):
		custom_branches = set()
		custom_branch_sign = self.app_version+'_android'
		for l in self.run_git_in(submodule_path, 'branch', '-a').splitlines():
			if custom_branch_sign in l:
				for s in ('*', 'remotes/origin/'):
					l = l.replace(s, '')
				custom_branches.add(l.strip())
		size = len(custom_branches)
		if size == 0:
			return True
		elif size > 1:
			 self.test_error('Too many possible custom branches for submodule %s: %s'%(submodule_path, custom_branches), '')
			 return False
		custom_branch = 'remotes/origin/'+custom_branches.pop()
		#print '__deb', submodule_path, custom_branch
		for l in self.run_git_in(submodule_path, 'branch', '-a', '--contains', 'HEAD').splitlines():
			if custom_branch in l:
				return True

		self.test_error('Submodule %s has branch %s, but it is NOT set'%(submodule_path, custom_branch), '')
		return False

	def main(self):
		self.run_git('fetch')

		self.init_branch_names()
		self.init_list_submodules()

		self.test_source_merge()
		self.test_submodules_merge('Source', self.source_branch)
		self.test_submodules_merge('Previous', self.prev_branch)
		self.test_submodules_custom_branch()

		return self.script_result

if __name__ == '__main__':
	exit(GitChecker().main())
