# coding=utf8

import argparse
import os
import sys
import shutil
import tempfile
import zipfile
import subprocess
import traceback
import fnmatch
import re
import io

import validation_tools

currfile_dir = os.path.realpath(os.path.dirname(__file__))
proj_dir = os.path.realpath(os.path.join(currfile_dir, '..', '..'))
rep_dir = os.path.realpath(os.path.join(proj_dir, '..', '..', '..', '..'))
build_dir = os.path.realpath(os.path.join(proj_dir, 'app', 'build'))

sys.path.append(os.path.join(rep_dir, 'ci', 'ci-utils'))
import include.utils as ci_utils

sys.path.append(os.path.join(currfile_dir, '..'))
import version as app_version


class HsAndroidTest:
	TEST_METHOD_PREF = 'test_'

	def __init__(self):
		self.__apk_infos = None
		self.__sdk_info = None
		self.__deps_info = None
		self.__temp_dir = None
		self.__unpack_apks = None

		self.parse_args()

	def clearup(self):
		if self.__temp_dir:
			shutil.rmtree(self.__temp_dir)

	def apk_infos(self):
		if self.__apk_infos is None:
			self.__apk_infos = [validation_tools.ApkInfo(p, self.sdk_info()) for p in self.get_apk_pathes()]
		return self.__apk_infos

	def sdk_info(self):
		if self.__sdk_info is None:
			self.__sdk_info = validation_tools.SdkInfo(proj_dir)
		return self.__sdk_info

	def deps_info(self):
		if self.__deps_info is None:
			self.__deps_info = validation_tools.DepsInfo(proj_dir, self.platform, self.build_type)
		return self.__deps_info

	def temp_dir(self):
		if self.__temp_dir is None:
			self.__temp_dir = tempfile.mkdtemp(prefix='hsAndrTmp_', dir='.')
		return self.__temp_dir

	def unpack_apks(self):
		if self.__unpack_apks is None:
			self.__unpack_apks = []
			for apk_path in self.get_apk_pathes():
				unpack_apk = os.path.join(self.temp_dir(), os.path.basename(apk_path))
				with zipfile.ZipFile(apk_path) as zf:
					zf.extractall(path=unpack_apk)
				self.__unpack_apks.append(unpack_apk)
		return self.__unpack_apks

	def parse_args(self):
		parser = argparse.ArgumentParser()

		parser.add_argument('-p', '--platform', required=True, choices=('amazon', 'google'))
		parser.add_argument('-b', '--build_type', required=True, choices=('debug', 'release'))
		parser.add_argument('--executable_list', help='Comma-separated list of tests for execution')
		parser.add_argument('--excluded_list', help='Comma-separated list of excluded tests')
		parser.add_argument('--ci_path', help='Argument for ci/validateProject.py, path to the unpacked apk')
		parser.add_argument('--ci_build_type', help='Argument build_type for ci/validateProject.py', default='enterprise')

		parser.parse_args(namespace=self)

		# сразу сделаем списки
		if self.excluded_list:
			self.excluded_list = map(lambda s: s.strip(), self.excluded_list.split(','))
		if self.executable_list:
			self.executable_list = map(lambda s: s.strip(), self.executable_list.split(','))

	def main(self):
		if self.executable_list:
			for test_name in self.executable_list:
				self.make_test(test_name)
		else:
			for a in self.__class__.__dict__:
				if a.startswith(self.TEST_METHOD_PREF):
					test_name = a[len(self.TEST_METHOD_PREF):]
					self.make_test(test_name)
		return 0

	def make_test(self, test_name):
		if self.excluded_list and test_name in self.excluded_list:
			print '\nSkipped %s test\n'%test_name
			return

		method_name = self.TEST_METHOD_PREF+test_name
		_dct = self.__class__.__dict__
		if method_name in _dct:
			test_method = _dct[method_name]
		elif '_'+method_name in _dct:
			test_method = _dct['_'+method_name]	# _test_NAME можно выполнить только явно указав в executable_list
		else:
			raise RuntimeError('No %s method into %s class'%(method_name, self.__class__.__name__))

		ci_utils.logTestStart(test_name)
		try:
			err_gen = test_method(self)
			if err_gen:	# возможен обычный метод возвр. None, не генератор (e.g. CiValidateProject)
				for err in err_gen:
					ci_utils.logTestFail('%s: %s'%(test_name, err), test_name)
		except Exception as e:
			ci_utils.logTestFail('exception during a test %s: %s(%s)'%(test_name, e.__class__.__name__, e.message), test_name)
			traceback.print_exc()

		ci_utils.logTestFinish(test_name)

	def get_apk_pathes(self):
		apk_dir = os.path.join(build_dir, 'outputs', 'apk', self.platform, self.build_type)
		apk_pathes = map(lambda p:os.path.join(apk_dir, p), fnmatch.filter(os.listdir(apk_dir), '*.apk'))
		if not apk_pathes:
			raise RuntimeError('no apk files')
		return apk_pathes

	def get_platform_option(self, options_dict):
		try:
			return options_dict[self.platform]
		except KeyError:
			for k in options_dict.iterkeys():
				if k not in ('google', 'amazon'):
					raise KeyError('unknown option "%s"'%k)

			raise KeyError('no option for platform "%s"'%self.platform)

	def _test_ApkSize(self):
		for apk_path in self.get_apk_pathes():
			apk_size = os.lstat(apk_path).st_size
			max_size = self.get_platform_option({'google':100*1024*1024, 'amazon': 150*1024*1024})

			if apk_size > max_size:
				yield '%s: file size %d bytes, oversize %d bytes'%(apk_path, apk_size, apk_size-max_size)

	def test_Package(self):
		versionName = app_version.getVersion('')
		versionCode = app_version.getVersionCode()
		checked_properties = {
			'name': self.get_platform_option({'google':'com.playrix.homescapes', 'amazon': 'com.playrix.homescapes.amazon'}),
			'sdkVersion': '15',
			'targetSdkVersion': '28'
		}

		for apk_info in self.apk_infos():
			apkVersionName = apk_info.get_property('versionName')
			if re.match(versionName.replace('.', r'\.')+r'\d{1,3}', apkVersionName) is None:
				yield '%s: wrong versionName; expected %s{MINOR}; found %s'%(apk_info.apk_path, versionName, apkVersionName)

			if 'x86' in os.path.basename(apk_info.apk_path):
				strVersionCode = str(200000+versionCode)
			elif 'arm64-v8a' in os.path.basename(apk_info.apk_path):
				strVersionCode = str(400000+versionCode)
			else:
				strVersionCode = str(100000+versionCode)
			if apk_info.get_property('versionCode') != strVersionCode:
					yield '%s: wrong versionCode; expected %s; found %s'%(apk_info.apk_path, strVersionCode, apk_info.get_property('versionCode'))

			for name, value in checked_properties.iteritems():
				real_value = apk_info.get_property(name)
				if real_value != value:
					yield '%s: wrong package property %s; expected %s; found %s'%(apk_info.apk_path, name, value, real_value)

	def test_Permissions(self):
		allowed_permissions_comm = (
			'android.permission.REQUEST_INSTALL_PACKAGES',		#?
			'android.permission.VIBRATE',
			'android.permission.ACCESS_NETWORK_STATE',
			'android.permission.WAKE_LOCK',
			'android.permission.ACCESS_WIFI_STATE',
			'android.permission.INTERNET',
			'com.android.vending.BILLING',
			'com.android.vending.CHECK_LICENSE',
			'com.google.android.c2dm.permission.RECEIVE',
			'com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE',
			# c доп условиями
			'android.permission.WRITE_EXTERNAL_STORAGE',
			'android.permission.READ_EXTERNAL_STORAGE',
		)
		allowed_permissions_google = (
			'com.playrix.permission.SharedContent.google',
                        'com.playrix.permission.SharedContent.google_ent',
			'com.playrix.homescapes.permission.C2D_MESSAGE',
		)
		allowed_permissions_amazon = (
			'com.playrix.permission.SharedContent.amazon',
			'com.playrix.permission.SharedContent.amazon_ent',
			'com.playrix.homescapes.amazon.permission.C2D_MESSAGE',
			'com.amazon.device.messaging.permission.RECEIVE',
			'com.playrix.homescapes.amazon.permission.RECEIVE_ADM_MESSAGE',
		)

		allowed_permissions = self.get_platform_option({'google':allowed_permissions_comm+allowed_permissions_google,
														'amazon': allowed_permissions_comm+allowed_permissions_amazon})

		for apk_info in self.apk_infos():
			uses_permission = apk_info.get_property('uses-permission')
			for p in uses_permission.iterkeys():
				if p not in allowed_permissions:
					yield '%s: unallowed permission %s'%(apk_info.apk_path, p)
				if p == 'android.permission.WRITE_EXTERNAL_STORAGE' or p == 'android.permission.READ_EXTERNAL_STORAGE':
					try:
						if int(uses_permission[p]['maxSdkVersion']) > 22:
							yield '%s: wrong permission %s'%(apk_info.apk_path, p)
					except:
						yield '%s: wrong permission %s'%(apk_info.apk_path, p)

	def _test_ApkContent(self):
		for unpack_apk in self.unpack_apks():
			pass
		# TODO тут можно проверять содержимое apk

	# тут вызываем общепроектные тесты
	def test_CiValidateProject(self):
		target_platform = self.get_platform_option({'google':'android_gp', 'amazon': 'android_amazon'})

		if self.ci_path is None:
			raise RuntimeError('empty ci_path argument')

		cmd = ('python', '-B', '-u', 'ci/validateProject.py',
				'--path', self.ci_path, '--build_type', self.ci_build_type, '--target_platform', target_platform)

		subprocess.check_call(cmd, cwd=rep_dir)

	# пытаемся проверить, что нативные методы вызываемые из джава кода (ищем их в dex), присутствуют в so
	def test_JavaNativeCalls(self):
		exclusions = (
			# billing.cpp не используется в проекте
			'Java_com_playrix_lib_PlayrixBilling_\w+',
			# engine/test не добавляется в jni/srcDirs
			'Java_com_playrix_lib_JniWrapperTests_\w+',
			# TODO причины отсутствия в so непонятны
			'Java_com_playrix_lib_Marketing_sendSmsResult',
			'Java_com_playrix_lib_PlayrixPermissions_nativeOnRequestPermissionsResult',
		)
		if self.build_type == 'release':
			exclusions += (
				# движковый код, джава код общий, а cpp под !ENGINE_BUILD_PRODUCTION
				'Java_com_playrix_lib_AsanaTaskDialog_\w+',
			)
		elif self.build_type == 'debug':
			exclusions += (
				# загадочные нативные методы appsflyer, в релизе вырезаются прогардом
				'Java_com_appsflyer_AppsFlyer2dXConversionCallback_\w+',
			)

		for unpack_apk in self.unpack_apks():
			dexInfo = validation_tools.DexInfo(unpack_apk, self.sdk_info())

			# либа по идее одна, но непонятно в какой директории
			for r, ds, fs in os.walk(os.path.join(unpack_apk, 'lib')):
				for f in fs:
					if f == 'libgame.so':
						soInfo = validation_tools.SoInfo(os.path.join(r, f), self.sdk_info())
						for m in dexInfo.native_methods:
							if m not in soInfo.java_functions:
								for e in exclusions:
									if re.match(e, m):
										print '\t"%s": is absent in so file'%m
										break
								else:
									yield '%s: Native call "%s" is absent in so file'%(os.path.basename(unpack_apk), m)

	# пытаемся проверить, что java методы вызываемые из c++ кода (ищем их в исходниках с++), присутствуют в dex
	def test_CppJavaCall(self):
		cpp_src_dirs = (
			os.path.join(rep_dir, 'src', 'Platforms', 'android', 'src'),
			os.path.join(rep_dir, 'src'),
			os.path.join(rep_dir, 'engine', 'src'),
			os.path.join(rep_dir, 'engine', 'android', 'jni', 'android'),
			os.path.join(rep_dir, 'sdk', 'Swrve', 'src'),
			os.path.join(rep_dir, 'sdk', 'Swrve', 'src'),
			os.path.join(rep_dir, 'sdk', 'Helpshift', 'platforms', 'android', 'src'),
			os.path.join(rep_dir, 'sdk', 'Facebook', 'platforms', 'android', 'src'),
		)
		cppSrcInfo = validation_tools.CppSrcInfo(cpp_src_dirs)

		excl_changes = {'com/playrix/lib/SwrveSDKWrapper.createModInstance' : 'com/playrix/lib/SwrveSDKWrapperBase.createModInstance'}

		for unpack_apk in self.unpack_apks():
			dexInfo = validation_tools.DexInfo(unpack_apk, self.sdk_info())
			for method, extra in cppSrcInfo.java_calls.iteritems():
				if not ( method in dexInfo.static_methods or
						 excl_changes.get(method) in dexInfo.static_methods):
					yield '%s: Method %s is required in %s:%s but absent into the dex files'%(os.path.basename(unpack_apk), method, extra[0], extra[1])
				#else: print 'OK:',method

	# проверяем, что версии зависимостей из "./gradlew app:dependencies" совпадают с версиями
	# в гугл-таблице https://docs.google.com/spreadsheets/d/1kFIt37LbVhTT1nvLwmQi1awaipj-fWam290mklh8kn0
	def test_Dependencies_version(self):
		try:
			csvFileObj = validation_tools.GdiskReader().get_sheet('1kFIt37LbVhTT1nvLwmQi1awaipj-fWam290mklh8kn0', io.BytesIO())
		except:
			local_debug_csv = '_local_hs_android_sdks.csv'
			if os.path.isfile(local_debug_csv):
				csvFileObj = open(local_debug_csv)
				print 'WARNING! Local file %s is used for Dependencies_version'%local_debug_csv
			else:
				raise

		depsInfo = self.deps_info()
		for row in csvFileObj:
			columns = map(lambda s:s.strip(), row.split(','))
			name, needVersion = columns[0], columns[1] if len(columns)>1 else ''
			if not name or name.startswith('//'):
				print 'Skipped "%s" dependency'%name
				continue
			realVersion = depsInfo.get_version(name)
			if realVersion is None:
				yield 'No information about version of %s'%name
			elif realVersion != needVersion:
				yield 'Dependency %s has version %s, but %s is needed'%(name, realVersion, needVersion)

if __name__ == '__main__':
	hsTest = HsAndroidTest()
	try:
		exit(hsTest.main())
	finally:
		hsTest.clearup()
