# coding=utf8

import os
import re
import subprocess
import sys
import argparse
import glob
import fnmatch
import xml.etree.cElementTree as ET
from apiclient.http import MediaIoBaseDownload

_curr = os.path.realpath(os.path.dirname(__file__))
sys.path.append(os.path.realpath(os.path.join(_curr, '..', 'it-google-api-client')))
import it_google_api_client

class SdkInfo:
	def __init__(self, proj_dir):
		with open(os.path.join(proj_dir, 'local.properties')) as f:
			for l in f:
				key, _, value = l.partition('=')
				if key=='sdk.dir' and value:
					self.sdk_dir = value.strip()
					break
			else:
				RuntimeError('cannot get sdk dir')

		tmpl = re.compile(r'\.buildToolsVersion\s*=\s*[\'\"](.*)[\'\"]')
		with open(os.path.join(proj_dir, 'settings.gradle')) as f:
			for l in f:
				mo = tmpl.search(l)
				buildToolsVersion = mo.group(1) if mo else None
				if buildToolsVersion:
					self.build_tools_dir = os.path.join(self.sdk_dir, 'build-tools', buildToolsVersion)
					break
			else:
				RuntimeError('cannot get buildToolsVersion')

class ApkInfo:
	def __init__(self, apk_path, sdk_info):
		self.properties = {'uses-permission':{}}
		self.apk_path = apk_path

		self.run_aapt(os.path.join(sdk_info.build_tools_dir, 'aapt'), apk_path)

	def run_aapt(self, aapt, apk_path):
		cmd = (aapt, 'dump', 'badging', apk_path)
		for l in subprocess.check_output(cmd).splitlines():
			key, sep, value = l.partition(':')
			if sep == ':':
				self.handle_property_from_aapt(key, value.strip())
			# TODO тут ещё можно получить другие параметры

	def handle_property_from_aapt(self, key, value):
		if key == 'sdkVersion' or key == 'targetSdkVersion':
			self.properties[key] = value.strip("'")
		elif key == 'package':
			for _value in value.split():
				k, _, v = _value.partition('=')
				self.properties[k] = v.strip("'")	# 'package_'+ ?
		elif key == 'uses-permission':
			perm_dict = {e[0]:e[2].strip("'") for e in map(lambda s:s.partition('='), value.split())}
			name = perm_dict.pop('name')
			self.properties[key][name] = perm_dict
		# ещё параметры

	def get_property(self, name):
		if name not in self.properties:
			raise RuntimeError('unknown property '+name)
		return self.properties[name]

class DexInfo:
	def __init__(self, dex_dir, sdk_info):	# dex_dir - директория с dex файлами, например распакованный apk
		self.native_methods = set()
		self.static_methods = set()

		# парсинг plain вывода dexdump (https://android.googlesource.com/platform/dalvik/+/master/dexdump/DexDump.cpp#1625)
		self.curr_class = self.curr_method = None
		self.re_class = re.compile(r"  Class descriptor  : 'L([\w/]+);'")	# printf("  Class descriptor  : '%s'\n", classDescriptor);
		self.re_method = re.compile(r"      name          : '([\w]+)'")		# printf("      name          : '%s'\n", name);
		self.re_access = re.compile(r"      access        : 0x(\w+) \((.*)\)")	#printf("      access        : 0x%04x (%s)\n", pDexMethod->accessFlags, accessStr);
		self.parse_dexdump_line = self.find_class

		self.run_dexdump(os.path.join(sdk_info.build_tools_dir, 'dexdump'), dex_dir)

	def run_dexdump(self, dexdump, dex_dir):
		# dexdump -f FILE.DEX
		# -l xml опция не выводит приватные методы
		cmd = [dexdump, '-l', 'plain', '-f']
		dex_files = glob.glob(os.path.join(dex_dir, '*.dex'))
		if not dex_files:
			raise RuntimeError('no dex files')

		for dex_f in dex_files:
			for l in subprocess.check_output(cmd+[dex_f]).splitlines():
				# skip lines?
				self.parse_dexdump_line(l)
			if self.curr_class is not None:
				raise RuntimeError('Unexpected end of dexdump output. %s %s'%(self.curr_class, self.curr_method))

	def find_class(self, l):
		mo = self.re_class.match(l)
		if mo:
			self.curr_class = mo.group(1)
			self.parse_dexdump_line = self.find_methods_begin
	def find_methods_begin(self, l):
		if l == "  Direct methods    -":
			self.parse_dexdump_line = self.find_method
	def find_method(self, l):
		mo = self.re_method.match(l)
		if mo:
			self.curr_method = mo.group(1)
			self.parse_dexdump_line = self.find_access
		elif l.startswith('  source_file_idx   :'):
			self.curr_class = None
			self.parse_dexdump_line = self.find_class
	def find_access(self, l):
		mo = self.re_access.match(l)
		if mo:
			self.hanlde_found_method(int(mo.group(1), 16), mo.group(2))
			self.parse_dexdump_line = self.find_method
	def hanlde_found_method(self, access_mask, access_words):
		#print self.curr_class, self.curr_method, '%04x'%access_mask, access_words
		if access_mask & 0x100:		# NATIVE
			name = 'Java_%s_%s'%(self.curr_class.replace('/','_'), self.curr_method)		# nm формат
			self.native_methods.add(name)
		if access_mask & 0x8:		# STATIC
			name = '%s.%s'%(self.curr_class, self.curr_method)	# CppSrcInfo формат
			self.static_methods.add(name)


class SoInfo:
	def __init__(self, so_path, sdk_info):
		self.java_functions = []

		# TODO windows nm path
		self.run_nm('nm', so_path)

	def run_nm(self, nm, so_path):
		cmd = (nm, '-D', '--defined-only', '-fp', so_path)
		for l in subprocess.check_output(cmd).splitlines():
			words = l.split()
			if words[0].startswith('Java_'):
				self.java_functions.append(words[0])

class CppSrcInfo:
	def __init__(self, cpp_src_dirs):
		if isinstance(cpp_src_dirs, str):
			cpp_src_dirs = (cpp_src_dirs,)

		self.java_calls = {}
		call_tmpl = re.compile(r'\s*(\w*)::CallStaticMethod\s*<\s*(\w*?)\s*>\s*\(\s*"(\w*?)"\s*,')
		decl_tmpl = re.compile(r'\s*JNI_DECLARE_CLASS\s*\(\s*(\w*)\s*,\s*"(.*?)"')
		for cpp_dir in cpp_src_dirs:
			if not os.path.isdir(cpp_dir):
				raise RuntimeError('No such dir %s'%(cpp_dir))
			for rt, ds, fs in os.walk(cpp_dir):
				for cpp_f in fnmatch.filter(fs, '*.cpp'):
					fname = os.path.join(rt, cpp_f)
					with open(fname) as file_:
						# сразу добавляем из движковых хидеров (e.g. engine/android/jni/android/JniWrapper/JniClass.h)
						# TODO и вообще непонятно как парсить хидеры
						decl_classes = {'JniPlayrix' : 'com/playrix/lib/Playrix',
							'JniWrapperHelpers' : 'com/playrix/lib/JniWrapperHelpers',
							'JniMarketing' : 'com/playrix/lib/Marketing',
							'JniPlayrixBilling' : 'com/playrix/lib/PlayrixBilling',
							'JniPlayrixPurchaseDetails' : 'com/playrix/lib/PlayrixBilling$PurchaseDetails',
							'JniPlayrixItemDetails' : 'com/playrix/lib/PlayrixBilling$ItemDetails',
							'JniPlayrixFacebook' : 'com/playrix/lib/PlayrixFacebook'
							}
						for n_line, line in enumerate(file_, 1):
							decl_mo = decl_tmpl.search(line)
							if decl_mo:
								decl_name, java_class_name = decl_mo.groups()
								decl_classes[decl_name] = java_class_name
								continue
							call_mo = call_tmpl.search(line)
							if call_mo:
								decl_name, ret_type, meth_name = call_mo.groups()
								if decl_name not in decl_classes:
									raise RuntimeError('%s contains undeclared class %s in %d line'%(fname, decl_name, n_line))
								self.java_calls['%s.%s'%(decl_classes[decl_name], meth_name)] = (fname,n_line)

class DepsInfo:
	def __init__(self, proj_dir, platform, buildType):
		self.deps = {}
		self.run_gradle_dependencies(proj_dir, platform, buildType)

	def get_version(self, name):
		if name in self.deps:
			return self.deps[name]

	def run_gradle_dependencies(self, proj_dir, platform, buildType):
		cfg = platform + buildType[0].upper() + buildType[1:] + 'RuntimeClasspath'

		# TODO windows gradlew
		cmd = ('./gradlew', 'app:dependencies', '--configuration', cfg)
		skip = True
		tmpl = re.compile(r'[+|\\][+|\-\\\s]+([\w.:\-]+):([\d.+]+)')
		cnt = 0
		for l in subprocess.check_output(cmd, cwd=proj_dir).splitlines():
			if skip or not l:
				if l.startswith(cfg):
					skip = False
				elif not l:
					skip = True
				continue
			# тут только строчки с версиями
			mo = tmpl.match(l)
			if mo is None:
				#if 'project' not in l and 'Download' not in l:		# только строки с версиями
				#	raise RuntimeError('Script error: '+l)
				continue
			dep, ver = mo.groups()
			#if dep not in self.deps: cnt+=1; print '%-5d%-70s%s'%(cnt, dep, ver)
			self.deps[dep] = ver


class GdiskReader:
	def __init__(self):
		key = 'secrets/it_automation/google/api/credentials/homescapesDynamo-docs-api'
		storage = it_google_api_client.get_storage(it_google_api_client.CREDENTIAL_STORE_TYPE.vault, key=key)
		self.service = it_google_api_client.build_google_api_service(service_name='drive', version='v3', storage=storage)

	def get_all_files(self, size=10):
		results = self.service.files().list(pageSize=size, fields="nextPageToken, files(id, name)").execute()
		items = results.get('files', [])
		if items:
			for item in items:
				yield (item['name'], item['id'])
		else:
			print '[GdiskReader.get_all_files] no files'

	def get_sheet(self, id_, fObj):
		request = self.service.files().export_media(fileId=id_, mimeType='text/csv')
		downloader = MediaIoBaseDownload(fObj, request)
		done = False
		while done is False:
			status, done = downloader.next_chunk()
		fObj.seek(0, 0)
		return fObj

if __name__ == '__main__':
	# типа тесты

	parser = argparse.ArgumentParser()
	parser.add_argument('--apk')
	parser.add_argument('--dex_dir', help=u'Директория с dex файлами, например распакованный apk')
	parser.add_argument('--so')
	parser.add_argument('--cpp_dir')
	parser.add_argument('--deps', help='platform, buildType = args.deps.split(":"); e.g. google:debug')
	parser.add_argument('--gdisk', action='store_true')
	args = parser.parse_args()

	currfile_dir = os.path.realpath(os.path.dirname(__file__))
	proj_dir = os.path.realpath(os.path.join(currfile_dir, '..', '..'))
	sdkInfo = SdkInfo(proj_dir)

	if args.apk:
		apkInfo = ApkInfo(args.apk, sdkInfo)
		print '-----'
		print apkInfo.properties

	if args.dex_dir:
		dexInfo = DexInfo(args.dex_dir, sdkInfo)
		for m in dexInfo.native_methods:
			print m
		print 'Total native:', len(dexInfo.native_methods)
		for m in dexInfo.static_methods:
			print m
		print 'Total static non-native:', len(dexInfo.static_methods)

	if args.so:
		soInfo = SoInfo(args.so, sdkInfo)
		print '-----'
		for m in soInfo.java_functions:
			print m
		print len(soInfo.java_functions)

	if args.cpp_dir:
		cppInfo = CppSrcInfo(args.cpp_dir)
		for k,v in cppInfo.java_calls.iteritems(): print k,v

	if args.deps:
		platform, buildType = args.deps.split(':')
		depsInfo = DepsInfo('../..', platform, buildType)
		for i,d in enumerate(sorted(depsInfo.deps)):
			print '%-5d%-70s%s'%(i, d, depsInfo.deps[d])

	if args.gdisk:
		# для локального теста надо задать переменные окружения
		# export VAULT_TOKEN=
		gR = GdiskReader()
		#for n,i in gR.get_all_files(): print '%s [%s]'%(n, i)
		import io
		f = gR.get_sheet('1kFIt37LbVhTT1nvLwmQi1awaipj-fWam290mklh8kn0', io.BytesIO())
		for row in f: print row.strip()
