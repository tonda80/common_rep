#!/usr/bin/python
#coding=utf8

import os
import subprocess
import tempfile
import json
import time

from uTkinter import *
from commonconfig import CommonConfig
from baseapp import BaseTkApp


class AppError(RuntimeError):
	pass


class App(BaseTkApp):
	def __init__(self):
		BaseTkApp.__init__(self)

		self.dev_cache_path = '/storage/emulated/0/Android/data/com.playrix.homescapes/cache/'
		self.extra_debug_assets_path = self.dev_cache_path+'extra_debug_assets/'

	def save_config(self):
		self.config['base_dir'] = self.base_dir_entry.get()
		self.config['pathes'] = self.pathes.get_all().strip()
		self.config.save()

	def load_config(self):
		app_name = 'hs_external_commands_030419'
		default_config = {'pathes':''}
		self.config = CommonConfig(app_name, **default_config)

	def create_gui(self):
		root = uTk(u'hs_external_commands', 40, 80, createStatus=True)

		lw = 7
		w2 = 20
		new_internal_frame = lambda: uFrame(root, relief=FLAT, gmArgs = {'side':TOP, 'expand':NO, 'fill':X, 'padx':3, 'pady':2})
		fillxGmArgs = {'side':RIGHT, 'expand':YES, 'fill':X}
		#
		f = new_internal_frame()
		uLabel(f, 'base_dir', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.base_dir_entry = uEntry(f, self.config['base_dir'], gmArgs = fillxGmArgs)
		#
		uSeparator(root, gmArgs={'side':TOP})
		f = new_internal_frame()
		uLabel(f, 'pathes', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.pathes = uText(f)
		self.pathes.append(self.config['pathes'])
		f = new_internal_frame()
		uButton(f, self.reload_lua_scripts, 'Reload lua files', {'side':LEFT, 'expand':YES, 'fill':X})
		#
		uSeparator(root, gmArgs={'side':TOP})
		f = new_internal_frame()
		uButton(f, self.copy_in_extra_assets, 'Copy files', {'side':LEFT, 'expand':YES, 'fill':X})
		uButton(f, lambda *a: self.clear_status_bar() or self.run_adb('shell', 'mkdir', self.extra_debug_assets_path, info_error=True),
				'create extra dir', width=10, gmArgs={'side':RIGHT})
		uButton(f, lambda *a: self.clear_status_bar() or self.run_adb('shell', 'rm', '-r', self.extra_debug_assets_path+'*', info_error=True),
				'rm extra files', width=10, gmArgs={'side':RIGHT})
		uButton(f, lambda *a: self.clear_status_bar() or self.log.info(self.run_adb('shell', 'ls', '-R', self.extra_debug_assets_path)),
				'ls extra debug files', width=20, gmArgs={'side':RIGHT})
		#
		uSeparator(root, gmArgs={'side':TOP})
		f = new_internal_frame()
		b_key = uEntry(f, width=w2, gmArgs = {'side':LEFT})
		b_value = uCheckbutton(f, width=w2, gmArgs = {'side':RIGHT})
		uButton(f, lambda: self.set_cheat(b_key, b_value), 'Set bool cheat', gmArgs = {'side':LEFT})
		f = new_internal_frame()
		i_key = uEntry(f, width=w2, gmArgs = {'side':LEFT})
		i_value = uEntry(f, 0, width=w2, ValueType = IntVar, gmArgs = {'side':RIGHT})
		uButton(f, lambda: self.set_cheat(i_key, i_value), 'Set int cheat', gmArgs = {'side':LEFT})
		#
		uSeparator(root, gmArgs={'side':TOP})

		root.set_exit_by_escape()
		root.set_delete_callback(self.save_config)
		return root

	def report_error(self, message):
		self.log.warn(message)
		self.set_status_bar('error: %s'%message)

	# декоратор
	# adb logcat | grep ExternalCommands для контроля удаленной стороны
	def _ui_report(body):
		def f(self, *a):
			self.clear_status_bar()
			try:
				body(self, *a)				
			except Exception as e:
				self.report_error(e.message)
				if type(e) != AppError:
					raise
		return f

	# получаем список файлов из поля. пути относительные от base_mm, для копирования
	def get_file_list(self):
		base_dir = self.base_dir_entry.get()
		if not os.path.isdir(base_dir):
			raise AppError('wrong base_dir')

		pathes = [p.strip() for p in self.pathes.get_all().split('\n') if p.strip()]
		for p in pathes:
			if p.startswith('#'):
				self.log.info('%s is commented, skipped'%p)
				continue
			if os.path.isabs(p):
				rp = os.path.relpath(p, base_dir)
				if '..' in rp:
					self.log.warn('wrong relpath for %s, skipped'%p)
					continue
				p = rp
			if not os.path.isfile(os.path.join(base_dir, p)):
				self.log.warn('no file %s, skipped'%p)
				continue
			yield p

	@_ui_report
	def copy_in_extra_assets(self):
		for p in self.get_file_list():
			self.run_adb('push', p, self.extra_debug_assets_path + p, cwd=self.base_dir_entry.get())		

	@_ui_report
	def reload_lua_scripts(self):
		json_cmd = {'command': 'lua_reload', 'lua_files': []}

	# получаем список
		lua_files = []
		for p in self.get_file_list():	# цикл для лога
			if p.endswith('.lua'):
				lua_files.append(p)
				json_cmd['lua_files'].append(p.replace('\\', '/'))
			else:
				self.log.warn('%s is not lua, skipped'%p)
		if not lua_files:
			raise AppError('no lua files')

	# засылаем файлы на девайс
		for pSrc, pDst in zip(lua_files, json_cmd['lua_files']):
			self.run_adb('push', pSrc, self.extra_debug_assets_path + pDst, cwd=self.base_dir_entry.get())
	# шлем команду
		response = self.send_cmd(json_cmd)

		self.set_status_bar('lua_reload result: "%s"'%response['result'])

	def send_cmd(self, json_cmd):
		self.log.info('[send_cmd] cmd: %s'%json_cmd)
		rem_dir = self.dev_cache_path
		loc_dir = self.get_tmp_file_dir()
		cmd_loc = os.path.join(loc_dir, '_cmd.json')
		cmd_rd = os.path.join(loc_dir, '_cmd_rd')
		resp_rd = rem_dir + '_resp_rd'
		resp_rem = rem_dir + '_resp.json'
		resp_loc = os.path.join(loc_dir, '_resp.json')
		with open(cmd_loc, 'w') as jsonf, open(cmd_rd, 'w'):
			json.dump(json_cmd, jsonf)
		self.run_adb('shell', 'rm', resp_rd, info_error=True)
		self.run_adb('push', cmd_loc, rem_dir)
		self.run_adb('push', cmd_rd, rem_dir)
	# смотрим ответ
		timeout = 7
		while not self.run_adb('shell', 'ls', resp_rd, log=False, ignore_error=True):
			timeout -= 1
			if timeout == 0:
				raise AppError('no response for lua_reload')
			time.sleep(1)
		self.run_adb('pull', resp_rem, loc_dir)
		response = json.load(open(resp_loc))
		self.log.info('[send_cmd] response: %s'%response)
		return response

	def run_adb(self, *args, **kw):
		cmd = ['adb']
		cmd.extend(args)
		if kw.get('log', True):
			self.log.info(' '.join(cmd))
		try:
			return subprocess.check_output(cmd, stderr=subprocess.STDOUT, cwd=kw.get('cwd'))
		except subprocess.CalledProcessError as e:
			if kw.get('ignore_error'):
				return
			elif kw.get('info_error'):
				self.report_error(e.output)
				return
			raise

	def get_tmp_file_dir(self):
		tmp_file_dir = tempfile.mkdtemp('hs_external_commands')
		self.log.info('[get_tmp_file_dir] -> %s'%tmp_file_dir)
		return tmp_file_dir

	@_ui_report
	def set_cheat(self, key_var, value_var):
		key = key_var.getVar()
		value = value_var.getVar()
		if not key:
			raise AppError('no cheat key')
		response = self.send_cmd({'command': 'set_cheat', 'key': key, 'value': value})
		self.set_status_bar('set_cheat result: "%s"'%response['result'])


if __name__=='__main__':
	App().mainloop()
