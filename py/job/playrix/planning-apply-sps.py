# coding=utf8


import argparse
import logging
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from common import asana                # noqa: E402


class AppError(RuntimeError):
    pass


# constants and common vars
class C:
    PLANNING_BOARD_ID = 713643013377670
    story_points_id = None       # custom field id, получаем его из тасков


class Task:
    def __init__(self, _dict):
        self._dict = _dict

    def name(self):
        return self._dict['name']

    def id(self):
        return self._dict['id']

    def section_name(self):
        for member in self._dict['memberships']:
            if member['project']['id'] == C.PLANNING_BOARD_ID:
                return member['section']['name']
        raise AppError('no section name: {}'.format(self._dict))

    def story_points(self):
        for f in self._dict['custom_fields']:
            if f['name'] == 'Story Points':
                id_ = f['id']
                if C.story_points_id:
                    if C.story_points_id != id_:  # проверим на всякий случай
                        raise AppError('story points id error: {}'.format(self._dict))
                else:
                    C.story_points_id = id_
                return f['number_value']
        raise AppError('no story points: {}'.format(self._dict))


class App:
    def __init__(self, args=None):
        self.args = self.parse_args(args)
        self.set_logger()

        self.asana_client = asana.create_client(asana.get_asana_token(self.args.asana_token))

    def parse_args(self, args):
        parser = argparse.ArgumentParser()
        parser.add_argument('-v', action='store_true', help='Verbose')
        parser.add_argument('--log_only', action='store_true', help='No update action')
        parser.add_argument('--asana_token')
        return parser.parse_args(args)

    def set_logger(self):
        logging.basicConfig(format='[%(levelname)s][%(name)s] %(message)s')
        self.log = logging.getLogger('app')
        lvl = logging.DEBUG if self.args.v else logging.INFO
        self.log.setLevel(lvl)
        logging.getLogger().setLevel(lvl)

    def main(self):
        fields = ('id', 'name', 'memberships.section.name', 'memberships.project.id', 'custom_fields')
        for t in self.asana_client.tasks.find_by_project(C.PLANNING_BOARD_ID, fields=fields):
            self.process_task(Task(t))

    def process_task(self, task):
        # pprint.pprint(task._dict);raw_input('\nnext')
        if task.section_name().isdigit():
            needed_story_points = int(task.section_name())
            if needed_story_points != task.story_points():
                self.update_story_points(task, needed_story_points)
            else:
                self.log.debug(u'task "{}" already has story points value {}'.format(task.name(), needed_story_points))
        else:
            self.log.debug(u'task "{}" is not estimated yet'.format(task.name()))

    def update_story_points(self, task, story_points):
        self.log.info(u'task "{}" will be updated: story_points = {}'.format(task.name(), story_points))
        if self.args.log_only:
            return
        fields = {'custom_fields': {C.story_points_id: story_points}}
        self.asana_client.tasks.update(task.id(), fields)
        self.log.info(u'updated')


if __name__ == '__main__':
    App().main()
