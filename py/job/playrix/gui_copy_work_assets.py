#!/usr/bin/python
#coding=utf8

import os

from uTkinter import *
from commonconfig import CommonConfig
from baseapp import BaseTkApp

import copy_work_assets


class AppError(RuntimeError):
	pass


class App(BaseTkApp):
	def __init__(self):
		BaseTkApp.__init__(self)

	def save_config(self):
		self.config['root_dir'] = self.root_dir.get()
		self.config['invert_src_trg'] = self.invert_src_trg.get_option()
		self.config['pathes'] = self.pathes.get_all().strip()
		self.config.save()

	def load_config(self):
		app_name = 'copy_work_assets_141218'
		default_config = {'invert_src_trg':'to git', 'pathes':''}
		self.config = CommonConfig(app_name, **default_config)

	def create_gui(self):
		root = uTk(u'assets copy', 40, 80, createStatus=True)

		lw = 7
		new_internal_frame = lambda: uFrame(root, relief=FLAT, gmArgs = {'side':TOP, 'expand':NO, 'fill':X, 'padx':3, 'pady':2})
		fillxGmArgs = {'side':RIGHT, 'expand':YES, 'fill':X}
		f = new_internal_frame()
		uLabel(f, 'root_dir', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.root_dir = uEntry(f, self.config['root_dir'], gmArgs = fillxGmArgs)
		f = new_internal_frame()
		uLabel(f, 'pathes', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.pathes = uText(f)
		self.pathes.append(self.config['pathes'])
		f = new_internal_frame()
		uLabel(f, 'direction', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.invert_src_trg = uOptionMenu(f, ('to git', 'to_assets'), gmArgs = {'side':LEFT})
		self.invert_src_trg.setVar(self.config['invert_src_trg'])
		uButton(f, self.run, 'Run', gmArgs = fillxGmArgs)

		root.set_exit_by_escape()
		root.set_delete_callback(self.save_config)
		return root

	class Args: pass	# наследие консольного варианта

	def run(self):
		args = App.Args()
		args.root_dir = self.root_dir.get()
		args.pathes = self.pathes.get_all().split()
		args.invert_src_trg = bool(self.invert_src_trg.get_option_number())
		try:
			res, msg = copy_work_assets.main(args)
		except copy_work_assets.MyError as e:
			self.set_status_bar('Error: %s'%e.message)
		except IOError as e:
			self.set_status_bar('Exception: %s %s'%(e.message, e.args))
			raise
		else:
			if res:
				self.set_status_bar('Error: %s'%msg)
			else:
				self.set_status_bar('Ok: %s'%msg)
				self.save_config()


if __name__=='__main__':
	App().mainloop()
