# coding=utf8


import argparse
import os
import logging
import HTMLParser
import datetime
import sys

import requests
import dateutil.parser

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from common import asana        # noqa: E402


class AppError(RuntimeError):
    pass


class C:    # константы
    base = 'https://knowledge.playrix.com'
    spaceKey = 'QAOutsource'


class App:
    def __init__(self, args=None):
        self.args = self.parse_args(args)
        self.set_logger()
        self.init_authentication()
        self.init_original_refs()

    def init_authentication(self):
        user = os.getenv('CONFLUENCE_USERNAME', self.args.user)
        password = os.getenv('CONFLUENCE_PASSWORD', self.args.password)
        if user is None or password is None:
            raise AppError('unknown credentials')
        self.auth = requests.auth.HTTPBasicAuth(user, password)

    def parse_args(self, args):
        parser = argparse.ArgumentParser()
        parser.add_argument('--user')
        parser.add_argument('--password')
        parser.add_argument('--limit', type=int, default=300, help='Limit for page requests')
        parser.add_argument('--start_page', type=int, default=0, help='Start for page requests (930 for quick test)')
        parser.add_argument('--no_asana', action='store_true',
                            help='Skip asana interaction, make "export ASANA_TOKEN=?" if omitted')
        parser.add_argument('-v', action='store_true', help='Verbose')
        return parser.parse_args(args)

    def set_logger(self):
        logging.basicConfig(format='[%(levelname)s][%(name)s] %(message)s')
        self.log = logging.getLogger('app')
        lvl = logging.DEBUG if self.args.v else logging.INFO
        self.log.setLevel(lvl)
        logging.getLogger().setLevel(lvl)

    def main(self):
        pcnt = 0
        self.asana_task_report = ''
        for page in self.get_all_pages():
            self.process_page(page)
            pcnt += 1
        self.log.info('total {} pages processed'.format(pcnt))
        if self.asana_task_report and not self.args.no_asana:
            self.create_asana_task()

    def make_request(self, method_name, request, **kw):
        method = getattr(requests, method_name)
        return method(C.base+request, auth=self.auth, **kw)

    def init_original_refs(self):
        ref_page_id = 85441989  # __original_links2 = 85441989; __original_links = 85436880
        resp = self.make_request('get', '/rest/api/content/{}?expand=body.view'.format(ref_page_id))
        resp.raise_for_status()     # raise если ошибка
        self.original_refs = RefParser(resp.json()['body']['view']['value'], self.log)

    def get_all_pages(self):
        # version - смотрим дату, children.page - непустой признак секции,
        # ancestors - последний элемент родительский элемент
        req = '/rest/api/space/{}/content/page?start={}&limit={}&expand=version,children.page,ancestors'.format(
                C.spaceKey, self.args.start_page, self.args.limit)
        while req:
            resp = self.make_request('get', req)
            resp.raise_for_status()     # raise если ошибка
            data = resp.json()
            for page in data['results']:
                yield page
            req = data['_links'].get('next')

    def process_page(self, page):
        # print json.dumps(page, indent=4, separators=(',', ': '))
        orig_ref = self.original_refs.get_by_page(page)
        if orig_ref is None:
            return
        orig_page = self.get_page_by_ref(orig_ref)
        if orig_page is None:
            return
        page['__date'] = dateutil.parser.parse(page['version']['when'])
        orig_page['__date'] = dateutil.parser.parse(orig_page['version']['when'])
        if orig_page['__date'] > page['__date']:
            self.process_outdated(page, orig_page)

    def get_page_by_ref(self, ref):
        if ref.startswith('/display'):                              # ссылка в виде /display/{space}/{title}
            ref_items = ref.split('/')
            req = '/rest/api/content/?spaceKey={}&title={}&expand=version'.format(ref_items[-2], ref_items[-1])
        elif ref.startswith('/pages/viewpage.action?pageId='):      # ссылка в виде /pages/viewpage.action?pageId={}
            req = '/rest/api/content/{}?expand=version'.format(ref[30:])
        else:
            self.log.error('[get_page_by_ref] a wrong ref {}'.format(ref))
            return

        resp = self.make_request('get', req)
        if not resp.ok:
            self.log.error('[get_page_by_ref] cannot get responce for {} ({}): {}'.format(req, ref, resp.text))
            return
        jresp = resp.json()
        results = jresp.get('results')
        if results is None:     # ответ на content/{id}
            return jresp
        if len(results) == 1:
            return results[0]   # ответ на content/?spaceKey={}&title={}
        else:
            self.log.error('[get_page_by_ref] wrong results for {} ({}): {}'.format(req, ref, jresp))

    def process_outdated(self, page, orig_page):
        self.mark_title_as_outdated(page)   # меняет page
        message = u'page {} is outdated {}; original page {} is dated {}'.format(
                    url_of(page), date_of(page), url_of(orig_page), date_of(orig_page))
        self.log.info(message)
        self.asana_task_report += message+'\n\n'

    def mark_title_as_outdated(self, page):
        mark = '*'
        title = page['title']
        if title.startswith(mark):  # уже помечено
            return
        update = {
            'title': u'{} {}'.format(mark, title),
            'version': {'number': page['version']['number'] + 1},
            'type': page['type']}
        resp = self.make_request('put', '/rest/api/content/{}'.format(page['id']), json=update)
        if not resp.ok:
            self.log.error('[mark_title_as_outdated] cannot mark {}: {}'.format(url_of(page), resp.text))
        else:
            upd_page = resp.json()
            for k in ('title', 'version', '_links'):
                page[k] = upd_page[k]

    def create_asana_task(self):
        asana_client = asana.create_client(asana.get_asana_token())

        params = {}
        # TODO параметризовать это?
        proj_id = 924102137145372
        section_id = 1102120673608257
        assignee_id = 179799910405964
        params['projects'] = [proj_id]
        params['memberships'] = [{'project': proj_id, 'section': section_id}]
        params['name'] = u'Неактуальные Outsource страницы {}'.format(datetime.datetime.utcnow().ctime())
        params['notes'] = self.asana_task_report
        params['assignee'] = assignee_id

        asana_client.tasks.create(params)


# хелперы для данных страницы
def url_of(page):
    return C.base+page['_links']['webui']


def date_of(page):
    return page['__date']   # сами добавляем эту dateutil дату


# для получения ссылок на оригинальные страницы из html
class RefParser(HTMLParser.HTMLParser):
    def handle_starttag(self, tag, attrs):
        if self._state == 'wait_set':
            if tag == 'tbody':
                self._state = 'wait_item'
            return
        if tag == 'tr':
            self._state = 'wait_ref0'
            self._column = -1
            self._item += 1
        elif tag == 'td':
            self._column += 1
        elif tag == 'a':
            ref = dict(attrs)['href']
            if self._column == 0 and self._state == 'wait_ref0':
                self._ref0 = ref
                self._state = 'wait_ref1'
            elif self._column == 1 and self._state == 'wait_ref1':
                self._refs[self._ref0] = ref
                self._state = 'wait_item'

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self._state != 'wait_set' and self._state != 'wait_item':
                self.log.warning('[RefParser] parsing error of item {}'.format(self._item))
        elif tag == 'tbody':
            raise StopIteration     # интересует только первая таблица

    def __init__(self, html, log):
        HTMLParser.HTMLParser.__init__(self)
        self.log = log
        self._refs = {}
        self._state = 'wait_set'
        self._ref0 = None
        self._item = self._column = -1
        try:
            HTMLParser.HTMLParser.feed(self, html)
        except StopIteration:
            pass

    def get_by_page(self, page):
        return self._refs.get(page['_links']['webui'])


if __name__ == '__main__':
    App().main()
