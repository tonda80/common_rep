# coding=utf8

import subprocess
import os
import re

import uTkinter as _
from commonconfig import CommonConfig
from baseapp import BaseTkApp
from lsh import start as lsh_start


class AppError(RuntimeError):
	pass


class App(BaseTkApp):
	DIR_TAG = 'directory_tag'

	def __init__(self):
		BaseTkApp.__init__(self)

		self.ls_parser = LsParser(self)

		# вроде бессмысленно работать с одним приложением а потом другим, может потом сделаю рестарт если будет надо
		self.package_name = self.package_entry.getVar()
		if not self.package_name:
			self.show_error('set package name and restart app')
			return

		self.tree_map = {}			# key - tree item id, value - TreeItem
		self.tree_init()

	def load_config(self):
		self.config = CommonConfig('runas_explorer_180319',
			package='',
		)

	def save_config(self):
		self.config['package'] = self.package_entry.getVar()
		self.config['download_path'] = self.download_path_entry.getVar()
		self.config.save()

	def create_gui(self):
		root = _.uTk(u'runas explorer', 640, 480)
		root.set_delete_callback(self.save_config)

		frame_1 = _.uFrame(root, gmArgs = {'expand':_.YES, 'fill':_.BOTH})

		lw = 7
		new_internal_frame = lambda: _.uFrame(frame_1, relief=_.FLAT, gmArgs = {'side':_.TOP, 'expand':_.NO, 'fill':_.X, 'padx':3, 'pady':2})
		f = new_internal_frame()
		_.uLabel(f, 'Package', width=lw, gmArgs = {'side':_.LEFT, 'anchor':_.W})
		self.package_entry = _.uEntry(f, self.config['package'], gmArgs = {'side':_.LEFT, 'expand':_.YES, 'fill':_.X})
		f = new_internal_frame()
		_.uLabel(f, 'Download', width=lw, gmArgs = {'side':_.LEFT, 'anchor':_.W})
		self.download_path_entry = _.uEntry(f, self.config['download_path'], gmArgs = {'side':_.LEFT, 'expand':_.YES, 'fill':_.X})
		_.uButton(f, lambda: lsh_start(self.download_path_entry.getVar()), 'Open dir', width=10, gmArgs = {'side':_.RIGHT, 'anchor':_.E})

		self.tree = _.uTreeView(frame_1, 'y', gmArgs = {'expand':_.YES, 'fill':_.BOTH})

		return root

	def tree_init(self):
		self.tree.tag_configure(self.DIR_TAG, font=('Arial' if os.name == 'nt' else 'Symbol', 9, 'bold'))	#background='red',
		self.tree['columns']=('info')
		#self.tree.column('', width=100 )
		self.tree.heading('info', text='info')

		self.tree.tag_bind(self.DIR_TAG, '<<TreeviewSelect>>', self.on_dir_select)
		popup_menu = _.PopupMenu(self.tree, self.on_tree_right_button)
		popup_menu.add_callback('update', self.on_popup_update)
		popup_menu.add_callback('pull', self.on_popup_pull)
		popup_menu.add_callback('rm', self.on_popup_rm)
		popup_menu.add_callback('path2log', self.on_popup_path2log)

		self.add_tree_item(True, '', '/', '/')
		iid = self.add_tree_item(True, '', '~', self.adb_run('pwd'))
		self.tree.selection_set(iid)
		self.on_dir_select(None)

	def add_tree_item(self, is_dir, parent, text, path, extra=''):
		tags = []
		if is_dir:
			tags.append(self.DIR_TAG)
		iid = self.tree.insert(parent, 'end', text=text, tags=tags, values=(extra,))
		self.tree_map[iid] = TreeItem(iid, path, is_dir)
		return iid

	def get_selected_tree_item(self):
		iids = self.tree.selection()
		if len(iids) != 1:
			raise AppError('unexpected {}'.format(iids))
		return self.tree_map[iids[0]]

	def on_dir_select(self, event):
		item = self.get_selected_tree_item()
		# какой-то глюк, правая кнопка мыши до нажатия левой таки вызывает колбек и для не DIR_TAG айтемов (self.tree.selection_set?)
		if item.seen or not item.is_dir:
			return

		if item.path == '':
			path = self.adb_run('pwd')	# могли добавить корневой элемент после неудачного pwd
			if path == '':
				return	# добавлять дочерние уже бессмысленно
			item.path = path

		out = self.adb_run('ls', '-la', item.path)
		self.ls_parser.parse(out, item.iid)
		if out:
			item.seen = True
		child_list = self.tree.get_children(item.iid)
		if len(child_list) > 0:
			self.tree.see(child_list[0])

	def on_tree_right_button(self, event):
		iid = self.tree.identify_row(event.y)
		self.tree.selection_set(iid)	# это не вызывает on_dir_select (?)

	def on_popup_update(self, event):
		item = self.get_selected_tree_item()
		self.tree.clear_item(item.iid)		# таки чистить рекурсивно tree_map?!
		item.seen = False
		if item.is_dir:
			self.on_dir_select(None)

	def on_popup_rm(self, event):
		item = self.get_selected_tree_item()
		cmd = ['rm', item.path]
		if item.is_dir:
			cmd.insert(1, '-r')		
		self.adb_run(*cmd)
		# TODO 1) как-то отслеживать что удаление было
		self.tree.delete(item.iid)
		del self.tree_map[item.iid]

	def on_popup_path2log(self, event):
		item = self.get_selected_tree_item()
		self.log.info('[path2log] '+item.path)

	def on_popup_pull(self, event):
		item = self.get_selected_tree_item()
		if item.is_dir:
			self.show_error('pull is not implemented for directories yet')
			return
		root_download_path = self.download_path_entry.getVar()
		if not root_download_path or not os.path.isdir(root_download_path):
			self.show_error('set correct dowload directory')
			return
		if item.path[0] != '/':
			raise AppError('unexpected path {}'.format(item.path))
		dest_path = os.path.join(root_download_path, item.path[1:])
		dest_dir = os.path.dirname(dest_path)
		if not os.path.isdir(dest_dir):
			os.makedirs(dest_dir)
		args = ('adb', 'shell', 'run-as', self.package_name, 'cat', item.path)
		with open(dest_path, 'wb') as destf:
			p = subprocess.Popen(args, stdout=destf, stderr=subprocess.PIPE)
			_, err = p.communicate()
			if p.returncode != 0:
				self.show_error(err.strip())
			else:
				self.set_status_bar('Successful pull')

	def on_new_ls_item(self, item, parent_iid):
		if item.name == '.' or item.name == '..':
			return
		path = self.tree_map[parent_iid].path + '/' + item.name	# не os.path.join!
		extra = '%s  %s'%(item.perms, item.others)
		self.add_tree_item(item.perms.startswith('d'), parent_iid, item.name, path, extra)

	def adb_run(self, *command):
		args = ('adb', 'shell', 'run-as', self.package_name) + command
		str_cmd = ' '.join(args)
		self.log.info('Started "%s"'%str_cmd)
		res = ''
		try:
			res = subprocess.check_output(args, stderr=subprocess.STDOUT)
		except subprocess.CalledProcessError as e:
			self.show_error('"%s" -> %s'%(str(e), e.output.strip()))
			if 'ls' in command and 'Permission denied' in e.output:		# TODO криво как-то. upd и уже непонятно зачем(
				res = e.output
		except Exception as e:
			self.show_error('"%s" -> "%s" %s'%(str_cmd, e, type(e)))
		else:
			self.clear_status_bar()

		return res.strip()

	def show_error(self, message):
		whole_message = '[error] '+message
		self.log.error(whole_message)
		self.set_status_bar(whole_message[:200])


class TreeItem:
	def __init__(self, iid, path, is_dir):
		self.iid = iid
		self.path = path
		self.is_dir = is_dir
		self.seen = False

class LsParser:
	class Item:
		Keys = ('perms', 'name', 'others')
		def __init__(self, aa):
			for k,v in zip(self.Keys, aa):
				setattr(self, k, v)
			self.__str__ = lambda: ' '.join(aa)

			if self.perms.startswith('l'):
				ll = self.name.rpartition('->')
				if not all(ll):
					raise AppError('[LsParser.Item] wrong link {}'.format(self))
				self.name = ll[0]
				self.src_link = ll[2]

	def __init__(self, owner):
		self.owner = owner
		self.ignor_patterns = [
			re.compile(r'total \d+'),
			re.compile(r'.+ Permission denied$'),
		]
		# каждый паттерн должен определять именованные группы из Item.Keys (?P<>) см https://docs.python.org/2.7/library/re.html#re.MatchObject.group
		self.item_patterns = [
			# lrwxrwxrwx   1 root    root             63 2019-03-18 15:04 lib -> /data/app/com.playrix.example1-GpHjLYGwJW4umVves8_b1w==/lib/arm
			# crw-rw-r--  1 system radio   10,  34 2019-03-24 14:34 alarm
			# считаем путь к файлу для ссылки частью имени (хз как его в регэкспе выделить), обработаем это в Item
			re.compile(r'(?P<perms>[drwxlstc\-]{10})\s+(?P<others>\d+\s+\w+\s+\w+\s+\d+(,\s+\d+)?\s+\d{4}-\d{2}-\d{2} \d{2}:\d{2})\s+(?P<name>[.\w /\->=@]+)$'),
		]

	def parse(self, out, parent_iid):
		cnt = 0
		for l in out.splitlines():
			state = 0
			for p in self.ignor_patterns:
				if p.match(l):
					state = 1
					break
			if state == 1:
				continue
			for p in self.item_patterns:
				mo = p.match(l)
				if mo:
					state = 2
					#print '__deb1', l; print '__deb2', mo.group(*self.item_keys)
					item = LsParser.Item(mo.group(*LsParser.Item.Keys))
					self.owner.on_new_ls_item(item, parent_iid)
					cnt += 1
					break
			if state != 2:
				self.owner.log.critical('[LsParser.parse] bad line: {}\n{}'.format(l, out))
				raise AppError('LsParser.parse incomplete implementation')
		return cnt

App().mainloop()
