#!/usr/bin/python

import lsh
import argparse
import os



parser = argparse.ArgumentParser()
parser.add_argument('enc_files', nargs='*',  help='Encoded files')
parser.add_argument('--src_dir', '-d',  help='Source directory')
parser.add_argument('--rep', '-r',  required=True, help='mansion-makeover repo path. 1, 2 are special options')
args = parser.parse_args()

if args.enc_files:
	enc_files = args.enc_files
elif args.src_dir is not None:
	enc_files = [f for f in lsh.ls(args.src_dir) if f.endswith('xml')]
else:
	print 'Encoded files are not defined'
	exit(1)

for enc_file in enc_files:
	copy_prefix = 'file://'
	if enc_file.startswith(copy_prefix):
		enc_file = enc_file.replace(copy_prefix, '', 1)

	if not os.path.isabs(enc_file) and args.src_dir is not None:
		enc_file = os.path.join(args.src_dir, enc_file)

	enc_file = os.path.realpath(enc_file)

	if args.rep == '1':
		rep = '/home/ant/job/rep-s/mansion-makeover'
	elif args.rep == '2':
		rep = '/home/ant/job/rep-s/mansion-makeover_2'
	else:
		rep = args.rep
	decode_script_dir = os.path.join(os.path.realpath(rep), 'Server/updater/utils')
	if not os.path.isdir(decode_script_dir):
		raise 'Wrong utils dir: '+decode_script_dir

	print 'Encoding', enc_file
	
	lsh.cp(enc_file, enc_file+'.bkp')
	
	cmd = ('python encode_file.py -d -k xor-keys.json ' + enc_file).split()
	print '  %s $ %s'%(decode_script_dir, ' '.join(cmd))
	lsh.subprocess.check_call(cmd, cwd=decode_script_dir)
