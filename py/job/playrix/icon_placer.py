# coding=utf8

# Attempt to automate application icons update
# python icon_placer.py -s /home/ant/temp/hs_ic -t /home/ant/job/rep-s/mansion-makeover/android/app/src/main/res -f

import argparse
import os
import shutil
import PIL.Image

class ImageInfo:
	# App.all_images
	pass

class App:
	def __init__(self):
		self.parse_args()

	def critical(self, msg):
		self.perror(msg)
		raise RuntimeError(msg)

	def perror(self, msg):
		self.print_message('error', msg)

	def pwarn(self, msg):
		self.print_message('warning', msg)

	def print_message(self, level, msg):
		print '[%s]'%level,
		print msg

	def parse_args(self):
		parser = argparse.ArgumentParser()

		parser.add_argument('-s', '--src_dir', required=True, help='Icon source dir')
		parser.add_argument('-t', '--trg_dir', required=True, help='Project RES target dir')
		parser.add_argument('--src_transparent_sign', default='notif', help='Sign of transparent icon for source files')
		parser.add_argument('--trg_not_transparent_sign', default='large', help='Sign of NOT transparent icon for target files')
		parser.add_argument('-f', action='store_true', help='Make changes. If omitted only intention will be printed')

		parser.parse_args(namespace=self)

	def main(self):
		self.analyze_src()
		#print self.icons, '\n', self.transparent_icons
		self.analyze_trg()
		#print self.intent_list
		self.make_changes()

	def all_images(self, dir_):
		for f in os.listdir(dir_):
			pth = os.path.join(dir_, f)
			if not os.path.isfile(pth):
				continue
			try:
				img = PIL.Image.open(pth)
			except IOError:
				self.pwarn(f + ' is not an image')
				continue

			if img.size[0] != img.size[1]:
				self.critical(f + ' has bad size %s'%(img.size, ))

			ii = ImageInfo()
			ii.fname = f
			ii.path = pth
			ii.size = img.size[0]

			yield ii

	def analyze_src(self):
		# size: path
		self.icons = {}
		self.transparent_icons = {}

		for ii in self.all_images(self.src_dir):
			if self.src_transparent_sign in ii.fname:
				self.transparent_icons[ii.size] = ii.path
			else:
				self.icons[ii.size] = ii.path

	def analyze_trg(self):
		self.intent_list = []	# pairs of src, trg files

		def try_append_from(ii, src_dict):
			if ii.size not in src_dict:
				self.critical('No change for %s. Needed size %d, there are sizes: %s'%(ii.path, ii.size, src_dict.keys()))
			if not os.access(ii.path, os.W_OK):
				self.critical('No write access to %s'%ii.path)
			self.intent_list.append((src_dict[ii.size], ii.path))


		for dens in ('mdpi', 'hdpi', 'xhdpi', 'xxhdpi', 'xxxhdpi'):
			for ii in self.all_images(os.path.join(self.trg_dir, 'mipmap-'+dens)):
				try_append_from(ii, self.icons)

			for ii in self.all_images(os.path.join(self.trg_dir, 'drawable-'+dens)):
				if self.trg_not_transparent_sign in ii.fname:
					try_append_from(ii, self.icons)
				else:
					try_append_from(ii, self.transparent_icons)

	def make_changes(self):
		for intent in self.intent_list:
			print '%-50s'%intent[0], '=>', intent[1]
			if self.f:
				shutil.copy(intent[0], intent[1])
				print 'Changed'

App().main()
