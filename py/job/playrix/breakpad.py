#!/usr/bin/python
#coding=utf8

# https://chromium.googlesource.com/breakpad/breakpad/+/master/README.ANDROID#93
# в хоккей\апцентр загружать zip symbols папки (ret_dir в ret_dir)


import os
import subprocess
import shutil

from baseapp import BaseConsoleApp
from commonconfig import CommonConfig


class AppError(RuntimeError):
	pass


def is_hex(s):
	try:
		int(s, 16)
	except ValueError:
		return False
	return True

class App(BaseConsoleApp):
	def add_arguments(self, parser):
		subparsers = parser.add_subparsers(help=u'Main target')
		gsym_parser = subparsers.add_parser('gsym', help='Generate symbols from so')
		gsym_parser.add_argument('so_path', help=u'Путь к so')
		gsym_parser.set_defaults(target_func=self.create_symbols)

		gsym_parser = subparsers.add_parser('rst', help='Config reset')		# тут похоже gsym_parser должен быть назван rst_parser чтоб не путать
		gsym_parser.set_defaults(target_func=self.config_reset)

	def main(self):
		self.cfg = CommonConfig('breakpad_040919')
		self.cfg.auto_asked('dump_syms_path')
		self.args.target_func()

	def config_reset(self):
		self.cfg.reset()

	def create_symbols(self):
		so_path = os.path.realpath(self.args.so_path)
		self.create_symbols_impl(so_path)

	def create_symbols_impl(self, so_path, dst_dirname = 'symbols'):
		self.log.debug('[create_symbols] source: {}'.format(so_path))
		so_name = os.path.basename(so_path)
		so_dir = os.path.dirname(so_path)			# результат работы тут, рядом с so
		sym_path = os.path.join(so_dir, so_name + '.sym')
		# собственно генерируем символы
		# TODO может надо проверять что уже есть сгенеренные. запоминать в commonconfig {so_md5: fin_sym_path}?
		dump_syms_cmd = (self.cfg['dump_syms_path'], so_path)
		self.log.info('[create_symbols] will be started: {} > {}'.format(dump_syms_cmd, sym_path))
		with open(sym_path, 'wb') as symf:
			subprocess.call(dump_syms_cmd, stdout=symf)
		# копируем в правильную папку fin_dir
		ret_dir = os.path.join(so_dir, dst_dirname)
		with open(sym_path) as symf:
			l0_words = symf.readline().split()
		if l0_words[0] != 'MODULE' or not is_hex(l0_words[3]) or l0_words[4] != so_name:
			self.log.error('Bad symbol\'s first line: {}'.format(l0_words))
			raise AppError('Bad symbol file {}'.format(sym_path))
		fin_dir = os.path.join(ret_dir, so_name, l0_words[3])
		if os.path.isdir(fin_dir):
			shutil.rmtree(fin_dir)	# удалим для ясности директорию со старыми символами
		os.makedirs(fin_dir)
		shutil.move(sym_path, fin_dir)
		self.log.info('[create_symbols] symbols are created into: {}'.format(fin_dir))

	# были идеи использования create_symbols
	# на лету создаем символы и тут же с их помощью раскручиваем дамп
	# это вроде так и не пошло, поэтому кусочки кода для этих целей выкину
	# сюда, ибо вдруг ещё пригодятся
	# # возвращаем и путь для раскрутки (symbols) и путь к собственно символам (вроде предполагалось что раскручивать по ним дамп)
	# fin_sym_path = os.path.join(fin_dir, os.path.basename(sym_path))
	# return (ret_dir, fin_sym_path)

	# не нужен зип ибо часто надо пару либ добавить, но это надо сохранить, как пример путей make_archive
	# if zip_: shutil.make_archive(ret_dir, 'zip', so_dir, dst_dirname)

	def unwind_dump(self, dump_path, sym_path):
		self.log.info('[unwind_dump] source: {} {}'.format(dump_path, sym_path))
		#minidump_stackwalk_cmd = (C.minidump_stackwalk_path, so_path)
			#with open(sym_file, 'w') as outf:
			#	subprocess.call(dump_syms_cmd, stdout=outf)

if __name__ == '__main__':
	App().main()
