#!/usr/bin/python
#coding=utf8

# clng в названии, чтоб не искал сам себя)

import lsh
import argparse
from time import sleep

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--delay', type = int, default = 2, help='How often to look')
parser.add_argument('-s', '--stop_counter', type = int, default = 5, help='How long script works when no processes (-1 forever)')
parser.add_argument('--show_all_files', action='store_true', help='Show processes with all files (\.s etc)')
args = parser.parse_args()

ignored_target_extensions = ('.s')

while 1:
	targets = []
	for p in lsh.find_process('clang', False):
		try:
			cmdline = p.cmdline()
			i = cmdline.index('-o')
		except:
			continue
		targets.append(cmdline[i+1])

	if not targets:
		args.stop_counter -= 1

	for target in targets:
		ignored = False
		if not args.show_all_files:
			for e in ignored_target_extensions:
				if target.endswith(e):
					ignored = True
					break
		if ignored:
			continue

		s_cnt = 4	# печатаем от этого с конца слеша
		i = 0
		while s_cnt and i != -1:
			i = target.rfind('/', 0, i-1)
			s_cnt -= 1
		print target[i+1:]
	print '-'

	if args.stop_counter == 0:
		break
	sleep(args.delay)
