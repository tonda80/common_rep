#!/usr/bin/python
#coding=utf8

# Работая со скриптами часто удобно править их прямо в assets директории.
# Потом измененные файлы нужно переносить в директорию для гита
# Эта чудо-утилита просто делает этот процесс чуточку менее дебильным

import lsh
import os
import argparse

class MyError(RuntimeError):
	pass

def test_dir(pth, what):
	print '%s: %s'%(what, pth)
	if not lsh.test_dir(pth):
		raise MyError('Wrong %s: %s'%(what, pth))

def main(args):
	root_dir = os.path.realpath(args.root_dir)
	assets_rel_path = 'src/Platforms/android/proj/tools/assets/dev_assets/assets'.split('/')
	assets_dir = os.path.realpath(os.path.join(root_dir, *assets_rel_path))
	git_dir = os.path.realpath(root_dir)

	print '\n---- Here we go ----'
	test_dir(assets_dir, 'assets directory')
	test_dir(git_dir, 'git directory')
	if not args.pathes:
		raise MyError('No input files')

	src = assets_dir
	trg = git_dir
	if args.invert_src_trg:
		src, trg = trg, src

	skipped = False

	print '\n---- Copying to %s ----'%('assets' if args.invert_src_trg else 'git' )
	cp_cnt = 0
	for p in args.pathes:
		if os.path.isabs(p):	# абсолютный путь, пытаемся понять относительный путь от ресурсов (более длинный)
			p = os.path.realpath(p)
			rel_p = os.path.relpath(p, assets_dir)
			if '..' in rel_p:	# но таки могли скопировать путь от гита
				rel_p = os.path.relpath(p, git_dir)
			if '..' in rel_p:	# файл вне репы
				print 'Skipping wrong path %s (out git)'%p
				skipped = True
				continue
			p = rel_p

		src_p = os.path.join(src, p)
		if not lsh.test_file(src_p):
			print 'Skipping wrong path %s [%s] (bad source)'%(p, src_p)
			skipped = True
			continue
		trg_p = os.path.join(trg, p)

		cp_cnt += 1
		print '%d) %s => %s'%(cp_cnt, src_p, trg_p)
		lsh.cp(src_p, trg_p)

	if skipped:
		return 1, 'some file were skipped'
	else:
		return 0, 'copied %d files'%cp_cnt


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--pathes', nargs='*', required=True, help='Copied pathes (absolute or relative to source or target directory)')
	parser.add_argument('-r', '--root_dir', required=True, help='Repo root directory')
	parser.add_argument('-i', '--invert_src_trg', action='store_true', help='Copies files from VCS to assets. The contrary (from assets to VCS) is default')
	args = parser.parse_args()

	main(args)
