# coding=utf8

# Запуск скрипта
# --------------
# python xml_test_task.py -t both -d out
# Можно также задать (--help для деталей):
#   директорию для генерируемых файлов, количество рабочих процессов, тип запуска (только генерация, только анализ)
# Тестировалось только на линуксе, на 2-м питоне, но думаю все должно работать и на винде.

# Комментарии по тестовому заданию
# --------------------------------
# > примерную оценку времени, которое вы потратили на выполнение задания;
# Грубо, часов 6.
#
# > комментарии по коду, почему выбран тот или иной способ решения;
# Сразу захотелось выставлять каждому рабочему процессу аффинити маску, поэтому применил велосипедный пул процессов.
# В остальном, вроде все очевидно. Скрипт запускает N процессов, по умолчанию N - количество cpu, каждый процесс работает на своем ядре,
# и занимается созданием\обработкой одного zip файла. Анализирующие процессы шлют csv строчки в очереди.
# Возможно создание временных файлов в анализирующих процессах и последующее их слияние в вызывающем ускорило бы процесс,
# но мне показалось что результат уже неплох (у меня 1.6 сек) и проверять я пока не стал.
#
# > возникали ли проблемы при выполнении тестового.
# Не заметил особых проблем)

# Замечания
# ---------
# Интересно, что однократная на файл запись в очередь (с накоплением данных в строке или листе строк) не дает никакого выигрыша в скорости
# То есть основная нагрузка видимо в прожевывании zip/xml

import argparse
import tempfile
import os
import shutil
import multiprocessing
import affinity
import time
import zipfile
import xml.etree.cElementTree as ET
import random
import string
import uuid
#import sys
import threading
import Queue

class App:
	def __init__(self):
		self.GEN_DIR = 'generated'
		self.OUT_1 = 'out1.csv'
		self.OUT_2 = 'out2.csv'
		self.CSV_SEP = ','
		self.N_ZIP = 50
		self.N_XML = 100

		self.parse_args()

		self.process_mgr = ProcessMgr(self.worker_quantity)

		random.seed()

	def parse_args(self):
		parser = argparse.ArgumentParser()

		parser.add_argument('-t', '--target', required=True, choices=('generate', 'analyze', 'both'), help='Target of launching')
		parser.add_argument('-d', '--dir', help='Work directory')
		parser.add_argument('-w', '--worker_quantity', type=int, help='Quantity of processes. CPU count is default')

		parser.parse_args(namespace=self)

	def main(self):
		self.prepare_work_dir()

		if self.target == 'generate' or self.target == 'both':
			t = time.time()
			print 'Generating'
			self.generate()
			print 'Generation time:', time.time() - t, 's'
		if self.target == 'analyze' or self.target == 'both':
			t = time.time()
			print 'Analysis'
			self.analyze()
			print 'Analysis time:', time.time() - t, 's'

	def prepare_work_dir(self):
		if self.dir == None:
			self.dir = tempfile.mkdtemp(dir='.')
		elif not os.path.isdir(self.dir):
			os.mkdir(self.dir)

		self.dir = os.path.abspath(self.dir)
		self.gen_dir = os.path.join(self.dir, self.GEN_DIR)
		print 'Work directory is', self.dir

	def clean_or_create_dir(self, dirname):
		if os.path.isdir(dirname):
			shutil.rmtree(dirname)
		os.mkdir(dirname)

	def generate(self):
		self.clean_or_create_dir(self.gen_dir)

		def tasks_generator():
			for i in xrange(self.N_ZIP):
				zip_name = os.path.join(self.gen_dir, str(i)+'.zip')
				yield (self.generate_zip_file, (zip_name,))

		# debug task
		'''
		def tasks_generator():
			def test_printer(s):
				cnt = 10000000
				while cnt > 0:
					sys.stdout.write(s)
					sys.stdout.flush()
					cnt -= 1
			for s in 'qwertyuiopasdfghjklzxcvbnm':
				yield (test_printer, s)
		#'''

		self.process_mgr.make(tasks_generator())

	def generate_zip_file(self, name):
		temp_dir = name+'_temp'
		self.clean_or_create_dir(temp_dir)

		with zipfile.ZipFile(name, 'w') as zfile:
			for i in xrange(self.N_XML):
				xml_name = os.path.join(temp_dir, str(i)+'.xml')
				self.generate_xml_file(xml_name)
				zfile.write(xml_name, os.path.basename(xml_name))

		shutil.rmtree(temp_dir)

	def generate_xml_file(self, xml_name):
		root = ET.Element('root')

		var = ET.SubElement(root, 'var')
		var.set('name', 'id')
		var.set('value', uuid.uuid4().get_hex())

		var = ET.SubElement(root, 'var')
		var.set('name', 'level')
		var.set('value', str(random.randrange(1, 101)))

		objects = ET.SubElement(root, 'objects')
		rand_str = lambda: ''.join(random.choice(string.letters) for _ in xrange(random.randrange(5, 15)))
		for i in xrange(random.randrange(1, 11)):
			object_ = ET.SubElement(objects, 'object')
			object_.set('name', rand_str())

		ET.ElementTree(root).write(xml_name)

	def analyze(self):
		self.queue1 = multiprocessing.Queue()
		self.queue2 = multiprocessing.Queue()

		def tasks_generator():
			for z in os.listdir(self.gen_dir):
				yield (self.analyze_zip_file, (z,))

		thread_work_flag = True
		def queue_thread_func(fname, queue):
			with open(fname, 'w') as outf:
				while thread_work_flag:
					try:
						outf.write(queue.get(True, 1))
					except Queue.Empty:
						continue


		t1 = threading.Thread(target=queue_thread_func, args=(os.path.join(self.dir, self.OUT_1), self.queue1))
		t2 = threading.Thread(target=queue_thread_func, args=(os.path.join(self.dir, self.OUT_2), self.queue2))
		t1.start()
		t2.start()

		self.process_mgr.make(tasks_generator())

		thread_work_flag = False
		t1.join()
		t2.join()

	def analyze_zip_file(self, zip_name):
		with zipfile.ZipFile(os.path.join(self.gen_dir, zip_name)) as zfile:
			for name in zfile.namelist():
				with zfile.open(name) as xfile:
					root = ET.parse(xfile).getroot()
					id_ = root.find("./var[@name='id']").get('value')
					level = root.find("./var[@name='level']").get('value')
					self.queue1.put(id_+self.CSV_SEP+level+'\n')

					for i in root.findall('./objects/object'):		#root.iter('object')
						obj_name = i.get('name')
						self.queue2.put(id_+self.CSV_SEP+obj_name+'\n')

class ProcessMgr:
	def __init__(self, worker_quantity):
		if worker_quantity is None:
			try:
				worker_quantity = multiprocessing.cpu_count()
			except NotImplementedError:
				worker_quantity = None

		if worker_quantity:
			self.workers = [Worker(1<<i) for i in xrange(worker_quantity)]
		else:
			self.workers = [Worker(1) for i in xrange(4)]	# random quantity

	def make(self, tasks):
		for task, args in tasks:
			should_be_made = True
			while should_be_made:
				for worker in self.workers:
					if worker.is_free():
						worker.make(task, args)
						should_be_made = False
						break
				else:
					time.sleep(0.1)

		for worker in self.workers:
			worker.join()

class Worker:
	def __init__(self, affinity_mask):
		self.proc = None
		self.affinity_mask = affinity_mask

	def make(self, task, args):
		self.proc = multiprocessing.Process(target=task, args=args)
		self.proc.start()
		affinity.set_process_affinity_mask(self.proc.pid, self.affinity_mask)

	def is_free(self):
		if self.proc is None:
			return True
		if self.proc.is_alive():
			return False

		if self.proc.exitcode != 0:
			print '[error] process exitcode ==', self.proc.exitcode
		self.proc = None
		return True

	def join(self):
		if self.proc:
			self.proc.join()



App().main()
