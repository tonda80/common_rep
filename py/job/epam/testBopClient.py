#coding=utf8

import socket
import struct
import threading
from time import time, sleep, strftime
import traceback
import os
from sys import argv

class BopMessageError(RuntimeError):
	pass

class BopMessage:
	# u16 msgSize_; char type_;
	HeaderStruct = '=Hc'
	HeaderSize = struct.calcsize(HeaderStruct)
	# see "depends on HeaderStruct" comments when change
	SizeCorrection = 2	# value of the size field is size of the rest of message,

	# char type_;
	THeaderStruct = '=c'
	THeaderSize = struct.calcsize(THeaderStruct)

	@classmethod
	def _add_spaces(cls, fields):
		ret = list(fields)
		for i,f in enumerate(ret):
			if isinstance(f, str) and len(f) > 1:
				ret[i] += ' '*40
		return tuple(ret)

	@classmethod
	def pack(cls, *fields):		# non trading message only
		fields = cls._add_spaces(fields)

		msgSize = struct.calcsize(cls.Struct) + cls.HeaderSize - 2 	# 2 == sizeof(msgSize_)
		header = struct.pack(cls.HeaderStruct, msgSize, cls.Type)

		content = struct.pack(cls.Struct, *fields)

		return header + content

	@classmethod
	def pack_trading(cls, payload):		# TradingMessage only
		msgSize = len(payload) + cls.HeaderSize - 2		# 2 == sizeof(msgSize_)
		header = struct.pack(cls.HeaderStruct, msgSize, cls.Type)

		return header + payload

	@classmethod
	def unpack(cls, packedMsgs):
		msgs = []

		while 1:
			size = len(packedMsgs)
			if size < cls.HeaderSize:
				break

			msgSize, type_ = struct.unpack(cls.HeaderStruct, str(packedMsgs[:cls.HeaderSize]))
			msgSize += cls.SizeCorrection

			MsgClass = cls.__getClass(type_)
			if MsgClass is None:
				raise BopMessageError('unknown BopMessage type: %s'%type_)

			if size < msgSize:
				break

			packedMsg = str(packedMsgs[:msgSize])
			del packedMsgs[:msgSize]

			# TODO many objects into one packedMsg

			msgObj = MsgClass()
			msgObj.unpack_fields(packedMsg, type_)
			msgs.append(msgObj)

		return msgs

	@classmethod
	def __getClass(cls, type_):
		return {
			'S' : TradingNotificationMessage,
			'R' : ClientHeartbeatMsg,
			'H' : ServerHeartbeatMsg,
			'A' : LoginAcceptedMsg,
			'J' : LoginRejectedMsgT,
		}.get(type_, None)

	def unpack_fields(self, packedMsg, type_):		# transport message implementation
		 self.fields = struct.unpack(self.Struct, packedMsg[self.HeaderSize:])

	def __str__(self):
		return self.Type + ':' + str(self.fields)


class LoginMsg(BopMessage):
	# char Username_[8]; char Password_[10]; char Session_[10]; uint64_t RequestedSeqNum_;
	Struct = '=8s10s10sQ'
	Type = 'L'

class ClientHeartbeatMsg(BopMessage):
	Struct = '='
	Type = 'R'

class ServerHeartbeatMsg(BopMessage):
	Struct = '='
	Type = 'H'

class LoginAcceptedMsg(BopMessage):
	Struct = '=10sQ'
	Type = 'A'

class LoginRejectedMsgT(BopMessage):
	Struct = '=c'
	Type = 'J'

# -------- TradingMessage --------

class TradingMessage(BopMessage):
	Type = 'U'

	@classmethod
	def pack(cls, *fields):
		fields = cls._add_spaces(fields)

		theader = struct.pack(cls.THeaderStruct, cls.TType)
		content = struct.pack(cls.Struct, *fields)

		return cls.pack_trading(theader+content)

class EnterOrderMsg(TradingMessage):
	# char OrderIdentifier_[14]; char SideIndicator_; uint64_t OrderQty_; char Symbol_[10]; int64_t Price_; uint32_t TimeInForce_; char OrdType_; char OriginatingTrader_[8];
	Struct = '=14scQ10sqIc8s'
	TType = 'O'

class CancelOrderMsg(TradingMessage):
	# char OrderIdentifier_[14]
	Struct = '=14s'
	TType = 'X'

class ReplaceOrderMsg(TradingMessage):
	# char ExistingOrderIdentifier_[14]; char ReplaceOrderIdentifier_[14]; uint64_t OrderQty_; int64_t Price_;
	Struct = '=14s14sQq'
	TType = 'U'

# -------- TradingNotificationMessage --------

class TradingNotificationMessage(BopMessage):
	Type = 'S'

	@classmethod
	def __getTClass(cls, type_):
		return {
			'J' : RejectedMsg,
			'A' : OrdedAcceptedMsg,
			'X' : OrderCancelledMsg,
			'E' : OrdedExecutedMsg,
			'U' : OrderReplaced,
		}.get(type_, None)

	def unpack_fields(self, packedMsg, type_):
		theader_beg = self.HeaderSize
		theader_end = theader_beg+self.THeaderSize
		self.ttype,  = struct.unpack(self.THeaderStruct, packedMsg[theader_beg:theader_end])
		if self.ttype == '#':	# TODO now just skip TimingData
			theader_end += 189; theader_beg += 189	# sizeof(SystemTimingData)
			self.ttype,  = struct.unpack(self.THeaderStruct, packedMsg[theader_beg:theader_end])

		MsgClass = self.__getTClass(self.ttype)
		if MsgClass is None:
			raise BopMessageError('unknown BopMessage trading type: %s'%self.ttype)
		tfields = struct.unpack(MsgClass.Struct, packedMsg[theader_end:])
		self.fields = (self.ttype, ) + tfields

	@classmethod
	def pack(cls, *fields):		# only for debugging
		theader = struct.pack(cls.THeaderStruct, cls.TType)
		content = struct.pack(cls.Struct, *fields)

		return cls.pack_trading(theader+content)

class OrdedAcceptedMsg(TradingNotificationMessage):
	# uint64_t TrackingNumber_; uint64_t Timestamp_; char OrderIdentifier_[14]; char SideIndicator_; uint64_t OrderQty_; char Symbol_[10]; int64_t Price_; uint32_t TimeInForce_; char OrdType_; char OriginatingTrader_[8]; uint64_t OrderRefNum_;
	Struct = '=QQ14scQ10sqIc8sQ'
	TType = 'A'

class RejectedMsg(TradingNotificationMessage):
	# uint64_t Timestamp_; char OrderIdentifier_[14]; char Reason_;
	Struct = '=Q14sc'
	TType = 'J'

class OrderCancelledMsg(TradingNotificationMessage):
	# uint64_t TrackingNumber_; uint64_t Timestamp_; char OrderIdentifier_[14]; char Reason_;
	Struct = '=QQ14sc'
	TType = 'X'

class OrdedExecutedMsg(TradingNotificationMessage):
	# uint64_t TrackingNumber_; uint64_t Timestamp_; char OrderIdentifier_[14]; uint64_t ExecutedQty_; int64_t ExecutionPrice_; uint64_t TradeID_; char MLegReportingType_;
	Struct = '=QQ14sQqQc'
	TType = 'E'

class OrderReplaced(TradingNotificationMessage):
	# uint64_t TrackingNumber_; uint64_t Timestamp_; char ReplaceOrderIdentifier_[14]; char SideIndicator_; uint64_t OrderQty_; char Symbol_[10]; int64_t Price_; uint32_t TimeInForce_; char OrdType_; char OriginatingTrader_[8]; uint64_t OrderRefNum_;
	Struct = '=QQ14scQ10sqIc8sQ'
	TType = 'U'

# --------------------------------------------

class Global:
	work_flag = True

class TestClient:
	def __init__(self, host, port, user, pass_, session, reqSeqNum):
		self.sck = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.sck.connect((host, port))
		loginMsg = LoginMsg.pack(user, pass_, session, reqSeqNum)
		#print len(loginMsg), '\n', repr(loginMsg)
		self.sck.send(loginMsg)
		self.sck.settimeout(2)
		self.sck_recv_callback = self.sck_err_callback = None

		threading.Thread(target=self.heartbeatThread).start()

	def start_recv(self, onRecv, onErr):
		self.sck_recv_callback = onRecv
		self.sck_err_callback = onErr
		threading.Thread(target=self.recvThread).start()

	def send(self, msg):
		return self.sck.send(msg)

	def recvThread(self):
		while Global.work_flag:
			try:
				recv_msg = self.sck.recv(4096)
			except socket.timeout:
				continue
			except socket.error as err:
				self.sck_err_callback(err)
				return
			if recv_msg == '':
				self.sck_err_callback('closed TestClient.recvThread')
				return

			self.sck_recv_callback(recv_msg)

	def heartbeatThread(self):
		hbMsg = ClientHeartbeatMsg.pack()
		cnt_delay = 5
		while Global.work_flag:
			cnt_delay -= 1
			if cnt_delay <= 0:
				try:
					self.sck.send(hbMsg)
				except socket.error as err:
					self.sck_err_callback(err)
					return
				cnt_delay = 5
			sleep(1)

class App:
	def __init__(self, host, port, user, pass_, session, reqSeqNum, logDir):
		self.log = open(os.path.join(logDir, strftime('%y%m%d_%H%M%S.log')), 'a')

		self.out('starting client for %s:%d %s '%(host, port, user))
		while 1:
			try:
				self.client = TestClient(host, port, user, pass_, session, reqSeqNum)
			except socket.error as err:
				self.out('Trying to reconnect (%s)'%err, 'error')
				sleep(1)
			else:
				break

		self.rec_buf = bytearray()
		self.client.start_recv(self.onRecv, self.onErr)

	def str_dump(self, st):
		return ' '.join('%02x'%ord(c) for c in st)

	def onRecv(self, recv_msg):
		#print '__deb', recv_msg
		self.rec_buf.extend(recv_msg)

		try:
			msgs = BopMessage.unpack(self.rec_buf)
		except Exception as e:
			self.out(e, 'error')
			self.out('Cannot unpack a message:\n%s\n%s'%(recv_msg, self.str_dump(recv_msg)), 'error')
			return

		if len(self.rec_buf) != 0:
			self.out('received message:\n%s\n%s'%(recv_msg, self.str_dump(recv_msg)), 'debug')
			self.out('receiving buffer:\n%s\n%s'%(self.rec_buf, self.str_dump(self.rec_buf)), 'debug')

		for msg in msgs:
			if msg.Type == 'H':
				return	# skips heartbeats

			self.out('recv message: %s'%msg)

	def send(self, MsgType, *args):
		self.out('sent message: %s%s'%(MsgType.__name__, args))
		bin_msg = MsgType.pack(*args)
		self.client.send(bin_msg)

	def onErr(self, err):
		self.out('socket error: %s'%err, 'error')

	def out(self, msg, level='info'):
		out_line = '%-8s %s'%(level, msg)
		print out_line
		self.log.write(out_line + '\n')
		self.log.flush()

	def run(self):
		try:
			self.__run()
		except KeyboardInterrupt:
			pass
		except BaseException as e:
			self.out('unhandled error: %s'%e, 'error')
			traceback.print_exc()
		except:
			self.out('unhandled unknown error', 'error')
			traceback.print_exc()
		finally:
			Global.work_flag = False

	def __run(self):
		while 1:
			cmd = raw_input()
			if cmd == 'q':
				self.out('exiting')
				return

			elif cmd == 'all':
				for t in os.listdir('test_cases'):
					if os.path.splitext(t)[1] == '.py':
						execfile(os.path.join('test_cases', t))

			#elif cmd == 't':	# internal test
			#	msg = RejectedMsg.pack(time(), 'someid', 'r')
			#	self.onRecv(msg)

			else:
				test_name = os.path.join('test_cases', '%s.py'%cmd)
				if os.path.isfile(test_name):
					execfile(test_name)
				else:
					self.out('unknown command: %s'%cmd, 'error')

if __name__ == '__main__':
	logDir = ''	#r'C:\Users2\ab\tmp\logs'
	if len(argv) <2:		# default
		app = App('127.0.0.1', 10503, 'epafen01', 'fustepam', 'testSess', 0, logDir)
	elif argv[1] == 'r':	# remote
		app = App('lonhftdev-d1', 10503, 'epafen01', 'fustepam', 'testSess', 0, logDir)

	app.run()


