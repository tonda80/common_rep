import lsh
import os
import re

ISSUES_DIR = r'D:\aberezin\job_issues\bfix'

def test_issue_id(id_):
	for d in os.listdir(ISSUES_DIR):
		d = d.upper()
		if d.find(id_) != -1:
			raise RuntimeError('issue id exists')

get_correct_id = True
while get_correct_id:		
	issue_id = raw_input('Enter the issue ID: ').upper()
	get_correct_id = re.search(r'[A-Z]{3,}-\d{3,}', issue_id) is None
	
issue_comment = raw_input('Enter the issue short comment: ').replace(' ', '_')
issue_name = '%s_%s'%(issue_id, issue_comment)

issue_dir = os.path.join(ISSUES_DIR, issue_name)
issue_memo = os.path.join(issue_dir, '%s.txt'%issue_name)

try:
	test_issue_id(issue_id)
	lsh.mkdir(issue_dir)
	lsh.touch(issue_memo)
except:
	print 'id exists %s'%issue_id
	raw_input()	
