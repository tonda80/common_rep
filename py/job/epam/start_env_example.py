#!/usr/bin/python

# Add SSL support to initiators in FIX Edge

import sys
sys.path.append(r'D:\aberezin\files\py\job\epam')
from common_env import *

import time

import argparse

def get_args():
	parser = argparse.ArgumentParser()

	parser.add_argument("-cl", action="store_true", help='Clear session logs')
	parser.add_argument("-e", action="store_true", help='Start FIXEdge')
	parser.add_argument("-ke", action="store_true", help='Kill FIXEdge')
	parser.add_argument("-s", action="store_true", help='Start Simple client')
	parser.add_argument("-ks", action="store_true", help='Kill Simple client')
	parser.add_argument("-b", action="store_true", help='Set clipboard')
	parser.add_argument("-r", action="store_true", help='Start raw_cap')
	parser.add_argument("-cd", action="store_true", help='Clear raw_cap dump')
	parser.add_argument("-l", action="store_true", help='Start quick_start_listener')

	return parser.parse_args()

# ------------------------
cmd_args = get_args()

LOG_PATHES = [
	r'D:\aberezin\job\_simple_client\samples\SimpleClient\x64-Release\logs',
	r'D:\aberezin\job_issues\BBP-2850_ssl2fixedge\fixedge\log',
	r'D:\rep-s\fix\engine\samples\FIX_QuickStart\Sender\logs',
	r'D:\rep-s\fix\engine\samples\FIX_QuickStart\Listener\logs',
]

# killings
if cmd_args.e or cmd_args.ke:
	print 'Kill FIXEdge'
	kill_by_name('FIXServer-vc10-MDD-x64.exe')
	time.sleep(1)

if cmd_args.s or cmd_args.ks:
	print 'Kill Simple client'
	kill_by_name('SimpleClient.exe')
	time.sleep(1)


if cmd_args.cl:
	print 'Clearing logs'
	remove_all_files_in(LOG_PATHES)

if cmd_args.e:
	print 'Start FIXEdge'
	args = (r'D:\rep-s\FIXServer.5.5\ab_ci_win64_213.build\ControlCentre\src\Debug\FIXServer-vc10-MDD-x64.exe',
			r'-console', r'D:\aberezin\job_issues\BBP-2850_ssl2fixedge\FixEdge.properties'
	)
	env = {	'PATH':r'%PATH%;D:\rep-s\FIXServer.5.5\ab_ci_win64_213.build\3dparty-dlls\debug',
			'SystemRoot':r'C:\WINDOWS'
	}
	fe = subprocess.Popen(args, env=env)

if cmd_args.s:
	print 'Start Simple client'
	double_click(r'D:\aberezin\job\_simple_client\samples\SimpleClient\x64-Release\run.bat')

if cmd_args.cd:
	clear_raw_cap_dump()

if cmd_args.r:
	start_raw_cap()

if cmd_args.b:
	print 'Set clipboard'
	set_clipboard('8=FIX.4.49=17535=D49=FIXECHOCLIENT56=FIXEDGE34=252=20131125-09:37:09.68011=Order#121=1100=155=TESTSMBL54=160=20131124-04:11:46.76838=2000040=244=34.710=203')

if cmd_args.l:
	print 'Start listener'
	double_click(r'D:\rep-s\fix\engine\samples\FIX_QuickStart\Listener\runD.bat')

# wait ---------------------
#if cmd_args.e:
#	fe.wait()