import quickfix
import argparse
import time
import shutil
import ConfigParser
import threading

# helper functions
def _get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('cfg', help='Configuration file name')
    parser.add_argument('-cl', action="store_true", help="Clear logs on start")
    parser.add_argument('-d', action="store_true", help="Enable a debug output")
    parser.add_argument('-dd', action="store_true", help="Enable an extra debug output")

    return parser.parse_args()

def print_debug(d):
    if args.d:
        print d
def print_extra_debug(d):
    if args.dd:
        print d

# normalized quickfix session ID to pythonic form
def normSessionId(id):
    return id.toString().replace('.', '').replace(':', '_').replace('->', '_')

# Example of the customization of sent messages
class SendCallbacks:
    @classmethod
    def doCallback(cls, message, sessionID):
        clb = cls.__dict__.get(normSessionId(sessionID), None)
        if clb is not None:
            clb.__func__(cls, message)

    @classmethod
    def FIX44_SI_TI(cls, msg):
        cls.FIX44_SI_TI_cnt += 1
        msg.setField(quickfix.ClOrdID(str(cls.FIX44_SI_TI_cnt)))
    FIX44_SI_TI_cnt = 0

# quickfix application
class QFApplication(quickfix.Application):
    # TODO Lock for these dict-s
    estSessionID = {}   # IDs of established session {string:SessionId}
    allSessionID = {}   # IDs of all known session {string:SessionId}

    def onCreate(self, sessionID):
        QFApplication.allSessionID[sessionID.toString()] = sessionID
        print_debug('onCreate %s'%sessionID.toString())

    def onLogon(self, sessionID):
        QFApplication.estSessionID[sessionID.toString()] = sessionID
        print_debug('onLogon from %s (%d)'%(sessionID, len(QFApplication.estSessionID)))

    def onLogout(self, sessionID):
        QFApplication.estSessionID.pop(sessionID.toString(), None)
        print_debug('onLogout from %s (%d)'%(sessionID, len(QFApplication.estSessionID)))

    def toApp(self, message, sessionID):
        SendCallbacks.doCallback(message, sessionID)
        # here may be the common callback

    def toAdmin(self, message, sessionID):
        return
    def fromAdmin(self, message, sessionID):
        return
    def fromApp(self, message, sessionID):
        return

class MainApplication():
    def __init__(self, args):
        self.args = args
        self.log_dirs = set()   # dirs for cleaning
        self.workers = {}       # {sessionID_string:worker}

        # raw reading
        self.raw_read_cfg()

        # ConfigParser reading
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read(self.args.cfg)

        self.clear_logs()

    # getting of parameters which ConfigParser cannot get
    def raw_read_cfg(self):
        with open(self.args.cfg) as file_:
            for st in file_:
                try:
                    par, value = st.strip().split('=')
                except ValueError:
                    continue
                if par == 'FileLogPath' or par == 'FileStorePath':
                    self.log_dirs.add(value)

    def clear_logs(self):
        if self.args.cl:
            print_debug('Clearing of logs')
            for d in self.log_dirs:
                try:
                    shutil.rmtree(d)
                except OSError:
                    pass

    def run(self):
        application = QFApplication()
        settings = quickfix.SessionSettings(self.args.cfg)
        storeFactory = quickfix.FileStoreFactory(settings)
        logFactory = quickfix.FileLogFactory(settings)

        try:
            initiators = quickfix.SocketInitiator(application, storeFactory, settings, logFactory)
            initiators.start()
        except quickfix.ConfigError as e:
            pass

        try:
            acceptors = quickfix.SocketAcceptor(application, storeFactory, settings, logFactory)
            acceptors.start()
        except quickfix.ConfigError as e:
            pass
        # quickfix engine was started

        for str_sess_id, sess_id in QFApplication.allSessionID.iteritems():
            w = Worker(self.cfg, sess_id)
            self.workers[str_sess_id] = w
            w.start()

        # waiting
        while 1:
            try:
                time.sleep(0.5)
            except KeyboardInterrupt:
                print '---------------------------'
                print 'Application will be stopped'
                break

        for w in self.workers.itervalues():
            w.stop = True
        for w in self.workers.itervalues():
            w.join(3)

class Worker(threading.Thread):
    runPrintLock = threading.Lock()    # lock for debug on start

    def __init__(self, cfg, sessId):
        threading.Thread.__init__(self)
        self.cfg = cfg          # ref to main application
        self.sessId = sessId    # worker serve this session
        self.stop = False

    def run(self):
        msg = self.get_message_from_config()
        algo = self.create_algo()
        str_ID = self.sessId.toString()

        Worker.runPrintLock.acquire()
        print_debug('Thread is run for %s (%s)'%(self.sessId, algo.info()))
        Worker.runPrintLock.release()

        while not self.stop:
            #print_extra_debug('Loop for %s'%str_ID)
            if not QFApplication.estSessionID.has_key(str_ID):
                time.sleep(0.5)
                continue
            algo.execute_delay()
            #print_extra_debug('Send to %s: %s'%(str_ID, msg))
            quickfix.Session.sendToTarget(msg, self.sessId)

    def get_message_from_config(self):
        str_msg = self.cfg.get('CUSTOM', 'Message')
        soh_sym = self.cfg.get('CUSTOM', 'SohSymbol')
        return quickfix.Message(str_msg.replace(soh_sym, '\x01'))

    def create_algo(self):
        pref = '%s_'%normSessionId(self.sessId)
        if not self.cfg.has_option('CUSTOM', pref+'Algo'):
            pref = ''   # default algorithm

        mode = self.cfg.get('CUSTOM', pref+'Algo')
        if mode == 'ContinuousSend':
            return ContinuousAlgo(self.cfg, pref)
        elif mode == 'MonotonicSend':
            return MonotonicAlgo(self.cfg, pref)
        elif mode == 'BunchSend':
            return BunchAlgo(self.cfg, pref)
        elif mode == 'DynamicBunchSend':
            raise NotImplementedError()
        else:
            raise RuntimeError('Parameter Algo has an inadmissible value %s'%mode)

class BaseAlgo:
    def __init__(self):
        self.last_sleep = None
    def info(self):
        return
    def execute_delay(self):
        raise NotImplementedError()
    def _do_sleep(self):
        if self.last_sleep is None:
            delay = self.delay
        else:
            delay = self.delay - (time.time() - self.last_sleep)
        #print delay, self.delay, time.time(), self.last_sleep
        if delay > 0:
            time.sleep(delay)
        self.last_sleep = time.time()

class ContinuousAlgo(BaseAlgo):
    def __init__(self, cfg, pref):
        BaseAlgo.__init__(self)
    def info(self):
        return 'Continuous'
    def execute_delay(self):
        return

class MonotonicAlgo(BaseAlgo):
    def __init__(self, cfg, pref):
        BaseAlgo.__init__(self)
        msgPerSecFreq = float(cfg.get('CUSTOM', pref+'MsgPerSecFreq'))
        self.delay = 1.0/msgPerSecFreq
    def info(self):
        return 'Monotonic, %.3f'%self.delay
    def execute_delay(self):
        self._do_sleep()

class BunchAlgo(BaseAlgo):
    def __init__(self, cfg, pref):
        BaseAlgo.__init__(self)
        msgPerSecFreq = float(cfg.get('CUSTOM', pref+'MsgPerSecFreq'))
        self.delay = 1.0
        self.size = int(msgPerSecFreq)
        self.cnt = 0
    def info(self):
        return 'Bunch, %.3f, %d'%(self.delay, self.size)
    def execute_delay(self):
        if self.cnt == 0:
            self._do_sleep()
        self.cnt += 1
        if self.cnt >= self.size:
            self.cnt = 0

# main --------------------

args = _get_args()

app = MainApplication(args)
app.run()

