#coding=utf8

import socket
import time
import itertools
import struct
import sys

import dpkt

class MulticastSender:
	def __init__(self, mgroup, mport, if_ = None, ttl = 2):
		self.multicast_address = (mgroup, mport)

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

		if if_:	# sending interface, Linux only!
			ifreq = struct.pack('16sH14s', if_, socket.AF_INET, '\x00'*14)
			self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, ifreq)

	def send(self, data):
		self.sock.sendto(data, self.multicast_address)

class PcapReader:
	def __init__(self, pcap_file, packet_type = dpkt.ethernet.Ethernet):
		self.reader = dpkt.pcap.Reader(open(pcap_file, 'rb'))
		self.PacketType = packet_type

	def __iter__(self):
		for ts, buf in self.reader:
			pck = self.PacketType(buf)
			yield ts, pck

def dst_ip_is(packet, ip):
	try:
		return socket.inet_ntoa(packet.ip.dst) == ip
	except AttributeError:
		return False

def src_ip_is(packet, ip):
	try:
		return socket.inet_ntoa(packet.ip.src) == ip
	except AttributeError:
		return False

def dst_udp_port_is(packet, port):
	try:
		return packet.ip.udp.dport == port
	except AttributeError:
		return False

def src_udp_port_is(packet, ip):
	try:
		return packet.ip.udp.sport == port
	except AttributeError:
		return False


if __name__ == '__main__':
	incr_sender = MulticastSender('224.192.70.40', 40040)
	snapshot_sender = MulticastSender('224.192.70.41', 40041)

	instr_sender = MulticastSender('224.192.70.11', 40011)
	def instr_getter():
		for t,p in PcapReader(r'D:\aberezin\job_issues\bfix\BBP-5892_spectra_issues\inv_logs\Moex_Spectra_Fast.pcap'):
			if dst_udp_port_is(p, 36011) and dst_ip_is(p, '239.195.5.11'):
				yield t,p

	ig = instr_getter()
	for i in xrange(50):
		t2,p2 = ig.next()
		instr_sender.send(p2.ip.udp.data)

	cnt = 0	#sn1 = sn2 =
	for pck1 in PcapReader(r'D:\aberezin\job_issues\bfix\BBP-5892_spectra_issues\inv_logs\orders-log.pcap'):
		t,p = pck1

		cnt += 1
		#if 50 < cnt < 7700 or 7900 < cnt < 10000: continue
		#print cnt

		if dst_ip_is(p, '239.195.10.41') and dst_udp_port_is(p, 44041):
			snapshot_sender.send(p.ip.udp.data)

			#psn1,sn1 = sn1, struct.unpack('H', p.ip.udp.data[:2])[0]
			#if psn1+1 != sn1:
			#	print '1', cnt, psn1,sn1

		elif dst_ip_is(p, '239.195.10.40') and dst_udp_port_is(p, 44040):
			incr_sender.send(p.ip.udp.data)

		else:
			print '??',
		#print t
		time.sleep(0.01)



