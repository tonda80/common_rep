import quickfix

import time
import sys

fileName = "int.ini"


class MyApplication(quickfix.Application):
    sessionID = None
    def p(self, *a): print a
    def onCreate(self, sessionID): self.p(sys._getframe().f_code.co_name, sessionID)
    def onLogout(self, sessionID): self.p(sys._getframe().f_code.co_name, sessionID)
    def toAdmin(self, message, sessionID): self.p(sys._getframe().f_code.co_name, message, sessionID)
    def toApp(self, message, sessionID): self.p(sys._getframe().f_code.co_name, message, sessionID)
    def fromAdmin(self, message, sessionID): self.p(sys._getframe().f_code.co_name, message, sessionID)

    def onLogon(self, sessionID):
        self.p(sys._getframe().f_code.co_name, sessionID)
        self.sessionID = sessionID

    def fromApp(self, message, sessionID):
        self.p(sys._getframe().f_code.co_name, message, sessionID)


    def run(self, sess=None):
        time.sleep(1)
        while 1:
            raw_input("Next..")
            self.send_test_msg()

    def send_test_msg(self):
        msg = quickfix.Message()
        header = msg.getHeader();
        header.setField(quickfix.BeginString("FIX.4.2"))
        #header.setField(quickfix.SenderCompID(TW))
        #header.setField(quickfix.TargetCompID("TARGET"))
        header.setField(quickfix.MsgType("D"))
        msg.setField(quickfix.OrigClOrdID("123"))
        msg.setField(quickfix.ClOrdID("321"))
        msg.setField(quickfix.Symbol("LNUX"))
        #msg.setField(quickfix.Side(Side_BUY))
        msg.setField(quickfix.Text("Cancel My Order!"))
        quickfix.Session.sendToTarget(msg, self.sessionID)



settings = quickfix.SessionSettings(fileName)
application = MyApplication()
storeFactory = quickfix.FileStoreFactory(settings)
logFactory = quickfix.FileLogFactory(settings)
initiator = quickfix.SocketInitiator(application, storeFactory, settings, logFactory)
initiator.start()

application.run()
initiator.stop()
