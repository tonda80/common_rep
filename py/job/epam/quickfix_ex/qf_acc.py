import quickfix

import time
import sys

fileName = "acc.ini"


class MyApplication(quickfix.Application):
	def onCreate(self, sessionID): print sys._getframe().f_code.co_name
	def onLogon(self, sessionID): print sys._getframe().f_code.co_name
	def onLogout(self, sessionID): print sys._getframe().f_code.co_name
	def toAdmin(self, message, sessionID): print sys._getframe().f_code.co_name
	def toApp(self, message, sessionID): print sys._getframe().f_code.co_name
	def fromAdmin(self, message, sessionID): print sys._getframe().f_code.co_name
	def fromApp(self, message, sessionID):
		print 'fromApp %s %s'%(sessionID, message)

try:
	settings = quickfix.SessionSettings(fileName)
	application = MyApplication()
	storeFactory = quickfix.FileStoreFactory(settings)
	logFactory = quickfix.FileLogFactory(settings)
	acceptor = quickfix.SocketAcceptor(application, storeFactory, settings, logFactory)
	acceptor.start()
	while True:
		time.sleep(1)
	acceptor.stop()
except quickfix.ConfigError, e:
	print e