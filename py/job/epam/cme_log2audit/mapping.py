# coding=utf-8

# https://kb.epam.com/display/EPMBFIXA/Export+to+CME+audit+file

from message import Message

class Mapping:
	columns = (	'Server_Timestamp',
				'Receiving_Timestamp',
				'Message_Direction',
				'Operator_ID',
				'Self_Match_Prevention_ID',
				'Account_Number',
				'Session_ID',
				'Execution_Firm_ID',
				'Manual_Order_Identifier',
				'Message_Type',
				'Customer_Type_Indicator',
				'Origin',
				'CME_Globex_Message_ID',
				'Message_Link_ID',
				'Order_Flow_ID',
				'Spread__Leg_Link_ID',
				'Instrument_Description',
				'Market_Segment_ID',
				'Client_Order_ID',
				'CME_Globex_Oreder_ID',
				'Buy__Sell_Indicator',
				'Quantity',
				'Limit_Price',
				'Stop_Price',
				'Order_Type',
				'Order_Qualifier',
				'IFM_Flag',
				'Display_Quantity',
				'Minimum_Quantity',
				'Country_of_Origin',
				'Fill_Price',
				'Fill_Quantity',
				'Cumulative_Quantity',
				'Remaining_Quantity',
				'Aggressor_Flag',
				'Source_of_Cancellation',
				'Reject_Reason',
				'Processed_Quotes',
				'Cross_ID',
				'Quote_Request_ID',
				'Message_Quote_ID',
				'Quote_Entry_ID',
				'Bid_Price',
				'Bid_Size',
				'Offer_Price',
				'Offer_Size'
				)

	def Server_Timestamp(self, msg):
		return msg.get_field(52)

	def Receiving_Timestamp(self, msg):
		return msg.get_field(60)

	def Message_Direction(self, msg):
		if msg.type in 'DFG':
			return 'TO CME'
		if msg.type in '893j':
			return 'FROM CME'

	def Operator_ID(self, msg):
		if msg.type in 'DFG':
			return msg.get_field(50)
		if msg.type in '893j':
			return msg.get_field(57)
		return '?'

	def Self_Match_Prevention_ID(self, msg):
		return msg.get_field(7928)

	def Account_Number(self, msg):
		return msg.get_field(1)

	def Session_ID(self, msg):
		if msg.type in 'DFG':
			return msg.get_field(49)
		if msg.type in '893j':
			return msg.get_field(56)

	def Execution_Firm_ID(self, msg):
		ret = ''
		if msg.type in 'DFG':
			ret =  msg.get_field(49)
		if msg.type in '893j':
			ret =  msg.get_field(56)
		return ret[4:7]

	def Manual_Order_Identifier(self, msg):
		return msg.get_field(1028)

	def Message_Type(self, msg):
		if msg.type in 'DFG':
			return msg.type
		if msg.type == '8':
			return concat('8/', msg.get_field(39))
		if msg.type == '9':
			return concat('9/', msg.get_field(434))

	def Customer_Type_Indicator(self, msg):
		if msg.type in 'DG':
			return msg.get_field(9702)

	def Origin(self, msg):
		if msg.type in 'DG':
			return msg.get_field(204)

	def CME_Globex_Message_ID(self, msg):
		if msg.type == '8':
			return msg.get_field(17)

	def Message_Link_ID(self, msg):
		return self.generate_message_id()

	def Order_Flow_ID(self, msg):
		id_ = None
		if msg.type == 'D':
			tag_11 = msg.get_field(11)
			if tag_11 in self.dct_11.iterkeys():
				self.curr_errs.append('Non-unique ClOrdID')
			id_ = self.generate_flow_id()
			self.dct_9717[msg.get_field(9717)] = id_
			self.dct_11[tag_11] = id_
		elif msg.type == 'G' or msg.type == 'F':
			tag_9717 = msg.get_field(9717)
			if tag_9717 is None:
				id_ = self.dct_11.get(msg.get_field(41))
				self.dct_11[msg.get_field(11)] = ret
			else:
				id_ = self.dct_9717.get(tag_9717)
		elif msg.type == '8' or msg.type == '9':
			tag_9717 = msg.get_field(9717)
			if tag_9717 is None:
				id_ = self.dct_11.get(msg.get_field(11))
			else:
				id_ = self.dct_9717.get(tag_9717)

		if id_ == None and msg.type in 'GF89':
			self.curr_errs.append('Cannot detect flow')
		return id_

	def Spread__Leg_Link_ID(self, msg):
		if msg.type == '8':
			return msg.get_field(527)

	def Instrument_Description(self, msg):
		return msg.get_field(107)

	def Market_Segment_ID(self, msg):
		if msg.type in 'DGF89':
			return msg.get_field(1300)
		if msg.type in '3j':
			return msg.get_field(50)

	def Client_Order_ID(self, msg):
		return msg.get_field(11)

	def CME_Globex_Oreder_ID(self, msg):
		if msg.type in 'GF89':
			return msg.get_field(37)

	def Buy__Sell_Indicator(self, msg):
		msg.get_field(54)

	def Quantity(self, msg):
		msg.get_field(38)

	def Limit_Price(self, msg):
		tag40 = msg.get_field(40)
		if tag40 == '2' or tag40 == '4':
			return msg.get_field(44)

	def Stop_Price(self, msg):
		tag40 = msg.get_field(40)
		if tag40 == '3' or tag40 == '4':
			return msg.get_field(99)

	def Order_Type(self, msg):
		return msg.get_field(40)

	def Order_Qualifier(self, msg):
		return msg.get_field(59)

	def IFM_Flag(self, msg):
		if msg.type in 'G8':
			return msg.get_field(9768)

	def Display_Quantity(self, msg):
		return msg.get_field(210)

	def Minimum_Quantity(self, msg):
		return msg.get_field(110)

	def Country_of_Origin(self, msg):
		if msg.type in 'DFG':
			return msg.get_field(142)

	def Fill_Price(self, msg):
		if msg.type == '8' and msg.get_field(150) == 'F':
			return msg.get_field(31)

	def Fill_Quantity(self, msg):
		if msg.type == '8' and msg.get_field(150) == 'F':
			return msg.get_field(32)

	def Cumulative_Quantity(self, msg):
		if msg.type == '8':
			return msg.get_field(14)

	def Remaining_Quantity(self, msg):
		if msg.type == '8':
			return msg.get_field(151)

	def Aggressor_Flag(self, msg):
		if msg.type == '8':
			return msg.get_field(1057)

	def Source_of_Cancellation(self, msg):
		if msg.type == '8' and msg.get_field(150) == '4' and msg.get_field(39) == '4':
			return msg.get_field(378)

	def Reject_Reason(self, msg):
		if msg.type == '8' and msg.get_field(150) == '8':
			return concat(msg.get_field(103), '/', msg.get_field(58))
		if msg.type == '9':
			return concat(msg.get_field(102), '/', msg.get_field(58))

	def Processed_Quotes(self, msg):
		return ''
	def Cross_ID(self, msg):
		return ''
	def Quote_Request_ID(self, msg):
		return ''
	def Message_Quote_ID(self, msg):
		return ''
	def Quote_Entry_ID(self, msg):
		return ''
	def Bid_Price(self, msg):
		return ''
	def Bid_Size(self, msg):
		return ''
	def Offer_Price(self, msg):
		return ''
	def Offer_Size(self, msg):
		return ''

	# ---------------------------- common ----------------------------

	def get_row(self, msg):
		res = []
		self.curr_errs = []
		for c in Mapping.columns:
			try:
				method = Mapping.__dict__[c]
			except KeyError:
				raise NotImplementedError('Mapping.%s method not found'%c)
			res.append(method(self, msg))
		return (res, self.curr_errs)

	def get_column_names(self):
		return map(lambda c: c.replace('__', '/').replace('_', ' '), Mapping.columns)

	def __init__(self):
		self.msg_id_counter = 0
		self.flow_id_counter = 0

		self.dct_9717 = {}
		self.dct_11 = {}

		self.curr_errs = []

	def generate_message_id(self):
		self.msg_id_counter += 1
		return 'mid%d'%self.msg_id_counter

	def generate_flow_id(self):
		self.flow_id_counter += 1
		return 'fid%d'%self.flow_id_counter

# ---------------------------- helpers ----------------------------

def concat(*args):
	res = ''
	for a in args:
		if a is not None:
			res += a
	return res

# ---------------------------- check of implementation ----------------------------

implementations = filter(lambda l: l[0].isupper(), Mapping.__dict__.keys())
if len(Mapping.columns) != len(implementations):
	raise NotImplementedError('Error of implementation of Mapping class')
for c in Mapping.columns:
	if c not in implementations:
		raise NotImplementedError('Implementation of %s column is absent'%c)
