﻿# coding=utf-8

from mapping import Mapping
from message import Message

import csv
import glob
import argparse

from time import time
from os import stat

class MessageSplitter:
	def __init__(self, in_file):
		self.file_ = open(in_file, 'rb')
		self.buffer = bytearray()

	def __iter__(self):
		return self

	def next_chunk(self, q_req):
		while True:
			chunk = self.file_.read(q_req)
			if chunk == '':
				raise StopIteration
			self.buffer.extend(chunk)
			q_req -= len(chunk)
			if q_req <= 0:
				break

	def wait(self, mark):
		cnt = 0
		while True:
			self.next_chunk(1)
			cnt += 1
			if self.buffer.endswith(mark):
				return cnt

	def find_message_begin(self):
		del self.buffer[:]
		while True:
			# search of a message start
			self.wait('8=')
			try:
				before_sym = self.buffer[-3]
			except IndexError:		# buffer begins from "8="
				return
			if chr(before_sym).isalnum():
				continue
			# else we have in buffer "xxxxxx 8="
			del self.buffer[:-2]
			return

	# TODO
	def error_log(self, what):
		print what, self.buffer

	def test_message_end(self):
		if self.buffer[-8:-4] != '\x0110=' or self.buffer[-1] != 1:
			self.error_log('test_message_end error')
			return False
		return True

	def next(self):
		while True:
			self.find_message_begin()	# Now self.buffer == "8="
			self.wait('\x019=')
			q = self.wait('\x01')
			tag9 = int(self.buffer[-q:-1])
			self.next_chunk(tag9 + 7)	# message + 10=XXX<SOH>
			if self.test_message_end():
				return self.buffer

class CsvMapper(Mapping):
	def __init__(self, file_name):
		Mapping.__init__(self)
		file_ = open(file_name, 'wb')
		self.csv = csv.writer(file_)
		self._add_header()
		self.err_file = open(file_name+'.err.txt', 'w')
		self.deb_file = open(file_name+'.deb.txt', 'wb')

	def _add_header(self):
		self.csv.writerow(self.get_column_names())

	def add_row(self, msg):
		row, errs = self.get_row(msg)
		if row:
			self.csv.writerow(row)
			self.deb_file.write(msg.body); self.deb_file.write('\n')
			if errs:
				self.err_file.write(msg.body); self.err_file.write('\n')
				for e in errs:
					self.err_file.write('\t'); self.err_file.write(e); self.err_file.write('\n')

class MessageReader:
	def __iter__(self):
		return self

	def __init__(self, file_patterns, begin_time = None, end_time = None):
		if isinstance(file_patterns, str):
			file_patterns = map(lambda s: s.strip(), file_patterns.split(','))

		self.file_names = []
		for fp in file_patterns:
			self.file_names.extend(glob.glob(fp))

		if not self.file_names:
			raise RuntimeError('There are no files to appropriate patterns %s'%file_patterns)

		self.msg_splitters = [MessageSplitter(f) for f in self.file_names]
		self.msq_queue = []
		self.msg_counter_total = 0
		self.msg_counter = 0

		Message.begin_time = begin_time
		Message.end_time = end_time

	def getMessageFromSplitter(self, splitter):
		try:
			while 1:
				msg = Message(splitter.next())
				self.msg_counter_total += 1
				if msg.is_needed:
					self.msg_counter += 1
					self.msq_queue.append( (msg, splitter) )
					self.msq_queue.sort(key = lambda x:x[0].time)
					break
		except StopIteration:
			pass  #self.msg_splitters.remove(splitter)

	def giveMessage(self):
		try:
			msg, splitter = self.msq_queue.pop(0)
		except IndexError:
			raise StopIteration
		self.getMessageFromSplitter(splitter)
		return msg

	def next(self):
		for s in self.msg_splitters:
			self.getMessageFromSplitter(s)
		while True:
			return self.giveMessage()

def get_args():
	parser = argparse.ArgumentParser()

	parser.add_argument('--begin', help='Begin time. A string in the log format. If omitted then there is no begin limit.')
	parser.add_argument('--end', help='End time. A string in the log format. If omitted then there is no end limit.')
	parser.add_argument('--input', help='Comma separated list of input files patterns. Default is "*.in,*.out".', default='*.in,*.out')
	parser.add_argument('--output', help='Output file name. Default is "out.csv".', default='out.csv')
	parser.add_argument('-p', action='store_true', help='Print statistic. False is default.')

	return parser.parse_args()

# ---- main ----
args = get_args()

reader = MessageReader(args.input, args.begin, args.end)
csv_writter = CsvMapper(args.output)

t0 = time()

for msg in reader:
	csv_writter.add_row(msg)

with open(args.output+'.done', 'w') as f:
	f.write('RECORD_COUNT=%d\n'%reader.msg_counter)

# ---- Print statistic ----
if args.p:
	t = time() - t0
	sz_k = 0
	for f in reader.file_names:
		sz_k += stat(f).st_size/1024

	print 'Input files: %s'%reader.file_names
	print 'Time %.3f; rate %.3fkb/s; messages: handled %d, total %d; size of input files %d kb'%(t, sz_k/t, reader.msg_counter, reader.msg_counter_total, sz_k)

