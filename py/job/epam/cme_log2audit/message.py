# coding=utf-8

class Message:
	begin_time = None
	end_time = None

	def __init__(self, str_msg):
		self.time = str(self.__get_field(str_msg, 52))
		self.type = str(self.__get_field(str_msg, 35))
		self.is_needed = self.__is_needed()

		if self.is_needed:
			self.body = str(str_msg)
			self.fields = {35:self.type, 52:self.time}

	def __is_needed(self):
		return (
			(self.type in 'DFG893j') and
			(Message.begin_time is None or self.time >= Message.begin_time) and
			(Message.end_time is None or self.time <= Message.end_time)
			)

	def __get_field(self, str_msg, n):
		mark0 = '\x01%d='%n
		i0 = str_msg.find(mark0)
		if i0 < 0:
			return None
		i0 += len(mark0)
		i1 = str_msg.find('\x01', i0)
		if i1 < 0:
			return None
		return str_msg[i0:i1]

	def get_field(self, n):
		if n in self.fields:
			return self.fields[n]
		f = self.__get_field(self.body, n)
		self.fields[n] = f
		return f