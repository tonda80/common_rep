#!/usr/bin/python

import os
import subprocess
import psutil
import signal
import time
import datetime
import argparse

try:
	import win32clipboard
except ImportError:
	pass

import lsh

def remove_all_files_in(pathes, skipFltr = lambda x: False):
	if isinstance(pathes, str):
		pathes = (pathes, )
	for p in pathes:
		for r, d, f in os.walk(p):	# root, dirs, files
			for e in f:
				if not skipFltr(e):
					lsh.guaranteed_rm(os.path.join(r, e))

def double_click(path):
	dir_name, file_name = os.path.split(path)
	if dir_name:
		mem_dir = os.getcwd()
		os.chdir(dir_name)
	os.startfile(file_name)
	if dir_name:
		os.chdir(mem_dir)

def set_clipboard(text):
	"One thread only!"
	from Tkinter import Tk
	r = Tk()
	r.withdraw()
	r.clipboard_clear()
	r.clipboard_append(text)
	r.destroy()

def win_set_cb(text):
	"Obviously win only clipboard setting"
	win32clipboard.OpenClipboard()
	win32clipboard.EmptyClipboard()
	win32clipboard.SetClipboardText(text)
	win32clipboard.CloseClipboard()

def win_get_cb():
	"Obviously win only clipboard getting"
	win32clipboard.OpenClipboard()
	res = win32clipboard.GetClipboardData()
	win32clipboard.CloseClipboard()
	return res

def hexstream2str():
	# source is "wireshark - copy - bytes - hex stream"
	stream = win_get_cb()
	res = ''.join(map(chr, map(lambda s: int(s,16), (stream[i:i+2] for i in xrange(0, len(stream), 2)))))
	win_set_cb("'"+res+"'")

def start_raw_cap(rawcap = r'D:\aberezin\programs\rawcap\RawCap.exe', loopback_num = '2', dump_file = r'D:\aberezin\temp\localhost.pcap'):
	"Capturing of loopback to file"
	args = (rawcap, '-f', loopback_num, dump_file)
	subprocess.Popen(args, shell=True)

def clear_raw_cap_dump(dump_file = r'D:\aberezin\temp\localhost.pcap'):
	while 1:
		try: open(dump_file, 'w')
		except (OSError, IOError) as e: raw_input(e)
		else: break

def create_log_dirs(pth=''):
	log = os.path.join(pth, 'log')
	backup = os.path.join(pth, 'log', 'backup')
	for d in (log, backup):
		if not lsh.test_dir(d):
			lsh.mkdir(d)

def kill_delay(proc, timeout = 2, sgn = signal.SIGTERM):
	if lsh.killall(proc, sgn) > 0:
		time.sleep(timeout)

def start_echo_server(engine_properties = '', clean_logs = True, kill_ = True):
	bin_path = r'D:\rep-s\fix\engine\samples\EchoServer\bin'
	bin_name = 'EchoServerD.exe'
	bin_full_name = os.path.join(bin_path, bin_name)

	args = ('start', bin_full_name, engine_properties)
	env = {'PATH': os.path.join(bin_path, r'..\..\..\lib')}
	if kill_:
		kill_delay(bin_name)
	if clean_logs:
		remove_all_files_in((os.path.join(bin_path, r'logs'),))
	return lsh.run_bg(args, env=env, shell=True)

def start_simple_client(clean_logs = True, kill_ = True):
	bin_path = 	r'D:\aberezin\job\_simple_client\samples\SimpleClient\x64-Release'
	bin_name = 'SimpleClient.exe'
	bin_full_name = os.path.join(bin_path, bin_name)

	args = (bin_full_name, )
	env = {'PATH': os.path.join(bin_path, r'..\..\..\lib')}
	if kill_:
		kill_delay(bin_name)
	if clean_logs:
		remove_all_files_in((os.path.join(bin_path, r'logs'),))
	return lsh.run_bg(args, env=env, cwd = bin_path)

def start_fixedge(conf_dir, clean_logs = True, kill_ = True, deb = True, bin_ = None, start_type = 0):
	if bin_:
		bin_full_name = bin_
	else:
		if deb:
			bin_full_name = r'D:\rep-s\FIXServer.5.5\ab_ci_win64_fix.build\ControlCentre\src\Debug\FIXServer-vc10-MDD-x64.exe'
		else:
			bin_full_name = r'D:\rep-s\FIXServer.5.5\ab_ci_win64_fix.build\ControlCentre\src\Release\FIXServer-vc10-MD-x64.exe'
	bin_path, bin_name = os.path.split(bin_full_name)
	lib_path = r'D:\rep-s\FIXServer.5.5\ab_ci_win64_fix.build\3dparty-dlls\debug;D:\rep-s\FIXServer.5.5\ab_ci_win64_fix.build\3dparty-dlls\release'

	logs = os.path.join(conf_dir, 'log')

	if kill_:
		kill_delay(bin_name)
	if clean_logs:
		remove_all_files_in((logs,))

	fe_properties = os.path.join(conf_dir, 'conf/FIXEdge.properties')
	args = ('start', bin_full_name, '-console', fe_properties)
	env = {'PATH': lib_path}

	if start_type == 0:		# usual start
		return lsh.run_bg(args, env=env, shell=True, cwd = conf_dir)
	elif start_type == 1:		# VS start
		return 0
	raise RuntimeError('Unknown start_type %d'%start_type)

def start(bin_path, args=(), kill_ = True):
	path_, bin_ = os.path.split(bin_path)
	if kill_:
		kill_delay(bin_)
	return lsh.run_bg((bin_path,) + args, cwd = path_)

def start_cmd(bin_path, args=(), kill_ = True):
	path_, bin_ = os.path.split(bin_path)
	if kill_:
		kill_delay(bin_)
	run_args = ('start', bin_path) + args
	return lsh.run_bg(run_args, shell=True, cwd = path_)

class LogTime:
	known_time_templates = ('%Y%m%d-%H:%M:%S', '%Y-%m-%d %H:%M:%S')

	def __init__(self, time_str, templ = None, fract_sep = '.'):
		usec = 0
		if time_str.count(fract_sep) == 1:
			time_str, usec_str = time_str.split(fract_sep)
			usec = int(usec_str) * 10**(6 - len(usec_str))

		if templ:
			tm = time.strptime(time_str, templ)
			self.datetime = datetime.datetime(tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, usec)
			self.time_template = templ
		else:	# try to find
			for t in LogTime.known_time_templates:
				try:
					tm = time.strptime(time_str, t)
					self.datetime = datetime.datetime(tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, usec)
				except ValueError:
					continue
				self.time_template = t
				return
			raise RuntimeError('Unknown time format')

def str_time_delta(t0, t1):
	lt0 = LogTime(t0)
	lt1 = LogTime(t1, lt0.time_template)
	return lt1.datetime - lt0.datetime

class Args:
	'A wrapper for argparse'
	def __init__(self):
		self.parser = argparse.ArgumentParser()
		self.args = None

	def add_bool(self, name, help = '', action='store_true'):
		self.parser.add_argument(name, action=action, help=help)

	def __getattr__(self, name):
		if self.args is None:
			self.args = self.parser.parse_args()
		return getattr(self.args, name)



if __name__ == '__main__':
	pass
