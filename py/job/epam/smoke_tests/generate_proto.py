# coding=utf8

import subprocess
import glob
import os.path

import settings

def generate_py_protobuf(protoc, src_dir, dst_dir='.'):
	protoc = os.path.normpath(protoc)
	src_dir = os.path.normpath(src_dir)
	dst_dir = os.path.normpath(dst_dir)
	for p in (src_dir, dst_dir):
		if not os.path.isdir(p):
			print 'Directory %s [%s] is not exist.\nExiting.'%(p, os.path.abspath(p))
			return
	if not (os.path.isfile(protoc) or os.path.isfile(protoc+'.exe')):
		print 'File %s [%s] is not exist.\nExiting.'%(protoc, os.path.abspath(protoc))
		return

	for in_proto_file in glob.glob(os.path.join(src_dir,'*.proto')):
		print in_proto_file,
		cmd = '{} -I={} --python_out={} {}'.format(protoc, src_dir, dst_dir, in_proto_file)
		res = subprocess.call(cmd.split())
		if res == 0:
			print '\t\t=> Done.'
		else:
			print '\t\t=> Error %d.'%res

if __name__ == '__main__':
	generate_py_protobuf(settings.Protobuf.Protoc, settings.Protobuf.SrcDir, settings.Protobuf.DstDir)