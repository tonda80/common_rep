# coding=utf8

import json
import sys

try:
	import MessageHeaders_pb2
	import MessageType_pb2
	import MessageDictionary_pb2
except ImportError:
	print 'Cannot import python protobuf files. Make "python generate_proto.py".\nExiting.'
	sys.exit(1)


def defaultNewOrderSingle(**fields):
	fields.setdefault('InstrumentID', 1)
	#fields.setdefault('StopPx', '1')
	fields.setdefault('OrderType', 2)
	fields.setdefault('OrderQty', 1)
	#fields.setdefault('TrailingStop', 1)
	#fields.setdefault('TrailingStopType', 1)
	fields.setdefault('OrderClass', 0)
	fields.setdefault('Side', 1)
	fields.setdefault('AccountID', 2)
	fields.setdefault('QuoteType', 1)
	fields.setdefault('TimeInForce', 6)
	fields.setdefault('ExpireTime', 1000000000000000)
	fields.setdefault('TransactTime', 1)
	fields.setdefault('ClOrdID', '1')
	if fields['OrderType'] == 2:
		fields.setdefault('Price', '1')
	if fields['OrderType'] == 1:
		fields['TimeInForce'] = 0
	return fields

def uiNewOrderSingle(fields=None):
	if not fields:
		fields = defaultNewOrderSingle()
	return json.dumps({'NewOrderSingle' : fields})


ProtoMapping = {
	MessageType_pb2.NEW_ORDER_SINGLE : MessageDictionary_pb2.NewOrderSingle,
	MessageType_pb2.L1_UPDATE : MessageDictionary_pb2.L1Update,
    MessageType_pb2.L2_UPDATE : MessageDictionary_pb2.L2Update,
    MessageType_pb2.TICKER : MessageDictionary_pb2.Ticker,
    MessageType_pb2.TRADE : MessageDictionary_pb2.Trade,
	MessageType_pb2.ORDER_CANCEL_REQUEST : MessageDictionary_pb2.OrderCancelRequest,
    MessageType_pb2.ORDER_CANCEL_REPLACE_REQUEST : MessageDictionary_pb2.OrderCancelReplaceRequest,
    MessageType_pb2.BUSINESS_MESSAGE_REJECT : MessageDictionary_pb2.BusinessMessageReject,
    MessageType_pb2.EXECUTION_REPORT : MessageDictionary_pb2.ExecutionReport,
	MessageType_pb2.POSITION_INFO : MessageDictionary_pb2.PositionInfo,
    MessageType_pb2.POSITION_DETAIL_INFO : MessageDictionary_pb2.PositionDetailInfo,
	MessageType_pb2.USER_INFO : MessageDictionary_pb2.UserInfo,
	MessageType_pb2.ACCOUNT_INFO : MessageDictionary_pb2.AccountInfo,

}
