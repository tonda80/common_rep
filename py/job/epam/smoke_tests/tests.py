# coding=utf8

import unittest
import logging

from core import *
from messages import *
import settings

log = logging.getLogger('tests')
log.setLevel(settings.DebugLevel)


class JavaTest(unittest.TestCase):
	@classmethod
	def setUpClass(self):
		Global.flag_work = True
		self.ui = WebSocketClient(settings.FDS.WebSocketAddress, settings.FDS.Username, settings.FDS.Password)
		self.fds = ZmqApp(settings.FDS)
		sleep(settings.Timeout.Common)

	@classmethod
	def tearDownClass(self):
		Global.flag_work = False
		sleep(settings.Timeout.Common)

	def setUp(self):
		self.fds.clear_all()

	def test_instruments(self):
		self.ui.subscribe_instruments()
		sleep(settings.Timeout.RespDelay)

		self.assertTrue(len(self.ui.instruments) > 0)

	#def test_(self):

class CppTest(unittest.TestCase):
	@classmethod
	def setUpClass(self):
		Global.flag_work = True
		self.fds = ZmqApp(settings.FDS)
		self.vrcs = ZmqApp(settings.VRCS)
		self.omms = ZmqApp(settings.OMMS)
		self.mds = ZmqApp(settings.MDS)
		self.pms = ZmqApp(settings.PMS)

		self.ui = WebSocketClient(settings.FDS.WebSocketAddress, settings.FDS.Username, settings.FDS.Password)
		if len(self.ui.accounts) < 2:
			TtrtError('User should have 2 accounts cannot get accounts')

		sleep(settings.Timeout.Common)

	@classmethod
	def tearDownClass(self):
		Global.flag_work = False
		sleep(settings.Timeout.Common)

	def setUp(self):
		self.fds.clear_all()
		self.vrcs.clear_all()
		self.omms.clear_all()
		self.mds.clear_all()
		self.pms.clear_all()

	def print_all_updates(self):
		for c in (self.fds, self.vrcs, self.omms, self.mds, self.pms):
			c.print_all_updates(log.debug)

	#def tearDown(self):

	def test_matching(self):
		log.debug('\ntest_matching')

		acc1 = self.ui.accounts[0]
		acc2 = self.ui.accounts[1]
		instr = settings.MainInstrument
		clOrdId1 = 'zzz'
		clOrdId2 = 'yyy'

		newOrderSingle1 = defaultNewOrderSingle(InstrumentID=instr, AccountID=acc1, Side=1, ClOrdID=clOrdId1, OrderQty=100000)
		newOrderSingle2 = defaultNewOrderSingle(InstrumentID=instr, AccountID=acc2, Side=2, ClOrdID=clOrdId2, OrderQty=100000)

		self.ui.send('/newOrderSingle', uiNewOrderSingle(newOrderSingle1))
		self.ui.send('/newOrderSingle', uiNewOrderSingle(newOrderSingle2))

		sleep(settings.Timeout.RespDelay)
		self.print_all_updates()
		# ---------------------

		self.assertFalse(check_update_list(self.fds.take_updates('A.D'), [
			TestUpdate(10, InstrumentID=instr, AccountID=acc1, Side=1, ClOrdID=clOrdId1),
			TestUpdate(10, InstrumentID=instr, AccountID=acc2, Side=2, ClOrdID=clOrdId2),
		]))

		self.assertFalse(check_update_list(self.vrcs.take_updates('I.D'), [
			TestUpdate(10, InstrumentID=instr, AccountID=acc1, Side=1, ClOrdID=clOrdId1),
			TestUpdate(10, InstrumentID=instr, AccountID=acc2, Side=2, ClOrdID=clOrdId2),
		]))

		self.assertFalse(check_update_list(self.omms.take_updates('I.G'), [
			TestUpdate(14, InstrumentID=instr, AccountID=acc1, Side=1, ClOrdID=clOrdId1, ExecType=7),
			TestUpdate(14, InstrumentID=instr, AccountID=acc1, Side=1, ExecType=13),
		]))

		self.assertFalse(check_update_list(self.mds.take_updates('I.F'), [
			TestUpdate(1, InstrumentID=instr)
		]))

		self.assertFalse(check_update_list(self.pms.take_updates('I.Position'), [
			TestUpdate(500, InstrumentID=instr)
		]))

	def test_reject(self):
		log.debug('\ntest_reject')

		acc = self.ui.accounts[0]
		instr = settings.MainInstrument

		newOrderSingle1 = defaultNewOrderSingle(InstrumentID=instr, AccountID=acc, Side=1)
		newOrderSingle2 = defaultNewOrderSingle(InstrumentID=instr, AccountID=acc, Side=2)

		self.ui.send('/newOrderSingle', uiNewOrderSingle(newOrderSingle1))
		self.ui.send('/newOrderSingle', uiNewOrderSingle(newOrderSingle2))

		sleep(settings.Timeout.RespDelay)
		self.print_all_updates()
		# ---------------------

		self.assertFalse(check_update_list(self.vrcs.take_updates('I.D'), [
			TestUpdate(10, InstrumentID=instr, AccountID=acc, Side=1),
		]))
		self.assertFalse(check_update_list(self.vrcs.take_updates('I.GRCV'), [
			TestUpdate(14, InstrumentID=instr, AccountID=acc, Side=2, RejectCode=32),
		]))

	def test_clear_instrument(self):
		res = clear_instrument(settings.MainInstrument)

		self.print_all_updates()
		self.assertTrue(res)


if __name__ == '__main__':
	try:
		unittest.main()
	except:
		Global.flag_work = False
		raise
	Global.flag_work = False
