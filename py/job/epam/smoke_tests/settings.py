# coding=utf8

# CRITICAL 50, ERROR 40, WARNING 30, INFO 20, DEBUG 10
DebugLevel = 10

# the mark of any value of a field
Any = '***'

MainInstrument = 16

class Timeout:
	Common = 0.3
	RecvThread = 0.5
	RespDelay = 2

class Protobuf:
	Protoc = '../../../../3rd/build/debug/Protobuf/bin/protoc'
	SrcDir = '../../../data-model-all/src/main/proto'
	DstDir = '.'

class FDS:
	WebSocketAddress = 'ws://10.17.131.24/fds/ws'
	Username = 'CPP_PARTICIPANT_2'	# TEST_PARTICIPANT_1 CPP_PARTICIPANT_2
	Password = Username
	AuxUsername = 'CPP_PARTICIPANT_6'
	AuxPassword = AuxUsername

	# name : [subsription endpoint]
	PubTopics = {
		'A.D' : ['tcp://10.17.131.24:5555'],
	}

class VRCS:
	# name : [subsription endpoint]
	PubTopics = {
		'I.GRCV' : ['tcp://10.17.128.180:5558'],
		'I.D' : ['tcp://10.17.128.180:5559']
	}

class OMMS:
	# name : [subsription endpoint]
	PubTopics = {
		'I.G' : ['tcp://10.17.130.255:5557'],
	}

class MDS:
	# name : [subsription endpoint]
	PubTopics = {
		'I.F' : ['tcp://10.17.130.255:7777'],
		'I.E' : ['tcp://10.17.130.255:7778'],
		'I.J' : ['tcp://10.17.130.255:7779'],
	}

class PMS:
	# name : [subsription endpoint]
	PubTopics = {
		'I.Position' : ['tcp://10.17.128.180:8888'],
		'I.PositionDetails' : ['tcp://10.17.128.180:8889'],
	}

class UAMS:
	PubTopics = {
		'G.UserInfo' : ['tcp://10.17.131.24:35322'],
	}
