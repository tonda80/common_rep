Preparation for run
----------------------
1. Install python 2.7 https://www.python.org/downloads/
2. Install pip https://pip.pypa.io/en/stable/installing/
3. Make commands (requires sudo for linux):
	pip install zmq
	pip install websocket-client
	pip install protobuf

Structure of package
----------------------
1. core.py - a base of package
2. messages.py - a file with definions os used messages
3. settings.py - a set of settings for tests and environment
4. tests.py - a set of work test cases
5. internal_tests.py - a set of test cases for internal needs

Generation of protobuf files
----------------------
It's necessary to generate actual python protobuf files (these files describe a format our system messages). To get protoc utility you may e.g. here https://developers.google.com/protocol-buffers/docs/downloads
Protobuf source files are into repository git@git.epam.com:ttrt-fxts/data-model-all.git. Select needed version of dictionary from master branch.
Change Protobuf settings into settings.py (pathes to protoc and source proto directory) and make "python generate_proto.py". Files kind of "*_pb2.py" should be into script's directory. If it will not be so, we will a demand to generate them.

Way of using
----------------------
test.py contains some sets of tests (CppTest, JavaTest)
In order that to run all test cases, make "python tests.py".
In order that to run all test cases of a set (e.g. CppTest), make "python tests.py CppTest".
In order that to run only one test case (e.g. test_matching from CppTest), make "python tests.py CppTest.test_matching".
In order that to add a new test case (e.g. into CppTest set). Find class CppTest and add a method test_SOME_NEW_CASE() below according to existing examples.

In order that to redirect script output into a file (e.g. file.log) add to end of the command " >> file.log 2>&1" or " > file.log 2>&1". First form will append output into the file, second one will create the new file. Full example of the run command "python tests.py CppTest.test_matching > file.log 2>&1"
