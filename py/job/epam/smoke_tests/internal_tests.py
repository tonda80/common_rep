# coding=utf8

import unittest
import logging

from core import *
import messages
import settings

log = logging.getLogger('internal_tests')
log.setLevel(settings.DebugLevel)

class DebugTest(unittest.TestCase):
	def test_uams(self):
		uams = ZmqApp(settings.UAMS)
		while 1:
			uams.print_all_updates(log.info)
			uams.clear_all()
			sleep(1)

	def test_accountInfo(self):
		cns = ZmqConsumer('G.AccountInfo', 'tcp://10.17.131.24:35322')
		while 1:
			cns.print_updates(log.info, 'AccounInfo')
			cns.clear_updates()
			sleep(1)

	def test_l1l2(self):
		ui = WebSocketClient(settings.FDS.WebSocketAddress, settings.FDS.Username, settings.FDS.Password)
		instr = 16

		ui.subscribe_l1(instr, lambda x,y:log.debug('%s %s', x,y))
		ui.subscribe_l2(instr, lambda x,y:log.debug('%s %s', x,y))
		sleep(settings.Timeout.RespDelay)



class InternalTest(unittest.TestCase):
	def test_check_update(self):
		upd = TestUpdate(1, f1=1, f2=2, f3='3')

		self.assertFalse(check_update(upd, TestUpdate(1, f1=1)))
		self.assertFalse(check_update(upd, TestUpdate(1, f3='3')))
		self.assertTrue(check_update(upd, TestUpdate(1, f1=1, f2=2, f3='3q')))

	def test_check_update_list(self):
		upd_list1 = [TestUpdate(221, f1=1, f2=2, f3='3'),
						TestUpdate(1, f1=1, f2=2, f3='3'),
						TestUpdate(221, f1=1, f2=2, f3='3'),
   						TestUpdate(2, f1=11, f2=22, f3='33'),
						TestUpdate(221, f1=1, f2=2, f3='3'),
						TestUpdate(221, f1=1, f2=2, f3='3'),
						TestUpdate(1, f1=111, f2=222, f3='333')
		]
		upd_list2 = [TestUpdate(1, f1=1, f2=2, f3='3'),
						TestUpdate(2, f1=11, f2=22, f3='33'),
						TestUpdate(221, f1=1, f2=2, f3='3'),
						TestUpdate(221, f1=1, f2=2, f3='3'),
						TestUpdate(1, f1=111, f2=222, f3='q333')
		]

		test_upd_list = [TestUpdate(1, f1=1, f2=2, f3='3'),
						TestUpdate(2, f1=11, f2=22, f3='33'),
						TestUpdate(1, f1=111, f2=222, f3='333')
		]

		self.assertFalse(check_update_list(upd_list1, test_upd_list))
		self.assertEqual(3, len(check_update_list(upd_list2, test_upd_list)))	# here should be 3 errors

	def test_check_update_list_real(self):
		real_list = [
			TestUpdate(10, **{'InstrumentID': 16, 'ExpireTime': 1000000000000000L, 'TrailingStopType': 1, 'StopPx': 1, 'OrderType': 2, 'Price': 1, 'RequestUserID': 31,'QuoteType': 1, 'TrailingStop': 1, 'TimeInForce': 6, 'OrderQty': 1, 'TransactTime': 1, 'OrderClass': 0, 'Side': 1, 'ClOrdID': u'zzz', 'AccountID': 32}),
			TestUpdate(10, **{'InstrumentID': 16, 'ExpireTime': 1000000000000000L, 'TrailingStopType': 1, 'StopPx': 1, 'OrderType': 2, 'Price': 1, 'RequestUserID': 31, 'QuoteType': 1, 'TrailingStop': 1, 'TimeInForce': 6, 'OrderQty': 1, 'TransactTime': 1, 'OrderClass': 0, 'Side': 2, 'ClOrdID': u'yyy', 'AccountID': 33})
		]

		test_list = [
			TestUpdate(10, InstrumentID=settings.Any, AccountID=32, Side=1, ClOrdID='zzz'),
			TestUpdate(10, InstrumentID=16, AccountID=33, Side=2, ClOrdID='yyy')
		]

		self.assertFalse(check_update_list(real_list, test_list))

	def test_check_update_list_real2(self):
		real_list = [
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 7, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450452885872L, 'ClOrdID': u'zzz', 'OrderID': 23, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 1, 'ExecID': 96300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 24, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450452885874L, 'ClOrdID': u'zzz', 'OrderID': 23, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 1, 'ExecID': 97300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 7, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450452885877L, 'ClOrdID': u'yyy', 'OrderID': 24, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 2, 'ExecID': 98300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 13, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450452885878L, 'ClOrdID': u'1', 'OrderID': 16, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 1, 'ExecID': 99300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 13, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450452885879L, 'ClOrdID': u'yyy', 'OrderID': 24, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 2, 'ExecID': 100300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 24, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450452885881L, 'ClOrdID': u'yyy', 'OrderID': 24, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 2, 'ExecID': 101300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
		]

		test_list = [
			TestUpdate(14, InstrumentID=16, AccountID=32, Side=1, ClOrdID='zzz', ExecType=7),
			TestUpdate(14, InstrumentID=16, AccountID=33, Side=2, ClOrdID='yyy', ExecType=7),
			TestUpdate(14, InstrumentID=16, AccountID=32, Side=1, ClOrdID='zzz', ExecType=13)
		]

		self.assertEqual(3, len(check_update_list(real_list, test_list)))

	def test_check_update_list_real3(self):
		real_list = [
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 7, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450460347590L, 'ClOrdID': u'yyy', 'OrderID': 133, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 2, 'ExecID': 504300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 13, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450460347592L, 'ClOrdID': u'1', 'OrderID': 120, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 1, 'ExecID': 505300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 13, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450460347593L, 'ClOrdID': u'yyy', 'OrderID': 133, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0,'Side': 2, 'ExecID': 506300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 1, 'UserID': 31, 'AccountID': 33, 'TimeInForce': 6, 'ExecType': 24, 'Price': 1, 'LastQty': 1, 'QuoteType': 1, 'LeavesQty': 0, 'OrderStatus': 5, 'TransactTime': 1450460347594L, 'ClOrdID': u'yyy', 'OrderID': 133, 'OrderType': 2, 'LastPx': 1, 'AvgPx': 1, 'OrderCreationTime': 1, 'OrderClass': 0,'Side': 2, 'ExecID': 507300, 'ActiveQty': 0, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 7, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450460347596L, 'ClOrdID': u'zzz', 'OrderID': 134, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0, 'Side': 1, 'ExecID': 508300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': False}),
			TestUpdate(14, **{'InstrumentID': 16, 'OrderQty': 1, 'CumQty': 0, 'UserID': 31, 'AccountID': 32, 'TimeInForce': 6, 'ExecType': 24, 'Price': 1, 'LastQty': 0, 'QuoteType': 1, 'LeavesQty': 1, 'OrderStatus': 6, 'TransactTime': 1450460347597L, 'ClOrdID': u'zzz', 'OrderID': 134, 'OrderType': 2, 'LastPx': 0, 'AvgPx': 0, 'OrderCreationTime': 1, 'OrderClass': 0,'Side': 1, 'ExecID': 509300, 'ActiveQty': 1, 'ExpireTime': 1000000000000000L, 'IsMatchingComplete': True}),
		]

		test_list = [
			TestUpdate(14, InstrumentID=16, AccountID=32, Side=1, ClOrdID='zzz', ExecType=7),
			TestUpdate(14, InstrumentID=16, AccountID=32, Side=1, ExecType=13)
		]

		self.assertEqual(1, len(check_update_list(real_list, test_list)))

	def test_empty(self):
		self.assertTrue(check_update_list([], [
			TestUpdate(10, InstrumentID=16, AccountID=1, Side=1),
			TestUpdate(10, InstrumentID=16, AccountID=1, Side=1),
		]))


if __name__ == '__main__':
	try:
		unittest.main()
	except:
		Global.flag_work = False
		raise
	Global.flag_work = False