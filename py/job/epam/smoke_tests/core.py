# coding=utf8

import websocket
import threading
import zmq
import sys
import json
import logging
import os
import copy
import time
sleep = time.sleep

try:
	import MessageHeaders_pb2
	import MessageType_pb2
	import MessageDictionary_pb2
except ImportError:
	print 'Cannot import python protobuf files. Make "python generate_proto.py".\nExiting.'
	sys.exit(1)

import settings
import messages

logging.basicConfig(format='%(levelname)s|%(name)s \t%(message)s')
log = logging.getLogger('core')
log.setLevel(settings.DebugLevel)

def temp_debug(msg):
	log.debug('__debug ' + str(msg))

class TtrtError(RuntimeError):
	pass

class Global:
	flag_work = True
	zmq_context = zmq.Context()

def wait_for(obj, attr, state, timeout, time_step = 0.2):
	end_time = time.time() + timeout
	while 1:
		if obj.__dict__[attr] == state:
			return True
		if time.time() > end_time:
			return False
		time.sleep(time_step)

def generate_py_protobuf(protoc, src_dir, dst_dir='.'):
	protoc = os.path.normpath(protoc)
	src_dir = os.path.normpath(src_dir)
	dst_dir = os.path.normpath(dst_dir)
	for in_proto_file in glob.glob(os.path.join(src_dir,'*.proto')):
		cmd = '{} -I={} --python_out={} {}'.format(protoc, src_dir, dst_dir, in_proto_file)
		subprocess.call(cmd.split())

class WebSocketClient:
	def __init__(self, addr, user, pwd):
		try:
			self.ws = websocket.create_connection(addr, 4)
		except websocket.socket.timeout:
			raise TtrtError('WebSocketClient cannot connect to FDS')
		self.ws.settimeout(settings.Timeout.RecvThread)
		threading.Thread(target=self.receivedThread).start()

		self.callbacks = {
			'ERROR' :  self.callback_error
			, '/login': self.callback_login
			, '/userInfo' : self.userinfo_login
			, '/accountInfo' : self.callback_print_debug
		}

		self.logged = False
		self.userId = None
		self.accounts = []
		self.login(user, pwd)

		self.instruments = []
		#self.subscribe_instruments()

		#self.subscribe('/blotter/execution', self.callback_debug_only)
		#self.subscribe('/blotter/position', self.callback_debug_only)
		#self.subscribe('/blotter/order', self.callback_debug_only)

	def subscribe_instruments(self):
		del self.instruments[:]
		self.subscribe('/trading/instruments', self.callback_get_instruments)

	def subscribe_l1(self, instr, clb):
		self.subscribe('/l1update %s'%instr, clb)

	def subscribe_l2(self, instr, clb):
		self.subscribe('/l2update %s'%instr, clb)

	def stop(self):
		for t in self.callbacks:
			self.unsubscribe(t)

	def receivedThread(self):
		while Global.flag_work:
			try:
				recv_msg = self.ws.recv().split('\n')
			except websocket.WebSocketTimeoutException:
				continue
			except websocket.WebSocketConnectionClosedException as e:
				log.critical('WebSocketClient: %s', e.message)
				self.logged = False
				return
			#log.debug('WebSocketClient has received %s', recv_msg)
			try:
				callback = self.callbacks[recv_msg[0]]
			except (IndexError, KeyError):
				log.warning('WebSocketClient has received %s, no callback', recv_msg)
			else:
				callback(recv_msg[0], recv_msg[1])

	def __send(self, msg):
		if not self.ws.connected:
			log.debug('WebSocketClient is not connected')
			return
		self.ws.send(msg)

	def send(self, topic, body):
		msg = 'SEND {}\n{}\n'.format(topic, body)
		self.__send(msg)

	def login(self, user, pwd):
		self.send('/login', json.dumps({'LoginRequest':{'UserName':user, 'Password':pwd}}))
		if not wait_for(self, 'logged', True, 5):
			raise TtrtError('WebSocketClient cannot login on FDS')
		if not self.userId:
			raise TtrtError('WebSocketClient cannot get user ID [%s]'%self.userId)
		if len(self.accounts) <= 0:
			raise TtrtError('WebSocketClient cannot get accounts')
		log.warning('WebSocketClient is successfully logged as %s. Id - %s, accounts - %s', user, self.userId, self.accounts)

	def callback_login(self, topic, body):
		try:
			status = json.loads(body)['status']
		except (KeyError, ValueError):
			log.warning('WebSocketClient.callback_login error %s %s', topic, body)

		self.logged = True
		if status != 0:
			log.error('WebSocketClient. LoginStatusCode == %s', status)

	def subscribe(self, topic, clb):
		log.info('WebSocketClient is trying to subscribe on %s', topic)
		msg = 'SUBSCRIBE {}\n'.format(topic)
		self.__send(msg)
		self.callbacks[topic] = clb

	def unsubscribe(self, topic):
		log.info('WebSocketClient is trying to unsubscribe on %s', topic)
		msg = 'UNSUBSCRIBE {}\n'.format(topic)
		self.__send(msg)

	def callback_error(self, topic, body):
		log.error('WebSocketClient has received: %s %s', topic, body)
	def callback_print_debug(self, topic, body):
		log.debug('WebSocketClient has received: %s %s', topic, body)
	def callback_debug_only(self, topic, body):
		log.debug('WebSocketClient has received: %s', topic)

	def userinfo_login(self, topic, body):
		msg = json.loads(body)
		self.userId = msg['userId']
		self.accounts = msg['accounts']

	def callback_get_instruments(self, topic, body):
		msg = json.loads(body)
		try:
			self.instruments = msg[u'SecurityDefinitionSnapshot'][u'SecurityDefinitionItems']
		except KeyError:
			log.warn('WebSocketClient has received an empty instruments message %s', msg)
			return
		#temp_debug(self.instruments)
		active_instruments = []
		for i in self.instruments:
			if i['RecordStatus'] == 0:
				active_instruments.append(i['InstrumentID'])
		log.info('WebSocketClient has received SecurityDefinitionSnapshot with %d(%d) instruments', len(self.instruments), len(active_instruments))
		log.info('Active instruments %s', active_instruments)

class ZmqProducer:
	def __init__(self, topic_name, pub_endpoint):
		self.pub_socket = Global.zmq_context.socket(zmq.PUB)
		self.pub_socket.bind(pub_endpoint)

		self.topic_name = topic_name

	def publishUpdate(self, message_type, body):
		header = MessageHeaders_pb2.MessageHeader()
		header.seqNumber = 0
		header.messageType = message_type
		self.pub_socket.send_multipart((self.topic_name, header.SerializeToString(), body))

class ZmqConsumer:
	def __init__(self, topic_name, sub_endpoint):
		self.sub_socket = Global.zmq_context.socket(zmq.SUB)
		self.sub_socket.connect(sub_endpoint)
		self.sub_socket.setsockopt(zmq.SUBSCRIBE, topic_name)

		self.topic_name = topic_name

		self.__updates = []
		self.updates_lock = threading.Lock()	# for clear and copying
		threading.Thread(target=self.receivedThread).start()

	def receivedThread(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while Global.flag_work:
			socks = dict(poller.poll(settings.Timeout.RecvThread*1000))
			if self.sub_socket in socks:
				msg = self.sub_socket.recv_multipart()
				self.process_update(msg)

	def process_update(self, msg):
		#log.debug('ZmqConsumer: Received update %s', msg)
		try:
			self.__updates.append(Update(msg))
		except TtrtError as e:
			log.error(e.message)

	def clear_updates(self):
		with self.updates_lock:
			del self.__updates[:]
	def take_updates(self):
		with self.updates_lock:
			ret = copy.copy(self.__updates)
			del self.__updates[:]
		return ret
	def print_updates(self, print_fn, pref):
		with self.updates_lock:
			for u in self.__updates:
				print_fn('%s | %s | %s'%(pref, self.topic_name, u))

class ZmqApp:
	def __init__(self, settings):
		self.dct_consumers = {}
		for k,v in settings.PubTopics.iteritems():
			self.dct_consumers[k] = ZmqConsumer(k, v[0])

		self.name = settings.__name__

	def clear_all(self):
		for c in self.dct_consumers.itervalues():
			c.clear_updates()
	def take_updates(self, topic):
		return self.dct_consumers[topic].take_updates()
	def print_all_updates(self, print_fn):
		for c in self.dct_consumers.itervalues():
			c.print_updates(print_fn, self.name)

# update from zmq protobuf
class Update:
	def __init__(self, msg):
		self.raw_msg = msg
		if len(msg) != 3:
			raise TtrtError('Update error, len != 3 %s'%msg)
		header = MessageHeaders_pb2.MessageHeader()
		header.ParseFromString(msg[1])
		if not header.IsInitialized():
			raise TtrtError('Update error. Bad proto header %s'%msg)
		self.type = header.messageType
		if self.type not in messages.ProtoMapping:
			raise TtrtError('Unknown message type %d. Update messages.ProtoMapping, or data-model-all or something else %s'%(self.type, msg))
		prbfObj = messages.ProtoMapping[self.type]()
		prbfObj.ParseFromString(msg[2])
		if not prbfObj.IsInitialized():
			raise TtrtError('Update error. Message %d parsing error %s'%(self.type, msg))
		self.fields = {}
		for descr, val in prbfObj.ListFields():
			self.fields[descr.name] = val

	def __str__(self):
		return '{}: {}'.format(self.type, self.fields)

# update from code
class TestUpdate:
	def __init__(self, type, **fields):
		self.type = type
		self.fields = fields
	def __str__(self):
		return '{}: {}'.format(self.type, self.fields)

def check_update(upd, test_upd):
	#temp_debug('\n\n%s\n%s'%(upd, test_upd))
	if upd.type != test_upd.type:
		return 'Type ({} != {})'.format(upd.type, test_upd.type)
	for k in test_upd.fields:
		if k not in upd.fields:
			return 'Key {} is absent'.format(k)
		if upd.fields[k] != test_upd.fields[k] and test_upd.fields[k] != settings.Any:
			return 'Value of "{}" ({} != {})'.format(k, upd.fields[k], test_upd.fields[k])
	return ''

def check_update_list(upd_list, test_upd_list):
	upd_list_copy = copy.copy(upd_list)
	common_res = ['No updates']
	for test_upd in test_upd_list:
		while 1:
			if not upd_list_copy:
				return common_res
			res = check_update(upd_list_copy.pop(0), test_upd)
			if res == '':
				common_res = []
				break
			common_res.append(res)
	return []

def clear_instrument(instrId):
	auxUser = WebSocketClient(settings.FDS.WebSocketAddress, settings.FDS.AuxUsername, settings.FDS.AuxPassword)
	f_empty = False

	def l2_action(t, body):
		try:
			l2_list = json.loads(body)['L2Snapshot']['L2Records']
		except (ValueError, KeyError):
			log.info('l2 message is ignored, %s %s', t, body)
			return

		if not l2_list:
			f_empty = True
			log.info('Instrument %s is clear', instrId)
			return

		for l2 in l2_list:
			instr = l2['InstrumentID']
			if l2['Side'] == 1:
				side = 2
			elif l2['Side'] == 2:
				side = 1
			acc = auxUser.accounts[0]

			newMarketOrder = messages.defaultNewOrderSingle(OrderType=1, InstrumentID=instr, AccountID=acc, Side=side)
			auxUser.send('/newOrderSingle', messages.uiNewOrderSingle(newMarketOrder))

	auxUser.subscribe_l2(instrId, l2_action)
	sleep(settings.Timeout.RespDelay)
	auxUser.subscribe_l2(instrId, l2_action)
	sleep(settings.Timeout.RespDelay)
	return f_empty

if __name__ == '__main__':
	ui = WebSocketClient(settings.FDS.WebSocketAddress)

	fds = ZmqApp(settings.FDS)
	vrcs = ZmqApp(settings.VRCS)
	sleep(0.5)
	fds.clear_all()
	vrcs.clear_all()
	ui.send('/newOrderSingle', messages.uiNewOrderSingle())
	sleep(0.5)
	for u in fds.take_updates('A.D'):
		log.info('fds %s', u.fields)
	for u in vrcs.take_updates('I.GRCV'):
		log.info('vrcs %s', u.fields)

# ------------------------
	try:
		raw_input()
	except KeyboardInterrupt:
		pass

	Global.flag_work = False
	log.info('Exiting..')