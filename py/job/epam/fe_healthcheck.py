#coding=utf8

import sys
import argparse
import os
import re
import time

class Args:
	pass

class TempFilesStorage:
	def __init__(self, rmTempFiles):
		self.entries = {}
		self.rmTempFiles = rmTempFiles
	def add(self, name):
		if self.entries.has_key(name):
			raise RuntimeError('Temp file %s is exist already'%name)
		self.entries[name] = open(name, 'wb')
		return self.entries[name]
	def close_all(self):
		for k,v in self.entries.iteritems():
			v.close()
	def clear(self):
		self.close_all()
		if self.rmTempFiles:
			for k in self.entries.iterkeys():
				os.remove(k)
	@staticmethod
	def copyFromTempFile(in_file, out_file):
			with open(in_file.name, 'rb') as in_file_:
				for line in in_file_:
					out_file.write(line)


class FEHealthCheck:

	def __init__(self, argv):
		self.get_args(argv)

		self.error_types = map(str.upper, self.args.error_types.split(','))
		self.keywords = filter(None, self.args.keywords.split(','))

		self.input_file_list = self.get_input_file_list()

		self.tempFilesStorage = TempFilesStorage(not self.args.preserve_temp_files)

	def get_args(self, argv):
		parser = argparse.ArgumentParser()

		parser.add_argument('--input', '-i', help='Path to the base input log file. "FixEdge.log" by default', default='FixEdge.log')
		parser.add_argument('--error_types', '-e', help='Types of errors for looking up. Comma separated list. "warn,error,fatal" by default', default='warn,error,fatal')
		parser.add_argument('--keywords', '-k', help='Arbitrary words for looking up. Comma separated list. Empty by default', default='')
		parser.add_argument('--preserve_temp_files', '-p', action='store_true', help='Preserve temp files. False is default')

		self.args = Args()
		parser.parse_args(argv[1:], self.args)

	def out_file_name(self, def_part = ''):
		return '%s_report%s.txt'%(self.args.input, def_part)

	def get_input_file_list(self):
		if not os.path.isfile(self.args.input):
			raise RuntimeError('File %s is not exist'%self.args.input)

		ret = [self.args.input]
		cnt = 0
		while 1:
			pth = '%s.%d'%(self.args.input, cnt)
			if os.path.isfile(pth):
				ret.insert(0, pth)
				cnt += 1
			else:
				break
		return ret

	def run(self):
		try:
			self.before_run()

			for in_file_path in self.input_file_list:
				with open(in_file_path, 'rb') as opened_file:
					for line in opened_file:
						if self.is_new_message(line):
							self.process_message()
							self.current_message = line
						else:
							self.current_message += line
					self.process_message()	#	last message in the file

			self.tempFilesStorage.close_all()
			self.result_to_text()

		finally:
			self.tempFilesStorage.clear()

	def result_to_text(self):

		with open(self.out_file_name(), 'wb') as result_file:
			new_block_line = lambda n: '\n-------- %s --------\n'%n

			result_file.write(new_block_line('Common'))
			fmt_string = '%-40s %s\n'
			result_file.write(fmt_string%('Input log files:', ','.join(self.input_file_list)))
			result_file.write(fmt_string%('First log time:', self.logTimeChecker.first_log_time))
			result_file.write(fmt_string%('Last log time:', self.logTimeChecker.last_log_time))
			result_file.write(fmt_string%('License errors:', self.licenseChecker.cnt_err))

			result_file.write(new_block_line('Log levels'))
			for lev in self.logLevelChecker.found_levels.iterkeys():
				result_file.write('Level %s is enabled\n'%lev)

			result_file.write(new_block_line('Progress of work (%d)'%self.feStateChecker.cnt_coinc))
			TempFilesStorage.copyFromTempFile(self.feStateChecker.storage, result_file)

			result_file.write(new_block_line('License (%d)'%self.licenseChecker.cnt_coinc))
			TempFilesStorage.copyFromTempFile(self.licenseChecker.storage, result_file)

			result_file.write(new_block_line('Sessions (%d)'%self.sessionStateChecker.cnt_coinc))
			TempFilesStorage.copyFromTempFile(self.sessionStateChecker.storage, result_file)

			result_file.write(new_block_line('Errors [%s] (%d)'%(', '.join(self.error_types), self.errorChecker.cnt_coinc)))
			TempFilesStorage.copyFromTempFile(self.errorChecker.storage, result_file)

			result_file.write(new_block_line('Keywords: [%s] (%d)'%(', '.join(self.keywords), self.keywordsChecker.cnt_coinc)))
			TempFilesStorage.copyFromTempFile(self.keywordsChecker.storage, result_file)

			for lev, exams in self.logLevelChecker.found_levels.iteritems():
				result_file.write(new_block_line('%s level examples'%lev))
				for ex in exams:
					result_file.write(ex)



	def before_run(self):
		self.timestamp_templ = self.get_timestamp_templ()

		self.current_message = ''

		self.logTimeChecker = LogTimeChecker(self)
		self.logLevelChecker = LogLevelChecker(self)

		self.licenseChecker = LicenseChecker(self, self.tempFilesStorage.add(self.out_file_name('_LICENSE')))
		self.errorChecker = WordsChecker(self, self.tempFilesStorage.add(self.out_file_name('_ERRORS')), self.error_types)
		self.keywordsChecker = WordsChecker(self, self.tempFilesStorage.add(self.out_file_name('_KEYWORDS')), self.keywords)
		self.sessionStateChecker = SessionStateChecker(self, self.tempFilesStorage.add(self.out_file_name('_SESSIONS')))
		self.feStateChecker = FEStateChecker(self, self.tempFilesStorage.add(self.out_file_name('_APP')))

	def process_message(self):
		self.logLevelChecker.check()
		self.logTimeChecker.check()

		if self.feStateChecker.check():
			return
		if self.licenseChecker.check():
			return
		if self.sessionStateChecker.check():
			return
		if self.errorChecker.check():
			return
		if self.keywordsChecker.check():
			return

	def get_time(self, line):
		mo = self.timestamp_templ.search(line)
		if mo:
			return mo.group()

	def is_new_message(self, line):
		lineTime = self.get_time(line)
		if lineTime:		#len(line) > 0 and (line[0] == ' ' or line[0] == '\t')
			self.lastTime = lineTime
			return True
		return False

	def get_timestamp_templ(self):
		# There are 3 types of timestamp:
		# date{FIX} "YYYYMMDD-HH:MM:SS.sss"
		# date{ISO8601} "YYYY-MM-DD HH:MM:SS,sss"
		# date "DD MMM YYYY HH:MM:SS,sss", where month in the letter form

		tmpl_dict = {	re.compile(r'\d{8}-\d{2}:\d{2}:\d{2}\.\d{3}') : 0,
						re.compile(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}') : 0,
						re.compile(r'\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d{4} \d{2}:\d{2}:\d{2},\d{3}') : 0
					}

		# constants of algo
		q_line = 1000	# quantity lines for analysis
		q_coinc = 100	# needed quantity of coincidences
		q_accid = 10	# acceptable quantity of other coincidences

		for in_file_path in self.input_file_list:
			with open(in_file_path, 'rb') as opened_file:
				for line in opened_file:
					for tmpl, cnt in tmpl_dict.iteritems():
						if tmpl.search(line):
							tmpl_dict[tmpl] = cnt + 1
							break
					q_line -= 1
					if q_line < 0:
						break
			if q_line < 0:
				break

		if q_line > 0:	# too little log file
			q_coinc = 2
			q_accid = 0

		sum_coinc = sum(tmpl_dict.itervalues())
		for tmpl, cnt in tmpl_dict.iteritems():
			if cnt > q_coinc and sum_coinc - cnt <= q_accid:
				return tmpl

		raise RuntimeError('Cannot define the type of timestamp (%d, %d, %d).'%tuple(tmpl_dict.itervalues()))

class BaseChecker:
	def __init__(self, app, storage, pattern):
		self.app = app
		if pattern:
			self.template = re.compile(pattern)
		else:
			self.check = lambda : False
		self.storage = storage
		self.cnt_coinc = 0

	def check(self):
		if self.template.search(self.app.current_message):
			self.storage.write(self.app.current_message)
			self.cnt_coinc += 1
			return True
		return False

class LicenseChecker(BaseChecker):
	def __init__(self, app, storage):
		err = r'Session.*was terminated because it.*license limitation'
		err += r'|was stopped because.*license'
		err += r'|FIX protocol is not supported by your current license!'
		err += r'|[Ll]icense is not valid'
		err += r'|Could not verify license during engine initialization'
		err += r'|License is invalid'
		err += r'|Cannot open the file.+the "LicenseFile" property'
		ok = r'The license for the.+expires'
		name = r'License: '
		pattern = '%s|%s|%s'%(err, ok, name)

		BaseChecker.__init__(self, app, storage, pattern)

		self.tmpl_err = re.compile(err)
		self.cnt_err = 0

	def check(self):
		if BaseChecker.check(self):
			if self.tmpl_err.search(self.app.current_message):
				self.cnt_err += 1
			return True

class WordsChecker(BaseChecker):
	def __init__(self, app, storage, lst):
		BaseChecker.__init__(self, app, storage, '|'.join(lst))

class SessionStateChecker(BaseChecker):
	def __init__(self, app, storage):
		BaseChecker.__init__(self, app, storage, r' Change state: old state=.+ new state=')

class FEStateChecker(BaseChecker):
	def __init__(self, app, storage):
		start = r'FIXEdge Version (.+) started\.'
		stop = r'The B2BITS FIX Engine was stopped\.'
		pattern = '%s|%s'%(start, stop)
		BaseChecker.__init__(self, app, storage, pattern)

class LogLevelChecker:
	def __init__(self, app):
		self.app = app

		self.required_levels = {}
		levels = ['NOTE', 'INFO','DEBUG','TRACE','WARN','ERROR','FATAL','CYCLE']
		for l in levels:
			self.required_levels[l] =  re.compile(l)

		# For common case it's impossible to distinguish the log level name and a piece of data
		# so each the found level has some examples for the possibility to discover the false find
		self.found_levels = {}
		self.q_examples = 3

	def check(self):
		for l in tuple(self.required_levels):
			if self.required_levels[l].search(self.app.current_message):
				self.found_levels.setdefault(l, []).append(self.app.current_message)
				if len(self.found_levels[l]) >= self.q_examples:
					self.required_levels.pop(l)
					if l == 'NOTE':
						self.required_levels.pop('INFO')
					elif l == 'INFO':
						self.required_levels.pop('NOTE')
				break


class LogTimeChecker:
	def __init__(self, app):
		self.app = app

		self.first_log_time = ''
		self.last_log_time = ''

	def check(self):
		if not self.first_log_time:
			self.first_log_time = self.app.lastTime
		self.last_log_time = self.app.lastTime

def sizeOfFiles(lstFiles):
	size = 0
	for f in lstFiles:
		size += os.stat(f).st_size
	return float(size)/2**20	# Mb

if __name__ == '__main__':
	beginTime = time.time()

	app = FEHealthCheck(sys.argv)
	app.run()

	fmt_string = '%-20s %s'
	print fmt_string%('Input logs:', ','.join(app.input_file_list))
	print fmt_string%('Error types:', ', '.join(app.error_types))
	print fmt_string%('Keywords:', ', '.join(app.keywords))
	print
	print fmt_string%('Runtime:', time.time() - beginTime)
	print fmt_string%('Input logs size:', sizeOfFiles(app.input_file_list))
	print fmt_string%('Output logs size:', sizeOfFiles((app.out_file_name(),)))
