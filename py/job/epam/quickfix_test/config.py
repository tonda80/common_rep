qf_config = 'config.ini'
debug_level = 10
wait_timeout = 2


# quickfix config
qf_cfg_default = {
	'ReconnectInterval': '10',
	'StartTime': '07:30:00',
	'EndTime': '23:30:00',
	'FileLogPath': 'log',
	'FileStorePath': 'log',
	'HeartBtInt': '20',
	'SocketAcceptPort': '0',

	'ValidateUserDefinedFields': 'N',
}

# default part for all sessions
qf_cfg_session_default = {

}

qf_cfg_sessions = [
	{
		'BeginString': 'FIX.4.4',
		'DataDictionary': 'resources/FIX44.xml',
		'ConnectionType': 'initiator',
		'SenderCompID': 'TEST001',
		'TargetCompID': 'NEPTUNE',
		'SocketConnectHost': '127.0.0.1',
		'SocketConnectPort': '9000',
		'_Username' : 'user1',
		'_Password' : 'pwd1',
		'ResetOnLogon' : 'Y',
	},
]

with open(qf_config, 'w') as f:
	f.write('[DEFAULT]\n')
	for k, v in qf_cfg_default.iteritems():
		f.write('%s=%s\n'%(k,v))

	for s in qf_cfg_sessions:
		f.write('\n[SESSION]\n')

		for k, v in qf_cfg_session_default.iteritems():
			s.setdefault(k, v)

		for k, v in s.iteritems():
			if not k.startswith('_'):
				f.write('%s=%s\n'%(k,v))
