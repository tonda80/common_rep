import quickfix
import unittest
import time
import Queue
import shutil

import config as cfg

def print_debug(d, lev = 20):
	if lev >= cfg.debug_level:
		print d

def msg_from_str(str_msg, validate = False, soh_sym='|'):
	dct = None
	flg_FIX50SP2 = str_msg.find('%s1128=9%s'%(soh_sym,soh_sym)) != -1 or str_msg.find('%s1137=9%s'%(soh_sym,soh_sym)) != -1

	if str_msg.find('8=FIX.4.4') == 0:
		dct = quickfix.DataDictionary("./resources/FIX44.xml")
	elif str_msg.find('8=FIXT.1.1') == 0 and flg_FIX50SP2:
		dct = quickfix.DataDictionary("./resources/FIX50SP2.xml")

	msg = quickfix.Message(str_msg.replace(soh_sym, '\x01'), dct, validate)

	return msg

def msg_to_str(msg, soh_sym='|'):
	return msg.toString().replace('\x01', soh_sym)

def add_repeating_group(msg, gr_field, fld_dict):
	gr = quickfix.Group(gr_field, 0)
	for k,v in fld_dict.iteritems():
		gr.setField(k, v)
	msg.addGroup(gr)

def UTCTimestamp(tm = None):
	if tm is None:
		tm = time.gmtime(time.time())
	return time.strftime('%Y%m%d-%H:%M:%S', tm)

class Fields(dict):
	def __init__(self, qf_msg, soh_sym='\x01'):
		dict.__init__(self)
		if not isinstance(qf_msg, str):
			qf_msg = qf_msg.toString()
		for e in qf_msg.split(soh_sym):
			if len(e)==0: continue
			k,v = e.split('=')
			self.setdefault(int(k), []).append(v)

	def getField(self, field):
		if len(self[field]) != 1:
			raise KeyError('Fields.getField must be called for a field with one value.')
		return self[field][0]

# quickfix application
class QFApplication(quickfix.Application):
	def __init__(self, app_rec_q, adm_rec_q):
		quickfix.Application.__init__(self)
		self.app_rec_q = app_rec_q
		self.adm_rec_q = adm_rec_q
		self.flg_logon = False	# flag of FE start

	def onCreate(self, sessionID):
		print_debug('onCreate %s'%sessionID.toString())

	def onLogon(self, sessionID):
		print_debug('onLogon from %s'%(sessionID, ))
		self.flg_logon = True

	def onLogout(self, sessionID):
		print_debug('onLogout from %s'%(sessionID, ))

	def toApp(self, message, sessionID):
		print_debug('toApp %s %s'%(message, sessionID), 5)

	def fromApp(self, message, sessionID):
		print_debug('fromApp %s %s'%(message, sessionID), 5)
		self.app_rec_q.put((sessionID, quickfix.Message(message)))

	def toAdmin(self, message, sessionID):
		print_debug('toAdmin %s %s'%(message, sessionID), 3)

	def fromAdmin(self, message, sessionID):
		print_debug('fromAdmin %s %s'%(message, sessionID), 3)
		ignor_msg_type = ('A', '5')
		if message.getHeader().getField(35) not in ignor_msg_type:
			self.adm_rec_q.put((sessionID, quickfix.Message(message)))

class QF_Mgr:
	pass

qf_mgr = QF_Mgr()

def setUpModule():
	qf_mgr.app_rec_q = Queue.Queue()
	qf_mgr.adm_rec_q = Queue.Queue()
	application = QFApplication(qf_mgr.app_rec_q, qf_mgr.adm_rec_q)
	settings = quickfix.SessionSettings(cfg.qf_config)
	storeFactory = quickfix.FileStoreFactory(settings)
	logFactory = quickfix.FileLogFactory(settings)

	try:
		qf_mgr.initiators = quickfix.SocketInitiator(application, storeFactory, settings, logFactory)
		qf_mgr.initiators.start()
	except quickfix.ConfigError as e:
		if not str(e).find('No sessions defined for initiator'):
			raise
	try:
		qf_mgr.acceptors = quickfix.SocketAcceptor(application, storeFactory, settings, logFactory)
		qf_mgr.acceptors.start()
	except quickfix.ConfigError as e:
		if not str(e).find('No sessions defined for acceptor'):
			raise

	#time.sleep(cfg.start_timeout)
	while not application.flg_logon:
		time.sleep(0.5)

def tearDownModule():
	try:
		qf_mgr.initiators.stop()
		qf_mgr.acceptors.stop()
	except:
		pass

class BaseTest(unittest.TestCase):
	def test_base_receiving(self):
		print 'receiving'
		while 1:
			try:
				msg = qf_mgr.app_rec_q.get(True, 1)
				print msg[0], msg[1]
			except Queue.Empty:
				pass
			except KeyboardInterrupt:
				break

	def test_base_sending(self):
		print 'sending'
		while 1:
			try:
				flg_stop = raw_input('>>')
				if flg_stop == 'c':
					break
				#msg = msg_from_str('8=FIX.4.4|9=54|35=D|11=321|40=1|54=1|55=FROM_QF|60=20150412-13:59:59|10=178|')
				msg = quickfix.Message()
				msg.getHeader().setField(8, 'FIX.4.4')
				msg.getHeader().setField(35, 'D')
				msg.setField(11, '321')
				msg.setField(40, '1')
				msg.setField(54, '1')
				msg.setField(55, 'FROM_QF')
				msg.setField(60, UTCTimestamp())
				quickfix.Session.sendToTarget(msg, 'Sender0', 'Receiver0')
			except KeyboardInterrupt:
				break

	def test_base_fe_loop(self):
		print 'base_fe_loop'
		msg_out = msg_from_str('8=FIX.4.4|9=54|35=D|11=321|40=1|54=1|55=FROM_QF|60=20150412-13:59:59|10=178|')
		quickfix.Session.sendToTarget(msg_out, 'SELLSIDE1', 'NEPTUNE')
		msg_in = qf_mgr.app_rec_q.get(True, cfg.wait_timeout)
		self.assertEqual('FIX.4.4:SELLSIDE1->NEPTUNE', msg_in[0].toString())
		self.assertEqual('D', msg_in[1].getHeader().getField(35))

class ValidValues(unittest.TestCase):
	def setUp(self):
		with qf_mgr.app_rec_q.mutex:
			qf_mgr.app_rec_q.queue.clear()

	def _check_message(self, msg_out):
		quickfix.Session.sendToTarget(msg_out)
		sess, msg_in = qf_mgr.app_rec_q.get(True, cfg.wait_timeout)
		self._check_msg_in(msg_out, msg_in)

	def _check_msg_in(self, msg_out, msg_in):
		#print 'Out\n%s\nIn\n%s\n'%(msg_to_str(msg_out), msg_to_str(msg_in))
		fld_in = Fields(msg_in)
		fld_out = Fields(msg_out)
		self.assertEqual(fld_in[56], fld_out[128])
		self.assertEqual(fld_in[115], fld_out[49])
		for k in fld_out.iterkeys():
			if not (quickfix.Message.isHeaderField(k) or quickfix.Message.isTrailerField(k)):
				try:
					self.assertEqual(fld_in[k], fld_out[k])
				except AssertionError as e:
					raise AssertionError(e.message + '\nfield %d'%k)

	def test_scenario_1(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=N/A|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=N/A|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_2(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=1|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=1|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_3(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_4(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_5(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_6(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_7(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_8(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_9(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=1|104=S|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=1|104=S|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_10(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|199=1|104=S|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|199=1|104=S|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_11(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_12(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_13(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=S|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		#msg_out2 = msg_from_str('')
		#self._check_message(msg_out2)

	def test_scenario_14(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=S|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=2|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=S|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_15(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=1|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=1|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_16(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_17(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=9|2530=1|2531=329.5|2532=1|2530=2|2531=329.5|2532=2|2530=3|2531=329.5|2532=3|2530=4|2531=329.5|2532=1|2530=5|2531=329.5|2532=2|2530=6|2531=329.5|2532=3|2530=7|2531=329.5|2532=1|2530=8|2531=329.5|2532=2|2530=9|2531=329.5|2532=3|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=9|2530=1|2531=329.5|2532=1|2530=2|2531=329.5|2532=2|2530=3|2531=329.5|2532=3|2530=4|2531=329.5|2532=1|2530=5|2531=329.5|2532=2|2530=6|2531=329.5|2532=3|2530=7|2531=329.5|2532=1|2530=8|2531=329.5|2532=2|2530=9|2531=329.5|2532=3|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_18(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=-100|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=E|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=-100|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_19(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

	def test_scenario_20(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=MATURITY|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=MATURITY|10=055|')
		self._check_message(msg_out2)

	def test_scenario_21(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=PUT|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=PUT|10=055|')
		self._check_message(msg_out2)

	def test_scenario_22(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=2|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		self._check_message(msg_out)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=2|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		self._check_message(msg_out2)

class InvalidValuesCombinations(unittest.TestCase):
	def setUp(self):
		with qf_mgr.adm_rec_q.mutex:
			qf_mgr.adm_rec_q.queue.clear()
		with qf_mgr.app_rec_q.mutex:
			qf_mgr.app_rec_q.queue.clear()

	def _check_message(self, msg_out, msg_check, type = 0):
		quickfix.Session.sendToTarget(msg_out)
		if type == 0:		# waiting of 35=3
			sess, msg_in = qf_mgr.adm_rec_q.get(True, cfg.wait_timeout)
			for i in (371, 372, 373, 58):
				self.assertEqual(msg_in.getField(i), msg_check.getField(i))
		elif type == 1:		# waiting of 35=j
			sess, msg_in = qf_mgr.app_rec_q.get(True, cfg.wait_timeout)
			for i in (372, 379, 380, 58):
				self.assertEqual(msg_in.getField(i), msg_check.getField(i))
		else:
			raise RuntimeError('argument "type" must be 0 or 1')

		for i in (8, 35):
			self.assertEqual(msg_in.getHeader().getField(i), msg_check.getHeader().getField(i))


	def test_scenario_1(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=2|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=217|35=3|49=NEPTUNE|56=TEST001|34=2|52=20150428-14:45:11.263|45=24|371=22|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 24, RefTagID: 22, RefMsgType: 6]|10=004|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=2|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=217|35=3|49=NEPTUNE|56=TEST101|34=2|52=20150428-14:45:11.572|45=23|371=22|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 23, RefTagID: 22, RefMsgType: 6]|10=084|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_2(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=1|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=248|35=3|49=NEPTUNE|56=TEST001|34=3|52=20150428-14:45:11.720|45=25|371=2532|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '0'[Group tag=2529, Entry #=0] [RefSeqNum: 25, RefTagID: 2532, RefMsgType: 6]|10=021|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=1|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=248|35=3|49=NEPTUNE|56=TEST101|34=3|52=20150428-14:45:12.030|45=24|371=2532|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '0'[Group tag=2529, Entry #=0] [RefSeqNum: 24, RefTagID: 2532, RefMsgType: 6]|10=093|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_3(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=2|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=223|35=3|49=NEPTUNE|56=TEST001|34=4|52=20150428-14:45:12.178|45=26|371=235|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: 'CLOSE' [RefSeqNum: 26, RefTagID: 235, RefMsgType: 6]|10=189|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=2|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=223|35=3|49=NEPTUNE|56=TEST101|34=4|52=20150428-14:45:12.485|45=25|371=235|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: 'CLOSE' [RefSeqNum: 25, RefTagID: 235, RefMsgType: 6]|10=011|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_4(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=S|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=219|35=3|49=NEPTUNE|56=TEST001|34=5|52=20150428-14:45:12.635|45=27|371=854|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '1' [RefSeqNum: 27, RefTagID: 854, RefMsgType: 6]|10=140|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=S|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=219|35=3|49=NEPTUNE|56=TEST101|34=5|52=20150428-14:45:12.943|45=26|371=854|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '1' [RefSeqNum: 26, RefTagID: 854, RefMsgType: 6]|10=219|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_5(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=L|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=219|35=3|49=NEPTUNE|56=TEST001|34=6|52=20150428-14:45:13.095|45=28|371=423|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 28, RefTagID: 423, RefMsgType: 6]|10=129|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=L|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=219|35=3|49=NEPTUNE|56=TEST101|34=6|52=20150428-14:45:13.404|45=27|371=423|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 27, RefTagID: 423, RefMsgType: 6]|10=200|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_6(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=188|35=3|49=NEPTUNE|56=TEST001|34=7|52=20150428-14:45:13.553|45=29|371=27|372=6|373=6|58=Incorrect field data format. Incorrect integer value: 'S' [RefSeqNum: 29, RefTagID: 27, RefMsgType: 6]|10=066|")
		self._check_message(msg_out, msg_check, 1)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=188|35=3|49=NEPTUNE|56=TEST101|34=7|52=20150428-14:45:13.865|45=28|371=27|372=6|373=6|58=Incorrect field data format. Incorrect integer value: 'S' [RefSeqNum: 28, RefTagID: 27, RefMsgType: 6]|10=149|")
		self._check_message(msg_out2, msg_check2, 1)

	def test_scenario_7(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=3|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=L|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=245|35=3|49=NEPTUNE|56=TEST001|34=8|52=20150428-14:45:14.015|45=30|371=104|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: 'L'[Group tag=199, Entry #=0] [RefSeqNum: 30, RefTagID: 104, RefMsgType: 6]|10=142|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=3|854=0|27=5000000|15=GBP|423=1|44=109.4|130=Y|199=1|104=L|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=245|35=3|49=NEPTUNE|56=TEST101|34=8|52=20150428-14:45:14.324|45=29|371=104|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: 'L'[Group tag=199, Entry #=0] [RefSeqNum: 29, RefTagID: 104, RefMsgType: 6]|10=240|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_8(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=2|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str('8=FIX.4.4|9=154|35=j|49=NEPTUNE|56=TEST001|34=9|52=20150428-14:45:14.473|45=31|372=6|379=1092093|380=0|58=Unsupported combination of values in IOIQualifier and Side tags|10=185|')
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=2|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str('8=FIXT.1.1|9=161|35=j|1128=9|49=NEPTUNE|56=TEST101|34=9|52=20150428-14:45:14.783|45=30|372=6|379=1092093|380=0|58=Unsupported combination of values in IOIQualifier and Side tags|10=076|')
		self._check_message(msg_out2, msg_check2)

	def test_scenario_9(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=0|2531=329.5|2532=1|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=218|35=3|49=NEPTUNE|56=TEST001|34=10|52=20150428-14:45:14.933|45=32|371=54|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '3' [RefSeqNum: 32, RefTagID: 54, RefMsgType: 6]|10=068|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=0|2531=329.5|2532=1|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=218|35=3|49=NEPTUNE|56=TEST101|34=10|52=20150428-14:45:15.245|45=31|371=54|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '3' [RefSeqNum: 31, RefTagID: 54, RefMsgType: 6]|10=142|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_10(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=1|2531=329.5|2532=0|235=CALL|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=220|35=3|49=NEPTUNE|56=TEST001|34=11|52=20150428-14:45:15.395|45=33|371=761|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 33, RefTagID: 761, RefMsgType: 6]|10=172|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=1|2531=329.5|2532=0|235=CALL|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=220|35=3|49=NEPTUNE|56=TEST101|34=11|52=20150428-14:45:15.702|45=32|371=761|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '2' [RefSeqNum: 32, RefTagID: 761, RefMsgType: 6]|10=241|")
		self._check_message(msg_out2, msg_check2)

	def test_scenario_11(self):
		msg_out = msg_from_str('8=FIX.4.4|9=364|35=6|49=TEST001|56=NEPTUNE|34=2|52=20151401-10:25:32.983|128=TEST002|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CLOSE|10=122|')
		msg_check = msg_from_str("8=FIX.4.4|9=249|35=3|49=NEPTUNE|56=TEST001|34=12|52=20150428-14:45:15.854|45=34|371=2530|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '0'[Group tag=2529, Entry #=0] [RefSeqNum: 34, RefTagID: 2530, RefMsgType: 6]|10=078|")
		self._check_message(msg_out, msg_check)

		msg_out2 = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST002|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=ROLLS|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CLOSE|10=055|')
		msg_check2 = msg_from_str("8=FIXT.1.1|9=249|35=3|49=NEPTUNE|56=TEST101|34=12|52=20150428-14:45:16.164|45=33|371=2530|372=6|373=6|58=Incorrect field data format. Field value does not meet ValBlock conditions. Value: '0'[Group tag=2529, Entry #=0] [RefSeqNum: 33, RefTagID: 2530, RefMsgType: 6]|10=150|")
		self._check_message(msg_out2, msg_check2)

class SessionLevel(unittest.TestCase):
	def setUp(self):
		with qf_mgr.adm_rec_q.mutex:
			qf_mgr.adm_rec_q.queue.clear()
		with qf_mgr.app_rec_q.mutex:
			qf_mgr.app_rec_q.queue.clear()

	def _check_message(self, msg_out, type = 0):
		quickfix.Session.sendToTarget(msg_out)
		if type == 0:		# waiting of 35=3
			sess, msg_in = qf_mgr.adm_rec_q.get(True, cfg.wait_timeout)
			self._check_msg_in_3(msg_out, msg_in)
		elif type == 1:		# waiting of 35=j
			sess, msg_in = qf_mgr.app_rec_q.get(True, cfg.wait_timeout)
			self._check_msg_in_j(msg_out, msg_in)
		else:
			raise RuntimeError('argument "type" must be 0 or 1')

	def _check_msg_in_3(self, msg_out, msg_in):
		print '%s\n%s\n\n'%(msg_to_str(msg_out), msg_to_str(msg_in))
		self.assertEqual(msg_in.getHeader().getField(56), msg_out.getHeader().getField(49))
		self.assertEqual(msg_in.getHeader().getField(35), '3')
		# self.assertEqual(msg_in.getField(45), msg_out.getHeader().getField(34))
		# self.assertEqual(msg_in.getField(372), '6')
		# self.assertEqual(msg_in.getField(58).find('Incorrect field data format.'), 0)

	def _check_msg_in_j(self, msg_out, msg_in):
		print '%s\n%s\n\n'%(msg_to_str(msg_out), msg_to_str(msg_in))
		self.assertEqual(msg_in.getHeader().getField(56), msg_out.getHeader().getField(49))
		self.assertEqual(msg_in.getHeader().getField(35), 'j')
		# self.assertEqual(msg_in.getField(45), msg_out.getHeader().getField(34))
		# self.assertEqual(msg_in.getField(372), '6')
		# self.assertEqual(msg_in.getField(58), 'Unsupported combination of values in IOIQualifier and Side tags')

	def test_scenario_1(self):
		msg_out = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=N/A|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		#self._check_message(msg_out, 1)

	def test_scenario_2(self):
		msg_out = msg_from_str('8=FIXT.1.1|9=381|35=6|1128=9|49=TEST101|56=NEPTUNE|128=TEST102|34=52|52=20150403-14:26:57.577|23=1092093|28=N|55=N/A|48=XS0944831154|22=4|541=20160618|223=3.375|54=1|854=0|27=5000000|15=GBP|423=1|44=109.4|130=N|199=2|104=E|104=M|58=4.4 to 5.0|60=20150401-10:24:14|215=2|216=5|217=JohnSmith@BuySide1.com|216=5|217=MaryJones@BuySide1.com|218=10|699=ASRR|761=4|2529=1|2530=3|2531=329.5|2532=1|235=CALL|10=055|')
		msg_out.getHeader().setField(1128, '2')
		self._check_message(msg_out)

	def test_scenario_3(self):
		msg_out = msg_from_str('8=FIXT.1.1|9=76|35=A|34=1|49=TEST101|52=20150428-08:34:10.216|56=NEPTUNE|98=0|108=20|1137=9|10=131|')
		msg_out.setField(1137, '2')
		self._check_message(msg_out)

if __name__ == '__main__':
	unittest.main()