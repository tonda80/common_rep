#coding=utf8

import socket
import time

import struct
import sys

import dpkt

IF_NAME= sys.argv[1] #'ens6f0'

MCAST_GRP = sys.argv[2]#'239.195.1.137'
MCAST_PORT = int(sys.argv[3])#17009

PCAP_FILE = sys.argv[4] #r'dump/pc_cr_eth_fast40-FX-2016-june.pcap'

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

ifreq = struct.pack('16sH14s', IF_NAME, socket.AF_INET, '\x00'*14)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, ifreq)

pcap = dpkt.pcap.Reader(open(PCAP_FILE, 'rb'))
for ts, buf in pcap:
	pck = dpkt.ethernet.Ethernet(buf)
	if socket.inet_ntoa(pck.ip.dst) != MCAST_GRP or 'udp' not in pck.ip.__dict__ or pck.ip.udp.dport != MCAST_PORT:
		continue

	sock.sendto(pck.ip.udp.data, (MCAST_GRP, MCAST_PORT))
	raw_input('Enter to next')

