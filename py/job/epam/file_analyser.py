# coding=utf8

import time
import datetime
import os
import re
import sys

class BaseApp:
	def run(self):
		for f in self.getFileList():
			f_out_name = self.outFileName(f)
			with open(f, 'rb') as in_f, open(f_out_name, 'wb') as out_f:
				self.output_file = out_f
				for l in in_f:
					self.checkLine(l)
			self.onEnd()

	# the following methods may be overrided
	def getFileList(self):
		return sys.argv[1:]

	def outFileName(self, f_in):
		return f_in + '.out.txt'

	def onEnd(self):
		pass

class BaseTimeDeltaApp(BaseApp):
	def checkLine(self, l):
		line_time = self.getTime(l)	# returns datetime or None
		if line_time is None:
			return

		delta = line_time - self.last_time
		self.last_time = line_time

		self.makeAction(line_time, delta, l)


class TimeDeltaApp(BaseTimeDeltaApp):
	def outFileName(self, f_in):
		return f_in + '.dlt.txt'

	def __init__(self):
		self.last_time = datetime.datetime(2099, 1, 1)		# for some big delta

		self.search_template = re.compile(r'\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{3})\]')
		# it kind of [2016-14-07 19:20:24.867]
		self.max_gap = datetime.timedelta(milliseconds = 100)

	def getTime(self, line):
		mo = self.search_template.match(line)
		if mo is None:
			return
		e = map(int, mo.groups())
		return datetime.datetime(e[0], e[2], e[1], e[3], e[4], e[5], e[6]*1000)

	def makeAction(self, line_time, delta, line):
		if delta > self.max_gap:
			self.output_file.write('%s   %s   %s'%(delta, line_time, line))


class SeqNumApp(BaseApp):
	def outFileName(self, f_in):
		return f_in + '.sqn.txt'

	def checkLine(self, l):
		seqnum = self.getSeqNum(l)	# returns number or None
		if seqnum is None:
			return
		if seqnum - 1 != self.last_seqnum and self.last_seqnum:
			print l
		self.last_seqnum = seqnum

	def __init__(self):
		self.search_template = re.compile(r'\[ClientStreamProtocol::OnUpdate\] received sequencenumber (\d*)')

		self.last_seqnum = None

	def getSeqNum(self, line):
		mo = self.search_template.search(line)
		if mo is None:
			return
		return int(mo.group(1))

class PubSizeApp(BaseApp):
	def checkLine(self, l):
		q = self.getSentBytes(l)	# returns number or None
		if q is None:
			return
		q = int(q)
		print q
		self.sum += q

	def __init__(self):
		self.search_template = re.compile(r'sending (\d*) bytes')

		self.sum = 0

	def getSentBytes(self, line):
		mo = self.search_template.search(line)
		if mo is None:
			return
		return int(mo.group(1))

	def onEnd(self):
		print 'Total  =', self.sum

#TimeDeltaApp().run()
#SeqNumApp().run()
PubSizeApp().run()
