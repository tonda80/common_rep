#coding=utf8

import sys
import argparse
import os
import re
import time

class Args:
	pass

class TempFilesStorage:
	def __init__(self, rmTempFiles):
		self.entries = {}
		self.rmTempFiles = rmTempFiles
	def add(self, name):
		if self.entries.has_key(name):
			raise RuntimeError('Temp file %s is exist already'%name)
		self.entries[name] = open(name, 'wb')
		return self.entries[name]
	def close_all(self):
		for k,v in self.entries.iteritems():
			v.close()
	def clear(self):
		self.close_all()
		if self.rmTempFiles:
			for k in self.entries.iterkeys():
				os.remove(k)
	@staticmethod
	def copyFromTempFile(in_file, out_file):
			with open(in_file.name, 'rb') as in_file_:
				for line in in_file_:
					out_file.write(line)

def sizeOfFiles(lstFiles):
	size = 0
	for f in lstFiles:
		size += os.stat(f).st_size
	return float(size)/2**20	# Mb


class LogAnalyser:

	def __init__(self, argv):
		self.get_args(argv)

		self.input_file_list = self.get_input_file_list()

		self.tempFilesStorage = TempFilesStorage(self.args.remove_temp_files)

		self.error_types = map(str.upper, self.args.error_types.split(','))
		self.keywords = filter(None, self.args.keywords.split(','))
		errorChecker = WordsChecker(self, self.tempFilesStorage.add(self.out_file_name('_ERRORS')), self.error_types)
		keywordsChecker = WordsChecker(self, self.tempFilesStorage.add(self.out_file_name('_KEYWORDS')), self.keywords)

		self.checkers = [errorChecker, keywordsChecker]

	def get_args(self, argv):
		parser = argparse.ArgumentParser()

		parser.add_argument('--input', '-i', help='Path to the base input log file. "FixEdge.log" by default', default='FixEdge.log')
		parser.add_argument('--error_types', '-e', help='Types of errors for looking up. Comma separated list. "warn,error,fatal" by default', default='warn,error,fatal')
		parser.add_argument('--keywords', '-k', help='Arbitrary words for looking up. Comma separated list. Empty by default', default='')
		parser.add_argument('--remove_temp_files', '-r', action='store_true', help='Remove temp files. False is default')

		self.args = Args()
		parser.parse_args(argv[1:], self.args)

	def out_file_name(self, def_part = ''):
		return '%s_report%s.txt'%(self.args.input, def_part)

	def get_input_file_list(self):
		if not os.path.isfile(self.args.input):
			raise RuntimeError('File %s is not exist'%self.args.input)

		ret = [self.args.input]
		cnt = 0
		while 1:
			pth = '%s.%d'%(self.args.input, cnt)
			if os.path.isfile(pth):
				ret.insert(0, pth)
				cnt += 1
			else:
				break
		return ret

	def run(self):
		try:
			self.before_run()

			for in_file_path in self.input_file_list:
				with open(in_file_path, 'rb') as opened_file:
					for line in opened_file:
						if self.is_new_message(line):
							self.process_message()
							self.current_message = line
						else:
							self.current_message += line
					self.process_message()	#	last message in the file

			self.tempFilesStorage.close_all()
			self.make_result()

		finally:
			self.tempFilesStorage.clear()

	def make_result(self):
		pass

	def before_run(self):
		self.current_message = ''

		if not self.checkers:
			raise RuntimeError('No added checkers')

	def add_checker(self, checker):
		self.checkers.append(checker)

	def process_message(self):
		for checker in self.checkers:
			checker.check()

	def is_new_message(self, line):
		if len(line) > 0 and (line[0] == ' ' or line[0] == '\t'):
			return False
		return True

class BaseChecker:
	def __init__(self, app, storage, pattern):
		self.app = app
		if pattern:
			self.template = re.compile(pattern)
		else:
			self.check = lambda : False
		self.storage = storage
		self.cnt_coinc = 0

	def check(self):
		if self.template.search(self.app.current_message):
			self.storage.write(self.app.current_message)
			self.cnt_coinc += 1
			return True
		return False

class WordsChecker(BaseChecker):
	def __init__(self, app, storage, lst):
		BaseChecker.__init__(self, app, storage, '|'.join(lst))

class SymbolChecker(BaseChecker):
	def __init__(self, app, sym_name):
		BaseChecker.__init__(self, app, app.tempFilesStorage.add(app.out_file_name('_SYMBOL_'+sym_name)), sym_name)


if __name__ == '__main__':
	beginTime = time.time()

	app = LogAnalyser(sys.argv)
	for sym_name in ('GAZP', 'GMKN', 'LKOH', 'NVTK', 'SBER', 'VTBR', 'USD000UTSTOM'):
		app.add_checker(SymbolChecker(app, sym_name))
	app.run()

	fmt_string = '%-20s %s'
	print fmt_string%('Input logs:', ','.join(app.input_file_list))
	print fmt_string%('Error types:', ', '.join(app.error_types))
	print fmt_string%('Keywords:', ', '.join(app.keywords))
	print
	print fmt_string%('Runtime:', time.time() - beginTime)
	print fmt_string%('Input logs size:', sizeOfFiles(app.input_file_list))
	print fmt_string%('Output logs size:', sizeOfFiles(app.tempFilesStorage.entries))
