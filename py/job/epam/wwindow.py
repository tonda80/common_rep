#coding=utf8

import win32gui
import win32process
import psutil
from time import sleep

class WWindowError(RuntimeError):
	pass

def get_pids_by_name(name):
	pids = []
	for proc in psutil.process_iter():
		try:
			if proc.name() == name:
				pids.append(proc.pid)
		except psutil.AccessDenied:
			pass
	return pids

def get_handles_by_pid(pid):
	def _clb(whndl, testPid):
		tid, pid = win32process.GetWindowThreadProcessId(whndl)
		if pid == testPid:
			#print tid, pid, whndl
			handles.append(whndl)
			#return False

	handles = []
	win32gui.EnumWindows(_clb, pid)
	return handles

class WWindow:
	# class for setting/getting coordinates of a Windows window

	def __init__(self, id_, handle_number = None):
		self.name = ''
		self.given_handle_number = handle_number	# known handle of window
		self.pid_handle_detect(id_)
		#print self.pid, self.__handle

	def pid_handle_detect(self, id_):
		# Sets pid, __handle, name (if possible)
		while 1:
			if isinstance(id_, int):
				pids = (id_,)
			elif isinstance(id_, str):
				self.name = id_
				pids = get_pids_by_name(id_)
			else:
				raise TypeError(id_)

			if len(pids) < 1:
				raise WWindowError('Pids of %s process are not found'%self.name)

			pids_handles = []
			for pid in pids:
				for handle in get_handles_by_pid(pid):
					pids_handles.append((pid, handle))

			q_pids_handles = len(pids_handles)
			if q_pids_handles < 1:
				raise WWindowError('Handle for %s process is not found'%self.name)
			elif q_pids_handles == 1:
				self.pid = pids_handles[0][0]
				self.__handle = pids_handles[0][1]
				return
			else:
				if self.given_handle_number is None:
					print 'Many %s handles are found. Select the necessary one'%self.name
					for i, p_h in enumerate(pids_handles):
						lx, ly, rx, ry = win32gui.GetWindowRect(p_h[1])
						print '%d:\t\tpid %5d, handle %8d, LT point %4d X %4d, size %4d X %4d'%(i, p_h[0], p_h[1], lx, ly, rx-lx, ry-ly)
					i = raw_input('Select number [0-%d] and press enter: '%(q_pids_handles-1))
					try: i = int(i)
					except ValueError: continue
					if 0 <= i < q_pids_handles:
						self.pid = pids_handles[i][0]
						self.__handle = pids_handles[i][1]
						return
				else:
					self.pid = pids_handles[self.given_handle_number][0]
					self.__handle = pids_handles[self.given_handle_number][1]
					return

	def handle(self):
		if not self.__handle or win32process.GetWindowThreadProcessId(self.__handle)[1] != self.pid:
			self.__handle = self.pid_handle_detect((self.pid, ))
		return self.__handle

	def windowInfo(self):
		return 'pid %d, handle %s, position %s'%(self.pid, self.handle(), self.getPosition())

	def setPosition(self, x0, y0, wdt, hgh):
		win32gui.SetWindowPos(self.handle(), 0, x0, y0, wdt, hgh, 0)

	def getPosition(self):
		lx, ly, rx, ry = win32gui.GetWindowRect(self.handle())
		return (lx, ly, rx - lx, ry - ly)	# x0, y0, wdt, hgh

	def show(self):
		win32gui.SetForegroundWindow(self.handle())
		sleep(0.2)
		win32gui.ShowWindow(self.handle(), 9)
		sleep(0.2)

if __name__ == '__main__':
	import time

	prg = raw_input('Enter the window name or pid: ')
	if not prg:
		prg = 'firefox.exe'	# firefox cmd
	try:
		prg = int(prg)
	except ValueError:
		pass

	w = WWindow(prg)
	while 1:
		try:
			print '\r', 50*' ',
			print '\r', w.windowInfo(),
			time.sleep(1)
		except KeyboardInterrupt:
			break
	print
	w.setPosition(10,10,500,500)
