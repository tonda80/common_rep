#coding=utf8

import argparse
import os
import sys

def get_args():
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', help='Input file name', required=True)
	parser.add_argument('-s', help='Output size (Mb)', required=True, type=int)
	return parser.parse_args()

def get_tag_value_edge(ba_line, tag):
	f_beg = '\x01%s='%tag		# NOT first tag!!
	f_end = '\x01'

	e1 = 0
	while 1:
		e0 = ba_line.find(f_beg, e1)
		if e0 == -1:
			return
		e0 += len(f_beg)
		e1 = ba_line.find(f_end, e0)
		if e1 == -1:
			e1 = len(ba_line)
		assert e0 < e1
		yield (e0, e1)

def add_tag_value_label(ba_line, tag, label):
	for e0, e1 in get_tag_value_edge(ba_line, tag):
		b = ba_line.rfind('[', e0, e1)
		e = ba_line.rfind(']', e0, e1)
		if b == -1 or e == -1 or b >= e:
			b = e = e1
		else:
			e += 1
		ba_line[b:e] = '[%s]'%label

def replace_tag_value(ba_line, tag, new_value):
	for e0, e1 in get_tag_value_edge(ba_line, tag):
		ba_line[e0:e1] = str(new_value)

def get_tag_value(ba_line, tag):
	e0, e1 = get_tag_value_edge(ba_line, tag).__iter__().next()
	return ba_line[e0:e1]

class InputLogFile:
	def __init__(self, fname):
		with open(fname, 'rb') as file_:
			self.src_lines = map(bytearray, file_.readlines())

		self.pack_cnt = 0
		self.seqnum = 0
		self.out_name = fname + '.mult'

	# 34 MsgSeqNum int, 11 ClOrdID str, 41 OrigClOrdID str, 37 OrderID str, 17 ExecID str
	def multiply(self, size):
		id_tags = (11, 41, 37, 17)
		with open(self.out_name, 'wb') as file_:
			while os.path.getsize(self.out_name) < size:
				for l in self.src_lines:
					if self.pack_cnt != 0:
						for tag in id_tags:
							add_tag_value_label(l, tag, self.pack_cnt)
						self.seqnum += 1
						replace_tag_value(l, 34, self.seqnum)

					file_.write(l)

				if self.pack_cnt == 0:	# last seqnum into the source file
					self.seqnum = int(get_tag_value(l, 34))

				self.pack_cnt += 1
				file_.flush()

if __name__ == '__main__':
	g_args = get_args()
	InputLogFile(g_args.i).multiply(g_args.s*2**20)
