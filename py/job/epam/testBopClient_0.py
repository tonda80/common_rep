# EnterOrderMsg(TradingMessage):
# char OrderIdentifier_[14]; char SideIndicator_; uint64_t OrderQty_; char Symbol_[10]; int64_t Price_; uint32_t TimeInForce_; char OrdType_; char OriginatingTrader_[8];

# CancelOrderMsg(TradingMessage):
# char OrderIdentifier_[14]

# ReplaceOrderMsg(TradingMessage):
# char ExistingOrderIdentifier_[14]; char ReplaceOrderIdentifier_[14]; uint64_t OrderQty_; int64_t Price_;


instr = '1986306'
case = 'c'
ordId = 'order_id11_34'
newOrdId = ordId + 'n'

if case == 'e':
	self.send(EnterOrderMsg, ordId, 'B', 1000000000, instr, 1100000000, 0, '2', 'epafen01')
elif case == 'c':
	self.send(CancelOrderMsg, ordId)
elif case == 'r':
	self.send(ReplaceOrderMsg, ordId, newOrdId, 1000000000, 1000000000)
