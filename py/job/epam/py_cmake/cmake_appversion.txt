file (COPY "${CMAKE_CURRENT_LIST_DIR}/generate_AppVersion.py" DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# VC only
add_custom_command(
	TARGET ${PROJECT_NAME}
	PRE_BUILD
	COMMAND python generate_AppVersion.py "${CMAKE_CURRENT_BINARY_DIR}/include/AppVersion.h" "${CMAKE_SOURCE_DIR}"
)

# common way
add_custom_target(
	helperTarget_${PROJECT_NAME}
	COMMAND python generate_AppVersion.py ${CMAKE_CURRENT_BINARY_DIR}/include/AppVersion.h ${CMAKE_SOURCE_DIR}
	VERBATIM
)
add_dependencies(${PROJECT_NAME} helperTarget_${PROJECT_NAME})
