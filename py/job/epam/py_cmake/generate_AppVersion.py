# coding=utf8

APP_VERSION_MAJOR = 0
APP_VERSION_MINOR = 1
APP_BRANCH_REV = 3		# TODO remove it

import sys
import subprocess
from os import path as os_path

def error_exit(what):
	sys.exit("generate_AppVersion.h error: %s"%what)

class ConfigWriter:
	def __init__(self, argv):
		if len(argv) < 3:
			error_exit("Wrong arguments")
		self.out_file = open(argv[1], 'w')
		self.root_dir = argv[2]

	def add_string(self, st):
		self.out_file.write('%s\n'%st)

	def add_parameter(self, type, name, value):
		self.out_file.write('const %s %s = %s;\n'%(type, name, value))

	def add_int_parameter(self, name, value):
		self.add_parameter('int', name, value)

	def add_string_parameter(self, name, value):
		self.add_parameter('char*', name, '"%s"'%value)

	def add_parameter_with_revision(self, name, rel_path='.'):
		if rel_path == '.':
			path = '.'
		else:
			path = os_path.join(self.root_dir, '..', rel_path)
		self.add_string_parameter(name, self.get_revision(path))

	def get_revision(self, path):
		git_cmd = ['git', 'log', '-1', '--format=<%ad>, <%H>']		# <%B>, <%ae>
		try:
			ret = subprocess.check_output(git_cmd, cwd=path)
		except (OSError, subprocess.CalledProcessError) as e:
			error_exit('Cannot execute git command into "%s" [%s]'%(path, e))

		return ret.strip().replace('\r\n', ' ').replace('\n', ' ')

# -------------------------

configWriter = ConfigWriter(sys.argv)

configWriter.add_string(
'''
#ifndef _APP_VERSION_H_
#define _APP_VERSION_H_

namespace ttrt { namespace fxts { namespace common {
''')

configWriter.add_int_parameter('APP_VERSION_MAJOR', APP_VERSION_MAJOR)
configWriter.add_int_parameter('APP_VERSION_MINOR', APP_VERSION_MINOR)
configWriter.add_int_parameter('APP_BRANCH_REV', APP_BRANCH_REV)
configWriter.add_parameter_with_revision('APP_REV')
configWriter.add_parameter_with_revision('APP_COMMON_CPP_REV', 'common-cpp')
configWriter.add_parameter_with_revision('APP_DATA_MODEL_ALL_REV', 'data-model-all')
configWriter.add_parameter_with_revision('APP_NETWORK_COMMUNICATION_REV', 'network-communication-cpp')

configWriter.add_string(
'''
}}}		// ttrt::fxts::common

#endif	// _APP_VERSION_H_
''')
