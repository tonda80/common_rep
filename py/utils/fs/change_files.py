#coding=utf8

# Как-то модифициреум файлы


import os

import baseapp


class AppError(RuntimeError):
	pass


class App(baseapp.BaseWalkerApp):
	def add_arguments(self, parser):
		parser.add_argument('--log_only', action='store_true', help=u'Только лог')
		parser.add_argument('--remove_spaces', action='store_true', help=u'Удалить пробелы из имен файлов')

	def remove_spaces(self, path):
		dirname, filename = os.path.split(path)
		if ' ' in filename:
			self.rename(path, os.path.join(dirname, filename.replace(' ', '_')))

	def rename(self, old, new):
		self.log.info('Renaming "{}" => {}'.format(old, new))
		if not self.args.log_only:
			return
		#os.path.rename(old, new)

	def main(self):
		if self.args.remove_spaces:
			self.file_callback = self.remove_spaces
		#elif :
		else:
			raise AppError('nothing to do')
		self.walk()


if __name__ == '__main__':
	App().main()
