#coding=utf8

# Пытается найти одинаковые файлы в заданной директории


import collections
import os
import sys
import subprocess
import platform
import shutil
import hashlib
import tempfile

import baseapp
from lsh import start as lsh_start


def md5_digest(path, buf_size=2**16):
	with open(path, "rb") as f:
		md5 = hashlib.md5()
		for chunk in iter(lambda: f.read(buf_size), b''):
			md5.update(chunk)
		return md5.hexdigest()

def _print(s):
		sys.stdout.write(s)
		sys.stdout.flush()


class App(baseapp.BaseWalkerApp):
	def add_arguments(self, parser):
		parser.add_argument('-i', '--interactive', action='store_true', help=u'Интерактивное взаимодействие')
		parser.add_argument('--exclude_dirs', help=u'Список директорий разделенный ";" для исключения')
		parser.add_argument('--exclude_exts', default='', help=u'Список расширений файлов разделенный ";" для исключения. Например, .db;.xml')

	def extension_filter(self, ext):
		return ext not in self.exclude_exts

	def file_callback(self, path):
		rel_dir = os.path.relpath(os.path.dirname(path), self.root_dir)
		if rel_dir in self.exclude_dirs:
			self.log.debug('Skipped %s'%path)
			return
		size = os.path.getsize(path)
		self.size_map.setdefault(size, []).append(path)

	def main(self):
		self.root_dir = os.path.realpath(self.args.root_dir)
		self.backup_dir = None

		self.exclude_dirs = self.args.exclude_dirs.split(';') if self.args.exclude_dirs else []
		self.exclude_exts = self.args.exclude_exts.split(';') if self.args.exclude_exts else []

		self.size_map = collections.OrderedDict() 		# ключ - размер, значение - список файлов
		self.walk()
		for size, ll in self.size_map.iteritems():
			if len(ll) > 1:
				md5_map = collections.OrderedDict()	# ключ - md5, значение - список файлов
				for p in ll:
					md5_map.setdefault(md5_digest(p), []).append(p)
				for md5, ll2 in md5_map.iteritems():
					if len(ll2) > 1:
						self.log.info('The same files size - {}, md5 - {}:'.format(size, md5))
						for p2 in ll2:
							self.log.info('\t'+p2)
						if self.args.interactive:
							self.interactive_actions(ll2)

	def interactive_actions(self, path_list):
		while 1:
			cmd = raw_input('\nInput the command ("h" for getting help): ')
			def get_n_path(cmd):
				s = cmd[1:]
				if s == 'a':
					for p in path_list:
						yield p
				else:
					try:
						yield path_list[int(s)]
					except Exception as e:
						self.log.error('Bad command [%s]: %s'%(cmd, e))

			if cmd == 'h':
				_print(
u'''
h    (help) помощь
n    (next) переходим к следующему списку
q    (quit) выход из программы
l    (list) вывести текущий список путей
dN   (dir) открыть директорию N-го файла, da - открыть все
oN   (open) открыть N-й файл, oa - открыть все
rN   (remove) удалить N-й файл, ra - удалить все
''')
			elif cmd == 'n':
				break
			elif cmd == 'q':
				sys.exit(0)
			elif cmd == 'l':
				for i,p in enumerate(path_list): _print('%d: %s\n'%(i, p))
			elif cmd.startswith('o'):
				for p in get_n_path(cmd):
					lsh_start(p)
			elif cmd.startswith('d'):
				for p in get_n_path(cmd):
					lsh_start(os.path.dirname(p))
			elif cmd.startswith('r'):
				for p in get_n_path(cmd):
					self.rm_path(p)
			else:
				print 'Error. Bad command [%s]'%(cmd,)

	def get_backup_dir(self):
		if not self.backup_dir:
			dir_ = os.path.abspath(os.path.join(self.root_dir, os.pardir))
			self.backup_dir = tempfile.mkdtemp(prefix='same_file.py_dir_', dir=dir_)
		return self.backup_dir

	def rm_path(self, path):
		log_file_name = '__log.txt'
		if not os.path.exists(path):
			self.log.warn('%s does not exist. Already removed?'%path)
			return
		rel_path = os.path.relpath(path, self.root_dir)
		if rel_path == log_file_name:
			self.log.warn('We cannot move %s. Skip it'%path)
			return
		bkp_dir = self.get_backup_dir()
		dst_dir = os.path.join(bkp_dir, os.path.dirname(rel_path))
		if not os.path.isdir(dst_dir):
			os.makedirs(dst_dir)
		s = '%s /=>/ %s'%(path, dst_dir)
		with open(os.path.join(bkp_dir, log_file_name), 'a') as file_:
			file_.write(s)
			file_.write('\n')
		self.log.info(s)
		shutil.move(path, dst_dir)

if __name__ == '__main__':
	App().main()
