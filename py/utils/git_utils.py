#!/usr/bin/python
# coding=utf8

# Попытаюсь тут автоматизировать часто встречающиеся гит операции


import os
import subprocess
import re

from baseapp import BaseConsoleApp


class AppError(RuntimeError):
	pass


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		# TODO! переделать на https://docs.python.org/2.7/library/argparse.html#sub-commands
		parser.add_argument('-t', '--target', required=True, help=u'Что делаем: см main для справки',
			choices=('subm_merge', 'find_comm', 'subm_diff',
			)
		)
		parser.add_argument('--rep', help='Path to git repository')
		parser.add_argument('--subm_path', help='Relative path to submodule')
		parser.add_argument('--subj', help='Part of subject of commit')
		parser.add_argument('--tcmt', help='Their commit')
		parser.add_argument('--ocmt', help='Our commit')

	def __init__(self):
		BaseConsoleApp.__init__(self)
		self.git = Git(self, self.args.rep)

	def main(self):
		if self.args.target == 'subm_merge':		# инфа о разнице коммитов в мерженных сабмодулях
			self.log.info(self.submodule_merge_info(*self.get_required_cli_arguments('subm_path')))
		elif self.args.target == 'find_comm':		# пытаемся найти все версии коммита в репе по комменту
			self.log.info(self.find_commit(*self.get_required_cli_arguments('subj')))
		elif self.args.target == 'subm_diff':		# каких коммитов из tcmt нет в ocmt
			args = self.get_required_cli_arguments('tcmt', 'ocmt')
			# subm_path тут опциональный.. вообще криво как-то
			args.append(self.args.subm_path)
			self.log.info(self.commit_diff_info(*args))

		return 0

	# для человеческого вывода что нужные аргументы не заданы, argparse не поддерживает иерархию аргументов
	# TODO удалить это
	def get_required_cli_arguments(self, *names):
		res = []
		err = False
		for name in names:
			val = getattr(self.args, name, None)
			if val is None:
				self.log.error('No needed CLI argument: {}'.format(name))
				err = True
			else:
				res.append(val)
		if err:
			raise RuntimeError('\nNo needed CLI arguments. See log above.')
		return res

	def find_commit(self, subject):
		report = 'commit subject: {}\npossible commits:\n'.format(subject)
		for commit in Commit.get_commit_list(self.git, '--all', '--grep', subject):
			report += commit.human_repr()
			report += '\nbranches: {}\n\n'.format(list(self.get_commit_branches(commit.sha)))
		return report

	def submodule_merge_info(self, submodule_path):
		theirs_sha, ours_sha = self.submodule_merge_shas(submodule_path)
		return self.commit_diff_info(theirs_sha, ours_sha, submodule_path)

	def submodule_merge_shas(self, submodule_path):
		re_pattern1 = re.compile(r'index (\w+)..(\w+) \d+')	# index 58f931a..7ada05c 160000
		re_pattern2 = re.compile(r'index (\w+),(\w+)..\d+')	# index cbc2093,bb3e52f..0000000
		git_out = self.git('diff', '--', submodule_path)
		for l in git_out.splitlines():
			mo = re_pattern1.match(l)
			if mo:
				theirs_sha, ours_sha = mo.group(1, 2)	# но это не точно
				return theirs_sha, ours_sha
			mo = re_pattern2.match(l)
			if mo:
				ours_sha, theirs_sha = mo.group(1, 2)	# и это не точно
				return theirs_sha, ours_sha
		raise AppError('Cannot get submodule\'s sha from:\n {}'.format(git_out))

	def commit_diff_info(self, theirs, ours, cwd=None):
		self.git('fetch', cwd=cwd)
		theirs_submodule_branches = list(self.get_commit_branches(theirs, cwd=cwd))
		ours_submodule_branches = list(self.get_commit_branches(ours, cwd=cwd))

		report = '\n'
		report += 'Their commit: {}\n'.format(theirs)
		report += 'Their commit branches: {}\n'.format(' '.join(theirs_submodule_branches))

		report += 'Our commit: {}\n'.format(ours)
		report += 'Our commit branches: {}\n'.format(' '.join(ours_submodule_branches))

		# список для определения уже возможно черри-пикнутого в ours
		ident_list = [commit.ident for commit in Commit.get_commit_list(self.git, ours, '^'+theirs, cwd=cwd)]

		# выводим список того что возможно надо переносить
		report += '\nNew commits (git log {} ^{}):\n'.format(theirs, ours)
		for commit in Commit.get_commit_list(self.git, theirs, '^'+ours, cwd=cwd):
			if commit.subject.startswith('Merge branch ') or commit.subject.startswith('Merge remote-tracking branch '):
				continue
			if commit.ident in ident_list:
				self.log.info('{} is cherry-picked already'.format(commit))
				continue
			report += commit.human_repr() + '\n\n'

		return report

	# по дефолту отдаем только remotes/origin/ ветки
	def get_commit_branches(self, commit, pref='remotes/origin/', **kw):
		for l in self.git('branch', '-a', '--contains', commit, **kw).splitlines():
			l = l.strip()
			if l.startswith(pref):
				yield l.lstrip(pref)


class Git:
	def __init__(self, owner, root_dir):
		if root_dir is None:
			root_dir = os.getcwd()
		self.root_dir = os.path.realpath(root_dir)
		self.owner = owner

	# args - аргументы гита, kw - доп. кастомизация
	def __call__(self, *args, **kw):
		cmd = ['git']
		cmd.extend(args)

		cwd = kw.get('cwd')
		if cwd is None:
			kw['cwd'] = self.root_dir
		elif not os.path.isabs(cwd):
			kw['cwd'] = os.path.join(self.root_dir, cwd)

		extra_kw = {}
		for extra_option in ('ignore_error',):
			extra_kw[extra_option] = kw.pop(extra_option, None)

		self.owner.log.debug('{} $ {}'.format(kw['cwd'], ' '.join(cmd)))
		try:
			return subprocess.check_output(cmd, **kw)
		except subprocess.CalledProcessError as e:
			if extra_kw.get('ignore_error'):
				return ''
			raise


class Commit:
	def __init__(self, log_line):	# see __LOG_ARGS
		items = log_line.split(self.__delimiter)
		self.sha = items[0]
		self.ident = '{} {}'.format(items[1], items[2])		# для идентификации черрипик коммитов. у них отличается sha но одинаковы автор и дата. коммент может отличаться
		self.subject = items[3]
		self.date = items[4]
		self.author = items[5]
		self.mail = items[1]

	def human_repr(self):
		return '{}\nAuthor:\t{} {}\nDate:\t{}\n\t{}'.format(self.sha, self.author, self.mail, self.date, self.subject)

	__delimiter = '__<[{dlmtr}]>__'
	__LOG_ARGS = ('log', '--format=%H{d}%ae{d}%at{d}%s{d}%ad{d}%an'.format(d=__delimiter))
	# %H: commit hash, %ae: author email, %at: author date, UNIX timestamp, %s: subject, %ad: author date, %an: author name
	@staticmethod
	def get_commit_list(git, *log_filter, **kw):
		out = git(*(Commit.__LOG_ARGS + log_filter), **kw)
		commits = []
		for l in out.splitlines():
			yield Commit(l)


if __name__ == '__main__':
	exit(App().main())
