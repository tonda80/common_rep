#coding=utf8

from pyscreenshot import grab
from datetime import datetime
from sys import argv
from os.path import isdir, sep

if len(argv) < 2:
	print "It's necessary to enter a dirname for screenshots files"
	exit(1)
	
dir_ = argv[1]
if isdir(dir_) == 0:
	print "Directory is not exist"
	exit(1)

img = grab()

timestamp = datetime.now().strftime('%y.%m.%d-%H.%M.%S.%f')
img.save(r'%s%s%s.jpg'%(dir_, sep, timestamp))
