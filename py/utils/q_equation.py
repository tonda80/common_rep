import sys

coeff = [0, 0, 0]
for i in xrange(3):
	try:
		coeff[i] = float(sys.argv[i+1])
	except IndexError:
		pass
a, b, c = coeff
print 'Equation:\n%f*x**2 + %f*x + %f'%(a, b, c)

if a == 0:
	print 'It\'s too easy. Bye.'
	exit(0)

d = b**2 - 4*a*c
if d < 0:
	d = complex(d)
sd = d**0.5
	
x1 = (-b + sd)/(2*a)
x2 = (-b - sd)/(2*a)
print 'Result:\n%s\n%s'%(x1, x2)
