import datetime
from time import sleep
import sys

while 1:
	s_bd = raw_input('When are you born? Default is "1980 6 18": ').strip()
	if not s_bd:
		s_bd = '1980 6 18'

	s_hm = raw_input('Specify hours and minutes if you wish. Default is "21 5": ').strip()
	if not s_hm:
		s_hm = '21 5'

	try:
		dt_begin = datetime.datetime.strptime(s_bd + ' ' + s_hm, '%Y %m %d %H %M')
	except ValueError as e:
		print '\n', e
		print 'Something is wrong. Repeat.\n'
		continue
	break

print
while 1:
	td = datetime.datetime.now() - dt_begin
	days = td.days
	weeks = days/7
	seconds = td.total_seconds()
	hours = seconds/3600
	minutes = seconds/60

	out = 'You are %d weeks or %d days or %d hours or %d minutes or %d seconds old'%(
		weeks, days, hours, minutes, seconds)

	print out, '\r',

	sys.stdout.flush()
	sleep(1)
