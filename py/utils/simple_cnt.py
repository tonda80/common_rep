# coding=utf8

from uTkinter import *
from commonconfig import CommonConfig

class App:
	def __init__(self):
		self.conf = CommonConfig('simple_cnt', 0)
		self.root = Tk()
		packArgs = {'side':LEFT, 'padx':10, 'pady':10}
		self.label = uMutableLabel(self.root, self.conf.get(), IntVar, width=5, font=("Helvetica", 20, "bold italic"), gmArgs=packArgs)
		uButton(self.root, self.btnUp, 'Up', width=7, gmArgs=packArgs)
		uButton(self.root, self.btnDown, 'Down', width=7, gmArgs=packArgs)
		uButton(self.root, self.btnReset, 'Reset', width=7, gmArgs=packArgs)
		self.root.protocol("WM_DELETE_WINDOW", self.delete_window)

	def btnUp(self):
		self.label.setVar(self.label.getVar() + 1)

	def btnDown(self):
		self.label.setVar(self.label.getVar() - 1)

	def btnReset(self):
		self.label.setVar(0)

	def delete_window(self):
		self.conf.set(self.label.getVar())
		self.conf.save()
		self.root.destroy()

App().root.mainloop()
