﻿import sys
import os
import time
import subprocess
import json

import win32gui
import win32process
from winhook import keyIsPressed

import win32clipboard
import pyautogui

import tkMessageBox
import tkSimpleDialog
sys.path.append(os.path.abspath(os.path.join('..', '..', 'modules')))
from uTkinter import *
import lsh

# TODOs
## remake all :)

CONFIG_FILE = 'puttyHelper.conf'

def set_clipboard(text):
	win32clipboard.OpenClipboard()
	win32clipboard.EmptyClipboard()
	win32clipboard.SetClipboardText(text, win32clipboard.CF_UNICODETEXT)
	win32clipboard.CloseClipboard()


class PuttyWindow:
	def __init__(self, pid):
		self.pid = pid
		self.__wHandle = None

	def copy_from(self, wnd):
		self.pid = wnd.pid
		self.__wHandle = wnd.__wHandle

	def wHandle(self):
		if not self.__wHandle or win32process.GetWindowThreadProcessId(self.__wHandle)[1] != self.pid:
			self.__wHandle = self.findWndHndl()
		return self.__wHandle

	def __str__(self):
		return 'pid %d, handle %s, position %s'%(self.pid, self.wHandle(), self.getPosition())

	def findWndHndl(self):
		def _clb(whndl, testPid):
			tid, pid = win32process.GetWindowThreadProcessId(whndl)
			if pid == testPid:
				#print tid, pid, whndl
				res.append(whndl)
				return False

		for i in xrange(5):
			res = []
			time.sleep(0.1)
			try:
				win32gui.EnumWindows(_clb, self.pid)
			except win32gui.error:
				pass #'Looks like this exception is some bug'
			if res:
				return res[0]
		return None

	def setPosition(self, obj):
		if self.wHandle() is None:
			appMessage('Can\'t get handle for window')
			return
		crd = [int(s) for s in obj.split(',')]	#'x, y, width, height'
		print 'setPosition:', crd
		win32gui.SetWindowPos(self.wHandle(), 0, crd[0], crd[1], crd[2], crd[3], 0)

	def getPosition(self):
		hndl = self.wHandle()
		if hndl is None:
			return ()
		lx, ly, rx, ry = win32gui.GetWindowRect(hndl)
		return '%s,%s,%s,%s'%(lx, ly, rx - lx, ry - ly)	# SetWindowPos input format

	def showWindow(self):
		hndl = self.wHandle()
		win32gui.SetForegroundWindow(hndl)
		time.sleep(0.2)
		win32gui.ShowWindow(hndl, 9)

class Session():
	def __init__(self, label, parameters):  # from item of dictionnary
		self.label = label
		self.parameters = parameters
		self.parameters.setdefault('winscp_proto', 'sftp')
		self.home_dir = self.parameters.get('home_dir', None)
		self.windows = []
		
	def isOpened(self):
		return bool(self.windows)

	def __str__(self):
		return 'Session %s: %s'%(self.label, self.parameters)

	def toObj(self):
		return (self.label, self.parameters)

	def getPuttyArgs(self):
		p = self.parameters
		return ['%s@%s'%(p['login'], p['ip']), '-pw', '%s'%p['password']]

	def getWinscpArgs(self):
		p = self.parameters
		return ['%s://%s:%s@%s'%(p['winscp_proto'], p['login'], p['password'], p['ip'])]

	def printArgs(self, args):
		for a in args:
			print a,
		print ''

	def __create(self, winscp, custom_position = None):
		if winscp:
			args = [g_config.winscp] + self.getWinscpArgs()
		else:
			args = [g_config.putty] + self.getPuttyArgs()
		self.printArgs(args)

		pid = subprocess.Popen(args).pid
		wnd = None
		if not winscp:	# winscp window doesn't interest us
			if pid > 0:
				wnd = PuttyWindow(pid)

			if custom_position is not None:
				wnd.setPosition(custom_position)
			elif self.parameters.has_key('position'):
				wnd.setPosition(self.parameters['position'])

		return wnd

	def open(self, winscp, custom_position = None):
		wnd = self.__create(winscp, custom_position)
		if (wnd):
			self.windows.append(wnd)
		return wnd

	def __kill(self, wnd):
		try:
			os.kill(wnd.pid, 9)
		except WindowsError:
			appMessage('Window (%d) cannot be killed'%wnd.pid)
			return False
		return True

	def close(self, wnd):
		self.__kill(wnd)
		try:
			self.windows.remove(wnd)
		except ValueError:
			appMessage('Unknown window')

	def showWindow(self, wnd, position=None):
		if (position):
			wnd.setPosition(position)
		wnd.showWindow()

	def restart(self, wnd):
		position = wnd.getPosition()
		if (self.__kill(wnd)):
			wnd.copy_from(self.__create(False, position))

	def closeAll(self):
		for w in self.windows[:]:
			self.close(w)

	def restartAll(self):
		for w in self.windows[:]:
			self.restart(w)

	def isEqual(self, other):
		return self.label == other.label

	def go_home(self, wnd):
		if self.home_dir:
			self.make_command(wnd, 'cd %s'%self.home_dir)

	def make_command(self, wnd, cmd):
		wnd.showWindow()
		x,y,w,h = [int(s) for s in wnd.getPosition().split(',')]
		set_clipboard('\x08'*30 + cmd + '\n')	# reset of input, command, enter
		curr_pos = pyautogui.position()
		pyautogui.click(x+w/2, y+h/2, button='right')
		pyautogui.moveTo(*curr_pos)


class SessionContainer(list):
	def __init__(self, sessions, default_parameters):
		list.__init__(self)
		sessionNames = sessions.keys()
		sessionNames.sort()
		for name in sessionNames:
			parameters = sessions[name]
			for k,v in default_parameters.iteritems():
				parameters.setdefault(k, v)
			self.append(Session(name, parameters))
			
	def areOpened(self):
		for s in self:
			if s.isOpened():
				return True
		return False

	def __str__(self):
		ret = 'SessionContainer\n'
		for s in self: ret += str(s)+'\n'
		return ret

	def contains(self, o):
		for s in self:
			if s.isEqual(o):
				return s
		return None

	def printWindowsInfo(self):
		for s in self:
			print s.label
			for w in s.windows:
				print '\t%s'%w

	def closeAll(self):
		for s in self:
			s.closeAll()

	def restartAll(self):
		for s in self:
			s.restartAll()

class Config:
	def __init__(self, fName):
		self.all = self.fromJsonFile(fName)
		self.putty = self.all['putty']
		self.winscp = self.all['winscp']
		self.defaultTop = self.all.setdefault('defaultTop', 1)
		self.sessions = self.all['sessions']
		
		self.default_session_parameters = {}
		if 'default_session_parameters' in self.all:
			self.default_session_parameters = self.all['default_session_parameters']
		
		# filter
		for k in self.sessions.keys():
			if k.startswith('__'):
				del self.sessions[k]

	def fromJsonFile(self, fName):
		with open(fName) as of:
			return json.load(of)

	def toJsonFile(self, fName):
		with open(fName, 'w') as of:
			json.dump(self.all, of, indent=2, separators=(',', ' : '), sort_keys=True)

class MainWindow:
	def __init__(self):
		self.root = Tk()
		self.root.title(u'PuTTY helper')
		
		self.root.protocol("WM_DELETE_WINDOW", self.delete_window)

		self.status_bar = StatusBar(self.root)
		sess_lframe = uLabelFrame(self.root, 'Sessions', gmArgs = {'expand':YES, 'fill':BOTH})
		self.sess_frame = uScrollFrame(sess_lframe, 'y', gmArgs = {'expand':YES, 'fill':BOTH})

		ctrl_frame = uFrame(sess_lframe, gmArgs = {'expand':NO, 'fill':X})
		self.root.wm_attributes('-topmost', g_config.defaultTop)
		uCheckbutton(ctrl_frame, 'Top', uCallback(self.topControl), g_config.defaultTop, gmArgs={'side':RIGHT})
		#self.root.bind("<Button-1>", lambda ev: self.status_bar.set(''))	# clear status

		uButton(ctrl_frame, self.printInfo, 'Info', width=4, gmArgs = {'side':RIGHT})

		uButton(ctrl_frame, lambda: os.startfile(CONFIG_FILE), 'Config', width=6, gmArgs = {'side':RIGHT})
		uButton(ctrl_frame, self.reloadConfig, 'Reload', width=6, gmArgs = {'side':RIGHT})
		helpmsg = 'w - Winscp mode\nq - Close putty window mode (Show is default)\ne - Edit name mode\nr - Restart mode\nh - Home mode'
		helpmsg += '\n\nScreeen scheme:\n1 | 2\n3 | 4'
		uButton(ctrl_frame, lambda: tkMessageBox.showinfo('Press keys to:', helpmsg), 'Help', width=6, gmArgs = {'side':RIGHT, 'padx':5})

		uButton(ctrl_frame, self.closeAllOwnWindows, 'Close all', width=8, gmArgs = {'side':LEFT})
		uButton(ctrl_frame, self.restartAllWindows, 'Restart all', width=8, gmArgs = {'side':LEFT})
		uButton(ctrl_frame, lambda: lsh.killall('PUTTY.EXE'), 'Kill all', width=3, font=("Arial", 5), gmArgs = {'side':LEFT})

		self.root.bind('<KeyRelease>', self.keyRelease)
		self.root.bind('<KeyPress>', self.keyPress)

		# --- logic
		self.sessions = SessionContainer(g_config.sessions, g_config.default_session_parameters)
		for s in self.sessions:
			self.createSessionHandle(s)

		self.root.minsize(500, 0)

		h = len(self.sessions)*40
		max_h = self.root.winfo_screenheight() - 80
		h = min(h, max_h)
		h = max(h, 200)
		placeWindow(self.root, 200, 10, 0, h)

		self.puttyButtons = []
		
	def delete_window(self):
		if self.sessions.areOpened() and tkMessageBox.askyesno('','Close all?', parent = self.root):
			self.sessions.closeAll()
		
		self.root.destroy()
		

	def keyRelease(self, ev):
		pos_mode = self.customPositionMode()
		if pos_mode:
			self.status_bar.set('Custom position mode - %s'%pos_mode)
		else:
			self.status_bar.set('')

	def keyPress(self, ev):
		#print ev.keycode, ev.__dict__
		key = ev.keysym
		if key == 'q':
			self.status_bar.set('Closing putty windows mode')
		elif key == 'w':
			self.status_bar.set('WinSCP mode')
		elif key == 'e':
			self.status_bar.set('Edit name mode')
		elif key == 'r':
			self.status_bar.set('Restart mode')
		elif key == 'h':
			self.status_bar.set('Home mode')
		else:
			pos_mode = self.customPositionMode()
			if pos_mode:
				self.status_bar.set('Custom position mode - %s'%pos_mode)

	def reloadConfig(self):
		config = Config(CONFIG_FILE)
		sessions = SessionContainer(config.sessions, config.default_session_parameters)
		for s in sessions:
			existS = self.sessions.contains(s)
			if existS:	# this session was early
				existS.parameters = s.parameters
			else:
				self.createSessionHandle(s)
				self.sessions.append(s)
		print self.sessions

	def closeAllOwnWindows(self):
		appMessage('Closing all!', 'Info')
		self.sessions.closeAll()
		for b in self.puttyButtons: b.destroy()

	def restartAllWindows(self):
		appMessage('Restart all!', 'Info')
		self.sessions.restartAll()

	def printInfo(self):
		print 'Windows info'
		self.sessions.printWindowsInfo()

	def topControl(self, srcBtn):
		self.root.wm_attributes('-topmost', int(srcBtn.getVar()))

	def createSessionHandle(self, session):
		frame = uFrame(self.sess_frame, relief = FLAT, gmArgs = {'expand':YES, 'fill':BOTH})
		uLabel(frame, session.label, width = 20, anchor = W, gmArgs = {'side':LEFT, 'padx':5})
		uButton(frame, uCallback(self.openSession, session, frame), 'Open', gmArgs = {'side':LEFT, 'padx':5})

	def openSession(self, srcBtn, session, frame):
		winscpFlag = keyIsPressed('w')
		wnd = session.open(winscpFlag, self.selectCustomPosition())	# winscp window is None
		if wnd:
			self.puttyButtons.append(uButton(frame, uCallback(self.sessionCallback, session, wnd), str(wnd.pid), gmArgs = {'side':LEFT}))
		elif not winscpFlag:
			appMessage('Can\'t create the session')

	def sessionCallback(self, srcBtn, session, wnd):
		if keyIsPressed('q'):
			session.close(wnd)
			srcBtn.destroy()
		elif keyIsPressed('e'):
			while keyIsPressed('e'): time.sleep(0.1)
			new_name = tkSimpleDialog.askstring('New button name', '')
			if new_name:
				srcBtn.config(text = new_name)
		elif keyIsPressed('r'):
			session.restart(wnd)
		elif keyIsPressed('h'):
			while keyIsPressed('h'): time.sleep(0.1)
			session.go_home(wnd)
		else:
			session.showWindow(wnd, self.selectCustomPosition())

	def customPositionMode(self):
		res = ''
		for s in '1234':
			if keyIsPressed(s):
				res += s
		return res

	def selectCustomPosition(self):
		#tkSimpleDialog.askstring('Position code', '\n\nValid combinations:\n13(default), 24, 12, 34, 1234, 1, 2, 3, 4')
		customPositionStr = self.customPositionMode()
		if not customPositionStr:
			return None

		h = self.root.winfo_screenheight()
		w = self.root.winfo_screenwidth()

		bi = 48	# bottom indent (TODO: parameter that, add the rest of indents)

		#'x, y, width, height'
		pos_map = {
			'13':(0,0,w/2,h-bi), '24':(w/2,0,w/2,h-bi), '12':(0,0,w,h/2), '34':(0,h/2,w,h/2-bi),
			'1234':(0,0,w,h-bi), '1':(0,0,w/2,h/2), '2':(w/2,0,w/2,h/2), '3':(0,h/2,w/2,h/2-bi), '4':(w/2,h/2,w/2,h/2-bi)
			}
		return '%d,%d,%d,%d'%pos_map.get(customPositionStr, (0,0,w/2,h))

	def newSessWindow(self):
		raise NotImplementedError
		w = Toplevel()
		w.title('New')
		row = 0
		fields = {}
		def add_field(name):
			uLabel(w, name, gmArgs = {'row':row, 'column':0, 'sticky':W, 'padx':2, 'pady':2})
			fields[name] = uEntry(w, gmArgs = {'row':row, 'column':1, 'sticky':E, 'padx':2, 'pady':2})
			return row + 1
		fldNames = Session().keys()
		fldNames.sort()
		for n in fldNames:
			row = add_field(n)
		uButton(w, lambda:self.addSession(fields), 'Add', gmArgs = {'row':row, 'column':1, 'sticky':E, 'padx':2, 'pady':2})

	def addSession(self, dct):
		raise NotImplementedError
		if self.sessions.contains(dct):
			tkMessagebox.showerror('Session exists')

	def mainloop(self):
		self.root.mainloop()


def appMessage(msg, level = 'Error'):
	out = '%s: %s'%(level, msg)
	print out
	wnd.status_bar.set(out)


if len(sys.argv) > 1:
	CONFIG_FILE = sys.argv[1]

print 'Path to config: %s'%CONFIG_FILE
g_config = Config(CONFIG_FILE)

wnd = MainWindow()
wnd.mainloop()

#g_config.toJsonFile(CONFIG_FILE)	# pretty formatting
