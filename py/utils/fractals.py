# coding=utf8

from PIL import Image as PILImage
from PIL import  ImageTk, ImageDraw
from uTkinter import *
import math

N_GEN = 14
SH_ANGLE = math.pi/6
CFT_LEN = 0.8
INIT_CFT_LEN = 0.2

def _print(widget, *a1, **a2):
	print widget, a1, a2

class App:
	def __init__(self):
		root = Tk()
		setWindowSize(root, 1024, 768)

		self.cnv = uCanvas(root,gmArgs={'side':TOP})
		Segment.setDrawCallback(self.draw_segment)

		orient='h'
		gmArgs={'side':TOP,'expand':NO, 'fill':X}
		command = lambda *a: self.redraw(None)
		self.parInputs = (
			# uScale(root, 1, 30, 1, defaultValue = '3', command=command, label = 'Generation', orient=orient, gmArgs=gmArgs),
			uSpinbox(root, 1, 20, 1, defaultValue = '3', callback=command, gmArgs=gmArgs),
			uSpinbox(root, 0, 180, 2, defaultValue = '45', callback=command, gmArgs=gmArgs),
			uSpinbox(root, 0.05, 0.95, 0.05, defaultValue = '0.5', callback=command, gmArgs=gmArgs),
			uSpinbox(root, 0.05, 0.95, 0.05, defaultValue = '0.5', callback=command, gmArgs=gmArgs),
			) # n_gen, angle, lDecr, initL
		self.old_parameters = []

		self.root = root

		self.parInputs[0].focus_set()
		self.cnv.bind("<Configure>", self.redraw)

	def redraw(self, event):
		parameters = map(lambda s: s.getVar(), self.parInputs)
		try:
			n_gen = int(parameters[0])
			shAngle = float(parameters[1])/180*math.pi
			lDecr = float(parameters[2])
			initL = float(parameters[3])
		except ValueError:
			return

		if (event is None and parameters == self.old_parameters):	# spinbox callback but no changes
			return
		self.old_parameters = parameters

		self.cnv.delete(ALL)
		cnv_w = self.cnv.winfo_width()
		self.cnv_h = self.cnv.winfo_height()

		#image = PILImage.new('RGB', (cnv_w, self.cnv_h), 'white')
		image = PILImage.new('1', (cnv_w, self.cnv_h))
		self.draw = ImageDraw.Draw(image)

		new_segments = (Segment((cnv_w/2, 0), math.pi/2, self.cnv_h*initL), )
		cnt = 0
		for i in xrange(n_gen):
			segments = new_segments
			new_segments = ()
			for s in segments:
				new_segments += s.make_children(shAngle, lDecr)

		# The application must keep a reference to the image object.
		#self.tkImg = ImageTk.PhotoImage(image)
		self.tkImg = ImageTk.BitmapImage(image)
		self.cnv.create_image(cnv_w/2, self.cnv_h/2, image=self.tkImg)

	def draw_segment(self, beg, end):
		self.draw.line((beg[0], self.cnv_h - beg[1], end[0], self.cnv_h - end[1]), fill='#ffffff')
		#self.draw.line(beg + end, fill='black')
		#self.cnv.create_line(beg[0], self.cnv_h - beg[1], end[0], self.cnv_h - end[1])

class Segment:
	@classmethod
	def setDrawCallback(cls, draw):
		cls.draw = draw

	def __init__(self, beg, angle, length):
		self.angle = angle
		self.length = length
		self.end = (beg[0] + length*math.cos(angle), beg[1] + length*math.sin(angle))
		Segment.draw(beg, self.end)

	def make_children(self, shAngle, lDecr):
		new_len = self.length*lDecr
		return (Segment(self.end, self.angle + shAngle, new_len), Segment(self.end, self.angle - shAngle, new_len))


App().root.mainloop()
