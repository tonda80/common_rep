# coding=utf8

import argparse
import itertools

# пазл не решается. видимо потому что сделали неправильную догадку при решении перебором
# может не должно быть такой ошибки при нормальном алгоритме
class PuzzleError(RuntimeError):
	pass

# ошибка входной строки
class InputStrError(RuntimeError):
	pass

# эти ошибки не должны перехватываться
class ImplementationError(RuntimeError):
	pass

_OPTIONS = set('123456789')

# идентификаторы квадратов - позиции верхних левых ячеек
# [27*i+3*j for i in xrange(3) for j in xrange(3)]
_SQUARE_IDS = (0, 3, 6, 27, 30, 33, 54, 57, 60)


class Cell:
	def __init__(self, pos, value):
		if value == ' ':
			self.options = set(_OPTIONS)	# копия
		elif value in _OPTIONS:
			self.options = set()
		else:
			raise ImplementationError('Wrong a cell value')
		self.value = value
		self.pos = pos
		self.row, self.col = divmod(pos, 9)
		self.square_id = self.row/3*27 + self.col/3*3		# pos of a top left cell of the square

	def empty(self):
		return self.value == ' '

	def set_value(self, value):
		if value not in self.options:
			raise ImplementationError('Set value %s is not in %s'%(value, self.options))
		self.value = value
		self.options = set()

	def set_if_one_option(self):
		if len(self.options) == 1:
			self.value = self.options.pop()
			return True

	def exclude(self, value):
		if value in self.options:
			self.options.remove(value)
			if len(self.options) == 0:
				raise PuzzleError('In %d[%d:%d] was excluded %s and now the cell has no options'%(self.pos, self.row, self.col, value))
			return True

	def coord(self):
		return '%dx%d'%(self.row, self.col)
	def __str__(self):
		return self.__repr__()
	def __repr__(self):
		if self.empty():
			str_options = ''.join(sorted(self.options)) if self.options != _OPTIONS else '?'
			suff_len = (9 - len(str_options))/2
			return '%9s'%(str_options + ' '*suff_len)
		else:
			return '   [%s]   '%self.value

class Solver:
	def __init__(self, args):
		self.args = args

	def _print(self, l):
		if not self.args.quiet:
			print l

	def emphasis_print(self, l, force=False):
		if not self.args.quiet or force:
			print l
			print '-'*len(l)
			print

	def print_state(self, pref, force=False):
		if not self.args.quiet or force:
			print pref
			for i in xrange(9):
				for j in xrange(9):
					print self.cells[i*9+j],
				print
			print

	def solve(self, puzzle_string, first_call = True):
		if first_call:
			self.emphasis_print('Let\' solve')
			self.solution_counter = 0

		expanded_string = self.expand_input(puzzle_string)
		self.cells = [Cell(i, expanded_string[i]) for i in xrange(81)]

		self.print_state('Source state', first_call)

		if self.definite_solution_part():
			self.print_state('Solution %d'%self.solution_counter, True)
		elif args.hypot:
			self.hypothetical_solution_part()	# там рекурсивный вызов solve для возможных вариантов

		if first_call:
			if self.solution_counter == 0:
				self.print_state('Cannot solve', True)
			elif args.hypot:
				self.emphasis_print('Total solutions: %d'%self.solution_counter, True)

	def first_pass(self):
		for cell in self.cells:
			if not cell.empty():
				self.exclude_all(cell)
		self.print_state('State after first pass')

	def definite_solution_part(self):
		self.first_pass()

		while 1:
			ready = True
			progress = False

			for cell in self.cells:
				if cell.empty():	# пытаемся определить
					if cell.set_if_one_option():
						set_reason = 'cell'
					else:
						set_reason = self.check_unique_all(cell)	# None не определили

					if set_reason:				# узнали новую
						self.exclude_all(cell)
						progress = True
						self.print_state('Found %s in cell %d:%d (only option in %s)'%(cell.value, cell.row, cell.col, set_reason))

				if cell.empty():
					ready = False

			if ready:
				self.solution_counter += 1
				return True

			if not progress:
				self.emphasis_print('complicated_exclusion')
				progress = self.complicated_exclusion()
				if progress:
					self.print_state('after complicated_exclusion')

			if not progress:
				return False

	def hypothetical_solution_part(self):
		self.print_state('I have given up. Let\'s guess!')

		# ищем первую клетку для гадания
		for hyp_cell in self.cells:
			if hyp_cell.empty():
				break
		self.emphasis_print('hypothetical_solution_part with the cell %d:%d [%s]'%(hyp_cell.row, hyp_cell.col, hyp_cell))

		state_list = [cell.value for cell in self.cells]
		for option in hyp_cell.options:
			state_list[hyp_cell.pos] = option
			try:
				self.solve(''.join(state_list), False)
			except PuzzleError as e:
				self.print_state('Maybe your algo sucks and you should think better. ' + e.message)
				raise ImplementationError	# TODO попробовать подумать ещё при случае?? см _x03439_x062....

	def get_same_row_cells(self, cell, e = False):
		for rcell in (self.cells[cell.row*9+i] for i in xrange(9)):
			if rcell.empty() and (e or rcell != cell):
				yield rcell

	def get_same_column_cells(self, cell, e = False):
		for rcell in (self.cells[i*9+cell.col] for i in xrange(9)):
			if rcell.empty() and (e or rcell != cell):
				yield rcell

	def get_same_square_cells(self, cell, e = False):
		for rcell in (self.cells[cell.square_id+i*9+j] for i in xrange(3) for j in xrange(3)):	# square_id - позиция верхней левой ячейки квадрата
			if rcell.empty() and (e or rcell != cell):
				yield rcell

	def check_unique_all(self, check_cell):
		for func in (self.get_same_row_cells, self.get_same_column_cells, self.get_same_square_cells):
			options = set(check_cell.options)
			for oth in func(check_cell):
				options -= oth.options
			if len(options) == 1:
				check_cell.set_value(options.pop())
				return func.__name__[9:-6]

	def exclude_all(self, src_cell):
		if src_cell.empty():
			raise ImplementationError('Excluding an empty value')

		for func in (self.get_same_row_cells, self.get_same_column_cells, self.get_same_square_cells):
			for cell in func(src_cell):
				cell.exclude(src_cell.value)

# Сперва исходная постановка задачи)
# ~~~~~~~~
	# проверять что возможный вариант заполнения клетки невозможен
	# например в 1-й строке '_ _ _ 4 5 6 7 8 9'
	# отсюда следует что в других строках первого квадрата не может быть 1
	# так как, тогда в 1-й строке 1 не будет совсем
	# +
	# значения 1,2 могут быть только в 2-х клетках мал. квадрата,
	# это значит, что остальные возможные значения из этих клеток можно исключить
	#
	#  обобщить, есть подозрение что exclude_by_square_analysis и все это должно быть в одном алгоритме
# И что в итоге родил МОЗГ)
# ~~~~~~~
	# для каждого набора (строка, столбец, квадрат)
	# 1) строим мап, с ключами - ненайденными решениями, и значениями - коллекциями возможных ячеек и для каждой такой коллекции
	#   1_1) если ячейки набора-квадрата из одной строки\столбца, удаляем ключ из остальных ячеек строки\столбца (старый exclude_by_square_analysis)
	#   1_2) если ячейки набора-строки\столбца из одного квадрата, удаляем ключ из остальных ячеек квадрата
	# 2) ищем ячейки с одинаковым набором опций; если есть N ячеек с N возможными значениями то эти значения можно удалить из остальных ячеек набора
	def complicated_exclusion(self):
		progress = False
		for g in itertools.chain(
				(self.get_same_row_cells(self.cells[i*9], True) for i in xrange(9)),
				(self.get_same_column_cells(self.cells[i], True) for i in xrange(9)),
				(self.get_same_square_cells(self.cells[i], True) for i in _SQUARE_IDS)
				):
			array = list(g)						# тут не должно быть решенных ячеек
			map_ = self._get_map_of_unknown(array)
			map_source = g.__name__[9:12]		# get_same_row_cells => row etc
			if map_source == 'squ':
				progress |= self._complicated_exclusion_1_1(map_)
			elif map_source == 'row' or map_source == 'col':
				progress |= self._complicated_exclusion_1_2(map_, map_source)

			progress |= self._complicated_exclusion_2(array)

		#import pdb;pdb.set_trace()
		return progress

	def _get_map_of_unknown(self, cells):
		map_ = {}
		for cell in cells:
			for opt in cell.options:
				map_.setdefault(opt, []).append(cell)
		return map_

	def _complicated_exclusion_1_1(self, map_):
		progress = False
		for key, collection in map_.iteritems():
			ref_cell = collection[0]
			if all(c.row == ref_cell.row for c in collection):
				for cell in (c for c in self.get_same_row_cells(ref_cell) if c.square_id != ref_cell.square_id and key in c.options):
					progress = True
					cell.exclude(key)
					self._print('\t excluded %s from %s by analisys of row %d of square %d'%(key, cell.coord(), ref_cell.row, ref_cell.square_id))
			if all(c.col == ref_cell.col for c in collection):
				for cell in (c for c in self.get_same_column_cells(ref_cell) if c.square_id != ref_cell.square_id and key in c.options):
					progress = True
					cell.exclude(key)
					self._print('\t excluded %s from %s by analisys of column %d of square %d'%(key, cell.coord(), ref_cell.col, ref_cell.square_id))
		return progress

	def _complicated_exclusion_1_2(self, map_, attr):
		progress = False
		for key, collection in map_.iteritems():
			ref_cell = collection[0]
			if all(c.square_id == ref_cell.square_id for c in collection):
				for cell in (c for c in self.get_same_square_cells(ref_cell) if getattr(c, attr) != getattr(ref_cell, attr) and key in c.options):
					progress = True
					cell.exclude(key)
					self._print('\t excluded %s from %s by analisys of %s %d AND square %d'%(key, cell.coord(), attr, getattr(ref_cell, attr), ref_cell.square_id))
		return progress

	def _complicated_exclusion_2(self, source_array):
		progress = False
		eq_options = []	# список списков ячеек с равными опциями
		for cell in source_array:
			for e in eq_options:
				if cell.options == e[0].options:
					e.append(cell)
					break
			else:
				eq_options.append([cell])

		for e in eq_options:
			if len(e)<2 or len(e)!=len(e[0].options):
				continue
			for cell in (c for c in source_array if c not in e):
				for value in e[0].options & cell.options:
					progress = True
					cell.exclude(value)
					self._print('\t excluded %s from %s by analisys of intersection %s'%(value, cell.coord(), e[0].options))
		return progress


# Ниже код для сравнения, того что было и того что стало в _exclusion_1_1
# ~~~~~~~
	# в каждом квадрате ищем значения гарантированно принадлежащее строке\столбцу
	# и удаляем это значение из строки\столбца соседних квадратов
	#def exclude_by_square_analysis(self):
		#res = False
		#self.emphasis_print('exclude_by_square_analysis')
		#for base in _SQUARE_IDS:
			#indexes = [base+9*i+j for i in xrange(3) for j in xrange(3)]	# клетки квадрата
			## столбцы -------
			#for col in (0,1,2):
				#other_indexes = indexes[:]			# тут будут индексы клеток не текущего столбца
				#curr_unique_values = set()			# тут будут значения которые есть только в этом столбце
				#for i in [base+col+9*i for i in xrange(3)]:	# для каждой клетки столбца квадрата
					#other_indexes.remove(i)
					#curr_unique_values |= self.cells[i].options
				#for i in other_indexes:
					#curr_unique_values -= self.cells[i].options

				#for value in curr_unique_values:	# исключим значения из столбца в других квадратах
					#_, col_shift = divmod(base+col, 9)
					#was_excluded = False
					#for i in xrange(9):
						#excl_index = col_shift + 9*i
						#if excl_index not in indexes:
							#if self.cells[excl_index].exclude(value):
								#res = was_excluded = True
					#if was_excluded:		# так чтоб не печатать несколько раз
						#self._print('\tValue %s may be only in %d column of the square with id %d'%(value, col, base))

			## строки -------
			#for row in (0,9,18):
				#other_indexes = indexes[:]
				#curr_unique_values = set()
				#for i in [base+row+i for i in xrange(3)]:
					#other_indexes.remove(i)
					#curr_unique_values |= self.cells[i].options
				#for i in other_indexes:
					#curr_unique_values -= self.cells[i].options

				#for value in curr_unique_values:
					#was_excluded = False
					#row_shift, _ = divmod(base+row, 9)
					#for i in xrange(9):
						#excl_index = 9*row_shift + i
						#if excl_index not in indexes:
							#if self.cells[excl_index].exclude(value):
								#res = was_excluded = True
					#if was_excluded:
						#self._print('\tValue %s may be only in %d row of the square with id %d'%(value, row, base))
		#return res

	def	expand_input(self, st):
		# на выходе строка из цифры 1-9 и пробела
		# _ и 0 заменяются на пробел
		# после mult_sign 2 цифры множителя предыдущего символа

		st = st.replace('_', ' ').replace('0', ' ')

		mult_sign = 'x'
		m = st.find(mult_sign)
		while m != -1:
			if m < 1 or m > (len(st)-3):
				raise InputStrError('Wrong string "%s". Wrong multiplier position %d'%(st, m))
			try:
				mlt = int(st[m+1:m+3])
			except ValueError:
				raise InputStrError('Wrong string "%s". Wrong multiplier after %d'%(st, m))
			st = st[:m-1] + st[m-1]*mlt + st[m+3:]
			m = st.find(mult_sign)

		if len(st) != 81:
			raise InputStrError('Wrong input string length. %s [%d]'%(st, len(st)))

		forbid_symbols = set(st)-_OPTIONS-{' '}
		if forbid_symbols:
			raise InputStrError('Wrong input string. There are forbidden symbols: %s'%''.join(forbid_symbols))

		return st

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-q', '--quiet', action='store_true', help='Doesn\'t print the progress of solving')
	parser.add_argument('-d', '--debug_mode', action='store_true', help='Read puzzle from file')
	parser.add_argument('--hypot', action='store_true', help='Use hypothetical way')
	args = parser.parse_args()

	solver = Solver(args)

	def get_puzzle_string():
		if args.debug_mode:
			with open('sudoku_puzzles.txt') as puzzle_file:
				for l in puzzle_file:
					l = l.strip('\n\r')
					if l and l[0] != '#':
						if l[0] == 'Z':
							break
						yield l
		else:
			while 1:
				yield raw_input('Input puzzle (see Solver.expand_input for the format details):\n')

	for p in get_puzzle_string():
		try:
			solution = solver.solve(p)
		except InputStrError as e:
			solver.emphasis_print('Error of the input: %s'%e.message, True)
