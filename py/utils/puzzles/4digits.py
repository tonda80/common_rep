# coding=utf8

# [26.08.2016 22:50:38] Алексей Князев: 4diget. Там за восемь ходов надо угадать четырехзначное число на основе комментариев о твоих предположениях. О числе известно только то, что оно состоит из четырех разрядов и то, что все цифры числа разные. По каждому из твоих предположений дается комментарий в виде <a-num>A<b-num>B, где a-num - количество цифр, которые стоят на правильных позициях в числе, а b-num - количество цифр, которые есть в конечном числе, но стоят на неправильных позициях. Например, комментарий
# 0A0B означает, что ты не угадал ни одной цифры, а комментарий 0A4B означает, что ты угадал все цифры, но все они стоят на неправильных позициях. Комментарий 4A0B означает, что ты угадал задуманное число.
# [26.08.2016 22:52:51] Алексей Князев: Я начал играть и заигрался. Сможешь ли ты составить алгоритм кратчайшего поиска ответа и написать программу с конрпрограммой, одна из которых играла бы за сервер, а другая за игрока. И главный вопрос - какое минимальное количество ходов должно гарантировать поиск правильного решения?

import random
import traceback

#helper
def random_pop(lst):
	r = random.choice(lst)
	lst.remove(r)
	return r

class Presenter:
	#def __init__(self):

	def reinit(self):
		digits = map(str, range(10))
		self.question = ''
		for i in xrange(4):
			self.question += random_pop(digits)
		self.attempt_cnt = 0

	def validate(self, var):
		if len(var) != 4:
			return
		if not var.isdigit():
			return
		for i in xrange(3):
			if var.count(var[i]) != 1:
				return
		return var

	def check(self, var):
		self.attempt_cnt += 1

		a = b = 0
		for i in xrange(4):
			if var[i] == self.question[i]:
				a += 1
			elif var[i] in self.question:
				b += 1

		return (a, b)

	def play(self):
		self.reinit()

		while 1:
			var = raw_input('Your guess? ')
			if 'q' == var:
				break
			elif 'g' == var:
				print 'You couldn\'t..', self.question
				break

			var = self.validate(var)
			if not var:
				print 'Wrong attempt! It must be a number of 4 different digit! Try again!'
				continue
			res = self.check(var)
			if res == (4, 0):
				print 'Bingo! You have won by %d attempts!'%self.attempt_cnt
				break
			print '%dA%dB'%res

class Player:
	def __init__(self, presenter):
		self.presenter = presenter

	def reinit(self):
		self.test_digits = map(str, xrange(10))
		self.history = dict(((d, set()) for d in self.test_digits))
		self.test_positions = range(4)

		self.absent_digits = set()
		self.b_digits = set()
		self.a_digits = set()

		self.a = self.old_a = self.b = self.old_b = 0

		while 1:
			self.test_number = [random_pop(self.test_digits) for i in xrange(4)]
			self.check()	# init check
			if self.a + self.b == 0:
				for d in self.test_number:
					self.absent_found(d)
			elif self.a == 4:
				for i, d in enumerate(self.test_number):
					self.a_found(i, d)
				break
			else:
				break

	def debug_print(self, where):
		print where
		print 'test_number', ''.join(self.test_number)
		print 'a=%d, b=%d, old_a=%d, old_b=%d'%(self.a, self.b, self.old_a, self.old_b)
		print 'a_digits %s, b_digits %s, absent_digits %s'%(self.a_digits, self.b_digits, self.absent_digits)
		print 'test_digits, test_positions, self.history', self.test_digits, self.test_positions, self.history
		print 'presenter.question', self.presenter.question
		print

	def check(self):
		check_str = ''.join(self.test_number)
		if not self.presenter.validate(check_str):
			self.algo_error('wrong test value')
		self.old_a, self.old_b = self.a, self.b
		self.a, self.b = self.presenter.check(check_str)
		self.debug_print('test_by after check')

	def found_digits_quantity(self):
		if len(self.a_digits) + len(self.b_digits) + len(self.absent_digits) > 10:
			self.algo_error('wrong digit definitions')
		return len(self.a_digits) + len(self.b_digits)

	def add_history(self, pos, value):
		self.history[value].add(pos)

	def a_found(self, pos, value):
		try:
			self.test_positions.remove(pos)
		except ValueError:
			if self.a != 4:	# final case
				raise
		self.a_digits.add(value)
		self.b_digits.discard(value)

	def b_found(self, pos, value):
		if value in self.a_digits:
			return
		self.b_digits.add(value)
		self.add_history(pos, value)

	def absent_found(self, value):
		self.absent_digits.add(value)

	def algo_error(self, what):
		err = 'player algo error: ' + what
		self.debug_print('algo_error %s'%err)
		raise RuntimeError(err)

	def select_test_position(self, test_digit, bPos = False):
		for i, d in enumerate(self.test_number):
			if d in self.absent_digits:
				return i

		positions = list(set(self.test_positions) - self.history[test_digit])
		while positions:
			pos = random_pop(positions)
			if (bPos and test_digit != self.test_number[pos]) or self.test_number[pos] not in self.b_digits:
				return pos

	def __replace(self, pos, test):
		initial = self.test_number[pos]
		self.test_number[pos] = test
		self.add_history(pos, test)
		return initial

	def __restore(self, type, pos, value):
		self.__dict__[type] += 1
		self.test_number[pos] = value

	def test_by(self, test):
		testIsB = test in self.b_digits or test in self.a_digits

		print '\n__debug test_by', test, testIsB

		if testIsB and test in self.test_number:
			return True

		pos = self.select_test_position(test)
		if pos is None:
			if testIsB:
				self.algo_error('test_by: no test position for B digit %s'%test)	# raise
			self.absent_found(test)
			return False

		initial = self.__replace(pos, test)
		self.check()

		success = False

		if self.a == self.old_a + 1:
			self.a_found(pos, test)
			success = True
			if self.b == self.old_b + 1:
				self.algo_error('test_by case: a+1, b+1')
			elif self.b == self.old_b:
				self.absent_found(initial)
			elif self.b == self.old_b - 1:
				self.b_found(pos, initial)
				self.test_by(initial)	# returns b
			else:
				self.algo_error('test_by case: a+1, else')

		elif self.a == self.old_a:
			if self.b == self.old_b + 1:
				self.b_found(pos, test)
				success = True
				self.absent_found(initial)
			# --------------------------------------------
			elif self.b == self.old_b:
				if testIsB:
					success = True
				elif initial in self.absent_digits:
					self.absent_found(test)
				elif self.absent_digits:
					success = self.test_by_absent(pos)
				else:
					self.test_number[pos] = initial
					success = self.test_by(test)

				if success:
					self.b_found(pos, initial)
					self.test_by(initial)	# returns b
				else:
					self.absent_found(initial)

			# --------------------------------------------
			elif self.b == self.old_b - 1:
				self.absent_found(test)
				self.b_found(pos, initial)
				self.__restore('b', pos, initial)
			else:
				self.algo_error('test_by case: a, else')
		if self.a == self.old_a - 1:
			self.a_found(pos, initial)
			self.__restore('a', pos, initial)
			if self.b == self.old_b + 1:
				self.b_found(pos, test)
				success = True
				self.b -= 1	# initial was restored
				self.test_by(test)	# returns b
			elif self.b == self.old_b:
				self.absent_found(test)
			elif self.b == self.old_b - 1:
				self.algo_error('test_by case: a-1, b-1')
			else:
				self.algo_error('test_by case: a-1, else')

		self.debug_print('test_by end')
		return success


	def test_by_absent(self, pos):
		print '\n__debug test_by_absent', pos

		test = self.__replace(pos, next(iter(self.absent_digits)))
		self.check()

		if self.a != self.old_a:
			self.algo_error('test_by_absent case: a != a_old')	# raise

		if self.b == self.old_b - 1:
			self.b_found(pos, test)
			self.__restore('b', pos, test)
			return True
		elif self.b == self.old_b:
			self.absent_found(test)
			return False

		self.algo_error('test_by_absent case: else')

	def final_test_by_absent(self, pos):
		print '\n__debug final_test_by_absent', pos

		test = self.__replace(pos, next(iter(self.absent_digits)))
		self.check()

		if self.a == self.old_a - 1:
			self.a_found(pos, test)
			self.__restore('a', pos, test)
			return True
		elif self.b == self.old_b - 1:
			self.b_found(pos, test)
			self.__restore('b', pos, test)
			return False

		self.algo_error('final_test_by_absent case: else')


	def final_test(self):
		print '\n__debug final_test'
		if self.found_digits_quantity() != 4 or len(self.absent_digits) == 0 or len(self.test_positions) != len(self.b_digits):
			self.algo_error('final_test use')

		test1 = next(iter(self.b_digits))
		pos1 = self.select_test_position(test1, True)
		pos2, test2 = self.__final_swap(pos1, test1)
		self.check()

		if self.a == self.old_a + 2 and self.b == self.old_b - 2:
			self.a_found(pos1, test1)
			self.a_found(pos2, test2)
		elif self.a == self.old_a + 1 and self.b == self.old_b - 1:
			if not self.final_test_by_absent(pos2):
				self.a_found(pos1, test1)
		elif self.a == self.old_a and self.b == self.old_b:
			pass
		else:
			self.algo_error('final_test else branch')


	def __final_swap(self, pos1, test1):
		pos2 = self.test_number.index(test1)
		test2 = self.test_number[pos1]

		self.test_number[pos1] = test1
		self.test_number[pos2] = test2

		self.add_history(pos1, test1)
		self.add_history(pos2, test2)

		return pos2, test2

	def play(self):
		self.presenter.reinit()
		self.reinit()

		while self.found_digits_quantity() < 4 and self.a + self.b != 4:		# and self.test_digits
			test_digit = random_pop(self.test_digits)
			self.test_by(test_digit)

		if not self.absent_digits:
			self.absent_digits.update(set(map(str, xrange(10))).difference(set(self.test_number)))

		if self.found_digits_quantity() < 4:
			for p in self.test_positions[:]:
				if p not in self.history[self.test_number[p]]:
					self.final_test_by_absent(p)

		while self.a != 4:
			self.final_test()

		answer = ''.join(self.test_number)
		if answer != self.presenter.question:
			self.algo_error('Wrong answer')

		return answer


presenter = Presenter()
#presenter.play(); exit(0)

class Stat:
	def __init__(self, period = 1):
		self.average = self.n = self.max = 0.0
		self.min = float('inf')
	def add(self, value):
		self.n += 1
		self.max = max(self.max, value)
		self.min = min(self.min, value)
		self.average = self.average + (value - self.average)/self.n

player = Player(presenter)
stat = Stat()
while 1:
	player.play()
	stat.add(presenter.attempt_cnt)
	out = 'Guessed by %d attempts, max %d, min %d, average %.1f, games %d'%(presenter.attempt_cnt, stat.max, stat.min, stat.average, stat.n)
	print '%s\n%s\n'%(out, '-'*len(out))
