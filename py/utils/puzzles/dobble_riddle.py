# coding=utf8

# пытаюсь понять суть генерации доббль карточек
# то есть нужно сгенерировать набор карточек сочетаний картинок в котором
# каждая карточка отличается от любой другой ровно одной картинкой
# надеюсь что таки хватит мозгом вывести формулу


import argparse
import itertools


class AppError(RuntimeError):
	pass

class App:
	def __init__(self, args=None):
		self.args = self.parse_args(args)

		self.items = range(int(self.args.n))
		self.k = self.args.k

	def parse_args(self, args):
		parser = argparse.ArgumentParser()
		parser.add_argument('--naive', action='store_true', help=u'Перебираем в лоб все варианты')
		parser.add_argument('--n', help=u'Общее число элементов')
		parser.add_argument('--k', type=int, required=True, help=u'Число элементов в сочетании')
		return parser.parse_args(args)

	def main(self):
		if self.args.naive:
			#print 'naive_way',
			res = self.naive_way()
		else:
			print 'nothing to do'
			return
		print 'results:'
		for r in res:
			print sorted(r)

	def naive_way(self):
		res = [set(self.items[:self.k])]
		for s in itertools.combinations(self.items, self.k):
			ss = set(s)
			for r in res:
				if len(r & ss) != 1:
					#if ss == {2,4,8,10}:print '__deb %s exc by %s'%(ss, r)
					break
			else:
				res.append(ss)
				print '_deb', sorted(s)
		return res


if __name__ == '__main__':
	App().main()
