# coding=utf8

import sys
import zipfile
import itertools

# по быстрому решаем задачу от Миши, выводим слова с заданным количеством слогов и букв

# http://speakrus.ru/dict/index.htm http://blog.harrix.org/article/3334
def get_vocabulary():
	vocabulary = {}
	zpf = zipfile.ZipFile('../../../extra/zdf-win.txt.zip')
	for s in itertools.chain(zpf.open(r'zdf-win.txt'), open('../../../extra/zdf-win.extra.txt')):	#word_rus.txt
		#print s
		w = s.strip().decode('utf8')
		vocabulary.setdefault(len(w), set()).add(w)
	return vocabulary

def test_volume(vocabulary):
	words = letters = test = 0
	for i,v in vocabulary.iteritems():
		l = len(v)
		words += l
		letters += i*l

		z = i - 3 + 1	# quantity of k-letters combinations
		if z > 0:
			test += z*l

		print i, l

	print words, letters, test


vocabulary = get_vocabulary()
#test_volume(vocabulary)
#sys.exit()

vowels = u'уеыаоэяиюё'
def get_syllable_quantity(word):
	q = 0
	for c in word:
		if c in vowels:
			q += 1
	return q


while 1:
	q_letters = int(raw_input(u'Количество букв: '.encode(sys.stdin.encoding)))
	q_syllable  = int(raw_input(u'Количество слогов: '.encode(sys.stdin.encoding)))

	word_result = []
	for word in vocabulary[q_letters]:
		if get_syllable_quantity(word) == q_syllable:
			word_result.append(word)
	word_result.sort()
	for word in word_result:
		print word
