# coding=utf8

# http://www.pythonchallenge.com
# http://holger.thoelking.name/python-challenge/all - solutions

import sys
import collections
import urllib2
import string
import re
import pickle
import zipfile
#from PIL import Image

import lsh

def get_content(url):
	return urllib2.urlopen(url).read()

def get_substring(src, sub_beg, sub_end, raise_if_empty = True):
	ret = src[src.find(sub_beg):src.rfind(sub_end) + len(sub_end)]
	if raise_if_empty and not ret:
		raise RuntimeError('No such substring')
	return ret

class UrlFile:
	def __get_name(self):
		name = '__temp_file'
		while lsh.test(name):
			name += '_'
		return name

	def __init__(self, opener, url, temp_file_name = None):		# if temp_file_name is None - try not download and not remove
		if temp_file_name:
			self.const_file = True
			self.temp_file_name = temp_file_name
		else:
			self.const_file = False
			self.temp_file_name = self.__get_name()

		self.opened_file = None
		try:
			if not lsh.test(self.temp_file_name):
				open(self.temp_file_name, 'wb').write(get_content(url))
			if opener:
				self.opened_file = opener(self.temp_file_name)
		except:
			self.__exit__()

	def __enter__(self):
		return self.opened_file

	def __exit__(self, *a):
		if self.opened_file:
			self.opened_file.close()
		if not self.const_file:
			lsh.rm(self.temp_file_name, '-f')


class Challenges:
	def challenge0(self):		# http://www.pythonchallenge.com/pc/def/0.html
		print 2**38

	def challenge1(self):
		content = get_content('http://www.pythonchallenge.com/pc/def/map.html')
		data = get_substring(content, 'g fmnc', 'rfc spj.')

		def algo(st):
			res = ''
			for c in st:
				if 'a' <= c <= 'z':
					res += chr((ord(c) - ord('a') + 2)%26 + ord('a'))
				else:
					res += c
			return res

		print algo(data)
		print algo('map')	# from url

	def challenge2(self):
		content = get_content('http://www.pythonchallenge.com/pc/def/ocr.html')

		# мудрый я из 24 года забраковал это решение из 16 наверное года)
		#data = get_substring(content, '<!--\x0a%%$@_', '$^$}*\x0a-->')
		# print filter(lambda c: c in string.ascii_letters, data)

		mark = 'find rare characters in the mess below'
		data = content[content.find(mark) + len(mark):]
		data = get_substring(data, '<!--', '-->')[4:-3]
		sym_freq = collections.OrderedDict()
		for c in data:
			sym_freq[c] = sym_freq.setdefault(c, 0) + 1
		print ''.join(k for k,v in sym_freq.iteritems() if v < 3)

	def challenge3(self):
		content = get_content('http://www.pythonchallenge.com/pc/def/equality.html')
		data = get_substring(content, '<!--\x0akAewt', 'piqBd\x0a-->')

		print ''.join(e[4] for e in re.findall(r'[^A-Z][A-Z]{3}[a-z]{1}[A-Z]{3}[^A-Z]', data))
		# from http://holger.thoelking.name/python-challenge/all
		# "".join(re.findall("[^A-Z]+[A-Z]{3}([a-z])[A-Z]{3}[^A-Z]+", data))

	def challenge4(self):	# http://www.pythonchallenge.com/pc/def/linkedlist.html
		templ = re.compile(r'and the next nothing is (\d+)')
		nothing = '75635'	# '13115'	# first '12345'
		keep_going = 1
		while keep_going:
			content = get_content('http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=%s'%nothing)[:]
			mo = templ.search(content)

			if mo:
				nothing = mo.group(1)
			elif content.find('Divide by two and keep going.') != -1:
				nothing = str(int(nothing)/2)
			else:
				keep_going = False
			print '[%s] [%s]'%(content, nothing)

	def challenge5(self):	# http://www.pythonchallenge.com/pc/def/peak.html
		content = get_content('http://www.pythonchallenge.com/pc/def/banner.p')
		data = pickle.loads(content)

		for l in data:
			st = ''
			for s, q in l:
				st += s*q
			print st

	def challenge6(self):	# http://www.pythonchallenge.com/pc/def/channel.html
		ans_lst = []

		with UrlFile(zipfile.ZipFile, 'http://www.pythonchallenge.com/pc/def/channel.zip') as zfile:
			templ = re.compile(r'Next nothing is (\d+)')
			fname = '90052.txt'
			while 	1:
				ans_lst.append(zfile.getinfo(fname).comment)

				cont = zfile.open(fname).read()
				mo = templ.search(cont)
				if mo is None:
					print fname, cont
					break
				else:
					fname = mo.group(1) + '.txt'

		print ''.join(ans_lst)

	def challenge7(self):	# http://www.pythonchallenge.com/pc/def/oxygen.html
		with UrlFile(Image.open, 'http://www.pythonchallenge.com/pc/def/oxygen.png') as pfile:		#, debug = 1
			msg = ''.join(re.findall(r'(.)\1{9,16}', filter(lambda c: c in string.printable, pfile.tostring())))
			print msg[:87]
			str_codes = re.search(r'\[(.*?)\]', msg).group(1)
			li_codes = map(int, str_codes.split(', '))
			print ''.join(map(chr, li_codes))
		# there is better solution into holger.thoelking.name


# --------------------------------------------

pref = 'challenge'
if len(sys.argv) != 2:
	print 'Give me know number of challenge [0 - %s]'%max(filter(lambda s: s.startswith(pref), Challenges.__dict__.keys()))[len(pref):]
else:
	Challenges.__dict__[pref + sys.argv[1]](Challenges())
