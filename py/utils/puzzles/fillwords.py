# coding=utf8

import sys
import math
import argparse

# http://speakrus.ru/dict/index.htm http://blog.harrix.org/article/3334
def get_vocabulary():
	global args
	vocabulary = {}
	for s in open(args.dict):
		#print s
		yield s.strip().decode('utf8')

def deb_print(*output):
	global args
	if args.d:
		print '__debug',
		for o in output:
			print o,	#.encode(sys.stdout.encoding)
		print

def _raw_input(unicode_inv=''):
	return raw_input(unicode_inv.encode(sys.stdout.encoding)).decode(sys.stdin.encoding).lower()

class DistributedVocabulary:
	class Node:
		nodeCounter = 0
		def __init__(self, letter):
			self.letter = letter
			self.nexts = {}
			self.wordEnd = False
			self.__class__.nodeCounter += 1

	class Seeker:
		def __init__(self, vocabulary):
			self.currNode = vocabulary.root
			self.history = []

		# returns: None - no such word, False - not the end of word, True - the end of word
		def check_next(self, letter):
			deb_print(u'Seeker.check_next, current ' + self.currNode.letter + u', checked ' + letter)

			if letter in self.currNode.nexts:
				self.history.append(self.currNode)
				self.currNode = self.currNode.nexts[letter]
				return self.currNode.wordEnd

		def back(self):
			self.currNode = self.history.pop()
			deb_print(u'Seeker.check_next, current ' + self.currNode.letter)

	def __init__(self):
		self.root = DistributedVocabulary.Node('')

		for word in get_vocabulary():
			link = self.root
			for l in word:
				if l not in link.nexts:
					node = DistributedVocabulary.Node(l)
					link.nexts[l] = node
				else:
					node = link.nexts[l]
				link = node
			node.wordEnd = True


class Letter:
	def __init__(self, owner, p ):
		assert owner.states[p] == 0, 'Implementation error, capture of a busy letter'
		owner.states[p] = 1

		self._position = p
		self._symbol = owner.raw_string[p]
		self.owner = owner

		deb_print('created letter', self)

	def position(self):
		assert self._position >=0, 'Algo error, access to a released letter'
		return self._position

	def symbol(self):
		assert self._symbol, 'Algo error, access to an released letter'
		return self._symbol

	def release(self):
		deb_print('released letter', self)

		self.owner.states[self._position] = 0
		self._position = -1
		self._symbol = ''

	def __repr__(self):
		r,c = divmod(self._position, self.owner.n)
		return '%s(%dx%d)'%(self._symbol.upper().encode(sys.stdout.encoding), r+1, c+1)
	def __str__(self): return self.__repr__()

class LetterTable:
	def __init__(self, st):
		self.len = len(st)
		self.n = int(math.sqrt(self.len))
		assert self.n**2 == self.len, 'Wrong len of init string'
		self.raw_string = st
		self.states = [0 for i in xrange(self.len)]	# 0 free, 1 busy

	def isGuessed(self):
		return 0 not in self.states

	def print_(self, force_out=False):
		global args
		if args.d or force_out:
			i = 0
			print
			for i in xrange(self.len):
				print '%s(%d)'%(self.raw_string[i], self.states[i]),
				if i % self.n == self.n-1:
					print
				i += 1
			print

	def get_neighbours(self, i):
		assert 0 <= i < self.len, 'Wrong a letter position'
		r, c = divmod(i, self.n)
		for nr, nc in ((r-1,c), (r,c-1), (r,c+1), (r+1,c)):
			l = self.__get_letter_by_rc(nr, nc)
			if l:
				yield l

	def __get_letter_by_rc(self, r, c):
		if r < 0 or r >= self.n or c < 0 or c >= self.n:
			return
		p = r*self.n + c
		return self.__get_letter(p)

	def __get_letter(self, p):
		if self.states[p] == 0:
			return Letter(self, p)

	def find_word_from_start(self, init_pos, min_length, checker, verifier):
		init_letter = self.__get_letter(init_pos)
		if init_letter is None:
			return
		if checker.check_next(init_letter.symbol()) is not None:
			word = [init_letter]
			for w in self.__find_word_from_start(word, checker):
				if len(w) >= min_length:
					if verifier(word):
						return

		init_letter.release()

	def __find_word_from_start(self, word, checker):
		for n in self.get_neighbours(word[-1].position()):
			check_result = checker.check_next(n.symbol())
			if check_result is not None:
				word.append(n)
				if check_result:
					yield word
				for w in self.__find_word_from_start(word, checker):
					yield w
				word.pop()
				checker.back()
			n.release()

	# get_all_pieces (and naturally __get_piece) is unnecessary
	# but a good example how to work with yield so let it be here
	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	def get_all_pieces(self, init_pos, length):
		init_letter = self.__get_letter(init_pos)
		for p in self.__get_piece([init_letter], length):
			yield p
		init_letter.release()

	def __get_piece(self, piece, length):
		assert len(piece) > 0, 'Wrong piece'
		assert length > 1, 'Wrong length, should be greater than 1'

		deb_print('__get_piece', piece)
		self.print_()	#debug

		for n in self.get_neighbours(piece[-1].position()):
			piece.append(n)
			if len(piece) >= length:
				yield piece
			else:
				for p in self.__get_piece(piece, length):
					yield p
			piece.pop().release()
	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def get_args():
	parser = argparse.ArgumentParser()
	parser.add_argument('--dict', help='Path to the dictionnary file', default=r'D:\temp\zdf-win.txt')
	parser.add_argument('-d', action='store_true', help='Enable debug output')

	return parser.parse_args()

class App:
	def __init__(self, min_word_length = 4):
		self.min_word_length = min_word_length
		self.distVocabulary = DistributedVocabulary()
		print 'Vocabulary is loaded (%d)'%DistributedVocabulary.Node.nodeCounter
		try:
			self.run()
		except KeyboardInterrupt:
			print 'Exiting..'

	def run(self):
		while 1:
			self.restart()
			if not self.get_boolean_answer(u'Играем дальше?'):
				break

	def restart(self):
		if 1:
			letter_table = LetterTable(self.get_table_string())
		else:
			letter_table = LetterTable(u'вагешткпо')

		self.absent_words = []
		self.letter_table = letter_table

		min_word_length = (self.min_word_length+3, self.min_word_length)

		for mwl in min_word_length:
			for i in xrange(letter_table.len):
				letter_table.print_()
				if letter_table.isGuessed():
					break

				checker = DistributedVocabulary.Seeker(self.distVocabulary)
				letter_table.find_word_from_start(i, mwl, checker, self.verify)

		if letter_table.isGuessed():
			print u'Угадали все слова!\n'
		else:
			print u'Увы, но остались неразгаданные слова.'
			letter_table.print_(1)


	def verify(self, word):
		word_symbols_only = u''.join(l.symbol() for l in word)
		if word_symbols_only in self.absent_words:
			return False
		print u'Найденное слово :', word_symbols_only, ' | ', word
		answer = self.get_string_answer(u'Подходит?', (u'', u'д', u'н', u'е'), u'д или пустая строка - Да; н - Нет; е - Ещё, т.е. слово есть, но расположено по другому')

		if answer == u'н':
			self.absent_words.append(word_symbols_only)
		return answer == u'' or answer == u'д'

	def get_boolean_answer(self, question):
		while 1:
			answer = _raw_input(question + u' (Д\н): ')
			if answer == '' or answer == u'д' or answer == u'н':
				return answer != u'н'
			print u'Введи д или н'

	def get_string_answer(self, question, options, help):
		options_s = u'\\'.join(options)
		while 1:
			answer = _raw_input(question + u' (%s): '%options_s)
			if answer in options:
				return answer
			print u'Введи %s. %s'%(options_s, help)

	def get_table_string(self):
		while 1:
			print u'Введи таблицу с буквами.',
			if self.get_boolean_answer(u'Построчно?'):
				print u'Введи таблицу построчно (пустая строка, чтобы начать заново)'
				parts = [_raw_input()]
				n = len(parts[0])
				cnt_row = 1
				while cnt_row < n:
					p = _raw_input()
					l = len(p)
					if l == n:
						parts.append(p)
						cnt_row += 1
					elif l == 0:
						print u'Начинаем заново'
						break
					else:
						print u'Неправильная длина. Повтори ввод последней строки'
				else:
					return u''.join(parts)
			else:
				print u'Введи строку с таблицей'
				table = _raw_input()
				l = len(table); n = int(math.sqrt(l))
				if n**2 == l:
					return table
				print u'Неправильная длина таблицы'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def letter_table_debug():
	while 1:
		cmd = raw_input()
		if cmd == 'q':
			sys.exit(0)
		elif cmd == 'p':
			letter_table.print_(True)
		else:
			try:
				length, init_pos = map(int, cmd.split())
			except ValueError:
				print 'Unknown command'
				continue
			cnt = 0
			for p in letter_table.get_all_pieces(init_pos, length):
				print '\n-->', p, '\n'
				cnt += 1
			print 'Total', cnt

def distributed_vocabulary_debug():
	distVocabulary = DistributedVocabulary()
	print 'ready'
	while 1:
		seeker = DistributedVocabulary.Seeker(distVocabulary)
		while 1:
			cmd = _raw_input()
			if cmd == u'_й':#q uit
				sys.exit(0)
			elif cmd == u'_к':#r estart
				break
			elif cmd == u'_и':#b ack
				seeker.back()
			elif len(cmd)==1:
				ret = seeker.check_next(cmd)
				if ret is None:
					print 'no word'
				else:
					print 'ok', ret

			else:
				print 'unknown command'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if __name__ == '__main__':
	args = get_args()

	App(4)

