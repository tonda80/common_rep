# coding=utf8

import time
import copy
import traceback

class Card:
	def __init__(self, descr):
		try:
			self.top, self.left, self.right, self.bottom = tuple(map(int, descr))
		except:
			raise RuntimeError('Wrong CardSet init: '+descr)
		self._layouts = set()	# layouts seqs which contains this card

	def holded_by(self, layout):
		self._layouts.add(layout.seq)

	def released_by(self, layout):
		self._layouts.remove(layout.seq)

	def check(self, layout, neighbors):
		topN, leftN, rightN, bottomN = neighbors
		return (
		  (layout.seq not in self._layouts) and
		  (topN is None or topN.bottom == self.top) and
		  (leftN is None or leftN.right == self.left) and
		  (rightN is None or rightN.left == self.right) and
		  (bottomN is None or bottomN.top == self.bottom)
		  )

	def __repr__(self):
		return self.__str__()

	def __str__(self):
		return '%d%d%d%d'%(self.top, self.left, self.right, self.bottom)

	def pprint(self):
		print self, '  --> ',  self._layouts

class CardSet:
	def __init__(self, card_descr):
		descr_type = 0
		if ' ' not in card_descr and descr_type == 0:
			descr_type = 1

		def _err_exit(what):
			raise RuntimeError('Wrong CardSet init: %s (%d). %s'%(card_descr, descr_type, what))

		if descr_type == 0:		# card by card (top, left, right, bottom) -> 'tlrb tlrb tlrb ...'
			cd_list = card_descr.split()

		elif descr_type == 1:	# solid stream, the game screen is read like a text
			slen = len(card_descr)
			l, _ = divmod(slen, 4)
			if _ != 0:
				_err_exit(1)
			s = int(l**0.5)
			if s**2 != l:
				_err_exit(2)

			cd_l_list = [[None for j in xrange(4)] for i in xrange(l)]
			for i in xrange(slen):
				# gr - номер группы карт по 4, sh - сдвиг буквы в группе, n - пор. номер карты в строимом списке, sd - сторона
				gr, sh = divmod(i, 4*s)
				if sh < s:
					sh2 = sh
					sd = 0	# top
				elif sh < 3*s:
					sh2, x = divmod(sh - s, 2)
					sd = 1 + x	# left or right
				else:
					sh2 = sh - 3*s
					sd = 3	# bottom
				n = gr*s + sh2

				cd_l_list[n][sd] = card_descr[i]

			cd_list = map(lambda l: ''.join(l), cd_l_list)

		else:
			_err_exit(3)

		l = len(cd_list)
		s = l**0.5
		if s**2 != l or l==0:
			_err_exit(4)

		self.cards = map(Card, cd_list)
		self.field_size = int(s)

	def get_cards(self, layout, neighbors):
		return filter(lambda card: card.check(layout, neighbors), self.cards)

	def pprint(self):
		print 'CardSet'
		for card in self.cards:
			card.pprint()


class Layout:
	@classmethod
	def setup(cls, size):
		cls.Size = size
		cls.Seq = 0
		cls.AllLayouts = []

	def __init__(self, card):
		self.seq = Layout.Seq
		Layout.Seq += 1

		self.filled = False

		if card:	# card is None for creation in copy
			self.cards = [None for i in xrange(Layout.Size**2)]
			self.add(card, 0, 0)

	def get_card(self, row, col):
		if row >= 0 and col >= 0 and row < Layout.Size and col < Layout.Size:
			return self.cards[Layout.Size*row + col]

	def add(self, card, row, col):
		ind = Layout.Size*row + col
		if self.cards[ind] is not None:
			print 'Algo error'
			self.pprint()
			print 'Trying add %s into (%d, %d)'%(card, row, col)
			raise RuntimeError
		self.cards[ind] = card
		self.last_pos = (row, col)
		card.holded_by(self)

	def copy(self):
		new_layout = Layout(None)
		new_layout.cards = copy.copy(self.cards)	# surface copying
		new_layout.last_pos = self.last_pos
		for card in filter(None, self.cards):
			card.holded_by(new_layout)
		return new_layout

	def add_oneself(self):
		self.AllLayouts.append(self)

	def remove_oneself(self):
		# unexpectedly it slows down process
		#for card in filter(None, self.cards):
		#	card.released_by(self)

		self.AllLayouts.remove(self)

	def pprint(self):
		print 'Layout #%d'%self.seq
		cnt = 0
		for card in self.cards:
			if card is None:
				print ' -- ',
			else:
				print card,
			cnt += 1
			if cnt % Layout.Size == 0:
				print

class Solver:
	def __init__(self, strategy):
		self.strategy = strategy

	def solve(self, card_descr):
		card_set = CardSet(card_descr)
		Layout.setup(card_set.field_size)
		layouts = Layout.AllLayouts = map(Layout, card_set.cards)

		while 1:
			solved = True
			for layout in layouts[:]:
				solved &= layout.filled		# all layouts must be filled

				if not layout.filled:
					self.strategy.fill(layout, card_set)	# here tries to multiply/reduce layouts

			if solved:
				return layouts
			if not layouts:
				return # no solution

class Strategy:
	def fill(self, layout, card_set):
		raise NotImplementedError

class StrategyOnebyOne(Strategy):
	def fill(self, layout, card_set):
		r, c = layout.last_pos
		mxm = layout.Size - 1

		c += 1
		if c > mxm:
			c = 0
			r += 1

		top = layout.get_card(r-1, c)
		left = layout.get_card(r, c-1)

		for card in card_set.get_cards(layout, (top, left, None, None)):
			new_layout = layout.copy()
			new_layout.add(card, r, c)
			new_layout.add_oneself()
			if r == c == mxm:
				new_layout.filled = True

		layout.remove_oneself()


if __name__ == '__main__':
	solver = Solver(StrategyOnebyOne())

	while 1:
		# oneshot test run
		tp = None
		#tp = '7310 9697 7965 5104'
		#tp = '0407743039221240'
		#tp = '3958 8563 4458 9750 7586 7443 6338 9792 5891 4399 4874 8170 3717 6775 9972 8445 2947 0075 3449 8249 0801 4124 7186 2336 1732'
		#tp = '0881 7040 5465 5476 8237 2478 2699 5312 5342 7182 9545 9149 2458 9706 1717 8088 1790 6411 6127 7006 3957 4808 2174 6260 7718'
		#tp = '8070 7798 1791 0097 4910 5538 0140 0994 3418 4224 6367 8221 3590 7243 9934 1647 3172 0384 4811 4499 7656 9740 4979 6401 6787 1976 2803 1046 2331 2404 1390 2152 0315 7973 0141 9116'
		#tp = '928491300533575432728277754045325951220828312344087827838568412196018982882828255612640343918505218315403735357660148577875752922473170123880487'

		start_time = time.time()
		try:
			if tp:
				puzzle = tp
			else:
				puzzle = raw_input('Input cardset (set of top_left_right_bottom numbers):\n')
			solutions = solver.solve(puzzle)
		except KeyboardInterrupt:
			print 'Bye..'
			exit(0)
		except Exception as e:
			print 'Something is wrong!!!!!!!!!!!!!!!!!'
			traceback.print_exc()
			if tp:
				exit(1)
			continue

		print 'It took %f sec.'%(time.time()-start_time)
		if not solutions:
			print 'No solutions :('
		else:
			print 'Solutions are:'
			for s in solutions:
				s.pprint()
				print
		if tp:
			exit(0)
