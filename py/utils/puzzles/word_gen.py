# coding=utf8

import sys
import zipfile

# http://speakrus.ru/dict/index.htm http://blog.harrix.org/article/3334
def get_vocabulary():
	vocabulary = {}
	zpf = zipfile.ZipFile('../../../extra/zdf-win.txt.zip')
	for s in zpf.open(r'zdf-win.txt'):	#word_rus.txt
		#print s
		w = s.strip().decode('utf8')
		vocabulary.setdefault(len(w), set()).add(w)
	return vocabulary

def test_volume(vocabulary):
	words = letters = test = 0
	for i,v in vocabulary.iteritems():
		l = len(v)
		words += l
		letters += i*l

		z = i - 3 + 1	# quantity of k-letters combinations
		if z > 0:
			test += z*l

		print i, l

	print words, letters, test


vocabulary = get_vocabulary()
#test_volume(vocabulary)
#sys.exit()

while 1:
	symbols = raw_input(u'Буквы: '.encode(sys.stdout.encoding)).decode(sys.stdin.encoding).lower()
	word_len = int(raw_input(u'Длина слова: '.encode(sys.stdin.encoding)))

	for word in vocabulary[word_len]:
		for s in set(word):
			if word.count(s) > symbols.count(s):
				break
		else:
			print word
