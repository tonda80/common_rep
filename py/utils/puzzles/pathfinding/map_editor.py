import field

from uTkinter import *
import tkSimpleDialog
import tkMessageBox
import tkFileDialog

class MapEditor:
	def __init__(self, maps_dir):
		self.maps_dir = maps_dir
		self.file_name = None

		self.root = Tk()
		f1 = uFrame(self.root, gmArgs = {'expand':YES, 'fill':BOTH})
		self.field = field.Field(f1)
		f2 = uFrame(self.root, gmArgs = {'expand':NO, 'fill':X})
		uButton(f2, self.btn_new, 'New', gmArgs = {'side':'left'})
		uButton(f2, self.btn_load, 'Load', gmArgs = {'side':'left'})
		uButton(f2, self.btn_save, 'Save', gmArgs = {'side':'left'})
		uButton(f2, self.btn_saveas, 'Save As', gmArgs = {'side':'left'})

		self.field.reset(10, 10)
		self.last_cell = None
		self.field.add_mouse_handler('<B1-Motion>', self.mouse_btn1_motion)
		self.field.add_mouse_handler('<Button-1>', self.mouse_btn1_press)

		placeWindow(self.root, 40, 0, 1000, 950)
		self.root.mainloop()

	def mouse_btn1_motion(self, row, col):
		if self.last_cell == (row, col):
			return
		self.last_cell = (row, col)
		self.reverse_item(row, col)

	def mouse_btn1_press(self, row, col):
		self.last_cell = (row, col)
		self.reverse_item(row, col)

	def reverse_item(self, row, col):
		if self.field.get_item(row, col):
			self.field.delete_item(row, col)
		else:
			self.field.add_item(row, col, 'rectangle', 0.9, 'blue')

	def btn_new(self):
		self.file_name = None
		self.root.title('New file...')
		descr = tkSimpleDialog.askstring('New map', 'Enter a map descriptor(ROWxCOL, e.g. 10x20)', initialvalue='50x50')
		try:
			q_row, q_col = map(int, descr.split('x'))
		except:
			tkMessageBox.showerror('', 'Bad descriptor!')
			return
		self.field.reset(q_row, q_col)

	def btn_load(self):
		self.file_name = tkFileDialog.askopenfilename(initialdir=self.maps_dir)
		if self.file_name:
			self.root.title(self.file_name)
			self.load()

	def load(self):
		error_callback = lambda what: tkMessageBox.showerror('', what)
		add_item_args = {'X':('rectangle', 0.9, 'blue')}
		self.field.load_from_plain_file(self.file_name, add_item_args, error_callback)

	def btn_save(self):
		if not self.file_name:
			self.file_name = tkFileDialog.asksaveasfilename(initialdir=self.maps_dir, defaultextension='.txt')
		self.save()

	def btn_saveas(self):
		self.file_name = tkFileDialog.asksaveasfilename(initialdir=self.maps_dir, defaultextension='.txt')
		self.save()

	def save(self):
		if self.file_name:
			self.root.title(self.file_name)
			self.field.save_to_plain_file(self.file_name)

if __name__ == '__main__':
	MapEditor('maps')