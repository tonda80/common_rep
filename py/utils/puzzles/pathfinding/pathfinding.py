import field

import Tkinter
import tkMessageBox
import os
import itertools
import bisect
import threading
import Queue
import argparse
import time

# tags
OBSTACLE_TAG = 'X'
BOUNDARY_TAG = 'B'
SHIP_TAG = 'S'
DEMO_TAG = 'D'

BOUNDARY_VIEW = ('circle', 0.8, 'green')
OBSTACLE_VIEW = ('rectangle', 0.9, 'blue')
SHIP_VIEW = ('rectangle', 1, 'red')
OPEN_LIST_VIEW = ('rectangle', 0.5, 'yellow')
CLOSED_LIST_VIEW = ('rectangle', 0.5, 'gray')
ROUTE_VIEW = ('rectangle', 0.8, 'green')

class SeaIndexError(IndexError):
	pass

class SeaPoint:
	def __init__(self, row, col):
		self.row, self.col = (row, col)

	def points(self):
		return ((self.row, self.col),)

	def destroy(self):
		for r,c in self.points():
			g_sea.delete_item(r,c)

	def draw(self, view, tag):
		for r, c in self.points():
			g_sea.add_or_change_item(r, c, *view).set_tag(*tag)

class Ship(SeaPoint):
	def __init__(self, row, col, vertical = True):
		SeaPoint.__init__(self, row, col)
		self.vertical = vertical

	def points(self):
		if self.vertical:
			return ((self.row-1, self.col),(self.row, self.col),(self.row+1, self.col))
		return ((self.row, self.col-1),(self.row, self.col),(self.row, self.col+1))

	@classmethod
	def adjacent_positions(cls, row, col, vertical):
		class Neighbour:
			pass
		nbs = (Neighbour(), Neighbour(), Neighbour(), Neighbour()) # top, bottom, left, right

		nbs[0].position = (row-1, col)
		nbs[1].position = (row+1, col)
		nbs[2].position = (row, col-1)
		nbs[3].position = (row, col+1)
		if vertical:
			#turns
			nbs[0].turn = nbs[1].turn = False
			nbs[2].turn = nbs[3].turn = True
			#top, bottom
			nbs[0].cond = ((row-2, col), )
			nbs[1].cond = ((row+2, col), )
			nbs[0].cond_opt1 = nbs[0].cond_opt2 = nbs[1].cond_opt1 = nbs[1].cond_opt2 = ()
			#left, right
			nbs[2].cond = ((row, col-1), (row, col+1), (row, col-2))
			nbs[3].cond = ((row, col-1), (row, col+1), (row, col+2))
			nbs[2].cond_opt1 = nbs[3].cond_opt1 = ((row-1, col-1), (row+1, col+1))
			nbs[2].cond_opt2 = nbs[3].cond_opt2 = ((row-1, col+1), (row+1, col-1))
		else:
			#turns
			nbs[0].turn = nbs[1].turn = True
			nbs[2].turn = nbs[3].turn = False
			#top, bottom
			nbs[0].cond = ((row-1, col), (row+1, col), (row-2, col))
			nbs[1].cond = ((row-1, col), (row+1, col), (row+2, col))
			nbs[0].cond_opt1 = nbs[1].cond_opt1 = ((row-1, col-1), (row+1, col+1))
			nbs[0].cond_opt2 = nbs[1].cond_opt2 = ((row-1, col+1), (row+1, col-1))
			#left, right
			nbs[2].cond = ((row, col-2), )
			nbs[3].cond = ((row, col+2), )
			nbs[2].cond_opt1 = nbs[2].cond_opt2 = nbs[3].cond_opt1 = nbs[3].cond_opt2 = ()

		for neighbour in nbs:
			if (g_sea.check_free(neighbour.cond, (OBSTACLE_TAG,)) and (
			       g_sea.check_free(neighbour.cond_opt1, (OBSTACLE_TAG,)) or
				   g_sea.check_free(neighbour.cond_opt2, (OBSTACLE_TAG,))
				)):
				yield (neighbour.position, neighbour.turn)

class PathPoint(SeaPoint):
	def __init__(self, row, col, parent, turn):	# turn is vertical for the init point!!
		SeaPoint.__init__(self, row, col)
		self.parent = parent

		if parent:
			self.vertical = parent.vertical ^ turn
			self.g_cost = parent.g_cost + self.cost_from(turn)
		else:
			self.vertical = turn # special case
			self.g_cost = 0

		self.h_cost = g_sea.distance_to_finish(row, col)*8
		self.f_cost = self.g_cost + self.h_cost

	# it doesn't work all the same, there are unnecessary turns
	# remove?
	def cost_from(self, turn):
		if turn:
			return 15
		return 10

	def try_udpate_cost(self, prob_parent, turn):
		new_g_cost = prob_parent.g_cost + self.cost_from(turn)
		if self.g_cost > new_g_cost:
			debug_print('Cost will be changed:', self, self.parent)
			self.g_cost = new_g_cost
			self.f_cost = self.g_cost + self.h_cost
			self.parent = prob_parent
			debug_print('Cost changed:', self, self.parent)
			return True
		return False

	# cmp and eq use different attributes!!!
	#
	# for sort, bisect.insort
	def __cmp__(self, oth):
		return self.f_cost - oth.f_cost
	# for index, 'in'
	def __eq__(self, (row, col,vertical)):
		return self.row == row and self.col == col and self.vertical==vertical

	def __repr__(self):
		return '[%d %d] [%d=%d+%d] [%d]'%(self.row, self.col, self.f_cost, self.g_cost, self.h_cost, self.vertical)


class Sea(field.Field):
	def __init__(self, master, maps_dir):
		field.Field.__init__(self, master)

		self.calculating = False
		self.calculating_thread = threading.Thread(target=self.__calculate_path, args=())
		self.draw_queue = Queue.Queue()
		self.draw_event = threading.Event()
		self.debug_path_points = []

		self._vertical_ship = True	# ship place position
		def _invert_vertical_ship(_): self._vertical_ship ^= True

		self.add_key_handler('<space>', self.space_handler)
		self.add_mouse_handler('<Button-1>', self.left_btn_handler)
		self.add_mouse_handler('<Button-3>', self.right_btn_handler)
		self.add_key_handler('<v>', _invert_vertical_ship)
		if g_args.s:
			self.add_key_handler('<n>', self.n_key_handler)
		if g_args.a:
			self.add_mouse_handler('<Button-2>', self.show_debug_info)

		self.map_file_iter = itertools.cycle(os.path.join(maps_dir, f) for f in os.listdir(maps_dir) if os.path.isfile(os.path.join(maps_dir, f)))
		self.reload()

	def reload(self):
		file_name = self.map_file_iter.next()
		print 'The map will be loaded from %s'%file_name

		error_callback = lambda what: tkMessageBox.showerror('', what)
		add_item_args = {'X':OBSTACLE_VIEW}
		self.load_from_plain_file(file_name, add_item_args, error_callback)

		self.path_begin = None
		self.path_end = None
		self.ship = None

	def distance_to_finish(self, row, col):
		return abs(row-self.path_end.row) + abs(col-self.path_end.col)

	def check_free(self, points, forbidden_tags):
		for p in points:
			if not self.is_it_inside(*p) or self.what_is_here(*p) in forbidden_tags:
				return False
		return True

	def space_handler(self, sym):
		self.reload()

	# next in the step mode
	def n_key_handler(self, sym):
		self.draw_event.set()

	def show_debug_info(self, row, col):
		if self.what_is_here(row, col) == DEMO_TAG:
			print self.debug_path_points[self.debug_path_points.index((row, col))]

	def left_btn_handler(self, row, col):
		path_begin = SeaPoint(row, col)
		ship = Ship(row, col, self._vertical_ship)
		if not (self.check_free(path_begin.points(), (OBSTACLE_TAG,BOUNDARY_TAG)) and
			    self.check_free(ship.points(), (OBSTACLE_TAG,))):
			return

		if self.path_begin:
			self.path_begin.destroy()
		self.path_begin = path_begin

		if self.ship:
			self.ship.destroy()
		self.ship = ship

		self.draw_ship()

		self.calculate_path()

	def right_btn_handler(self, row, col):
		path_end = SeaPoint(row, col)
		if not self.check_free(path_end.points(), (OBSTACLE_TAG,BOUNDARY_TAG,SHIP_TAG)):
			return

		if self.path_end:
			self.path_end.destroy()
		self.path_end = path_end

		self.draw_ship()

		self.calculate_path()

	def stop_calculation(self):
		while self.calculating_thread.isAlive():
			self.draw_event.set()
			self.calculating = False
			self.calculating_thread.join(0.01)

		while 1:	#clear queue
			try:
				self.draw_queue.get_nowait()
			except Queue.Empty:
				break

	def restart_drawing_from_thread(self):
		if g_args.a:
			self.after_idle(self.draw_from_calculating_thread)	# interactive representation of viewed point
		else:
			self.after(300, self.draw_from_calculating_thread)	# only draw the route at the end

	def calculate_path(self):
		if self.path_begin is None or self.path_end is None:
			return

		self.stop_calculation()
		self.delete_by_tag(DEMO_TAG)

		self.calculating = True
		self.calculating_thread = threading.Thread(target=self.__calculate_path, args=())
		self.calculating_thread.start()
		self.restart_drawing_from_thread()

	def draw_from_calculating_thread(self):
		try:
			point, view = self.draw_queue.get_nowait()
			if view is None: # one strange particular case
				self.draw_route(point)
			else:
				point.draw(view, DEMO_TAG)
			if not g_args.s:
				self.draw_event.set()
		except Queue.Empty:
			pass
		if self.calculating:
			self.restart_drawing_from_thread()

	def __calculate_path(self):
		init_point = PathPoint(self.path_begin.row, self.path_begin.col, None, self.ship.vertical)
		open_list = [init_point]
		self.debug_path_points = [init_point]
		closed_list = []
		route = None

		while open_list and not route and self.calculating:
			debug_print('open_list:', open_list, '\n')
			curr = open_list.pop(0)
			closed_list.append(curr)
			self.draw_closed_point(curr)
			for n_pos, n_turn in Ship.adjacent_positions(curr.row, curr.col, curr.vertical):
				adj_point = n_pos + (curr.vertical^n_turn,)
				if adj_point in closed_list:
					continue

				need_resort = False
				if adj_point in open_list:
					assert open_list.count(adj_point)==1
					need_resort = open_list[open_list.index(adj_point)].try_udpate_cost(curr, n_turn)		#not too good
				else:
					new_point = PathPoint(n_pos[0], n_pos[1], curr, n_turn)
					self.draw_open_point(new_point)
					if new_point.row == self.path_end.row and new_point.col == self.path_end.col:
						route = new_point
						break
					bisect.insort_left(open_list, new_point)

				if need_resort:
					open_list.sort()

		self.send_draw_task(route, None)

		self.calculating = False

	def draw_route(self, route):
		if route:
			debug_print('[%d,%d] --> [%d,%d]'%(self.path_begin.row, self.path_begin.col, self.path_end.row, self.path_end.col))

			curr = route
			while curr:
				curr.draw(ROUTE_VIEW, DEMO_TAG)
				debug_print('route point:', curr)
				curr = curr.parent
		else:
			print 'No path!'

		self.draw_ship()

	def draw_ship(self):
		self.draw_boundary()
		if self.ship:
			self.ship.draw(SHIP_VIEW, SHIP_TAG)

	def draw_boundary(self):
		if self.path_end:
			self.path_end.draw(BOUNDARY_VIEW, BOUNDARY_TAG)
		if self.path_begin:
			self.path_begin.draw(BOUNDARY_VIEW, BOUNDARY_TAG)

	def draw_open_point(self, path_point):
		if g_args.a:
			self.send_draw_task(path_point, OPEN_LIST_VIEW)
			self.debug_path_points.append(path_point)
		debug_print('new open:', path_point)

	def draw_closed_point(self, path_point):
		if g_args.a:
			self.send_draw_task(path_point, CLOSED_LIST_VIEW)
		debug_print('new closed:', path_point)

	def send_draw_task(self, point, view):
		self.draw_event.clear()
		self.draw_queue.put_nowait((point, view))
		self.draw_event.wait()

def debug_print(*aa):
	if g_args.d:
		for a in aa:
			print a,
		print

def get_args():
	parser = argparse.ArgumentParser()
	parser.add_argument('-d', action='store_true', help='Debug output')
	parser.add_argument('-a', action='store_true', help='Animate the search process')
	parser.add_argument('-s', action='store_true', help='Animation in the step mode')
	return parser.parse_args()

g_args = None
g_sea = None

if __name__ == '__main__':
	root = Tkinter.Tk()
	root.geometry('%dx%d+%d+%d'%(950, 950, 40, 0))
	g_args = get_args()
	g_sea = Sea(root, 'maps')

	#TODO add into Sea
	def destroy_clb():
		g_sea.stop_calculation()
		root.destroy()
	root.protocol("WM_DELETE_WINDOW", destroy_clb)
	try:
		root.mainloop()
	except KeyboardInterrupt:
		destroy_clb()
