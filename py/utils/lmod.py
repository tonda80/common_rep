#!/usr/bin/python
#coding=utf8

import argparse
import re
import os
import sys
import itertools


def _log(msg):
	print msg

class App:
	F_FUNCTION_NAME = 'lmod'

	def __init__(self):
		parser = argparse.ArgumentParser(description='An utility for transformation of lines from standard input. \
A main thing is reluctance to learn sed and awk. I just don\'t want to do it.')

		parser.add_argument('-r', help='Regexp. Only a part of string corresponding to regexp will be returned. \
Don\'t forget quotes for complicated cases.')
		parser.add_argument('-l', help='Lambda. End of "lambda l:"')
		parser.add_argument('-n', help='Line numbers. Python range-like format, e.g. 20:45, :30, 10:')
		parser.add_argument('-f', help='Name of file with a definition of \'%s\' function. The most flexible way, e.g. any actions may be defined here'%self.F_FUNCTION_NAME)

		parser.add_argument('-s', help='Format string for "words" of each input line. E.g. "0 column is {0}"')
		parser.add_argument('--delimiter', help='Optional delimiter for getting of "words". For using with -s option')
		parser.add_argument('--raise_format_error', action='store_false',
					help='Raise exception if the format string cannot be formed. For using with -s option')
		parser.add_argument('--default_empty_word', action='store_true',
					help='Extends output result. With this option "-s {0} {1} {2}" with line "0 1" returns "0 1" but no empty line.')

		parser.add_argument('--in_file', help='Input file (stdin by default)')
		parser.add_argument('--out_file', help='Output file (stdout by default, @ for auto naming)')

		parser.add_argument('-nn', '--no_newline', action='store_true', help='Do not add a newline for output')

		parser.add_argument('--unique', action='store_true', help='Output unique lines only')

		# TODO after, before

		parser.parse_args(namespace = self)

	def get_in(self):
		if self.in_file is None:
			return sys.stdin
		return open(self.in_file, 'rb')

	def get_out(self):
		if self.out_file is None:
			return sys.stdout
		elif self.out_file=='@':
			self.out_file = '_out'.join(os.path.splitext(self.in_file)) if self.in_file else '_out'
		return open(self.out_file, 'wb')

	# Chooses a line modifier according to command line arguments
	def get_modifier(self):
		if self.r:
			self.re_template = re.compile(self.r)
			return self.regexp_modify_line
		elif self.f:
			scope = {}
			execfile(self.f, scope)
			# тут тонкий момент который я до конца не понимаю
			# только вызов с одним scope приводит к захвату в globals внутренней "рабочей" функции имен определенных в файле
			# например те только при таком вызове функции модулей определенных внутри файла будут доступны внутри рабочей функции
			return scope[self.F_FUNCTION_NAME]
		elif self.s:
			return self.format_modify_line
		elif self.l:
			return eval('lambda l:'+self.l)
		elif self.n:
			start, del_, end = self.n.partition(':')
			err = del_ != ':'
			try:
				self.n_start = int(start) if start else 0
				self.n_end = int(end) if end else float('inf')
			except:
				err = True
			if err or self.n_start >= self.n_end:
				raise RuntimeError('Wrong -n argument: %s'%self.n)
			self.n_cnt = -1
			#print self.n_start, self.n_end; exit(0)
			return self.counter_line

		raise RuntimeError('Modifier is NOT defined ')

	def regexp_modify_line(self, line):
		mo = self.re_template.search(line)
		if mo:
			return mo.group()

	def format_modify_line(self, line):
		splitted = line.split(self.delimiter)
		if self.default_empty_word:
			max_n = max(itertools.chain(map(int, filter(None, re.findall(r'\{(\d*?)\}', self.s))), [0]))
			k = max_n + 1 - len(splitted)
			if k > 0:
				splitted += ['']*k

		try:
			return self.s.format(*splitted)
		except:
			if not self.raise_format_error:
				raise

	def counter_line(self, line):
		self.n_cnt += 1
		if self.n_cnt >= self.n_start and self.n_cnt < self.n_end:
			return line

	def main(self):
		outf = inpf = None
		unique_lines = set()
		try:
			modifier = self.get_modifier()
			inpf = self.get_in()
			outf = self.get_out()
			for line in inpf:
				mod_line = modifier(line)
				if mod_line is not None:
					if self.unique:
						if mod_line in unique_lines:
							continue
						unique_lines.add(mod_line)

					outf.write(mod_line)
					if not self.no_newline:
						outf.write(os.linesep)
		except KeyboardInterrupt:
			pass
		finally:
			if inpf and inpf is not sys.stdin:
				inpf.close()
			if outf and outf is not sys.stdout:
				outf.close()

if __name__ == '__main__':
	App().main()
