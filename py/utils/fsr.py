#!/usr/bin/python
#coding=utf8

# File System Researcher

import os
import sys

import lsh
import path_getter
from getch import getch

class DirInfo:
	def __init__(self, **kw):
		for k,w in kw.iteritems():
			setattr(self, k, w)

class DirItem:
	def __init__(self, fullpath):
		self.fullpath = fullpath
		self.name = os.path.basename(fullpath)
		self.size = lsh.du(fullpath)[2]
		if os.path.isdir(fullpath):
			self.dir_flag = 'D'
		else:
			self.dir_flag = ''

	# for sorting
	def get_name(self):	return self.name
	def get_size(self):	return self.size

	def __str__(self):	# for debugging
		return '{} {} {} {}'.format(self.fullpath, self.name, self.size, self.isdir)

class Hlp:
	@staticmethod
	def get_int(st, upper_limit):
		try:
			num = int(st)
		except ValueError:
			return -1
		if num >= upper_limit:
			return -1
		return num

	@staticmethod
	def get_num_sym_by_sym(st, upper_limit):
		Hlp.print_('Item: %s'%st)

		max_len = len(str(upper_limit-1))
		while len(st) < max_len:
			c = getch()
			c_code = ord(c)
			if (lsh.f_lin and c_code == 127) or (lsh.f_win and c_code == 8):
				if st:
					Hlp.print_('\b')
				st = st[:-1]
			elif c.isdigit():
				st += c
				Hlp.print_(c)
			elif c_code == 13:
				break
			else:
				pass
				#print '>> [%s %d]'%(c, ord(c))
		print
		return Hlp.get_int(st, upper_limit)

	@staticmethod
	def print_(s):
		sys.stdout.write(s)
		sys.stdout.flush()

class FSApp:
	def __init__(self):
		path = path_getter.get_dir_name()

		self.need_print = False
		self.set_dir_info(path.decode('utf8'))	# see http://stackoverflow.com/questions/10180765/open-file-with-a-unicode-filename
		self.mainloop()

	def mainloop(self):
		while 1:
			self.print_dir_info()
			Hlp.print_('\nInput the command ("h" for getting help): ')
			cmd = getch()
			cd = ord(cmd)
			print cmd

			if cmd == 'h':
				self.print_help()
			elif cmd == 'q':
				break
			elif cmd == 'u':
				self.set_dir_info(os.path.dirname(self.dir_info.path))
			elif cmd == 'r':
				self.set_dir_info(self.dir_info.path)
			elif cd == 13:
				pass
			elif cmd.isdigit():
				n = Hlp.get_num_sym_by_sym(cmd, len(self.dir_info.items))
				if n != -1:
					self.set_dir_info(self.dir_info.items[n].fullpath)
				else:
					print 'Error. Wrong item.'
			else:
				print 'Error. Bad command: [%s](%d)'%(cmd, cd)

	def print_help(self):
		print '''
	h		Print this help
	q		Quit from the program
	u		Up to the parent directory
	r		Refresh the current directory
	[n]		Give a number of item for jump which should correspond to the existing item and the item should be a directory.
	'''

	def print_dir_info(self):
		if not self.need_print:
			return

		head = '\n%s into %s'%(self.dir_info.size, self.dir_info.path)
		print head
		print '-'*len(head)
		for i,e in enumerate(self.dir_info.items):
			try:
				print '%-4d %12s %2s %-2s %-s'%(i, lsh.human_size(e.size), '', e.dir_flag, e.name)
			except UnicodeEncodeError:
				# cp866 на винде не может вывести некоторые имена, пробуем в utf8, не знаю что тут умнее можно сделать
				print '%-4d %12s %2s %-2s %-s'%(i, lsh.human_size(e.size), '', e.dir_flag, e.name.encode('utf8'))

		self.need_print = False

	def set_dir_info(self, path):
		if not os.path.isdir(path):
			print 'Error. %s is not directory'%path
			return
		# TODO?? system size getting for velocity
		self.dir_info = DirInfo(path=path, size=lsh.du(path, '-h')[2], items=[])
		for e in os.listdir(path):
			self.dir_info.items.append(DirItem(os.path.join(path, e)))

		self.dir_info.items.sort(key=DirItem.get_size, reverse=True)		# default sorting
		self.need_print = True

if __name__ == '__main__':
	lsh.SIZE_ERROR_BEHAVIOUR = 1
	try:
		FSApp()
	except KeyboardInterrupt:
		print '\n\nCtrl-C. Exiting.'
