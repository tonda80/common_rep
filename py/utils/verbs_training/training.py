# coding=cp1251
from Tkinter import *

from random import choice as randomFrom
import os

from commonconfig import CommonConfig

TrainingFile = 'verbs.txt'
AnswerNumber = 4

class WndTraining():

	def __init__(self):
		self.root = Tk()
		self.root.title(u'Irregular verbs');
		self.root.minsize(600, 200)
		#w=640; h=100; x = self.root.winfo_screenwidth()/2-w/2; y = self.root.winfo_screenheight()/2-h/2
		#self.root.geometry('%dx%d+%d+%d'%(w,h,x,y))

		self.conf = CommonConfig('verbs_training', bflags=[1,1,1,1])

		#�������������� ����
		infoFrame = Frame(self.root, height=20, borderwidth=2, relief=GROOVE)
		infoFrame.pack(side=BOTTOM, expand=NO, fill=X)
		self.info = StringVar()
		Label(infoFrame, textvariable = self.info).pack(side = LEFT)

		mainFrame = Frame(self.root, borderwidth=2, relief=GROOVE)
		mainFrame.pack(side=BOTTOM, expand=YES, fill=BOTH)

		questionFrame = Frame(mainFrame, height=150, borderwidth=5, relief=GROOVE)
		questionFrame.pack(side=TOP, expand=NO, fill=X)
		self.question = StringVar()
		Label(questionFrame, textvariable = self.question, font=("Helvetica", 16, "bold")).pack()#side = CENTER)

		#variantFrame = Frame(mainFrame, height=40, borderwidth=1)
		#variantFrame.pack(side=TOP, expand=NO, fill=X)

		answerFrame = Frame(mainFrame, height=50)
		answerFrame.pack(side=TOP, expand=YES, fill=X, pady=20)

		self.answers = [StringVar() for i in xrange(AnswerNumber)]
		Label(answerFrame, textvariable=self.answers[0], font=("Courier New", 14)).grid(row=0,column=0, sticky=W, padx=20)
		Label(answerFrame, textvariable=self.answers[1], font=("Courier New", 14)).grid(row=0,column=1, padx=20)
		Label(answerFrame, textvariable=self.answers[2], font=("Courier New", 14)).grid(row=0,column=2, padx=20)
		Label(answerFrame, textvariable=self.answers[3], font=("Arial", 14, 'italic')).grid(row=0,column=3, padx=20, sticky=E)
		for i in xrange(answerFrame.grid_size()[0]):
			answerFrame.grid_columnconfigure(i,weight=1)

		self.tasks = []
		_path = os.path.join(os.path.dirname(__file__), TrainingFile)
		if not self.openTrainingFile(_path):
			return

		ButtonFrame = Frame(mainFrame, height=40, borderwidth=1, relief=GROOVE)
		ButtonFrame.pack(side=TOP, expand=NO, fill=X)
		#Button(ButtonFrame, text='Next',width=10,command=self.nextQuestion,takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)
		#Button(ButtonFrame, text='Check',width=10,command=self.checkAnswer,takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)
		Button(ButtonFrame, text='Check/Next',width=20,command=self.checkAndNext,takefocus=OFF).pack(side=RIGHT,padx=5,pady=5)

		bflags = self.conf.get('bflags')
		self.selectQuestion = [BooleanVar(ButtonFrame, bflags[i]) for i in xrange(AnswerNumber)]
		lbls = {1:'I', 2:'II', 3:'III', 4:'RU'}
		for i in xrange(AnswerNumber):
			Checkbutton(ButtonFrame,variable=self.selectQuestion[i],takefocus=OFF, text=lbls[i+1]).pack(side=LEFT, padx=5)

		self.nextQuestion()
		self.setInfo(u'')

		self.root.bind('<Key>', self.hndl_key)

		self.root.protocol("WM_DELETE_WINDOW", self.delete_window)

	def delete_window(self):
		self.conf.set('bflags', [c.get() for c in self.selectQuestion])
		self.conf.save()
		self.root.destroy()

	def hndl_key(self, ev):
		if ev.keysym=='space':
			self.checkAndNext()

	def	checkAndNext(self):
		if self.state == 'showAnswer':
			self.nextQuestion()
		elif self.state == 'showQuestion':
			self.checkAnswer()

	def setInfo(self, infoText):
		self.info.set(infoText)
	def setQuestion(self, question):
		self.question.set(question)
	def setAnswer(self, i, answer):
		self.answers[i].set(answer)
	def setAnswers(self, aList):
		for answer, newValue in zip(self.answers, aList):
			answer.set(newValue)

	def	nextQuestion(self):
		self.currTask = randomFrom(self.tasks)
		possibleQuestionList = []
		for word, checkbox in zip(self.currTask, self.selectQuestion):
			if checkbox.get(): possibleQuestionList.append(word)
		if possibleQuestionList:
			self.setQuestion(randomFrom(possibleQuestionList))
		else:
			self.setQuestion(self.currTask[0])
		self.setAnswers(['' for e in self.answers])
		self.state = 'showQuestion'

	def	checkAnswer(self):
		if self.state == 'showQuestion':
			self.setAnswers(self.currTask)
			self.state = 'showAnswer'

	def openTrainingFile(self, fileName):
		try:
			for st in open(fileName, 'r'):
				self.addTask(st.strip())
		except IOError:
			self.setInfo(u'���� %s �� ������'%fileName)
			return False
		#print self.tasks
		print u'Total %d verbs'%len(self.tasks)
		return True

	def addTask(self, st):
		if len(st) == 0:
			return
		tList = st.split(';')
		if len(tList) != AnswerNumber:
			print 'Skipped string <%s>'%st
			return
		self.tasks.append(map(str.strip, tList))



if __name__=='__main__':
	w=WndTraining()

	#w.setInfo('Lalalal')
	#w.setQuestion('Question')
	#for i in xrange(AnswerNumber):
#		w.setAnswer(i, 'Answer_'+str(i)*5)

	w.root.mainloop()
