﻿import sys
import os
import math
from time import time

class Stopwatch():
	Deb_Time_Print = False
	def __init__(self):
		self.time_label = time()
	def reset(self):
		self.time_label = time()
	def check_print(self, text_label = ''):
		curr_time = time()
		if self.Deb_Time_Print:
			print '%-25s%f'%(text_label, curr_time - self.time_label)
		self.time_label = curr_time

# находим простые числа меньшие rang
def find_primes(rang):
	if rang <= 2: return []
	primes = [2]
	#for d in xrange(3, rang, 2):	# ошибка для long чисел
	d = 3
	while d<rang:
		for p in primes:
			if (d%p == 0): break
			if (p*p > d):		# (p > d/2): # плохое условие
				primes.append(d)
				break
		d += 2
	return primes

# находим простые множители числа num
# Медленный. Сперва ищем все простые, потом проверяем
def find_factors_slow(num):
	lst_pr = find_primes( int(math.sqrt(num)) + 1 )	# список простых чисел из которых может состоять число num меньшие корня из num
	lst_fact = []	# список множителей
	for d in lst_pr:
		while num%d == 0:
			num /= d
			lst_fact.append(d)
		if num == 1: return lst_fact
	lst_fact.append(num)	# перебрали все простые меньше корня, значит осталось одно большее
	return lst_fact

# Вспомогательная функция. primes - проинициализированный список простых
# Поиск (и вставка в primes) меньшего простого из диапазона [n_first, n_last] перебор НЕЧЕТНЫХ, т.е n_first должно быть нечетным!!
def find_first_prime(n_first, n_last, primes):
	d = n_first
	while d <=n_last:
		for p in primes:
			if (d%p == 0): break
			if (p*p > d):
				primes.append(d)
				return d
		d += 2
	return

# находим простые множители числа num. Быстрее чем find_factors_slow()
def find_factors(num):
	if num <= 0: raise ValueError
	if num <= 3: return ((num, 1), )
	lst_fact = []
	primes = [2, 3]	# список простых

	cnt = 0
	# для 2 отдельный тест, чтобы далее проверять лишь нечетные
	#--------------------
	while num%2 == 0:	# является ли 2 множителем
			num /= 2
			cnt += 1
	if cnt > 0:			# добавляем в список множителей
		lst_fact.append((2, cnt))
		cnt = 0
	#--------------------

	pr = 3			# простое число для проверки
	while pr:
		while num%pr == 0:	# является ли это простое число множителем
			num /= pr
			cnt += 1
		if cnt > 0:			# добавляем в список множителей
			lst_fact.append((pr, cnt))
			cnt = 0
		pr = find_first_prime(pr+2, int(math.sqrt(num))+1, primes)

	if num != 1:	# в остатке простое число большее своего корня
		lst_fact.append((num, 1))
	return lst_fact

if __name__== '__main__':
	def usage():
		print u'''python primes.py SOMETHING
Что именно
0 \t\tВыводит простые числа меньшие заданного
1 \t\tвыводит множители вводимых чисел
2 \t\tпроверяем что сумма всех простых чисел меньших миллиона является простым числом
3 N1 N2 \tсокращаем N1, N2 чисел на общие множители
4 \t\tтест
5 и 6 \t\tИллюстрация к проблеме Гольдбаха (первая проблема Ландау)
7 \t\tBased on [How many circular primes are there below one million? https://projecteuler.net/problem=35]
8 \t\tПроверяет простое ли введенное число
'''

	# разные варианты применения в зависимости от переданного аргумента
	if len(sys.argv)<2:
		usage()
		exit()

	if sys.argv[1]=='--help':
		usage()
		exit()

	if sys.argv[1]=='1' or sys.argv[1]=='0':
		'выводит множители вводимых чисел'
		while 1:
			try:
				input = int(raw_input('>> '))
			except ValueError:
				print(u'Надо ввести натуральное число!'); continue
			except KeyboardInterrupt:
				exit()
			if input <=0 : print(u'Надо ввести положительное число!'); continue

			if sys.argv[1]=='0':	# вывод простых чисел
				pp = find_primes(input)
				for p in pp:
					print p,
				print '\nTotal %d'%len(pp)
				continue

			list_out = find_factors(input)	# вывод простых множителей
			for n, c in list_out[:-1]:
				print '%d^%d +'%(n, c),
			print '%d^%d'%(list_out[-1][0], list_out[-1][1])

	elif sys.argv[1]=='2':
		# проверяем что сумма всех простых чисел меньших миллиона является простым числом
		lst = find_primes(1000000)
		print lst[-10:]
		s = sum(lst)
		print find_factors_slow(s)
		print find_factors(s)

	elif sys.argv[1]=='3':
		# сокращаем пару чисел на общие множители
		try:
			a = int(sys.argv[2])	# первое число
			b = int(sys.argv[3])	# второе число
		except IndexError, ValueError:
			print u'Чего-то не то вводишь, друг..'
			exit()
		# находим множители
		f_a = find_factors(a)
		f_b = find_factors(b)
		m = 1
		for n_b, c_b in f_b:
			for n_a, c_a in f_a:
				if n_a == n_b:
					m *= n_a**min(c_b, c_a)
				if n_a > n_b:	# списки упорядочены
					break
		print '%d/%d \t%f \t\t%d'%(a/m, b/m, float(a)/b, m)

	elif sys.argv[1]=='4':
		# сравнение find_factors_slow и find_factors
		while 1:
			num = int(raw_input('>> '))
			t1 = os.times()
			l = find_factors(num)
			t2 = os.times()
			print l
			print 'Time for a new method - %f'%(t2[0]-t1[0]+t2[1]-t1[1])

			t1 = os.times()
			l = find_factors_slow(num)
			t2 = os.times()
			print l
			print 'Time for a old method - %f'%(t2[0]-t1[0]+t2[1]-t1[1])

	elif sys.argv[1]=='5' or sys.argv[1]=='6':
		'''Проблема Гольдбаха (первая проблема Ландау): доказать или опровергнуть, что каждое чётное число, большее двух, может быть представлено в виде суммы двух простых чисел, а каждое нечётное число, большее 5, может быть представлено в виде суммы трёх простых чисел.'''
		TEST_MAX = 100000 + 1
		PROGRESS_STEP = 10000
		OUTPUT_SUCCESS = 0	# вывод подтверждения предположения

		primes = find_primes(TEST_MAX)
		#rev_primes = primes[:]; rev_primes.reverse()

		# возможные оптимизации
		# если evN == 3 + prime, то ev(N+1) = 5 + prime и ev(N+2) = 7 + prime
		# как-то быстрее приходить к нужному j, не пробегать заведомо маленькие значения
		def test_even_number(even):
			for i in primes:
				for j in primes:
					s = i + j
					if s == even:
						if OUTPUT_SUCCESS:
							print '%d == %d + %d'%(even, i, j)
						return True
					if s > even:
						break
			raise RuntimeError('We refuted it! Even %d cannot be factorized'%even)

		def test_odd_number(odd):
			for i in primes:
				for j in primes:
					for k in primes:
						s = i + j + k
						if s == odd:
							if OUTPUT_SUCCESS:
								print '%d == %d + %d + %d'%(odd, i, j, k)
							return True
						if s > odd:
							break
			raise RuntimeError('We refuted it! Odd %d cannot be factorized'%odd)

		def show_progress(step):
			global curr_time
			if step%PROGRESS_STEP == 0:
				print '*', time() - curr_time
				curr_time = time()

		curr_time = time()
		if sys.argv[1]=='5':
			print 'Test for even'
			for even in xrange(4, TEST_MAX, 2):
				test_even_number(even)
				show_progress(even)
		elif sys.argv[1]=='6':
			print 'Test for odd'
			for odd in xrange(7, TEST_MAX, 2):
				test_odd_number(odd)
				show_progress(odd)
		print '\nAt least up to %d Landau guessed right!'%TEST_MAX

	elif sys.argv[1]=='7':
		''' Based on [How many circular primes are there below one million? https://projecteuler.net/problem=35] '''
		N = 1000000

		def rev_num(n):
			ret = 0
			while n > 0:
				n,c = divmod(n,10)
				ret = ret*10 + c
			return ret

		sw = Stopwatch()

		p_nums = set(find_primes(N+1))
		sw.check_print('find_primes')

		reverse_primes = set()
		cnt_mirror_nums = 0

		for n in p_nums:
			rn = rev_num(n)
			if rn in p_nums and rn not in reverse_primes:
				reverse_primes.add(n)
				#print n,
				if n == rn:
					cnt_mirror_nums += 1
					print n,
		print
		sw.check_print('reverse_primes')

		q_uniq = len(reverse_primes)
		q_all = q_uniq*2 - cnt_mirror_nums
		q_primes = len(p_nums)
		print 'Numbers less %d: primes - %d; reverse primes - %d (%d); mirror primes - %d'%(N, q_primes, q_all, q_uniq, cnt_mirror_nums)

	elif sys.argv[1]=='8':
		while 1:
			try:
				input = int(raw_input('>> '))
			except ValueError:
				print(u'Надо ввести натуральное число!'); continue
			except KeyboardInterrupt:
				exit()
			if input <=0 : print(u'Надо ввести положительное число!'); continue

			if len(find_factors(input)) == 1:
				print u'Простое'
			else:
				print u'Составное'

	else:
		usage()