# coding=utf8

# на самом деле, удобнее пользоваться python -mjson.tool
# но пусть будет вдруг расширю функционал

import json
import sys

try:
    json.dump(json.load(sys.stdin), sys.stdout, indent=4, separators=(',', ' : '), sort_keys=True)
except Exception as e:
    sys.stderr.write('{}'.format(e))
sys.stdout.write('\n')
