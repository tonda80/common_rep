#coding=utf8

import logging
import fnmatch
import importlib
from datetime import datetime
import subprocess
#from time import sleep
from uTkinter import *
import lsh

def create_global_logger(format='%(levelname)s  %(asctime)s  %(message)s', level = 20):
	global g_logger
	logging.basicConfig(format=format)
	g_logger = logging.getLogger('main')
	g_logger.setLevel(level)	# CRITICAL 50, ERROR 40, WARNING 30, INFO 20, DEBUG 10, NOTSET 0

# Intefaces ----------------------------------

class IGetter:
	def __init__(self, root, excluding_patterns = None):
		self.root = root			# directory for analysis

		if excluding_patterns:		# glob patterns for excluding (str, tuple or list)
			if isinstance(excluding_patterns, basestring):
				excluding_patterns = (excluding_patterns, )
			def excluder(name):
				for p in excluding_patterns:
					if fnmatch.fnmatchcase(name, p):
						return False
				return True
			self.excluding_clb = excluder
		else:
			self.excluding_clb = None

	def get_files(self):			# filterd _find_files
		return filter(self.excluding_clb, self._find_files())

	def _find_files(self):			# returns a list of file pathes (relative from root)
		raise NotImplementedError

class ISender:
	def __init__(self, connection):
		self.connection = connection	# object with settings of connection (e.g.: host, port, login, password )
		self.open()

	def open(self):					# opens connection, may be called for already opened connection
		raise NotImplementedError

	def send(self, src, trg):		# sends src to trg
		raise NotImplementedError

class IApp:
	def __init__(self, config):
		self.config = config
		### config mandatory attributes
		# config.Sender				type of used Sender
		# config.Connection			Connection attributes depend on appropriate Sender (e.g.: host, port, login, password)
		# config.Getter				type of used Getter
		# config.SourceRoot			root directory of the source
		# config.DestinationRoot	root directory of the destination
		### config optional attributes for Getter
		# config.ExcludIngPatterns		glob patterns for excluding (str, tuple or list) E.g.: '.git' or ['*.txt', 'bad?.bmp']
		# config.GetterExtraParameters	some set of specific for concret getter parameters (e.g. perforcePath for PerforceGetter)
		#								this is not transferred into IGetter

		self.sender = config.Sender(config.Connection)

		excluding_patterns = getattr(config, 'ExcludIngPatterns', None)
		getter_extra_parameters = getattr(config, 'GetterExtraParameters', {})
		self.getter = config.Getter(config.SourceRoot, excluding_patterns, **getter_extra_parameters)

		self.sourceWinSep = False
		self.destWinSep = False
		if config.SourceRoot.find('\\') != -1:
			self.sourceWinSep = True
			g_logger.info('Detected win separator for source files')
		if config.DestinationRoot.find('\\') != -1:
			self.destWinSep = True
			g_logger.info('Detected win separator for destination files')

	def src_full_path(self, f):
		path = self.config.SourceRoot + '/' + f
		if self.sourceWinSep:
			path = path.replace('/', '\\')
		return path
	def dest_full_path(self, f):
		path = self.config.DestinationRoot + '/' + f
		if self.destWinSep:
			path = path.replace('/', '\\')
		return path

	def get_files(self):
		return self.getter.get_files()

	def send_files(self, files):
		if files:
			self.sender.open()
			for f in files:
				self.sender.send(self.src_full_path(f), self.dest_full_path(f))

# Applications ----------------------------------

class SimpleConsoleApp(IApp):
	def __init__(self, config):
		IApp.__init__(self, config)
	def make_sync(self):
		self.send_files(self.get_files())

class TkApp(IApp):
	def __init__(self, config):
		Q_CHECK_COLUMNS = 10

		IApp.__init__(self, config)

		self.root = Tk()
		self.root.title(u'fsync')
		self.root.minsize(440, 240)

		self.info = StatusBar(self.root)

		# frame with checks
		self.fr_checks = uFrame(self.root, gmArgs = {'expand':YES, 'fill':BOTH})
		self.dict_checks = {}	# {path:uCheckButton, }
		self.q_check_columns = Q_CHECK_COLUMNS
		self.update()

		# button frame
		self.fr_btn = uFrame(self.root, height=30, gmArgs = {'expand':NO, 'fill':X})
		btn_gm_args = {'side':LEFT, 'padx':5, 'pady':1}
		uButton(self.fr_btn, self.send, 'Send', takefocus=True, gmArgs = btn_gm_args).focus_set()
		uButton(self.fr_btn, self.update, 'Update', takefocus=True, gmArgs = btn_gm_args)
		uButton(self.fr_btn, self.check_all, 'Check all', takefocus=True, gmArgs = btn_gm_args)
		uButton(self.fr_btn, self.uncheck_all, 'Uncheck all', takefocus=True, gmArgs = btn_gm_args)

	def mainloop(self):
		self.root.mainloop()

	def check_all(self):
		for c in self.dict_checks.itervalues(): c.setVar(True)
	def uncheck_all(self):
		for c in self.dict_checks.itervalues(): c.setVar(False)
	def clear_all(self):
		for c in self.fr_checks.children.values(): c.destroy()
		self.dict_checks = {}

	def send(self):
		sending = []
		for k,v in self.dict_checks.iteritems():
			if v.getVar(): sending.append(k)
		self.info.set('%s  Trying to send %s files'%(self.timestamp(), len(sending)))
		self.root.update_idletasks()
		self.send_files(sending)
		self.info.set('%s  Total %s'%(self.timestamp(), len(sending)))

	def timestamp(self):
		return datetime.now().strftime('%H:%M:%S')

	def update(self):
		self.clear_all()
		new_frame = lambda: uFrame(self.fr_checks, relief=FLAT, gmArgs = {'expand':YES, 'fill':BOTH, 'side':LEFT})
		currFrame = new_frame()
		check_cnt = 0
		for f in self.get_files():
			self.dict_checks[f] = uCheckbutton(currFrame, f, defaultValue = True, gmArgs = {'side':TOP,'anchor':'w','expand':NO,'pady':0,'ipady':0, 'padx':10})
			check_cnt += 1
			if check_cnt >= self.q_check_columns:
				currFrame = new_frame()
				check_cnt = 0

# Senders ----------------------------------

class TestSender(ISender):
	def __init__(self, conn):
		ISender.__init__(self, conn)

	def open(self):
		g_logger.info('TestSender.open %s', self.connection)

	def send(self, src, trg):
		g_logger.info('TestSender.send %s => %s', src, trg)

class FTPSender(ISender):
	def __init__(self, conn):
		self.ftplib = importlib.import_module('ftplib')
		ISender.__init__(self, conn)

	def open(self):
		c = self.connection
		g_logger.info('FTPSender is connecting to %s', c.host)
		self.ftp = self.ftplib.FTP(c.host, c.login, c.password)

	def send(self, src, trg):
		g_logger.info('FTPSender.send %s => %s', src, trg)
		self.ftp.storbinary('STOR '+trg, open(src, 'rb'))

class SCPSender(ISender):
	def __init__(self, conn):
		self.paramiko = importlib.import_module('paramiko')
		self.scp = importlib.import_module('scp')

		self.sshclient = self.paramiko.SSHClient()
		self.sshclient.set_missing_host_key_policy(self.paramiko.AutoAddPolicy())
		self.sshclient.connect(hostname=conn.host,port=conn.port,username=conn.login,password=conn.password)

		ISender.__init__(self, conn)

	def open(self):
		g_logger.info('SCPSender is connecting to %s', self.connection.host)
		self.scpclient = self.scp.SCPClient(self.sshclient.get_transport())							# it works quickly

	def reopen(self):
		c = self.connection
		g_logger.info('SCPSender is reopening connection to %s', c.host)
		self.sshclient.connect(hostname=c.host,port=c.port,username=c.login,password=c.password)	# it takes visible time
		self.scpclient = self.scp.SCPClient(self.sshclient.get_transport())

	def send(self, src, trg):
		g_logger.info('SCPSender.send %s => %s', src, trg)
		try:
			self.scpclient.put(src, trg)
		except:								# tries more one time TODO defne reason
			self.reopen()								# special case for expired connections
			self.sshclient.exec_command('rm -f ' + trg)	# special case for write-protected file
			self.scpclient.put(src, trg)


class SFTPSender(ISender):
	pass	# paramiko has builtin sftp

# Getters ----------------------------------

class TestGetter(IGetter):
	def __init__(self, root, e_p):
		IGetter.__init__(self, root, e_p)
		self.os = importlib.import_module('os')

	def _find_files(self):
		#return raw_input('\n\nEnter files ("," is separator): ').split(',')
		return self.os.listdir(self.root)

class GitGetter(IGetter):
	def __init__(self, root, e_p):
		IGetter.__init__(self, root, e_p)

	def _find_files(self):
		result = set()
		data = lsh.run(('git', 'status', '-su'), cwd=self.root)
		for st in data.split('\n'):
			g_logger.debug('git string: %s', st)
			words = st.split()
			if len(words) == 2:
				result.add(words[1])
			elif len(words) != 0:
				g_logger.debug('unexpected git string: %s', st)
		return result

class PerforceGetter(IGetter):
	def __init__(self, root, e_p, **extra_parameters):
		IGetter.__init__(self, root, e_p)
		self.perforce_root = extra_parameters['perforce_root']
		self.p4_env = extra_parameters['p4_env']

	def _find_files(self):
		result = set()
		data = lsh.run(('p4', 'opened', '-s', self.perforce_root+'...'), cwd=self.root, env=self.p4_env)
		for st in data.split('\n'):
			err = True
			g_logger.debug('perforce string: %s', st)
			if '- edit default change' in st:
				fname = st.split()[0]
				if fname.startswith(self.perforce_root):
					err = False
					fname = fname[len(self.perforce_root):]
					g_logger.debug('file is detected: %s', fname)
					result.add(fname)
			elif st.strip() == '' or 'file(s) not opened on this client.' in st:
				err = False

			if err:
				g_logger.warning('unexpected string: %s', st)

		return result


# Debug ----------------------------------

if __name__ == '__main__':
	class Config:
		class FTPConnection:
			host = '192.168.126.130'
			login = 'ant'
			password = 'linuxant'
		class SSHConnection:
			host = '10.17.237.20'
			port = 22
			login = 'Anton_Berezin'
			password = 'zeb33ra'

		Connection = FTPConnection				#FTPConnection, SSHConnection
		Sender = FTPSender						#TestSender, FTPSender, SCPSender
		Getter = GitGetter						#TestGetter, GitGetter
		SourceRoot = r'D:\files\py'		#r'D:\rep-s\ttrt\other_project\qa-regression-testing'
		DestinationRoot = 'py'			#'rep-s/ttrt/other_projects/qa-regression-testing'
		#ExcludIngPatterns = ('*.mp3', '2.*', '?.*', '*fs*.py')

	create_global_logger()	#level=1
	#SimpleConsoleApp(Config).make_sync()
	TkApp(Config).mainloop()

