#!/usr/bin/python
#coding=utf8

# https://www.finances-analysis.ru/procent/annuity.htm
# http://www.banki.ru/wikibank/raschet_differentsirovannogo_plateja/

from uTkinter import *

# A = P * (1+P)^N / ((1+P)^N-1)
def ann_k(per_y, n):
	p = per_y/12.0*0.01
	s = (1+p)**n
	return p*s/(s-1)

def calc_ann_credit(credit_sum, per_y, months):
	a = ann_k(per_y, months)
	return a*credit_sum

def calc_diff_credit(credit_sum, per_y, months):
	fixed_part = credit_sum/months
	payments = []
	for i in xrange(months):
		perc_part = (credit_sum - fixed_part*i)*per_y/100*30/365	# тут может быть 28-31 день
		payments.append(fixed_part + perc_part)
	return tuple(payments)

if __name__ == '__main__':
	root = uTk(u'tkgrep', 640, 80)
	fIn = uFrame(root, gmArgs={'expand': YES, 'fill': BOTH})
	fOut = uFrame(root, gmArgs={'expand': YES, 'fill': BOTH})

	lblFont = ('Symbol', 12)
	entrFont = ('Symbol', 12)
	answFont = ('Symbol', 12, 'bold')
	pady=10
	padx=30

	uLabel(fIn, u'Сумма кредита', gmArgs={'row' : 0, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
	eSum = uEntry(fIn, 200, DoubleVar, gmArgs={'row' : 0, 'column': 1, 'pady': pady}, font=entrFont)

	uLabel(fIn, u'Процент', gmArgs={'row' : 1, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
	ePercent = uEntry(fIn, 12, DoubleVar, gmArgs={'row' : 1, 'column': 1, 'pady': pady}, font=entrFont)

	uLabel(fIn, u'Срок в месяцах', gmArgs={'row' : 2, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
	ePeriod = uEntry(fIn, 12, IntVar, gmArgs={'row' : 2, 'column': 1, 'pady': pady}, font=entrFont)

	oAnn, oDiff = (u'Аннуитетный', u'Дифференцированный')
	optType = uOptionMenu(fIn, (oAnn, oDiff), gmArgs={'row' : 3, 'column': 0, 'pady': pady}, )
	optType.config(width=30)	# ?? it doesn't work as constructor arg

	def calculate():
		root.statusBar.set(u'')
		try:
			credit_sum = eSum.getVar()
			percent = ePercent.getVar()
			months = ePeriod.getVar()
			fAnn = optType.getVar() == oAnn
		except ValueError:
			root.statusBar.set(u'Ошибка в исходных данных')
			return

		for ch in fOut.winfo_children():
			ch.destroy()

		if fAnn:
			ann_payment = calc_ann_credit(credit_sum, percent, months)
			return_sum = ann_payment*months
		else:
			diff_payments = calc_diff_credit(credit_sum, percent, months)
			#root.statusBar.set(u'Детали дифференцированного платежа в консоли')
			print diff_payments
			return_sum = sum(diff_payments)

		uLabel(fOut, u'Возвращаемая сумма', gmArgs={'row' : 0, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
		uLabel(fOut, '%f'%return_sum, gmArgs={'row' : 0, 'column': 1, 'pady': pady}, width = 40, font=answFont)

		overpayment1 = return_sum - credit_sum
		overpayment2 = return_sum/credit_sum
		uLabel(fOut, u'Переплата', gmArgs={'row' : 1, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
		uLabel(fOut, '%f (%f)'%(overpayment1, overpayment2), gmArgs={'row' : 1, 'column': 1, 'pady': pady}, width = 40, font=answFont)

		if fAnn:
			uLabel(fOut, u'Ежемесячный платеж', gmArgs={'row' : 2, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
			uLabel(fOut, '%f'%ann_payment, gmArgs={'row' : 2, 'column': 1, 'pady': pady}, width = 40, font=answFont)
		else:
			uLabel(fOut, u'Максимальный платеж', gmArgs={'row' : 2, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
			uLabel(fOut, '%f'%diff_payments[0], gmArgs={'row' : 2, 'column': 1, 'pady': pady}, width = 40, font=answFont)

			uLabel(fOut, u'Минимальный платеж', gmArgs={'row' : 3, 'column': 0, 'pady': pady}, width = 40, font=lblFont, anchor=W, padx=padx)
			uLabel(fOut, '%f'%diff_payments[-1], gmArgs={'row' : 3, 'column': 1, 'pady': pady}, width = 40, font=answFont)

	uButton(fIn, calculate, u'Расчитать', gmArgs={'row' : 3, 'column': 1, 'pady': pady}, width = 40, font=lblFont)
	root.bind_return(calculate)

	root.mainloop()

