#!/usr/bin/python
# coding=utf8

import threading
import time

class Scheduler:
	class Task:
		counter = 0
		counter_lock = threading.Lock()

		def __init__(self, clb, args, nRep, delay):
			if nRep == 0:	#! negative nRep means always
				raise RuntimeError('wrong a number of repeats') 
			self.clb = clb
			self.args = args
			self.nRep = nRep
			self.clb = clb
			self.delay = delay
			self.worked = True
			with self.__class__.counter_lock:
				self.id_ = self.__class__.counter
				self.__class__.counter += 1

			self._restart()	
				
		def _restart(self, dt=0):
			if self.worked:
				self.timer = threading.Timer(self.delay-dt, self._execute)
				self.timer.start()

		def _execute(self):	# это вызывается в отдельном потоке, но не должно быть никаких гонок по идее
			if self.worked:
				if self.nRep > 0:
					self.nRep -= 1
				if self.nRep == 0:
					self.worked = False	
				
				t0 = time.time()
				self.clb(*self.args)
				#! коллбек может узнать о конце работы опросив worked
				#! также там можно поменять worked, delay итп
				self._restart(time.time()-t0)	# компенсируем время выполнения колбека

		def cancel(self):
			self.worked = False
			self.timer.cancel()
			
		def join(self):
			if self.worked:
				self.timer.join()
	
if __name__ == '__main__':
	def test1():
		print 'test1'
		cnt = [0]
		def test():
			print 'hi', cnt, time.time()
			cnt[0] += 1
			if cnt[0]==5: task.delay = 2
			if cnt[0]==7: task.cancel()
			time.sleep(0.1)
			
		task = Scheduler.Task(test, (), 10, 0.1)
		task.join()


	test1()
