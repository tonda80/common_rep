#coding=utf8

import tkFileDialog
import Tkinter
from os.path import dirname
import commonconfig

class SubElement:
	def __init__(self, line_list):
		self.num = line_list[0]
		self.str_time = line_list[1]
		self.text = ''.join(line_list[2:])

class SubReader:
	def __init__(self, fname):
		self.fname = fname

	def split(self, action):
		with open(self.fname) as file_:
			buffer = []
			for line in file_:
				if not line.strip() and buffer:
					action(SubElement(buffer))
					buffer = []
				else:
					buffer.append(line)
			if buffer:
				action(SubElement(buffer))

fname = raw_input('Enter a full file name (an empty string for interactive select): ')
if not fname:
	w = Tkinter.Tk(); w.withdraw()
	config = commonconfig.CommonConfig('subcrop', initialdir='D:\\')
	fname = tkFileDialog.askopenfilename(initialdir=config['initialdir'])
OUTP = fname + '.txt'

config['initialdir'] = dirname(fname)
config.save()


reader = SubReader(fname)
outf = open(OUTP, 'w')
def pp(el):
	outf.write(el.text.replace('\n', ' '))
	outf.write('\n')

reader.split(pp)