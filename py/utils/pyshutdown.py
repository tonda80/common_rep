# coding=cp1251
from Tkinter import *
from datetime import datetime, timedelta
from subprocess import call

from commonconfig import CommonConfig

class mainWindow():

	def __init__(self):
		self.root = Tk(); self.root.title(u'Shutdown');

		self.config = CommonConfig('pyshutdown', 30)

		# info frame
		fr_info = Frame(self.root, height=10, borderwidth=2, relief=GROOVE)
		fr_info.pack(side=BOTTOM, expand=NO, fill=X)
		self.info = StringVar()
		Label(fr_info, textvariable = self.info).pack(side = LEFT)

		# frame with checks
		fr_main = Frame(self.root, borderwidth=2, relief=GROOVE)
		fr_main.pack(side=TOP, expand=YES, fill=BOTH)

		fr_1 = Frame(fr_main); fr_1.pack(side=TOP, expand=YES, fill=BOTH)
		fr_2 = Frame(fr_main); fr_2.pack(side=TOP, expand=YES, fill=BOTH)
		fr_3 = Frame(fr_main); fr_3.pack(side=TOP, expand=YES, fill=BOTH)

		Button(fr_1, text='Turn off after',width=10, command=self.turnOffAfter).pack(side=LEFT,padx=5,pady=5)
		self.entr_time = Entry(fr_1, width=10)
		self.entr_time.pack(side=LEFT,padx=5,pady=5)
		self.entr_time.insert(0, str(self.config.cfg))
		Label(fr_1, text='min').pack(side=LEFT,padx=5,pady=5)

		Button(fr_2, text='Cancel',width=10, command=self.cancelTurnOff).pack(side=LEFT,padx=5,pady=5)

		Button(fr_3, text='Turn off now!',width=10, command=self.turnOffNow).pack(side=LEFT,padx=5,pady=5)

		self.root.protocol("WM_DELETE_WINDOW", self.delete_window)

		self.root.mainloop()

	def	turnOffAfter(self):
		try:
			min = int(self.entr_time.get())
		except ValueError:
			self.info.set(u'������ - �� ������!')
			return

		ret = call('shutdown -s -t %d'%(min*60,), shell=True)
		if ret==0:
			dateTurnOff = datetime.now() + timedelta(minutes=min)
			self.info.set(u'���������� � %s'%dateTurnOff.strftime('%H:%M:%S'))
			self.config.cfg = min
		else:
			self.info.set(u'����-�� �� ���������� (%d)'%ret)

	def	cancelTurnOff(self):
		ret = call('shutdown -a', shell=True)
		if ret==0:
			self.info.set(u'���������� ��������')
		else:
			self.info.set(u'����-�� �� ���������� (%d)'%ret)

	def	turnOffNow(self):
		call('shutdown -p', shell=True)

	def delete_window(self):
		self.config.save()
		self.root.destroy()


mainWindow()
