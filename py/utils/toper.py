#coding=utf8

import random
from getch import getch

SUITS = ('hearts', 'clubs', 'diamonds', 'spades')	# черви, крести, буби, пики

class Card:
	str_values = {1:'joker', 11:'jack', 12:'queen', 13:'king', 14:'ace'}

	def __try_set_value(self, value, num_value, str_value):
		if value == num_value or (isinstance(value, str) and value.lower() == str_value):
			self.value = value
			return True
		return False

	def __init__(self, value, suit = ''):
		# special joker case
		if self.__try_set_value(value, 1, 'joker'):
			if not isinstance(suit, str):
				RuntimeError('Wrong a joker label: '+suit)
			self.suit = suit	# some joker label if any (red, star, color, something else)
			return
		# suit
		if suit in SUITS:
			self.suit = suit
		elif 0 <= suit < len(SUITS):
			self.suit = SUITS[suit]
		else:
			raise RuntimeError('Wrong a card suit: '+suit)
		# value
		self.value = None
		if isinstance(value, int) and (5 < value < 11):
			self.value = value
		else:
			for n,s in self.str_values.iteritems():
				if n != 1 and self.__try_set_value(value, n, s):
					break
		if not self.value:
			raise RuntimeError('Wrong a card value: '+value)

	def __repr__(self):
		return self.__str__()
	def __str__(self):
		return '%s_%s'%(self.str_value(), self.suit)

	def str_value(self):
		return self.str_values[self.value] if self.value in self.str_values else str(self.value)

def pop_random(list_):
	return list_.pop(random.randrange(len(list_)))


class ToperGame:
	class Thrower:
		def __init__(self, enabled):
			self.enabled = enabled
			self.pack = []
		def reset(self):
			self.pack = []
			self.curr_thrown = 6
			self.curr_thrown_cnt = 0
		def check(self, buf):
			if not self.enabled:
				return
			while True:
				for crd in buf[:]:
					if crd.value == self.curr_thrown:
						buf.remove(crd)
						self.pack.append(crd)
						self.curr_thrown_cnt += 1
						if self.curr_thrown_cnt == 4:
							self.curr_thrown += 1
							self.curr_thrown_cnt = 0
							break
				else:
					break

	def __init__(self):
		self.need_print_each_step = False
		self.need_print_state_each_step = False
		self.step_mode = False
		self.taking_mode = 1	# see winner_takes_cards method
		self.thrower = self.Thrower(False)

	def restart(self):
		random.seed()
		init_pack = [Card(v, s) for v in xrange(6, 15) for s in xrange(4)]
		self.pack1 = []; self.pack2 = []
		while init_pack:
			self.pack1.append(pop_random(init_pack))
			self.pack2.append(pop_random(init_pack))
		self.step = 0
		self.dispute = []
		self.thrower.reset()

		#self.all_aces_to_player1()

		self.debug_check_error(len(self.pack2) != 18 or len(self.pack1) != 18, 'Wrong begin')

		self.print_state('Begin of game!')
		self.loop()

		if not self.pack1 and not self.pack2:
			game_result = 'Draw!'
		else:
			game_result = '%s player have won!'%('First' if self.pack1 else 'Second')
		self.print_state('End of game! %s'%game_result)

	def compare(self, card1, card2):
		return card1.value - card2.value

	def loop(self):
		while self.pack1 and self.pack2:
			self.step += 1
			card1 = self.pack1.pop(0)
			card2 = self.pack2.pop(0)
			cmp_res = self.compare(card1, card2)

			self.dispute.append(card1)
			self.dispute.append(card2)

			if cmp_res == 0:
				cmp_s = '=='
			else:
				if cmp_res > 0:
					winner = self.pack1
					cmp_s = '<='
				else:
					winner = self.pack2
					cmp_s = '=>'
				self.winner_takes_cards(winner)		# move self.dispute to the winner

			self.check_user_request()

			if self.need_print_step or self.step_mode:
				self.print_step('%s %s %s'%(card1, cmp_s, card2))

			if self.need_print_state_each_step or self.step_mode:
				self.print_state()

			if self.step_mode:
				raw_input('Press enter to the next step\n')

			# debug
			total = len(self.pack1) + len(self.pack2) + len(self.dispute) + len(self.thrower.pack)
			self.debug_check_error(total != 36, 'Total cards %d'%total)

	def winner_takes_cards(self, winner):
		# The winner takes as the buffer gets (1th, 2th, 1th etc). This is the endless game
		if self.taking_mode == 0:
			pass
		# Shuffle
		elif self.taking_mode == 1:
			random.shuffle(self.dispute)
		# The senior card must be taken earlier
		elif self.taking_mode == 2:
			# The dispute always has 2n cards (n>=1), and differents cards are only last
			l,r = divmod(len(self.dispute), 2)
			self.debug_check_error(l<1 or r!=0, 'Wrong the dispute buffer (1)')
			cmp_res = self.compare(self.dispute[-2], self.dispute[-1])
			self.debug_check_error(cmp_res==0, 'Wrong the dispute buffer (2)')
			if cmp_res < 0:
				self.dispute[-2], self.dispute[-1] = self.dispute[-1], self.dispute[-2]

		self.thrower.check(self.dispute)
		winner.extend(self.dispute)
		self.dispute = []

	def check_user_request(self):
		k = getch(False)
		self.need_print_step = self.need_print_each_step
		if k == 's':
			self.need_print_step = True
		elif k == 'a':
			self.print_state()
		elif k == 'e':
			self.need_print_each_step = not self.need_print_each_step
		elif k == 'r':
			self.restart()
		elif k is not None:
			print '%s? "s a e r" are known options'%k


	def print_step(self, step_str):
		print '%d: %s;  %d %d'%(self.step, step_str, len(self.pack1), len(self.pack2))

	def print_state(self, pref='State of game'):
		print pref
		print '-'*len(pref)
		print 'Step: ', self.step
		print 'Player 1 (%d): '%len(self.pack1), self.pack1
		print 'Player 2 (%d): '%len(self.pack2), self.pack2
		print 'Dispute (%d): '%len(self.dispute), self.dispute
		if self.thrower.enabled:
			print 'Thrown (%d): '%len(self.thrower.pack), self.thrower.pack

	def debug_check_error(self, cond, msg):
		if cond:
			self.print_state('Script logic error: '+msg)
			raise RuntimeError(msg)

	def all_aces_to_player1(self):
		print 'All aces to player1!!!'
		#self.print_state('Before all_aces_to_player1()')
		for p2 in xrange(len(self.pack2)):
			if p2 >= len(self.pack2):	# list is being modified on the fly
				break
			if self.pack2[p2].str_value() == 'ace':
				len_pack1 = len(self.pack1)
				for p1 in xrange(len_pack1):
					if self.pack1[p1].str_value() != 'ace':
						break
				ace = self.pack2.pop(p2)
				if p1 != len_pack1:
					no_ace = self.pack1.pop(p1)
					self.pack2.insert(p2, no_ace)
				self.pack1.insert(p1, ace)
		#self.print_state('After all_aces_to_player1()')

		for i in xrange(len(self.pack2)):
			self.debug_check_error(self.pack2[i].str_value() == 'ace', 'all_aces_to_player1 doesnt work')
		#raw_input('press enter to continue')


if __name__ == '__main__':
	try:
		ToperGame().restart()
	except KeyboardInterrupt:
		print '\n\nCtrl-C was pressed. Exit.'
