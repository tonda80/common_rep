#!/usr/bin/python2
#coding=utf8

## TODO
# сделать внутренний просмотрщик
# Поддержка нескольких mask
# exclude mask
# если понадобится, кастомизировать finder, shower

import tkFileDialog
import threading
import Queue
import os
import re
import argparse

from uTkinter import *
from commonconfig import CommonConfig
import lsh

def _deb(*a):
	print '__deb ', a

WIN = lsh.f_win

class EntryPopupMenu(PopupMenu):
	def __init__(self, root_entry, labels):
		PopupMenu.__init__(self, root_entry)
		self.label_font = 'System 9'
		self.labels = labels			#! keeps list of labels
		for i in xrange(len(self.labels)-1, -1, -1):
			if self.labels.count(self.labels[i]) > 1:
				del self.labels[i]
		self.root_entry = root_entry
		self.callback = lambda l: root_entry.setVar(l)

		self.add_callback(labels, self.callback)
		self.__create_clear_command()

	def __create_clear_command(self):
		brd = '-'*5
		self.add_callback(brd+' Clear all '+brd, self.clear)

	def update_labels(self):
		label = self.root_entry.getVar()
		while label in self.labels:
			i = self.labels.index(label)
			self.labels.pop(i)
			self.delete(i)
		self.labels.insert(0, label)
		self.add_callback(label, self.callback, 0)

	def clear(self, *_):
		del self.labels[:]
		self.delete(0, 1000) # last index?  см py3 uTkinter
		self.__create_clear_command()


class App:
	re_format = re.compile(r'(.+):(\d+):')		# 'filename:linenumber:contentorsomething'	grep output format

	def __init__(self):
		self.parse_args()

		self.load_config()
		self.create_gui()
		self.create_binding()

		self.launcher = Launcher()
		self.get_from_launcher()

		self.root.set_delete_callback(self.delete_callback)

		self.finder = self.shower = self.cwd = None

	def parse_args(self):
		parser = argparse.ArgumentParser()

		parser.add_argument('-p', dest='args_where', default=None, help='Default search path')
		parser.add_argument('--reset_config', action='store_true', help='Clear config')
		parser.parse_args(namespace=self)


	def load_config(self):
		app_name = 'tkgrep_220517'
		default_config = {
			'where':lsh.pwd(),
			'flags':(0,0,0),
			'last_where':[],
			'last_what':[],
			'last_masks':[]
		}
		self.config = CommonConfig(app_name, **default_config)
		self.config.auto_asked('texteditor_path')
		if self.reset_config:
			self.config['texteditor_path'] = None
		_ = self.config['texteditor_path']	# костыль, запрос до создания gui, но с другой стороны логично, видно приглашение в момент запуска TODO поэкспериментировать что за проблема с raw_input и tk

		if self.args_where:
			self.config['where'] = os.path.realpath(self.args_where)

		last_where = self.config['last_where']
		for i in xrange(len(last_where)-1, -1, -1):
			path = os.path.realpath(last_where[i])
			if last_where.count(path) > 1:
				del last_where[i]
			elif last_where[i] != path:
				last_where[i] = path

	def save_config(self):
		self.config['where'] = self.search_dir.getVar()
		self.config['what'] = self.search_str.getVar()
		self.config['flags'] = (self.flag_wo.getVar(), self.flag_ic.getVar(), self.flag_re.getVar())
		self.config['masks'] = self.masks.getVar()
		self.config['excl_dir'] = self.excl_dir.getVar()

		self.config['last_where'] = self.config['last_where'][:20]
		self.config['last_what'] = self.config['last_what'][:20]
		self.config['last_masks'] = self.config['last_masks'][:20]

		self.config.save()

	def delete_callback(self):
		self.stop_search()

	def get_from_launcher(self):
		while 1:
			try:
				rcv = self.launcher.queue.get_nowait()
			except Queue.Empty:
				self.bg_interface_tasks()
				self.root.after(300, self.get_from_launcher)
				return

			self.search_result.append(rcv)

	def bg_interface_tasks(self):
		if self.launcher.work:
			self.startBtn.config(state=DISABLED)
			self.stopBtn.config(state=NORMAL)
		else:
			self.startBtn.config(state=NORMAL)
			self.stopBtn.config(state=DISABLED)

		self.root.statusBar.set_r('Total: '+str(self.search_result.size()))

	def create_binding(self):
		def button1_handler(ev):
			for p in self.story_popups:
				p.unpost()
		self.root.bind('<Button-1>', button1_handler)

		self.root.bind('<Return>', lambda e: self.start_search())
		#self.root.bind('<Escape>', lambda e: self.stop_search())
		self.root.bind('<Escape>', lambda e: self.root.close())

	def create_gui(self):
		self.root = uTk(u'tkgrep', 640, 480)

		frame_1 = uFrame(self.root, gmArgs = {'side':LEFT, 'expand':YES, 'fill':BOTH})
		#frame_preview = uFrame(self.root, gmArgs = {'side':RIGHT, 'expand':YES, 'fill':BOTH})

		frame_setup = uFrame(frame_1, gmArgs = {'side':TOP, 'expand':NO, 'fill':X})
		frame_result = uFrame(frame_1, gmArgs = {'side':TOP, 'expand':YES, 'fill':BOTH})

		lw = 7; lw2 = 9
		new_internal_frame = lambda: uFrame(frame_setup, relief=FLAT, gmArgs = {'side':TOP, 'expand':NO, 'fill':X, 'padx':3, 'pady':2})
		f = new_internal_frame()	# -----------
		uLabel(f, 'Where', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.search_dir = uEntry(f, self.config['where'], gmArgs = {'side':LEFT, 'expand':YES, 'fill':X})
		uButton(f, self.select_dir, 'Select', gmArgs = {'side':RIGHT})
		f = new_internal_frame()	# -----------
		uLabel(f, 'What', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.search_str = uEntry(f, self.config['what'], gmArgs = {'side':LEFT, 'expand':YES, 'fill':X})
		f = new_internal_frame()	# -----------
		uLabel(f, '', width=lw, gmArgs = {'side':LEFT, 'anchor':W})
		self.flag_wo = uCheckbutton(f, text = 'Word only', defaultValue = self.config['flags'][0], gmArgs = {'side':LEFT, 'anchor':W, 'padx':10})
		self.flag_ic = uCheckbutton(f, text = 'Ignore case', defaultValue = self.config['flags'][1], gmArgs = {'side':LEFT, 'anchor':W, 'padx':10})
		self.flag_re = uCheckbutton(f, text = 'Regexp', defaultValue = self.config['flags'][2], gmArgs = {'side':LEFT, 'anchor':W, 'padx':10})
		f = new_internal_frame()	# -----------
		uLabel(f, 'Masks', width=lw2, gmArgs = {'side':LEFT, 'anchor':W})
		self.masks = uEntry(f, self.config['masks'], gmArgs = {'side':LEFT, 'expand':YES, 'fill':X})
		f = new_internal_frame()	# -----------
		uLabel(f, 'Exclude dir', width=lw2, gmArgs = {'side':LEFT, 'anchor':W})
		self.excl_dir = uEntry(f, self.config['excl_dir'], gmArgs = {'side':LEFT, 'expand':YES, 'fill':X})

		self.story_popups = (	EntryPopupMenu(self.search_dir, self.config['last_where']),
								EntryPopupMenu(self.search_str, self.config['last_what']),
								EntryPopupMenu(self.masks, self.config['last_masks'])
                              )

		f = new_internal_frame()	# -----------
		self.startBtn = uButton(f, self.start_search, 'Start', gmArgs = {'side':LEFT, 'padx':70})
		self.stopBtn = uButton(f, self.stop_search, 'Stop',  gmArgs = {'side':LEFT})
		def clear_story():
			for p in self.story_popups:
				try:
					p.clear()
				except TclError:
					pass
		uButton(f, clear_story, 'Clear story',  gmArgs = {'side':RIGHT})
		uButton(f, self.search_result_to_clipboard, 'Copy result',  gmArgs = {'side':RIGHT})

		self.search_result = uListbox(frame_result, 'xy', font=('Helvetica', 11), gmArgs = {'side':TOP, 'expand':YES, 'fill':BOTH})
		self.search_result.addMouseCallback(lambda *a:self.search_click_line(True, *a), '<Control-Button-1>')
		self.search_result.addMouseCallback(lambda *a:self.search_click_line(False, *a), '<Button-1>')

		#self.preview = uText(frame_preview, 'xy', state='disabled', gmArgs = {'side':TOP, 'expand':YES, 'fill':BOTH})

	def select_dir(self):
		d = tkFileDialog.askdirectory(mustexist=True, initialdir=self.search_dir.getVar())
		if d:
			self.search_dir.setVar(d)

	def search_result_to_clipboard(self):
		sr_list = self.search_result.get(0, END)
		self.root.clipboard_clear()
		self.root.clipboard_append(''.join(sr_list))

	# TODO to custom search here
	def set_finder(self):
		self.finder = Grep(self.root.statusBar, self.search_str.getVar(), self.masks.getVar(),
							self.flag_wo.getVar(), self.flag_ic.getVar(), self.flag_re.getVar(), self.excl_dir.getVar())

	def set_shower(self):
		self.shower = TextEditor1(self.config['texteditor_path'])

	def start_search(self):
		for p in self.story_popups:
			p.update_labels()

		self.search_result.clear()
		self.set_finder()
		self.cwd = self.search_dir.getVar()
		if not os.path.isdir(self.cwd):
			self.root.statusBar.set('Search directory is not exist!')
			return
		self.launcher.start(self.finder, self.cwd)

		self.save_config()

	def stop_search(self):
		self.launcher.stop()

	def search_click_line(self, ctrl_pressed, n, line):
		mo = self.re_format.match(line)
		if mo is None:
			print 'Error line:', line
			return
		full_path = os.path.join(self.cwd, mo.group(1))
		line = mo.group(2)
		if True or ctrl_pressed:
			self.set_shower()
			self.shower.show(full_path, line)
		else:
			print '__deb', full_path, line

class Launcher:
	def __init__(self):
		self.work = False
		self.queue = Queue.Queue()

	def start(self, finder, cwd):
		if self.work:
			return
		self.finder = finder
		self.process = lsh.TrackedProcess(finder.cmd_args, cwd=cwd)
		self.thread = threading.Thread(target = self.process.watch, args=(self.line_handler,))
		self.thread.start()
		self.work = True

	def clear_queue(self):
		while 1:
			try:
				self.queue.get_nowait()
			except Queue.Empty:
				break

	def stop(self, external = True):
		#print '__deb Launcher.stop ',external, self.work
		if not self.work:
			return
		if external:
			self.process.stop()
			self.thread.join()
			self.clear_queue()
		self.work = False

	def line_handler(self, line):
		if line == '':
			self.stop(False)
			return
		fo = self.finder.line_handler(line)
		if fo:
			self.queue.put_nowait(fo)

class IFinder:
	cmd_args = None				# command for running
	def line_handler(self, line):		# returns line for addition	(App.re_format)
		raise NotImplementedError

class Grep(IFinder):
	def __init__(self, statusBar, what, masks, wo, ic, re_, excl_dir):
		self.cmd_args = ['grep', '-rnI']	# ignore binary files

		esc_chars = '\'"'
		if not re_:
			esc_chars = '\\.^[' + esc_chars		# \ must be first!!
			# regexp special symbols. эксперимент показал что они должны быть экранированы

		for s in esc_chars:
			if s in what:
				what = what.replace(s, '\\'+s)

		if wo:	#word only
			if re_:
				print 'Ignored the option "Word only". It is inconstistent with "Regexp"'
			else:
				self.cmd_args.append('-w')

		if ic:			# ignore case
			self.cmd_args.append('-i')
		if re_:			# regexp
			self.cmd_args.append('-E')
		if excl_dir:	# --exclude-dir=PATTERN  directories that match PATTERN will be skipped. Паттерн - глоб для директории в любом месте иерархии поиска
			self.cmd_args.append('--exclude-dir=%s'%excl_dir)

		masks = masks.strip().split()
		q = len(masks)
		if q > 1:
			statusBar.set('Support of some masks in not implemented yet')
		if q > 0:
			self.cmd_args.append('--include='+masks[0])

		self.cmd_args.append(what)

		if WIN:	# на новом вин это уже не надо (?)
			self.cmd_args.append('.')

		statusBar.set(' '.join(self.cmd_args))

		# subprocess ломается на русских буквах, перекодируеv явно
		# может правильнее включать utf8 в консоли?
		if WIN:
			for i, a in enumerate(self.cmd_args):
				if isinstance(a, unicode):
					self.cmd_args[i] = self.cmd_args[i].encode('cp1251')


	def line_handler(self, line):
		return line

class IShower:
	def show(self, file_name, line_num):
		raise NotImplementedError

class TextEditor1(IShower):
	def __init__(self, exec_path):
		self.exec_path = exec_path
	def show(self, file_name, line_num):
		lsh.run_bg((self.exec_path, '%s:%s'%(file_name, line_num)))


App().root.mainloop()
