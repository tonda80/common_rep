import hashlib
import tkFileDialog
import Tkinter

fname = raw_input('Enter a full file name (an empty string for interactive select): ')
if not fname:
    w = Tkinter.Tk(); w.withdraw()
    fname = tkFileDialog.askopenfilename()

md5_hex = None
with open(fname, "rb") as f:
    print 'Wait..\n'
    hash = hashlib.md5()
    for chunk in iter(lambda: f.read(2**15), b""):
        hash.update(chunk)
    print 'File:', fname
    md5_hex = hash.hexdigest()
    print 'Md5 :', md5_hex

check_str = raw_input('\nCheck the result? Enter an expected result or just press enter to exit: ').lower()
if check_str:
    if check_str == md5_hex:
        print 'Ok'
    else:
        print '%s\n!=\n%s'%(md5_hex, check_str)

    raw_input('\nPress enter to exit')
