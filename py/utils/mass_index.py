# coding=utf8
import sys
from Tkinter import *

sys.path.append(r'..\modules')
from graph import Graph

root = Tk()

fr_gr=Frame(); fr_gr.pack(fill=BOTH, expand=YES)
gr = Graph(fr_gr)
gr.pack(fill=BOTH, expand=YES)

# собственно графики
# -------------------------------------------------------

def create_func(i):
	return lambda h: h*h*i

zone_names = (u'дефицит', u'недостаток', u'норма', u'избыточная', u'ожирение 1', u'ожирение 2', u'ожирение 3')
indexes = (16, 18.5, 25, 30, 35, 40)
colors = ('purple', 'blue', 'green', 'gray', 'red', 'black')

MIN_M = 40
MAX_M = 140
D_M = 10		# дельта массы (для осей)
MIN_H = 1.4
MAX_H = 2.1
D_H = 0.1	# дельта роста (для осей)

# графики
for i,c in map(None, indexes,colors):
	func = create_func(i)
	gr.draw(func, MIN_H, MAX_H-MIN_H, 1000, c)

# подписи
label_x = MIN_H + (MAX_H - MIN_H)/2
D_I = 3	# для крайних меток
mass = lambda i: label_x*label_x*i

gr.add_text(label_x, mass(indexes[0]-D_I), zone_names[0])
for j in xrange(len(indexes)-1):
	i0 = indexes[j]; i1 = indexes[j+1]
	av_i = i0 + (i1 - i0)/2
	gr.add_text(label_x, mass(av_i), zone_names[j+1])
gr.add_text(label_x, mass(indexes[-1]+D_I), zone_names[-1])

# оси
for m in xrange(MIN_M, MAX_M+D_M, D_M): gr.create_axe_x(m)
def heights():
	h = MIN_H
	while h <= MAX_H:
		yield h
		h += D_H
for h in heights(): gr.create_axe_y(h)

# -------------------------------------------------------
gr.initial_scaling(False)

def showLimits():	# show limits for the given height
	try:
		h = float(vHeight.get())
	except:
		vMasses.set('Enter the height in meters')
		return
	res = ''
	delim = '    '
	for i in indexes:
		res += str(i*h**2) + delim
	vMasses.set(res[:-len(delim)])

	gr.clear('HEIGHT_AXE')
	gr.draw_par(lambda x: h, lambda y: MAX_M if y>MIN_H else MIN_M, MIN_H, MAX_H-MIN_H, 1, 'gray', 'HEIGHT_AXE')

vHeight = StringVar()
vMasses = StringVar()
Entry(textvariable=vHeight).pack()
Button(command=showLimits, text='Calculate for height').pack()
Label(textvariable=vMasses).pack()

root.mainloop()
