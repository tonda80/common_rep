﻿'''
Связывание соседей в дереве
          1-> Nil
         / \
        2-> 3-> Nil
       /   / \
     4 -> 6-> 7-> Nil
etc   \    \  /
       8    9 A
https://habrahabr.ru/post/276673/#comment_8764489

Про деревья вообще
https://habrahabr.ru/post/66926/
'''

class Knot:
	def __init__(self, data, left = None, right = None):
		self.data = data
		self.left = left
		self.right = right
		self.neighbour = None

class Tree:
	# class RecursIterator:
		# def __init__(self, tree):
			# self.curr_knot = tree.root
		# def next(self):

	def __init__(self, root):
		self.root = root

def recursive_walk_tree(init_knot, action):
	action(init_knot)
	for knot in (init_knot.left, init_knot.right):
		if knot is not None:
			recursive_walk_tree(knot, action)

# see the main description
tree = Knot(1, Knot(2, Knot(4, None, Knot(8))), Knot(3, Knot(6, None, Knot(9)), Knot(7, Knot(10))))

def act1(knot):
	print knot.data
recursive_walk_tree(tree, act1)

