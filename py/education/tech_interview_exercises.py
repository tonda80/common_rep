#coding=utf8

# решаем тут разные задачки для тех собеседований


import sys
from inspect import isclass
import random
import itertools

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('target', help=u'Class name of exercise (or help)')

	def main(self):
		_module = sys.modules[__name__]
		def is_exercise(c): return isclass(c) and issubclass(c, BaseExercise) and c is not BaseExercise

		if self.args.target == 'help':
			print 'Possible target values: '+', '.join(k for k,v in _module.__dict__.iteritems() if is_exercise(v))
			return
		ExClass = getattr(_module, self.args.target, None)
		if not is_exercise(ExClass):
			self.log.error('Unknown exercise: {} ({})'.format(self.args.target, ExClass))
			return
		ExClass(self).run()


class BaseExercise:
	def __init__(self, app):
		self.app = app


# Есть однонаправленный список из структур. В нём random указывает на какой-то еще элемент этого же списка.
# Требуется написать функцию, которая копирует этот список с сохранением структуры
# (т.е. если в старом списке random первой ноды указывал на 4-ю, в новом списке должно быть то же самое – рандом первой ноды указывает на 4-ю ноду нового списка).
# O(n), константная дополнительная память + память под элементы нового списка.
# https://tproger.ru/articles/problems/
class ListCopy(BaseExercise):
	class E:
		def __init__(self, data):
			self.next_ = self.random = None
			self.data = data		# дата - счетчик для проверки
		def __str__(self):
			return '[{}({}) {}]'.format(self.data, self.next_.data if self.next_ else ' ', self.random.data if self.random else ' ')
		def __iter__(self):
			self.ptr = self
			return self
		def next(self):
			if self.ptr is None:
				raise StopIteration
			ret = self.ptr
			self.ptr = self.ptr.next_
			return ret

	def generate_list(self):
		cnt = 0
		head = self.E(cnt)
		ll = [head]
		curr = head
		for i in xrange(random.randrange(10, 14)):
			cnt += 1
			curr.next_ = self.E(cnt)
			ll.append(curr.next_)
			curr = curr.next_
		for e in ll:
			e.random = random.choice(ll)
		return head

	def list_to_str(self, lst):
		s = ''
		for e in lst:
			s += '{} --> '.format(e)
		s += '[]'
		return s

	def copy_list(self, src):
		for e in src:
			dup = self.E(e.data)
			dup.next_ = e.random			# добавляем дупликат в разрыв оригинал-рандом_оригинала
			e.random = dup
		dst = src.random
		for e in src:
			dup = e.random
			dup.random = dup.next_.random	# теперь рандом дупликата указывает на правильный дупликат
		for e in src:
			dup = e.random
			e.random = dup.next_			# восстановили оригинальный рандом
			dup.next_ = e.next_.random if e.next_ else None
											# теперь next дупликата указывает на правильный дупликат
		return dst

	def compare_lists(self, l1, l2):
		for e1, e2 in itertools.izip(l1, l2):
			if (e1 and e2) is None:
				raise RuntimeError('lists are NOT equal (1)')
			if e1.data != e2.data or e1.random.data != e2.random.data or (e1.next_ and e1.next_.data != e2.next_.data):
				raise RuntimeError('lists are NOT equal (2): {} {}'.format(e1, e2))
			if e1 is e2:
				raise RuntimeError('wrong impl')
		self.app.log.info('compare_lists OK')

	def run(self):
		src = self.generate_list()
		self.app.log.info('src:\n'+ self.list_to_str(src))
		dst = self.copy_list(src)
		self.app.log.info('dst:\n'+ self.list_to_str(dst))
		self.compare_lists(src, dst)


if __name__ == '__main__':
	App().main()
