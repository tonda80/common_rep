#coding=utf8

# Школьная задачка от Князевых
# обход поля с клетками из нижнего левого угла по спирали
# разумеется, решение в аналитической форме правильнее :)
# N - высота (количество строк), M - ширина (количество столбцов)
# N <= M, N - четное		x = N/2 ; y = N/2 + 1
# N <= M, N - нечетное		x = M - (y-1) = M - (N-1)/2 ; y = (N+1)/2
# N > M, M - четное			x = M/2 ; y = M/2 + 1
# N > M, M - нечетное		x = (M+1)/2 ; y = N - (x-1) = N - (M-1)/2

from itertools import cycle

class SnakeField:
	def __init__(self, w_size, h_size, pdeb = True):
		self.x = 1
		self.y = 1
		self.w_max = w_size
		self.w_min = 1
		self.h_max = h_size
		self.h_min = 1

		self.pdeb = pdeb

		self.actions = cycle((self.right, self.up, self.left, self.down))
		self.next_action()

	def next_action(self):
		self.action = self.actions.next()
		if self.pdeb:
			print 'Turn'
		return True

	def print_current(self, force = False):
		if self.pdeb or force:
			print 'x %4d, y %4d'%(self.x, self.y)

	def right(self):
		if self.x == self.w_max:
			self.h_min += 1
			self.next_action()
			return True
		self.x += 1

	def up(self):
		if self.y == self.h_max:
			self.w_max -= 1
			return self.next_action()
		self.y += 1

	def left(self):
		if self.x == self.w_min:
			self.h_max -= 1
			return self.next_action()
		self.x -= 1

	def down(self):
		if self.y == self.h_min:
			self.w_min += 1
			return self.next_action()
		self.y -= 1

	def run(self):
		self.print_current()
		last_res = False

		while 1:
			res = self.action()
			if not res:
				self.print_current()

			if res and last_res:	# 2 turns in a row is sign of end
				self.print_current(True)
				break

			last_res = res


# N <= M, N - четное		x = N/2 ; y = N/2 + 1
SnakeField(11, 8, False).run()
SnakeField(100, 12, False).run()
SnakeField(50, 50, False).run()
print

# N <= M, N - нечетное		x = M - (y-1) = M - (N-1)/2 ; y = (N+1)/2
SnakeField(111, 99, False).run()
SnakeField(10, 9, False).run()
SnakeField(33, 33, False).run()
print

# N > M, M - четное			x = M/2 ; y = M/2 + 1
SnakeField(10, 80, False).run()
SnakeField(40, 41, False).run()
print

# N > M, M - нечетное		x = (M+1)/2 ; y = N - (x-1) = N - (M-1)/2
SnakeField(29, 31, False).run()
SnakeField(13, 256, False).run()
print

SnakeField(5, 4, False).run()
SnakeField(5000, 2015, False).run()