import random


def random_test():
	N = int(1e4)
	D = 100

	g_ok = True
	for x in xrange(D):
		cnt = 0
		for i in xrange(N):
			if random.randrange(D) == x:
				cnt += 1
		v = float(cnt)/N
		e = abs(v - 1.0/D)
		ok = e < 0.002
		g_ok &= ok
		print '%d:%d    %f %f'%(x, ok, v, e)
	print g_ok

class MontyHall:
	def __init__(self, box_n, test_n=30000):
		assert(box_n >= 3)

		self.box_n = box_n
		self.test_n = test_n
		self.__reinit_boxes()

	def __reinit_boxes(self):
		# True - closed win, False - closed fake, 0 - opened fake
		self.boxes = [False for i in xrange(self.box_n)]
		self.boxes[random.randrange(self.box_n)] = True

	def __test(self, strategy, *args):
		cnt = 0
		for i in xrange(self.test_n):
			self.__reinit_boxes()
			choice = strategy(*args)
			success = self.boxes[choice]
			if success:
				cnt += 1
		return float(cnt)/self.test_n

	def test_no_change_strategy(self):
		return self.__test(self.__no_change_strategy)

	def test_change_strategy(self, open_box_n=None):
		if open_box_n is None:
			open_box_n = self.box_n - 2
		assert(open_box_n > 0)

		return self.__test(self.__change_strategy, open_box_n)

	def __no_change_strategy(self):
		return random.randrange(self.box_n)

	def __change_strategy(self, open_box_n):
		choice1 = random.randrange(self.box_n)

		self.__box_opening(open_box_n, choice1)
		choice2 = self.__change_choice(choice1)
		#print choice1, choice2, self.boxes
		return choice2

	def __box_opening(self, open_box_n, choice1):
		vars = [i for i,v in enumerate(self.boxes) if v is False and i != choice1 ]
		for i in xrange(open_box_n):
			var = random.choice(vars)
			vars.remove(var)
			self.boxes[var] = 0

	def __change_choice(self, choice1):
		vars = [i for i,v in enumerate(self.boxes) if v is not 0 and i != choice1 ]
		var = random.choice(vars)
		return var



#random_test()

print MontyHall(3).test_no_change_strategy()
print MontyHall(3).test_change_strategy()

