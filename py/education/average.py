#coding=utf8

# Простое скользящее среднее
# https://ru.wikipedia.org/wiki/%D0%A1%D0%BA%D0%BE%D0%BB%D1%8C%D0%B7%D1%8F%D1%89%D0%B0%D1%8F_%D1%81%D1%80%D0%B5%D0%B4%D0%BD%D1%8F%D1%8F

import random

class NativeAverage:
	def __init__(self):
		self.average = None
		self.n = self.sum = 0

	def add(self, value):
		self.n += 1
		self.sum += float(value)
		self.average = self.sum/self.n

class Sma:
	def __init__(self, period = 1):
		self.average = self.n = 0.0
	def add(self, value):
		self.n += 1
		self.average = self.average + (value - self.average)/self.n

# http://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

if __name__ == '__main__':
	av1 = NativeAverage()
	av2 = Sma()
	while 1:
		value = random.randrange(1000)
		av1.add(value)
		av2.add(value)
		if not isclose(av1.average, av2.average):
			print value, av1.average, av2.average
			raise RuntimeError
		print value, av1.average

