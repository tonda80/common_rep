#coding=utf8

def g2b(g):
	ret = 0
	while g:
		ret ^= g
		g >>= 1
	return ret

def b2g(b):
	return b ^ (b >> 1)

if __name__ == '__main__':
	for i in xrange(1000):
		# output
		if i < 30:
			print '%d \t%02x'%(i, b2g(i))

		# test
		if i != g2b(b2g(i)) or i != b2g(g2b(i)):
			print 'Error for %d!'%i
