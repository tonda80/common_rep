import random

def randomList(minSize = 10, maxSize = 30, minRange = 0, maxRange = 100):
    if maxSize < minSize: maxSize = minSize
    if maxRange < minRange: maxRange = minRange
    listSize = random.randint(minSize, maxSize)
    return [random.randint(minRange, maxRange) for i in xrange(listSize)]

def debugList(size = 20):
    return range(size, 0, -1)
    
def checkSorted(ll):
    lLen = len(ll)
    for i in xrange(lLen-1):
        if ll[i] > ll[i+1]:
            print 'checkSorted error! %d, %d, %d'%(i, ll[i], ll[i+1])
            print ll
            raise AssertionError

# ----------------------------------------------            

def bublSort(ll):
    lSize = len(ll)
    for i in xrange(lSize - 1):
        for j in xrange(lSize - 1):
            if ll[j] > ll[j+1]:
                ll[j+1], ll[j] = ll[j], ll[j+1]
                
def mergeSort(ll):
    def merge(ll1, ll2):
        ret = []
        while 1:
            if not ll1: ret.extend(ll2); return ret
            if not ll2: ret.extend(ll1); return ret
            if ll1[0] < ll2[0]:
                ret.append(ll1.pop(0))
            else:
                ret.append(ll2.pop(0))
            
    sortedLists = [[i] for i in ll]
    while len(sortedLists) > 1:
        if len(sortedLists)%2 != 0: sortedLists.append([])
        newSortedList = []
        for i in xrange(len(sortedLists)/2):
            rr = merge(sortedLists[2*i], sortedLists[2*i+1])
            newSortedList.append(rr)
        sortedLists = newSortedList
    print '__debug', len(ll), len(sortedLists[0])
    return sortedLists[0]
    
def shellSort(ll):
    lLen = len(ll)
    gap = lLen/2
    cnt = 0
    while gap > 0:
        for i in xrange(gap, lLen):
            for j in xrange(i-gap, -1, -gap):
                cnt += 1
                print '__debug %3d %3d %3d %3d '%(gap, i, j, j+gap), ll, 
                if ll[j] > ll[j+gap]:
                    print ' swap'
                    ll[j+gap], ll[j] = ll[j], ll[j+gap]
                else:
                    print ' break'
                    break
        gap /= 2
    print '%d steps'%cnt
        
if __name__ == '__main__':
    # print 'Bubl sort'
    # ll = randomList()
    # print ll
    # bublSort(ll)
    # print ll

    # print 

    # print 'Merge sort'
    # ll = randomList()
    # print ll
    # print mergeSort(ll)

    print 'Shell sort'
    ll = randomList(10, 10)      # randomList
    print ll
    shellSort(ll)
    checkSorted(ll)
    print ll