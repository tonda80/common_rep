# coding=utf8

# вообще для перестановок см itertools
## product - прямое произведение, все возможные наборы из элементов последовательностей
## permutations - размещения\перестановки, упорядоченные наборы из n элементов по k; A = n!/(n-k)!
## combinations - сочетания, неупорядоченные наборы из n элементов по k; C = n!/k!/(n-k)!
## combinations_with_replacement - сочетания с повторениями, те каждый элемент может повторяться

from sys import argv, exit
import operator

# default
debug_print = lambda *args: None

# ------------------------------

def narayana_next_p(seq):
# narayana algo
# https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%9D%D0%B0%D1%80%D0%B0%D0%B9%D0%B0%D0%BD%D1%8B
# http://prog-cpp.ru/placement/

	j = len(seq) - 2
	while seq[j] >= seq[j+1]:
		j -= 1
	if j == -1:
		return None

	l = len(seq) - 1
	while seq[j] >= seq[l]:
		l -= 1

	seq[j], seq[l] = seq[l], seq[j]

	j += 1
	i = len(seq) - 1
	while j < i:
		seq[j], seq[i] = seq[i], seq[j]
		j += 1; i -= 1

	return seq

def narayana_demo(N):
	cnt = 1
	seq = range(N)
	while narayana_next_p(seq):
		print seq
		cnt += 1
		#raw_input()
	print 'Total', cnt

# ------------------------------

def permutations_with_rep(n, length):
	# permutations with repetitions
	# https://ru.wikibooks.org Реализации алгоритмов/Комбинаторика/Размещения
	permutations = n**length
	output = [[0]*length for i in xrange(permutations)]

	for i in xrange(length):
		p1 = 0
		while p1 < permutations:
			for al in xrange(n):
				for p2 in xrange(n**i):
					output[p1][i] = al
					#print p1,i,al
					p1 += 1
	return output


def permutations_with_rep_demo(N):
	for e in permutations_with_rep(N, N):
		print e

# ------------------------------
# Липский, Комбинаторика для программистов, Генерирование перестановок, Алгоритм 1.10 стр 21

def reverse_l1(lst, j):
	i = 0
	while i < j:
		lst[i], lst[j] = lst[j], lst[i]
		i += 1; j -= 1

def antylex_l1(m, lst, clb):
	debug_print('__deb1\t', m, '\t', lst)

	if m == 0:
		clb(lst)	# new permutation here
	else:
		for i in xrange(m+1):
			antylex_l1(m-1, lst, clb)

			debug_print('__deb2\t', m, i)

			if i < m:
				lst[i], lst[m] = lst[m], lst[i]
				reverse_l1(lst, m-1)

				debug_print('__deb3 change\t', lst)

def permutation_lipsky1_demo(N):
	clb = PermGetter()

	lst = range(N)  #[l for l in u'АБВГ']
	antylex_l1(len(lst)-1, lst, clb)

	print 'Checksum', clb.check_result()

# ------------------------------
# Липский, Комбинаторика для программистов, Генерирование перестановок, Алгоритм 1.11 стр 23

def B_l2(m, i):
	if m%2 == 1 and m > 1:
		if i < m-1:
			return i
		else:
			return m-2
	else:
		return m-1

def perm_l2(m, lst, clb):
	debug_print('__deb1\t', m, '\t', lst)

	if m == 0:
		clb(lst)	# new permutation here
	else:
		for i in xrange(m+1):
			perm_l2(m-1, lst, clb)

			debug_print('__deb2\t', m, i)

			if i < m:
				b = B_l2(m, i)
				lst[b], lst[m] = lst[m], lst[b]

				debug_print('__deb3 change\t', lst)

def permutation_lipsky2_demo(N):
	clb = PermGetter()

	lst = range(N)  #[l for l in u'АБВГ']
	perm_l2(len(lst)-1, lst, clb)

	print 'Checksum', clb.check_result()

# ------------------------------
# Липский, Комбинаторика для программистов, Генерирование перестановок, Алгоритм 1.12 стр 27

def perm_l3(m, lst, clb):
	n = len(lst)
	C = [1 for i in xrange(n)]; C[n-1] = 0
	PR = [True for i in xrange(n)]

	clb(lst)

	i = 1
	while i < n:
		i = 1; x = 0
		while C[i-1] == n-i+1:
			PR[i-1] = not PR[i-1]; C[i-1] = 1
			if PR[i-1]:
				x += 1
			i += 1

		if i < n:
			if PR[i-1]:
				k = C[i-1] + x
			else:
				k = n - i + 1 - C[i-1] + x
			lst[k-1], lst[k] = lst[k], lst[k-1]
			clb(lst)
			C[i-1] += 1

def permutation_lipsky3_demo(N):
	clb = PermGetter()

	lst = range(N)
	perm_l3(len(lst)-1, lst, clb)

	print 'Checksum', clb.check_result()

# ------------------------------

def print_help():
	print '''
Using "python permuttations.py T N [D]

where T:
1	demo of Narayana algo, narayana_next_p()
2	demo of permutation into dedicated array, permutations_with_rep
3	Lipsky 1.10 algo
4	Lipsky 1.11 algo
5	Lipsky 1.12 algo

where N: number of elements

where D: debug demo output if second argument exists

'''

demos = {
	'1': narayana_demo,
	'2': permutations_with_rep_demo,
	'3': permutation_lipsky1_demo,
	'4': permutation_lipsky2_demo,
	'5': permutation_lipsky3_demo,

}

class PermChecker:
	def __init__(self):
		self.sum_list = None
	def add_check(self, checked_list):
		if self.sum_list is None:
			self.sum_list = checked_list[:]
		else:
			self.sum_list = map(operator.add, self.sum_list, checked_list)

class PermGetter:
	def __init__(self):
		self.cnt = 0
		self.checker = PermChecker()

	def __call__(self, perm):
		self.cnt += 1
		self.checker.add_check(perm)
		print self.cnt, '\t', perm		#if cnt_p%1000000 == 0:

	def check_result(self):
		return self.checker.sum_list


if __name__ == '__main__':
	try:
		demo = demos[argv[1]]
	except (IndexError, KeyError):
		print_help()
		exit()

	try:
		N = int(argv[2])
	except (IndexError, ValueError):
		N = 4
		print 'Defaul value 4 will be used'

	if len(argv) > 3:
		def debug_print(*args):
			for a in args:
				print a,
			print

	demo(N)
