#!python

import subprocess

class doException(Exception):
    def __init__(self, cmd, reason):
		self.cmd = cmd
		self.reason = reason
		msg = '`%s` cannot be executed. %s'%(cmd, reason)
		if isinstance(reason, DoResult) and isinstance(reason.returncode, int):
			msg = '`%s` returned %d'%(cmd, reason.returncode)
		Exception.__init__(self, msg)
		
class DoResult:
	def __init__(self):
		self.stdout = self.stderr = self.returncode = None
	def __str__(self):
		#return 'DoResult instance\nstdout\n<<<\n%s\n>>>\nstderr\n<<<\n%s\n>>>\nreturncode %s'%(self.stdout, self.stderr, self.returncode)
		return 'DoResult instance <\n%s\n%s\n%s>'%(self.returncode, self.stdout, self.stderr)		

def do(cmd, shell = False, raiseException = True):
	res = DoResult()
	try:
		pr = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = shell)
	except OSError as e:
		if raiseException:
			raise doException(cmd, type(e))
		return res
	(res.stdout, res.stderr) =  pr.communicate()
	res.returncode = pr.returncode
	if raiseException and res.returncode != 0:
		raise doException(cmd, res.returncode)
	return res
	
if __name__ == '__main__':
	def win_pwd():
		print do(('echo', '%cd%'), shell=True).stdout,
	def cd(path):
		do(('cd', path), shell=True)
	
	win_pwd()
	cd('..')
	win_pwd()