#coding=utf8

import ctypes
import threading
import time

#
user32dll = ctypes.windll.user32
kernel32dll = ctypes.windll.kernel32

# ------------ win types
class POINT(ctypes.Structure):
	_fields_ = [
		('x', ctypes.c_long),
		('y', ctypes.c_long)
	]
class MSLLHOOKSTRUCT(ctypes.Structure):
	_fields_ = [
		('pt', POINT),
		('mouseData', ctypes.c_int),
		('flags', ctypes.c_int),
		('time', ctypes.c_int),
		('dwExtraInfo', ctypes.POINTER(ctypes.c_ulong))
		]
PMSLLHOOKSTRUCT = ctypes.POINTER(MSLLHOOKSTRUCT)

class KBDLLHOOKSTRUCT(ctypes.Structure):
	_fields_ = [
		('vkCode', ctypes.c_int),
		('scanCode', ctypes.c_int),
		('flags', ctypes.c_int),
		('time', ctypes.c_int),
		('dwExtraInfo', ctypes.POINTER(ctypes.c_ulong))
		]
PKBDLLHOOKSTRUCT = ctypes.POINTER(KBDLLHOOKSTRUCT)

HOOK_CLB_FUNC = ctypes.WINFUNCTYPE(ctypes.c_longlong, ctypes.c_int, ctypes.c_long, ctypes.c_long)

# ------------ win types end

# ------------ common part

def common_hook_function(obj):
	def py_clb_func(nCode, wParam, lParam):
		obj.hook_event(nCode, wParam, lParam)
		return obj.user32dll.CallNextHookEx(ctypes.c_void_p(), nCode, wParam, lParam)

	clb_func = HOOK_CLB_FUNC(py_clb_func)
	obj.hook = obj.user32dll.SetWindowsHookExW(obj.hook_type, clb_func, None, 0)
	if not obj.hook:
		raise WinHookError('SetWindowsHookEx error %d'%(obj.kernel32dll.GetLastError(), ))

	Tarr = ctypes.c_int * 100	# type for dummy
	dummy = Tarr()
	obj.user32dll.GetMessageW(ctypes.pointer(dummy), None, 0, 0)

	obj.user32dll.UnhookWindowsHookEx(obj.hook)

class WinHookError(RuntimeError):
	pass

# ------------ common part end

# ------------ mouse hook

class MouseHook:
	EventTypes = {	0x200:'MOUSEMOVE', 0x20a:'MOUSEWHEEL',  0x20e:'MOUSEHWHEEL',
					0x201:'LBUTTONDOWN', 0x202:'LBUTTONUP', 0x204:'RBUTTONDOWN',  0x205:'RBUTTONUP',
					0x207:'MBUTTONDOWN', 0x208:'MBUTTONUP'}

	user32dll = ctypes.windll.user32	# the module object can be destroyed early the class object
	kernel32dll = ctypes.windll.kernel32

	def __init__(self, clb, events = None, clb_args=()):

		self.hook_type = 14
		self.callback = clb
		self.callback_args = clb_args
		if events is None:
			self.events = MouseHook.EventTypes.values()
		elif isinstance(events, list) or isinstance(events, tuple):
			self.events = events
		else:
			raise WinHookError('Wrong the event list')

		self.hook = None
		self.hook_thread = threading.Thread(target=common_hook_function, args=(self, ))
		self.hook_thread.daemon = True
		self.hook_thread.start()

	def __del__(self): self.stop()

	def stop(self):
		self.__class__.user32dll.PostThreadMessageW(self.hook_thread.ident, 0x0012, 0, 0)	# WM_QUIT, but generally any message stops thread
		self.hook_thread.join()

	def hook_event(self, nCode, wParam, lParam):
		if nCode != 0:
			return
		evType = MouseHook.EventTypes[wParam]
		if evType not in self.events:
			return

		lParam =  ctypes.cast(lParam, PMSLLHOOKSTRUCT)
		ev = MouseEvent(evType, lParam.contents.pt.x, lParam.contents.pt.y)
		if wParam == 0x20a:	# wheel event
			ev.wheel_delta = lParam.contents.mouseData >> 16

		self.callback(ev, *self.callback_args)

class MouseEvent:
	def __init__(self, type, x, y):
		self.type = type
		self.x = x
		self.y = y
		self.wheel_delta = None		# forward direction of the wheel
	def __str__(self):
		return 'MouseEvent %s (%d %d) %s'%(self.type, self.x, self.y, self.wheel_delta)

# ------------ mouse hook end

# ------------ keyboard hook

class KeyboardHook:
	EventTypes = {	0x100:'KEYDOWN', 0x101:'KEYUP',  0x104:'SYSKEYDOWN', 0x105:'SYSKEYUP'}

	user32dll = ctypes.windll.user32	# the module object can be destroyed early the class object
	kernel32dll = ctypes.windll.kernel32

	def __init__(self, clb, events = None, clb_args=()):

		self.hook_type = 13
		self.callback = clb
		self.callback_args = clb_args
		if events is None:
			self.events = KeyboardHook.EventTypes.values()
		elif isinstance(events, list) or isinstance(events, tuple):
			self.events = events
		else:
			raise WinHookError('Wrong the event list')

		self.hook = None
		self.hook_thread = threading.Thread(target=common_hook_function, args=(self, ))
		self.hook_thread.daemon = True
		self.hook_thread.start()

	def __del__(self): self.stop()

	def stop(self):
		self.__class__.user32dll.PostThreadMessageW(self.hook_thread.ident, 0x0012, 0, 0)	# WM_QUIT, but generally any message stops thread
		self.hook_thread.join()

	def hook_event(self, nCode, wParam, lParam):
		if nCode != 0:
			return
		evType = KeyboardHook.EventTypes[wParam]
		if evType not in self.events:
			return

		lParam =  ctypes.cast(lParam, PKBDLLHOOKSTRUCT)
		ev = KeyboardEvent(evType, lParam.contents.vkCode, lParam.contents.scanCode, lParam.contents.flags)

		self.callback(ev, *self.callback_args)

class KeyboardEvent:
	def __init__(self, type, vkCode, scanCode, flags):
		self.type = type
		self.vkCode = vkCode
		self.scanCode = scanCode
		self.flags = flags
		self.sym = None
		if 48 <= vkCode <= 57 or 65 <= vkCode <= 90:	# 0-9, A-Z
			self.sym = chr(vkCode)
	def __str__(self):
		return 'KeyboardEvent %s %d(%s) %d %d'%(self.type, self.vkCode, self.sym, self.scanCode, self.flags)

# ------------ keyboard hook end

# it's not hook, but useful for similat purposes
def keyIsPressed(vKeyCode):
	if isinstance(vKeyCode, str) and len(vKeyCode)==1:	# other  virtual-key codes are here https://msdn.microsoft.com/ru-ru/library/windows/desktop/dd375731%28v=vs.85%29.aspx
		vKeyCode = ord(vKeyCode.upper())

	user32dll.GetAsyncKeyState.restype = ctypes.c_short
	state = user32dll.GetAsyncKeyState(vKeyCode)

	if state < 0:		# most significant bit is set, the key is down
		return True
	elif state == 1:	# if the least significant bit is set, the key was pressed after the previous call
		return False
	return None			# not pressed (state==0) or smth else

if __name__ == '__main__':
	if 1:
		def clb(ev):
			print ev

		mh = MouseHook(clb, ('LBUTTONDOWN', 'RBUTTONUP'))
		kh = KeyboardHook(clb)
		try:
			raw_input()
		except KeyboardInterrupt:
			pass
		mh.stop()
		kh.stop()
	else:	# test with Tkinter (passed)
		import logger
		root = logger.Tk()
		log = logger.Logger(root, font=('Arial', 10))
		log.pack(expand=1, fill=logger.BOTH)
		mh = MouseHook(lambda ev:log.addLine(ev), ('LBUTTONDOWN', 'RBUTTONUP'))
		root.mainloop()
		mh.stop()