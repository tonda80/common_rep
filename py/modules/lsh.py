#coding=utf8

# like shell

import re
import shutil
import os
import time
import sys
import signal
import glob
import subprocess
import threading
import platform
import distutils.dir_util

try:
	import psutil
except ImportError:
	print 'Warning. psutil is not imported. Some functionality (find_process, killall) will not be affordable.'

path_join = os.path.join

# helper functions
# ----------------------

platform_ = platform.system()
f_win = (platform_ == 'Windows')
f_lin = (platform_ == 'Linux')

# part generator: строку делит на части, внутренности кавычек (' и ") - это одна часть
# наверняка сырой и нужен только для удали молодецкой, написал потому что мог!
# регулярное же выражение нарушает порядок выдачи, что некрасиво)
#	for el in re.findall('(".*?"|\'.*?\')', fl):
#		yield el[1:-1]	# наверное не бывает glob в кавычках
#		fl = fl.replace(el, '', 1)
#	fl = fl.split()		# и остаток в лист
def _pg(ss):
	prev_qm = ib = iq = None	# прошлая кавычка, индексы начала слова и кавычки
	for i, c in enumerate(ss):
		if c.isspace():
			if ib != None and iq == None:
				yield ss[ib:i]
				ib = None
		elif c == '"' or c == "'":
			if ib != None:
				raise RuntimeError('[lsh._pg] [{}]: wrong {} position {}'.format(ss, c, i))
			elif iq == None:
				iq = i
				prev_qm = c
			elif prev_qm == c:
				yield ss[iq+1:i]
				iq = None
			else:
				raise RuntimeError('[lsh._pg] [{}]: wrong {} in {} position'.format(ss, prev_qm, iq))
		elif ib == None and iq == None:		# не пробельный и не кавычка
			ib = i
	if iq != None:
		raise RuntimeError('[lsh._pg] [{}]: unfinished {} in {} position'.format(ss, prev_qm, iq))
	elif ib != None:
		yield ss[ib:]	# последний элемент


def path_gen(paths):
	if isinstance(paths, basestring):
		paths = (paths, )
	for p in paths:
		for e in open_glob(p):
			yield e

# returns tuple of pathes
def open_glob(pth):
	pth = os.path.expanduser(pth)
	glob_lst = glob.glob(pth)
	if len(glob_lst) > 0:
		return glob_lst
	return (pth, )	# вернем tuple из входа, если глоб не раскрылся

# extention of file
def extention(pth):
	return os.path.splitext(os.path.normcase(pth))[1]

def human_size(s):
	prefs = ('K', 'M', 'G', 'T')
	D = 1024
	Sym_byte = 'B'
	if s < D:
		return '%d %s'%(s, Sym_byte)
	s = float(s)
	for pref in prefs:
		s /= D
		if s < D:
			break
	return '%.3f %s%s'%(s, pref, Sym_byte)

def unicode_file_name(pth):
	if f_win:
		return pth.decode('cp1251')
	elif f_lin:
		return pth.decode('utf8')
	raise NotImplementedError


# like shell
# ----------------------

# aliases for convenience
# --
pwd = os.getcwd

dirname = os.path.dirname
basename = os.path.basename

pjoin = os.path.join

# --

def test(pth):
	pth = os.path.expanduser(pth)
	return os.path.exists(pth)

def test_file(pth):
	pth = os.path.expanduser(pth)
	return os.path.isfile(pth)

def test_dir(pth):
	pth = os.path.expanduser(pth)
	return os.path.isdir(pth)


__oldpwd = None
def cd(pth):
	global __oldpwd
	if pth == '-':
		if __oldpwd is None:
			raise RuntimeError('lsh.cd: __oldpwd not set')
		pth = __oldpwd
	else:
		lst_pth = open_glob(pth)
		if len(lst_pth) > 0:
			pth = lst_pth[0]
	__oldpwd = os.getcwd()
	os.chdir(pth)


# creation only (no time update)
def touch(fl):
	for f in path_gen(fl):
		open(f, 'a')

def mkdir(pth, args=''):
	p = '-p' in args.split()
	for e in path_gen(pth):
		if p:
			if not os.path.exists(e):
				os.makedirs(e)
		else:
			os.mkdir(e)


def rm(pth, args=''):
	args = args.split()
	r_arg = '-r' in args
	f_arg = '-f' in args
	for e in path_gen(pth):
		try:
			if os.path.isdir(e):
				if not r_arg:
					raise RuntimeError('%s is a directory'%e)
				shutil.rmtree(e)
			else:
				os.remove(e)
		except OSError:
			if not f_arg:
				raise

# there is a difference from bash cp
# it creates all missing destination directories (not only last path element)
# TODO? to forbid this behavior artificially
def cp(src, dst, args=''):
	args = args.split()
	r_arg = '-r' in args

	dst = os.path.expanduser(dst)
	for e in path_gen(src):
		if os.path.isdir(e):
			if not r_arg:
				raise RuntimeError('%s is a directory'%e)

			if os.path.isfile(dst):
				raise RuntimeError('%s is a directory, but destination %s is an existing file'%(e, dst))
			elif os.path.isdir(dst):
				new_dst = os.path.join(dst, os.path.basename(e))
			else:
				new_dst = dst

			# TODO? to get rid of distutils import
			# уже не понимаю коммент ниже) какие то были проблемы с shutil.copytree, вроде из-за того что она требует отсутсвия dst
			# а может проблема в создании родительских директорий
			# видимо вот проблема https://stackoverflow.com/questions/1868714/how-do-i-copy-an-entire-directory-of-files-into-an-existing-directory-using-pyth
			# supposed prohibition here, to check that os.path.dirname(new_dst) exists
			distutils.dir_util.copy_tree(e, new_dst)

		else:
			shutil.copy(e, dst)

def mv(src, dst):
	dst = os.path.expanduser(dst)
	for e in path_gen(src):
		shutil.move(e, dst)


# Very rough analog. Returned a tuple (number of dirs, number of files, size in bytes or in a human representation)
def du(pth, args=''):
	if not test(pth):
		raise RuntimeError('File is not exist')

	if os.path.isfile(pth):
		d_num = 0
		f_num = 1
		sz = get_size(pth)
	elif os.path.isdir(pth):
		d_num = f_num = sz = 0
		for r,ds,fs in os.walk(pth):
			d_num += len(ds)
			f_num += len(fs)
			for e in fs:
				sz += get_size(os.path.join(r, e))
	else:
		raise NotImplementedError

	if '-h' in args.split():
		sz = human_size(sz)
	return (d_num, f_num, sz)

SIZE_ERROR_BEHAVIOUR = 0	# дефолт, см get_size для значения кода

def get_size(path):
	try:
		return os.lstat(path).st_size
	except OSError:
		if SIZE_ERROR_BEHAVIOUR == 0:	# стреляемся
			raise
		elif SIZE_ERROR_BEHAVIOUR == 1:	# ошибка в stdout
			sys.stdout.write('[error][lsh.get_size] cannot get size of {}\n'.format(path))
		elif SIZE_ERROR_BEHAVIOUR == 2:	# игнор
			pass
		else:
			raise RuntimeError('unexpected value of lsh.SIZE_ERROR_BEHAVIOUR: {}'.format(SIZE_ERROR_BEHAVIOUR))
	return 0


# TODO? l,a arguments
def ls(pth = None):
	if pth is None:
		pth = '.'
	res = {}
	for e in path_gen(pth):
		if os.path.isdir(e):
			res[e] = os.listdir(e)
		elif os.path.exists(e):
			res.setdefault('', []).append(e)
		#else: raise??

	if len(res) == 1:
		return res.itervalues().next()
	return res

def find_process(name, exactly = True):
	for proc in psutil.process_iter():
		try:
			proc_name = proc.name()
		except psutil.AccessDenied:
			continue
		if (exactly and proc_name == name) or (not exactly and name in proc_name):
			yield proc

def get_process(pid):
	try:
		return psutil.Process(pid)
	except psutil.Error:
		pass

# TODO? maybe to check the existence of the process after kill
def killall(name, sgn = signal.SIGTERM):
	cnt = 0
	for proc in find_process(name):
		os.kill(proc.pid, sgn)
		cnt += 1
	return cnt

def guaranteed_rm(f, args=''):
	while 1:
		try:
			rm(f, args)
		except (OSError, ) as e:
			raw_input(e)
		else:
			break


# Useful popen_args:
#		env - dict of the environment variables (path etc)
#		cwd - current directory for the new process
#		shell
#		stdin, stdout and stderr - they can be PIPE, file descriptor, file object.

def create_popen(args, **popen_args):
	if f_win and 'env' in popen_args and 'SystemRoot' not in popen_args['env']:
		popen_args['env']['SystemRoot'] = os.path.expandvars('$SystemRoot')

	return subprocess.Popen(args, **popen_args)

# background launch is how it looks for users
run_bg = create_popen

# communicate variant
def run_communicate(args, **popen_args):
	popen_args.setdefault('stdout', subprocess.PIPE)
	popen_args.setdefault('stderr', subprocess.PIPE)

	popen_obj = create_popen(args, **popen_args)

	out, err = popen_obj.communicate()

	return popen_obj.returncode, out, err

# communicate variant, getting stdout (with stderr), exception when an error
def run(args, **popen_args):
	popen_args.setdefault('stderr', subprocess.STDOUT)

	returncode, out, err = run_communicate(args, **popen_args)
	if returncode != 0:
		raise RuntimeError('returncode %d\n%s'%(returncode,out))
	return out

# for the immediate getting each line of process and possibility to stop it
class TrackedProcess:
	def __init__(self, args, **popen_args):
		popen_args['stderr'] = subprocess.STDOUT
		popen_args['stdout'] = subprocess.PIPE

		self.popen_obj = create_popen(args, **popen_args)

	def stop(self):
		self.popen_obj.kill()

	def watch(self, line_handler):
		try:
			while 1:
				returncode = self.popen_obj.poll()
				if returncode is not None:
					break
				line = self.popen_obj.stdout.readline()
				line_handler(line)
				if line == '':		# empty line is a sign that app is finished
					return

		except KeyboardInterrupt:
			returncode = 'KeyboardInterrupt'

		if returncode != 'KeyboardInterrupt':		# after KeyboardInterrupt we have IOError when reading
			for line in self.popen_obj.stdout:		# gets of rest
				line_handler(line)

		line_handler('')	# sign of end

		return returncode

# for the interactive output
def run_tee(args, **popen_args):
	class Nlocal:
		out = ''
	def line_handler(line):
		sys.stdout.write(line)
		Nlocal.out += line

	returncode = TrackedProcess(args, **popen_args).watch(line_handler)

	if returncode != 0:
		raise RuntimeError('returncode %s\n%s'%(returncode, Nlocal.out))
	return Nlocal.out

# открыть path программой по умолчанию
def start(path):
	shell = False
	if platform_ == 'Linux':
		args = ('xdg-open', path)
	elif platform_ == 'Windows':
		args = ('start', path)
		shell = True
	else:
		raise NotImplementedError
	subprocess.check_call(args, shell=shell)

# unittest
# "python lsh.py lsh_unittest.test_cp_mv" to run certain test
# ----------------------
if __name__ == '__main__':
	import unittest

	class lsh_unittest(unittest.TestCase):
		def test_str2list(self):
			self.assertEqual(['Program Files', 'q.txt', 'new file.txt'], list(_pg('"Program Files" q.txt "new file.txt"')))
			self.assertEqual([u'qw q', u'bla bla -', u'фыва'], list(_pg((u"'qw q'   \"bla bla -\"   фыва "))))
			self.assertEqual(['qw', 'xc', '1'], list(_pg('qw   xc 1')))

		def test_tilde(self):
			# test of expanduser comprehension
			test_path = os.path.join('any', 'path', 'to')
			self.assertEqual(os.path.join(os.path.expanduser('~'), test_path), os.path.expanduser(os.path.join('~', test_path)))

		def test_pwd_cd_create_remove(self):
			curr_dir = pwd()
			temp_dir = 'temp_dir_%f'%time.time()
			mkdir(temp_dir)
			cd(temp_dir)
			touch(_pg('f1 "file 2" f_3 f_4 f_5 ff_6z ff_7y'))
			mkdir(('d1','d2'))

			self.assertTrue(test_file('f1') and test_file('file 2'))
			self.assertTrue(test_file('f_3') and test_file('f_4') and test_file('f_5') and test_file('ff_6z') and test_file('ff_7y'))
			self.assertTrue(test_dir('d1') and test_dir('d2'))
			self.assertEqual(len(ls()), 7+2)
			#raw_input('Test files will be removed after Enter pressure')

			rm(_pg('f1 d2'), '-r')
			rm(('file 2', 'd1'), '-r')
			rm(_pg('f_[0-9] ff_[67]*'))

			self.assertFalse(test_file('f1') or test_file('file 2'))
			self.assertFalse(test_file('f_3') or test_file('f_4') or test_file('f_5') or test_file('ff_6z') or test_file('ff_7y'))
			self.assertFalse(test_dir('d1') or test_dir('d2'))
			self.assertEqual(len(ls()), 0)
			#raw_input('Check it')

			cd('-')
			rm(temp_dir, '-r')
			self.assertEqual(os.getcwd(), curr_dir)
			self.assertFalse(test_dir(temp_dir))

		def test_cp_mv(self):
			temp_dir = 'temp_dir_%f'%time.time()
			mkdir(temp_dir)
			cd(temp_dir)
			temp_dir2 = 'temp_dir_%f'%time.time()
			mkdir(temp_dir2)

			touch('_f1 f_2 f3 of4 f_5 sf6 _f7'.split())
			cp("f3", "f_8")
			cp(("f_*", 'of4'), temp_dir2)
			mv("of4", "nf4")
			mv("_f*", temp_dir2)

			curr_cont = [temp_dir2, 'f_2', 'f3', 'nf4', 'f_5', 'sf6', 'f_8']
			temp_dir2_cont = ['f_2', 'of4', 'f_5', '_f1', '_f7', 'f_8']
			#import pdb; pdb.set_trace()
			self.assertItemsEqual(ls(), curr_cont)
			self.assertItemsEqual(ls(temp_dir2), temp_dir2_cont)
			self.assertItemsEqual(ls(('.', temp_dir2)), {'.':curr_cont, temp_dir2:temp_dir2_cont})

			temp_dir3 = temp_dir2+'copy'
			cp(temp_dir2, temp_dir3, '-r')
			self.assertItemsEqual(ls(temp_dir2), ls(temp_dir3))
			cp(os.path.abspath(temp_dir2), temp_dir3, '-r')
			self.assertItemsEqual(ls(temp_dir2) + [temp_dir2,], ls(temp_dir3))

			temp_dir4 = temp_dir2+'copy2'
			cp(os.path.join('..', temp_dir, temp_dir2), temp_dir4, '-r')
			self.assertItemsEqual(ls(temp_dir2), ls(temp_dir4))

			temp_dir50 = u'temp dir with spaces %f'%time.time()
			temp_dir51 = path_join(temp_dir50, 'another  temp dir with spaces %f'%time.time())
			temp_dir52 = path_join(temp_dir50, u'again temp dir with spaces %f'%time.time())
			mkdir([temp_dir51], '-p')
			mkdir([temp_dir52])
			f51 = path_join(temp_dir51, '5 1 file')
			touch([f51])
			f52 = path_join(temp_dir52, '5 2 file')
			touch([f52])

			self.assertItemsEqual(ls([temp_dir50]), [os.path.basename(temp_dir51), os.path.basename(temp_dir52)])
			self.assertItemsEqual(ls([temp_dir51]), ['5 1 file'])
			self.assertItemsEqual(ls([temp_dir52]), ['5 2 file'])
			mv([f51], temp_dir52)
			mv([f52], temp_dir51)
			self.assertItemsEqual(ls([temp_dir51]), ['5 2 file'])
			self.assertItemsEqual(ls([temp_dir52]), ['5 1 file'])

			cd('-')
			#raw_input('Check it')
			rm(temp_dir, '-r')

		def test_extention(self):
			self.assertEqual(extention('video.avi'), '.avi')
			self.assertEqual(extention('NoExtention'), '')

		def test_human_size(self):
			self.assertEqual(human_size(500), '500 B')
			self.assertEqual(human_size(7000), '6.836 KB')
			self.assertEqual(human_size(12e6), '11.444 MB')
			self.assertEqual(human_size(500e9), '465.661 GB')

		def test_unicode_file_name(self):
			if f_win:
				self.assertEqual(unicode_file_name('\xd4\xe0\xe9\xeb'), u'Файл')
			elif f_lin:
				self.assertEqual(unicode_file_name('\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb'), u'Файл')
			else:
				raise NotImplementedError

		def test_run_tee(self):
			print '\nThis is test_run_tee'
			if f_win:
				run_tee('ping -n 3 ya.ru')
			elif f_lin:
				run_tee(('ping', '-c3', 'ya.ru'))

		def _test_run_and_stop(self):
			print '\nThis is test_run_and_stop'
			cmd = ('ping', 'ya.ru')
			rp = TrackedProcess(cmd)
			#time.sleep(3)
			def pp(l):
				sys.stdout.write(str(len(l)) + ': ' + l)
				sys.stdout.flush()
			th = threading.Thread(target = rp.watch, args=(pp,))
			th.start()
			raw_input('\nPress Enter to stop..\n\n')
			rp.stop()
			th.join()

		def test_mkdir_p(self):
			base_tst_pth = '1';	tst_pth = os.path.join(base_tst_pth, '2', '3')
			mkdir(tst_pth, '-p')
			mkdir(tst_pth, '-p')
			#raw_input('check it')
			rm(base_tst_pth, '-r')

	unittest.main()
