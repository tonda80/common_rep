# coding=utf8

import os

if __name__ == '__main__':
	from sys import getfilesystemencoding
	from stat import ST_SIZE

def abs_path(dir_abs_path, file_name):
	return os.path.join(dir_abs_path, file_name)
	
class FFinder:
	'''Рекурсивный поиск файлов'''
	def __init__(self, root, depth = -1):
		'''Начальная директория и глубина поиска (0 - искать только в корне, -1 - искать до конца)'''
		if not os.path.isdir(root):
			raise RuntimeError('Directory %s is not exist'%root)
		self.root = root
		self.depth = depth
		
	def find(self, cb_filter):
		'''Если cb_filter(abs_fname) == True, этот файл нас интересует
		Возращает словарь: ключ - абсолютный адрес директории, значение - список из имен файлов
		'''
		ret_dict = {}
		self.find_in(self.root, 0, cb_filter, ret_dict)		
		return ret_dict
		
	def find_in(self, dir_abs_path, curr_depth, cb_filter, ret_dict):
		'private method'
		search_bottom = self.depth >= 0 and curr_depth >= self.depth	# дно поиска		
		
		names = os.listdir(dir_abs_path)
		for n in names:
			abs_n = abs_path(dir_abs_path, n)
			if cb_filter(abs_n):
				ret_dict.setdefault(dir_abs_path, []).append(n)
			if os.path.isdir(abs_n) and not search_bottom:
				self.find_in(abs_n, curr_depth+1, cb_filter, ret_dict)
				
if __name__ == '__main__':
	coding = getfilesystemencoding()
	def print_path(p):
		try:
			print unicode(p, coding)
		except UnicodeEncodeError:
			print p
	
	ff = FFinder(r'D:\photos')
	ret_d = ff.find(lambda n: n.lower().endswith('.avi'))
	cntD = cntF = size = 0
	
	for d, n_l in ret_d.iteritems():
		print_path(d)
		cntD +=1
		for n in n_l:
			print '\t',
			print_path(n)
			cntF +=1
			size += os.stat(abs_path(d, n))[ST_SIZE]
				
	print '\n------------------------\nTotal %d dirs, %d files %dM'%(cntD, cntF, size/float(2**20))
				
