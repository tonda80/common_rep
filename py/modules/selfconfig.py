# coding=utf8

from os.path import abspath
import json
import copy

class SelfConfig:
	def __init__(self, file_name, marker='#parameters', write_changed_only = True):
		self.parameters = {}
		self.file = open(file_name, 'r+b')
		self.__load_parameters(marker)
		self.__init_parameters = None
		if write_changed_only:
			self.__init_parameters = copy.deepcopy(self.parameters)

	def __load_parameters(self, marker):
		while 1:
			line = self.file.readline()
			if len(line) == 0:
				raise RuntimeError('No marker for a parameter begin')
			if line.strip() == marker:
				break	# parameter's block is found

		self.par_begin = self.file.tell()
		self.file.seek(self.par_begin)	# it flushs next's buffer (seed doc for file.next())
		for l in self.file:
			tmp_l = l.split('#')
			conf_line = l.lstrip('#').strip()	# drop first # (python's comment)
			# conf_line is {"par_name" : value_of_some_type [, ....]}
			self.parameters.update(json.loads(conf_line))

	def write_parameters(self):
		if self.parameters != self.__init_parameters:	# writing only if it's necessary
			self.file.seek(self.par_begin)
			self.file.truncate()
			for n, v in self.parameters.iteritems():
				self.file.write('# %s\n'%json.dumps({n : v}))

	def close(self):
		self.par_begin = None
		self.file.close()

	def __getitem__(self, k):
		return self.parameters[k]

	def __setitem__(self, k, v):
		self.parameters[k] = v


if __name__ == '__main__':
	conf = SelfConfig(abspath(__file__))
	print conf.parameters
	conf.parameters['counter'] += 1
	# or right so
	conf['numbers'][0] += 2
	conf.write_parameters()

#parameters
# {"counter": 330}
# {"numbers": [15, 8, 4]}
# {"name": "King Artur"}
