#coding=utf8

import wwindow
import winhook
from subprocess import Popen
import time
import threading
from re import match
from sys import argv

import pyautogui

class Action:
	def __init__(self, **parameters):
		self.parameters = parameters
		self.class_name = self.__class__.__name__
	def __str__(self):
		s = self.class_name + '('
		for k,v in self.parameters.iteritems():
			s += '%s=%r, '%(k,v)
		if len(self.parameters) > 0:
			s = s[:-2]
		return s + ')'
	def __repr__(self):
		return self.__str__()

class MouseAction(Action):
	def __init__(self, **parameters):
		Action.__init__(self, **parameters)
		self.type = parameters['type']
		self.x = parameters['x']
		self.y = parameters['y']
class SleepAction(Action):
	def __init__(self, **parameters):
		Action.__init__(self, **parameters)
		self.t = parameters['t']

# TODO maybe to catch delay times?
class Catcher:
	def __init__(self, id_, stop_key = 0x91, handle_number = None, ):	# 0x91 VK_SCROLL
		self.win = wwindow.WWindow(id_, handle_number)
		self.win.show()
		self.mouse_hook = winhook.MouseHook(self.mouse_callback, ('LBUTTONDOWN', 'LBUTTONUP','RBUTTONDOWN', 'RBUTTONUP',))
		#self.key_hook = KeyboardHook(self.key_callback)
		self.story = []
		self.stop_event = threading.Event()
		self.stop_key = stop_key

	def wait(self):
		self.stop_event.wait()
		self.mouse_hook.stop()
		self.polish_story()

	# TODO change it
	def polish_story(self):
		MOUSE_MISTAKE = 3
		if self.story:
			new_story = []
			for e in self.story:
				try:
					last_e = new_story[-1]
				except IndexError:
					new_story.append(e)
					continue
				if (	e.class_name == last_e.class_name == 'MouseAction' and
						match(r'(R|L|M)BUTTONUP', e.type) and match(r'(R|L|M)BUTTONDOWN', last_e.type) and
						abs(e.x-last_e.x)<MOUSE_MISTAKE and abs(e.y-last_e.y)<MOUSE_MISTAKE ):
					new_story[-1] = MouseAction(type=e.type[0]+'BUTTONCLICK', x=e.x, y=e.y)
				else:
					new_story.append(e)

			self.story = new_story

	def print_story(self):
		print '\n----print_story----'
		for e in self.story:
			print e


	def check_stop(self):
		if winhook.keyIsPressed(self.stop_key):
			print 'Stop!'
			self.stop_event.set()
			return True

	def mouse_callback(self, ev):
		if not self.check_stop():
			x0, y0, wdt, hgh = self.win.getPosition()
			self.story.append(MouseAction(type=ev.type, x=ev.x-x0, y=ev.y-y0))
			print self.story[-1]

class ActionPlayer:
	def __init__(self, id_, action_list, handle_number = None, step_delay = 0, debug_print = True):
		self.debug_print = debug_print
		self.win = wwindow.WWindow(id_, handle_number)
		self.win.show()
		for action in action_list:
			self.execute_action(action)
			time.sleep(step_delay)

	def execute_action(self, action):
		if self.debug_print:
			print action

		if isinstance(action, MouseAction):
			x0, y0, wdt, hgh = self.win.getPosition()
			x = x0 + action.x
			y = y0 + action.y
			btn_type = {'L':'left', 'R':'right', 'M':'middle'}[action.type[0]]	# first letter of type
			if self.debug_print:
				print x, y, btn_type
			if action.type.endswith('DOWN'):
				pyautogui.mouseDown(x, y, button=btn_type)
			elif action.type.endswith('UP'):
				pyautogui.mouseUp(x, y, button=btn_type)
			elif action.type.endswith('CLICK'):
				pyautogui.click(x, y, button=btn_type)
			else:
				raise RuntimeError('Unknown the mouse action')
		elif isinstance(action, SleepAction):
			time.sleep(action.t)

if __name__ == '__main__':
	#pr = Popen(r'D:\aberezin\programs\calc.exe')
	id_ = 'calc.exe'	#pr.pid  TOFSIM32.exe
	if len(argv) > 1:
		id_ = argv[1]
	time.sleep(0.5)
	catcher = Catcher(id_, handle_number = 0)
	print 'Go...'
	catcher.wait()
	catcher.print_story()
	if raw_input('To play? (y/_): ') == 'y':
		ActionPlayer(id_, catcher.story, handle_number = 0)		# step_delay = 0.2




