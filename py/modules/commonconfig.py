# coding=utf8

import os
import sqlite3
import json
import datetime

# Использование
# default_config = {'parameter1':1, 'parameter2':2}
# config = CommonConfig('wonder_app_220517', default_config)
# стоит добавлять в имя приложения дату, чтобы исключить создание новой чудо утилиты с именем старой.
# или можно config = CommonConfig('wonder_app_220517', parameter1=1, parameter2=2)
# config['parameter1'] += 1
# config.save()
# config['any_absent_parameter'] is None


class CommonConfig:
	@staticmethod
	def dir_path(): return os.path.join(os.path.expanduser('~'), '.pyutils')
	@staticmethod
	def db_path(): return os.path.join(CommonConfig.dir_path(), 'config')
	@staticmethod
	def version(): return '2.1'

	def __init__(self, util_name, default_config=None, **kw):
		self._auto_asked = set()		# 	если таких параметров не будет, они будут запрошены у юзера

		new_db = False	# db is not exist yet
		if not os.path.exists(CommonConfig.dir_path()):
			os.mkdir(CommonConfig.dir_path())
			new_db = True
		if not os.path.exists(CommonConfig.db_path()):
			new_db = True

		self.sql_conn = sqlite3.connect(CommonConfig.db_path(), isolation_level = None)		# autocommit mode
		self.sql_cursor = self.sql_conn.cursor()

		if new_db:
			self.__init_db()

		self.util_name = util_name

		if default_config is None:
			default_config = kw
		elif not isinstance(default_config, dict):
			raise ValueError('default_config must be dict')

		self.__load(default_config)

	def auto_asked(self, *args):
		self._auto_asked = set(args)

	def __init_db(self):
		print 'A new pyutil database is created'

		self.sql_cursor.execute("CREATE TABLE __about__ (field_name TEXT, value TEXT)")
		self.sql_cursor.execute("INSERT INTO __about__ VALUES ('version', ?)", (CommonConfig.version(),))
		self.sql_cursor.execute("INSERT INTO __about__ VALUES ('creation_time', ?)", (datetime.datetime.utcnow().isoformat(),))

		self.sql_cursor.execute("CREATE TABLE __utils__ (util_name TEXT UNIQUE, config TEXT)")

	def __get_config(self):
		self.sql_cursor.execute("SELECT config from __utils__ WHERE util_name==?", (self.util_name,))
		return self.sql_cursor.fetchall()

	def __load(self, default_config):
		sql_req = self.__get_config()
		if len(sql_req) == 0 or len(sql_req[0]) == 0:
			self._config = default_config
			self.save()
		else:
			self._config = json.loads(sql_req[0][0])
			for k in default_config.iterkeys():
				self._config.setdefault(k, default_config[k])

	def save(self):
		# named placeholders see https://docs.python.org/2/library/sqlite3.html#sqlite3.Cursor.execute
		parameters = {'util_name': self.util_name, 'config': json.dumps(self._config)}
		if len(self.__get_config()) != 0:
			sql = "UPDATE __utils__ SET config = :config WHERE util_name == :util_name"
		else:
			sql = "INSERT INTO __utils__ VALUES (:util_name, :config)"
		self.sql_cursor.execute(sql, parameters)

	def __setitem__(self, k, v):
		self.set(k, v)
	def set(self, k, v):
		self._config[k] = v

	def __getitem__(self, k):
		return self.get(k)
	def get(self, k, def_ = None):
		ret = self._config.get(k, def_)
		if ret is None and k in self._auto_asked:
			ret = self.ask_user(k)
			self.save()
		return ret

	def ask_user(self, key):
		# TODO gui метод или как то в правильном потоке вызывать
		new_value = raw_input('\nInput a value of {}: '.format(key)).strip()
		self.set(key, new_value)
		return new_value

	def reset(self):
		self._config = {}
		self.save()


if __name__ == '__main__':
	default_parameters = {'p1':12, 'p2':45, 'p3':{'pp1':[1,2,3,4], 'pp2':None}, 'late_par':0}
	my_config = CommonConfig('commonconfig_test', **default_parameters)

	print my_config._config

	my_config['p1'] += 8
	my_config['p2'] += 1
	my_config['late_par'] += 1
	v = my_config['quote_test_par']
	if v is None:
		my_config['quote_test_par'] = "test of 'bad symbols'"

	print "my_config['absent_parameter'] is", my_config['absent_parameter']

	my_config.save()
