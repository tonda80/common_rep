# coding=cp1251
from Tkinter import *
from math import *

class Logger(Text):
	def __init__(self, master, **kw):
		kw.setdefault('font', ('Courier New', 11))
		kw.setdefault('wrap', NONE)	# CHAR WORD
		
		# creating Text and scrollbars on Frame
		self._frame = Frame(master)
		scr_y = Scrollbar(self._frame); scr_x = Scrollbar(self._frame, orient=HORIZONTAL)
		scr_y.pack(side=RIGHT, fill=Y); scr_x.pack(side=BOTTOM, fill=X)
		Text.__init__(self, self._frame, yscrollcommand=scr_y.set, xscrollcommand=scr_x.set, state='disabled', **kw)
		Text.pack(self, expand=YES, fill=BOTH)
		scr_y.config(command=self.yview); scr_x.config(command=self.xview)
		
		self.cntMark = 0
		
	def pack(self, *arg, **kw):
		self._frame.pack(*arg, **kw)
		
	def addLine(self, txt):
		self.config(state = 'normal')
		self.insert(END, txt)
		self.insert(END, '\n')
		self.config(state = 'disabled')
			
	def clearAll(self):
		self.config(state = 'normal')
		self.delete('1.0', END)
		self.config(state = 'disabled')
		
	# demonstration of abilities!!
	def addCleanableLine(self, txt, btnText='<<'):
		begClean = 'b%d'%self.cntMark
		endClean = 'e%d'%self.cntMark
		self.cntMark += 1
		def clean():
			self.config(state = 'normal')
			#print begClean, endClean, self.index(begClean), self.index(endClean)
			self.delete(begClean, endClean)
			self.mark_unset(begClean)
			self.mark_unset(endClean)
			self.config(state = 'disabled')
			
		self.config(state = 'normal')
		self.mark_set(INSERT, END)	# if just to insert to end, the mark will move together with end. See difference print self.index(END), self.index(INSERT)
		self.mark_set(begClean, INSERT); self.mark_gravity(begClean, LEFT)
		self.insert(END, txt)
		self.insert(END, ' '); self.window_create(END, window=Button(self, text=btnText, command=clean))
		self.insert(END, '\n')
		self.mark_set(endClean, INSERT); self.mark_gravity(endClean, LEFT)	
		self.config(state = 'disabled')
		
if __name__ == '__main__':
	root = Tk()
	
	log = Logger(root, font=('Arial', 10))
	
	log.pack(expand=1, fill=BOTH)
	
	Button(root, text='Add',width=10,command=lambda:log.addLine(addSt.get())).pack(side=LEFT,padx=5,pady=5)
	Button(root, text='Add Cleanable',command=lambda:log.addCleanableLine(addSt.get())).pack(side=LEFT,padx=5,pady=5)
	Button(root, text='Clear',width=10,command=log.clearAll).pack(side=LEFT,padx=5,pady=5)
	addSt = StringVar(value='Hello world');
	Entry(root,textvariable = addSt).pack(side=LEFT,expand=YES,fill=X,padx=5,pady=5)
	
	def test(): print log.index(INSERT)
	Button(root, text='Test',width=10,command=test).pack(side=RIGHT,padx=5,pady=5)
	
	root.mainloop()
