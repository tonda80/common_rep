#coding=utf8

import argparse
import logging
import os


class BaseApp(object):
	# создает self.args и self.log
	def __init__(self, args=None):
		# классы в середине иерархии добавляют аргументы в _create_parser (см пример в BaseWalkerApp)
		# класс конечного приложения добавляет свои аргументы в add_arguments
		parser = self._create_parser()
		self.add_arguments(parser)
		self.args = parser.parse_args(args)

		self.log = self._create_logger()
		self._set_log_level(logging.DEBUG if self.args.verbose else logging.INFO)

		# тут загружаем конфигурацию приложения (например через commonconfig), вызов и реализация save_config на стороне конечного приложения
		self.load_config()

	def load_config(self):
		pass

	def _create_logger(self):
		logging.basicConfig(format=self.log_format())
		return logging.getLogger(self.__class__.__name__)

	def _set_log_level(self, level):
		self.log.setLevel(level)
		logging.getLogger().setLevel(level)

	def log_format(self):
		return '[%(levelname)s][%(name)s] %(message)s'

	def _create_parser(self):
		parser = argparse.ArgumentParser(parents=self._get_parent_parsers())
		parser.add_argument('--verbose', '-v', action='store_true', help='Verbose output')
		return parser

	def _get_parent_parsers(self):
		return ()

	def add_arguments(self, parser):
		pass

	def app_assert(self, cond, message):
		if not cond:
			raise AssertionError(message)


BaseConsoleApp = BaseApp


class BaseTkApp(BaseApp):
	def __init__(self, args=None):
		super(BaseTkApp, self).__init__(args)

		self.root = self.create_gui()	# создаем Tk gui
		self.clear_status_bar = lambda: self.set_status_bar('')
		self.root.bind_left_click(lambda e: self.clear_status_bar()) 			# очищаем статус бар кликом по окну

	def set_status_bar(self, msg):
		self.root.statusBar.set(msg)
		self.root.update()

	def mainloop(self):
		return self.root.mainloop()


# консольное приложение которое бегает по ФС
class BaseWalkerApp(BaseApp):
	def __init__(self, args=None):
		super(BaseWalkerApp, self).__init__(args)
		self.init_callbacks('dir_callback', 'file_callback', 'dirnames_callback', 'extension_filter')

	def _create_parser(self):
		parser = super(BaseWalkerApp, self)._create_parser()
		parser.add_argument('--root_dir', required=True, help='Root directory for search')
		return parser

	# класс наследник объявляет коллбеки для обработки файлов\директорий
	# тут мы присваиваем отсутсвующим коллбекам None
	# плюсы: имеем в одном месте полный список	коллбеков и не вызываем пустые
	# минусы: таки все это похоже на очередной закидон)
	def init_callbacks(self, *names):
		for name in names:
			if name not in self.__class__.__dict__:
				setattr(self, name, None)

	# https://docs.python.org/2.7/library/os.html#os.walk
	def walk(self):
		root_path = os.path.realpath(self.args.root_dir)
		if not os.path.isdir(root_path):
			raise RuntimeError('Directory %s does not exist'%root_path)
		self.log.info('Started search from %s'%root_path)

		for dirpath, dirnames, filenames in os.walk(root_path, onerror=self.on_walk_error):
			if self.dir_callback:
				for d in dirnames:
					self.dir_callback(os.path.join(dirpath, d))
			if self.file_callback:
				for f in filenames:
					if not self.extension_filter or self.extension_filter(os.path.splitext(f)[1].lower()):
						self.file_callback(os.path.join(dirpath, f))

			if self.dirnames_callback and dirnames:
				self.dirnames_callback(dirpath, dirnames)	# тут можно модифицировать список директорий для дальнейшего поиска

	def on_walk_error(self, err):
		raise err

if __name__ == '__main__':
	class App(BaseApp):
		def add_arguments(self, parser):
			parser.add_argument('--arg1')
			parser.add_argument('--arg2')

		def main(self):
			self.log.info('I do very useful work!')

	class WalkerApp(BaseWalkerApp):
		def dirnames_callback(self, dirpath, dirnames):
			self.log.info('  %s:%s'%(dirpath, dirnames))
			test_exclude = 'SeaBattle'
			if test_exclude in dirnames:
				dirnames.remove(test_exclude)

		def dir_callback(self, path):
			self.log.info('D %s'%path)
			if not os.path.isdir(path):
				RuntimeError('walk error')

		def file_callback(self, path):
			self.log.info('F %s'%path)
			if not os.path.isfile(path):
				RuntimeError('walk error')

		def extension_filter(self, ext):
			return ext != '.py'
			#return ext == '.json'

		def main(self):
			self.walk()

	# заготовка для типичного tk app
	class TkApp(BaseTkApp):
		def load_config(self):
			#app_name = '{app_name}_{data_label}'
			#default_config = {}
			# надо from commonconfig import CommonConfig
			#self.config = CommonConfig(app_name, **default_config)
			self.log.info(u'это вызывается в конструкторе, тут можно ещё добавить какую то раннюю инициализацию')

		def save_config(self):
			#self.config['{some_key}'] = '{some_type_value}'
			#self.config.save()
			self.log.info(u'вызываем save_config когда надо, например при выходе из приложения')

		def create_gui(self):
			root = uTkinter.uTk(u'{title}', 100, 100, createStatus=True)
			# тут создаем нужный gui
			uTkinter.uButton(root, lambda: self.set_status_bar('Hello!'), 'press me')
			# и настраиваем поведение окошка
			root.set_exit_by_escape()
			root.set_delete_callback(self.save_config)
			return root		# !

	App(args=()).main()
	WalkerApp(args=('--root_dir', os.path.join('..', 'utils'), '-v')).main()

	import uTkinter
	TkApp().mainloop()
