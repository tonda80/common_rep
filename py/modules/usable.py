# coding=cp1251

def input_loop(f_act, f_conv=int):
	'''���������� ��������� f_act ��� �������������� ������� (����������� f_act(f_conv(raw_input('>> ')))'''
	while 1:
		try: inp_val = f_conv(raw_input('>> '))
		except ValueError:
			print u'������ �����'
			continue
		except KeyboardInterrupt:
			return
		
		f_act(inp_val)

		

class SimpleCallback:
	'''��������� ��������� ��������� ������, ���������� �� ����,
	 ������������ ������� ������� ��������� (Scott David Daniels),
	 ������� ����� �������� � � ��������� �������-�����������.'''
	def __init__(self, callback, *firstArgs):
			self.__callback = callback
			self.__firstArgs = firstArgs
	def __call__(self, *args):
			return self.__callback(*(self.__firstArgs + args))


def ishexdigit(st):
	''' ���������� True ���� ������ ������� �� 16-������ ����'''
	if st=='': return False
	else:
		for c in st:
			if ('0'<=c<='9') or ('a'<=c<='f') or ('A'<=c<='F'): continue
			else: print time.clock()-t; return False
		return True
		

class ValidatingEntry(Entry):
    # base class for validating entry widgets
	def __init__(self, master, max, value='', **kw):
		Entry.__init__(self, master, **kw)
		self.__value = value
		self.__max = max
		self.__variable = StringVar()
		self.__variable.set(value)
		self.__variable.trace('w', self.__callback)
		self.config(textvariable=self.__variable)

	def __callback(self, *dummy):
		value = self.__variable.get()
		newvalue = self.validate(value)
		if newvalue is None:
			self.__variable.set(self.__value)
		elif newvalue != value:
			self.__value = newvalue
			self.__variable.set(newvalue)
		else:
			self.__value = value

	def validate(self, value):
		# override: return value, new value, or None if invalid
		#print value
		if value=='': return ''
		try: num = int(value)
		except ValueError: return None
		if num>self.__max: return str(self.__max)
		return value

			



if __name__ == '__main__':
	
	def pp(x): print x+1
	
	def s2d(s):
		try:
			return int(s)
		except ValueError:
			try:
				return int(s, 16)
			except ValueError:
				return None
			
	input_loop(pp, lambda x: int(x,16))
	