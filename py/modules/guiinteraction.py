#coding=utf8

import psutil
import platform

class GuiInteractionError(RuntimeError):
	pass

def pgrep(name):
	pids = []
	for proc in psutil.process_iter():
		try:
			if name in proc.name():
				pids.append(proc.pid)
		except psutil.AccessDenied:
			pass
	return pids

def get_proc_by_pid(pid):
	for proc in psutil.process_iter():
		try:
			if pid == proc.pid:
				return proc
		except psutil.AccessDenied:
			pass

# must return one item (or raise exception)
def manual_choice_of_one(options, what='options', describer=str):
	len_ = len(options)
	if len_ == 1:
		return options[0]
	if len_ == 0:
		raise GuiInteractionError('empty options')
	print 'Too many %s. You must choose one'%what
	for i,o in enumerate(options):
		print '%d:\t%s'%(i, describer(o))
	while 1:
		try:
			return options[int(raw_input('make a choice: '))]
		except (IndexError, ValueError):
			continue

platform_ = platform.system()
if platform_ == 'Linux':
	import Xlib, Xlib.display

	# based on this good example
	# https://unix.stackexchange.com/questions/5999/setting-the-window-dimensions-of-a-running-application/17395
	class LinuxDelegate:
		def __init__(self):
			self.display = Xlib.display.Display()
			self.root = self.display.screen().root

		def get_handle(self, verbose=False, **kw):
			if not kw:
				raise RuntimeError('empty kw')
			req_window_id = kw.pop('window_id', None)
			req_wm_name = kw.pop('wm_name', None)
			req_process_name = kw.pop('process_name', None)
			req_pid = kw.pop('pid', None)
			if kw:
				raise RuntimeError('unknown arguments: %s'%kw)

			req_pids = [req_pid] if req_pid else []
			if req_process_name:
				req_pids.extend(pgrep(req_process_name))
			#
			if verbose:
				print '__debug req %s, %s, %s, %s'%(req_window_id, req_wm_name, req_process_name, req_pids)
			ret = []
			windowIDs = self.root.get_full_property(self.display.intern_atom('_NET_CLIENT_LIST'), Xlib.X.AnyPropertyType).value
			for windowID in windowIDs:
				window = self.display.create_resource_object('window', windowID)
				wm_name = window.get_wm_name()
				prop = window.get_full_property(self.display.intern_atom('_NET_WM_PID'), Xlib.X.AnyPropertyType)
				pid = prop.value[0] if prop else None	# TODO??? e.g. it doesn't work with tkgrep 
				#
				if verbose:
					print windowID, wm_name, pid
				if (not req_window_id or req_window_id == windowID) and \
				(not req_wm_name or req_wm_name == wm_name) and \
				(not(req_process_name or req_pid) or pid in req_pids):
					ret.append((window, pid))

			def descr(el):
				return '%s, pid %s'%(self.info(el[0]), el[1])

			return manual_choice_of_one(ret, 'windows', descr)[0]

		def info(self, w):
			_d = w.get_geometry()._data
			return 'X window: id %d, wm_name %s, pos (%d:%d), size (%dx%d)'%(w.id, w.get_wm_name(), _d['x'], _d['y'], _d['width'], _d['height'])

		def set_position(self, win, x, y, w, h):
			kw = {}
			if x is not None: kw['x'] = x
			if y is not None: kw['y'] = y
			if w is not None: kw['width'] = w
			if h is not None: kw['height'] = h

			win.configure(**kw)
			self.display.sync()

		def get_position(self, w):
			_d = w.get_geometry()._data
			return (_d['x'], _d['y'], _d['width'], _d['height'])

		def show(self, w):
			w.raise_window()
			self.display.sync()

	os_delegate = LinuxDelegate()

elif platform_ == 'Windows':
	raise NotImplementedError	# TODO
else:
	raise NotImplementedError

class Window:
	def __init__(self, verbose=False, **kw):
		try:
			self.win_handle = os_delegate.get_handle(verbose, **kw)
		except GuiInteractionError:
			raise GuiInteractionError('Cannot identify a window')

	def __str__(self):
		return os_delegate.info(self.win_handle)

	def set_position(self, x=None, y=None, w=None, h=None):
		return os_delegate.set_position(self.win_handle, x, y, w, h)

	def get_position(self):
		return os_delegate.get_position(self.win_handle)

	def show(self):
		os_delegate.show(self.win_handle)

if __name__ == '__main__':
	#win = Window(process_name='calc')
	win = Window(wm_name='tkgrep')
	print 'Got', win
	x,y,w,h = win.get_position()
	win.show()
	new_x, new_y = x+30, y+30
	raw_input('press enter to move %d:%d'%(new_x, new_y))
	win.set_position(x=new_x, y=new_y)
