#coding=utf8

# http://stackoverflow.com/questions/510357/python-read-a-single-character-from-the-user

import platform
platform_ = platform.system()

if platform_ == 'Windows':
	import msvcrt

	def win_getch(wait = True):
		if wait or msvcrt.kbhit():
			return msvcrt.getch()
		return None

	getch = win_getch

elif platform_ == 'Linux':
	import sys, tty, termios, select

	def linux_getch(wait = True):
		in_ = sys.stdin.fileno()
		old_term_attr = termios.tcgetattr(in_)
		try:
			tty.setraw(in_, termios.TCSANOW)
			if select.select((in_,), (), (), None if wait else 0)[0]:
				c = sys.stdin.read(1)
				# is it necessary to clear stdin???? there are the strange sleep effects
				#while select.select((in_,), (), (), 0)[0]:
				#	sys.stdin.read(1)
			else:
				c = None
		finally:
			termios.tcsetattr(in_, termios.TCSANOW, old_term_attr)
		if c == '\x03':
			raise KeyboardInterrupt
		return c

	getch = linux_getch

else:
	raise NotImplementedError


if __name__ == '__main__':
	import time

	print 'non-blocking getch test (ctrl-c to stop)'
	print '-'*20
	while 1:
		try:
			c = getch(False)
			if c:
				print '-> %s'%c
			else:
				print 'not pressed'
			time.sleep(2)
		except KeyboardInterrupt:
			break

	print 'blocking getch test (ctrl-c to stop)'
	print '-'*20
	while 1:
		print '--- '*3
		s = getch()
		ord_s = ord(s)
		print '%-4s%-4d%-4s'%(s, ord_s, hex(ord_s))
		if ord_s == 27: # ESC
			break
