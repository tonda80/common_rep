# coding=utf8

import math


#import re
#re_bin = re.compile(r'^-?[01]+$')
#def bin2dec(string):
	#''' Переводит строку двоичного кода в число num
	#Есть стандартная int(s, 2), но раньше я об этом не знал :)
	#'''
	#if re_bin.match(string) is None:
		#raise ValueError('"%s" is not binary'%string)
	#ret = 0
	#sign = 1
	#if string[0] == '-':
		#sign = -1
		#string = string[1:]
	#pow_ = 1
	#for sym in reversed(string):
		#if sym == '1':
			#ret += pow_
		#pow_ = pow_ << 1
	#return sign*ret

#def dec2bin(num):
	#''' Переводит число num в строку двоичного кода
	#Вообще то есть стандартная bin, но раньше я об этом не знал, а потом знал но забыл :)
	#'''
	#sign = ''
	#if num < 0:
		#sign = '-'
		#num = -num
	#elif num == 0:
		#return '0'
	#s = ''
	#while num != 0:
		#num, r = divmod(num, 2)
		#if r: s = '1' + s
		#else: s = '0' + s
	#return sign + s


def n2cc(x, minus1):
	''' Переводит число x в дополнительный код
	minus1 - -1 в этом коде
	'''
	max_pos = minus1>>1	# максимальное положительное
	if x > max_pos or x < -(max_pos+1): return # ошибка
	return x & minus1

def cc2n(cc, minus1):
	''' Переводит число cc из дополнительного кода в знаковый
	minus1 - -1 в этом коде
	'''
	if cc > minus1 or cc < 0: return	# ошибка
	if cc & (~(minus1>>1)): return ~minus1 | cc
	else: return x


def n2sym(num):
	if 0 <= num <= 9:
		return str(num)
	elif 10 <= num <=35:
		return chr(num - 10 + ord('A'))
	else:
		return '[%X]'%num

def int2str(num, base=10):
	''' число в строку произвольной системы счисления
	'''
	if type(num) is not int or base<2:
		return None
	st = ''
	if num == 0:
		return '0'
	elif num < 0:
		st='-'
		num = -num
	# pow_ по сути позиция старшего разряда от 0
	# f=lambda num,base:math.trunc(math.log(num, base)) глючит!
	#	f(243,3) -> 4, а должно быть 5
	#	math.log(243,3) -> 4.99999999999
	#	math.log(243,3)+sys.float_info.epsilon*2 -> 5.0
	#	то же с floor, поэтому проверяем
	pow_ = math.trunc(math.log(num, base))
	if base**(pow_+1) <= num:
		pow_ += 1
	while pow_ >= 0:
		currDig, num = divmod(num, base**pow_)
		st = st + n2sym(currDig)
		pow_ -= 1
	return st

# можно сделать обобщенный str2int(и sym2ciph) на основе bin2dec, но смысла мало
# стандартного int, для base < 37, более чем хватает

word2int = lambda w:int(w, 36)
int2word = lambda i:int2str(i, 36)

if __name__=='__main__':
	import random
	import sys

	action = sys.argv[1]

	if action == '0':
		print 'python int demo'
		# int() base must be >= 2 and <= 36
		max_base = 36
		for i in xrange(1, 256):
			c = chr(i)
			try:
				n = int(c, max_base)
			except ValueError:
				continue
			print 'int(%s, max_base) -> %d'%(c, n)
		print u'\nИ чудеса нумерологии!'
		print int('YourName', max_base)
		# запоминать числа (по тексту и базе)?
		# 3 буквы - макс. 46656, для номера карты выгода неочевидна math.log(10**12,36) => 7.710583253630812
	elif action == '1':
		print 'n2sym demo'
		for i in xrange(40):
			print n2sym(i)
	elif action == '2':
		print 'int2str demo'
		n, b = map(int, sys.argv[2:])
		print int2str(n, b)
	elif action == '3':
		print 'int2str test'	# см коммент в методе, зачем такой тест
		cnt = 0
		while 1:
			n, b = random.randrange(1000000), random.randrange(2, 37)
			exc = False
			tSt = back_n = None
			try:
				tSt = int2str(n, b)
				back_n = int(tSt, b)
			except:
				exc = True
			if exc or n != back_n:
				print ' error', n, back_n, b, tSt, cnt, exc
				break
			cnt += 1
			if cnt%100000==0:
				sys.stdout.write('*')
				sys.stdout.flush()
	elif action == '4':
		print 'sys.float_info.epsilon demo'
		def machineEpsilon(func=float):
			machine_epsilon = func(1)
			while func(1)+machine_epsilon != func(1):
				machine_epsilon_last = machine_epsilon
				machine_epsilon = machine_epsilon / func(2)
			return machine_epsilon_last
		print machineEpsilon()
		print sys.float_info.epsilon
