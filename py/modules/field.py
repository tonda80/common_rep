# coding=utf8

from Tkinter import *
import itertools

# game field, copy from the old module Table
class Field(Canvas):
	DEFAULT_ITEM_SYM = 'X'
	EMPTY_ITEM_SYM = '-'

	class CanvasItem():
		def __init__(self, owner, row, column):
			self.owner = owner
			self.row, self.column = row, column
			self.scale = 1
			self.type = self.__class__.__name__.lower()
			self.canvas_object = None 				# must be defined into the derived classes
			self.tag = Field.DEFAULT_ITEM_SYM 		# one symbol; used for serialization

		def set_tag(self, sym):
			if not (isinstance(sym, str) and len(sym)==1 and sym!=Field.EMPTY_ITEM_SYM):
				raise RuntimeError('Bad symbol')
			self.tag = sym

		def redraw(self, scale=None, **tk_options):
			if scale:
				self.scale = scale
			self.owner.itemconfigure(self.canvas_object, tags=(self.tag,), **tk_options)
			self.owner.coords(self.canvas_object, *self.bbox())

		def destroy(self):
			self.owner.delete(self.canvas_object)

		def bbox(self):
			if self.scale < 0:
				self.scale = 0
			x = self.column*self.owner.w_cell
			y = self.row*self.owner.h_cell
			x_d = (1 - self.scale)*self.owner.w_cell/2
			y_d = (1 - self.scale)*self.owner.h_cell/2
			return (x+x_d, y+y_d, x+self.owner.w_cell-x_d, y+self.owner.h_cell-y_d)

	class Circle(CanvasItem):
		def __init__(self, owner, row, col):
			Field.CanvasItem.__init__(self, owner, row, col)
			self.canvas_object = self.owner.create_oval(0,0,0,0)

	class Rectangle(CanvasItem):
		def __init__(self, owner, row, col):
			Field.CanvasItem.__init__(self, owner, row, col)
			self.canvas_object = self.owner.create_rectangle(0,0,0,0)

	ItemTypes = dict((t.__name__.lower(), t) for t in (Circle, Rectangle))

	# ---------------

	def __init__(self, master, default_pack=True, **kw):
		kw.setdefault('bg', 'white')
		kw.setdefault('height', 300); kw.setdefault('width', 300)
		Canvas.__init__(self, master, **kw)

		self.master = master
		self.bind('<Configure>', self.redraw)

		self.initialized = False

		if default_pack:
			self.pack(fill=BOTH, expand=YES)

	def reset(self, q_row, q_column, square = True, visible_grid = True):
		self.initialized = False

		self.delete('all')

		self.square = square
		self.visible_grid = visible_grid

		self.q_row = q_row
		self.q_column = q_column
		self.w_cell = self.h_cell = 0

		self.vert_lines = [self.create_line(0,0,0,0) for i in xrange(self.q_column + 1)]
		self.hor_lines = [self.create_line(0,0,0,0) for i in xrange(self.q_row + 1)]
		self.items = [[None for i in xrange(self.q_column)] for i in xrange(self.q_row)]

		self.initialized = True
		self.redraw()

	def clear(self):
		for item in self.all_items(True):
			self.delete_item(item.row, item.column)

		self.redraw()

	def redraw(self, event=None):
		if not self.initialized:
			return

		self.w_cell = self.winfo_width()/self.q_column
		self.h_cell = self.winfo_height()/self.q_row

		if self.square:
			self.w_cell = self.h_cell = min(self.w_cell, self.h_cell)

		w_tbl = self.w_cell*self.q_column
		h_tbl = self.h_cell*self.q_row

		coordinates = (0, 0, 0, 0)	# default case for invisible lines

		# TODO unbind the line drawing from Tkinter
		for i in xrange(self.q_column + 1):		# vertical lines
			if self.visible_grid:
				x = self.w_cell*i
				coordinates = (x, 0, x, h_tbl)
			self.coords(self.vert_lines[i], *coordinates)
		for i in xrange(self.q_row + 1):		# horizontal lines
			if self.visible_grid:
				y = self.h_cell*i
				coordinates = (0, y, w_tbl, y)
			self.coords(self.hor_lines[i], *coordinates)

		for item in self.all_items(True):
			item.redraw()

	# <KeyPress>, <KeyRelease>, <Return>, <Up>, <z> and so on
	def add_key_handler(self, event_type, clb):
		def key_handler(event):
			clb(event.keysym)
		self.master.bind(event_type, key_handler)	# The canvas by default does not get keyboard focus.

	# <Button-1>, <Double-Button-3> and so on
	def add_mouse_handler(self, event_type, clb):
		# TODO handle repeating events for Motion

		def mouse_handler(event):
			if not self.initialized:
				return
			x = event.x/self.w_cell
			y = event.y/self.h_cell
			if x<self.q_column and y<self.q_row:
				clb(y, x)

		self.bind(event_type, mouse_handler)

	def cancel_handler(self, event_type):
		self.unbind(event_type)

	def add_or_change_item(self, row, col, type_=None, scale=None, color=None, **tk_options):
		if self.items[row][col] and type_ and self.items[row][col].type != type_:
			self.delete_item(row, col)

		if not self.items[row][col]:
			if type_ not in self.ItemTypes:
				raise RuntimeError('An unknown item type %s, valid values are: %s'%(type_, ', '.join(self.ItemTypes.iterkeys())))
			self.items[row][col] = self.ItemTypes[type_](self, row, col)

		if color:
			tk_options.setdefault('outline', color)
			tk_options.setdefault('fill', color)

		self.items[row][col].redraw(scale, **tk_options)
		return self.items[row][col]

	def add_item(self, row, col, type_, scale=1, color=None, **tk_options):
		if not self.items[row][col]:
			return self.add_or_change_item(row, col, type_, scale, color, **tk_options)

	def change_item(self, row, col, type_=None, scale=None, color=None, **tk_options):
		if self.items[row][col]:
			return self.add_or_change_item(row, col, type_, scale, color, **tk_options)

	def delete_item(self, row, col):
		if self.items[row][col]:
			self.items[row][col].destroy()
			self.items[row][col] = None

	def delete_by_tag(self, tag):
		for item in self.all_items(True):
			if item.tag == tag:
				item.destroy()
				self.items[item.row][item.column] = None

	def get_item(self, row, col):
		return self.items[row][col]

	def what_is_here(self, row, col):
		if self.is_it_inside(row, col) and self.items[row][col]:
			return self.items[row][col].tag

	def is_it_inside(self, row, col):
		if row<0 or row >= self.q_row or col<0 or col>=self.q_column:
			return False
		return True

	def all_items(self, existing_only):
		for item in itertools.chain(*self.items):
			if not existing_only or item:
				yield item

	# the file content is obvious text matrix, each symbol is item
	def load_from_plain_file(self, file_name, add_item_args, error_callback):
		q_col = 0
		item_list = []
		with open(file_name, 'rb') as f:
			for row, line in enumerate(f):
				line = line.strip()
				if line:
					if q_col == 0:
						q_col = len(line)
					if q_col != len(line):
						error_callback('The bad file content, the wrong length of %d string.'%row)
						return
					item_list.append(line)

		self.reset(len(item_list), q_col)
		for r,row in enumerate(item_list):
			for c,sym in enumerate(row):
				if sym == self.EMPTY_ITEM_SYM:
					continue
				if sym not in add_item_args:
					error_callback('Unexpected symbol %s (%d, %d)'%(sym, r, c))
					return
				add_item_arg = add_item_args[sym]
				if isinstance(add_item_arg, dict):
					self.add_item(r, c, **add_item_arg)
				elif isinstance(add_item_arg, tuple):
					self.add_item(r, c, *add_item_arg)
				else:
					raise RuntimeError('Bad add_item_args')
				self.items[r][c].set_tag(sym)

	def save_to_plain_file(self, file_name):
		with open(file_name, 'wb') as f:
			for item_row in self.items:
				for item in item_row:
					if item:
						f.write(item.tag)
					else:
						f.write(self.EMPTY_ITEM_SYM)
				f.write('\n')

# test
if __name__ == '__main__':

	root = Tk()

	class DemoField(Field):
		def __init__(self, master, q_row, q_column):
			Field.__init__(self, master)
			self.reset(q_row, q_column, visible_grid=1)
			self.key = ''
			self.add_key_handler('<KeyPress>', self.key_press)
			self.add_key_handler('<KeyRelease>', self.key_release)
			self.add_mouse_handler('<Button-1>', self.btn1)

		def key_press(self, sym):
			#print sym
			if sym == 'v':
				self.visible_grid = not self.visible_grid
				self.redraw()
			elif sym == 'l':
				self.clear()
			else:
				self.key = sym

		def key_release(self, sym):
			self.key = ''

		def btn1(self, row, col):
			if self.key == 'g':
				self.add_or_change_item(row, col, 'circle', 0.5, 'green')
			elif self.key == 'r':
				self.add_item(row, col, 'rectangle', 1.2, 'blue')
			elif self.key == 'b':
				self.change_item(row, col, color='blue')
			elif self.key == 'equal':
				self.change_item(row, col, scale=self.get_item(row, col).scale+0.1)
			elif self.key == 'minus':
				self.change_item(row, col, scale=self.get_item(row, col).scale-0.1)
			elif self.key == 'd':
				self.delete_item(row, col)
			else:
				self.add_or_change_item(row, col, 'circle', 0.9, 'red')


	fld = DemoField(root, 10, 10)
	root.mainloop()
