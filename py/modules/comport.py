# coding=cp1251
import serial
from thread import start_new_thread
from time import sleep

MAX_READ = 1000		# ������������ ���������� �������� ������� ����� ��������� �� ���
WAIT_ANS = 0.05		# ������� ���� ������ ��� ���������� �������
PAUSE_RD_PERM = 0.1	# ����� ��� ���������� ������ (����� ��� �� ��������� ���������)

class ComportError(Exception):
	pass

class Comport():
	
	def __init__(self, port, baudrate, timeout=0, **kw):
		try: self.uart = serial.Serial(port-1, baudrate=baudrate, timeout=timeout, **kw)
		except serial.SerialException:
			raise ComportError('Cannot open COM%d'%(port,))
		
		self.ctrl_rd_always = 0		# ���������� ����������� ������ (1 ���������)
		self.flg_rd_always = 0		# 1 - ������� ������������ ������
				
	def send(self, cmd):
		''' ������ � ���� ������� cmd'''
		start_new_thread(self.uart.write, (cmd+'\r', ))
		
	def read(self):
		''' ������'''
		return self.uart.read(MAX_READ)
		
	def write(self, s):
		''' ������'''
		return self.uart.write(s)
		
	def start_read_prm(self, callback=None):
		''' ��������� (��� callback ������������) ����������� ������'''
		self.ctrl_rd_always = 1
		
		if callback==None: return	# ������ ������������ ������
		
		def rd_inf(callback):	# ��������� ����������� ������, �������� ��������� ���������� � callback
			while 1:
				sleep(PAUSE_RD_PERM)
				if (self.ctrl_rd_always):
					self.flg_rd_always = 1
					st = self.uart.read(MAX_READ)
					if st!='': callback(st)
				else:
					self.flg_rd_always = 0
		
		start_new_thread(rd_inf, (callback, ))		
		
	def stop_read_prm(self):
		''' ������������� ����������� ������'''
		self.ctrl_rd_always = 0
		while self.flg_rd_always: pass	
		
	def ex_cmd(self, cmd):
		''' ��������� ������� cmd, ���������� ������ �� ����������� ������ ����� ������� (0) � �� (1)'''
		sleep(WAIT_ANS)
		before_cmd = self.read()
		self.uart.write(cmd+'\r')
		sleep(WAIT_ANS)
		return (self.read(), before_cmd)
				
		
if __name__ == '__main__':

	PORT =	2
	BAUD = 	19200
	TIMEOUT = 0
	
	comport = Comport(PORT, BAUD, TIMEOUT)
	
	'''
	# ������� � �������
	while 1:
		comport.write(raw_input('>> '))
		s = comport.read()
		while s:
			print s,
			s = comport.read()
	'''
	

	from Tkinter import *	
	
	root = Tk(); root.title(u'COM-���� ����')
	#
	sbar = Scrollbar(root)
	sbar.pack(side=RIGHT, fill=Y)
	list_r = Listbox(root, yscrollcommand=sbar.set)
	list_r.pack(expand=1,fill=BOTH)
	sbar.config(command=list_r.yview)
	#
	st_inp = StringVar()
	Entry(root, textvariable=st_inp).pack(side=LEFT,expand=1,fill=X)
	#
	
	def send_msg():
		#comport.send(st_inp.get()+'\r')
		comport.stop_read_prm()
		comport.write('inf0\r')
		ans = comport.ex_cmd(st_inp.get())
		print ans
		comport.start_read_prm()
		comport.write('inf1\r')
		
	Button(root,text='send',command=send_msg).pack(side=RIGHT,expand=1,fill=X)
	
	def ins_new_str(st):
		list_r.insert(END, st)
		list_r.see(END)
	
	def check():
		s = comport.read().strip()
		if s: ins_new_str(s)			
		list_r.after(400, check)
	#check()
	
	comport.start_read_prm(ins_new_str)
	
	root.mainloop()
	
