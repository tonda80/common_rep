﻿from Tkinter import *

class Table(Canvas):
	def __init__(self, master, rows, columns, noedit = None, callback = None, **kw):
		kw.setdefault('bg', 'white')
		kw.setdefault('height', rows*20); kw.setdefault('width', columns*20*4); # коффициенты экспериментальные
		Canvas.__init__(self, master, **kw)
		self.bind('<Configure>', self.hndl_configure)
		self.bind('<Button-1>', self.hndl_btn)
		self.bind('<Double-Button-1>', self.hndl_double_btn)
		self.bind('<Key>', self.hndl_key)

		if not noedit: self.focus_set() # move focus to canvas

		self.q_row = rows		# количество строк
		self.q_column = columns	# количество столбцов
		self.q_cell = rows*columns	# количество ячеек

		self.noedit = noedit	# признак noedit
		self.callback = callback	# обратный вызов

		self.pointer_row = 0; self.pointer_col = 0	# позиция указателя

		self.val_el = ['' for i in xrange(rows*columns)]	# значения элементов

		self.lines_col = [self.create_line(0,0,0,0) for i in xrange(self.q_column + 1)]	# список item вертикальных линий

		self.lines_row = [self.create_line(0,0,0,0) for i in xrange(self.q_row + 1)]	# список item горизонтальных линий

		self.markers = {}	# создаем словарь маркеров, ключ - номер соответствующего элемента, значение - item прямоугольника маркера

		self.item_elem = [self.create_text(0, 0, anchor = SE, tag = 'elem') for i in xrange(self.q_cell)]	# список item элементов

	# обработчик двойного клика по элементу
	def hndl_double_btn(self, event):
		item = self.focus()
		if not item: return
		self.select_from(item, 0); self.select_to(item, END)

	# обработчик клика по таблице
	def hndl_btn(self, event):
		if self.noedit: return
		x = self.canvasx(event.x); y = self.canvasy(event.y)
		'''
		for col in self.lines_col:
			if self.coords(col)[0] > x: break
		else: return
		for row in self.lines_row:
			if self.coords(row)[1] > y: break
		else: return
		n_row = self.lines_row.index(row-1); n_col = self.lines_col.index(col-1)
		'''
		n_row = int(y/self.h_cell); n_col = int(x/self.w_cell)

		self.pointer_row = n_row; self.pointer_col = n_col; self.point_to()	# перемещаем указатель

		item = self.item_elem[n_row*self.q_column +n_col]
		if self.type(item) != "text": return; # умудрились не попасть

		if self.focus(): self.end_entry(1)	# если на каком-то другом элементе есть фокус, сбрасываем его редактирование

		self.edit_elem(item)	# готовим к редактированию
		self.icursor(item, "@%d,%d" % (x+self.font[1]/4, y))


	# обработчик нажатия на клавишу
	def hndl_key(self, event):
		if self.noedit: return
		item = self.focus()	# без параметров возвращает текущий фокус
		if not item:	# нет элемента в фокусе. выбираем элемент для редактирования
			if event.keysym == 'Left':
				if self.pointer_col == 0: self.pointer_col = self.q_column - 1
				else: self.pointer_col -= 1
			elif event.keysym == 'Right':
				if self.pointer_col == self.q_column - 1: self.pointer_col = 0
				else: self.pointer_col += 1
			elif event.keysym == 'Up':
				if self.pointer_row == 0: self.pointer_row = self.q_row - 1
				else: self.pointer_row -= 1
			elif event.keysym == 'Down':
				if self.pointer_row == self.q_row - 1: self.pointer_row = 0
				else: self.pointer_row += 1
			elif event.keysym == 'Return':
				item = self.item_elem[self.pointer_row*self.q_column + self.pointer_col]
				self.edit_elem(item)	# готовим к редактированию
				self.icursor(item, END)
			elif event.char >= ' ':
				item = self.item_elem[self.pointer_row*self.q_column + self.pointer_col]
				self.edit_elem(item)	# готовим к редактированию
				self.dchars(item, 0, END)
				self.insert(item, 0, event.char)
				self.icursor(item, END)
			self.point_to()
		else:			# есть элемент в фокусе. редактируем его
			char = event.char
			insert = self.index(item, INSERT)
			flg_selected = self.tk.call(self._w, 'select', 'item')	# признак выделения
			if event.keysym == 'Delete':
				if flg_selected: self.dchars(item, 0, END)
				else: self.dchars(item, insert)
			elif event.keysym == 'BackSpace':
				if flg_selected: self.dchars(item, 0, END)
				elif insert != 0: self.dchars(item, insert-1)
			elif event.keysym == 'Left': self.icursor(item, insert - 1)
			elif event.keysym == 'Right': self.icursor(item, insert + 1)
			elif event.keysym == 'Home': self.icursor(item, 0)
			elif event.keysym == 'End': self.icursor(item, END)
			elif event.keysym == 'Escape': self.end_entry(0)
			elif event.keysym == 'Return': self.end_entry(1)
			elif char >= ' ':	# печатаемые символы (?)
				if flg_selected: self.dchars(item, 0, END)
				self.insert(item, insert, char)
				self.highlight()

			self.select_clear()	# снимаем выделение

	# обработчик события configure
	def hndl_configure(self, event):
		self.w_cell = self.winfo_width()/self.q_column	# ширина ячейки
		self.h_cell = self.winfo_height()/self.q_row	# высота ячейки

		w_tbl = self.w_cell*self.q_column	# широта таблицы
		h_tbl = self.h_cell*self.q_row		# высота таблицы

		coord_elem = []		#  элемент списка - список из коорд. X, коорд. Y тектовых элементов
		for r in xrange(self.q_row):	# заполняем список
			for c in xrange(self.q_column):
				coord_elem.append( ((c+1)*self.w_cell, (r+1)*self.h_cell) )

		self.font = ('Courier', int(min(self.h_cell*0.6, self.w_cell*0.6)), 'bold')	# шрифт таблицы

		for i in xrange(self.q_column + 1): x = self.w_cell*i; self.coords(self.lines_col[i], x, 0, x, h_tbl)	# изменяем столбцы

		for i in xrange(self.q_row + 1): y = self.h_cell*i; self.coords(self.lines_row[i], 0, y, w_tbl, y)		# изменяем строки

		for i in xrange(self.q_cell):		# изменяем положение и вид элементов
			self.coords(self.item_elem[i], coord_elem[i])
			self.print_text(self.item_elem[i])

		for mark in self.markers: self.__draw_marker(mark)	# перерисовываем маркеры
		self.point_to()		# перерисовываем указатель
		self.highlight()	# перерисовываем подсветку ввода


	# готовит элемент к редактированию
	def edit_elem(self, item):
		self.focus_set() # move focus to canvas
		self.focus(item) # set focus to text item
		self.itemconfig(item, text = self.__get_val_el(item), fill = 'red')
		self.highlight()

	# mark focused item.  note that this code recreates the rectangle for each update, but that's fast enough for this case.
	def highlight(self):
		self.delete("highlight")	# подсветка только 1
		item = self.focus()
		if not item: return
		bbox = self.bbox(item)
		r = self.create_rectangle(bbox, fill="white", tag="highlight")	# зачем-то было if bbox:
		self.lower(r, item)

	# рисует указатель
	def point_to(self):
		if self.noedit: return
		self.delete("pointer")	# подсветка только 1
		x1 = self.pointer_col*self.w_cell; y1 = self.pointer_row*self.h_cell
		coord_pointer = x1, y1, x1 + self.w_cell, y1 + self.h_cell	# координаты прямоугольника указателя
		r = self.create_rectangle(coord_pointer, width=4, tag="pointer")	# рисуем указатель
		self.lower(r, 'elem')

	# конец ввода
	def end_entry(self, flg_apply):	# принимаем редактирование, если передается True
		item = int(self.focus())
		if not item: return
		if flg_apply: self.__set_val_el(item, self.itemcget(item, 'text'))
		self.print_text(item)
		self.select_clear()			# снимаем выделение
		self.delete("highlight")	# убрали подсветку
		self.focus('')				# сняли фокус

	# отображение текста
	def print_text(self, item):
		text_el = self.__get_val_el(item)
		try: q_el_place = int(self.w_cell/(self.font[1]*0.84))	# сколько символов помещается в ячейке
		except (ZeroDivisionError, AttributeError): return
		if q_el_place < len(text_el) : self.itemconfig(item, text = text_el[:q_el_place], fill = 'blue', font = self.font)	# печатаем обрезанные значения
		else: self.itemconfig(item, text = text_el, fill = 'black', font = self.font)										# печатаем нормальные значения

	# возвращает значение элемента по item
	def __get_val_el(self, item):
		return self.val_el[self.item_elem.index(item)]

	# устанавливает значение элемента  по item
	def __set_val_el(self, item, text):
		num = self.item_elem.index(item)
		self.val_el[num] = text
		if self.callback: self.callback(num, text, self)	# сообщаем об изменении num-го элемента на text

	# возвращает значение элемента
	def get_elem(self, num):
		if num < 0 or num >= self.q_cell: return
		return self.val_el[num]

	# устанавливает значение элемента
	def set_elem(self, num, value):
		if num < 0 or num >= self.q_cell: return
		self.val_el[num] = value
		self.print_text(self.item_elem[num])

	# устанавливает маркер на элементе
	def set_marker(self, num_elem, color = 'red'):
		if self.markers.has_key(num_elem) or num_elem>=self.q_cell: return	# если есть маркер то уходим. Возможно надо self.itemconfig(self.markers[num_elem], fill = color)
		self.markers[num_elem] = self.create_rectangle(0,0,0,0, fill=color, tag='marker')	# создаем маркер и добавляем в словарь
		self.__draw_marker(num_elem)
	# рисуем маркер
	def __draw_marker(self, num_elem):
		row, column = divmod(num_elem, self.q_column);
		x1 = column*self.w_cell; y1 = row*self.h_cell
		self.coords(self.markers[num_elem], x1, y1, x1 + self.w_cell, y1 + self.h_cell)	# меняем координаты прямоугольника маркера
		self.lower(self.markers[num_elem], 'pointer')
	# удаляем маркер
	def del_marker(self, num_elem):
		try: self.delete(self.markers[num_elem])
		except KeyError: return	# если нет маркера то уходим
		del self.markers[num_elem]

# test
if __name__ == '__main__':

	def test():
		if strvar.get()[0:1]=='c':
			tbl.set_marker(int(strvar.get()[1:]))
		else:
			tbl.del_marker(int(strvar.get()))


	root = Tk()

	tbl = Table(root, 4, 8)#, insertbackground = 'green',insertborderwidth=3)
	tbl.pack(fill=BOTH, expand=YES)

	for i in xrange(7*8): tbl.set_elem(i,str(i))

	Button(root, text = 'Test', command = test).pack()
	strvar = StringVar(value=1); Entry(root, textvariable = strvar).pack()

	root.mainloop()
