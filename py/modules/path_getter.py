# Main goal: to return an absolute path from argument or from tk dialog

import os
from sys import argv
import Tkinter
import tkFileDialog

class PathGetterError(RuntimeError):
	pass

class PathGetter:
	def __init__(self, path_is_dir, check_first_arg = True, use_tk_dialog = True):
		self.path_is_dir = path_is_dir
		self.check_first_arg = check_first_arg
		self.use_tk_dialog = use_tk_dialog

	def __check_existence(self, path):
		if self.path_is_dir:
			return os.path.isdir(path)
		else:
			return os.path.isfile(path)

	def get_path(self):
		path = ''
		if self.check_first_arg and len(argv) > 1:
			path = os.path.abspath(argv[1]).decode('utf8')	# see http://stackoverflow.com/questions/10180765/open-file-with-a-unicode-filename
			if self.__check_existence(path):
				return path
			else:
				raise PathGetterError('Wrong path from argument: %s'%argv[1])

		if self.use_tk_dialog:
			w = Tkinter.Tk(); w.withdraw()	# masking window
			if self.path_is_dir:
				path = tkFileDialog.askdirectory()
			else:
				path = tkFileDialog.askopenfilename()

			if path:
				return path					# tkFileDialog methods return unicode string already
			raise PathGetterError('Cannot get a path')

	def get_path_no_throw(self):
		try:
			path = self.get_path()
		except PathGetterError:
			if self.path_is_dir and curr_dir_as_default:
				return os.getcwd().decode('utf8')
			return u''
		return path

# useful helpers

def get_file_name():
	return PathGetter(0).get_path()

def get_dir_name():
	return PathGetter(1).get_path()

def get_file_name_no_throw():
	return PathGetter(0).get_path_no_throw()

def get_dir_name_no_throw():
	return PathGetter(1).get_path_no_throw()

if __name__ == '__main__':
	print 'path:', get_file_name_no_throw()
	print 'path:', get_dir_name_no_throw()
