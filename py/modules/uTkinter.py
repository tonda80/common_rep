﻿#!/usr/bin/python

from Tkinter import *
import ttk

class uCallback:
	'The callback with the label and the source widget'
	def __init__(self, clb, *args1, **args2):
		self.clb = clb
		self.args1 = args1
		self.args2 = args2
		self.widget = None

	def setWidget(self, widget):
		self.widget = widget

	@staticmethod
	def trySetWidget(obj, wid):
		if isinstance(obj, uCallback):
			obj.setWidget(wid)

	def __call__(self, *unusedArgs):
		self.clb(self.widget, *self.args1, **self.args2)

class VariableWrapper:
	'Successors use self.var and methods'
	def __init__(self, ValueType, defaultValue):
		self.var = ValueType(value=defaultValue)

	def setVar(self, v):
		self.var.set(v)

	def getVar(self):
		return self.var.get()

class GeometryManager:
	def putOn(self, options, put_obj = None):
		if options == None:
			options = {}
		if put_obj == None:
			put_obj = self

		grid_signs = ('row', 'column')
		place_signs = ('x', 'y', 'relx', 'rely')

		def contains_any_from(seq1, seq2):
			# seq1 should be less
			# other variant => any(x in a for x in b)
			for x in seq1:
				if x in seq2:
					return True
			return False

		if contains_any_from(grid_signs, options.iterkeys()):
			self.gmType = 'grid'
			self.setDefaultGridArgs(options)
			put_obj.grid(**options)
		elif contains_any_from(place_signs, options.iterkeys()):
			self.gmType = 'place'
			self.setDefaultPlaceArgs(options)
			put_obj.place(**options)
		else:															# pack is default
			self.gmType = 'pack'
			self.setDefaultPackArgs(options)
			put_obj.pack(**options)

	def setDefaultPackArgs(self, options): pass
	def setDefaultGridArgs(self, options): pass
	def setDefaultPlaceArgs(self, options): pass

# gmArgs = {'expand':YES, 'fill':BOTH}
class uFrame(Frame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)

	def __init__(self, master, gmArgs = None, **args):
		self.setDefaultArgs(args)
		Frame.__init__(self, master, **args)
		self.putOn(gmArgs)

class uCanvas(Canvas, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)
		args.setdefault('background', 'white')

	def setDefaultPackArgs(self, options):
		options.setdefault('expand', YES)
		options.setdefault('fill', BOTH)

	def __init__(self, master, gmArgs = None, **args):
		self.setDefaultArgs(args)
		Canvas.__init__(self, master, **args)
		self.putOn(gmArgs)

# pack inside! i.e. any other managers are forbidden for the master widget!
def _set_scrolling(scrollmode, master, widget, bg=None):
		scrollmode = scrollmode.lower()
		assert scrollmode in ('x', 'y', 'xy', 'yx', ''), 'Wrong value of scrollmode: %s'%scrollmode

		x_scroll = y_scroll = None
		if 'x' in scrollmode:
			x_scroll = Scrollbar(master, command=widget.xview, orient=HORIZONTAL, background=bg)
			x_scroll.pack(side=BOTTOM, fill=X)
			widget.configure(xscrollcommand = x_scroll.set)
		if 'y' in scrollmode:
			y_scroll = Scrollbar(master, command=widget.yview, orient=VERTICAL, background=bg)
			y_scroll.pack(side=RIGHT, fill=Y)
			widget.configure(yscrollcommand = y_scroll.set)

		return (x_scroll, y_scroll)

class uScrollFrame(Frame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)

	def __init__(self, master, scrollmode, gmArgs = None, **args):
		self.setDefaultArgs(args)
		if 'bg' in args:
			bg = args['bg']
		elif 'background' in args:
			bg = args['background']
		else:
			bg = master.cget('bg')

		_base_frame = Frame(master, background=bg, **args)
		self.putOn(gmArgs, _base_frame)

		self._canvas = Canvas(_base_frame,  background=bg)

		_set_scrolling(scrollmode, _base_frame, self._canvas, bg)

		self._canvas.pack(expand='YES', fill=BOTH)

		Frame.__init__(self, self._canvas, background=bg)
		self._canvas.create_window((0,0), window=self, anchor='nw')
		self.bind("<Configure>", self._configure_clb)

	def _configure_clb(self, event):
		self._canvas.configure(scrollregion=self._canvas.bbox(ALL), width=20, height=20)

class uLabelFrame(LabelFrame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)

	def __init__(self, master, label, gmArgs = None, **args):
		self.setDefaultArgs(args)
		LabelFrame.__init__(self, master, text = label, **args)
		self.putOn(gmArgs)

class uButton(Button, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('width', 10)
		args.setdefault('takefocus', OFF)

	def __init__(self, master, command, text = 'Button', gmArgs = None, **args):
		self.setDefaultArgs(args)
		uCallback.trySetWidget(command, self)
		Button.__init__(self, master, text = text, command = command, **args)
		self.putOn(gmArgs)

class uEntry(Entry, GeometryManager, VariableWrapper):
	def setDefaultArgs(self, args):
		args.setdefault('width', 20)

	def __init__(self, master, defaultValue = '', ValueType = StringVar, callback = None, gmArgs = None, **args):
		self.setDefaultArgs(args)
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Entry.__init__(self, master, textvariable = self.var, **args)

		self.putOn(gmArgs)

		uCallback.trySetWidget(callback, self)
		if callback is not None:
			self.var.trace('w', callback)

		#self.var.trace('w', self._internal_w_callback)	# will be called earlier
		#def _internal_w_callback(self, *args):

class uScale(Scale, GeometryManager, VariableWrapper):
	def __init__(self, master, from_, to, resolution=1, command = None, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Scale.__init__(self, master, from_ = from_, to = to, resolution = resolution, command = command, variable = self.var, **args)

		self.putOn(gmArgs)

		uCallback.trySetWidget(command, self)
# Scale selected options
# label, length, orient ('h', 'v'), resolution, showvalue, sliderlength, sliderrelief, width

# ATTENTION! 'command' is for the button actions, 'callback' is for any
class uSpinbox(Spinbox, GeometryManager, VariableWrapper):
	def __init__(self, master, from_, to, increment=1.0, callback = None, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Spinbox.__init__(self, master, from_ = from_, to = to, increment = increment, textvariable = self.var, **args)

		self.putOn(gmArgs)

		uCallback.trySetWidget(callback, self)
		if callback is not None:
			self.var.trace('w', callback)

class uCheckbutton(Checkbutton, GeometryManager, VariableWrapper):
	def __init__(self, master, text = '', callback = None, defaultValue = False, gmArgs = None, **args):
		VariableWrapper.__init__(self, BooleanVar, defaultValue)
		uCallback.trySetWidget(callback, self)
		Checkbutton.__init__(self, master, variable = self.var, command = callback, text = text, **args)

		self.putOn(gmArgs)

class uOptionMenu(OptionMenu, GeometryManager, VariableWrapper):
	def __init__(self, master, options, command = None, ValueType = StringVar, gmArgs = None):
		VariableWrapper.__init__(self, ValueType, options[0])
		uCallback.trySetWidget(command, self)
		OptionMenu.__init__(self, master, self.var, *options, command = command)	# 'command' is the only supported keyword (OptionMenu.__init__)

		self.putOn(gmArgs)

		self.options = options	# не нашел такого списка в коде Menu.add_command

	def get_option(self):
		return self.var.get()
	def get_option_number(self):
		return self.options.index(self.var.get())

class uText(Text, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('font', 'Courier 10')
		args.setdefault('height', 10)

	def setDefaultPackArgs(self, packArgs):
		packArgs.setdefault('expand', YES)
		packArgs.setdefault('fill', BOTH)

	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		self.setDefaultArgs(args)
		Text.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	def get_state(self):
		return self.config()['state'][-1]	# easier?

	def get_all(self):
		return self.get('1.0', END)

	def append(self, txt):
		self.insert(END, txt)

	class EditorContext:
		def __init__(self, utext):
			self.utext = utext
		def __enter__(self):
			self.actual_state = self.utext.get_state()
			self.utext.config(state='normal')
		def __exit__(self, *a):
			self.utext.config(state=self.actual_state)

	def clear_all(self):
		with uText.EditorContext(self):
			self.delete('1.0', END)	# Line numbers start at 1, while column numbers start at 0
			# TODO delete marks?

class uLabel(Label, GeometryManager):
	def __init__(self, master, text, gmArgs = None, **args):
		Label.__init__(self, master, text = text, **args)
		self.putOn(gmArgs)

class uMutableLabel(Label, GeometryManager, VariableWrapper):
	def __init__(self, master, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Label.__init__(self, master, textvariable = self.var, **args)
		self.putOn(gmArgs)

class RadiobuttonGroup(VariableWrapper):
	def __init__(self, master, values, command = None, title = '', texts = (), intSide = TOP, ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, values[0])

		uCallback.trySetWidget(command, self)

		self.frame = uLabelFrame(master, title, gmArgs)

		self.radiobuttons = []
		for value, text in map(None, values, texts):
			if value is None:
				break
			if text is None:
				text = str(value)
			self.radiobuttons.append(Radiobutton(self.frame, text=text, variable=self.var, value=value, command=command, **args))
			self.radiobuttons[-1].pack(side=intSide, anchor=W)

class uListbox(Listbox, GeometryManager):
	def setDefaultPackArgs(self, options):
		options.setdefault('expand', YES)
		options.setdefault('fill', Y)

	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		Listbox.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	def setMode(self, mode):
		# SINGLE, BROWSE (useful, default), MULTIPLE, EXTENDED (useful)
		self.config(selectmode=mode)

	def addMouseCallback(self, clb, event_type):
		# <Double-Button-1> etc
		def int_callback(ev):
			# self.index("@%d,%d"%(ev.x, ev.y)), self.get(ACTIVE)
			i = self.nearest(ev.y)
			if i >= 0:
				clb(i, self.get(i))
		self.bind(event_type, int_callback)

	def append(self, item):
		self.insert(END, item)

	def clear(self):
		self.delete(0, END)

# https://docs.python.org/2/library/ttk.html#treeview
class uTreeView(ttk.Treeview, GeometryManager):
	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		ttk.Treeview.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	# удаляем всех потомков item-а
	def clear_item(self, iid):
		self.delete(*self.get_children(iid))

# https://docs.python.org/2/library/ttk.html#separator
class uSeparator(ttk.Separator, GeometryManager):
	def __init__(self, master, horizontal=True, gmArgs = None):
		if horizontal:
			orient = 'horizontal'
			gmArgs.setdefault('fill', X)
		else:
			orient = 'vertical'
			gmArgs.setdefault('fill', Y)

		ttk.Separator.__init__(self, master, orient=orient)
		self.putOn(gmArgs)

#~ class u(, GeometryManager):
	#~ def setDefaultArgs(self, args):
	#~
	#~ def setDefaultPackArgs(self, packArgs):
#~
	#~ def __init__(self, master, , gmArgs = None, **args):
		#~ self.setDefaultArgs(args)
		#~ .__init__(self, master, , **args)
		#~ self.putOn(gmArgs)

class uToplevel(Toplevel):
	def __init__(self, title=None):
		Toplevel.__init__(self)
		self.title(title)

class PopupMenu(Menu):
	def __init__(self, master, on_master_rb=None):
		Menu.__init__(self, master, tearoff=0)
		self.label_font = None
		master.bind("<Button-3>", self.__popup)
		self.on_master_rb = on_master_rb		# для передачи <Button-3> события

	def __popup(self, event):
		if self.on_master_rb:
			self.on_master_rb(event)
		self.post(event.x_root, event.y_root)

	def add_callback(self, labels, callback, index=None):		# labels may be a string or a set of strings
		if isinstance(labels, basestring):
			labels = (labels, )

		for label in labels:
			def p(label=label): # closure in the loop (https://stackoverflow.com/questions/233673/lexical-closures-in-python)
				callback(label)
			if index is None:
				self.add_command(label=label, command=p, font=self.label_font)
			else:
				self.insert_command(index, label=label, command=p, font=self.label_font)

class StatusBar:
	def __init__(self, master, font = 'System 11'):
		f = uFrame(master, height=30, borderwidth=2, relief=GROOVE, gmArgs={'side':BOTTOM, 'fill':X, 'expand':NO})
		f.pack_propagate(False)		# запрещаем растягивание длинными сообщениями
		# For some reason "expand - YES" places the bar into a window middle
		self.infoLabel = uMutableLabel(f, font = font, gmArgs={'side':LEFT, 'pady':0, 'padx':3})
		self.rightInfoLabel = uMutableLabel(f, font = font, gmArgs={'side':RIGHT, 'pady':0, 'padx':3})

	def set(self, st):
		self.infoLabel.setVar(st)

	def add(self, st):
		self.infoLabel.setVar(self.infoLabel.getVar() + st)

	def set_r(self, st):
		self.rightInfoLabel.setVar(st)

	def add_r(self, st):
		self.rightInfoLabel.setVar(self.infoLabel.getVar() + st)

class uTk(Tk):
	def __init__(self, title='', minw=640, minh=480, createStatus=True):
		Tk.__init__(self)

		self.title(title)
		self.minsize(minw, minh)

		if createStatus:
			self.statusBar = StatusBar(self)

		self.delete_callback = lambda:None
		self.delete_args = ()

	# в Tk нет метода с таким именем
	# при destroy не вызывается заданное в WM_DELETE_WINDOW
	def close(self):
		self.delete_callback(*self.delete_args)
		self.destroy()

	def set_delete_callback(self, callback, args=()):
		self.delete_callback = callback
		self.delete_args = args
		self.protocol("WM_DELETE_WINDOW", self.close)

	# FUB (frequently used bindings)
	def bind_return(self, callback):
		self.bind('<Return>', lambda e: callback())
	def set_exit_by_escape(self):
		self.bind('<Escape>', lambda e: self.close())
	def bind_left_click(self, callback):
		self.bind('<Button-1>', lambda e: callback(e))


def placeWindow(wnd, x, y, width, height):
	wnd.geometry('%dx%d+%d+%d'%(width, height, x, y))

def placeWindowTo(wnd, x, y):
	wnd.geometry('+%d+%d'%(x,y))

def placeWindowToCenter(wnd, dX = 0, dY = 0):
	x = wnd.winfo_screenwidth()/2 - dX
	y = wnd.winfo_screenheight()/2 - dY
	wnd.geometry('+%d+%d'%(x,y))

def setWindowSize(wnd, w , h):
	wnd.geometry('%dx%d'%(w, h))


if __name__ == '__main__':

	def pp(a = None):
		print 'Something pressed', a, type(a)

	def pp2(widget, *a1, **a2):
		print '%s from %s with %s %s'%(widget.getVar(), widget.__class__, a1, a2)

	w = Tk()

	f = uFrame(w, gmArgs = {'expand':YES, 'fill':BOTH})

	uButton(f, pp, 'Press me')

	e1 = uEntry(f, 12.34, DoubleVar, uCallback(pp2, 'e1'))
	e2 = uEntry(f, 1, callback=uCallback(pp2, 'e2'))

	cb1 = uCheckbutton(f, 'Check me', pp)
	cb2 = uCheckbutton(f, 'Named callback', uCallback(pp2, 'cb2'))
	cb3 = uCheckbutton(f, 'Named callback 2', uCallback(pp2, 'cb3'))

	uOptionMenu(f, (1, 2, 3), uCallback(pp2, 'opt menu 1'))

	f2 = uLabelFrame(w, 'Text', borderwidth=4, gmArgs = {'expand':YES, 'fill':BOTH})
	t = uText(f2, scrollmode='y')
	uButton(f2, t.clear_all, 'clear')
	b_tt = uButton(f2, None, 'toggle state')
	def toggle_text_state():
		new_state = 'normal' if t.get_state()=='disabled' else 'disabled'
		t.config(state=new_state)
		b_tt.config(text='text is '+new_state)
	b_tt.config(command=toggle_text_state)

	uLabel(f, 'Label')

	uOptionMenu(f, ('red', 'green', 'yellow'), lambda s: ml.setVar(s))
	ml = uMutableLabel(f, 'uMutableLabel')
	uButton(f, uCallback(lambda w, l: ml.setVar(l), ''), 'reset')	# uCallback just for demo

	RadiobuttonGroup(f, ('var1', 'other var'), uCallback(pp2, 'rb1'))
	rbg2 = RadiobuttonGroup(f, range(5), uCallback(pp2, 'rb2'), 'Numbers', ('zero', 'one'), LEFT)
	def rb_test(): print rbg2.getVar()
	uButton(f, rb_test, 'rb_test')

	uScale(w, 1.0, 10.0, 0.1, command=uCallback(pp2), orient='h', label = 'Scale', gmArgs={'expand':YES, 'fill':X})

	uSpinbox(w, 1.0, 10.0, 0.1, callback=uCallback(pp2))

	def create_toplevel(title=None):
		tl = uToplevel(title)
		tl.protocol("WM_DELETE_WINDOW", w.destroy)
		return tl

	w2 = create_toplevel('w2')
	StatusBar(w2).set('Some very helpful line')
	setWindowSize(w2, 300, 400)
	placeWindowToCenter(w2)

	cnt = 0
	def add_btn():
		global cnt; uButton(scrFr, pp, 'Press %d'%cnt, gmArgs = {'pady':8}); cnt += 1
	uButton(w2, add_btn)
	scrFr = uScrollFrame(w2, 'y', gmArgs = {'expand':YES, 'fill':BOTH})
	for i in xrange(30):
		uButton(scrFr, pp, 'Press')

	w3 = create_toplevel('w3')
	lbx = uListbox(w3, 'yx', font=("Helvetica", 12))
	def p_lb(i, s): print i, s
	lbx.addMouseCallback(p_lb, '<Double-Button-1>')
	for s in 'one two three addCallback somethingelse'.split() + map(str, xrange(30)):
		lbx.append(s)
	lbx.append('really long string '*20)

	uButton(w3, lbx.clear, 'Clear')
	#print lbx.config()

	popup_menu = PopupMenu(lbx)
	popup_menu.add_callback('test1', pp)
	popup_menu.add_callback('test2', pp)
	popup_menu.add_callback(['test3','test4','test5'], pp)

	w4 = create_toplevel('w4')
	tv = uTreeView(w4, 'xy', gmArgs={'expand':YES, 'fill':BOTH})
	columns = ['column%d'%i for i in xrange(5)]
	tv['columns'] = columns		# тут задаем id столбцов, а чуть ниже текст
	for c in columns:
		tv.heading(c, text=c)
		tv.column(c, minwidth=100)
	#tree.configure(selectmode = 'none')	# none extended browse(default)
	tv.tag_configure('special_tag', foreground='red')
	tv.tag_configure('another_tag', font=('Symbol', 10, 'bold'))
	for i in xrange(30):	# элементы верхнего уровня
		iid = 'item%d'%i	# это можно не передавать, тогда само сгенерирует и вернет из insert
		tv.insert('', 'end', iid=iid, text=iid, values=['column value %d %d'%(i,j) for j in xrange(len(columns))])
	tv.item('item2', tags=['special_tag'])
	tv.item('item1', tags=['another_tag'])
	for item in ('item4', 'item8', 'item10'):
		tv.insert(item, 'end', text='subitem of %s'%item, values=map(str, xrange(3)), tags=['special_tag'])

	w.mainloop()

# Tkinter memo
# ------------
# pack options: side, expand, fill, ipadx, ipady, padx, pady, anchor
#
# self.root.title(u'Title')
# self.root.bind('<Key>', self.hndl_key)
# self.root.minsize(500, 200)
# self.root.attributes('-fullscreen', True)
# geometry('%dx%d+%d+%d'%(w,h,x,y))
# packArgs.setdefault('side', TOP)
# packArgs.setdefault('fill', X)
# args.setdefault('font', 'Courier 10')
# args.setdefault('indicatoron', True)
# root.protocol("WM_DELETE_WINDOW", self.delete_callback)	AND root.destroy() into callback
# root.protocol("WM_TAKE_FOCUS", pp)
# root.iconify()
# root.wm_attributes('-topmost', 1)	always on top
# colors
#	RRGGBB tk_rgb = "#%02x%02x%02x" % (128, 192, 200)
#	'red', 'green', 'blue', ....   for Windows SystemActiveBorder, SystemActiveCaption,
# font
#	("Helvetica", 10, "bold italic"), ("Symbol", 8)
#	tkFont.Font(family="Times", size=10, weight=tkFont.BOLD)	more options - slant, underline, overstrike
#	system fonts

# tkMessageBox.askyesno('','Are you sure?', parent = root)
# tkSimpleDialog.askstring('title', 'Enter smth')

# btn['text']= 'New button name' OR b.config(text = 'New name')
#

# <binding events>
# http://effbot.org/tkinterbook/tkinter-events-and-bindings.htm
# http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/event-types.html
# widget.bind(event_type, callback)
# Button-1, ButtonRelease-1, Double-Button-1 (2 middle, 3 right)
# Motion, B1-Motion, ..
# Enter, Leave (the mouse entered/left the widget)
# FocusIn, FocusOut
# Configure
# KeyPress, KeyRelease (for any key)
# Shift-Up
# any letter (with or without <>)
# Return (the Enter key), Cancel(the Break key на линуксе не работает), BackSpace, Tab, Shift_L , Control_L , Alt_L (any key), Pause,
#   Caps_Lock, Escape, Prior (Page Up), Next (Page Down), End, Home, Left, Up, Right, Down, Print, Insert, Delete,
#   F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, Num_Lock, and Scroll_Lock.
#
# event attributes
# for a in (a_ for a_ in dir(event) if a_[0]!='_'): print a, getattr(event, a)
