# coding=cp1251
from Tkinter import *
from math import *
import itertools

TAG_GRAPH = 'graph'
TAG_LINE = 'line'
TAG_LABEL = 'label'
TAG_AXIS = 'axis'
TAG_TEXT = 'text'

class Graph(Canvas):
	def __init__(self, master, **kw):
		# создаем канвас и скроллбары
		scr_y = Scrollbar(master); scr_x = Scrollbar(master, orient=HORIZONTAL)
		scr_y.pack(side=RIGHT, fill=Y); scr_x.pack(side=BOTTOM, fill=X)
		kw.setdefault('bg', 'white')
		Canvas.__init__(self, master, yscrollcommand=scr_y.set, xscrollcommand=scr_x.set, **kw)
		scr_y.config(command=self.yview); scr_x.config(command=self.xview)

		# обработчики событий
		self.bind('<Configure>', self.hndl_configure)
		self.bind('<ButtonPress-1>', self.hndl_button1_press)
		self.bind('<ButtonRelease-1>', self.hndl_button1_rel)
		self.bind('<ButtonPress-3>', self.hndl_button3_press)
		self.bind('<MouseWheel>', self.hndl_wheel)
		self.bind('<Motion>', self.hndl_motion)
		self.bind('<Leave>', self.hndl_leave)
		self.bind('<Key>', self.hndl_keypress)

		self.x_min = self.x_max = self.y_min = self.y_max = 0	# экстремумы точек графика

		self.x0 = self.y0 = 0.0	# смещение координат графика и канваса (точки канваса соответствующие нулю осей графика)
		self.kx = self.ky = 1.0	# коэффициент растяжения координат графика на канвас

		self.create_text(0, 0, tags = TAG_LABEL, anchor=SW)	# создаем метку
		self.iter_anchor_label = itertools.cycle((W,NW,CENTER,N,NE,E,SE,S,SW))

		self.w = self.h = None

		self.btn1_pressed = False

		# оси координат, в списках кортеж (координата, ширина, цвет)
		self.x_axis = []
		self.y_axis = []

	def hndl_keypress(self, event):
		#print event.char, event.keysym, event.keysym_num, event.keycode
		key = event.keysym
		d = 0.2
		if key == 'l':
			self.itemconfig(TAG_LABEL, anchor = self.iter_anchor_label.next())
		elif key == 'minus':
			self.scale_graph(1-d, 1-d)
		elif key == 'equal':
			self.scale_graph(1+d, 1+d)
		elif key == 'Down':
				self.scale_graph(1, 1-d)
		elif key == 'Up':
			self.scale_graph(1, 1+d)
		elif key == 'Left':
			self.scale_graph(1-d, 1)
		elif key == 'Right':
			self.scale_graph(1+d, 1)
		elif key == 'BackSpace':
			self.scale_graph(1/self.kx, 1/self.ky)
			self.view_all(prop=True)

	def hndl_configure(self, event):	# обработчик изменения размера
		self.config(scrollregion=self.bbox(TAG_GRAPH))	# все графики в поле видимости
		self.w = self.winfo_width(); self.h = self.winfo_height()

		self.redraw_axis()

	def hndl_button1_press(self, event):	# обработчик нажатия 1 кнопки мыши
		self.btn1_pressed = True
		self.focus_set()
		self.x_press, self.y_press = self.canvasx(event.x), self.canvasy(event.y)
	def hndl_button1_rel(self, event):	# обработчик отпускания 1 кнопки мыши
		self.btn1_pressed = False

	def hndl_button3_press(self, event):	# обработчик нажатия 3 кнопки мыши
		self.view_all(prop=True)

	def hndl_motion(self, event):	# обработчик перемещения мыши
		# работаем с меткой
		x = self.canvasx(event.x); y = self.canvasy(event.y)
		self.coords(TAG_LABEL, x, y)
		self.itemconfig(TAG_LABEL, text = '%.3f, %.3f'%(self.x_graph(x), self.y_graph(y)))
		# перемещаем графики
		if self.btn1_pressed:
			self.move_graph(x-self.x_press, y-self.y_press)
			self.x_press, self.y_press = x, y
	def hndl_leave(self, event):	# обработчик выхода мыши
		self.itemconfig(TAG_LABEL, text = '')

	def hndl_wheel(self, event):	# прокрутка колеса мыши
		k = 1 + event.delta/1200.0
		#print 'hndl_wheel', k		# не работает на линуксе или конкретной мыши?
		self.scale_graph(k, k)

	def test_extr(self, x, y):	# проверка на экстремум
		if x < self.x_min: self.x_min = x
		elif x > self.x_max: self.x_max = x
		if y < self.y_min: self.y_min = y
		elif y > self.y_max: self.y_max = y

	def x_canv(self, x):		# преобразует координату x графика в координаты канваса
		return x*self.kx + self.x0
	def y_canv(self, y):		# преобразует координату y графика в координаты канваса
		return -y*self.ky + self.y0
	def x_graph(self, x):	# преобразует координату x канваса в координаты графика
		return (x - self.x0)/self.kx
	def y_graph(self, y):	# преобразует координату y канваса в координаты графика
		return (y - self.y0)/-self.ky

	def draw(self, func, x0, l, quant, color='red', tag=''):		# рисует график функции func(x) [x0, x0+l] по quant+1 точкам цветом color
		discr = float(l)/quant	# dx
		x_start = float(x0)
		y_start = func(x_start)
		self.test_extr(x_start, y_start)

		x = x_start
		for i in xrange(1, quant+1):
			x += discr
			y = func(x)
			self.create_line(self.x_canv(x_start), self.y_canv(y_start), self.x_canv(x), self.y_canv(y), fill = color, tags=(TAG_LINE,TAG_GRAPH,tag))
			self.test_extr(x, y)
			x_start = x; y_start = y
		self.lower(TAG_LINE)

	def draw_par(self, func_x, func_y, t0, l, quant, color='red', tag=''):		# рисует график параметрической функции задающейся func_x(t) и func_y(t) на интервале [t0, t0+l] по quant+1 точкам цветом color
		discr = float(l)/quant	# dt
		x_start = func_x(t0)
		y_start = func_y(t0)
		self.test_extr(x_start, y_start)

		t = t0
		for i in xrange(1, quant+1):
			t += discr
			x = func_x(t)
			y = func_y(t)
			self.create_line(self.x_canv(x_start), self.y_canv(y_start), self.x_canv(x), self.y_canv(y), fill = color, tags=(TAG_LINE,TAG_GRAPH,tag))
			self.test_extr(x, y)
			x_start = x; y_start = y
		self.lower(TAG_LINE)

	def add_text(self, x, y, text, font=None):
		self.create_text(self.x_canv(x), self.y_canv(y), text=text, font=font, tags=(TAG_TEXT, TAG_GRAPH))

	def clear(self, tag):	# убираем элементы с тегом tag
		self.delete(tag)
	def clear_all(self):	# очищаем канвас
		self.delete(ALL)

	def scale_graph(self, kx=1, ky=1):	# растянуть график в kx по x и в ky по y
		self.scale(TAG_GRAPH, self.x0, self.y0, kx, ky)
		self.ky *= ky; self.kx *= kx
		self.config(scrollregion=self.bbox(TAG_GRAPH))# все в поле видимости

		#TODO font scaling??

		self.redraw_axis()

	def move_graph(self, dx=0, dy=0):	# сместить график на dy по y и на dx по x
		self.move(TAG_GRAPH, dx, dy)
		self.y0 += dy; self.x0 += dx
		self.config(scrollregion=self.bbox(TAG_GRAPH))# все в поле видимости

		self.redraw_axis()

	def view_all(self, prop=False):	# все графики размещает в поле видимости. Если prop==True, то сохраняет пропорции
		if self.w == None or self.h == None:
			#print 'view_all postponed'
			self.after(250, lambda: self.view_all(prop))
			return

		box = self.bbox(TAG_LINE)
		if not box: return # если пусто, уходим
		x_min, y_min, x_max, y_max = box
		ky = float(self.h)/(y_max - y_min)			# во сколько раз растянуть
		kx = float(self.w)/(x_max - x_min)			# во сколько раз растянуть
		if prop:
			k = min(kx, ky)
			self.scale_graph(k, k)
		else:
			self.scale_graph(kx, ky)
		dy = self.h/2.0 - (y_max + y_min)/2	# на сколько сместить
		dx = self.w/2.0 - (x_max + x_min)/2	# на сколько сместить
		self.move_graph(dx, dy)

	def initial_scaling(self, prop=False):
		self.view_all(prop)
		self.view_all(prop)
		# 2 раза, потому что первоначальное масштабирование с мизерных координат не растягивает на весь канвас

	def _axis_box(self):
		box = self.bbox(TAG_GRAPH)
		if box is None:
			box = self.bbox(ALL)
		return box

	def __draw_axe_x(self, y_gr, width, color):	# рисует ось-икс в точке графика y
		x_min_bb, y_min_bb, x_max_bb, y_max_bb = self._axis_box()
		x_min = min(self.canvasx(0), x_min_bb)
		x_max = max(self.canvasx(self.winfo_width()), x_max_bb)
		y = self.y_canv(y_gr)
		self.create_line(x_min, y, x_max, y, fill = color, tags=TAG_AXIS)

	def create_axe_x(self, y_gr, width=1.5, color='black'):	# создает ось-икс в точке графика y
		self.__draw_axe_x(y_gr, width, color)
		self.x_axis.append( (y_gr, width, color) )

	def __draw_axe_y(self, x_gr, width, color):	# рисует ось-игрек в точке графика x
		x_min_bb, y_min_bb, x_max_bb, y_max_bb = self._axis_box()
		y_min = min(self.canvasy(0), y_min_bb)
		y_max = max(self.canvasy(self.winfo_height()), y_max_bb)
		x = self.x_canv(x_gr)
		self.create_line(x, y_min, x, y_max, fill = color, tags=TAG_AXIS)

	def create_axe_y(self, x_gr, width=1.5, color='black'):	# создает ось-игрек в точке графика x
		self.__draw_axe_y(x_gr, width, color)
		self.y_axis.append( (x_gr, width, color) )

	def redraw_axis(self):			# перерисовываем оси
		self.delete(TAG_AXIS)
		for a in self.x_axis: self.__draw_axe_x(*a)
		for a in self.y_axis: self.__draw_axe_y(*a)


# test
if __name__ == '__main__':
	root = Tk()

	fr_gr=Frame(); fr_gr.pack(fill=BOTH, expand=YES)
	gr = Graph(fr_gr)
	gr.pack(fill=BOTH, expand=YES)

	Button(root, text='axis x', command=lambda:gr.create_axe_x(float(e1.get()))).pack(side=RIGHT)
	e1 = Entry(root)
	e1.insert(0,'0')
	e1.pack(side=RIGHT)
	Button(root, text='axis y', command=lambda:gr.create_axe_y(float(e2.get()))).pack(side=RIGHT)
	e2 = Entry(root)
	e2.insert(0,'0')
	e2.pack(side=RIGHT)

	# обычные функции
	#gr.draw(lambda x: x**2,-4,8,1000, color='blue')
	gr.draw(cos,-4*pi,8*pi,1000, color='red')
	gr.draw(lambda x: x,-1,18,10, color='black')
	#gr.draw(exp,-5,8,1000, color='green')
	gr.draw(lambda x: (1/(1+e**(-x))),-10,20,2000, color='green')

	# параметрические функции
	gr.draw_par(lambda t: sin(1.3*t+pi/4), lambda t: cos(2.4*t) + 3, 0, 10*pi, 1000)
	gr.draw_par(lambda t: cos(t), lambda t: sin(t), 0, 2*pi, 1000, 'black')

	#gr.add_text(0, 0, "I am a graph!")

	gr.create_axe_x(0)
	gr.create_axe_y(0)

	gr.initial_scaling(True)
	root.mainloop()
