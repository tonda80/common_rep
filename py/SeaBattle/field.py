# coding=cp1251

from Tkinter import *
from string import lowercase

CL_PL = 'green'		# ���� ����������� ��������
CL_BR = 'red'		# ���� �������� ��������
CL_EMP = 'blue' 	# ���� �������
CL_LBL = 'gray50'	# ���� �����
CL_IDL = 'gray90'	# ���� ����������� ����
	
class Field(Canvas):
	
	
	def __init__(self, master, state, whose, callback = None, size = 10, **kw):
		kw.setdefault('bg', 'white'); kw.setdefault('height', 300); kw.setdefault('width', 300);
		Canvas.__init__(self, master, **kw)
		self.bind('<Configure>', self.hndl_configure)
		self.bind('<Button>', self.hndl_btn)
		
		self.callback = callback	# �������� �����
		
		self.size = size	# ������ ����
		
		self.lines = [(self.create_line(0,0,0,0), self.create_line(0,0,0,0)) for i in xrange(size+1)]		# ������ item ����� 
		
		self.elem = [[0, self.create_oval(0,0,0,0, outline=CL_PL, fill=CL_PL)] for i in xrange(size**2)]	# ������ item �������
		# ��� ��� ������, item ������
		# 0 - �������� ���, 1 - ������������� �������, 2 - ������, 3 - ������������� � �������, 4 - �����
		
		self.hor_text = [self.create_text(0, 0, text=c, justify=CENTER) for c in lowercase[:size]]
		self.ver_text = [self.create_text(0, 0, text=str(c), justify=CENTER) for c in xrange(1, size+1)]
		
		self.ships = [0 for i in xrange(size**2)]	# ������ "��������"
		# 0 - ��� �������, 1 - ����, 2 - ������
		
		self.last_shot = None	# �� ����� ������ �������� ���������
		
		self.state = state	# 'placing', 'game', 'idle', 'debug' - ��������� ����
		
		self.whose = whose	# 'our', 'other' - ��� ����, ���� ��� �����
	
	# ����� ����
	def reset(self):
		self.ships = [0 for i in xrange(self.size**2)]	# �������� �������
		for i in xrange(self.size**2): self.remove_circle(i)	# ������ ��� �����
		if self.last_shot is not None: self.itemconfig(self.elem[self.last_shot][1], state='')
		self.last_shot = None
		# ��������� ������� ���� � ���������				
		
	# ���������� ������� configure	
	def hndl_configure(self, event):		
		self.s_cell = min(self.winfo_width(), self.winfo_height())/(self.size+1)	# ������ ������
		s_fld = self.s_cell*self.size	# ������ ����
		for i, l in enumerate(self.lines):
			s = self.s_cell*i
			self.coords(l[0], s, 0, s, s_fld)	# �������� �������
			self.coords(l[1], 0, s, s_fld, s)	# �������� ������
		for i, el in enumerate(self.elem):
			if el[0]:
				if el[0] >= 3: self.draw_circle(i, 'small')
				else: self.draw_circle(i, 'big')
		font = ('Courier', self.s_cell/2, 'bold')
		for i, c in enumerate(self.hor_text):
			self.coords(c, (i+0.5)*self.s_cell, s_fld+self.s_cell/2); self.itemconfig(c, font=font)			
		for i, c in enumerate(self.ver_text):
			self.coords(c, s_fld+self.s_cell/2, (i+0.5)*self.s_cell); self.itemconfig(c, font=font)
			
			
		
	# ���������� ����� �� ���� 
	def hndl_btn(self, event):
		x = self.canvasx(event.x); y = self.canvasy(event.y)
		n_col = min(int(x/self.s_cell), self.size-1)
		n_row = min(int(y/self.s_cell), self.size-1)
		n_cell = n_row*self.size + n_col
		if event.num == 1:	# ����� ������
			if self.state == 'placing':
				self.place_el(n_cell)
			elif self.state == 'game' and self.callback:
				self.callback(n_cell)
			elif self.state == 'debug':
				#print self.near(n_cell)
				self.shot(n_cell)				
		elif event.num == 3 and self.whose == 'other':	# ������ ������ � ����� ����
			self.place_label(n_cell)
			
	# ���������\������� ������-�����
	def place_label(self, n_cell):
		el = self.elem[n_cell]	# ���� ������� ��� ����. ��������
		if el[0] == 0:	# ������  ���
			el[0] = 4
			self.draw_circle(n_cell, 'small', color=CL_LBL)
		elif el[0] == 4:	# ����� ����
			self.remove_circle(n_cell)	
			 	
			
	# ���������\������� ������ ��� ���������� �����	
	def place_el(self, n_cell):
		if self.elem[n_cell][0] == 0:	# �������� ���
			self.elem[n_cell][0] = 1
			self.ships[n_cell] = 1
			self.draw_circle(n_cell, 'big', color=CL_PL)
		else:	# ������� ����
			self.ships[n_cell] = 0
			self.remove_circle(n_cell)
			
	# ������� �� ����. => 2 - ����, 1 - �����, 0 - ����
	def shot(self, n_cell):
		if self.last_shot is not None: self.itemconfig(self.elem[self.last_shot][1], state=NORMAL)		# ���������� ������� ������� ���������
		self.last_shot = n_cell
		if self.ships[n_cell] == 1:		# ������
			self.ships[n_cell] = 2
			self.elem[n_cell][0] = 2
			self.draw_circle(n_cell, 'big', CL_BR)
			if self.test_dead(n_cell):
				return 2
			else:
				return 1
		elif self.ships[n_cell] == 0:	# ������������
			self.elem[n_cell][0] = 3
			self.draw_circle(n_cell, 'small', CL_EMP)
		return 0
		
		
	#������ ����������� ������, ���� ���� �� �����, �� �� ������ ���
	def draw_circle(self, n_cell, size, color=None):
		el = self.elem[n_cell]	# ���� ������� ��� ����. ��������
		n_row, n_col = divmod(n_cell, self.size)
		if size =='big':
			dlt = self.s_cell/20
		if size =='small':
			dlt = self.s_cell/2.5
		x0, y0 = n_col*self.s_cell + dlt, n_row*self.s_cell + dlt
		x1, y1 = x0+self.s_cell - 2*dlt, y0+self.s_cell - 2*dlt		
		self.coords(el[1], x0, y0, x1, y1)
		if color: self.itemconfigure(el[1], fill=color, outline=color)
	
	# ������� ������
	def remove_circle(self, n_cell):
		self.elem[n_cell][0] = 0
		self.coords(self.elem[n_cell][1], 0,0,0,0)
		
	# � ��������� idle ������ ���� �����
	def __setattr__(self, name, value):
		self.__dict__[name] = value
		if name=='state':
			if value=='idle': self.config(bg = 'gray')
			else: self.config(bg = 'white')
			
	# ���������� ������ ����� �������
	def near(self, n_cell):
		lst_near = []
		if n_cell%self.size != 0: lst_near.append(n_cell-1)				# ����� �����
		if n_cell%self.size != self.size-1: lst_near.append(n_cell+1)	# ������ �����
		near = n_cell-self.size
		if near >= 0: lst_near.append(near)								# ������� �����
		near = n_cell+self.size
		if near < self.size**2: lst_near.append(near)					# ������ �����
		return tuple(lst_near)
		
	# ��������� ���� �� �������. ���������� 1 ���� ��� ���������� "��������" � 0 ���� ����
	def test_dead(self, n_cell, lst_see=None):
		if lst_see is None: lst_see = []
		for c in self.near(n_cell):
			if c in lst_see:
				continue
			if self.ships[c]==0:
				lst_see.append(c)
				continue				
			elif self.ships[c]==1:
				return 0
			elif self.ships[c]==2:
				lst_see.append(c)
				if self.test_dead(c, lst_see)==0:
					return 0	
		return 1
		
	# ��� �������� ���������	
	def blink_last(self):
		if self.last_shot is None: return
		if self.itemcget(self.elem[self.last_shot][1], 'state') == HIDDEN:
			self.itemconfig(self.elem[self.last_shot][1], state='')
		else:
			self.itemconfig(self.elem[self.last_shot][1], state=HIDDEN)
			
	# ���������� ������ �����
	def lives(self):
		l = []
		for i, s in enumerate(self.ships):
			if s == 1: l.append(i)
		return tuple(l)
		
if __name__ == '__main__':
	root = Tk()
	
	def tst():
		sea.blink_last()
		root.after(500, tst)
	
	sea = Field(root, state = 'placing', whose='other')
	sea.pack(fill=BOTH, expand=YES)
	
	def tst_cmd():
		#sea.state = 'debug'#'idle'
		sea.reset()
	Button(text='Test', command=tst_cmd).pack()
	
	tst()
	root.mainloop()