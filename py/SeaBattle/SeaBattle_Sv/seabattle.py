# coding=cp1251

from Tkinter import *
from jbot import Jbot, JbotError
from field import Field
from settings import *

class App(Tk):
	def __init__(self):
		Tk.__init__(self)
		self.title(u'������� ���..  ' + USERNAME)
		
		# �������������� �����
		fr_info = Frame(self, height=20, borderwidth=2, relief=GROOVE)
		fr_info.pack(side=BOTTOM, expand=0, fill=X)
		self.info = StringVar(fr_info)
		Label(fr_info, textvariable=self.info).pack(side=LEFT)
		
		# �������  ����
		self.sea_our = Field(self, state='placing', whose='our')		
		self.sea_our.pack(side=LEFT, fill=BOTH, expand=YES)
		self.sea_other = Field(self, state = 'idle', callback=self.shot, whose='other')
		self.sea_other.pack(side=RIGHT, fill=BOTH, expand=YES)
		
		# ������ ����������
		self.btn_conn = Button(text='connect', command=self.connect, width=8)
		self.btn_conn.pack(padx=10, pady=10)
		
		# ������ �����
		self.btn_start = Button(text='start', command=self.start, width=8)
		self.btn_start.pack(padx=10, pady=10)
		
		# ������ �����
		self.btn_reset = Button(text='reset', command=self.reset, width=8)
		self.btn_reset.pack(padx=10, pady=10)
		
		self.state = 'init'
		
		# ������ �������
		#def debug(): print self.state, self.sea_our.state, self.sea_other.state
		#Button(text='debug', command=self.deiconify, width=8).pack(padx=10, pady=10)
		
		self.connect()
		
		
	def timer(self):
		self.botty.check()
		self.sea_our.blink_last()
		self.after(400, self.timer)
		
	def connect(self):
		try:
			self.botty = Jbot(uname=USERNAME, psw=PASSWD, serv=SERVER, serv_a=SERVER_ADDR, port=PORT, callback=self.receive)
		except JbotError, s:
			self.info.set(s)
		else:
			self.info.set(u'���������� �����������')
			self.btn_conn['state'] = 'disabled'
			self.timer()
				
	def send(self, mess):
		try:
			self.botty.send(PARTNER, mess)
		except JbotError, s:
			self.info.set(s)
			self.btn_conn['state'] = 'normal'
		except AttributeError:
			self.info.set(u'���������� �� �����������')
			
	def receive(self, mess):
		if mess[0].split('/',1)[0] != PARTNER: return		# �������� �������� (����� / ���� ������)
		
		if mess[1]=='i ready' and self.state=='init':			# ������ ������ �� ����
			self.change_state('wait_place', '5 sec', u'� ������� �� ��� �����!')
		
		elif mess[1]=='5 sec' and self.state=='wait_partner':	# ������� ���������
			self.change_state(None, None, u'������� ��������!')
		
		elif mess[1]=='go' and self.state=='wait_partner':		# ������� �����
			self.change_state('game', None, u'���� ��������! ����!')
		
		elif mess[1][:3]=='pli' and self.state=='wait_move':	# ����� ������� 
			try: n_cell = int(mess[1][3:])
			except ValueError:
				print('Error message: ', mess)
				return
			hit = self.sea_our.shot(n_cell)	# ������ ��������� ��������			
			if hit==2 :		# �����
				if self.sea_our.ships.count(1) == 0:	# ����� ����
					self.change_state('game_over', 'i loser'+mess[1][3:], u'�� ��������. ������..')
				else:
					self.change_state(None, 'hit2'+mess[1][3:], u'��� ���� ������� ���������!')
			elif hit==1 :	# ������
				self.change_state(None, 'hit1'+mess[1][3:], u'���� �������. �� ������� ��� �� �����!')
			else:			# �� ������
				self.change_state('game', 'hit0'+mess[1][3:], u'��.. �� ��������. ����!')
		
		elif mess[1][:3]=='hit' and self.state=='wait_hit':		#  ����� � ����� ��������
			try:
				n_cell = int(mess[1][4:])
				hit = int(mess[1][3])
			except ValueError:
				print('Error message: ', mess)
				return
			if hit==2: # ����
				self.sea_other.ships[n_cell] = 1
				self.change_state('game', None, u'�������. ������� ����� �� ���. ��������� � ��� �� ����.')
			elif hit==1: # �����
				self.sea_other.ships[n_cell] = 1
				self.change_state('game', None, u'���������! �� ������� ��� �� �����! ���� �����!')
			else: 	# �����������
				self.change_state('wait_move', None, u'���� :(')
			self.sea_other.shot(n_cell)	# ������ ��������� ���� 
		
		elif mess[1][:7]=='i loser' and self.state=='wait_hit':	# ��������� � ���������
			try: n_cell = int(mess[1][7:])
			except ValueError: print('Error message: ', mess); return
			self.sea_other.ships[n_cell] = 1
			self.sea_other.shot(n_cell)	# ������ ��������� ���� 
			self.change_state('game_over', 'live' + ','.join(map(str, self.sea_our.lives())), u'�������! ������ �������� �� ����!')
		
		elif mess[1][:4]=='live' and self.state=='game_over':	# ��������� �� ���������� � ����� �������� ���������
			try:
				lst_live = map(int, mess[1][4:].split(','))
			except ValueError: print('Error message: ', mess); return
			for i in lst_live:
				self.sea_other.place_el(i)
			
	def start(self):
		if self.sea_our.ships.count(1) != 20:	# �������� ������������� ����, ����� ������� ����� �����
			self.info.set(u'� ���� �� �� ���������!')
			#return
			
		self.sea_our.state = 'idle'	# ��������� ���� ����)
		
		if self.state=='init' or self.state=='wait_partner':
			self.change_state('wait_partner', 'i ready', u'������� ������ ���������...')
		elif self.state=='wait_place':
			self.change_state('wait_move', 'go', u'���� ��������! ��� ���������.')
			
	def reset(self):
		self.state = 'init'
		self.sea_our.state='placing'
		self.sea_our.reset()
		self.sea_other.state = 'idle'
		self.sea_other.reset()
			
	def shot(self, n_cell):
		self.change_state('wait_hit', 'pli'+str(n_cell), u'����-�..')
		
	# ����� ���������
	def change_state(self, state, send_mess, info_mess):
		if state != None: self.state = state
		if send_mess != None: self.send(send_mess)
		if info_mess != None: self.info.set(info_mess)
		
	# ������ ��������� ������ ���� ��� ��������� ����
	def __setattr__(self, name, value):
		self.__dict__[name] = value
		if name=='state':
			if value=='game': self.sea_other.state = 'game'
			else: self.sea_other.state = 'idle'
			

		
		
App().mainloop()
















