# Опять мучаю уровень "Return to party" в fillets (труба сложной формы не должна упасть при перемещении по рюмкам). Спустя лет 14 пытался безуспешно показаться себе умнее(
# в итоге бросил так и не домучив :( и даже прошлое свое решение я до конца не понял, судя по всему я и там какие то промежуточные результаты получил
# подсказка к решению пазла (полученная увы :( просмотром своего старого сейва): расставить рюмки в коде похоже у меня и тогда не получилось, нужно додуматься до ещё одного действия


import copy

tube_descr =  '1000101001'											# форма трубы (см класс)
field_descr = '2001020001002010000100000001000010000200000001'		# рельеф (см класс)


class Tube:
	def __init__(self, descr):
		self._descr = tuple(c == '1' for c in descr)

	def is_leg(self, i):
		return self._descr[i]

class FieldCell:
	def __init__(self, code):
		self.is_leg = code == '1'
		self.is_hole = code == '2'
		self.supported = []			# сюда будем писать шаги на которых рюмка тут поддерживает трубу


class Game:
	def __init__(self, tube_descr, field_descr):
		self.tube = Tube(tube_descr)
		self.field = [FieldCell(c) for c in field_descr]
		self.tube_size = len(tube_descr)
		self.step_numbers = range(1, len(self.field) - self.tube_size + 1)	# все шаги трубы до конца поля

		for i in self.step_numbers:
			self._check(i)

	def _check(self, step):
		'для i-го шага, найдем все варианты как можно поддержать трубу'
		poss_support_idx = []
		for i in range(self.tube_size):
			if not self.tube.is_leg(i):
				continue
			cell = self.field[step+i]
			if cell.is_leg:
				return		# нога на ноге - не нужна поддержка
			elif not cell.is_hole:		# пока считаем что обойдемся без поддержки в ямах
				poss_support_idx.append(i)
		for i in poss_support_idx:
			self.field[step+i].supported.append(step)

	def print_info(self):
		for i in range(1, len(self.field)):
			supported = self.field[i].supported
			if len(supported):
				print(f'{i:02} {supported}')

	def get_needed(self):
		'проанализируем результат'
		self.print_info()

		all_supported = []		# все шаги нуждающиеся в поддержке
		for c in self.field:
			all_supported.extend(c.supported)
		bad_steps = [i for i in self.step_numbers if i in all_supported]	# то же но по порядку

		# и ещё проверим что нет шагов с уникальной поддержкой
		for i in bad_steps:
			assert all_supported.count(i) != 1

		max_supp = max(map(lambda c: len(c.supported), self.field))
		print(f'max_supp={max_supp}, bad_steps={bad_steps}')

		# будем смотреть только на поля с максимальной поддержкой (3 шага)
		check_idxs = [i for i,c in enumerate(self.field) if len(c.supported) == max_supp]

		def print_rest():
			print(f'-- Total {len(check_idxs)}')
			for i in check_idxs:
				print(f'{i} {self.field[i].supported}')

		print_rest()

		# удалим не уникальные ячейки
		for i in check_idxs[::-1]:
			uniq = copy.copy(self.field[i].supported)
			for j in check_idxs:
				if i == j:
					continue
				for u in uniq[:]:
					if u in self.field[j].supported:
						uniq.remove(u)
				if not uniq:
					break
			if not uniq:
				#print(f'removal {i}')
				check_idxs.remove(i)

		# смотрим сколько роняющих шагов осталось
		for i in check_idxs:
			for s in self.field[i].supported:
				try:
					bad_steps.remove(s)
				except ValueError:
					pass
		assert len(bad_steps) == 0	# не должно быть

		print_rest()

		# в итоге тут не решение, а только инфа к размышлению в выводе


if __name__ == '__main__':
	game = Game(tube_descr, field_descr)
	game.get_needed()



exit()
# далее попытка решения 1 - неудачная. пытался рассмотреть участок в "вакууме" и к нему требования к рельефу явно не минимальные

tube = '1000101001'		# форма трубы. 1 - "нога"
tube_size = len(tube)


def is_hold_up(tube_in_window, window):
	'держится (не падает) ли труба на участке'
	for p,q in zip(tube_in_window, window):
		if p == '1' and q == '1':
			return True
	return False

def gen_tube_in_window():
	'генерируем прохождение трубы через "окно" ее размера'
	ext = '0'*(tube_size-1)
	ext_tube = ext + tube + ext
	for i in range(tube_size*2-2, -1, -1):
		yield ext_tube[i:i+tube_size]

def min_relief():
	'найдем минимально необходимый рельеф участка (размера трубы), чтобы труба не упала при прохождении по нему'
	relief = ['0' for i in range(tube_size)]

	# если сейчас одна нога в окне - обязаны опереться на нее
	for t in gen_tube_in_window():
		cur_n_legs = t.count('1')	# ноги в окне :)
		assert cur_n_legs > 0
		if cur_n_legs == 1:
			relief[t.find('1')] = '1'

	# проверим хватит ли минимального
	ok = True
	for t in gen_tube_in_window():
		if not is_hold_up(t, relief):
			ok = False
			print('!!!', t, relief)
	if not ok:
		raise NotImplementedError

	print(''.join(relief))


if __name__ == '__main__':
	for t in gen_tube_in_window():
		print(t)
	print('--')
	min_relief()
