#!/usr/bin/env python3


# Миша принес из школы шахматную задачу
# Поле 4x5, нужно обойти все клетки конем
# Он набрал 15, у меня получилось 16, дети набирали до 18, учитель показывал 20


import random


class Field:
	class Filled(Exception):
		pass

	class NoMove(Exception):
		pass

	def __init__(self, w, h):
		self.columns = w
		self.rows = h
		self.size = w*h

		self.reset((0, 0))

	def reset(self, start = None):
		if start is None:
			start = divmod(random.randrange(self.size), self.columns)

		self.cells = [[None for j in range(self.columns)] for i in range(self.rows)]
		#print('__deb0', start, self.cells, self.rows)
		self._make_move(start, 1)

	def q_moves(self):
		return self.cells[self.cur_pos[0]][self.cur_pos[1]]

	# 0,0  0,1  0,2 .. 0,w-1
	# 1,0  1,1 ..      1,w-1
	knight_shifts = ((-2,-1), (-2, 1), (2, -1), (2, 1), (-1, 2), (1, 2), (-1, -2), (1, -2))

	def _possible_moves(self):
		for sw, sh in self.knight_shifts:
			r = self.cur_pos[0] + sw
			c = self.cur_pos[1] + sh
			#print('_deb', w, h, self.cells)
			if 0 <= c < self.columns and 0 <= r < self.rows and self.cells[r][c] is None:
				yield r, c

	def _make_move(self, to, sign = None):
		if sign is None:
			sign = self.q_moves() + 1

		self.cells[to[0]][to[1]] = sign
		self.cur_pos = to

	def make_random_move(self):
		moves = list(self._possible_moves())
		#print('__deb', self.cur_pos, moves)

		if not moves:
			raise Field.NoMove

		self._make_move(random.choice(moves))

		if all(map(all, self.cells)):
			raise Field.Filled

	def state_string(self):
		def str_el(e):
			return f'{e:2}' if e else ' x'
		return '\n'.join('  '.join(map(str_el, l))  for l in self.cells)



def task1():
	# пытаемся найти полное заполнение
	field = Field(5, 4)
	best = 0

	work = True
	while work:
		field.reset()
		while 1:
			try:
				field.make_random_move()
				#input(f'\n{field.state_string()}\npress to continue')
			except Field.NoMove:
				total_moves = field.q_moves()
				if total_moves > best:
					best = total_moves
					print(f'Found solution with {best} moves')
				break
			except Field.Filled:
				print(f'Found the best solution!\n{field.state_string()}')
				work = False
				break



if __name__ == '__main__':
	task1()
