#!/usr/bin/env python3

# wordle
# решение английского, но можно будет сделать решение русского и загадывание обоих

try:
	import readline
except Exception as e:
	print('Warning:', e)
#import itertools

from baseapp import BaseConsoleApp
import vocabulary
from mix_utils import pop_rand


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-l', '--language', choices=('en', 'ru'), default='en', help='Язык')
		self.parser.add_argument('--size', type=int, help='Размер слова')

		sp1 = self.add_subparser('s', self.main_solver, help='Решать wordly')
		sp1.add_argument('-w', '--words', nargs='*', help='Тестовые слова для решения (весна [12000] итд)')

		sp2 = self.add_subparser('p', self.main_proposer, help='Загадывать wordly')

		sp3 = self.add_subparser('f', self.main_finder, help='Искать слово')

	def main(self):
		if self.args.size is None and self.args.target_main != self.main_finder:	# TODO костыль
			self.args.size = 5

		self.voc = Vocabulary(self.args.language, self.args.size)
		return self.sub_main()

	def main_solver(self):
		while 1:
			print('Let\'s guess!')
			res = self._main_solver_body()
			print(res, 'Handled words:', self.voc.handled_words)
			input('Proceed?')
			print()

	def _main_solver_body(self):
		test_words = self.args.words[:] if self.args.words else []
		for w in test_words:
			if len(w) != self.args.size:	#  or not w.isalpha()
				raise RuntimeError(f'Wrong test word "{w}"!')
		guess_word = GuessWord(self.args.size)
		self.voc.init_new_search(guess_word)
		try_cnt = 0

		while 1:
			test_word = test_words.pop(0) if test_words else self.voc.get_suitable_word()
			if not test_word:
				return f'FAIL! Cannot guess word! My best is "{guess_word.state(True, True)}".'

			try_cnt += 1
			if test_words and all(map(lambda c: c in '012', test_words[0])):
				answer = test_words.pop(0)
			else:
				answer = self._get_answer_from_user(test_word)
				if not answer: continue

			guess_word.check(test_word, answer)
			if guess_word.is_guessed():
				return f'SUCCESS! Guess word is "{guess_word.state()}" from {try_cnt}th attempt.'

			if not guess_word.is_valid():
				return f'FAIL! Ambiguous state "{guess_word.state(True, True)}". Looks like you deceive me, try anew.'

			print(f'Tried "{test_word}", result is "{guess_word.state(True)}"')

	# ----

	def main_proposer(self):
		raise NotImplementedError

	# ----

	def main_finder(self):
		if self.args.size:
			print('Warning. "--size" argument is superfluous in find mode')
		while 1:
			res = self._main_finder_body()
			print(f'Total {res} words, handled {self.voc.handled_words}')

	def _main_finder_body(self):
		mask_p1, mask_p2 = self._get_mask_from_user()

		# настройка
		w_len = len(mask_p1)
		guess_word = GuessWord(w_len)
		self.voc.init_new_search(guess_word)
		#
		for i, l in enumerate(mask_p1):
			if l != '*':
				guess_word.word[i] = l
		for l in mask_p2:
			guess_word.ex_letters[l] = set()

		# поиск
		cnt = 0
		while 1:
			if opt_word := self.voc.get_suitable_word():
				print(opt_word)
				cnt += 1
			else:
				return cnt

	def _get_answer_from_user(self, test_word):
		print(f'Is it "{test_word}"?')
		while 1:
			answer = input('> ')
			if answer == 'y' or answer == 'д':
				return '1'*self.args.size
			if answer == 's':
				return ''
			if answer == 'p':
				self.voc.print_rest_of_words()
				continue
			if any(l not in '012' for l in answer) or len(answer) != self.args.size:
				print('Reply must be: y (yes), s (skip), p (print me the rest) or a word from symbols: 0, 1, 2. The last one means: 0 - no such letter, 1 - the letter is right here, 2 - the letter is somewhere else.')
				continue
			return answer

	def _get_mask_from_user(self):
		while 1:
			try:
				mask_p1, _, mask_p2 = input('> ').partition(' ')
			except KeyboardInterrupt:
				raise
			except Exception as e:
				print('Что то не то. Ожидается: "a*b** cd", a, b - зеленые буквы, cd - желтые (опционально)')
				continue
			return mask_p1, mask_p2



class Vocabulary:
	def __init__(self, lang, size = None):
		if lang == 'en':
			word_gen = vocabulary.words_alpha		# wordlist_10000
		elif lang == 'ru':
			word_gen = vocabulary.zdf_win
		else:
			raise NotImplementedError

		self.all_worlds = {}	# size: (set0, set1)  - set0 - все буквы разные, для поиска, set1 - остаток

		for w in word_gen():
			w_len = len(w)
			if size is None or size == w_len:
				sets = self.all_worlds.setdefault(w_len, (set(), set()))
				if w_len == len(set(w)):
					sets[0].add(w)
				else:
					sets[1].add(w)

	def init_new_search(self, guess_word):
		size = guess_word.size
		sets = self.all_worlds.setdefault(size, (set(), set()))
		self.cur_words0 = list(sets[0])
		self.cur_words1 = list(sets[1])

		self.guess_word = guess_word

		self.handled_words = 0

	def get_suitable_word(self):
		for lst in self.cur_words0, self.cur_words1:
			while lst:
				self.handled_words += 1
				word = pop_rand(lst)
				if self.guess_word.is_suitable(word):
					return word

	def print_rest_of_words(self):
		# таки заодно удалим уже не подходящие
		for lst in self.cur_words0, self.cur_words1:
			for i in range(len(lst) - 1, -1, -1):
				if self.guess_word.is_suitable(lst[i]):
					print(lst[i], end=' ')
				else:
					lst.pop(i)
					# ?	self.handled_words += 1
		print(f'Total {len(self.cur_words0) + len(self.cur_words1)}')


class GuessWord:
	def __init__(self, size):
		self.size = size
		self._is_valid = True

		self.word = ['' for i in range(size)]
		self.nonex_letters = set()
		self.ex_letters = {}		# буква слова: сет позиций в которых ее нет

	# проверим, подходит ли слово
	def is_suitable(self, test_word):
		assert len(test_word) == self.size

		for l in test_word:						# в тест слове есть исключенная буква
			if l in self.nonex_letters:
				return 0

		for l, ii in self.ex_letters.items():
			if l not in test_word:				# в тест слове нет буквы которая должна быть
				return 0
			for i in ii:
				if test_word[i] == l:			# в тест слове буква в "желтой" позиции
					return 0

		for i, l in enumerate(self.word):
			if l and test_word[i] != l:			# тест слово не совпадает с уже угаданной частью
				return 0

		return 1

	def is_guessed(self):
		return all(self.word)

	def is_valid(self):
		return self._is_valid

	def state(self, known = False, excluded = False):
		ret = ''.join(l if l  else '*' for l in self.word)	# word
		if known:
			k = ','.join(l for l,_ in self.ex_letters.items())		# может исключать угаданное? нет ибо смотрим в ex_letters когда подбираем слово
			ret += f' (known: {k})'
		if excluded:
			e = ','.join(l for l in self.nonex_letters)
			ret += f' (excluded: {e})'
		return ret

	def check(self, test_word, answer):
		for i, s in enumerate(answer):
			letter = test_word[i]
			if s == '0':
				self.nonex_letters.add(letter)
				if letter in self.word or letter in self.ex_letters:
					self._is_valid = False
				# на ошибочное добавление проверять не будем, не могу сходу придумать когда оно сломается
			elif s == '1':
				self.word[i] = letter
			elif s == '2':
				self.ex_letters.setdefault(letter, set()).add(i)
			else:
				raise NotImplementedError

if __name__ == '__main__':
	App().start()
