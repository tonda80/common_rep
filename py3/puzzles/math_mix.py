#!/usr/bin/env python3

# Набор небольших математических задачек
# В help output попадает 1-я строчка __doc__ функции

import argparse
import itertools
import sys
import time


def math_rebus(*, args, **kw):
	''' Математический ребус (e.g. -d 'ВЕТКА+ВЕТКА=ДЕРЕВО')
'''
	rebus = ''.join(filter(lambda c: not c.isspace(), args.data)).replace('=', '==')
	letters = ''.join({l for l in rebus if l.isalpha()})
	r = len(letters)
	print(f'{rebus} ({letters})')

	def eval_for(values):
		assert len(values) == r
		test = rebus
		for i in range(r):
			test = test.replace(letters[i], values[i])

		# и боремся с SyntaxError: leading zeros in decimal integer literals are not permitted
		i = 0
		while 1:
			i = test.find('0', i)
			if i == -1:
				break
			if (i==0 or not test[i-1].isdecimal()) and test[i+1].isdecimal():
				#return False, test		# Мише вот не нравится решения с ведущими нулями TODO надо бы сделать аргумент
				test = test[:i] + ' ' + test[i+1:]
			i += 1

		#print('__deb', test)
		return eval(test, {}), test

	if r > 10 or r < 2:
		return f'Wrong rebus \'{rebus}\' too {"many" if r>10 else "few"} letters {r}'
	try:
		_, test = eval_for('1'*r)
	except:
		return f'Wrong rebus \'{rebus}\'. Can\'t eval'

	for values in itertools.permutations('0123456789', r):
		ok, test = eval_for(values)
		if ok:
			out = test.replace('==', '=').replace(' ', '0')
			map_ = ', '.join(f'{k}={v}' for k, v in zip(letters, values))
			print(f'Found solution: {out}  ({map_})')

# TODO сделать вариант с листами и проверить скорость
	# letter_distr = {}
	# for i, l in enumerate(rebus):
		# if l.isalpha():
			# letter_distr.setdefault(l, []).append(i)
	# r = len(letter_distr)


def whisper_and_shout(*, args, **kw):

	''' шепнул*5 = крикнул
	Ребус, каждая буква соответствует одной цифре.
'''
	for s, e, p, n, u, l, k, r, i in itertools.permutations(range(10), 9):
		if (s*100_000 + e*10_000 + p*1000 + n*100 + u*10 + l)*5 == k*1000_000 + r*100_000 + i*10_000 + k*1000 + n*100 + u*10 + l:
			print(f'шепнул={s}{e}{p}{n}{u}{l} крикнул={k}{r}{i}{k}{n}{u}{l}')

def whisper_and_shout_optim1(*, args, **kw):
	''' шепнул*5 = крикнул (оптимизированная)
	см предыдущую whisper_and_shout, тут то же самое, но насколько хватило ума оптимизированное решение'''
	l, u = 0, 5			# это знаем точно
	def var(n, k, s):	# для n k s есть 2 варианта
		rest = set(range(10)) - {l, u, n, k, s}
		for e, p, r, i in itertools.permutations(rest, 4):
			if args.verbose:
				print(f'\t test for: епри={e}{p}{r}{i}')
			if (s*100_000 + e*10_000 + p*1000 + n*100 + u*10 + l)*5 == k*1000_000 + r*100_000 + i*10_000 + k*1000 + n*100 + u*10 + l:
				print(f'шепнул={s}{e}{p}{n}{u}{l} крикнул={k}{r}{i}{k}{n}{u}{l}')
	var(2, 1, 3)
	var(7, 3, 6)


_tasks = (
	math_rebus,
	whisper_and_shout,
	whisper_and_shout_optim1,
)


def get_args(arg_src = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('task_id', type=int, help=f'Выбор задачи: {" ".join(f"{i}) {t.__doc__.splitlines()[0]}" for i, t in enumerate(_tasks))}')
	parser.add_argument('-d', '--data', help='Custom task\'s data')
	parser.add_argument('-t', dest='print_execution_time', action='store_true', help='Print time of execution')
	parser.add_argument('-v', dest='verbose', action='store_true', help='Verbose output')
	return parser.parse_args(arg_src)

def main():
	args = get_args()
	try:
		task = _tasks[args.task_id]
	except IndexError:
		print('No such task. Bye.')
		return 1
	t0 = time.process_time()
	ret = task(args=args)
	if args.print_execution_time:
		print(f'Time of execution: {time.process_time() - t0}')

	ret_code = 0
	if isinstance(ret, str) and ret:
		print(f'Error: {ret}')
		ret_code = 1
	if isinstance(ret, int):
		ret_code = ret
	return ret_code


if __name__ == '__main__':
	sys.exit(main())
