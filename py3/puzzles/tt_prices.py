#!/usr/bin/env python3

# Вам дан массив prices, где prices[i] — цена акции на i-й день. Вы хотите максимизировать свою прибыль, выбрав один день для покупки одной акции и выбрав другой день в будущем для продажи этой акции. Верните максимальную прибыль, которую вы можете получить от этой сделки. Если вы не можете получить никакой прибыли, верните 0.
# Пример 1: Входные данные: prices = [7,1,5,3,6,4]  Ответ: 5
# Пример 2: Входные данные: prices = [7,6,4,3,1] Ответ: 0
# Пример 3: Входные данные: prices = [7,2,10,1,5,3,6,4] Ответ: 8


def algo(prices):
	size = len(prices)
	assert size > 2

	prof = b_i = 0
	buy = prices[0]
	inds = {}

	for i in range(1, size):
		cur_prof = prices[i] - buy
		if cur_prof > prof:
			prof = cur_prof
			inds[prof] = b_i, i

		if prices[i] < buy:
			buy = prices[i]
			b_i = i

	if prof > 0:
		print(f'profit={prof}, days={inds[prof]}')
	else:
		print('No profit')



if __name__ == '__main__':
	algo([7,1,5,3,6,4])
	algo([7,6,4,3,1])
	algo([7,2,10,1,5,3,6,4])
