#!/usr/bin/env python3


# Пара часов с 12 часовым циферблатом показывают 12 часов. Одни часы отстают на 15 минут в сутки, другие спешат на 10 минут в сутки.
# Через сколько суток часы вновь покажут одинаковое время?
# В исходной задаче предлагалось узнать когда часы покажут одинаковое и ВЕРНОЕ время, что сильно проще, так как
# первые часы показывают верное время ровно через 12*4 суток, а вторые ровно через 12*6 суток
# TODO реши про одинаковое время аналитически!


def min2human(minutes):
	hours, m = divmod(minutes, 60)
	d, h = divmod(hours, 24)
	return f'{int(d)} days {int(h)} hours {m} minutes'


def discr(f):
	return lambda t: int(f(t))


# clock_time = k * time
def time1(t):
	return t*1425/1440

def time2(t):
	return t*1450/1440


def clock12(t):
	return t % 720

def clock24(t):
	return t % 1440


clock_time = clock12

for m in range(1, 150*24*60):
	t1 = time1(m)
	t2 = time2(m)
	ct1 = clock_time(t1)
	ct2 = clock_time(t2)
	eq = ct1 == ct2
	discr_eq = eq or int(ct1) == int(ct2)
	if eq or discr_eq:
		print(min2human(m), m, '|', min2human(t1), t1, '|', min2human(t2), t2, '\t This is discrete solution!' if not eq else '')
