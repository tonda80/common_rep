'''Встречаются два приятеля - математика:
- Ну как дела, как живешь?
- Все хорошо, растут два сына дошкольника.
- Сколько им лет?
- Произведение их возрастов равно количеству голубей возле этой скамейки.
- Этой информации мне недостаточно.
- Старший похож на мать.
- Теперь я знаю ответ на твой вопрос.
Сколько лет сыновьям?
Поробнее см в заметках в гугл доке
'''

def pp(what, o):
    print(what)
    for k,v in o.items():
        print(f'\t{k}: {v}')

MAX = 7	# 8 уже ошибка
options = {}
for i in range(MAX+1):
    for j in range(MAX+1):
        options.setdefault(i*j, set()).add(tuple(set((i, j))))   # не хотим различать 2,7 и 7,2
        #options.setdefault(i*j, []).append((i, j))

pp('Все варианты', options)

# уберем с одним ответом, те неизвестное после инфы 1
options = {k: v for k, v in options.items() if len(v) > 1}
pp('Неочевидные после 1', options)

# уберем с одинаковыми возрастами
for k,vv in options.items():
    for v in vv.copy():
        if len(v) == 1:
            vv.remove(v)
pp('Без одинаковых возрастов', options)

# уберем все еще непонятное после инфы 2
options = {k: v for k, v in options.items() if len(v) == 1}
pp('И тут должен быть 1 вариант', options)

assert len(options) == 1
