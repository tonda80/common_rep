#!/usr/bin/env python3

# тестируем производительность sqlite


import sqlite3
import os
import tempfile
import timeit
import random

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('target', choices=('rd', 'wr'), help='Read/write')
		self.parser.add_argument('--db', help='database')
		self.parser.add_argument('--n', type=int, default=10, help='Количество операций для timeit')
		self.parser.add_argument('--ro', action='store_true', help='readonly mode, incompatible with db in memory')
		self.parser.add_argument('--wal', action='store_true', help='wal mode')

	def _db_connect_args(self):
		if self.args.ro or not self.args.db:
			db = os.path.join(tempfile.gettempdir(), 'sqlitetest.db')
		else:
			db = self.args.db
		if self.args.ro:
			db = f'file:{db}?mode=ro'
		return db, self.args.ro

	def exec_(self, sqlite_str, *args):
		self.cursor.execute(sqlite_str, args)

	def exec_and_commit(self, sqlite_str, *args):
		self.cursor.execute(sqlite_str, args)
		self.connection.commit()

	def timeit_exec(self, sqlite_str, *args):
		return timeit.timeit(lambda: self.exec_(sqlite_str, *args), number=self.args.n, globals = {'self': self})

	def timeit_exec_and_commit(self, sqlite_str, *args):
		return timeit.timeit(lambda: self.exec_and_commit(sqlite_str, *args), number=self.args.n, globals = {'self': self})


	def main(self):
		db, uri = self._db_connect_args()
		self.log.info(f'Use db \'{db}\'')

		self.connection = sqlite3.connect(db, uri=uri, timeout=5)
		self.cursor = self.connection.cursor()

		if self.args.wal:
			self.connection.execute('PRAGMA journal_mode=WAL')

		if self.args.target == 'wr':
			self.connection.execute('CREATE TABLE IF NOT EXISTS t (key TEXT, value TEXT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)')
			self.write_test()
		elif self.args.target == 'rd':
			self.read_test()
		else:
			raise NotImplementedError

	def write_test(self):
		while 1:
			print('write time =', self.timeit_exec_and_commit('INSERT INTO t (key, value) VALUES (?1, ?2)', random.randrange(999), random.randrange(999)))

	def read_test(self):
		while 1:
			try:
				res = self.timeit_exec('SELECT * FROM t LIMIT 10')
			except sqlite3.OperationalError as e:
				print(f'{e.__class__.__name__}: {e}')
				continue
			self.cursor.fetchall()
			self.exec_('SELECT * FROM t ORDER BY time DESC LIMIT 1')
			print(f'read time = {res:.3f}, current = {self.cursor.fetchall()}')



if __name__ == '__main__':
	try:
		App().main()
	except (SystemExit, KeyboardInterrupt):
		pass

