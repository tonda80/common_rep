import sys
import types



def Z_lala():
	pass

def Z_lala2():
	pass



# типа "библиотечная функция"
def __check_module(module, pref):
	for name, attr in module.__dict__.items():
		if not name.startswith('__') and \
					isinstance(attr, types.FunctionType) and \
					not name.startswith(pref):
			raise Warning(f'module {module.__name__} contains a function with a bad name: {name}')


# и вызов
__check_module(sys.modules[__name__], 'Z_')
