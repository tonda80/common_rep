#!/usr/bin/env python3

# https://github.com/pytransitions/transitions


from transitions import Machine
import random



class NarcolepticSuperhero(object):

	states = ['asleep', 'hanging out', 'hungry', 'sweaty', 'saving the world']

	def __init__(self, name):
		self.name = name
		self.kittens_rescued = 0

		self.machine = Machine(model=self, states=NarcolepticSuperhero.states, initial='asleep')

		self.machine.add_transition(trigger='wake_up', source='asleep', dest='hanging out')

		self.machine.add_transition('work_out', 'hanging out', 'hungry')

		self.machine.add_transition('eat', 'hungry', 'hanging out')
		self.machine.add_transition('cond_eat', 'hungry', 'hanging out', conditions='always_false')

		self.machine.add_transition('distress_call', '*', 'saving the world', before='change_into_super_secret_costume')

		self.machine.add_transition('complete_mission', 'saving the world', 'sweaty', after='update_journal')

		self.machine.add_transition('clean_up', 'sweaty', 'asleep', conditions=['is_exhausted'])
		self.machine.add_transition('clean_up', 'sweaty', 'hanging out')

		self.machine.add_transition('nap', '*', 'asleep')


		print(id(self.machine), dir(self.machine))

	def update_journal(self):
		self.kittens_rescued += 1

	@property
	def is_exhausted(self):
		return random.random() < 0.5

	def change_into_super_secret_costume(self):
		print("Beauty, eh?")


	def always_false(self):
		return False




batman = NarcolepticSuperhero("Batman")

assert batman.state == 'asleep'

#Error batman.complete_mission()  но бывает ignore_invalid_triggers

batman.distress_call()
assert batman.state == 'saving the world'

batman.complete_mission()
#batman.to_sweaty()		# туда же перейдет и не проверяя текущее состояние, + есть auto_transitions
assert batman.state == 'sweaty'


# ошибка   batman.eat()
# и так ошибка  batman.cond_eat()

print(batman.state)

# есть add_ordered_transitions(loop, ) для линейного перемещения по состояниям
# да и чего там только нет!!!
