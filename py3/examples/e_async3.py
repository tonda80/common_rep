import asyncio
import random
import time


async def worker(name, delay = 2):
    start = time.time()
    print(f'begin worker {name} with {delay:.2f}')
    await asyncio.sleep(delay)
    print(f'end worker {name}: {time.time() - start:.2f}')


def get_random_delay(from_ = 1, to = 3):
    assert to > from_
    return from_ + (to - from_)*random.random()



async def main():
    workers = (worker(i, get_random_delay()) for i in range(20))
    print('Start')
    res = await asyncio.gather(*workers)
    #print(res)


asyncio.run(main())
