import aiohttp
import asyncio
import sys


# и пример aiohttp и поиск по квиз-плиз играм

g_seek = sys.argv[1]	# 'СССР'
print(f'trying to find {g_seek}')


async def get_page(session, page):
	url = 'https://saratov.quizplease.ru/schedule-past?page={}&per-page=12'
	async with session.get(url.format(page)) as resp:
		assert resp.ok
		process(page, await resp.text())

async def main():
	async with aiohttp.ClientSession() as session:
		tasks = [get_page(session, i) for i in range(2, 150)]
		await asyncio.gather(*tasks)
		process_result()

# --

result = []

def process(page, text):
	idx = 0
	while 1:
		idx = text.find(g_seek, idx)
		if idx == -1:
			break
		idx += 4
		s = text[idx-30:idx+30].strip()
		if s.endswith('</div>'):
			result.append((page, s))

def process_result():
	result.sort(key = lambda e: e[0])
	for e in result:
		print(*e)


if __name__ == '__main__':
	asyncio.run(main())
