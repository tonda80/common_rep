import asyncio
import concurrent.futures
import os
import time


def handle():
    n = 5
    print(os.getpid(), f'PROC is here for {n} sec')
    time.sleep(n)
    return os.getpid()


async def main():
    with concurrent.futures.ProcessPoolExecutor(max_workers=5) as pool:

        # opt 3 (run only)
        while 1:
            print(os.getpid(), 'MAIN is starting new handle')
            pool.submit(handle)

            await asyncio.sleep(0.1)

        # opt 2
        futs = []
        while 1:
            print(os.getpid(), 'MAIN is starting new handle')
            futs.append(pool.submit(handle))

            for i in range(len(futs)-1, -1, -1):
                if futs[i].done():
                    res = futs.pop(i).result()     # not block here longer
                    print(f'{res} is finished. rest {len(futs)}')

            await asyncio.sleep(0.5)



        # opt 1
        while 1:
            print(os.getpid(), 'MAIN still working')
            fut = pool.submit(handle)
            print(f'done={fut.done()}, running={fut.running()}')
            res = fut.result()     # block here
            print(f'done={fut.done()}, running={fut.running()}, res={res}')


asyncio.run(main())
