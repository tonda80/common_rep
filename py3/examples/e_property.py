
# https://docs.python.org/3/library/functions.html#property
# https://www.programiz.com/python-programming/property


class C:
	def get(self):
		return 7

	def set_(self, v):
		print('attempt to set to', v)

	p = property(get, set_)

	# то же самое через декораторы
	@property
	def attr(self):
		return 'qq'

	@attr.setter
	def attr(self, v):
		print('attr attempt to set to', v)


	# или только геттер
	@property
	def read_only(self):
		return 12



c = C()

c.p = 44
print('c.p =', c.p)

c.attr = 'zz'
print('c.attr =', c.attr)

print('c.read_only =', c.read_only)
c.read_only = 44		# AttributeError
