# тренируемся работать с pydantic валидацией на параметрах isp migrate скрипта

from pydantic import BaseModel, Field, validator, ValidationError
from enum import Enum
from typing import Literal, Any, Union


JsonType = Union[None, int, str, bool, list[object], dict[str, object]]			# 2-е приближение


class Action(str, Enum):
	IMPORT = 'import'
	EXPORT = 'export'


class Parameters(BaseModel):
	action: Action
	#action: Literal['import', 'export']

	platform: str
	@validator('platform')
	def check_platform(cls, v):
		if v.endswith('_err'):
			raise ValueError('validation error message here')
		return v

	data: list[str]
	# validate strings

	transport: list[JsonType]


	class NestedParameters(BaseModel):
		np1: int = 0
		np2: str = 'x'
	nest1: NestedParameters
	nest2: NestedParameters = NestedParameters()



if __name__ == '__main__':
	try:
		par = Parameters(
			action = 'export',
			platform = 'pl',
			data = [1, 2, 3],
			transport = [{'k1': 'v1'}, {'k11': 'v11', 'k12': 4}],
			nest1 = {'np1': 1, 'np2': 'v'},
		)
	except ValidationError as e:
		print(e)
		exit(1)


	print(f"par = [{par}]")


	print(dir(Parameters))
