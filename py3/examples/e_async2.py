# экспериментируем с примерами из статьи
# https://www.twilio.com/blog/asynchronous-http-requests-in-python-with-aiohttp

import aiohttp
import asyncio
import time
import sys
import requests


N_REQS = 50             # 150 ok

def pokemon_url(number):
	return f'https://pokeapi.co/api/v2/pokemon/{number}'

async def get_pokemon_async(session, number):
	async with session.get(pokemon_url(number)) as resp:
		pokemon = await resp.json()
		return pokemon['name']

async def get_pokemon_sync(session, number):
	if session is None:
		resp = requests.get(pokemon_url(number))
	else:
		resp = session.get(pokemon_url(number))
	pokemon = resp.json()['name']
	return pokemon['name']


async def main1():
	# совсем асинхронный
	async with aiohttp.ClientSession() as session:
		tasks = []
		async for number in range(1, N_REQS + 1):
			tasks.append(get_pokemon_async(session, number))          # не понял зачем тут было asyncio.ensure_future

		original_pokemon = await asyncio.gather(*tasks)
		for pokemon in original_pokemon:
			print(pokemon)


async def main2():
	# тупой асинхронный, работает быстрее синхронного из исходного примера (main4) только за счет одной сессии
	async with aiohttp.ClientSession() as session:
		for number in range(1, N_REQS + 1):
			pokemon = await get_pokemon_async(session, number)
			print(pokemon)


async def main3():
	# вообще не асинхронный, но практически такой же по скорости что и 2
	with requests.Session() as session:
		for number in range(1, N_REQS + 1):
			resp = session.get(pokemon_url(number))
			pokemon = resp.json()['name']
			print(pokemon)


async def main4():
	# не асинхронный и не сессионный, из статьи
	for number in range(1, N_REQS + 1):
		resp = requests.get(pokemon_url(number))
		pokemon = resp.json()['name']
		print(pokemon)



way = sys.argv[1]
main = locals()['main' + way]

start_time = time.time()
asyncio.run(main())
t = time.time() - start_time
print(f'--- Way {way} takes {t} seconds ---')
