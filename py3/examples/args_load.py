# тренируемся читать аргументы из файла
# https://stackoverflow.com/a/27434050


import argparse
import yaml


def load_yaml(path):
	with open(path) as f:
		return yaml.safe_load(f)


# https://docs.python.org/3/library/argparse.html#argparse.Action
class ReadArgsFromFileAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string = None):
		yaml_data = load_yaml(values)
		print('__debug', parser, namespace, values, option_string, yaml_data)

		opt = 1

		# вариант в лоб. плохо что не работает валидация
		if opt == 1:
			for k, v in yaml_data.items():
				if hasattr(namespace, k) and getattr(namespace, k) is None:
					setattr(namespace, k, v)
		# прогоняем через парсер, для контроля. непонятно как быть со store_*, nargs, ... непонятно как некостыльно формировать имена аргументов
		elif opt == 2:
			parser_args = []
			for k, v in yaml_data.items():
				if hasattr(namespace, k):
					parser_args.extend((f'-{k}', str(v)))		# аргументы только с одним -
			parser.parse_args(parser_args, namespace)



parser = argparse.ArgumentParser()
parser.add_argument('-a1', required=True)		# и required портит все(
parser.add_argument('-a2', action='store_true')
parser.add_argument('-a3', default='a3 defvalue')
parser.add_argument('-a4', type=int, choices=(1, 2, 3))

parser.add_argument('-af', action=ReadArgsFromFileAction, help='Файл из которого можем читать аргументы')

args = parser.parse_args()
print('=====\n', args, sep='\n')
