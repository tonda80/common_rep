#!/usr/bin/env python3

# who i am


import sys

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('a3',  help='')
		self.parser.add_argument('--a', required=True, help='')
		self.parser.add_argument('--a2', action='store_true', help='')

	def main(self):
		print(f'Hi')


class MultiTargetApp(BaseConsoleApp):
	def add_arguments(self):
		trg_parser1 = self.add_subparser('trg1', self.target1_main, help='some target 1')
		trg_parser1.add_argument('--a1', required=True, help='')
		trg_parser1.add_argument('--a2', action='store_true', help='')

		trg_parser2 = self.add_subparser('trg2', self.target2_main, help='another target')
		trg_parser2.add_argument('b1')
		trg_parser2.add_argument('--b2', action='store_true', help='')

		self.parser.add_argument('--c', required=True, help='Общий для обеих таргетов аргумент, в командной строке указывается до задания таргета')

	def main(self):
		# тут общий код, например обработка общих аргументов
		return self.sub_main()

	def target1_main(self):
		print('target1: делаем что-то полезное', self.args.a1, self.args.a2, self.args.c)

	def target2_main(self):
		print('target2: делаем что-то другое, не менее полезное', self.args.b1, self.args.b2, self.args.c)



# заготовки, если вдруг не хотим использовать BaseConsoleApp

def init_logger(level = logging.INFO, root_level = logging.WARNING, name = 'App'):
	logging.basicConfig(format='[%(asctime)s.%(msecs)03d][%(levelname)s] %(message)s', datefmt='%d %b %H:%M:%S')

	log = logging.getLogger(name)

	log.setLevel(level)
	logging.getLogger().setLevel(root_level)

	return log


def init_args(arg_src = None):
	parser = argparse.ArgumentParser()
	# parser.add_argument('--pb', dest='protobuf_dir', required=True, help='Directory with compiled proto files')
	# parser1.add_argument('--a2', action='store_true', help='')

	return parser.parse_args(arg_src)



if __name__ == '__main__':
	MultiTargetApp().start()
	sys.exit(App(("--a", "33", "fake_a3")).start())
