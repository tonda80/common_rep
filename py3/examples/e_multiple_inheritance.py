#!/usr/bin/env python3

# множественное наследование
#
# https://rhettinger.wordpress.com/2011/05/26/super-considered-super/
# https://stackoverflow.com/questions/9575409/calling-parent-class-init-with-multiple-inheritance-whats-the-right-way



class B1(object):
	def __init__(self):
		print('B1.__init__')
		super().__init__()
		print('B1.__init__ done')

class B2(object):
	def __init__(self):
		print('B2.__init__')
		super().__init__()
		print('B2.__init__ done')

# если не описывать конструктор D, то будет вызван только конструктор первого базового класса
class D(B1, B2):
	def __init__(self):
		print('D.__init__')

		super().__init__()

		# если у базовых классов есть super().__init__ то тут будут спецэффекты
		#B1.__init__(self)
		#B2.__init__(self)

		# или можно вызвать явно если базовые классы не вызывают супер
		#B1.__init__(self)
		#B2.__init__(self)
		# тоже явный вызов но с применением super
		#super().__init__()
		#super(B1, self).__init__()

		print('D.__init__ done')


if __name__ == '__main__':
	d = D()