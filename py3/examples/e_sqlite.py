#!/usr/bin/env python3

import sqlite3


conn = sqlite3.connect(':memory:')
curs = conn.cursor()

def execute_generator(sqlite_str, *args):
	for row in curs.execute(sqlite_str, args):
		yield row

def ex(sqlite_str, *args):
	print(tuple(execute_generator(sqlite_str, *args)))


# в 1-й функции для ipython
def db_worker(db=':memory:'):
	conn = sqlite3.connect(db)
	curs = conn.cursor()
	def exec_(sql_str, *args):
		for row in curs.execute(sql_str, args):
			print(row)
	return exec_


# чтобы удалить содержимое memory базы, надо сделать conn.close() (возможно и conn.commit())
# curs.execute возвращает тот же curs
# select name from sqlite_master where type = 'table';		список таблиц

# 'CREATE TABLE IF NOT EXISTS t (key TEXT, value TEXT);'
# 'INSERT INTO t (key, value) VALUES (?1, ?2);'
# 'SELECT * FROM t WHERE key = ?1;'
# 'SELECT * FROM t LIMIT 10;'
# 'UPDATE t SET value = ?2 WHERE key = ?1;'
# 'DELETE FROM t WHERE id = ?1;'
