#!/usr/bin/env python3

# docker run -p 5672:5672 -p 15672:15672 --name rabbitmq --rm rabbitmq:3.9.8-management
# <>-management если нужна вебморда на http://localhost:15672/#/

# https://www.rabbitmq.com/tutorials/amqp-concepts.html
# тут описана суть: типы exchanges, очереди, каналы итд

# https://www.rabbitmq.com/getstarted.html
# https://www.rabbitmq.com/tutorials/tutorial-one-python.html
#    сам пример
# https://www.rabbitmq.com/tutorials/tutorial-two-python.html
#    как не терять сообщения при рестарте rabbitmq
#    настройка диспетчеризации
# https://www.rabbitmq.com/tutorials/tutorial-three-python.html
#    exchanges - их типы итд
#    publish/subscribe паттерн
# https://www.rabbitmq.com/tutorials/tutorial-six-python.html
#    rpc request/reply паттерн

# https://www.cloudamqp.com/blog/part4-rabbitmq-for-beginners-exchanges-routing-keys-bindings.html
# и тут ещё хорошо разрисованы понятия: exchange, queue, routing key

# Заметки
# Канал это типа логическое легковесное соединение (экономим порты и ресурсы TCP)
# Очереди и обменники, это как бы независимые сущности. Связываются отдельной командой queue_bind и очередь может быть связана с многими обменниками, обратное очевидно тоже верно. Сообщения шлются в обменник, попадают по правилам маршрутизации (зависят от типа обменника) в очереди и уже оттуда потребляются потребителями.
# Тип существующего обменника поменять нельзя: direct - строгое равенство для routing_key, topic - задание routing_key по маске, fanout - игнор routing_key.
# topic задаем при подписке! Это строка с точками разделителями слов, * - любое слово, # - любое количество слов. topic без # и * как direct, # как fanout.
# fanout самый быстрый, игнорирует routing_key.
# К одной очереди может быть подключено несколько потребителей, тогда они будут получать сообщения по очереди.
# Для вещания на несколько приемников надо создавать несколько очередей. fanout обменник доставляет сообщение во все связанные очереди. С topic можно гибче настроить прием. Но можно использовать и direct, и биндить разные очереди с одним routing_key.
# Для подключения можно использовать временные очереди (пустое имя для автогенерации оного и флаг exclusive для автоудаления)



import pika		# pip3 install pika

import sys
import time

from baseapp import BaseConsoleApp



class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-q', '--queue_name', default='py_test_queue', help='Имя очереди')
		self.parser.add_argument('-e', '--exchange_name', default='py_test_exchange', help='Имя обменника')
		self.parser.add_argument('-et', '--exchange_type', default='direct', choices=('direct', 'fanout', 'topic', 'headers'), help='Тип обменника')
		self.parser.add_argument('-rk', '--routing_key', help='routing_key для отправки и бинда')

		self.parser.add_argument('-hb', '--heartbeat', default=0, type=int, help='Use 0 to deactivate heartbeats and None (тут -1) to always accept the broker\'s proposal')

		self.parser.add_argument('--new', action='store_true', help='Удаляем обменник и очередь на старте')

		trg_parser1 = self.add_subparser('send', self.sender, help='Sender from tutorial 1')
		trg_parser2 = self.add_subparser('recv', self.receiver, help='Receiver from tutorial 1')

		trg_parser3 = self.add_subparser('rpcs', self.rpc_server, help='RPC server from tutorial 6')
		trg_parser4 = self.add_subparser('rpcc', self.rpc_client, help='RPC client from tutorial 6')

	def app_init(self):
		self.connection = None
		self.msg_count = 0


	def cleanup(self):
		if self.connection:
			self.connection.close()


	def get_message(self):
		self.msg_count += 1
		msg = input('> ')
		if not msg:
			msg = f'message_{self.msg_count}'
		return msg


	def main(self):
		heartbeat = self.args.heartbeat if self.args.heartbeat >= 0 else None
		# heartbeat задает таймаут отключения без сообщений (pika.exceptions.StreamLostError: Transport indicated EOF на отправителе при отключении)
		# см help pika.ConnectionParameters
		self.log.debug(f'heartbeat={heartbeat}')

		self.exchange = self.args.exchange_name
		self.queue = self.args.queue_name
		self.routing_key = self.args.routing_key

		self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost', heartbeat=heartbeat))

		self.channel = self.connection.channel()

		if self.args.new:
			self.channel.exchange_delete(self.exchange)
			self.channel.queue_delete(self.queue)

		self.channel.queue_declare(queue=self.queue)
		#input('press to enter')										# если не биндить, биндится к дефолтному

		if self.exchange:															# для дефолтного будет ошибка
			self.channel.exchange_declare(exchange=self.exchange, exchange_type=self.args.exchange_type)
			self.channel.queue_bind(exchange=self.exchange, queue=self.queue, routing_key=self.routing_key)
			# если routing_key отсутсвует, то равен queue см исходники

		return self.sub_main()

	def sender(self):
		print('\nstarted publish loop (ctrl-c to stop)')
		while 1:
			msg = self.get_message()
			routing_key = self.routing_key if self.routing_key is not None else self.queue

			self.channel.basic_publish(exchange=self.exchange, routing_key=routing_key, body=msg)
			print(f' sent "{msg}", routing_key="{routing_key}"')

	def receiver(self):
		def consume_callback(ch, method, properties, body):
			print('recieved:', ch, method, properties, body, sep='\n  --> ')

		self.channel.basic_consume(queue=self.queue, auto_ack=True, on_message_callback=consume_callback)

		print('\nstarted consume loop (ctrl-c to stop)')
		self.channel.start_consuming()


	def rpc_server(self):
		def request_executor(req):
			resp = f'response_to_{req}'
			#time.sleep(1)
			return resp

		def on_request(ch, method, props, body):
			print(f'rpc_server got request {body}')

			resp = request_executor(body)

			ch.basic_publish(exchange=self.exchange,
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=resp)
			ch.basic_ack(delivery_tag=method.delivery_tag)
			print(f'rpc_server replied by {resp} (reply_to={props.reply_to}, correlation_id={props.correlation_id}, delivery_tag={method.delivery_tag})')



		self.channel.basic_qos(prefetch_count=1)
		self.channel.basic_consume(queue=self.queue, on_message_callback=on_request)

		print('\nstarted RPC request listener loop (ctrl-c to stop)')
		self.channel.start_consuming()


	def rpc_client(self):
		res = self.channel.queue_declare(queue='', exclusive=True)
		self.callback_queue = res.method.queue

		responce = None
		correlation_ids = set()
		def on_response(ch, method, props, body):
			nonlocal responce, correlation_ids
			if props.correlation_id in correlation_ids:
				print(f'received responce for {props.correlation_id}: {body}')
				correlation_ids.remove(props.correlation_id)
				responce = body

		self.channel.basic_consume(queue=self.callback_queue,
					on_message_callback=on_response,
					auto_ack=True)
        # создали ещё одну callback очередь и "подписались" на сообщения из неё

		print('\nstarted publish request loop (ctrl-c to stop)')
		while 1:
			req = self.get_message()

			correlation_id = f'corr_id_{self.msg_count}'
			assert correlation_id not in correlation_ids
			correlation_ids.add(correlation_id)

			self.channel.basic_publish(exchange=self.exchange,
					routing_key=self.queue,
					properties=pika.BasicProperties(reply_to=self.callback_queue, correlation_id=correlation_id),
					body=req)

			print(f' sent request "{req}"')

			if 0:
				self.connection.process_data_events()		# получаем ответы только после юзерского ввода, но зато отправляем много запросов. пусть так для простоты
			else:
				while responce is None:						# один запрос и ждем ответ
					self.connection.process_data_events()
				responce = None


if __name__ == '__main__':
	sys.exit(App().start())
