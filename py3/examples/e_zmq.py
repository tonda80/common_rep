#!/usr/bin/env python3
# coding=utf8


import time
import zmq

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-p', '--port', type=int, default='21345')
		self.parser.add_argument('--ip', default='127.0.0.1')
		self.parser.add_argument('target', choices=('req', 'rep', 'pub', 'sub'))
		self.parser.add_argument('--TODO', help='Bind or connect socket')

	def main(self):
		zmq_context = zmq.Context()
		ep = f'tcp://{self.args.ip}:{self.args.port}'
		{
			'req' : self.req_demo,
			'rep' : self.rep_demo,
			'pub' : self.pub_demo,
			'sub' : self.sub_demo,
		}[self.args.target](zmq_context, ep)

	def user_message(self):
		return input('> ').encode()

	def req_demo(self, zmq_context, ep):
		socket = zmq_context.socket(zmq.REQ)
		socket.connect(ep)
		self.log.info(f'connected to {ep}')
		while 1:
			msg = self.user_message()
			socket.send(self)
			print(f'[{msg}] was sent')
			while 1:												# send без успешного recv-а вызывает исключение
				msg_in = self.recv_with_timeout(socket, 5000)		# socket.recv() для вечного ожидания
				if msg_in is not None:
					print(f'[{msg_in}] was received')
					break
				print('recv timeout')

	def rep_demo(self, zmq_context, ep):
		socket = zmq_context.socket(zmq.REP)
		socket.bind(ep)
		self.log.info(f'bound to {ep}')
		while 1:
			msg_in = socket.recv()
			print(f'[{msg_in}] was received')
			msg = f'reply to [{msg_in}]'.encode()
			socket.send(msg)
			print(f'[{msg}] was sent')

	def recv_with_timeout(self, socket, msec):
		if socket.poll(msec) & zmq.POLLIN:		# 2-й параметр poll-а - flags : int [default: POLLIN]
			return socket.recv()

	def recv_noblock(self, socket):
		try:
			return socket.recv(zmq.NOBLOCK)
		except zmq.ZMQError:
			pass

	# для теста, не надо это использовать,
	def recv_with_timeout2(self, socket, msec):
		till = time.time() + msec/1000
		while 1:
			try:
				return socket.recv(zmq.NOBLOCK)
			except zmq.ZMQError:
				if time.time() > till:
					return
			time.sleep(0.2)

	def pub_demo(self, zmq_context, ep):
		socket = zmq_context.socket(zmq.PUB)
		socket.bind(ep)			# TODO custom it
		self.log.info(f'ready for pub to {ep}')
		while 1:
			msg = self.user_message()
			socket.send(msg)
			print(f'[{msg}] was published')

	def sub_demo(self, zmq_context, ep):
		socket = zmq_context.socket(zmq.SUB)
		socket.connect(ep)							# TODO custom it
		socket.setsockopt(zmq.SUBSCRIBE, b'')		# TODO? custom too
		self.log.info(f'subcribed to {ep}')
		while True:
			msg_in = socket.recv()
			print(f'[{msg_in}] was received')

if __name__ == '__main__':
	try:
		App().main()
	except KeyboardInterrupt:
		pass
