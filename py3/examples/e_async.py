#!/usr/bin/env python3

import asyncio
import random
import time
import threading

# Конспект смотри в asyncio.md


# корутинная функция
async def async_func(n, d=0.5, *, exc_test = 0):
	def maybe_raise(k):
		if exc_test == k:
			raise RuntimeError(f'exception {exc_test} into async code with {n}!')

	print(f'async_func({n}) begin')

	maybe_raise(1)

	await asyncio.sleep(d)

	maybe_raise(2)

	print(f'async_func({n}) end')
	return n*2


async def main():
	await async_func(1)
	print('дождались await и продолжаем')

	await async_func(2)
	print('и этого дождались')

	try:
		await async_func(2.1, exc_test=1)
	except Exception as e:
		print('а тут исключение!', e)

	print()
	print('теперь запустим конкурентно')
	tasks = []
	for i in range(3, 8):
		try:
			tasks.append(asyncio.create_task(async_func(i, 2-i*0.1), name=str(i)))
		except:
			raise RuntimeError('а сюда то не попадаем!!')
			# добавь  `, exc_test = 2 if i == 3 else 0` чтобы увидеть что исключение вылезет не здесь, а в run!!!!

	# если таску не ждать, то программа закончится не успев напечатать '..  end' строчки
	for t in tasks:
		print(f'task {t.get_name()} done = {t.done()}')		# тут нельзя получать результат, будет исключение
		await t
		print(f'task {t.get_name()} result = {t.result()}')	# если отменен или кинул исключение тут тоже будет исключение
	print('другое дело!')

	# в примерах выше, тут суть в том что 1-е await-ы запускают и ждут выполнения, т.е. общее время ожидания равно суммме времени всех корутин
	# а во втором случае мы сперва запускаем корутины на выполнение, а потом их все ждем, т.е. общее время ожидания равно времени максимальной корутины (в нашем случае время ожидания = работе)

	print()
	task = asyncio.create_task(asyncio.sleep(1, 'РЕЗУЛЬТАТ!'))
	await task
	print('async.sleep умеет возвращать', task.result())

	print()
	acc_res = await asyncio.gather(async_func(8), async_func(9))
	print('gather умеет запускать кучу тасков и аккумулировать результат:', acc_res)
	#
	# gather возвращает future и если его не дождаться (tmt = 10) - в конце будет ошибка в выводе (вроде подавленное исключение)
	tmt = 0
	acc_res2 = asyncio.gather(async_func(80, tmt), async_func(90, tmt))
	print('raw gather result', acc_res2)


	print()
	res = await asyncio.create_task(async_func(10))
	print('и только теперь я понял что и так можно:', res)
	print('отдельно заметь как интересно "сломался" вывод из-за прошлого "нежданного" gather')

	print()
	try:
		await asyncio.wait_for(async_func(11, 1000), timeout=1)
	except asyncio.TimeoutError:
		print('wait_for отменяет таски по таймауту')

	print()
	res = await test_async_func(12)
	print('вот он результат работы корутины из обычного метода!', res)


# пытаемся тут решить задачу вызова корутины и получения результат её работы из обычной функции
def test_async_func(n):
	print(f'test_async_func begin')
	crt_delay = 2

	#r = asyncio.get_running_loop().run_until_complete(async_func(n, crt_delay))		# так нельзя - RuntimeError: This event loop is already running

	# так тоже нельзя - висим тут вечно ибо нет await-a чтобы переключится с текущей корутину на новую
	#t = asyncio.get_running_loop().create_task(async_func(n, crt_delay))
	#while not t.done():
	#	time.sleep(0.5)
	#	print('waiting')
	#r = t.result()

	# похоже прямо тут - это сделать невозможно, потому что с одной стороны надо как то бросить выполнение, чтобы дать выполниться новой корутине,
	# но в то же время нужно остаться тут, чтоб получить результат, а для такого фокуса и нужен async def
	# нужно или городить потоки или вот так и потом ждать результата снаружи

	t = asyncio.get_running_loop().create_task(async_func(n, crt_delay))
	r = None

	print(f'test_async_func end', r)

	return t


if __name__ == '__main__':
	try:
		asyncio.run(main())
	except Exception as e:
		print('ИСКЛЮЧЕНИЕ В asyncio.run!', e)

	# asyncio.run(main())			# так можно, но типа нехорошо
