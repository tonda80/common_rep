
# соберем тут наработки которые не пригодились и не тянут на e_*.py,
# но  выкидывать совсеим их жалко ибо таки могут пригодится


# импортируем в глобальное пространстов имен внутри функции
# dir_ - директория в которой есть anvil/groups/group_service_pb2.py
def import_proto(dir_):
	global anvil

	if not os.path.isdir(dir_):
		raise RuntimeError('Incorrect protobuf files directory')

	sys.path.append(dir_)

	import anvil.groups.group_service_pb2



# хотим чтобы unittest не только получил аргументы строки, но даже показал свой хелп по -h
def init_args(arg_src = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('--pb', dest='protobuf_dir', required=True, help='Directory with compiled proto files')

	try:
		args, rest = parser.parse_known_args(arg_src)
	except SystemExit as e:		# либо -h, либо не хватает необходимых аргументов
		if e.code != 0:
			raise		# не хватает аргументов
		print('\n\n+++++++ UNITTEST HELP +++++++\n\n')
		return None, arg_src
	rest.insert(0, sys.argv[0])		# unittest хочет чтобы было имя скрипта

	return args, rest
#
# и использование
# args, unittest_argv = cls.init_args()
# ....
# unittest.main(argv=unittest_argv)
