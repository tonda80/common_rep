#!/usr/bin/env python3

# https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-Your-first-Bot
# https://github.com/python-telegram-bot/python-telegram-bot


import os
import logging

from telegram.ext import Updater
from telegram.ext import CommandHandler, MessageHandler, Filters



def init_logger(name = 'App', level = logging.INFO, root_level = logging.WARNING):
	logging.basicConfig(format='[%(asctime)s.%(msecs)d][%(levelname)s] %(message)s',
			datefmt='%d %b %H:%M:%S')

	log = logging.getLogger(name)

	log.setLevel(level)
	logging.getLogger().setLevel(root_level)

	return log

log = init_logger()	#root_level = logging.DEBUG


token = os.environ.get('TGBOT_TOKEN')
if not token:
	raise RuntimeError('no token')

updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher



def start(update, context):
	context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")
dispatcher.add_handler(CommandHandler(['s', 'start'], start))


updater.start_polling()		# как видим неблокирующий, можно вызывать add_handler и до и после


def help_handler(update, context):
	context.bot.send_message(chat_id=update.effective_chat.id, text='go away')
dispatcher.add_handler(CommandHandler('help', help_handler))


def meow(update, context):
	#log.info(update)	# то 'edited_message', то 'message'! но вроде работают update.effective_ атрибуты
	#log.info(dir(update))
	log.info(update.effective_chat.first_name)
	log.info(update.effective_chat.id)
	log.info(update.effective_message.text)
	context.bot.send_message(chat_id=update.effective_chat.id, text='bow-wow')
	# вот так оно работает
	print('__deb new', update.message is update.effective_message)
	print('__deb edited', update.edited_message is update.effective_message)
dispatcher.add_handler(CommandHandler(['m', 'meow'], meow))


# дефолтовый обработчик команд. он должен быть добавлен последним, добавленное после него скрывается
def unknown_handler(update, context):
	context.bot.send_message(chat_id=update.effective_chat.id, text='Sorry, what do you say??')
dispatcher.add_handler(MessageHandler(Filters.command, unknown_handler))
