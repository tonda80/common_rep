# контекст менеджер (наверняка уже есть но пусть будет ещё)

import time
import signal
import sys


class CtxMgr:
	def __init__(self, *args):
		self.args = args
		print(f'\nCtxMgr.__init__{args}')

	def __enter__(self):
		print(f'CtxMgr.__enter__() {self.args}')
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		print(f'CtxMgr.__exit__({exc_type, exc_value, traceback}) {self.args}')
		return 'no_exc' in self.args	# вернуть True чтобы не передавать исключение

	def do_something(self):
		print(f'CtxMgr.do_something() {self.args}')

		if 'hang' in self.args:
			while 1:
				print('i am hanging')
				time.sleep(3)


with CtxMgr(1) as cm1:
	cm1.do_something()

with CtxMgr(2, 'no_exc') as cm2:
	cm2.do_something()
	cm2.raise_exc_by_nonexistent_method()

#with CtxMgr(3) as cm3:
#	cm3.do_something()
#	cm3.raise_exc_by_nonexistent_method()

def sighandler(sig, frame):
	print(f'received signal {sig}')
	sys.exit(1)
signal.signal(signal.SIGTERM, sighandler)
signal.signal(signal.SIGINT, sighandler)

with CtxMgr(4, 'hang') as cm4:
	cm4.do_something()
