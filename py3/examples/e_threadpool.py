from concurrent.futures import ThreadPoolExecutor
import time
import random
import itertools

def func(i, j):
    print('Started', i, j)
    time.sleep(1 + random.random()/5)
    if i == 5:
        raise RuntimeError(f'thread error! ({i})')
    print('Stopped', i)
    return i + j

N_THREADS = 3
with ThreadPoolExecutor(max_workers = 3) as pool:
    it = pool.map(func, range(10), itertools.repeat(7))
    # timeout = 3 срабатывает при next на it
    print('map returns', type(it))

print('`with` waits all executions')

# если только получить исключение
#for i in it: pass

# нельзя получить результаты после исключения
# только использовать сабмит https://stackoverflow.com/questions/51071378/exception-handling-in-concurrent-futures-executor-map
while 1:
    try:
        i = next(it)
    except StopIteration:
        print('__deb stopiter')
        break
    except Exception as e:
        print(e)
        continue
    print('returned', i, type(i))
