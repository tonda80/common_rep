# шпаргалка для dataclass

from dataclasses import dataclass, asdict
import json


@dataclass
class Struct:
    s1: str
    i: int = -1		# дефолт значения обязательны, чтобы явно не указывать поле в конструкторе
    s2: str = None

d1 = Struct(s1='q', s2='w')

print(f'd1={d1}')

d1_json = json.dumps(asdict(d1))

print(f'd1 json is \'{d1_json}\'')


