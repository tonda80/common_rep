# эксперимент c __await__
# https://jacobtomlinson.dev/posts/2021/awaitable-objects-and-async-context-managers-in-python/
# https://github.com/noirello/bonsai/tree/v1.5.1/src    вот тут я такое увидел, и ещё интересно бы понять как они си код используют

import asyncio


class Hello:
    def __init__(self):
        print("init")

    def __await__(self):
        async def closure():
            print("await")
            return self

        return closure().__await__()

    async def method(self):
        print("method")


async def main():
    h = await Hello()       # без этого не работает. Понять, почему так. В bonsai такой объект под async with
    await h.method()


if __name__ == "__main__":
    asyncio.run(main())
