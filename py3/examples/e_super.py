#!/usr/bin/env python3


class B1:
	def f(self):
		print('B1.f')
		super().f()

class B2:
	def f(self):
		print('B2.f')
		super().f()

		# вообще супер это загадочный объект
		#print(dir(super()))
		has_dict2 = hasattr(super(), '__dict__')
		print(f'\t[B2] super() has {"" if has_dict2 else "NO "} __dict__ attribute')
		# НО
		has_dict1 = '__dict__' in dir(super())
		print(f'\t[B2] super() has {"" if has_dict1 else "NO "} __dict__ in dir output')


class B3:
	def f(self):
		print('B3.f')
		# это не работает super().f()

		has_dict2 = hasattr(super(), '__dict__')
		print(f'\t[B3] super() has {"" if has_dict2 else "NO "} __dict__ attribute')


class D(B1, B2, B3):
	def f(self):
		print('D.f')
		super().f()


D().f()

