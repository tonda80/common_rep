

def deco1(f):
	def ret():
		print('decorated by deco1:', end=' ')
		f()
	return ret


def deco2(n):
	def deco(f):
		def ret():
			print(f'decorated by deco2 with n={n}:', end=' ')
			f()
		return ret
	return deco


# ----


def func():
	print('func')

@deco1
def func1():
	print('func1')

@deco2(777)
def func2():
	print('func2')

@deco1
@deco2(8)
def func3():
	print('func3')

@deco2(1)
@deco1
@deco2(2)
def func3():
	print('func3')

# ----


func()
func1()
func2()
func3()
