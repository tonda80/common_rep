# btc эксперименты
import ecdsa
import base58
import cryptos
from hashlib import sha256

# https://github.com/bitcoinbook/bitcoinbook
#		тут вроде норм теория
# https://github.com/primal100/pybitcointools
#		либа с реализацией
# https://en.bitcoinwiki.org/wiki/Bitcoin_address
#		и тут теория


# https://www.freecodecamp.org/news/how-to-generate-your-very-own-bitcoin-private-key-7ad0f4936e6c/
# на сайте bitaddress.org сгенерил пары без BIP38
btc_addr = '1MBC1dpdxUYwkjYGbWWhrLNtGxwLwroWhg'
priv_key = 'KxmACwVyBXi86NpAi3hFZwvTjBqc2JL3RXmKe1QcGnNz8ocCDQSs'		# Private Key WIF Compressed 52 characters base58, starts with a 'K' or 'L'


# https://rosettacode.org/wiki/Bitcoin/address_validation#Python
# the last four bytes are a checksum check. They are the first four bytes of a double SHA-256 digest of the previous 21 bytes.
def validate_btc_pub_addr(addr):
	bcbytes = base58.b58decode(addr)		# base58.b58decode работает норм, так же как самопальное
	hash_of_beg = sha256(bcbytes[:-4]).digest()
	double_hash_of_beg = sha256(hash_of_beg).digest()
	return bcbytes[-4:] == double_hash_of_beg[:4]

res_val = validate_btc_pub_addr(btc_addr)
assert res_val, 'btc_addr is valid'


# это в лоб - не работает - там надо ещё выдернуть ключи из формата
# https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch04.asciidoc
# +
# https://stackoverflow.com/questions/34451214/how-to-sign-and-verify-signature-with-ecdsa-in-python
# https://stackoverflow.com/a/41432138

# используем cryptos и https://github.com/bitcoinbook/bitcoinbook/blob/173974f69e263c7de536a334224d642e6dca7d71/code/key-to-address-ecc-example.py#L2
# чтобы проверить что нам сайт дал правильные пары, а точнее чтобы хоть что то тут начать понимать

decoded_private_key = cryptos.decode_privkey(priv_key)
print("Private Key is:", decoded_private_key)

# и далее как в примере
public_key = cryptos.fast_multiply(cryptos.G, decoded_private_key)
print("Public Key (x,y) coordinates is:", public_key)

(public_key_x, public_key_y) = public_key
compressed_prefix = '02' if (public_key_y % 2) == 0 else '03'
hex_compressed_public_key = compressed_prefix + (cryptos.encode(public_key_x, 16).zfill(64))
print("Compressed Public Key (hex) is:", hex_compressed_public_key)
compressed_btc_addr = cryptos.pubkey_to_address(hex_compressed_public_key)
print("Compressed Bitcoin Address (b58check) is:", compressed_btc_addr)

assert compressed_btc_addr == btc_addr, 'рассчитали то же что и bitaddress.org'

