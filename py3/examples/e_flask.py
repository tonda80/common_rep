#!/usr/bin/env python3

# https://flask-russian-docs.readthedocs.io/ru/latest/quickstart.html
# https://flask.palletsprojects.com/en/1.1.x/

import flask						# pip install Flask
from flask import request


app = flask.Flask(__name__)

@app.route('/')
def hello_world(*a, **kw):			# flask.request - глобальный thread local объект представляющий текущий запрос https://werkzeug.palletsprojects.com/en/1.0.x/wrappers/#werkzeug.wrappers.Request
    return f'''Hello world!<br/>
<br/>
dir(request) = {dir(request)}
<br/><br/>
a = {str(a)}
<br/>
kw = {str(kw)}
<br/><br/>
например, request.method={request.method}
'''

@app.route('/fixed/route')
def fixed_route1(*a, **kw):
    return 'fixed route1'

@app.route('/<var1>/<var2>')
def variable_route(var1, **kw):
    return f'''
<b>variable url parts</b> <br/>
var1={var1} {kw}<br/>
'''

@app.route('/fixed/route')
def fixed_route2(*a, **kw):
    return 'а это уже НЕ РАБОТАЕТ из-за fixed_route1'

@app.route('/dl/<path:filename>')
def download_file(filename):
	# return f'{filename}'
	stor_dir = '/home/anton/temp'
	fname = '1.jpeg'
	as_attachment = False	# send this file with a Content-Disposition: attachment header. интересно что браузеры запоминают где то у себя если as_attachment был True и чтобы вернуть поведение False надо скинуть историю браузера
	return flask.send_from_directory(stor_dir, fname, as_attachment = as_attachment)


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8080, debug=True)
	# debug позволяет не перезапускать сервер для подхватывания изменений!
	# 0.0.0.0 для слушания запросов извне
