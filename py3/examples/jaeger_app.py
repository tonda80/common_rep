#!/usr/bin/env python3

# https://github.com/yurishkuro/opentracing-tutorial/tree/master/python


import opentracing			# pip3 install opentracing
import jaeger_client		# pip3 install jaeger-client

import sys
import time
import logging
import multiprocessing.connection

from baseapp import BaseConsoleApp

# только для теста энвила (-s1 -at)
import mix_utils
sys.path.append(mix_utils.path_to(__file__, '../job/b2b/anvil_tests'))
import amqp
import config
sys.path.append(config.proto_dir)
import anvil.general_pb2		# pip3 install protobuf
import anvil.groups.group_service_pb2




class App(BaseConsoleApp):
	def log_format(self):
		return {
			'format': '[%(name)s][%(levelname)s] %(asctime)s.%(msecs)d %(message)s',
			'datefmt': '%d %b %H:%M:%S'
		}

	def add_arguments(self):
		self.parser.add_argument('-n', '--service_name', default='pytest', help='Имя сервиса для трейсера')
		self.parser.add_argument('-p', '--port', type=int, default=24555, help='Порт для IPC')

		trg_parser1 = self.add_subparser('s1', self.sender_parent, help='Send parent span')
		trg_parser1.add_argument('-at', '--anvil_test', action='store_true', help='IPC сообщение шлем в b2b_anvil, а не в питон скрипт')

		trg_parser2 = self.add_subparser('s2', self.sender_child, help='Send child span from other process')


	def app_init(self):
		self.conn = None
		self.listener = None

	def cleanup(self):
		if self.conn:
			self.conn.close()

		if self.listener:
			self.listener.close()

		self.root_span.finish()
		time.sleep(1.5)		# https://github.com/jaegertracing/jaeger-client-python/issues/50
		self.tracer.close()


	def dummy_action(self, ms):
		time.sleep(ms/1000)


	def init_tracer(self):
		if self.args.verbose:
			self.set_root_log_level(logging.DEBUG)

		cfg_dict = {
			'service_name': self.args.service_name,
			'sampler': {		# https://www.jaegertracing.io/docs/1.25/sampling/
				'type': 'const', 'param': 1
			},
			'logging': True,#self.args.verbose,
        }

		config = jaeger_client.Config(config=cfg_dict, validate=True)
		return config.initialize_tracer()


	def main(self):
		self.tracer = self.init_tracer()
		root_span_name = 'parent_app' if self.args.target_main == self.sender_parent else 'child_app'
		self.root_span = self.tracer.start_span(root_span_name)		# можно использовать start_active_span для создания спана (контекста) доступного из trace https://github.com/yurishkuro/opentracing-tutorial/tree/master/python/lesson02#propagate-the-in-process-context

		return self.sub_main()


	def sender_parent(self):
		with self.tracer.start_span('say-hello', child_of=self.root_span) as span1:
			t = 30
			self.dummy_action(t)
			span1.set_tag('time', t)
			span1.log_kv({'log_key': 'log_value', 'message': 'log is not tag?'})

			# инжектируем контекст
			carrier = {'k1': 'v1', 'k2':'v2'}
			self.tracer.inject(span1, opentracing.propagation.Format.TEXT_MAP, carrier)
			self.log.info(f'carrier is extended: {carrier}')

			self.send_ipc_message(carrier)


		# просто пример спана без контекст менеджера
		span2 = self.tracer.start_span('say-bye', child_of=self.root_span)
		self.dummy_action(50)
		span2.finish()

		self.log.debug('sender_parent end')
		#input()


	def send_ipc_message(self, carrier):
		if self.args.anvil_test:
			rpc_client = amqp.RpcClient(config.amqp.connection, config.amqp.exchange, config.amqp.req_queue, self.log)
			body = anvil.groups.group_service_pb2.GroupLimitOffset(limit_offset=anvil.general_pb2.LimitOffset(limit=2, offset=0)).SerializeToString()

			rpc_client.sync_request('GroupService.ReadByLimitOffset', body, headers=carrier)

		else:
			self.conn = multiprocessing.connection.Client(('localhost', self.args.port))
			self.conn.send(carrier)		# тут только carrier




	def sender_child(self):
		self.listener = multiprocessing.connection.Listener(('localhost', self.args.port))

		self.log.info(f'start listening of {self.args.port}')

		while True:
			self.conn = self.listener.accept()
			self.log.info(f'accepted connection from f{self.listener.last_accepted}')
			while True:
				try:
					msg = self.conn.recv()
				except EOFError:
					self.log.debug('broken connection')
					break
				self.process_ipc_message(msg)

	def process_ipc_message(self, msg):
		# похоже тут кастомные поля не поддерживаются см /usr/local/lib/python3.8/dist-packages/jaeger_client/codecs.py TextCodec.extract
		carrier = {'uber-trace-id': msg['uber-trace-id']}
		# копируем для экспериментов
		#for k in ('k1', 'k2', 'uber-trace-id'):			# 'k2',
		#	carrier[k] = msg[k]
		#carrier['k2'] = 'wrong value!'

		span_ctx = self.tracer.extract(opentracing.propagation.Format.TEXT_MAP, carrier)
		assert span_ctx
		with self.tracer.start_span('say-hi_in_reply', child_of=span_ctx) as span3:
			t = 70
			self.dummy_action(t)
			span3.set_tag('reply', '1')
			span3.set_tag('time', t)



if __name__ == '__main__':
	sys.exit(App().start())
