#!/usr/bin/env python3

import random
from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-n', type=int, default=10_000_000, help='Количество экземпляров')
		self.parser.add_argument('-s', action='store_true', help='Создаем __slots__ объекты')

	def main(self):
		print(f'creation of {self.args.n} {"" if self.args.s else "NO "}__slots__ items')
		cls = CSlots if self.args.s else CNoSlots
		lst = [cls() for i in range(self.args.n)]
		input('press enter to exit')

		# top -bc -d1 | grep -v grep | grep slots
		# 10M: no slots 1.6g, slots 0.65g


class CNoSlots:
	def __init__(self):
		self.x = random.randrange(100)
		self.y = random.randrange(100)
		#self.z = random.randrange(100)		# no error

class CSlots:
	__slots__ = ('x', 'y')
	def __init__(self):
		self.x = random.randrange(100, 200)
		self.y = random.randrange(100, 200)
		#self.z = random.randrange(100, 200)	# error


if __name__ == '__main__':
	App().main()
