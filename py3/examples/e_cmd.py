#!/usr/bin/env python3

import cmd


class MyShell(cmd.Cmd):
	intro = 'Welcome to ....   Type help or ? to list commands.\n'
	prompt = '> '

	def do_c1(self, arg):
		'Делает полезную работу 1'
		print(f'а вот и работа: arg=\'{arg}\'')

	def do_q(self, arg):
		'Выход'
		print('bye!')
		return True		# выход из cmdloop

	def do_smth(self, arg):
		'что-то'
		self.onecmd('qwe rty uio')

	def do_shell(self, arg):
		'это может быть вызвано с !'
		print(f'do_shell {arg}')

	def precmd(self, line):
		print(f'precmd hook. line={line}')
		return line		# тут можно подправить команду

	def default_commented(self, line):	# default кастомный хендлер для неизвестной команды
		print(f'default. line={line}')
		return line

	def onecmd_commented(self, line):	# onecmd можно использовать как универсальный обработчик. А можно так вызывать выполнение do_* из кода
		print(f'onecmd. line={line}')


if __name__ == '__main__':
	MyShell().cmdloop()
