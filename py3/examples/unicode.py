# соберем тут любопытный юникод код


# демо utf8 https://en.wikipedia.org/wiki/UTF-8#Encoding

print('-- Диапазоны --')

# и тут проверим правильно ли в utf выделили диапазоны :)
def check_utf8_range(first_cp, last_cp, *n_bites):
	global prev_last_cp
	assert prev_last_cp is None or prev_last_cp == first_cp - 1
	prev_last_cp = last_cp

	n_cp = last_cp - first_cp + 1
	n_poss = 2**sum(n_bites)
	print(f'[{first_cp}, {last_cp}] range. {n_cp} <= {n_poss}? {n_cp <= n_poss}')

prev_last_cp = None
check_utf8_range(0x0000,   0x007f,    7)
check_utf8_range(0x0080,   0x07ff,    5, 6)
check_utf8_range(0x0800,   0xffff,    4, 6, 6)
check_utf8_range(0x1_0000, 0x10_ffff, 3, 6, 6, 6)

print('-- Кодировщик --')

def _bits(num, q, start):
	# returns q bits starting from start bit as number
	return (num >> start) & ~(-1 << q)

# byte signs
U8_S2 = 0b110_00000
U8_S3 = 0b1110_0000
U8_S4 = 0b11110_000
U8_SF = 0b10_000000

def cp_to_utf8(cp):
	# codepoint int => byte tuple (big endian)
	assert 0x0 <= cp <= 0x10ffff
	if cp <= 0x7f:
		return (cp, )
	elif cp <= 0x7ff:
		return U8_S2|_bits(cp, 5, 6), U8_SF|_bits(cp, 6, 0)
	elif cp <= 0xffff:
		return U8_S3|_bits(cp, 4, 12), U8_SF|_bits(cp, 6, 6), U8_SF|_bits(cp, 6, 0)
	elif cp <= 0x10ffff:
		return U8_S4 | _bits(cp, 3, 18), U8_SF | _bits(cp, 6, 12), U8_SF | _bits(cp, 6, 6), U8_SF | _bits(cp, 6, 0)

def _test_cp_to_utf8(letter):
	cp = ord(letter)
	bytes_ = cp_to_utf8(cp)
	bs = bytes(bytes_)
	assert bs == letter.encode('utf8')
	print(f'{letter} => {bs}')

_test_cp_to_utf8('q')
_test_cp_to_utf8('f')
_test_cp_to_utf8('ы')
_test_cp_to_utf8('г')
_test_cp_to_utf8('€')
_test_cp_to_utf8('∫')
_test_cp_to_utf8('😐')
_test_cp_to_utf8('🙊')

print('-- Декодировщик --')

def _check_mask_get_rest(num, mask):
	mask2 = (mask >> 1) | 0x80		# e.g 11_0* => 111_0* and so on
	if num & mask2 == mask:
		return num & ~mask

def utf8_to_cp(sym_bs):
	# one symbol byte string => codepoint int
	sym_len = len(sym_bs)
	assert sym_len in (1, 2, 4)

	for i, s in enumerate(U8_S2, U8_S3, U8_S4):
		byte = _check_mask_get_rest(s)
		if byte is not None:
			bytes_ = [byte]
			need_len = i + 2
			break
	else:
		assert False, 'Invalid first byte'

	assert sym_len == need_len

	for i in range(1, need_len):
		byte = _check_mask_get_rest(U8_SF)
		assert byte is not None, f'Invalid {i}th byte'
		bytes_.append(byte)

	# недоделано(
