# пишем лог в journald

import logging


# journalctl -f -overbose -tjournal_log
# journalctl -S -1h  -tjournal_log
SYS_ID = 'journal_log'

log = logging.getLogger('journal_log.py')
#log.propagate = False
log.setLevel(logging.DEBUG)


if 1:   # systemd

    from systemd import journal     # apt install python3-systemd  OR  apt install libsystemd-dev && pip3 install systemd

    log.addHandler(journal.JournalHandler(SYSLOG_IDENTIFIER=SYS_ID, KEY0=100))	# KEY0 будет во всех сообщениях. _SYSTEMD_UNIT='tryrewrite' - не переписывается

if 0:   # logging_journald

    from logging_journald import JournaldLogHandler, check_journal_stream   # pip3 install logging-journald

    log.addHandler(JournaldLogHandler(SYS_ID))


def some_func():
    log.warning('Call from function', extra={'MESSAGE_ID': 'mid4', 'KEY1': 123})


log.warning('Hello journald! I am %s', 'journal_log.py', extra={'MESSAGE_ID': 'mid', 'KEY1': 123})

some_func()

log.debug('Bye journald!', extra={'MESSAGE_ID': 'mid1', 'KEY1': 456})
