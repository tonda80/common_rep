import asyncio
import os
import time

import aioredis


# тут тестовый код для работы, но может когда-нибудь пригодится и как пример
# redis-cli  -p 7996 -a 02182ECFD949F01CACC7


REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", "6379")
REDIS_PASSWORD = os.getenv("REDIS_PASSWORD")
MESSAGE_TTL = int(os.getenv("REDIS_MESSAGE_TTL", "300"))


async def get_server_streams(redis):
        """Returns list of streams from redis server"""

        async with redis.client() as conn:
            cur = b"0"  # set initial cursor to 0
            while cur:
                cur, keys = await conn.scan(cur)
                yield keys

def get_old_timestamp() -> str:
    """Return newest timestamp for old messages"""

    return f"{int(time.time()) - MESSAGE_TTL}000-0"

async def xrange(redis, stream: str, first_message_id: str = "-", last_message_id: str = "+") -> list[set]:
    """XRange from redis stream

    stream - redis stream name
    last_message_id - ID of the last message in range

    Below is example of the raw result of `conn.xrange`
    [('1663131618970-0', {...}), ('1663131619041-0', {..})]
    """

    async with redis.client() as conn:
        return await conn.xrange(name=stream, min=first_message_id, max=last_message_id)


async def main():
    redis = aioredis.from_url(f"redis://{REDIS_HOST}:{REDIS_PORT}", password=REDIS_PASSWORD, encoding="utf-8", decode_responses=True
        )
    await redis.set("my-key", "value")
    value = await redis.get("my-key")
    print("__deb", value)

    async for streams in get_server_streams(redis):
        print("__deb", streams)
        for stream in streams:
            old_messages = await xrange(redis, stream=stream, first_message_id="-", last_message_id=get_old_timestamp())
            print("__deb", len(old_messages), old_messages)

if __name__ == "__main__":
    asyncio.run(main())
