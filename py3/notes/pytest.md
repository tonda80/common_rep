## Полезные ссылки
[Цикл переводных статей на хабре](https://habr.com/ru/post/448782/)
[Хабр ещё](https://habr.com/ru/articles/269759/)

## Запуск
pytest [options] [file_or_dir] [file_or_dir].

Если опустить file_or_dir - рекурсивно ищет тесты в текущей директории. По умолчанию: шаблон для файлов - test_<>.py или <>_test.py, функций - test_<>, классов - Test<>. Но в pytest.ini (по крайней мере для файлов) это можно поменять. Вообще конфигурационных файлов куча: pytest.ini, tox.ini или setup.cfg.
Также см test-discovery.
Можно указать конкретный файл для запуска *'pytest file.py'* или конкретный тест *'pytest file.py::function_OR_class'*.

Для проверок используется нативный питоновский **assert**. 'with pytest.raises' для проверки генерации исключений.

Тесты можно параметризовать (@pytest.mark.parametrize или @pytest.fixture(scope="function", params=), и тест будет вызван несколько раз с заданным набором параметров.
TODO

## Тесты можно маркировать
чтобы
* пометить для пропуска  @pytest.mark.skip(reason='temp')
* пометить для пропуска с условием  @pytest.mark.skipif(sys.version_info < (3, 6))
* пометить, что тест должен сфейлится  @pytest.mark.xfail(reason='not implemented yet')
* создать набор тестов для запуска с -m. Маркеры регистрируются в pytest.ini (пример ниже) или в pyproject.toml.
Тест помечается @pytest.mark.foo и тогда при запуске с -m foo только такие тесты будут исполнены. Условия могут быть и сложнее -m 'not foo and not slow'
pytest.ini
[pytest]
markers =
  slow: they take very long time
  foo: any comment
  bar

Интересно, что @pytest.mark.skip, @pytest.mark.xfail, @pytest.mark.foo итд могут быть с вызовом а могут и без (понятно, что без вызова не передать доп. аргументы типа reason)

## Примеры см в
* common_rep/py/examples/pytest/test_pytest_hp.py 2-й питон (dssl времена). Эксперименты по мотивам изучения теории. Setup\teardown специальные методы.
* common_rep/py/examples/pytest/test_api.py. 2-й питон, но все концепции на момент написания рабочие и в 3-м. Тестируется апи некоего сервиса (vizorlabs для dssl). Можно подсмотреть основные идеи: фикстуры, моки итп

## Fixtures
Есть возможность писать setup\teardown функции как в unittest, см xunit-style. Имена: setup|teardown_module, setup|teardown_function, setup|teardown (причем эти вообще не очевидны, вызываются после  <>function методов). В классе примерно то же: setup|teardown_class, setup|teardown_method, setup|teardown
Но предпочитаемый механизм - фикстуры. Функция с декоратором @pytest.fixture() является фикстурой. Имя такой функции-фикстуры, может использоваться как аргумент теста, т.о. функция будет вызвана и ее результат будет доступен внутри теста. Для setup\teardown фикстура может yield-ить что-то и после него будет код очистки. Дефолтный scope фикстуры - function, т.е. для каждого теста будет свой вызов. Можно изменить scope фикстуры и ее возвращаемое значение будет общим для класса, модуля, пакета, сессии (в порядке увеличения срока жизни). Задается  аргументом @pytest.fixture(scope='module').
Фикстуры могут вызывать другие фикстуры (причем хитро - результат кешируется, они ищутся с точки зрения теста итп). Общие для разных файлов текстуры кладут в conftest.py. Также они бывают автовызываемыми, помеченные флагом autouse будут вызываться и для тестов у которых нет в аргуметах этих текстур. Ещё есть куча готовых текстур см built-in fixtures [тут](https://pytest.org/en/7.4.x/reference/fixtures.html#fixtures).
Опишем тут интересные
* request - Тут предоставляется информация о вызванно йтетс функции. В request можно добавлять финалайзер - код который будет вызван после того как этот самый реквест "закончится", вроде как по дефолту это конец теста (см Adding finalizers directly).
* tmp_path - Временная директория для теста
* capsys - захват вывода out и err
* monkeypatch - позволяет изменить для теста атрибуты объектов, значения в словаре, переменные окружения
* mocker - ставится из отдельного пакета pytest-mock. (Вроде как: использует модуль mock для использования в pytest, повторяет его интерфейс. Есть родной unittest.mock с 3.3 и бэкпорт для младших версий в пип).
Примеры mocker фич:
  * **spy** отслеживает вызовы `spy = mocker.spy(mod_class, "meth_name"); spy.assert_called_once(); assert spy2.call_count == N` Список методов spy см [в](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.Mock)
  * **patch** подменяет вызовы `mocker.patch('json.dumps', return_value='no_way'); assert json.dumps({}) == 'no_way'` или вот так `mocker.patch('math.pow', side_effect=lambda x, y: x - y); assert math.pow(2, 5) == -3` Отслеживаются вызовы "под капотом", то есть math.pow подменится и вызываемое внутри каких то библиотечных тестируемых функций. Но можно до патча сделать orig_pow = math.pow и использовать orig_pow для нормальных вычислений.
  * **stub** заглушка для колбеков.


## Опции
* -s                show print statements in console, по дефолту выводит только для сломанных тестов
* -v                для подробного вывода
* -h справка
* -k EXPR           выбор запускаемого теста по части имени, можно использовать строку с and or not
* -m MEXPR          запуск тестов с маркером. Тесты маркируются декоратором @pytest.mark.MARK. MEXPR - тоже хитрое выражение как и EXPR выше (c and, or)
* --collect-only    только покажет тесты, без выполнения
* -q quiet mode
* --durations=N     отчет о медленных тестах
* --fixtures        выводит список фикстур тестов
* --log-cli-level=DEBUG     чтобы включить лог вывод
* -o log_cli=true           тоже включает лог вывод

