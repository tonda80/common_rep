#!/usr/bin/env python3
# coding=utf8


# общий код для ttk скриптов

import os
import json


_home_mnt_users = ('nuvo', 'nuva')
_opt_mnt_users = ('mnt', )
_ambiguous_mnt_users = ('mic', )
# TODO выпилить подгруппы
known_mnt_users = set(_home_mnt_users + _opt_mnt_users + _ambiguous_mnt_users)


# subprocess.run('id -u -n'.split(), check=True, stdout=subprocess.PIPE).stdout.strip() не работает для запуска под судо
def get_mnt_username():
	users = [e.name for e in os.scandir('/home') if e.is_dir() and e.name in known_mnt_users]
	if len(users) == 1:
		return users[0]
	raise RuntimeError(f'[get_mnt_username] cannot get mnt user: {users}')


# все было хорошо, пока не появился mic с systemd
def __deprecated_get_mnt_directory(user=None):
	if user is None:
		user = get_mnt_username()

	if user in _opt_mnt_users:
		return '/opt/mnt2'
	elif user in _home_mnt_users:
		return f'/home/{user}/mnt2'
	raise RuntimeError(f'[get_mnt_directory_by_user] unknown user {user}')


def get_local_mnt_directory():
	pre_variants = [f'{e.path}/mnt2' for e in os.scandir('/home') if e.name in known_mnt_users] + ['/opt/mnt2']
	variants = [p for p in pre_variants if os.path.isdir(p)]	# тут может надо будет проверять что-то ещё
	if len(variants) == 1:
		return variants[0]
	raise RuntimeError(f'[get_local_mnt_directory] cannot get, variants: {variants}')



# определить систему управления сервисами (раньше можно было содержимое servicelist.json, но теперь у каждого модуля там оба параметра)
def __deprecated_get_service_system(servicelist=None):
	if servicelist is None:
		mnt_dir = __deprecated_get_mnt_directory() if mnt_dir is None else mnt_dir
		servicelist_path = os.path.join(mnt_dir, 'configurator', 'servicelist.json')
		with open(servicelist_path) as f:
			servicelist = json.load(f)

	if all(map(lambda e: 'ModuleSystemdName' in e, servicelist['Services'])):
		return 'systemd'
	elif all(map(lambda e: 'ModuleLoaderConfName' in e, servicelist['Services'])):
		return 'supervisor'
	raise RuntimeError(f'[__deprecated_get_service_system] wrong {servicelist_path}')

# смотрим содержимое конфиг директорий systemd и supervisor
def get_service_system():
	systd_conf_dir = '/etc/systemd/system'
	suprv_conf_dir = '/etc/supervisor/conf.d/'
	test_modules = ('configurator', )		# 'can', 'candecoder'

	systd_flag = all(map(os.path.isfile, (f'{systd_conf_dir}/mnt-{f}.service' for f in test_modules)))
	suprv_flag = all(map(os.path.isfile, (f'{suprv_conf_dir}/{f}.conf'        for f in test_modules)))

	#print('__deb', [(p, os.path.isfile(p)) for p in (f'{systd_conf_dir}/mnt-{f}.service' for f in test_modules)])
	#print('__deb', [(p, os.path.isfile(p)) for p in (f'{suprv_conf_dir}/{f}.conf' for f in test_modules)])

	if not (systd_flag ^ suprv_flag):
		raise RuntimeError(f'[get_service_system] cannot detect ({systd_flag}, {suprv_flag})')
	if systd_flag:
		return 'systemd'
	return 'supervisor'

def swap2(v):
	assert v < (1<<16), 'wrong swap2 value'
	return ((v<<8) & 0xff00) | ((v>>8) & 0xff)

def swap4(v):
	assert v < (1<<32), 'wrong swap4 value'
	return ((v<<24) & 0xff00_0000) | ((v<<8) & 0x00ff_0000) | ((v>>8) & 0xff00) | ((v>>24) & 0xff)

def value_from_can(raw_value, start_bit_str, length):
	sb = start_bit_str.partition('.')
	n_byte = int(sb[0]) - 1
	n_bit = int(sb[2]) - 1
	assert 0 <= n_byte < 8 and 0 <= n_bit < 8, f'wrong start_bit {start_bit_str}'
	assert length > 0, 'bad length'
	assert n_bit + length < 9 or (n_bit == 0 and length%8), 'bad case'	# работает если в нескольких байтах у нас значения только из целого числа байт начинающиеся с бита 1
	mask = (1 << length) -1
	shift = 8*(7-n_byte) + n_bit
	value = (raw_value >> shift) & mask
	print(f'value={value:x} shift={shift} mask={mask:x} n_byte={n_byte} n_bit={n_bit}')
	if length == 16:
		return swap2(value)
	elif length == 32:
		return swap4(value)
	return value



def _test(test_str):
	try:
		res = eval(test_str)
	except Exception as e:
		res = f'Exception {e.__class__.__name__}: {e}'
	print(f'{test_str} = {res}')

if __name__ == '__main__':
	#_test('1/0')
	_test('get_local_mnt_directory()')
	_test('get_service_system()')


