#!/usr/bin/env python3
# coding=utf8

# тестовая заглушка mnt2 модулей

import zmq				# pip install zmq
import sys
import json
import os
import time
import threading
import random
import cmd
import traceback

sys.path.append(os.path.expanduser('~/ab/py_env'))

import mntproto_pb2		# pip install protobuf
import usbdiag_pb2
import canparser_pb2
import board_pb2
import boarddevice_pb2
import navigation_pb2
import alert_pb2
import dido_pb2
import routes_pb2
import uiactions_pb2
import getversion_pb2
import redis_pb2
import wssm_pb2
import gtfs_pb2
import dispatcher_pb2
import duter_pb2
#import validator_pb2

from baseapp import BaseConsoleApp
import zmq_client_mnt2
from zmq_client_mnt2 import get_data_object


class AppError(RuntimeError):
	pass


class G:
	stop = threading.Event()
	log = None
	zmq_client = None



class UserCmd(list):
	def __init__(self, str_cmd):
		list.__init__(self, str_cmd.split())		# признак модуля сюда не попадает!)
		self.dict = {}
		for i, word in enumerate(self[1:]):     # (янв 25) тут похоже ошибка, непонятно зачем 1 эл-т пропускаем
			k, s, v = word.partition('=')
			if s:
				self.dict[k] = v

	def __str__(self):
		return f'[ {list(self)}, {self.dict} ]'


	def has_key(self, key):
		return key in self.dict

	def get(self, key, default = None):
		if isinstance(key, str):
			return self.dict.get(key, default)
		# elif isinstance(key, int):
		return self[key] if len(self) > key else default

	def geti(self, key, default = None, base = 10):
		ret = self.get(key)
		if ret is not None:
			try:
				return int(ret, base)
			except ValueError:
				if default is None:
					raise
				G.log.info(f'`{ret}` is not int! Using default')
		return default

	def getf(self, key, default):
		return float(self.get(key, default))


def data_log_str(data, oneline = True, utf8_transform = False):
	if data is None:
		return ''
	s = str(data)
	if oneline:
		s = s.replace('\n', ' ')
	if utf8_transform:
		s = s.encode('latin1').decode('unicode-escape').encode('latin1').decode('utf8')		# https://stackoverflow.com/questions/46639512/converting-octet-strings-to-unicode-strings-python-3
	return s


def create_user_command_handler(test_obj):
	def cmd_handler(self, arg):
		test_obj.user_command_handler(arg)
		time.sleep(0.1)		# time чтобы не мешался вывод пришедших с приглашением
	cmd_handler.__doc__ = f'Тестовые команды для {test_obj.module_name()}'
	return cmd_handler


class App(BaseConsoleApp, cmd.Cmd):
	def add_arguments(self):
		self.parser.add_argument('--router', '-r', action='store_true', help='Взаимодействие через роутер')
		self.parser.add_argument('--data', help='Что-то кастомное для тестов')
		self.parser.add_argument('-m', '--modules', help='Список тестируемых модулей разделенный запятыми. Варианты: ' + self.m_arg_help())
		self.parser.add_argument('-ss', '--set_state_out', action='store_true', help='Выводить SetState сообщения в info, иначе в debug')
		self.parser.add_argument('--ip', default='127.0.0.1', help='IP для коммуникации, например 192.168.136.114')

	def tested_modules(self):
		return {
			'_' : CommonTest,
			'u' : USBDIAGTest,
			#'v' : VALIDATORTest,
			't' : TSDIAGTest,
			'w' : WIPSSENDERTest,
			'hw' : HWMONITORTest,
			'ws' : WSSMTest,
			'r': ROUTTest,
			'r2': ROUTEUPD2Test,
		}

	def m_arg_help(self):
		res = []
		for sign, class_ in self.tested_modules().items():
			res.append(f'{sign}({class_.module_name()})')
		return ','.join(res)

	def main(self):
		G.log = self.log
		G.args = self.args

		args_modules = set(self.args.modules.split(',')) if self.args.modules else None

		self.test_modules_by_zmq_name = {}
		self.test_modules_by_sign = {}
		for sign, class_ in self.tested_modules().items():
			if args_modules and sign not in args_modules:
				continue
			t_obj = class_()
			self.test_modules_by_zmq_name[t_obj.module_name()] = t_obj								# добавляем в словарь для обработки zmq сообщений
			self.test_modules_by_sign[sign] = t_obj											# доп словарь см
			setattr(self.__class__, f'do_{sign}', create_user_command_handler(t_obj))	# добавляем do_* методы для Cmd
		self.__class__.intro = f'Run with: {", ".join(self.test_modules_by_zmq_name.keys())}'

		if self.args.router:
			router_mode = True
			pub_endpoint = f'tcp://{self.args.ip}:10125'
			sub_endpoint = f'tcp://{self.args.ip}:10126'
			self.log.info('App started for communication with router')
		else:
			router_mode = False
			pub_endpoint = f'tcp://{self.args.ip}:10126'
			sub_endpoint = f'tcp://{self.args.ip}:10125'

		zmq_client_mnt2.G.init(G)
		self.zmq_client = zmq_client_mnt2.ZmqMntClient(b'TestApp', self.zmq_recv_handler, (pub_endpoint, sub_endpoint), router_mode, 1)
		self.zmq_client.error_handler = self.zmq_error_handler
		G.zmq_client = self.zmq_client

		cmd.Cmd.__init__(self)
		self.cmdloop()	# Cmd method

	prompt = '> '

	def do_q(self, arg):
		'Выход'
		G.stop.set()
		print('exiting')
		return True

	def do_rs(self, arg):
		'Выводим записи модуля хранящиеся в "редисе"'
		if arg and arg not in self.test_modules_by_sign:
			self.log.error(f'unknown module sign {arg}, valid values: {list(self.test_modules_by_sign.keys())}')
			return
		module_pref = f'mnt2/{self.test_modules_by_sign[arg].real_module_name()}' if arg else ''
		out = []
		now = time.time() * 1000
		#print('__deb', module_pref, MTest.redis_storage)
		for k, v in list(MTest.redis_storage.items()):
			create_time, data = v
			if data.life_time_msec != -1 and create_time + data.life_time_msec < now:
				del MTest.redis_storage[k]
				continue
			if k.startswith(module_pref):
				out.append(f'    {data_log_str(data)}')
				try:
					out.append(f'        parsed result="{json.loads(data.result)}"')
				except:
					pass
		out_str = '\n'.join(out)
		G.log.info(f'Redis records:\n{out_str}')


	def zmq_recv_handler(self, mntCommand):
		#self.log.debug('received {} from {}'.format(mntCommand.header.name, mntCommand.header.source))

		test = self.test_modules_by_zmq_name.get(mntCommand.header.source)
		if test:
			test.process_zmq_command(mntCommand)
		# TODO? передавать в Common

	def zmq_error_handler(self, msgs):		# msgs - сырое мультипарт сообщение
		dst = msgs[1] if self.zmq_client.router_mode else msgs[0]
		if dst in self.test_modules_by_zmq_name:
			self.log.error(f'wrong recv messages {msgs}')
		elif self.args.verbose:
			s_msgs = str(msgs)
			if len(s_msgs) > 150:
				s_msgs = s_msgs[:100] + ' ...'
			self.log.error(f'wrong recv messages {s_msgs}')




class MTest:
	redis_storage = {}

	def __init__(self):
		self.user_command_handlers = {
			'?' : ('print help', lambda cmd: [print(f'\t{k}: {v[0]}') for k,v in self.user_command_handlers.items()]),
			'v' : ('get app version', self.send_getversion),
		}

		self.zmq_handlers = {
			'GetConfig' : lambda *a: G.log.info(f'received GetConfig from {self.module_name()}'),
			'GetVersion' : self.process_getversion,
			'SetState' : self.process_setstate,
		}

	def user_command_handler(self, s_cmd):
		cmd = UserCmd(s_cmd)
		self.user_command_handlers.get(cmd[0] if len(cmd) > 0 else None, ('fake desc', self.report_unknown_command))[1](cmd)

	def process_zmq_command(self, mntCommand):
		self.zmq_handlers.get(mntCommand.header.name, self.default_zmq_command_handler())(mntCommand)

	def default_zmq_command_handler(self):
		return self.report_unknown_message

	@classmethod
	def module_name(cls):
		assert cls.__name__[-4:] == 'Test', f'wrong MTest subclass name {cls_name}'
		return cls.__name__[:-4]
	def module_name_b(self):
		return self.module_name().encode('utf8')

	def real_module_name(self):
		# module_name это имя роутер клиента, а это "настоящее" имя модуля
		return self.module_name().lower()

	def report_unknown_command(self, cmd):
		G.log.error(f'{self.__class__.__name__}: unknown command {cmd}')

	def report_unknown_message(self, cmd):
		G.log.info(f'unknown message from {self.module_name()} {cmd}')

	def publish(self, name, data = None, dest = None, rawData = None):
		if dest is None:
			dest = self.module_name_b()
		elif isinstance(dest, str):
			dest = dest.encode()
		G.log.info(f"publish {name}: data='{data_log_str(data)}' rawData='{rawData}' to {dest}")
		G.zmq_client.publish(dest, name, data, rawData)

	def send_getversion(self, cmd):
		self.publish(b'GetVersion')

	def process_getversion(self, cmd):
		self.log_message_data(cmd, getversion_pb2.GetVersion)

	def process_setstate(self, cmd):
		data = self.log_message_data(cmd, redis_pb2.SetState, G.log.info if G.args.set_state_out else G.log.debug)
		self.redis_storage[data.address] = (cmd.header.createTime, data)

	def log_message_raw_data(self, cmd):
		G.log.info(f'{cmd.rawData} rawData with name {cmd.header.name} for {cmd.header.destination} is received from {cmd.header.source}')

	def log_message_data(self, cmd, ProtoClass, log_method=None, **kw):
		data = get_data_object(cmd, ProtoClass)
		if log_method is None:
			log_method = G.log.info
		s = data_log_str(data, **kw)
		log_method(f'{ProtoClass.__name__}({s}) with name {cmd.header.name} for {cmd.header.destination} is received from {cmd.header.source}. rawData={cmd.rawData}')
		return data

	def publish_can_message(self, dest = None, **fields):
		can_pck = canparser_pb2.CanPacket()
		can_item = can_pck.Items.add()
		# видимо обязательно: Name (конфиг декодера), ParsedValue
		# опционально: Address, RawValue, StartBit итд см в canparser.proto CanItem
		for k, v in fields.items():
			setattr(can_item, k, v)
		self.publish('CanMessage', can_pck, dest = dest)



# TODO вроде как этот класс стоит выпилить
class CommonTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.pop('v')
		self.user_command_handlers.update({
			'c' : ('send can message (from decoder); args: destination and next like in canparser.proto', self.send_canmessage),
		})

		#self.zmq_handlers.update({})

	def send_canmessage(self, cmd):
		fields = {}
		def add_fields(cmd_arg, field_name, method_name = 'geti'):
			if cmd.has_key(cmd_arg):
				fields[field_name] = getattr(cmd, method_name)(cmd_arg, None)
		add_fields('n', 'Name', 'get')
		add_fields('v', 'ParsedValue')
		add_fields('a', 'Address')
		add_fields('p', 'PGN')
		add_fields('r', 'RawValue')
		add_fields('s', 'StartBit')
		add_fields('l', 'Length')
		add_fields('c', 'comment', 'get')
		# +
		dest = cmd[1]
		self.publish_can_message(dest, **fields)



class USBDIAGTest(MTest):
	usbdiag = b'USBDIAG'			# usbdiag router client name
	CanItemName = 'USBCHRG'			# usbdiag  UCRGTST USBCHRG

	def log_method(self, sign):
		# --data кастомизирует вывод в лог, а то больно много хлама выводим
		# s: state messages
		# e:'extra state message
		# M: coMmands to chargers
		if sign in self.log_data:
			return G.log.info
		return G.log.debug


	def __init__(self):
		MTest.__init__(self)

		self.log_data = G.args.data if G.args.data else ''
		G.log.info(f'[usbdiag] logging data: {self.log_data}')

		self.user_command_handlers.update({
			'c' : ('шлет на зарядку [c]ommands: SET_ADDRESS = 0; RESET_ADDRESS = 1; RESET_ALL = 2; REQUEST_INFO = 3', self.send_usb_control),
			's1' : ('эмулирует основное сообщение от зарядки', self.send_usb_state),
			's2' : ('эмулирует дополнительное сообщение от зарядки', self.send_usb_state2),
			'v' : ('запрос версии', self.send_getversion),
			'gs' : ('запрос состояния (не поддерживается с 2.41)', lambda cmd: self.publish('GetState')),
		})
		self.zmq_handlers.update({
			'usbdiagState' : lambda cmd: self.log_message_data(cmd, usbdiag_pb2.usbdiagState, self.log_method('s')),
			'usbdiagExtraState' : lambda cmd: self.log_message_data(cmd, usbdiag_pb2.usbdiagExtraState, self.log_method('e')),
			'CanMessage' : self.process_usbdiag_CanMessage,
			'GetState' : self.process_usbdiag_GetState_reply,
			#'CanEncodeMessage' : self.process_usbdiag_CanEncodeMessage,
		})

	# обработчик для usbdiag->can сообщений (команд на устройства) (пример json внезапно в can.proto)
	def process_usbdiag_CanMessage(self, mntCommand):
		can_out_packet = get_data_object(mntCommand, board_pb2.CANData)
		self.log_method('M')('[usbdiag] received CanMessage: {}'.format(can_out_packet.data))
		# data = int(can_out_packet.data['Data'], 16); cmd = data & 0xf; addr = data >> 4 & 0xf

	# отправка управления usbcharger-ами от UI
	def send_usb_control(self, cmd):
		ctrl = usbdiag_pb2.usbdiagControl()
		ctrl.command = cmd.geti('c', 1)		# SET_ADDRESS = 0; RESET_ADDRESS = 1; RESET_ALL = 2; REQUEST_INFO = 3;
		ctrl.address = cmd.geti('a', 5)
		G.log.info('send_usb_control [{}]'.format(data_log_str(ctrl)))
		G.zmq_client.publish(self.usbdiag, b'usbdiagControl', ctrl)

	# эмулируем отправку coстояния usbcharger-ов
	def send_usb_state(self, cmd):
		raw_state = 0x80_0000_0000
		addr = cmd.geti('a', 5)
		fail = cmd.geti('f', 0) != 0
		power = float(cmd.get('p', 12.34))
		d_power = int(power/0.1)
		for i in range(3):
			voltage = float(cmd.get(f'v{i}', '{0}.{0}{0}{0}'.format(i)))
			d_voltage = int(voltage/0.05)
			raw_state |= d_voltage << 8*(3-i)
		raw_state |= addr    << 32 & 0x0f_0000_0000
		raw_state |= fail    << 36 & 0x10_0000_0000
		raw_state |= d_power << 29 & 0x20_0000_0000
		raw_state |= d_power & 0xff
		self.publish_CanMessage(addr, raw_state)

	# эмулируем отправку дополнительного coстояния usbcharger-ов
	def send_usb_state2(self, cmd):
		raw_state = 0
		addr = int(cmd.get('a', 5))
		fail = int(cmd.get('f', 0)) != 0
		ver = int(cmd.get('v', '14'), 16)
		raw_state |= addr    << 8 & 0x0f_00
		raw_state |= fail    << 12 & 0x10_00
		raw_state |= ver & 0xff
		#G.log.info('send_usb_state_can_message {:x}'.format(raw_state))
		self.publish_CanMessage(addr, raw_state)

	def publish_CanMessage(self, addr, parsed_value):
		G.log.info(f'publish_CanMessage parsed_value={parsed_value:x}')
		can_pck = canparser_pb2.CanPacket()
		can_item = can_pck.Items.add()
		can_item.Name = self.CanItemName
		can_item.Address = 0x18FF09FA + addr
		can_item.ParsedValue = parsed_value
		G.zmq_client.publish(self.usbdiag, 'CanMessage', can_pck)

	# это видимо что-то мертвое уже
	def process_usbdiag_GetState_reply(self, cmd):
		data = get_data_object(cmd, uiactions_pb2.UIGetState)
		G.log.info(f'received usbdiag GetState: status={data.status}, result={json.loads(data.result)}')


class VALIDATORTest(MTest):
	def user_command_handler(self, cmd):
		if cmd == 's':
			G.zmq_client.publish(b'VALIDATOR', b'GetState')
		else:
			self.report_unknown_command(cmd)

	def process_zmq_command(self, mntCommand):
		{
			#'GetConfig' : lambda *a: None,
			'GetState' : self.process_validator_ValidatorGetState,
		}.get(mntCommand.header.name,
			lambda cmd: G.log.info('unknown message from VALIDATOR {}'.format(cmd))
		)(mntCommand)

	def process_validator_ValidatorGetState(self, mntCommand):
		G.log.info(f'ValidatorGetState received {mntCommand.rawData}')


class TSDIAGTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'd' : ('publish getdbinfo', lambda cmd: self.publish(b'getdbinfo')),
			'ss' : ('sqlite stress test', self.sqlite_stress_test),
			'ss2' : ('sqlite stress test', self.sqlite_stress_test2),
			't' : ('send temperature info', self.send_temp),
			'c' : ('send can message. t c ID [a=ADDRESS] [i=INDEX] [spn=] etc', self.send_can_message),
			'cc' : ('send can message from several items. t cc [itemName=itemValue]', self.send_can_message2),
			'ds' : ('send dispatcher message. t ds ID [v=value]', self.send_dispatcher_message),
			'dt' : ('send duter message to dispatcher. t dt [v=value]', self.send_duter_message),

		})
		self.zmq_handlers.update({
			'tsdiagevent' : self.process_tsdiagevent,
			'getdbinfo' : self.log_message_raw_data,
		})

	def process_tsdiagevent(self, cmd):
		G.log.info(f'{cmd.header.source}=>{cmd.header.destination} {cmd.header.name}: rawData={cmd.rawData}')
		raw_data = json.loads(cmd.rawData)
		#if 'map' in raw_data: G.log.debug(f'\tmap={}')

	_ts_diag_dict = {}
	@classmethod
	def ts_diag_dict(cls):
		if not TSDIAGTest._ts_diag_dict:
			if G.args.data is None or not os.path.isfile(G.args.data):
				raise AppError('not defined argument --data "path/to/dict1.json" (or wrong path)')
			with open(G.args.data) as f:
				cls._ts_diag_dict = json.load(f)
		return cls._ts_diag_dict

	def _dtc_code(self, s_spn, s_fmi='0'):
		s = int(s_spn)
		f = int(s_fmi)
		return ((s&0xff) << 5*8) | ((s>>8&0xff) << 4*8) | ((s>>16&0x7) << 3*8+5) | ((f&0x1f) << 3*8)

	def send_can_message(self, cmd):
		can_pck = canparser_pb2.CanPacket()
		can_item = can_pck.Items.add()

		id_ = cmd.get(1)
		for par in self.ts_diag_dict()['params']:
			if id_ == par['id']:
				break
		else:
			G.log.error(f'unknown id {id_}')
			return

		can_item.comment = par.get('desc')
		can_item.Address = int(cmd.get('a', '0x1234567'), 16)
		can_item.CanIndex = int(cmd.get('i', '0'))
		if 'dm1Name' in par:
			can_item.Name = par.get('dm1Name')
			spn = cmd.get('spn', par.get('spn', 0))
			fmi = cmd.get('fmi', par.get('fmi', 0))
			can_item.RawValue = self._dtc_code(spn, fmi)
			can_item.ParsedValue = can_item.RawValue
		else:
			can_item.Name = par.get('id')
			can_item.RawValue = cmd.geti('r', random.randrange(65536), 16)
			can_item.ParsedValue = cmd.geti('v', random.randrange(256), 16)

		#G.log.info(f'sending: {can_item.Name} | {can_item.ParsedValue:X} | {can_item.comment}')
		self.publish('CanMessage', can_pck)

	def send_can_message2(self, cmd):
		addr = 0x1234567
		can_indx = 0
		raw_value = random.randrange(65536)
		# если будет надо, то что выше получать из cmd.dict с удалением ключа
		can_pck = canparser_pb2.CanPacket()
		for name in cmd.dict.keys():
			can_item = can_pck.Items.add()
			can_item.Address = addr
			can_item.CanIndex = can_indx
			can_item.RawValue = raw_value
			can_item.Name = name
			can_item.ParsedValue = cmd.geti(name)

		self.publish('CanMessage', can_pck)

	def send_dispatcher_message(self, cmd):
		ds_pck = dispatcher_pb2.DispatcherCanMessage()
		ds_item = ds_pck.items.add()

		id_ = cmd.get(1)
		for par in self.ts_diag_dict()['params']:
			if id_ == par['id']:
				break
		else:
			G.log.warning(f'unknown id {id_}')

		ds_item.name = id_
		ds_item.value = cmd.getf('v', 777.8)

		self.publish('DispatcherCanMessage', ds_pck)

	def sqlite_stress_test(self, cmd):
		Q_packets = 1000_000
		Q_packet_items = 10
		#
		for i in range(Q_packets):
			can_pck = canparser_pb2.CanPacket()
			for i in range(Q_packet_items):
				can_item = can_pck.Items.add()
				par = random.choice(self.ts_diag_dict()['params'])

				can_item.comment = par.get('desc')
				can_item.Address = random.randrange(65536)
				if 'dm1Name' in par:
					can_item.Name = par.get('dm1Name')
					can_item.RawValue = self._dtc_code(par.get('spn', 0), par.get('fmi', 0))
					can_item.ParsedValue = can_item.RawValue
				else:
					can_item.Name = par.get('id')
					can_item.RawValue = random.randrange(65536)
					can_item.ParsedValue = random.randrange(256)
				#G.log.debug(f'sending: {can_item.Name} | {can_item.ParsedValue:X} | {can_item.comment}')
			self.publish('CanMessage', can_pck)

	def sqlite_stress_test2(self, cmd):
		Q_packets = 1000_000
		Q_packet_items = 10

		sent_pars = []
		for par in self.ts_diag_dict()['params']:
			if 'h' in par['flags'] or 'd' in par['flags']:
				sent_pars.append(par)

		if not sent_pars:
			raise RuntimeError('no db parameters!')

		p_idx = 0
		for i in range(Q_packets):
			can_pck = canparser_pb2.CanPacket()
			for i in range(Q_packet_items):
				can_item = can_pck.Items.add()
				par = sent_pars[p_idx]
				p_idx += 1
				if p_idx == len(sent_pars): p_idx = 0

				can_item.comment = par.get('desc')
				can_item.Address = random.randrange(65536)
				if 'dm1Name' in par:
					can_item.Name = par.get('dm1Name')
					can_item.RawValue = self._dtc_code(par.get('spn', 0), par.get('fmi', 0))
					can_item.ParsedValue = can_item.RawValue
				else:
					can_item.Name = par.get('id')
					can_item.RawValue = random.randrange(65536)
					can_item.ParsedValue = random.randrange(256)
				#G.log.debug(f'sending: {can_item.Name} | {can_item.ParsedValue:X} | {can_item.comment}')
			self.publish('CanMessage', can_pck)

	# шлем температуру как модуль temp
	def send_temp(self, cmd):
		pb_msg = boarddevice_pb2.BoardTemperature()

		pb_msg.internalTemperature = cmd.geti('i', random.randrange(-40, 40))
		pb_msg.externalTemperature = cmd.geti('e', random.randrange(-40, 40))
		pb_msg.internalTemperatureValid = cmd.geti('iv', True)
		pb_msg.externalTemperatureValid = cmd.geti('ev', True)

		self.publish('AVERAGE', pb_msg)

	def send_duter_message(self, cmd):
		pb_msg = duter_pb2.DuterMessage()

		pb_msg.Level = cmd.geti('v', random.randrange(0, 100))

		self.publish('DUTERMessage', pb_msg, dest = 'DISPATCHER')

class WIPSSENDERTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'n' : ('send navigation data', self.send_navifation),
			'a' : ('send alert', self.send_alert),
			'd1' : ('send dido type 1', self.send_dido),
			'I' : ('init data', self.init_data),
			'G' : ('generate data set', self.generate_data_set),
		})
		self.zmq_handlers.update({

		})

	def send_navifation(self, cmd):
		msg = navigation_pb2.NavigationData()
		msg.Lat = cmd.getf('lt', 51.541)
		msg.Lon = cmd.getf('ln', 46.009)
		msg.Valid = True
		msg.Heading = cmd.getf('h', 10)
		msg.Speed = cmd.getf('s', 12.34)
		msg.UnixTime = int(time.time())
		self.publish('NavigationData', msg)

	def send_alert(self, cmd):
		msg = alert_pb2.Alert()
		msg.ID = cmd.geti('i', 1)
		self.publish('AlarmButton', msg)

	def send_dido(self, cmd):
		msg = dido_pb2.DiDoType1()
		msg.din = cmd.geti('i', 0x5, 16)
		msg.dout = cmd.geti('o', 0x7, 16)
		msg.ain1 = cmd.geti('a1', 0xc)
		msg.ain2 = cmd.geti('a2', 0xe)
		msg.dinExists = msg.doutExists = msg.ain1Exists = msg.ain2Exists = True
		for i in range(1, 9):
			t = cmd.getf(f't{i}', -999)
			if t != -999:
				setattr(msg, f'temp{i}', t)
				setattr(msg, f'temp{i}Exists', True)

		self.publish('DiDoType1', msg)

	def init_data(self, cmd):
		self.user_command_handler('n')
		self.user_command_handler('d')
		self.user_command_handler('a')

	def generate_data_set(self, cmd):
		self.user_command_handler('n')
		for i in range(12):
			self.user_command_handler('a i=1')
			self.user_command_handler('a i=0')


class HWMONITORTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'm' : ('send GetMediaPaths request', lambda cmd: self.publish('GetMediaPaths')),
		})
		self.zmq_handlers.update({
			'GetMediaPaths' : lambda cmd: self.log_message_data(cmd, routes_pb2.USBDeviceList),
		})

class WSSMTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'r' : ('send data request, 3-th arg: f[orecast], v[ehicle], e[xchange], w[eather]', self.data_request),
		})

		self.zmq_handlers.update({
			'GtfsForecast' : lambda cmd: self.log_message_data(cmd, wssm_pb2.GtfsForecast, utf8_transform = True),		# oneline = False
			'VehiclePosition' : lambda cmd: self.log_message_data(cmd, wssm_pb2.VehiclePosition, utf8_transform = True),
			'ExchangeRate' : lambda cmd: self.log_message_data(cmd, wssm_pb2.ExchangeRate),
			'WeatherForecast' : lambda cmd: self.log_message_data(cmd, wssm_pb2.WeatherForecast),
			#
			'GtfsForecastRequest' : lambda cmd: self.log_message_data(cmd, wssm_pb2.GtfsForecast, utf8_transform = True),
			'VehiclePositionRequest' : lambda cmd: self.log_message_data(cmd, wssm_pb2.VehiclePosition, utf8_transform = True),
			'ExchangeRateRequest' : lambda cmd: self.log_message_data(cmd, wssm_pb2.ExchangeRate),
			'WeatherForecastRequest' : lambda cmd: self.log_message_data(cmd, wssm_pb2.WeatherForecast),
		})

	def data_request(self, cmd):
		key = cmd.get(1, '')
		name = {'f':'GtfsForecastRequest', 'v':'VehiclePositionRequest', 'e':'ExchangeRateRequest', 'w':'WeatherForecastRequest'}.get(key)
		if name:
			self.publish(name)
		else:
			G.log.error(f'"{key}" is unknown data request key')


class ROUTTest(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'i' : ('send command to import route, single arg is path to import', self.import_route_request),
			'iu' : ('import from updater', self.updater_import_route_request),
			'gr' : ('send command getRoute', lambda cmd: self.publish('getRoute')),
			'ar' : ('send command UIGetAllRoutes', lambda cmd: self.publish('UIGetAllRoutes', rawData='{}')),
			'g' : ('send gtfs route, e.g. `g extId stopId0,stopId1`', self.gtfs_route),
			's' : ('выбор маршрута, e.g. `s dbId`', self.select_route),
			'tt' : ('что-то тестовое', self.test_stuff),
		})

		self.zmq_handlers.update({
			'ImportResult' : lambda cmd: self.log_message_data(cmd, routes_pb2.ImportResult, utf8_transform = True),
			#'arrivalCalculations': self.process_arrivalCalculations,
			'RouteData' : self.on_RouteData,
			'ValidRoutes' : lambda cmd: self.log_message_data(cmd, routes_pb2.ValidRoutes, utf8_transform = True, log_method=G.log.debug),
			'GtfsAutoRoute' : self.log_message_raw_data,
		})

	def default_zmq_command_handler(self):
		def handler(mnt_cmd):
			G.log.debug(f'[ROUT] unknown command: {mnt_cmd.header.name}')
		return handler


	def updater_import_route_request(self, cmd):
		self.import_route_request(cmd, 'ImportRoute')

	def import_route_request(self, cmd, zmq_cmd_name = 'ImportRoute2'):
		path = cmd.get(1)
		if os.path.isdir(path):
			self.publish(zmq_cmd_name, rawData = path)
		else:
			G.log.error('no path')

	def process_arrivalCalculations(self, mntCommand):
		data = get_data_object(mntCommand, routes_pb2.arrivalCalculations)
		out = '[ROUT][aCalc]\n'
		for a in data.calculations:
			err = ''
			# if a.curDistace > a.trackDistace:
				# err += '!'
			# if a.calculatedTimeH < 0 or a.calculatedTimeM < 0 or a.calculatedTimeS < 0:
				# err += '?'
			out += f'{err}\t{a.stop} c={a.calculatedTimeH:02d}:{a.calculatedTimeM:02d}:{a.calculatedTimeS:02d} track={a.trackDistace:.2f} cur={a.curDistace:.2f} s={a.calculatedSpeed:.2f} prev={a.prevStop}\n'
			#out += f'\t\t prevStopId={a.prevStopId} stopId={a.stopId}\n'
		G.log.info(out)
		if data.calculations:
			if data.calculations[0].curDistace > data.calculations[0].trackDistace:
				print('!'*10)

	def gtfs_route(self, cmd):
		msg = gtfs_pb2.GtfsRoute()
		msg.route_id = cmd.get(1, '')
		msg.stops_id.extend(cmd.get(2, '').split(','))
		self.publish('GtfsRoute', msg)

	def on_RouteData(self, cmd):
		data = get_data_object(cmd, routes_pb2.RouteData)
		route = json.loads(data.routeDataJSON)
		G.log.info(f'RouteData id={route["id"]} name={route["name"]} ')
		G.log.debug(f'RouteData full \n\n{route}')
		#print('markup =', json.loads(route['markup']))

	def select_route(self, cmd):
		dbid = cmd.geti(1, -1)
		if dbid >= 0:
			self.publish('UIGetRouteDataById', rawData = json.dumps({'uiRouteId': dbid}))
		else:
			G.log.error(f'wrong route db id {dbid}')

	def test_stuff(self, cmd):
		self.publish('TESTCOMMAND', dest=[b'ROUT', b'ROUT', b'ROUT', b'ROUT', b'ROUT', b'USBDIAG'])


class ROUTEUPD2Test(MTest):
	def __init__(self):
		MTest.__init__(self)

		self.user_command_handlers.update({
			'u' : ('send CheckRouteUpdate', self.send_CheckRouteUpdate),
			's' : ('send StopRouteUpdate', lambda *a: self.publish('StopRouteUpdate')),
			#'t' : ('send test', lambda *a: self.publish('TTTT')),
		})

		self.zmq_handlers.update({
			'RouteUpdateStatus' : lambda cmd: self.log_message_data(cmd, routes_pb2.RouteUpdateStatus),
		})

	def send_CheckRouteUpdate(self, cmd):
		def_route = cmd.get('i', '39')		# 39 26R 9
		data = {
			'extId': def_route,
			'direction': cmd.get('d', 'forward'),
			'tsType': cmd.get('t', 'Тм'),
			'name': cmd.get('n', def_route),
			'routeId': cmd.get('r', def_route),
			'tripShortName': cmd.get('tsn', def_route),
		}
		self.publish('CheckRouteUpdate', rawData = json.dumps(data))


if __name__ == '__main__':
	try:
		App().main()
	except (SystemExit, KeyboardInterrupt):
		pass
	finally:
		G.stop.set()
