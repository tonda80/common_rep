#!/usr/bin/env python3
# coding=utf8

# автоматизируем работу с конфигом tsdiag


import os
import json
import sqlite3
import sys
import re
import copy
import glob

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-c', '--config', help='Путь к tsdiag конфигу (словарю)')
		self.parser.add_argument('--dec_dir', help='Путь к директории конфигов декодера')
		self.parser.add_argument('--show_rep_err', action='store_true', help='Показывать повторяющиеся ошибки')

		ge_parser = self.add_subparser('ge', self.generate_can_emulator_json_main, help='Генерировать json для кан эмулятора')
		ge_parser.add_argument('--out', default='tsdiag_can_emul.json', help='Выходной файл')

		ip_parser = self.add_subparser('ip', self.ipython_work_main, help='ipython manual work')

		ch_parser = self.add_subparser('ch', self.check_config_main, help='Проверить валидность tsdiag конфига')

		chall_parser = self.add_subparser('chall', self.check_all_config_main, help='Проверить валидность всех tsdiag конфигов')
		chall_parser.add_argument('-td', '--tsdiag_dir', required=True, help='Путь к директории с конфигами tsdiag-а')
		chall_parser.add_argument('-cd', '--candecoder_dir', required=True, help='Путь к директории с конфигами кан декодера')

		#dbrd_parser = self.add_subparser('dbread', self.db_read_main, help='Читаем базу с данными и выводим в stdout')
		#dbrd_parser.add_argument('--db', required=True, help='Путь к sqlite файлу')
		#dbrd_parser.add_argument('--ignore_err', action='store_true')

		invest_parser = self.add_subparser('invest', self.invest_main, help='Как-то анализируем базу и логи')
		#invest_parser.add_argument('--inv_path', required=True, help='Путь к директории с базой и логами')
		invest_parser.add_argument('--db', required=True, help='Путь к sqlite файлу')
		invest_parser.add_argument('--out', required=True, help='Выходной файл')

	def read_tsdiag_config(self, tsdiag_config):
		if not tsdiag_config:
			return
		tsdiag_cfg_path = os.path.realpath(tsdiag_config)
		with open(tsdiag_cfg_path) as f:
			self.tsdiag_cfg = json.load(f)
		self.tsdiag_params = dict((p['id'], p) for p in self.tsdiag_cfg['params'])

	def main(self):
		self.no_decoder_items = {'BoardTemperatureExternal', 'BoardTemperatureInternal', '__par_dict_path__'}

		self.read_tsdiag_config(self.args.config)

		self.decoder_items = self.read_decoder_config(self.args.dec_dir)

		return self.sub_main()

	# ./tsdiag_cfg_util.py -c ~/job/builds/tsdiag/par_dict1.json --dec_dir ~/rep-s/mnt2/go/cmd/canDeCoder/nefaz_5299  ge --out ~/temp/tsdiag_can_emul.json
	def generate_can_emulator_json_main(self):
		out_arr = []

		all_ids = set()

		for p in self.tsdiag_cfg['params']:
			if 'dm1Name' in p:
				is_dtc = True
				can_item_name = p['dm1Name']
			else:
				is_dtc = False
				can_item_name = p['id']
			items = self.decoder_items.get(can_item_name, ())
			len_items = len(items)
			if len_items == 0:
				self.log.error(f'can decoder config doesn\'t have \'{can_item_name}\' item')
				continue
			if len_items > 1 and can_item_name != 'DCBN':	# "not is_dtc" condition?
				self.log.error(f'can decoder config has {len_items} \'{can_item_name}\' items')
				continue
			can_item = items[0]

			elem = {'enable': True, 'delay': 1000}
			elem['description'] = p['desc']		# can_item[6]	# comment
			elem['id'] = can_item[1]	# id
			elem['data'] = self.get_emul_data_value(is_dtc, can_item, p)
			out_arr.append(elem)

			all_ids.add(int(can_item[1], 16))

		with open(self.args.out, 'w') as f:
			json.dump(out_arr, f, ensure_ascii=False, indent='\t')

		# и тут же заодно генерируем файлик с фильтрацией нужных кан сообщений для кан модуля (TODO обрабатывать диапазоны)
		all_ids_list = list(all_ids)
		all_ids_list.sort()
		can_rules = []
		for i in all_ids_list:
			can_rules.append({'init_command': f'AT+CAN_AFI=0x{i:08X}', "wait_after":100})
		сan_rules_file_name = '{0}_can_rules{1}'.format(*os.path.splitext(self.args.out))
		with open(сan_rules_file_name, 'w') as f:
			json.dump(can_rules, f, ensure_ascii=False, indent='\t')


	def _dtc_code(self, s, f):
		return ((s&0xff) << 5*8) | ((s>>8&0xff) << 4*8) | ((s>>16&0x7) << 3*8+5) | ((f&0x1f) << 3*8)

	def get_emul_data_value(self, is_dtc, can_item, tsdiag_par):
		if is_dtc:
			value = self._dtc_code(tsdiag_par.get('spn'), tsdiag_par.get('fmi', 0))
		else:
			value = 0x0123456789ABCDEF
		s_value = f'{value:016X}'
		assert len(s_value) == 16, f'[get_emul_data_value] error {len(s_value)}'
		return ' '.join(s_value[i:i+2] for i in range(0, 16, 2))

	# возвращает dict ('can item name', list of config words)
	def read_decoder_config(self, dec_dir):
		if not dec_dir: return
		decoder_items = {}

		spec_cfg_path = os.path.realpath(dec_dir)
		assert os.path.isdir(spec_cfg_path)
		for f in os.listdir(spec_cfg_path):
			self._read_matrix_file(os.path.join(spec_cfg_path, f), decoder_items)

		gen_cfg_path = os.path.realpath(os.path.join(dec_dir, '..', '..', 'general'))
		assert os.path.isdir(gen_cfg_path)
		for dirpath, dirnames, filenames in os.walk(gen_cfg_path):
			for fn in filenames:
				self._read_matrix_file(os.path.join(dirpath, fn), decoder_items)

		return decoder_items

	# в старой версии файлы матрицы лежали в одной директории
	def read_decoder_config_old(self, dec_dir):
		if not dec_dir: return
		decoder_items = {}
		dec_cfg_path = os.path.realpath(dec_dir)
		for f in os.listdir(dec_cfg_path):
			self._read_matrix_file(os.path.join(dec_cfg_path, f), decoder_items)
		return decoder_items

	def _read_matrix_file(self, path, decoder_items):
		with open(path) as dec_cfg_f:
			for l in dec_cfg_f:
				item_arr = l.split('\t')
				len_item_arr = len(item_arr)
				if len_item_arr < 2:
					continue
				assert len_item_arr >= 7, f'unexpected line in {f}; length {len_item_arr}: {l}'
				item_arr[6] = item_arr[6].rstrip()	# обрежем коммент
				decoder_items.setdefault(item_arr[0], []).append(item_arr)


	# %run /home/anton/_my/common_rep/py3/job/ttk/tsdiag_cfg_util.py  -c /home/anton/job/builds/tsdiag/par_dict1.json --dec_dir ~/rep-s/mnt2/go/cmd/canDeCoder/matrix/nefaz_5299 ip
	def ipython_work_main(self):
		return self

	# ./tsdiag_cfg_util.py -c ~/job/builds/tsdiag/par_dict1.json --dec_dir ~/rep-s/mnt2/go/cmd/canDeCoder/nefaz_5299 ch
	def check_config_main(self):
		ok = True
		found_errors = set()
		def error_found(what):
			nonlocal ok
			ok = False
			if what not in found_errors or self.args.show_rep_err:
				self.log.error(what)
				found_errors.add(what)

		tsdiag_ids = set()
		#tsdiag_uinames = set()
		for p in self.tsdiag_cfg['params']:
			if p['id'] in tsdiag_ids:
				error_found(f'repeated id {p["id"]}')
			tsdiag_ids.add(p['id'])

			#if p['ui_name'] in tsdiag_uinames:
			#	error_found(f'repeated ui_name {p["ui_name"]}')
			#tsdiag_uinames.add(p['ui_name'])

			is_dtc = False
			if 'dm1Name' in p:
				is_dtc = True
				can_item_names = (p['dm1Name'], )
			elif 'consistOf' in p:
				can_item_names = p['consistOf']
				if not isinstance(can_item_names, list):
					error_found(f'{p["id"]} has bad consistOf item')
					can_item_names = ()
			else:
				can_item_names = (p['id'], )

			prev_canid = None
			for can_item_name in can_item_names:
				if can_item_name in self.no_decoder_items:
					continue
				if can_item_name not in self.decoder_items:
					error_found(f'{can_item_name} is absent into the decoder config')
				else:
					decoder_items = self.decoder_items[can_item_name]
					q = len(decoder_items)
					if q != 1 and can_item_name != 'DCBN':		# уже не помню почему DCBN дублируется в матрице и это не ошибка (
						error_found(f'parameter {p["id"]} corresponds to {q} items {can_item_name} into the decoder config')
					else:
						canid = decoder_items[0][1]
						if prev_canid is not None and prev_canid != canid:
							error_found(f'consistOf items of {p["id"]} have different canid')
							break
						prev_canid = canid

			filterSuff = p.get('filterSuff')
			if filterSuff:
				if filterSuff not in self.decoder_items:
					error_found(f'parameter of {p["id"]}; filterSuff {filterSuff} is absent into the decoder config')
				elif self.decoder_items[filterSuff][0][1] != prev_canid:
					error_found(f'parameter of {p["id"]}; filterSuff canid is {self.decoder_items[filterSuff][0][1]} but it must be {prev_canid} (canid of consistOf items)')


			expr = p.get('expr')
			if expr is not None:
				func1 = lambda x:x
				try:
					eval(expr, {'x':1, 'swap2':func1, 'swap4':func1, 'swap3':func1})
				except:
					error_found(f'{p["id"]} has wrong expression {expr}')

		if ok:
			print('OK')
		else:
			print('ERROR')


	def check_all_config_main(self):
		if not os.path.isdir(self.args.tsdiag_dir):
			raise RuntimeError('wrong tsdiag config dir')
		if not os.path.isdir(self.args.candecoder_dir):
			raise RuntimeError('wrong decoder matrices dir')

		tsdiag_matrix_map = {
			'par_dict1.json' : 'nefaz_5299',
			'par_dict2.json' : 'liaz_6274',
			'par_dict5.json' : 'liaz_5292',
			'par_dict7.json' : 'maz',
			'par_dict8.json' : 'paz_electrobus',
			'par_dict9.json' : 'kamaz_diesel_itelma_obv',
		}

		checked = {}	# tsdiag_config_path : decoder_matrix_path

		tsdiag_config_glob = os.path.join(os.path.realpath(self.args.tsdiag_dir), 'par_dict*.json')
		for cfg_path in sorted(glob.glob(tsdiag_config_glob)):
			b = os.path.basename(cfg_path)
			matrix_name = tsdiag_matrix_map.get(b)
			if matrix_name is None:
				raise RuntimeError(f'unknown matrix for {b}')
			if matrix_name == '':
				print(f'SKIPPED check for {b}')
				continue
			matrix_path = os.path.join(os.path.realpath(self.args.candecoder_dir), matrix_name)
			if not os.path.isdir(matrix_path):
				raise RuntimeError(f'matrix dir {matrix_path} for {b} is not exists')
			checked[cfg_path] = matrix_path

		for cfg_path, matrix_path in checked.items():
			print(f'\nchecking: {os.path.basename(cfg_path)} with {os.path.basename(matrix_path)}')
			self.read_tsdiag_config(cfg_path)
			self.decoder_items = self.read_decoder_config(matrix_path)
			self.check_config_main()


	def db_cursor(self, path):
		if not os.path.isfile(path):
			raise RuntimeError(f'no file {path}')
		conn = sqlite3.connect(f'file:{path}?mode=ro', uri=True)	# read only mode
		cursor = conn.cursor()
		db_version = cursor.execute('PRAGMA user_version;').fetchall()[0][0]
		return cursor, db_version


	def invest_main(self):
		def check_id(id_, unique_ids):
			was_in = id_ in unique_ids
			if was_in:
				return False
			unique_ids.append(id_)
			if id_  not in self.tsdiag_params:
				print(f'db has unknown id {id_}', file=sys.stderr)
				return False
			return not was_in

		unique_ids = []
		out_params = copy.deepcopy(self.tsdiag_params)

		curs, _ = self.db_cursor(self.args.db)
		for row in curs.execute('SELECT id, value FROM diag ORDER BY time DESC'):
			id_ = row[0]; value = row[1]
			if not check_id(id_, unique_ids):
				continue
			out_params[id_]['LAST_VALUE'] = value

		unique_ids.sort()
		print(f'total {len(unique_ids)}', unique_ids)

		with open(os.path.join(self.args.out), 'w') as f:
			json.dump(out_params, f, ensure_ascii=False, indent='\t')





if __name__ == '__main__':
	ret = App().main()
