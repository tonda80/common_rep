import datetime


class G:
	log = None


class LogTime:
	def __init__(self, str_time):
		self.str_time = str_time
		self.datetime = datetime.datetime.strptime(str_time, self._format)		# TODO упадем тут 29 февраля _curr_year = datetime.datetime.now().year

	def __lt__(self, other):
		return seconds_between(self, other) > 0
	def __ge__(self, other):
		return not (self < other)

	def __str__(self):
		return self.str_time



# возвращает количество секунд между LogTime объектами (t1 - t0)
# пытаемся тут обработать вариант с не указанным годом
def seconds_between(log_time0, log_time1):
	dt0, dt1 = log_time0.datetime, log_time1.datetime

	if dt0.year != 1900 and dt1.year != 1900:
		return (dt1 - dt0).total_seconds()

	# иначе считаем без указания года
	dt0 = dt0.replace(year = 1900)
	dt1 = dt1.replace(year = 1900)
	td = dt1 - dt0
	# считаем что большая разница означает переход года

	if td > datetime.timedelta(days=183):
		td -= datetime.timedelta(days=365)
	if td < datetime.timedelta(days=-183):
		td += datetime.timedelta(days=365)

	# 30 дней должно быть достаточно, если же нет придется учитывать високосность (1900 дефолтный не високосный)
	# пока не будем мудрить и ограничимся ошибкой
	if td > datetime.timedelta(days=30) or td < datetime.timedelta(days=-30):
		G.log.error(f'too big time difference may provoke a mistake: {dt0} {dt1}')

	return td.total_seconds()


class SyslogTime(LogTime):
	_str_len = 15
	_format = '%b %d %H:%M:%S'

	@staticmethod
	def from_line(line):
		return SyslogTime(line[:SyslogTime._str_len])

	@staticmethod
	def from_bytes(line, coding):
		return SyslogTime(line[:SyslogTime._str_len].decode(coding))

	@staticmethod
	def to_str(dttime):
		return dttime.strftime(SyslogTime._format)

