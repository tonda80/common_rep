
# gui представление мнт логов (вариант 2)


import gzip
import os

import mix_utils

import logtime



class Line(mix_utils.Struct):
	@staticmethod
	def create(pos, line, kind, reader):
		return Line(pos=pos, line=line, kind=kind, time=reader.timeClass.from_line(line), reader = reader)



class LineReader:
	coding = 'utf8'	# TODO?

	# filter_ - функция принимающая строку и возвращающая None если строка неинтересна или её тип (индекс массива filters в json)
	def __init__(self, path, filter_, log):
		self.filter = filter_
		self.log = log

		self.file_obj = self._get_file_obj(path)
		self.file_name = os.path.basename(self.file_obj.name)

		self.timeClass = self._detect_time_class()

		self.previous_lines_cache = []

		self.log.info(f'opened {path}')

	def close(self):
		self.file_obj.close()
		self.log.info(f'closed {self.file_obj.name}')

	def seek(self, pos):
		self.previous_lines_cache.clear()
		self.file_obj.seek(pos)

	def tell(self):
		return self.file_obj.tell()

	def _readline(self):
		line = self.file_obj.readline()
		if line:
			return line.decode(self.coding, errors='replace')

	def _readline_time(self, step = 1):
		for i in range(step):
			line = self.file_obj.readline()
			if not line:
				return
		return self.timeClass.from_bytes(line, self.coding)

	def next_line(self):
		self.previous_lines_cache.clear()

		#tmr = mix_utils.DebugTimer(1000000)	tmr.mark(f'_deb next_line while')
		while 1:
			pos = self.file_obj.tell()
			line = self._readline()
			if not line:
				return
			kind = self.filter(line)
			if kind is not None:
				try:
					return Line.create(pos, line, kind, self)
				except Exception as e:
					self.log.error(f'cannot create line object for "{line}": {e}')

	def _get_previous_line_from_cache(self):
		line_obj = self.previous_lines_cache.pop()
		self.file_obj.seek(line_obj.pos)
		#print('_deb _get_previous_line_from_cache', self.file_name, line_obj.pos)
		return line_obj

	def previous_line(self):
		if self.previous_lines_cache:
			return self._get_previous_line_from_cache()

		step_offset = 5000		# величина шага перемотки назад
		end_pos = self.file_obj.tell()
		if end_pos == 0:
			return
		while 1:
			back_pos = end_pos - step_offset
			if back_pos < 0:
				back_pos = 0
			self.file_obj.seek(back_pos)
			if back_pos != 0:
				self.file_obj.readline()	# игнорируем вероятный кусок строки
			# перемотали куда-то назад (или остались на месте если вдруг step_offset <= размера строки)

			start_pos = pos = self.file_obj.tell()
			while pos < end_pos:
				line = self._readline()
				kind = self.filter(line)
				if kind is not None:
					try:
						self.previous_lines_cache.append(Line.create(pos, line, kind, self))
					except Exception as e:
						self.log.error(f'cannot create line object for "{line}": {e}')
				pos = self.file_obj.tell()
			# вычитали строки до end_pos
			assert pos == end_pos, 'for some reason we started not from line start?'

			if self.previous_lines_cache:
				return self._get_previous_line_from_cache()
				# либо вернули последнее найденное

			if back_pos == 0:
				self.seek(0)	# тут надо "вручную" перейти на 0, т.к. в начале могут быть неинтересные строки и требуется как то вызывающего уведомить что дошли до начала
				return
				# либо дошли до начала

			end_pos = start_pos
			step_offset *= 2
			# либо продолжаем поиск

	# перемотка вперед на время не меньшее time0 (или в конец)
	def rewind_forward_to(self, time0):
		while 1:
			line = self._readline()		# TODO? use _readline_time
			if not line:
				return
			time_ = self.timeClass.from_line(line)
			if logtime.seconds_between(time_, time0) < 0:
				return

	# перемотка на время не меньшее time0 (или в конец)
	def rewind_to(self, time0):
		self.seek(0)
		step = 1
		reached = False
		while 1:
			pos = self.file_obj.tell()
			time_ = self._readline_time(step)
			if not time_ or time_ >= time0:	# конец файла или время больше time0
				reached = True
				self.seek(pos)
				if step == 1:
					return
			else:							# время меньше time0
				if not reached:
					step *= 2
			if reached and step > 1:
				step //= 2

	def _get_file_obj(self, path):
		if mix_utils.check_ext(path, '.gz'):
			fobj = gzip.open(path, 'rb')
			fobj.peek(1)	# test
		else:
			fobj = open(path, 'rb')
		return fobj

	def _detect_time_class(self):
		return logtime.SyslogTime		# TODO



class Reader:
	def __init__(self, pathes, filter_, log):
		self.log = log

		self.file_readers = []
		for p in pathes:
			self.file_readers.append(LineReader(p, filter_, log))		# TODO? свои фильтры для ридеров

		self.n_first_positions = [0 for r in self.file_readers]		# позиции в файлах соответствующие выданному вызовами *_n_lines
		self.n_last_positions = [0 for r in self.file_readers]

		self.p_first_positions = [0 for r in self.file_readers]		# позиции в файлах соответствующие выданному вызовами *_p_lines
		self.p_last_positions = [0 for r in self.file_readers]


	def close(self):
		for r in self.file_readers:
			r.close()

	def _save_positions(self, list_):
		for i, r in enumerate(self.file_readers):
			list_[i] = r.tell()

	def _restore_positions(self, list_):
		for i, r in enumerate(self.file_readers):
			r.seek(list_[i])

	def zero_n_positions(self):
		self.n_first_positions = [0 for r in self.file_readers]
		self.n_last_positions = [0 for r in self.file_readers]

	def zero_p_positions(self):
		self.p_first_positions = [0 for r in self.file_readers]
		self.p_last_positions = [0 for r in self.file_readers]

	def is_previous_n_begin(self):
		# previous_n_lines перемотал до начала (что-то не 0)?
		return not any(self.n_first_positions)

	def is_previous_p_begin(self):
		# previous_p_lines перемотал до начала (что-то не 0)?
		return not any(self.p_first_positions)

	# перечитываем начиная с текуще выданного
	def reread_next_n_lines(self, q):
		self.n_last_positions = self.n_first_positions[:]
		return self.next_n_lines(q)

	def reread_next_p_lines(self, seconds):
		self.p_last_positions = self.p_first_positions[:]
		return self.next_p_lines(seconds)

	def _get_lines_indx(self, lines, next_):
		if next_:
			get_method = LineReader.next_line
			extremum_func = min
		else:
			get_method = LineReader.previous_line
			extremum_func = max

		rd_indexes = list(range(len(self.file_readers)))
		while 1:
			for i in rd_indexes[:]:
				if i not in lines:				# читаем из каждого файла строку, если ещё не
					line = get_method(self.file_readers[i])
					if line is None:
						rd_indexes.remove(i)	# удаляем индекс файла из которого нечего читать
					else:
						lines[i] = line
			if not lines:
				break
			i_extr = extremum_func(lines, key=lambda i: lines[i].time)
			yield i_extr		# выдали ключ с МИН\МАКС временем

	def _return_lines(self, lines, next_):
		# не попавшие в выходной список строки возвращаем в файл
		for i, l in lines.items():
			if next_:
				pos = l.pos
			else:
				pos = l.pos + len(l.line.encode(l.reader.coding))
			self.file_readers[i].seek(pos)

	def next_n_lines(self, q):
		self._restore_positions(self.n_last_positions)

		ret_lines = []
		lines = {}
		for i in self._get_lines_indx(lines, True):
			ret_lines.append(lines.pop(i))
			if len(ret_lines) >= q:
				break
		self._return_lines(lines, True)

		if ret_lines:
			self.n_first_positions = self.n_last_positions[:]
			self._save_positions(self.n_last_positions)
		return ret_lines

	def previous_n_lines(self, q):
		self._restore_positions(self.n_first_positions)

		ret_lines = []
		lines = {}
		for i in self._get_lines_indx(lines, False):
			ret_lines.insert(0, lines.pop(i))
			if len(ret_lines) >= q:
				break
		self._return_lines(lines, False)

		if ret_lines:
			self.n_last_positions = self.n_first_positions[:]
			self._save_positions(self.n_first_positions)
		return ret_lines

	def next_p_lines(self, seconds):
		self._restore_positions(self.p_last_positions)

		ret_lines = []
		lines = {}
		start_time = None
		for i in self._get_lines_indx(lines, True):
			if start_time is None:
				start_time = lines[i].time
			if logtime.seconds_between(start_time, lines[i].time) < seconds:
				ret_lines.append(lines.pop(i))
			else:
				break
		self._return_lines(lines, True)

		if ret_lines:
			self.p_first_positions = self.p_last_positions[:]
			self._save_positions(self.p_last_positions)
		return ret_lines

	def previous_p_lines(self, seconds):
		self._restore_positions(self.p_first_positions)

		ret_lines = []
		lines = {}
		end_time = None
		for i in self._get_lines_indx(lines, False):
			if end_time is None:
				end_time = lines[i].time
			if logtime.seconds_between(lines[i].time, end_time) < seconds:
				ret_lines.insert(0, lines.pop(i))
			else:
				break
		self._return_lines(lines, False)

		if ret_lines:
			self.p_last_positions = self.p_first_positions[:]
			self._save_positions(self.p_first_positions)
		return ret_lines

	# next_p_lines выдает массив, а тут выдаем строки по одной
	def lines_from_to(self, start_time, end_time):
		self.rewind_to(start_time)

		lines = {}
		for i in self._get_lines_indx(lines, True):
			l = lines[i]
			if l.time > end_time:
				break
			yield lines.pop(i)
		self._return_lines(lines, True)


	def rewind_n_forward_to(self, log_line):
		self._restore_positions(self.p_first_positions)	# вызывается после клика по точке и искать будем вперед, поэтому мотаем на первую возможную точку
		for r in self.file_readers:
			if r is log_line.reader:
				r.seek(log_line.pos)
			else:
				r.rewind_forward_to(log_line.time)
		self._save_positions(self.n_last_positions)
		#self._save_positions(self.p_last_positions)


	# перематываем файлы на время не меньше time0
	def rewind_to(self, time0, save_last_pos = False):
		for r in self.file_readers:
			r.rewind_to(time0)

		if save_last_pos:
			self._save_positions(self.n_last_positions)
			self._save_positions(self.p_last_positions)
