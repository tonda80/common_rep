import datetime

import uTkinter as utk

import logtime


class Timeline(utk.uCanvas):
	Hint_tag = 'hint'
	Point_tag = 'point'
	Line_tag = 'line'
	Label_tag = 'label'

	Point_diam = 20
	Row_height = Point_diam + 5

	def __init__(self, owner, *args, **kwargs):
		utk.uCanvas.__init__(self, *args, **kwargs)

		self.owner = owner
		self.log = owner.log

		self.points = {}

		self.create_text(0, 0, tags = self.Hint_tag, anchor=utk.SW, font='Courier 10 bold')

		# чекбоксы
		for i, filter_ in enumerate(self.owner.filters):
			y = self._get_row_y(i)
			self.create_window((0, y), window = utk.uCheckbutton(
				None, filter_.name, utk.uCallback(self.on_filter_checkbox_changed, i),
				defaultValue = filter_.enabled, bg = self['bg']), anchor=utk.W)		# fg = filter_.color
		self.config(scrollregion=self.bbox(utk.ALL))

		self.bind('<ButtonPress-1>', self._button1_press_handler)
		self.bind('<ButtonPress-3>', self._button3_press_handler)
		self.bind('<Motion>', self._motion_handler)

	def _get_row_y(self, i):
		# середина i-й строки (нулевая для подписи времени)
		return (i + 1)*self.Row_height + self.Row_height//2

	def represent_points(self, log_lines, period):
		self.log.debug(f'represent_points {len(log_lines)} {period}')
		if log_lines:
			# стираем старое
			self.delete(self.Point_tag)
			self.points.clear()
			self.delete(self.Line_tag)
			self.delete(self.Label_tag)

			lshift = 120
			rshift = 10
			coef = (self.winfo_width() - lshift - rshift)/period

			# рисуем точки
			dt0 = log_lines[0].time
			for l in log_lines:
				x = lshift + logtime.seconds_between(dt0, l.time)*coef
				self._add_point(x, l)

			# рисуем линии фильтров (горизонтальные)
			for i, _ in enumerate(self.owner.filters):
				y = self._get_row_y(i)
				self.create_line((lshift, y, self.winfo_width(), y), tags = self.Line_tag)

			# рисуем линии времени (вертикальные) с подписями
			q = 5
			try:
				ts0 = dt0.datetime.timestamp()
			except OSError:
				ts0 = dt0.datetime.replace(year=2000).timestamp()	# на винде timestamp не работает с 1900 годом
			for i in range(q):
				x = i*self.winfo_width()/q + lshift
				dttime = datetime.datetime.fromtimestamp(ts0 + (x - lshift)/coef) 		# обратное к получению x выше
				self.create_text(x + 5, self.Row_height//2, text = logtime.SyslogTime.to_str(dttime), tags = self.Label_tag, anchor=utk.NW)
				self.create_line((x, 0, x, self.winfo_height()), tags = self.Line_tag)

			self.itemconfig(self.Line_tag, width = 1, fill = utk.rgb(200, 200, 200))
			self.tag_lower(self.Line_tag)

			self.config(scrollregion=self.bbox(utk.ALL))


	def _add_point(self, x, log_line):
		y = self._get_row_y(log_line.kind)
		d = self.Point_diam/2
		color = self.owner.get_kind_color(log_line.kind)

		item = self.create_oval(x - d, y - d, x + d, y + d, fill=color, width=0, tags=self.Point_tag)	# , outline='white'
		self.points[item] = log_line


	def _button1_press_handler(self, event):
		item = self._find_point_item(event)
		if item is not None:
			self.itemconfig(self.Point_tag, width=0)
			# сбросили подсветку
			self.itemconfig(item, width=4, outline='black')
			self.tag_raise(item)
			# подсветили
			self.owner.timeline_point_left_button_handler(self.points[item])

	def _button3_press_handler(self, event):
		item = self._find_point_item(event)
		if item is not None:
			self.owner.timeline_point_right_button_handler(self.points[item])


	def	_motion_handler(self, event):
		item = self._find_point_item(event)
		if item:
			x, y, _, _ = self.coords(item)
			self.coords(self.Hint_tag, x, y-self.Row_height//2)
			self.itemconfig(self.Hint_tag, text = self.points[item].time.str_time, anchor=utk.NW if x < self.winfo_width()/2 else utk.NE)
			self.tag_raise(self.Hint_tag)
		else:
			self.itemconfig(self.Hint_tag, text = '')

	def _find_point_item(self, event):
		x = self.canvasx(event.x)
		y = self.canvasy(event.y)
		d = 2
		for it in self.find_overlapping(x, y, x+1, y+1):
			if self.Point_tag in self.gettags(it):
				return it

	def on_filter_checkbox_changed(self, checkbox, i):
		self.owner.filters[i].set_enabled(checkbox.getVar())
