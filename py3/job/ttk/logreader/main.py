
import json
import os
import random
import math
import re
import datetime
import tkinter.filedialog
import tkinter.simpledialog
import clipboard		# pip3 install clipboard; apt install xclip

from baseapp import BaseConsoleApp
import uTkinter as utk
import mix_utils

from reader import Reader
from timeline import Timeline
import logtime
from doors_report import DoorsReport


class Config:
	def __init__(self, path, log):
		self.log = log
		with open(path) as f:
			root = json.load(f)
		self.filters = root['filters']

		self.page_size = root.get('page_size')


class Filter:
	def __init__(self, log, num, filter_dict):
		self.log = log
		self.num = num

		self.name = filter_dict.get('name', '')

		self.enabled = bool(filter_dict.get('enabled', True))

		self.mnt_module = filter_dict.get('mnt_module')
		self.mnt_module_re = re.compile(r'[ :\w]{15} \w+ (\w+)') if self.mnt_module else None		# syslog format, TODO

		self.contains = filter_dict.get('contains')

		self.color = filter_dict.get('color', 'gray')
		try:
			utk.Label(None, bg=self.color)
		except utk.TclError:
			raise RuntimeError(f'filter "{self.name}" ({num}-th) has wrong color')

		self.extra_filter_str = ''
		self.extra_filter = None
		extra_filter_str = filter_dict.get('extra_filter', '')
		try:
			self.set_extra_filter(extra_filter_str)
		except Exception as e:
			log.error(f'Cannot apply extra filter {extra_filter_str} for {self.name}: {e}')

	def set_enabled(self, value):
		self.enabled = value

	def set_extra_filter(self, extra_filter_str):
		extra_filter_str = extra_filter_str.strip()
		if not extra_filter_str:	# пустая строка - сброс
			self.extra_filter_str = ''
			self.extra_filter = None
			return

		func = eval('lambda line:' + extra_filter_str)
		func('some test string')
		# если не кинули исключение, то считаем все ок
		self.extra_filter_str = extra_filter_str
		self.extra_filter = func

	def check(self, line):
		if not self.enabled:
			return

		ok = False
		if self.mnt_module is not None:
			mo = self.mnt_module_re.match(line)
			ok = mo and mo.group(1).lower() == self.mnt_module
		elif self.contains is not None:
			ok = self.contains in line

		if ok and self.extra_filter is not None:
			ok = self.extra_filter(line)

		if ok:
			return self.num



class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('--logs', nargs='+', help='Пути к логам')
		self.parser.add_argument('--cfg', help='Путь к конфигу')
		self.parser.add_argument('-g', action='store_true', help='Сгенерировать тестовый файл')

	def check_line(self, line):
		for f in self.filters:
			n = f.check(line)
			if n is not None:
				return n

	def get_kind_color(self, i):
		return self.filters[i].color

	def _get_config(self):
		if self.args.cfg:
			return self.args.cfg
		def_config = mix_utils.path_to(__file__, 'mnt_config.json')
		if os.path.isfile(def_config):
			return def_config
		return tkinter.filedialog.askopenfilename(title='Select config')

	def main(self):
		if self.args.g:
			return self._generate_test_log(self.args.logs[0])

		logtime.G.log = self.log

		self.root = utk.uTk(title='Log reader')

		config = self._get_config()
		if not config:
			return
		self.config = Config(config, self.log)

		self.filters = []
		for i, filter_dict in enumerate(self.config.filters):
			self.filters.append(Filter(self.log, i, filter_dict))

		log_pathes = self.args.logs if self.args.logs else self._get_log_pathes()
		if not log_pathes:
			return
		self.log_reader = Reader(log_pathes, self.check_line, self.log)

		self.first_point_line = self.last_point_line = None

		self.init_gui()

		self.export_window = None

		self.period_options = (60, 600, 3600, 3*3600, 12*3600, 24*3600, 48*3600)
		self.period_index = 0

		try:
			self.root.mainloop()
		finally:
			self.log_reader.close()

	def init_gui(self):
		timeline_height = 200	# подстраивать под размер?

		#root.set_delete_callback(self.delete_callback)

		frame_log_view = utk.uFrame(self.root, gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})
		frame_control = utk.uFrame(self.root, gmArgs = {'side':utk.BOTTOM, 'expand':utk.NO, 'fill':utk.X})
		frame_timeline = utk.uFrame(self.root, height=timeline_height, gmArgs = {'side':utk.BOTTOM, 'expand':utk.NO, 'fill':utk.X})

		self.log_view = utk.uListbox(frame_log_view, 'xy', font=('Helvetica', 11), gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})
		self.log_view.addMouseCallback(lambda i,s:clipboard.copy(s), '<Double-Button-1>')

		self.timeline = Timeline(self, frame_timeline, scrollmode='y', height=timeline_height, gmArgs = {'side':utk.TOP, 'expand':utk.NO, 'fill':utk.X})

		btn_gmArgs = {'side':utk.LEFT}
		bw = 7

		utk.uButton(frame_control, self.update, 'Update', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.rewind, 'Rewind', width = bw, gmArgs = btn_gmArgs)
		#
		utk.uLabel(frame_control, '', {'side':utk.LEFT, 'padx':40})
		utk.uButton(frame_control, self.log_view_first, 'First', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.log_view_up, 'Up', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.log_view_down, 'Down', width = bw, gmArgs = btn_gmArgs)
		#utk.uButton(frame_control, self.log_view_last, 'Last', gmArgs = btn_gmArgs)
		#
		utk.uLabel(frame_control, '', {'side':utk.LEFT, 'padx':40})
		utk.uButton(frame_control, self.timeline_first, '<<', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline_left, '<', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline_right, '>', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline_more, '+', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline_less, '-', width = bw, gmArgs = btn_gmArgs)
		#utk.uButton(frame_control, self.timeline.last, '>>', gmArgs = btn_gmArgs)
		#
		utk.uLabel(frame_control, '', {'side':utk.LEFT, 'padx':40})
		utk.uButton(frame_control, self.export_to_win, 'To win', width = bw, gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.export_to_file, 'To file', width = bw, gmArgs = btn_gmArgs)
		#
		utk.uLabel(frame_control, '', {'side':utk.LEFT, 'padx':10})
		utk.uButton(frame_control, lambda:CreateReportWindow(self) , 'Report', width = bw, gmArgs = btn_gmArgs)

		utk.maximizeWindow(self.root)
		self.root.after(250, self.after_opening)

	def after_opening(self):
		self.listbox_size = self.config.page_size if self.config.page_size else self._get_listbox_size()
		assert self.listbox_size > 1

		self.log_view_down()
		self.timeline_right()
		#print('_deb', len(box_log_lines), self.listbox_size, len(timeline_log_lines), period)


	def _get_listbox_size(self):
		# костылище, но умней не нашлось и не придумалось
		self.log_view.append(''); self.log_view.append('')
		for i in range(50):
			if self.log_view.nearest(i) == 1:
				break
		self.log_view.clear()
		return math.ceil(self.log_view.winfo_height()/(i-1))

	def represent_lines(self, log_lines):
		self.log.debug(f'represent_lines {len(log_lines)}')
		if log_lines:
			self.log_view.clear()
			for l in log_lines:
				out_s = f'[{l.reader.file_name}]  {l.line}'		# TODO номер строки?
				self.log_view.append(out_s, bg=self.get_kind_color(l.kind))

	def represent_points(self, timeline_log_lines, period):
		self.root.statusBar.set_r(f'period: {period} seconds')
		self.timeline.represent_points(timeline_log_lines, period)
		if timeline_log_lines:
			self.first_point_line = timeline_log_lines[0]
			self.last_point_line = timeline_log_lines[-1]

	def timeline_point_left_button_handler(self, log_line):
		self.log_reader.rewind_n_forward_to(log_line)

		self.log_view_down()
		#self.timeline_right()

	def timeline_point_right_button_handler(self, log_line):
		filter_ = self.filters[log_line.kind]
		def apply_extra_filter():
			try:
				filter_.set_extra_filter(entry.getVar())
			except Exception as e:
				msg_label.config(text=f'Cannot apply filter.\n{e}')
				return
			win.destroy()

		gm_args = {'pady': 5, 'side':utk.TOP, 'expand':utk.YES, 'fill':utk.X}
		win = utk.uToplevel(self.root, True, f'Extra filter for "{filter_.name}"')
		entry = utk.uEntry(win, filter_.extra_filter_str, gmArgs=gm_args, width=100, font='Courier 12')
		utk.uButton(win, apply_extra_filter, 'Apply', gmArgs=gm_args)
		msg_label = utk.uLabel(win, '')


	def log_view_down(self):
		box_log_lines = self.log_reader.next_n_lines(self.listbox_size)
		self.represent_lines(box_log_lines)

	def log_view_up(self):
		box_log_lines = self.log_reader.previous_n_lines(self.listbox_size)
		if self.log_reader.is_previous_n_begin():	# and len(box_log_lines) < self.listbox_size
			# дошли до начала и не набрали на экран, перечитаем вперед с начала
			self.log_reader.zero_n_positions()
			box_log_lines = self.log_reader.next_n_lines(self.listbox_size)
		self.represent_lines(box_log_lines)

	def log_view_first(self):
		self.log_reader.zero_n_positions()
		self.log_view_down()

	def log_view_reread(self):
		box_log_lines = self.log_reader.reread_next_n_lines(self.listbox_size)
		self.represent_lines(box_log_lines)

	def timeline_right(self):
		period = self.period_options[self.period_index]
		timeline_log_lines = self.log_reader.next_p_lines(period)
		self.represent_points(timeline_log_lines, period)

	def timeline_left(self):
		period = self.period_options[self.period_index]
		timeline_log_lines = self.log_reader.previous_p_lines(period)
		if self.log_reader.is_previous_p_begin():	# тут вроде никак не проверить набрали ли мы на экран
			# дошли до начала, перечитаем вперед с начала
			self.log_reader.zero_p_positions()
			timeline_log_lines = self.log_reader.next_p_lines(period)
		self.represent_points(timeline_log_lines, period)

	def timeline_first(self):
		self.log_reader.zero_p_positions()
		self.timeline_right()

	def timeline_more(self):
		self._change_period(1)

	def timeline_less(self):
		self._change_period(-1)

	def _change_period(self, d):
		new_index = self.period_index + d
		if 0 <= new_index < len(self.period_options):
			self.period_index = new_index
			self.timeline_reread()

	def timeline_reread(self):
		period = self.period_options[self.period_index]

		timeline_log_lines = self.log_reader.reread_next_p_lines(period)
		self.represent_points(timeline_log_lines, period)

	def update(self):
		self.timeline_reread()
		self.log_view_reread()

	def rewind(self):
		str_time = tkinter.simpledialog.askstring('Rewind', 'time', initialvalue='Jul  2 07:40:27')
		if not str_time:
			return
		try:
			time_ = logtime.SyslogTime.from_line(str_time)
		except Exception as e:
			tkinter.messagebox.showerror('Wrong time string', str(e))
			return

		self.log_reader.rewind_to(time_, True)
		self.log_view_down()
		self.timeline_right()

	def _generate_test_log(self, path):
		if os.path.exists(path):
			self.log.error(f'{path} exists')
			return
		options = {'usb':0.1, 'pci':0.2, 'died':0.05}
		size = random.randint(1000, 2000)
		dttime = datetime.datetime.now() - datetime.timedelta(days=60)
		with open(path, 'w') as f:
			for i in range(size):
				data = f'some uninteresting string {random.randint(0, 99):02d}'
				for opt, prob in options.items():
					if random.random() < prob:
						data = f'{opt} {random.randint(0, 99):02d}'
				dttime += datetime.timedelta(seconds=random.randint(10, 20))
				f.write(f'{logtime.SyslogTime.to_str(dttime)} {data}\n')

	def _get_log_pathes(self):
		class C:
			log_pathes = set()
			applied = False
			initialdir = None

		def add_pathes():
			for p in tkinter.filedialog.askopenfilenames():
				p = os.path.realpath(p)
				if p not in C.log_pathes:
					C.log_pathes.add(p)
					lbox.add(utk.END, p)
				C.initialdir = os.path.dirname(p)
		def clear():
			C.log_pathes.clear()
			lbox.clear()
		def go():
			C.applied = True
			win.destroy()

		gm_args = {'pady': 1, 'side':utk.TOP, 'expand':utk.YES, 'fill':utk.X}
		win = utk.uToplevel(self.root, True, f'Add log files')
		lbox = utk.uListbox(utk.uFrame(win, gmArgs={'expand':utk.YES, 'fill':utk.BOTH}), 'xy', gmArgs = {'expand':utk.YES, 'fill':utk.BOTH})
		utk.uButton(win, add_pathes, 'Add', gmArgs=gm_args, width=60)
		utk.uButton(win, clear, 'Clear', gmArgs=gm_args)
		utk.uButton(win, go, 'Go', gmArgs=gm_args)

		utk.placeWindowTo(win, self.root.winfo_x(), 0)
		win.wait_window()
		if C.applied:
			return C.log_pathes

	def export_to_file(self):
		_lines_to_file(self.log_view)

	def export_to_win(self):
		if not self.export_window or not self.export_window.winfo_exists():
			self.export_window = ExportWindow(self.root)
		self.export_window.export(self.log_view)


def _lines_to_file(listbox):
		fname = tkinter.filedialog.asksaveasfilename(title='File to export')
		if not fname:
			return
		with open(fname, 'w') as f:
			for i in range(listbox.size()):
				f.write(listbox.get(i))


class ExportWindow(utk.uToplevel):
	def __init__(self, master):
		utk.uToplevel.__init__(self, master, False, 'Export parts')
		frame_exported = utk.uFrame(self, gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})
		frame_control = utk.uFrame(self, gmArgs = {'side':utk.BOTTOM, 'expand':utk.NO, 'fill':utk.X})

		self.export_listbox = utk.uListbox(frame_exported, 'xy', font=('Helvetica', 11), gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})

		btn_gmArgs = {'side':utk.LEFT}
		#
		utk.uButton(frame_control, lambda: self.export_listbox.clear(), 'Clear', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.export_to_file, 'To file', gmArgs = btn_gmArgs)

		#utk.maximizeWindow(self)

	def export(self, listbox):
		for i in range(listbox.size()):
			self.export_listbox.append(listbox.get(i), bg=listbox.itemcget(i, 'bg'))
		self.export_listbox.append('....\n')

	def export_to_file(self):
		_lines_to_file(self.export_listbox)


class CreateReportWindow(utk.uToplevel):
	report_makers = {
		'doors' : DoorsReport
	}

	def __init__(self, app):
		self.app = app

		utk.uToplevel.__init__(self, app.root, True, 'Create report')

		gmArgs={'row':0, 'column':0, 'padx':20, 'pady':10, 'sticky': 'WE'}
		def n_col(): gmArgs['column'] += 1
		def n_row(): gmArgs['row'] += 1; gmArgs['column'] = 0

		utk.uLabel(self, 'Report type', gmArgs=gmArgs); n_col()
		self.type_om = utk.uOptionMenu(self, list(self.report_makers), gmArgs=gmArgs); n_row()

		utk.uLabel(self, 'Start time', gmArgs=gmArgs); n_col()
		init_s = app.first_point_line.time.str_time if app.first_point_line else ''
		self.start_e = utk.uEntry(self, init_s, gmArgs=gmArgs); n_row()

		utk.uLabel(self, 'End time', gmArgs=gmArgs); n_col()
		init_s = app.last_point_line.time.str_time if app.last_point_line else ''
		self.end_e = utk.uEntry(self, init_s, gmArgs=gmArgs); n_row()

		self.out_file_name = ''
		def set_out_file_name():
			self.out_file_name = tkinter.filedialog.asksaveasfilename(title='Select report file')
		utk.uButton(self, set_out_file_name, 'Set file', gmArgs=gmArgs); n_col()
		utk.uButton(self, self.go, 'Start', gmArgs=gmArgs)

		utk.placeWindowTo(self, app.root.winfo_x(), 0)

		#debug self.set_delete_callback(app.root.close)

	def _get_time(self, entry):
		try:
			return logtime.SyslogTime.from_line(entry.getVar())
		except ValueError:
			entry.select_all()
			return

	def go(self):
		self.start_e.selection_clear()
		self.end_e.selection_clear()

		start_time = self._get_time(self.start_e)
		end_time = self._get_time(self.end_e)
		if not start_time or not end_time:
			return

		try:
			report_maker = self.report_makers[self.type_om.getVar()](self.app, self.out_file_name)
			for l in self.app.log_reader.lines_from_to(start_time, end_time):
				report_maker.handle(l)
		finally:
			report_maker.restore_filter()



if __name__ == '__main__':
	if 1:
		App().main()
	else:
		import cProfile
		cProfile.run('App().main()')
