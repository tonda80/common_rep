Старт
main --cfg CONFIG --logs LOG_FILES
	cfg и logs могут быть опущены, тогда файлы надо будет выбирать через диалог
main -h		помощь по аргументам командной строки

Дополнительный фильтр можно ввести кликом правой кнопки по точкам таймлайна.
Вводится строка вида, "SOME FILTER" in line or "ANOTHER" in line and "MORE" not in line
