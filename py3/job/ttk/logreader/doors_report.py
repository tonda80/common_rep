
import json
import re
import sys
import json


class BaseReport:
	def __init__(self, app, out_file):
		self.app = app
		self.curr_log_line = None
		self.out_file = open(out_file, 'w') if out_file else sys.stdout
		self._set_filter()
		self.init()

	def _set_filter(self):
		self.saved_filters = {}
		for f in self.app.filters:
			self.saved_filters[f.name] = f.enabled
			if f.name in self.need_filters:
				f.enabled = True
			else:
				f.enabled = False

	def restore_filter(self):
		for f in self.app.filters:
			f.enabled = self.saved_filters[f.name]

	def handle(self, line):
		self.curr_log_line = line.line
		mnt_module = self.app.filters[line.kind].mnt_module
		#print(self.curr_log_line, mnt_module)
		assert mnt_module in self.need_filters
		handle = self.__class__.__dict__.get(mnt_module+'_handle')
		if handle:
			handle(self)
		else:
			self.default_handler()

	def default_handler(self):
		pass #print('zzz', self.curr_log_line)

	def report_out_file(self, essence = ''):
		self.out_file.write(essence)
		self.out_file.write('\t\t\t\t')
		self.out_file.write(self.curr_log_line)



class DoorsReport(BaseReport):
	need_filters = {'mqtt', 'routesmodule', 'mqttsender', 'apc'}

	def init(self):
		self.stop_re = re.compile(r'INFO: rID .+ sID .+ sName ([\w ]+)$')
		self.door_re = re.compile(r'INFO: Door #(\d+).*Collect data:\s?(\w+)')
		self.apc_report_open_re = re.compile(r'DEBUG: Send to MQTTSENDER: .*  FIRST OPENED')
		self.mqtt_passenger_counter_re = re.compile(r'topic:<PASSENGER_COUNTER/.* data:<(.*)>')
		#self._re = re.compile(r'')


	def routesmodule_handle(self):
		mo = self.stop_re.search(self.curr_log_line)
		if mo:
			last_stop = mo.group(1)
			self.out_file.write('\n\n')
			self.report_out_file(f'Зона остановки "{last_stop}"')

	def apc_handle(self):
		mo = self.door_re.search(self.curr_log_line)
		if mo:
			state = mo.group(2)
			if state == 'STATE_DETECTED_OPEN':
				self.report_out_file(f'Открыта дверь {mo.group(1)}')
			elif state == 'STATE_DETECTED_CLOSE':
				self.report_out_file(f'Закрыта дверь {mo.group(1)}')
			return
		mo = self.apc_report_open_re.search(self.curr_log_line)
		if mo:
			self.report_out_file('APC report to MQTT')
			return

	def mqtt_handle(self):
		mo = self.mqtt_passenger_counter_re.search(self.curr_log_line)
		if mo:
			data = json.loads(mo.group(1))
			self.report_out_file(f"MQTT шлет данные по остановке \"{data['stop_name']}\": in:{data['in']}, out:{data['out']}, not_payed_counter:{data['not_payed_counter']}, total_counter:{data['total_counter']}")



if __name__ == '__main__':
	pass
