#!/usr/bin/env python3
# coding=utf8


# шлем из консоли команды в ttk_helper

import json
import sys
import zmq

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-p', '--port', type=int, default=21347)

		deploy_parser = self.add_subparser('dp', self.deploy_command, help='Шлет деплой команду')
		deploy_parser.add_argument('host', help='ttk_helper имя хоста')
		deploy_parser.add_argument('target', help='ttk_helper таргет (_Files)')

	def main(self):
		zmq_context = zmq.Context()
		ep = f'tcp://127.0.0.1:{self.args.port}'
		self.socket = zmq_context.socket(zmq.REQ)
		self.socket.connect(ep)
		self.log.debug(f'connected to {ep}')

		return self.sub_main()


	def deploy_command(self):
		print('started deploy')
		msg = json.dumps({'command': 'deploy', 'host': self.args.host, 'target': self.args.target})
		self.socket.send(msg.encode())
		self.log.debug(f'sent {msg}')
		msg_in = self.recv_with_timeout(self.socket, 25000)
		if msg_in is None:
			print('recv timeout')
			return 1
		reply = json.loads(msg_in.decode())
		self.log.debug(f'recieved {reply}')
		result = reply.get('result')
		print(f'deploy {"ok" if result else "fail"} {reply.get("description")}')
		return 0 if result else 1


	def recv_with_timeout(self, socket, msec):
		if socket.poll(msec) & zmq.POLLIN:
			return socket.recv()


if __name__ == '__main__':
	try:
		sys.exit(App().main())
	except KeyboardInterrupt:
		sys.exit(1)
