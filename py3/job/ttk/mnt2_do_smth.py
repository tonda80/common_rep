#!/usr/bin/env python3
# coding=utf8


# чудо скрипт для максимы, делает разное. вызывается из mnt_route_loop.sh


import zmq				# pip install zmq
import sys
import json
import os
import time
import threading

sys.path.append(os.path.expanduser('~/ab/py_env'))

import mntproto_pb2		# pip install protobuf
#import usbdiag_pb2
#import canparser_pb2
#import board_pb2
#import boarddevice_pb2
import navigation_pb2
#import alert_pb2
#import dido_pb2
import routes_pb2
import uiactions_pb2
#import getversion_pb2
#import redis_pb2
#import wssm_pb2
#import validator_pb2

from baseapp import BaseConsoleApp
import zmq_client_mnt2
from zmq_client_mnt2 import get_data_object



class G:
	stop = threading.Event()
	log = None


class Waiter:
	def __init__(self, checker):
		self.checker = checker			# получает mnt сообщение, если возвращает не None - дождались нужного, возвращаемое сохраняем в result
		self.event = threading.Event()
		self.wait = self.event.wait
		self.result = None

	def check(self, mnt_command):
		res = self.checker(mnt_command)
		if res is not None:
			self.event.set()
			self.result = res
			return True


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('--router', '-r', action='store_true', help='Взаимодействие через роутер')
		self.parser.add_argument('--ip', default='127.0.0.1', help='IP для коммуникации, например 192.168.136.114')
		#
		self.parser.add_argument('--target', '-t', required=True, choices=('sr', 'srmp'), help='''
Что делать:
	sr - выбор маршрута (и переход в авто режим),
	srmp - выбор маршрута с ручным проирыванием оного
''')
		self.parser.add_argument('data', nargs='*')


	def main(self):
		G.log = self.log
		G.args = self.args

		self.waiters = []

		if self.args.router:
			router_mode = True
			pub_endpoint = f'tcp://{self.args.ip}:10125'
			sub_endpoint = f'tcp://{self.args.ip}:10126'
		else:
			router_mode = False
			pub_endpoint = f'tcp://{self.args.ip}:10126'
			sub_endpoint = f'tcp://{self.args.ip}:10125'

		zmq_client_mnt2.G.init(G)
		self.zmq_client = zmq_client_mnt2.ZmqMntClient(b'TestApp', self.zmq_recv_handler, (pub_endpoint, sub_endpoint), router_mode, 1)
		self.zmq_client.error_handler = self.zmq_error_handler
		G.zmq_client = self.zmq_client

		# и собственно ради чего огород
		if self.args.target == 'sr':
			self.action_select_route()
			self.route_set_auto_mode()		# на всякий случай включим
		elif self.args.target == 'srmp':
			self.action_select_route_and_manual_play()
		else:
			raise RuntimeError('unknown target')

	def zmq_recv_handler(self, mntCommand):
		#self.log.debug('received {} from {}'.format(mntCommand.header.name, mntCommand.header.source))
		expired = []
		for i, w in enumerate(self.waiters):
			if w.check(mntCommand):
				expired.append(i)
		for i in reversed(expired):
			del self.waiters[i]

	def zmq_error_handler(self, msgs):		# msgs - сырое мультипарт сообщение
		s_msgs = str(msgs)
		if len(s_msgs) > 150:
			s_msgs = s_msgs[:100] + ' ...'
		self.log.error(f'non-parced recv messages {s_msgs}')

	def add_waiter(self, checker):
		self.waiters.append(Waiter(checker))
		return self.waiters[-1]

	def data_arg(self, i, default = None, tr = None):
		try:
			ret = self.args.data[i]
		except IndexError:
			if default is None:
				raise
			return default
		if tr:
			return tr(ret)
		return ret

	# выбор маршрута
	def action_select_route(self):
		route_db_id = self.data_arg(0, tr=int)
		def is_selected(mntCommand):
			if mntCommand.header.name == 'RouteData':		# не будем тут проверять что именно мы там выбрали
				return mntCommand
		waiter = self.add_waiter(is_selected)
		while 1:
			self.zmq_client.publish(b'ROUT', b'UIGetRouteDataById', None, json.dumps({'uiRouteId': route_db_id}))
			if waiter.wait(3):
				break
			self.log.warning('Looks like route was not selected, repeating')
		self.log.info(f'route {route_db_id} is selected')
		return waiter.result


	# выбор маршрута с "ручным" проигрыванием оного
	def action_select_route_and_manual_play(self):
		route_data_cmd = self.action_select_route()
		data = get_data_object(route_data_cmd, routes_pb2.RouteData)
		route = json.loads(data.routeDataJSON)
		sp = self.data_arg(1, 2, tr=float)	# в 0 номер выбираемого маршрута

		G.log.info(f'route: id={route["id"]} name={route["name"]}; delay {sp}')
		G.log.debug(f'all RouteData \n\n{route}')

		def stop_name(i):
			return route['stops'][i]['name']
		def gap_name(i):
			return route['betweenGaps'][i]['name']

		# --- и играем

		self.route_set_manual_mode()

		for i in range(len(route['stops']) - 1):
			time.sleep(sp)
			self.send_UISendRouteCurrentStop(uiRouteName=route['name'], uiCurrentStopName=stop_name(i), uiNextStopName=stop_name(i+1), uiStopId=i, isStop=True)
			time.sleep(sp)
			self.send_UISendRouteNextStop(uiRouteName=route['name'], uiNextStopName=gap_name(i), uiStopId=i)
			time.sleep(sp)
			self.send_UISendRouteCurrentStop(uiRouteName=route['name'], uiCurrentStopName=gap_name(i), uiNextStopName=stop_name(i+1), uiStopId=i, isStop=False)
			time.sleep(sp)
			self.send_UISendRouteNextStop(uiRouteName=route['name'], uiNextStopName=stop_name(i+1), uiStopId=i)

		# объявим последнюю остановку (uiNextStopName - нулевая как в ui)
		time.sleep(sp)
		self.send_UISendRouteCurrentStop(uiRouteName=route['name'], uiCurrentStopName=stop_name(i+1), uiNextStopName=stop_name(0), uiStopId=i+1, isStop=True)

		self.route_set_auto_mode()	# ?


	def send_UISendRouteCurrentStop(self, **raw_data):
		msg = uiactions_pb2.UISendRouteCurrentStop()
		msg.uiRouteName = raw_data['uiRouteName']
		msg.uiCurrentStopName = raw_data['uiCurrentStopName']
		msg.uiNextStopName = raw_data['uiNextStopName']
		msg.uiStopId = raw_data['uiStopId']
		msg.isStop = raw_data['isStop']
		self.zmq_client.publish((b'BRD', b'MBRD', b'ROUT', b'VALIDATOR'), b'UISendRouteCurrentStop', msg, json.dumps(raw_data, ensure_ascii=False))

	def send_UISendRouteNextStop(self, **raw_data):
		msg = uiactions_pb2.UISendRouteNextStop()
		msg.uiRouteName = raw_data['uiRouteName']
		msg.uiNextStopName = raw_data['uiNextStopName']
		msg.uiStopId = raw_data['uiStopId']
		self.zmq_client.publish((b'BRD', b'MBRD', b'ROUT'), b'UISendRouteNextStop', msg, json.dumps(raw_data, ensure_ascii=False))

	def route_set_auto_mode(self):
		self.send_UISwitchAutoInformation(1)
	def route_set_manual_mode(self):
		self.send_UISwitchAutoInformation(2)
	def send_UISwitchAutoInformation(self, status):
		msg = uiactions_pb2.UISwitchAutoInformation()
		msg.uiStatus = status
		msg.action = uiactions_pb2.SwitchOnOffAutoInformation
		self.zmq_client.publish((b'BRD', b'MBRD', b'ROUT', b'VALIDATOR'), b'UISwitchAutoInformation', msg,
				json.dumps({'action': uiactions_pb2.SwitchOnOffAutoInformation, 'uiStatus': status}))

if __name__ == '__main__':
	try:
		App().main()
	except (SystemExit, KeyboardInterrupt):
		pass
	finally:
		G.stop.set()
