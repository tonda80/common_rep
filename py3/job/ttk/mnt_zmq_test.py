#!/usr/bin/env python3
# coding=utf8


import time
import os
import sys

sys.path.append(os.path.expanduser('~/ab/py_env'))
from zmq_client_mnt2 import *

me = b'zmqTester'

class SentData:
	pass
sentData = SentData()
sentData.cnt = sentData.max_time = sentData.max_loop_time = 0

def rec_handler(cmd = None):
	if sentData.cnt:
		if cmd.rawData != sentData.msg:
			print(f'received unexpected data! {cmd}')
			os._exit(1)
		t = time.time() - sentData.send_time
		if t > sentData.max_time:
			sentData.max_time = t
			print(f'new max time = {sentData.max_time:e}!')
		if t > sentData.max_loop_time:
			sentData.max_loop_time = t
		if sentData.cnt%40000 == 0:
			print(f'max_loop_time={sentData.max_loop_time:e} curr_time={t:e} max_time={sentData.max_time:e} data={cmd.rawData}')
			sentData.max_loop_time = 0
	sentData.send_time = time.time()
	sentData.cnt += 1
	sentData.msg = f'hi{sentData.cnt}'
	zmqClient.publish(me, 'test', rawData=sentData.msg)


G.log = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)
G.stop = threading.Event()

zmqClient = ZmqMntClient(me, rec_handler, ('tcp://127.0.0.1:10125', 'tcp://127.0.0.1:10126'), sniffer_mode=0)

rec_handler()

zmqClient.join()
