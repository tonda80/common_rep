#!/usr/bin/env python3
# coding=utf8

# локальный деплой в mnt2 (хз что тут будет на самом деле)
#
# sudo ab/temp/local_deploy.py --rm  ab/temp/temp temp/temp
# ab/local_deploy.sh

import os
import shutil
import sys

sys.path.append(os.path.expanduser('~/ab/py_env'))
from baseapp import BaseConsoleApp
import ttk_utils


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-i', '--info_mode', action='store_true', help='Нет реальных действий, только логи')
		#self.parser.add_argument('--rm', action='store_true', help='Удалить dst перед копированием')
		self.parser.add_argument('src')
		self.parser.add_argument('--dst', help='Назначение, относительно mnt2. По дефолту {src}/{src}')

	def main(self):
		self.action_counter = 0

		self.user = ttk_utils.get_mnt_username()
		mnt_directory = ttk_utils.get_local_mnt_directory()		#__deprecated_get_mnt_directory(self.user)

		src = self.args.src
		dst_rel = self.args.dst if self.args.dst else '{0}/{0}'.format(os.path.basename(src))
		dst = os.path.join(mnt_directory, dst_rel)

		if os.path.isfile(dst):
			self.do_action(lambda:os.remove(dst), f'rm {dst}')
		else:
			if input(f'\'{dst}\' does not exist. It\'s OK? [yN]') != 'y':
				return 0

		self.do_action(lambda:shutil.copy(src, dst), f'cp {src} {dst}')

		self.chown_action(dst)

	def do_action(self, action, comment):
		self.action_counter += 1
		print(f'{self.action_counter}) {comment}')
		if not self.args.info_mode:
			action()

	def chown_action(self, path):
		self.do_action(lambda: shutil.chown(path, self.user, self.user), f'chown {path} {self.user} {self.user}')


if __name__ == '__main__':
	App().main()
