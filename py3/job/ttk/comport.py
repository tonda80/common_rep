#!/usr/bin/env python3
# coding=utf8

# работаем с ком портом

import serial

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-m', '--mode', required=True, choices=['r', 'w'], help='r - reader, w - writer')
		self.parser.add_argument('-p', '--port', default='/dev/ttyS5', help='Открываемый порт, на линукс путь к /dev/ttyX')
		self.parser.add_argument('-b', '--baudrate', type=int, default=115200)
		self.parser.add_argument('-t', '--timeout', type=int, default=5, help='Таймаут при чтении')
		self.parser.add_argument('--ignore_read_error', action='store_true')

	def main(self):
		self.comport = serial.Serial(self.args.port, baudrate=self.args.baudrate, timeout=self.args.timeout)
		self.work_flag = True
		worker = ExtensionBoardInteraction(self)	# кастомизировать тут

		if self.args.mode == 'r':
			worker.read_loop()
		elif self.args.mode == 'w':
			worker.write_loop()


class ExtensionBoardInteraction:
	def __init__(self, app):
		self.app = app
		self.excluded = []	# читать из файла?

	def is_excluded(self, s):
		for e in self.excluded:
			if s.startswith(e):
				return True

	def read_loop(self):
		while self.app.work_flag:
			try:
				s = self.app.comport.readline()
			except serial.SerialException:
				if self.app.args.ignore_read_error:
					continue
				raise
			if s and not self.is_excluded(s):		# пустая строка - сработал таймаут
				print(s.rstrip(b'\r\n').decode('utf8'))

	def write_loop(self):
		while self.app.work_flag:
			cmd = input('> ').encode('utf8') + b'\r\n'
			self.app.comport.write(cmd)

if __name__ == '__main__':
	App().main()
