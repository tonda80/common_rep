#!/usr/bin/env python3

import flask						# pip install Flask
import os
import sys
import argparse
import logging
import traceback
import json
import glob
import fnmatch
import random
import io
import zipfile
import itertools


class G:
	log = None


class Route:
	def __init__(self, path):
		self.log = G.log
		with open(path) as f:
			self.json = json.load(f)

		self.extId = self.get('extId')
		if not self.extId:
			id_ = self.get('id')
			self.log.info(f'route has no extId, id {id_} will be used as extId')
			self.extId = id_

		# направление вроде есть в route_dir, но там какая-то каша, будем ориентироваться на имя файла
		fname = os.path.basename(path)
		if '_reverse' in fname:
			self.direction = 'reverse'
		else:
			self.direction = 'forward'
			if '_forward' not in fname:
				self.log.error(f'cannot determine the direction of routu by file name {fname}, consider it as forward')

		#self.key = f'{self.extId}|{self.direction}'

	def get(self, name):
		ret = self.json.get('basicInfo', self.json).get(name)
		if ret is None:
			raise RuntimeError(f'route has no attribute {name}')
		return ret


def get_value(obj, *keys):
	for k in keys:
		if k in obj:
			return obj[k]


class RouteContainer:
	def __init__(self, routes_dir):
		self.log = G.log
		self.routes = []

		route_files = glob.glob(os.path.join(routes_dir, '*.json'))
		if not route_files:
			raise RuntimeError(f'No route files in {routes_dir}')
		for rf in route_files:
			route = Route(rf)
			#if route.key in self.routes: raise RuntimeError(f'duplicate route {route.key}')
			self.routes.append(route)
		self.log.info(f'Read {len(self.routes)} routes from {routes_dir}')

	def route_generator(self, req):
		found = False
		ext_id = req['required_route_info']['route_number']
		vehicle_type = 'ALL'			# не будем пока смотреть на vehicle_type
		# vehicle_type_map = {0 = автобус1 = трамвай2 = троллейбус3 = поезд4 = метро5 = такси6 = не определено}

		for r in self.routes:
			if r.extId == ext_id:
				found = True
				yield r
		if not found:
			self.log.warning(f'No routes with ext_id={ext_id}, vehicle_type={vehicle_type}')


class MediaContainer:
	def __init__(self, media_dir):
		if not os.path.isdir(media_dir):
			raise f'[MediaContainer] dir {media_dir} is not exists'
		self.media_dir = media_dir
		self.log = G.log
		self.arch_files = []

	def condition_schedule(self, req, rep):
		id_ = req['media_info']['route_number']
		rep['id'] = id_
		rep['media_block_id'] = [id_]	# несколько?

	def media_list(self, req, rep, get_media_url):
		jpath = os.path.join(self.media_dir, req['media_list']['media_id']+'.json')
		self.log.info(f'opened {jpath}')
		with open(jpath) as jf:
			rp = json.load(jf)

		self.arch_files = []
		for e in itertools.chain(rp['NoncommercialContents'], rp['Campaigns']):
			for v in e['Videos']:
				self.arch_files.append(os.path.join(self.media_dir, 'data', v['VideoIdentifier']))
		for e in rp['EmergencyContents']:
			e['VideoUrl'] = get_media_url(e['VideoUrl'])
			for i in e['Images']:
				i['Url'] = get_media_url(i['Url'])

		rep.update(rp)

	def media_package(self, req, rep, get_media_url):
		rep['url'] = get_media_url('arch.zip')
		rep['duration'] = 999

	def get_file(self, name):
		if name == 'arch.zip':
			mem_zip = io.BytesIO()
			with zipfile.ZipFile(mem_zip, mode='a', compression=zipfile.ZIP_DEFLATED) as zf:
				#zf.writestr('zip_file1.txt', 'zip_file1 content')
				for f in self.arch_files:
					zf.write(f, os.path.basename(f))
			mem_zip.seek(0)
			return mem_zip
		else:
			#return io.BytesIO(f'fake file {name}'.encode())
			return open(os.path.join(self.media_dir, 'data', name), 'rb')



class Infomatics:
	@classmethod
	def mnt_route_to_trip(cls, route, get_audio_url):
		G_MAX = 4
		S_MAX = 5
		ret = {}
		ret['direction'] = 2 if route.direction == 'reverse' else 1
		ret['trip_token'] = f'{route.extId}-{route.direction[0]}'		# ?
		ret['shapes'] = []
		for i in range(0, min(G_MAX*2, len(route.json['geometry'])) - 1, 2):
			ret['shapes'].append({'coordinates' : [str(route.json['geometry'][i+1]), str(route.json['geometry'][i])]})
		ret['stops'] = []
		for s_in in route.json['stops'][:S_MAX]:
			s_out = {}
			s_out['long_name'] = get_value(s_in, 'name', 'long_name')
			s_out['stop_token'] = s_in['id']
			s_out['name_eng'] = s_in['eng_text']
			s_out['longitude'] = s_in['geometry'][0]
			s_out['latitude'] = s_in['geometry'][1]
			s_out['voice_url_rus'] = get_audio_url()
			#s_out['voice_url_eng'] = ''
			ret['stops'].append(s_out)

		return ret

	@classmethod
	def board_visual_settings(cls):
		return {}



class InfomaticsDummyServer:
	flask_app = flask.Flask(__name__)

	def main(self):
		self.args = self.get_args()
		G.log = self.log = self.get_logger()

		self.routes = RouteContainer(os.path.realpath(self.args.routes_dir))

		self.media = MediaContainer(os.path.realpath(self.args.media_dir))

		self.audio_files = []
		if self.args.audio_dir:
			self.audio_files = [f for f in os.listdir(self.args.audio_dir) if fnmatch.fnmatch(f, '*.mp3')]
			if not self.audio_files:
				self.log.warning(f'no audio files in {self.args.audio_dir}')

		self.flask_app.run(host='0.0.0.0', port=self.args.port, debug=True)

	def get_args(self, aa = None):
		parser = argparse.ArgumentParser()
		parser.add_argument('--verbose', '-v', action='store_true', help='Verbose output')
		parser.add_argument('--port', '-p', type=int, default=8888, help='Port to listen to')
		parser.add_argument('--routes_dir', '-r', required=True, help='Route directory')
		parser.add_argument('--audio_dir', '-a', required=True, help='Audio directory for audio download emulation')
		parser.add_argument('--media_dir', '-m', required=True, help='Media directory')
		return parser.parse_args(aa)

	def get_logger(self):
		logging.basicConfig(format='[%(asctime)s.%(msecs)d][%(levelname)s] %(message)s', datefmt='%d %b %H:%M:%S')
		log = logging.getLogger('app')
		level = logging.DEBUG if self.args.verbose else logging.INFO
		log.setLevel(level)
		logging.getLogger().setLevel(level)
		return log

	# extra
	# --

	def audio_url(self, name = None):
		PART_WITH_AUDIO = 0.2
		if not self.audio_files or random.random() > PART_WITH_AUDIO:
			return ''
		if name and name not in self.audio_files:
			self.log.warning(f'audio file {name} is absent')
		if name is None:
			name = random.choice(self.audio_files)
		self.audio_cnt += 1
		return f'http://127.0.0.1:{self.args.port}/audio/{name}'


	def get_audio_file(self, name):
		if self.args.audio_dir:
			audio_file = os.path.join(self.args.audio_dir, name)
			if os.path.isfile(audio_file):
				return self.args.audio_dir, name
		return None, None

	def media_url(self, end):
		return f'http://127.0.0.1:{self.args.port}/media/{end}'

	def get_media_file(self, name):
		return self.media.get_file(name)

	# infomatics methods
	# --

	def register_monitor(self, req, rep):
		reply_fields = ((f, 'dummy') for f in ('session_token', 'target_config_profile_bag_id', 'target_container_id', 'target_display_unit_id'))
		rep.update(reply_fields)

	def route_package(self, req, rep):
		self.routes = RouteContainer(os.path.realpath(self.args.routes_dir))	# перечитаем файлы маршрутов

		rep['board_visual_settings'] = Infomatics.board_visual_settings()
		rep['trips'] = []
		self.audio_cnt = 0
		for r in self.routes.route_generator(req):
			rep['trips'].append(Infomatics.mnt_route_to_trip(r, self.audio_url))
		self.log.info(f'route_package gave {self.audio_cnt} audio urls')

	def condition_schedule(self, req, rep):
		self.media.condition_schedule(req, rep)

	def media_list(self, req, rep):
		self.media.media_list(req, rep, self.media_url)

	def media_package(self, req, rep):
		self.media.media_package(req, rep, self.media_url)



server = InfomaticsDummyServer()

@InfomaticsDummyServer.flask_app.route('/api/v0143/<method>', methods=['POST'])
def infomatics_request_handler(method):
	if not hasattr(server, method):
		return f'method {method} not found', 404

	try:
		reply = {}
		getattr(server, method)(flask.request.json, reply)
		resp = flask.jsonify(reply)
	except Exception as e:
		resp = f'{type(e)}: {e}', 500
		traceback.print_exc()
	return resp


@InfomaticsDummyServer.flask_app.route('/audio/<name>', methods=['GET'])
def infomatics_audio_request_handler(name):
	dir_, fname = server.get_audio_file(name)
	if fname is None:
		return flask.Response(status=204)	# HTTPStatus.NO_CONTENT
	return flask.send_from_directory(dir_, fname, as_attachment=True)


@InfomaticsDummyServer.flask_app.route('/media/<name>', methods=['GET'])
def infomatics_media_request_handler(name):
	fo = server.get_media_file(name)
	if fo is None:
		return flask.Response(status=204)	# HTTPStatus.NO_CONTENT
	return flask.send_file(fo, mimetype='application/octet-stream', as_attachment=True, attachment_filename=name)

if __name__ == '__main__':
	server.main()
