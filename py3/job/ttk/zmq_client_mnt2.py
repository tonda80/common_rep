#!/usr/bin/env python3
# coding=utf8


# реализация zmq взаимодействия в ZmqMntClient

import threading
import time
import logging
import sys

import zmq				# pip install zmq

import mntproto_pb2		# pip install protobuf


class G:
	log = None
	stop = None

	@staticmethod
	def init(g):
		G.log = g.log
		G.stop = g.stop


def get_data_object(mnt_command, ProtoClass):
	ret = ProtoClass()
	if not mnt_command.data.Unpack(ret):		# ещё можно проверять так => any_object.Is(ProtoClass.DESCRIPTOR)
		raise RuntimeError(f'wrong {ProtoClass.__name__} object into Any')
	return ret


class ZmqMntClient(threading.Thread):
	def __init__(self, mnt_name, command_handler, endpoints=None, router_mode = True, sniffer_mode = 1):
		threading.Thread.__init__(self, name='ZmqMntClient')
		self.mnt_name = mnt_name
		self.command_handler = command_handler
		self.id = 0
		self.pub_lock = threading.Lock()
		self.error_handler = None

		self.router_mode = router_mode

		if endpoints:
			pub_endpoint, sub_endpoint = endpoints
		else:
			with open(os.path.join(C.curr_dir, '..', 'global.json')) as f:
				global_cfg = json.load(f)
			pub_endpoint = f"tcp://127.0.0.1:{global_cfg['zmq'].get('publish', 10125)}"
			sub_endpoint = f"tcp://127.0.0.1:{global_cfg['zmq'].get('subscribe', 10126)}"

		zmq_context = zmq.Context()
		self.pub_socket = zmq_context.socket(zmq.PUB)
		self.sub_socket = zmq_context.socket(zmq.SUB)
		if router_mode:
			self.pub_socket.connect(pub_endpoint)
			self.sub_socket.connect(sub_endpoint)
		else:
			self.pub_socket.bind(pub_endpoint)
			self.sub_socket.bind(sub_endpoint)

		self.sniffer_mode = sniffer_mode
		if sniffer_mode == 0:					# подписываемся только на свое, поведение мнт модуля
			zmq_filter = mnt_name
		elif sniffer_mode == 1:					# ловим сообщения для роутера, в роутер режиме очевидно не можем биндить порты роутера поэтому ловим сообщения для сниффера (они такие же как отправляемые от модулей роутеру, только SNIFFER в msgs[0] вместо ROUTER)
			zmq_filter = b'SNIFFER' if self.router_mode else b'ROUTER'
		elif sniffer_mode == 2:					# подписываемся на все, отдаем в нераспакованном виде
			zmq_filter = b''
		else:
			raise NotImplementedError(f'wrong sniffer_mode {sniffer_mode}')

		self.sub_socket.setsockopt(zmq.SUBSCRIBE, zmq_filter)

		time.sleep(.2)			# без паузы не работает zmq!
		self.start()
		G.log.info(f'[ZmqMntClient] started with pub_endpoint={pub_endpoint}, sub_endpoint={sub_endpoint}, filter={zmq_filter}, router_mode={router_mode}, sniffer_mode={sniffer_mode}')

	def new_mnt_command(self):
		mntCommand = mntproto_pb2.MntCommand()
		h = mntCommand.header
		h.version = mntproto_pb2.V1_1
		h.type = mntproto_pb2.MESSAGE
		h.source = self.mnt_name;
		h.createTime = int(time.time()*1000)
		h.id = self.id
		self.id += 1
		h.replyTo = 0
		h.isReplyMandatory = False
		return mntCommand

	def publish(self, dest, name, data = None, rawData = None):
		mntCommand = self.new_mnt_command()
		if isinstance(dest, (list, tuple)):
			if not self.router_mode:			# изображая роутер, пересылаем многим
				raise NotImplementedError		# не тестировалось, ибо было без надобности
				for d in dest:
					self.publish(d, name, data, rawData)
				return

			mntCommand.header.destination.extend(dest)
			dest = b'\0'.join(dest)
		else:
			mntCommand.header.destination.append(dest)

		mntCommand.header.name = name
		if data:
			mntCommand.data.Pack(data)
		if rawData:
			mntCommand.rawData = rawData

		if self.router_mode:
			msgs = (b'ROUTER', dest, mntCommand.SerializeToString())
		else:
			msgs = (dest, mntCommand.SerializeToString())

		with self.pub_lock:		# (?) zmq antipattern
			self.pub_socket.send_multipart(msgs)
		#G.log.debug(f'published {msgs}')

	def run(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while not G.stop.is_set():
			socks = dict(poller.poll(1*1000))
			if self.sub_socket in socks:
				msgs = self.sub_socket.recv_multipart()
				if self.sniffer_mode == 2:
					self.command_handler(msgs)
					continue
				#print('__deb', msgs)
				bin_command = msgs[2] if self.sniffer_mode == 1 else msgs[1]	# else это self.sniffer_mode == 0
				mntCommand = mntproto_pb2.MntCommand()
				try:
					mntCommand.ParseFromString(bin_command)
				except Exception as e:
					if self.error_handler:
						self.error_handler(msgs)
					else:
						G.log.exception(f'[ZmqClient] wrong recv messages {msgs}: {e}')
				try:
					self.command_handler(mntCommand)
				except:
					G.log.exception(f'[ZmqClient] error during processing of {mntCommand}')


if __name__ == '__main__':
	G.log = logging.getLogger()
	logging.basicConfig(level=logging.DEBUG)

	G.stop = threading.Event()

	sniffer_mode = int(sys.argv[1]) if len(sys.argv) > 1 else 2
	name = sys.argv[2].encode() if len(sys.argv) > 2 else b'TestClient'		# оно же subscribe filter если sniffer_mode=0

	zmqClient = ZmqMntClient(name, lambda c: print(c, '\n'), ('tcp://127.0.0.1:10125', 'tcp://127.0.0.1:10126'), router_mode = True, sniffer_mode=sniffer_mode)
	zmqClient.publish(b'anyone', b'hi!')

	#print('wait and exit...')
	#time.sleep(2)
	try:
		input('enter to exit\n')
	except KeyboardInterrupt:
		pass

	G.stop.set()
	zmqClient.join()
