import lsh
import os
import zipfile


BUILD_DIR = r'C:\rep-s\build-ARC-Desktop_Qt_5_10_1_MinGW_32bit-Release\release'
DISTRIB_DIR = r'C:\arc\ARC_Distrib\ARC_v2.0.13'
REP_DIR = r'C:\rep-s\autonomous_route_collector'


def copy(src, dst = None):
	if not dst:
		dst = lsh.j(DISTRIB_DIR, src)
		src = lsh.j(BUILD_DIR, src)
	print(f'[copy] {src} => {dst}')
	lsh.cp(src, dst)

def clean(*pathes):
	target = lsh.j(DISTRIB_DIR, *pathes)
	print(f'[clean] {target}')
	lsh.rm(target, '-r')

def get_version():
	with open(lsh.j(REP_DIR, 'ARC.pro')) as pro_f:
		for l in pro_f:
			if l.startswith('VERSION = '):
				return l[10:].strip()
	raise RuntumeError('cannot get version')

def make_zip(version):
	def arcname(path):
		return os.path.relpath(path, os.path.dirname(DISTRIB_DIR))
		
	zipname = lsh.j(os.path.dirname(DISTRIB_DIR), f'ARC_v{version}.zip')
	print(f'[make_zip] {zipname}')
	with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zfile:
		for root, dirs, files in os.walk(DISTRIB_DIR):
			for f in files:
				path = lsh.j(root, f)
				zfile.write(path, arcname(path))
			for d in dirs:		# для пустых директорий
				path = lsh.j(root, d)
				if not os.listdir(path):	
					zinfo = zipfile.ZipInfo(arcname(path) + '/')  
					zfile.writestr(zinfo, '')  


version = get_version()

copy('ARC.exe')
copy(lsh.j(REP_DIR, 'ARC_en.qm'), lsh.j(DISTRIB_DIR, 'ARC_en.qm'))

clean('Projects', '*')
clean('arcDB', '*')
clean('logfile.txt')

make_zip(version)

print(f'DONE "{version}"\n')
