#!/usr/bin/env python3
# coding=utf8

# утилитка для отладки wialon ISP

import socket
import threading
import time

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('ports', nargs='+', help='listened ports')
		self.parser.add_argument('-d', '--delay', type=int, default=0, help='Answer delay in seconds')

	def main(self):
		threads = []
		stop_work = threading.Event()
		for p in tuple(map(int, self.args.ports)):
			threads.append(threading.Thread(target = self.listen_thread, args = (p, stop_work)))
			threads[-1].start()

		try:
			for t in threads: t.join()
		except KeyboardInterrupt:
			self.log.info('Exiting')
			stop_work.set()
		for t in threads: t.join()

	def listen_thread(self, port, stop_work):
		tmt = 1
		with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as sock:
			sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			sock.settimeout(tmt)
			sock.bind(('127.0.0.1', port))
			sock.listen()

			while not stop_work.is_set():
				try:
					conn, addr = sock.accept()
				except socket.timeout:
					continue

				self.log.info(f'Connection from {addr}')
				conn.settimeout(1)
				while not stop_work.is_set():
					try:
						data = conn.recv(65536)
					except socket.timeout:
						continue
					except ConnectionResetError:
						break
					if not data:
						self.log.info(f'Connection closed: {port}')
						break
					try:
						self.process_data(data, conn)
					except Exception as e:
						self.log.exception(f'exception in process_data: {port}')

	def process_data(self, data, conn):
		self.log.info(f'{data} recieved on {conn.getsockname()}')	# remote - getpeername
		parts = data.decode('utf8').split('#')
		if len(parts) < 3 or parts[0] or not parts[-1].endswith('\r\n'):
			self.log.error(f'wrong received packet {data}')
			return

		# TODO? check crc
		answer = None
		if parts[1] == 'L':
			ret = '1'	# ок: 1, ошибки: 0 01 10
			answer = f'#AL#{ret}'
		elif parts[1] == 'D':
			ret = '1'	# ок: 1, ошибки: -1 0 10-16
			answer = f'#AD#{ret}'
		elif parts[1] == 'B':
			sz = parts[2].count('|')
			self.log.info(f'black box size = {sz}')
			ret = sz 	# ок: кол-во пакетов, ошибка - пустая строка
			answer = f'#AB#{ret}'
		elif parts[1] == 'P':
			answer = '#AP#'

		if answer:
			if self.args.delay:
				time.sleep(self.args.delay)

			conn.send((	answer+'\r\n').encode('utf8'))
			self.log.debug(f'\'{answer}\' is sent')
		else:
			self.log.error(f'unknown packet type {data}')



if __name__ == '__main__':
	App().main()
