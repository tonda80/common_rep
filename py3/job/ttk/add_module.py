#!/usr/bin/env python3
# coding=utf8

# автоматизируем настройку нового модуля
# sudo ./add_module.py -i tsdiag
# рядом можно положить конфиги (системы управления сервисами и модуля), если нет, то они сгенерятся рядом


import os
import shutil
import json
import subprocess
import sys

sys.path.append(os.path.expanduser('~/ab/py_env'))
from baseapp import BaseConsoleApp
import ttk_utils


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('module_name', help='Имя "регистрируемого" модуля')
		self.parser.add_argument('--mnt_module_name', help='Если не указано, то используется module_name.upper()')
		self.parser.add_argument('-i', '--info_mode', action='store_true', help='Нет реальных действий, только логи')
		self.parser.add_argument('-u', '--user', choices=ttk_utils.known_mnt_users, help='Имя пользователя `id -u -n`')
		self.parser.add_argument('-s', '--system_type', choices=['supervisor', 'systemd'], help='Система управления модулями.')


	def main(self):
		self.user = ttk_utils.get_mnt_username() if self.args.user is None else self.args.user

		system_name = ttk_utils.get_service_system() if self.args.system_type is None else self.args.system_type

		mnt_directory = ttk_utils.get_local_mnt_directory()		#__deprecated_get_mnt_directory(self.user)
		module_dir = os.path.join(mnt_directory, self.args.module_name)

		conf_name = f'{self.args.module_name}.json'

		new_service_list_element = {
			'ConfigFileName' : conf_name,			# ModuleConfName
			'MntName' : self.args.mnt_module_name if self.args.mnt_module_name else self.args.module_name.upper(),		# ModuleMntName
			'SystemdName' : f'mnt-{self.args.module_name}.service',	# ModuleSystemdName
			'SupervisorConf' : f'{self.args.module_name}.conf',		# ModuleLoaderConfName
			'IsService' : True,
		}

		if system_name == 'systemd':
			system_default_conf = systemd_conf
			sys_conf_dir = '/etc/systemd/system'
			sys_conf_name = new_service_list_element['SystemdName']
			system_commands = [
				f'systemctl enable mnt-{self.args.module_name}'.split(),		# для автостарта модуля
			]
			command_hints = ['systemctl restart mnt-configurator', f'systemctl restart mnt-{self.args.module_name}', f'systemctl disable mnt-{self.args.module_name}']
		elif system_name == 'supervisor':
			system_default_conf = supervisor_conf
			sys_conf_dir = '/etc/supervisor/conf.d/'
			sys_conf_name = new_service_list_element['SupervisorConf']
			system_commands = [
				'supervisorctl update'.split(),		# supervisorctl reread?
			]
			command_hints = ['supervisorctl restart configurator', f'supervisorctl restart {self.args.module_name}']

		if not os.path.isfile(conf_name):
			with open(conf_name, 'w') as f:
				json.dump(default_app_config, f, ensure_ascii=False, indent='\t')

		if not os.path.isfile(sys_conf_name):
			with open(sys_conf_name, 'w') as f:
				f.write(system_default_conf.format(module_name=self.args.module_name, exec_path=os.path.join(module_dir, self.args.module_name), work_dir=module_dir))

		try:
			input(f'service system = {system_name}\nmodule_dir = {module_dir}\ninfo_mode = {self.args.info_mode}\nContinue?\n')
		except KeyboardInterrupt:
			return 0
		# -----------

		self.action_counter = 0

		# 1) создаем директорию приложения
		if os.path.isdir(module_dir):
			self.log.warn(f'{module_dir} exists aready')
		else:
			self.do_action(lambda: os.mkdir(module_dir), f'создаем {module_dir}')
		self.chown_action(module_dir)

		# 2) копируем конфиг приложения и системный конфиг
		conf_dir = os.path.join(mnt_directory, 'configurator', 'conf')
		self.copy_file_actions(conf_name, conf_dir)
		self.copy_file_actions(sys_conf_name, conf_dir)
		self.copy_file_actions(sys_conf_name, sys_conf_dir, chown=False)

		# 3) добавляем элемент в servicelist.json конфигуратора
		servicelist_path = os.path.join(mnt_directory, 'configurator', 'servicelist.json')
		with open(servicelist_path) as f:
			servicelist = json.load(f)
		if any(map(lambda e: e['ConfigFileName'] == conf_name, servicelist['Services'])):
			self.log.warn(f'{servicelist_path} has {conf_name} element aready')
		else:
			def rewrite_servicelist():
				with open(servicelist_path, 'w') as f:
					json.dump(servicelist, f, ensure_ascii=False, indent='\t')
			servicelist['Services'].append(new_service_list_element)
			bkp = servicelist_path+'.bkp'
			shutil.copy(servicelist_path, bkp)
			self.do_action(rewrite_servicelist, f'добавляем {conf_name} элемент в {servicelist_path}')
			os.remove(bkp)

		# 4) команды управления сервисами
		for cmd in system_commands:
			self.run_process_action(cmd, 'запуск')

		cmd_sep = '\n  '
		command_hints.append(f'vim {os.path.join(sys_conf_dir, sys_conf_name)}')
		print(f'И осталось скопировать приложение с конфигом и выполнить если нужно:{cmd_sep}{cmd_sep.join(command_hints)}')

	def copy_file_actions(self, src_file, dest_dir, *, chown = True):
		dest_file = os.path.join(dest_dir, src_file)
		if os.path.isfile(dest_file):
			self.log.warn(f'{dest_file} exists aready')
		else:
			self.do_action(lambda: shutil.copy(src_file, dest_file), f'копируем {src_file} => {dest_file}')
		if chown:
			self.chown_action(dest_file)

	def run_process_action(self, command, comment):
		self.do_action(lambda: subprocess.run(command, check=True), f'{comment}: {" ".join(command)}')

	def chown_action(self, path):
		self.do_action(lambda: shutil.chown(path, self.user, self.user), f'chown {path} {self.user} {self.user}')

	def do_action(self, action, comment):
		self.action_counter += 1
		print(f'{self.action_counter}) {comment}')
		if not self.args.info_mode:
			action()


systemd_conf = '''[Unit]
Description=MNT {module_name}

[Service]
Type=simple
ExecStart={exec_path}
WorkingDirectory={work_dir}
Restart=on-failure
RestartSec=2s
User=root
Group=root
StandardError=null

[Install]
WantedBy=multi-user.target
'''

supervisor_conf = '''[program:{module_name}]
environment=PATH={work_dir}:%(ENV_PATH)s
directory={work_dir}
command={exec_path}
autostart=true
autorestart=true
stdout_logfile_maxbytes=10MB
stderr_logfile_maxbytes=10MB
stdout_logfile_backups=5
stderr_logfile_backups=5
stderr_logfile=/var/log/{module_name}.err.log
stdout_logfile=/var/log/{module_name}.out.log
'''

default_app_config = {"IsSubConfig": False, "LastChange": 0, "ModuleEnabled": True}

if __name__ == '__main__':
	App().main()
