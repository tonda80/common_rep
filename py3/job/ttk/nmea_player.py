#!/usr/bin/env python3
# coding=utf8

# проигрывает nmea поток для mnt модулей
# Читает построчно nmea файл(ы), фоормирует zmq сообщения с данными навигации (координата и скорость).
# https://ru.wikipedia.org/wiki/NMEA_0183

# pyinstaller nmea_player.py -F --distpath ~/temp/nmpl --specpath ~/temp/nmpl/spec --workpath ~/temp/nmpl/build --exclude-module tkinter --exclude-module numpy


import zmq				# pip install zmq
import sys
import json
import os
import time
import threading
import datetime

sys.path.append(os.path.expanduser('~/ab/py_env'))

import navigation_pb2
import routes_pb2

from baseapp import BaseConsoleApp
import zmq_client_mnt2



class AppError(RuntimeError):
	pass


class NmeaLine:
	def __init__(self, num, line):
		self.line_number = num
		self.error = None
		if not line.endswith('\r\n'):
			self.error = f'Wrong line end: {ord(line[-2])}{ord(line[-1])}'
			return
		self.line = line[:-2]

		self.parts = self.line.split(',')
		self.parts[-1], _, self.crc = self.parts[-1].partition('*')
		if not self.crc:
			self.error = 'No CRC'
			return
		# TODO? проверять crc, начало id итп
		self.id = self.parts[0][1:]

		self.is_valid_rmc = False
		self.init_rmc()

	def init_rmc(self):
		# $GNRMC,104455.244,A,5959.1142,N,03025.5665,E,0.233,164.26,260820,,,A*4A
		if self.id in ('GNRMC', 'GPRMC', 'GLRMC', 'GARMC'):
			try:
				time_str = self.parts[9] + self.parts[1]
				#time_str = time_str[:-3] + '000' + time_str[-3:]	# непонятно как оно работает, судя по доке надо добавить нули чтобы получились микросекунды '260820104455.000244', но нет
				self.timestamp = datetime.datetime.strptime(time_str, '%d%m%y%H%M%S.%f').timestamp()
				#
				lt = self.parts[3]
				self.lat = int(lt[:2]) + float(lt[2:])/60
				if self.parts[4] == 'S':
					self.lat *= -1
				elif self.parts[4] != 'N':
					raise AppError(f'cannot get rmc data from f{self.parts}: unexpected lat')
				#
				ln = self.parts[5]
				self.lon = int(ln[:3]) + float(ln[3:])/60
				if self.parts[6] == 'W':
					self.lon *= -1
				elif self.parts[6] != 'E':
					raise AppError(f'cannot get rmc data from f{self.parts}: unexpected lon')
				#
				self.valid = True if self.parts[2] == 'A' else False if self.parts[2] == 'V' else None
				if self.valid is None:
					raise AppError(f'cannot get rmc data from f{self.parts}: unexpected status')
				#
				v = self.parts[7]
				self.velocity = float(v) if v else None		# скорость в узлах
				h = self.parts[8]
				self.heading = float(h) if h else None
				#
				self.is_valid_rmc = True
			except Exception as e:
				if self.parts[2] == 'A':
					App.log.warning(f'ignored rmc line "{self.line}": {e}')
				# если статус - данные недостоверны, игнорируем

	def __str__(self):
		return f'[NmeaLine]: {self.__dict__}'


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('nmea_files', nargs='+', help='Список nmea файлов для проигрывания')
		self.parser.add_argument('--no_send', action='store_true', help='Только вывод в лог')
		self.parser.add_argument('-a', '--acceleration', type=int, default=1, help='Ускорение проигрывания')
		self.parser.add_argument('-s', '--since', type=int, default=0, help='Пропускать строки до заданной (работает только для первого nmea файла)')
		self.parser.add_argument('-m', '--manual', action='store_true', help='Ручной режим (ждет enter, чтобы сделать что-то полезное)')
		self.parser.add_argument('--ip', default='127.0.0.1', help='IP для коммуникации, например 192.168.136.114')
		self.parser.add_argument('-fd', action='store_true', help='Full destination list, eсли установлено, шлем на все модули что и модуль навигации, иначе только в модуль маршрутов')

	def app_init(self):
		self.log.debug('started nmea_player version 0.2')

		self.stop = threading.Event()
		zmq_client_mnt2.G.init(self)

	def log_format(self):
		return {
			'format': '[%(levelname)s] %(message)s',
		}

	def main(self):
		# заранее проверим что файлы на месте
		for f in self.args.nmea_files:
			if not os.path.isfile(f):
				raise AppError(f'no input file: {f}')

		pub_endpoint = f'tcp://{self.args.ip}:10125'
		sub_endpoint = f'tcp://{self.args.ip}:10126'
		self.zmq_client = zmq_client_mnt2.ZmqMntClient(b'NmeaTestApp', self.zmq_recv_handler, (pub_endpoint, sub_endpoint), router_mode = True, sniffer_mode = 0)

		self.nmea_play_loop()

	def zmq_recv_handler(self, mntCommand):
		self.log.warning('unexpectedly got {} from {}'.format(mntCommand.header.name, mntCommand.header.source))

	def nmea_play_loop(self):
		self.rmc_init_timestamp = None		# таймштамп первого rmc сообщения
		self.rmc_init_time = None			# время нашей отправки первого rmc сообщения
		self.first_file = True

		for f in self.args.nmea_files:
			with open(f, newline='\r\n') as in_file:
				self.log.info(f'Opened {f}')
				for i, line in enumerate(in_file):
					if not line.strip():
						continue	# игнорим пустые
					nmea_line = NmeaLine(i, line)
					if nmea_line.error:
						self.log.error(f'{i}-th contains wrong nmea data, {nmea_line.error}: "{line}"')
					else:
						self.process_line(nmea_line)
			self.first_file = False
		self.log.info('nmea_play_loop ended')


	def process_line(self, nmea_line):
		if self.first_file and nmea_line.line_number < self.args.since:
			return

		if nmea_line.is_valid_rmc:
			self.rmc_process_line(nmea_line)

	def rmc_process_line(self, nmea_line):
		self.rmc_wait(nmea_line)
		self.rmc_send_navigation(nmea_line)

	def user_wait(self, info):
		if self.args.manual:
			inp = input(f'{info} > ')

	def rmc_wait(self, nmea_line):
		if self.args.no_send or self.args.manual:	# в ручном режиме и лог режимах не соблюдаем таймаут!
			return

		if self.rmc_init_timestamp is None:
			self.rmc_init_timestamp = nmea_line.timestamp
			self.rmc_init_time = time.time()

		delay = (nmea_line.timestamp - self.rmc_init_timestamp)/self.args.acceleration
		step = 1/self.args.acceleration/10
		assert delay >= 0
		while delay > time.time() - self.rmc_init_time and not self.stop.is_set():
			time.sleep(step)

	def rmc_send_navigation(self, nmea_line):
		self.log.debug(f'[rmc_send_message] [{nmea_line.line_number}] t={nmea_line.parts[1][4:6]} lt={nmea_line.lat:.8f} ln={nmea_line.lon:.8f} v={nmea_line.velocity:.2f}')

		if self.args.no_send:
			return

		self.user_wait(nmea_line.line_number)

		self.publish_NavigationData(nmea_line.lat, nmea_line.lon, nmea_line.valid, nmea_line.heading, nmea_line.velocity)

		self.publish_VehicleSpeed(nmea_line.velocity)

	def publish_NavigationData(self, lat, lon, valid, heading, velocity):
		msg = navigation_pb2.NavigationData()
		msg.Lat = lat
		msg.Lon = lon
		msg.Valid = valid
		msg.Heading = heading if heading is not None else 0
		msg.Speed = velocity if velocity is not None else 0
		msg.UnixTime = int(time.time())
		if self.args.fd:
			dest = [b'NAVIGATIONSENDER', b'ROUT', b'UI', b'MBRD', b'SENDDATA', b'ADAS', b'SYNCTIME', b'WEBSENDER', b'MQTTSENDER', b'MSRV', b'ROUTESRV', b'WIPSSENDER', b'EXCELSFTP', b'GTFS']
		else:
			dest = b'ROUT'
		self.zmq_client.publish(dest, 'NavigationData', msg)

	def publish_VehicleSpeed(self, v):
		if v is None:
			self.log.debug('[publish_VehicleSpeed] ignoring empty velocity')
			return
		msg = routes_pb2.VehicleSpeed()
		msg.mph = v
		msg.kmh = v*1.852				# узлы => км/ч
		msg.mps = msg.kmh*1000/3600		# => м/c
		if self.args.fd:
			dest = [b'ROUT', b'ADAS']
		else:
			dest = b'ROUT'
		self.zmq_client.publish(dest, 'VehicleSpeed', msg)


if __name__ == '__main__':
	try:
		app = App()
		app.main()
	except (SystemExit, KeyboardInterrupt):
		pass
	except (AppError,) as e:
		app.log.critical(f'Error: {e}. exiting.')
	finally:
		try:
			app.stop.set()
		except NameError:
			pass
