#!/usr/bin/env python3

# Прототип модуля обновления для MNT2


import sys
import os
import threading
import time
import requests
from http import HTTPStatus
import re
import tarfile
import tempfile
import shutil
import json
import collections
import subprocess

import zmq					# pip install zmq
#
import configurator_pb2		# pip install protobuf
import mntproto_pb2

from baseapp import BaseConsoleApp


class C:
	module_name = 'updater'
	mnt_zmq_name = b'UPDATER'	# USBDIAG UPDATER
	curr_dir = os.path.realpath(os.path.dirname(__file__))


class G:
	log = None
	cfg = None
	stop = threading.Event()


class Config:
	def __init__(self, json_config):
		self.raw_cfg = json_config
		for p in (
				'updated_modules',				# список модулей для апдейта
				'check_updates_delay',			# пауза в секундах между проверками апдейтов
				#'check_updates_delay_err',		# TODO? пауза в секундах между проверками апдейтов при ошибке
				'download_path',				# директория для хранения загруженного
				'updates_url',	 				# url для запроса апдейтов
				'module_manager',				# fake, supervisor, systemd
				'mnt_root',						# mnt2 directory
			): setattr(self, p, json_config[p])


class App(BaseConsoleApp):
	#def add_arguments(self):

	def main(self):
		G.log = self.log
		G.log.info('ctrl-c to exit')

		# TODO? reply_handler в ZmqMntClient или как то ещё переделать
		self.reply_condvar = threading.Condition()
		self.curr_reply = None

		self.zmq_client = ZmqMntClient(C.mnt_zmq_name, self.mntcommand_handler)
		try:
			G.cfg = Config(self.get_json_config())
		except:
			G.log.exception('cannot get config, exiting')
			G.stop.set()
			return 1

		threads = []
		threads.append(self.zmq_client)
		threads.append(UpdateManager())

		try:
			while 1: input()
		except KeyboardInterrupt:
			pass
		finally:
			self.log.info('exiting')
			G.stop.set()

		for t in threads:
			t.join()

	def get_json_config(self):
		deb_config_path = os.path.join(C.curr_dir, f'{C.module_name}_debug_config.json')
		if os.path.isfile(deb_config_path):
			G.log.warn(f'using default config {deb_config_path}')
			with open(deb_config_path) as f:
				return json.load(f)

		get_cfg_request = configurator_pb2.GetConfig()
		get_cfg_request.module = C.mnt_zmq_name
		with self.reply_condvar:
			self.curr_reply = None
			self.zmq_client.publish(b'CONFIGURATOR', b'GetConfig', get_cfg_request)
		for i in range(5):	# вдруг получим не тот reply
			with self.reply_condvar:
				if self.reply_condvar.wait_for(lambda: self.curr_reply, 5):
					get_cfg_reply = configurator_pb2.Config()
					if self.curr_reply.data.Unpack(get_cfg_reply):
						return json.loads(get_cfg_reply.config)
					elif self.curr_reply.header.result != 0 and self.curr_reply.header.source == 'CONFIGURATOR':
						raise RuntimeError(f'CONFIGURATOR replied {self.curr_reply.header.result}')
					self.curr_reply = None
			raise RuntimeError('cannot get Config message')

	# это работает в треде ZmqMntClient
	def mntcommand_handler(self, mntCommand):
		h = mntCommand.header
		if h.type == mntproto_pb2.REPLY:
			with self.reply_condvar:
				self.curr_reply = mntCommand
				self.reply_condvar.notify_all()
		else:
			# TODO?
			G.log.warn(f'ignored {mntCommand.header.name} from {mntCommand.header.source}')


class UpdateManager(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self, name='UpdateManager')
		self.clear_download_path()
		self.start()

	def run(self):
		server_interaction = UpdateServerInteraction()
		update_applier = UpdateApplier()
		while not G.stop.is_set():
			stor_dir = os.path.join(G.cfg.download_path, str(int(time.time())))
			os.mkdir(stor_dir)
			updates = {}
			for name in G.cfg.updated_modules:
				try:
					curr_version = self.get_module_version(name)
				except Exception as e:
					G.log.exception(f'[UpdateManager] cannot get version of {name}')
					continue
				update_path = server_interaction.get_update(name, curr_version, stor_dir)
				if update_path:
					updates[name] = update_path

			# тут надо спрашивать разрешение на обновление
			update_applier.apply_downloaded(updates)
			# TODO check error
			# TODO? check version

			self.wait_timeout()
			self.clear_download_path()

	def get_module_version(self, module_name):
		custom_path_key = f'pathto_{module_name}'

		if custom_path_key in G.cfg.raw_cfg:
			p = G.cfg.raw_cfg[custom_path_key]
			bin_path = p if os.path.isabs(p) else os.path.join(G.cfg.mnt_root, p)
		else:
			bin_path = os.path.join(G.cfg.mnt_root, module_name, module_name)
		args = (bin_path, '-v')
		res = subprocess.run(args=args, stdout=subprocess.PIPE, check=True, timeout=3)
		version = res.stdout.split()[-1].decode('utf8')
		return version

	def wait_timeout(self):
		time_to_go_next = time.time() + G.cfg.check_updates_delay
		sleep_time = 1
		while not G.stop.is_set():
			time.sleep(sleep_time)
			if time_to_go_next < time.time():
				return

	# removes all subdirs into download_path directory
	def clear_download_path(self):
		for p in os.listdir(G.cfg.download_path):
			p = os.path.join(G.cfg.download_path, p)
			if os.path.isdir(p):
				shutil.rmtree(p)


class UpdateServerInteraction():
	def get_update(self, name, curr_version, stor_dir):
		try:
			resp = requests.get(f'{G.cfg.updates_url}/getupdate/{name}/{curr_version}')
		except Exception as e:
			G.log.error(f'[UpdateServerInteraction.get_update] requests.get exception: {e}')
			return

		if resp.status_code == HTTPStatus.NO_CONTENT:
			G.log.info(f'[UpdateServerInteraction.get_update] no update for {name} version {curr_version}')
		elif resp.status_code != HTTPStatus.OK:
			G.log.error(f'[UpdateServerInteraction.get_update] unexpected response {resp}')
		elif int(resp.headers.get('Content-Length', -1)) <= 0:
			G.log.error(f'[UpdateServerInteraction.get_update] unexpected Content-Length {resp.headers}')
		else:
			fname = os.path.join(stor_dir, self.file_name_from_response(resp, f'{name}.zip'))
			with open(fname, 'wb') as f:
				f.write(resp.content)
			G.log.info(f'[UpdateServerInteraction.get_update] got update {fname}')
			return fname

	def file_name_from_response(self, resp, default):
		try:
			return re.findall('filename=(.[^;]+)', resp.headers['Content-Disposition'])[0]
		except Exception as e:
			G.log.warn(f'[file_name_from_response] using default for {resp.headers}: {e}')
			return default


class UpdateApplier(threading.Thread):
	def __init__(self):
		self.module_manager = {'fake': BaseModuleManager, 'systemd': SystemdModuleManager, 'supervisor': SupervisorModuleManager}[G.cfg.module_manager]()

	def apply_downloaded(self, downloaded):
		if not downloaded:
			G.log.info('[UpdateApplier.apply_downloaded] no updates')

		for module_name, path in downloaded.items():
			try:
				self.update_module(module_name, path)
			except Exception as e:
				G.log.exception(f'[UpdateApplier.apply_downloaded] cannot apply update for {module_name}:{path}')

	def update_module(self, module_name, path):
		G.log.info(f'[UpdateApplier.update_module] {module_name} will be updated')

		with tarfile.open(path, mode = 'r:gz') as zf:
			update_dir = tempfile.mkdtemp(prefix=f'{module_name}_', dir=os.path.dirname(path))
			zf.extractall(path=update_dir)

		self.module_manager.stop(module_name)

		self.apply_package(update_dir)

		self.module_manager.start(module_name)
		G.log.info(f'[UpdateApplier.update_module] {module_name} is updated')

	def apply_package(self, dir_):
		with open(os.path.join(dir_, 'package.json')) as jfile:
			package_cfg = json.load(jfile)
		for fname, pars in package_cfg.items():
			src = os.path.join(dir_, fname)
			dst = pars['path']
			if not pars['path'].startswith('/'):
				dst = os.path.join(G.cfg.mnt_root, dst)

			if fname.endswith('.json'):
				self.merge_json_config(src, dst)
			else:
				shutil.copy(src, dst)
				G.log.info(f'[UpdateApplier.apply_package] updated {fname} => {dst}')

	def merge_json_config(self, src, dst):
		if not os.path.isfile(dst):
			shutil.copy(src, dst)
			G.log.info(f'[UpdateApplier.merge_json_config] created {dst}')
			return

		with open(src) as sf, open(dst) as df:
			src_cfg = json.load(sf, object_hook=collections.OrderedDict)
			dst_cfg = json.load(df, object_hook=collections.OrderedDict)

		# добавляем новые поля в dst
		# TODO?? обновлять в глубину
		updated = False
		for k, v in src_cfg.items():
			if k not in dst_cfg:
				dst_cfg[k] = v
				updated = True
		if updated:
			with open(dst, 'w') as df:
				json.dump(dst_cfg, df, indent=4)
			G.log.info(f'[UpdateApplier.merge_json_config] updated {dst}')
		else:
			G.log.info(f'[UpdateApplier.merge_json_config] no changes for {dst}')


class ZmqMntClient(threading.Thread):
	def __init__(self, mnt_name, command_handler):
		threading.Thread.__init__(self, name='ZmqMntClient')
		self.mnt_name = mnt_name
		self.command_handler = command_handler
		self.id = 0
		self.pub_lock = threading.Lock()

		with open(os.path.join(C.curr_dir, '..', 'global.json')) as f:
			global_cfg = json.load(f)
		pub_endpoint = f"tcp://127.0.0.1:{global_cfg['zmq'].get('publish', 10125)}"
		sub_endpoint = f"tcp://127.0.0.1:{global_cfg['zmq'].get('subscribe', 10126)}"

		zmq_context = zmq.Context()
		self.pub_socket = zmq_context.socket(zmq.PUB)
		self.sub_socket = zmq_context.socket(zmq.SUB)
		self.pub_socket.connect(pub_endpoint)
		self.sub_socket.connect(sub_endpoint)
		self.sub_socket.setsockopt(zmq.SUBSCRIBE, mnt_name)
		time.sleep(.2)			# без паузы не работает!
		self.start()
		G.log.info(f'[ZmqMntClient] started with pub_endpoint={pub_endpoint}, sub_endpoint={sub_endpoint}')

	def new_mnt_command(self):
		mntCommand = mntproto_pb2.MntCommand()
		h = mntCommand.header
		h.version = mntproto_pb2.V1_1
		h.type = mntproto_pb2.MESSAGE
		h.source = self.mnt_name;
		h.createTime = int(time.time()*1000)
		h.id = self.id
		self.id += 1
		h.replyTo = 0
		h.isReplyMandatory = False
		return mntCommand

	def publish(self, dest, name, data = None):
		mntCommand = self.new_mnt_command()
		mntCommand.header.destination.append(dest)
		mntCommand.header.name = name
		if data:
			mntCommand.data.Pack(data)

		msgs = (b'ROUTER', dest, mntCommand.SerializeToString())
		with self.pub_lock:		# (?) zmq antipattern
			self.pub_socket.send_multipart(msgs)
		G.log.debug(f'published {msgs}')

	def run(self):
		poller = zmq.Poller()
		poller.register(self.sub_socket, zmq.POLLIN)
		while not G.stop.is_set():
			socks = dict(poller.poll(1*1000))
			if self.sub_socket in socks:
				msgs = self.sub_socket.recv_multipart()
				mntCommand = mntproto_pb2.MntCommand()
				try:
					mntCommand.ParseFromString(msgs[1])
				except:
					G.log.exception(f'[ZmqClient] wrong recv messages {msgs}')
				try:
					self.command_handler(mntCommand)
				except:
					G.log.exception(f'[ZmqClient] error during processing of {mntCommand}')


class BaseModuleManager:
	def stop(self, module_name):
		G.log.info(f'{module_name} kinda stopped')

	def start(self, module_name):
		G.log.info(f'{module_name} kinda started')

class SystemdModuleManager(BaseModuleManager):
	def stop(self, module_name):
		cmd_s = f'systemctl stop mnt-{module_name}'
		subprocess.run(cmd_s.split(), check=True)
		G.log.info(f'"{cmd_s}" OK')

	def start(self, module_name):
		cmd_s = f'systemctl start mnt-{module_name}'
		subprocess.run(cmd_s.split(), check=True)
		G.log.info(f'"{cmd_s}" OK')



class SupervisorModuleManager(BaseModuleManager):
	def __init__(self): raise NotImplementedError


if __name__ == '__main__':
	sys.exit(App().main())
