#!/usr/bin/env python3

import flask						# pip install Flask
from flask import request

import os
from http import HTTPStatus


class Config:
	updates_dir = '/home/anton/job/stuff/updates'


def get_update_file(module_name, curr_version):
	comp_fname = f'{module_name}_{curr_version}.tar.gz'
	dname = os.path.join(Config.updates_dir, module_name)
	if os.path.isdir(dname):
		upd_list = [p for p in os.listdir(dname) if p.startswith(module_name) and p.endswith('.tar.gz')]
		upd_list.sort()
		print(f'[DEBUG] {comp_fname}: {upd_list}')
		if upd_list and upd_list[-1] > comp_fname:
			return dname, upd_list[-1]
	return None, None


app = flask.Flask(__name__)


@app.route('/getupdate/<module_name>/<curr_version>')
def get_update(module_name, curr_version):
	dir_, fname = get_update_file(module_name, curr_version)
	if fname is None:
		return flask.Response(status=HTTPStatus.NO_CONTENT)
	return flask.send_from_directory(dir_, fname, as_attachment=True)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
