#!/usr/bin/env python3
# coding=utf8

# хелпер по рутинным действиям в транстелематике


import platform
import subprocess
import importlib
import os
import glob
import tempfile
import json
import collections
import time
#import threading

import psutil		# pip install psutil
#import pyperclip	# pip install pyperclip
import paramiko		# pip install paramiko
import pty
import signal
import re
import zmq			# pip install zmq

from baseapp import BaseTkApp
import uTkinter as utk
import tkinter.messagebox
import tkinter.filedialog
from commonconfig import CommonConfig
import lsh
import ttk_utils
import mix_utils


class G:
	curr_dir = os.path.realpath(os.path.dirname(__file__))
	app = None
	restart = True


def set_clipboard(s):
	pyperclip.copy(s)
	pyperclip.paste()



class App(BaseTkApp):
	def add_arguments(self):
		def_set_path = os.path.join(G.curr_dir, '../../../../trash_rep/py/settings/ttk_helper_settings.py')
		self.parser.add_argument('-s', dest='app_settings_path', default=def_set_path, help='Путь к файлу с настройками')
		self.parser.add_argument('-p', '--port', type=int, default=21347, help='Порт для zmq соединения (0, чтобы не устанавливать)')

	def app_init(self):
		G.app = self
		G.restart = False

		app_name = 'ttkk helper'
		default_config = {
			'path1': {}
		}
		self.config = CommonConfig(app_name, **default_config)

		self.settings = self.import_settings(self.args.app_settings_path)
		if len(self.settings.all_hosts) != len(self.settings.all_hosts_dict):
			raise RuntimeError('wrong host settings (identical names?)')

	def create_gui(self):
		root = utk.uTk(u'ttm helper', 1000, 400, createStatus=True)

		f_gmArgs = {'side':utk.TOP, 'expand':utk.NO, 'fill':utk.X, 'padx':3, 'pady':10, 'ipady':10}

		SshWindowActions(utk.uLabelFrame(root, 'ssh', gmArgs=f_gmArgs), self.settings.SshWindowsSettings)
		self.deploy_actions = DeployActions(utk.uLabelFrame(root, 'deploy', gmArgs=f_gmArgs), self.settings.DeploySettings)

		f = utk.uFrame(root, gmArgs=f_gmArgs)
		b_gmArgs={'side':utk.LEFT}
		utk.uButton(f, lambda: lsh.start(os.path.realpath(__file__)), 'me', gmArgs=b_gmArgs)
		utk.uButton(f, lambda: lsh.start(self.args.app_settings_path), 'config', gmArgs=b_gmArgs)
		utk.uButton(f, self.restart, 'restart', gmArgs=b_gmArgs)

		root.set_delete_callback(self.save_config)

		if self.settings.icon_path:
			root.iconphoto(False, utk.PhotoImage(file=self.settings.icon_path))

		# тут стартуем коммуникацию по zmq
		if self.args.port:
			ep = f'tcp://127.0.0.1:{self.args.port}'
			self.zmq_comm = ZmqComm(ep, self.zmq_comm_handler, root.after)

		return root

	def cleanup(self):
		self.zmq_comm.close(self.root)

	def zmq_comm_handler(self, cmd_dict):
		cmd = cmd_dict.get('command')
		if cmd == 'deploy':
			return self.deploy_actions.exec_deploy_command(cmd_dict.get('host'), cmd_dict.get('target'))
		else:
			return False, 'unknown command'

	def report(self, msg, msg_ext=None):
		G.app.set_status_bar(msg)
		G.app.log.info(msg_ext if msg_ext else msg)

	def report_error(self, msg, msg_ext=None):
		G.app.set_status_bar('[ERROR] '+msg)
		G.app.log.error(msg_ext if msg_ext else msg)

	def report_exception(self, msg, msg_ext=None):
		G.app.set_status_bar('[ERROR] '+msg)
		G.app.log.exception(msg_ext if msg_ext else msg)

	def restart(self):
		G.restart = True
		self.root.close()



class SshWindowActions:
	def __init__(self, root, settings):
		self.windows = []
		self.settings = settings

		#f_gmArgs = {'side':utk.TOP, 'fill':utk.X, 'pady':10}
		#f1 = utk.uFrame(root, relief=utk.FLAT, gmArgs=f_gmArgs)

		gmArgs1 = {'side':utk.LEFT, 'padx':5}
		self.hosts_option_menu = utk.uOptionMenu(root, tuple(settings.all_hosts_dict.keys()), gmArgs = gmArgs1)
		G.app.init_kept_uvar(self.hosts_option_menu, 'any_ssh_host')
		#
		utk.uButton(root, self.create_window, 'Open', gmArgs = gmArgs1)

		utk.x_spacer(root, 40)
		btn_props = {'width': 8, 'gmArgs': gmArgs1}
		utk.uButton(root, self.try_reset_key, 'Reset key', **btn_props)
		utk.uButton(root, self.close_all, 'Close all', **btn_props)
		utk.x_spacer(root, 40)

		for h in reversed(settings.btn_hosts):
			utk.uButton(root, lambda h=h: self.create_window(h), h.name, font = ("System", 8), gmArgs = {'side':utk.RIGHT, 'padx':1})

	def create_window(self, host = None):
		if not host:
			host = self.settings.all_hosts_dict[self.hosts_option_menu.getVar()]
		self.windows.append(SshWindow(host))

	def close_all(self):
		while self.windows:
			self.windows.pop().close()

	def try_reset_key(self):
		host = self.settings.all_hosts_dict[self.hosts_option_menu.getVar()]

		# запускаем ssh -p PORT HOST
		ssh_pty = SshPtyInteraction(host.host, host.port)
		cmd_out = ssh_pty.read().decode()
		mo = re.search(r'(ssh-keygen -f .*?)[\r\n]', cmd_out)
		if mo:
			cmd = mo.group(1).replace('"', '').split()	# TODO корректно разделять на аргументы (с учетом кавычек) или формировать команду самому или что-то ещё
			returncode = subprocess.run(cmd).returncode
			msg = f'`{cmd}` returned {returncode}'
			G.app.log.info(msg)
			if returncode != 0:
				G.app.report_error(msg)
				return
		else:
			G.app.log.info('Maybe key reset is not needed? Out: ' + cmd_out.strip())

		ssh_pty2 = SshPtyInteraction(host.host, host.port)
		rd2 = ssh_pty2.read()
		if b'Are you sure you want to' in rd2:
			ssh_pty2.write(b'yes\n')
			G.app.report('A new key is set')
		else:
			G.app.report_error('Confirm fail: ' + rd2.decode())


class SshPtyInteraction:
	def __init__(self, host, port = 22):
		self.child_pid, self.child_fd = pty.fork()
		if not self.child_pid:
			cmd = ['/usr/bin/ssh', '-p', str(port), host]
			os.execv(cmd[0], cmd)
		time.sleep(0.2)		# в паренте ждем старта

	def read(self, bufsize=1024):
		return os.read(self.child_fd, bufsize)

	def write(self, data):
		if isinstance(data, str):
			data = data.encode()
		os.write(self.child_fd, data)

	def wait(self, terminate = True):
		if terminate:
			os.kill(self.child_pid, signal.SIGTERM)
		os.waitpid(self.child_pid, 0)

	def __del__(self):
		self.wait()


class SshWindow:
	def __init__(self, host, pos = None):
		if not try_set_creds(host):
			return

		platf = platform.platform()
		if 'Ubuntu' in platf or 'debian' in platf or 'Linux' in platf:		# TODO?
			self.process = self.run_gnome_terminal(host, pos)
			#print '__deb', self.process
		elif 'Windows' in platf:
			self.process = self.run_putty(host, pos)
		else:
			raise NotImplementedError

	def close(self):
		if self.process:
			try:
				self.process.terminate()
			except psutil.Error as e:
				msg = '[SshWindow.close] closing error {}'.format(e)
				G.app.log.warning(msg)
				G.app.set_status_bar('[WARN] {}'.format(msg))

	def run_gnome_terminal(self, host, pos):
		# gnome-terminal --geometry=10x10+200+5 -- sshpass -p {pwd} ssh -p{port} {user}@{host}	# TODO && wmctrl -r :ACTIVE: -N {host.name}
		#wmctrl -r :ACTIVE: -N {title} &&
		cmd = ['gnome-terminal', '--',
			'sshpass', '-p', host.pwd, 'ssh', '-p', f'{host.port}', f'{host.user}@{host.host}'
		]
		if pos:
			cmd.insert(1, f'--geometry={pos}')
		#print('__deb', cmd)

		# в итоге sshpass стартует gnome-terminal-server и получить pid через subprocess не получается
		# получаем его через такой workaround
		proc_name = 'sshpass'
		old_pids = set(p.pid for p in lsh.find_process(proc_name))
		G.app.log.info(f'`{" ".join(cmd[5:])}` will be run')
		subprocess.call(cmd)			# Popen() оставляет после себя defunct gnome-terminal процесс, а call почему то нет, наверное из-за отложенного удаления объекта
		new_processes = list(p for p in lsh.find_process(proc_name) if p.pid not in old_pids)
		if len(new_processes) == 1 and new_processes[0].parent().name() == 'gnome-terminal-server':		# + на всякий случай проверяем родителя
			return new_processes[0]
		else:
			msg = '[SshWindow.run_gnome_terminal] cannot find started process'
			G.app.log.warning('{} {}'.format(msg, new_processes))
			G.app.set_status_bar('[WARN] {}'.format(msg))

	def run_putty(self, host, pos):
		# putty {user}@{host} -pw {pwd}
		cmd = ['putty', f'{host.user}@{host.host}', '-pw', host.pwd]
		p = subprocess.Popen(cmd)
		return psutil.Process(p.pid)


class DeployActions:
	def __init__(self, root, settings):
		self.settings = settings
		self.senders = {}

		f_gmArgs = {'side':utk.TOP, 'fill':utk.X, 'pady':10}
		f1 = utk.uFrame(root, relief=utk.FLAT, gmArgs=f_gmArgs)
		# ---

		self.hosts_option_menu = utk.uOptionMenu(f1, tuple(self.settings.all_hosts_dict.keys()), gmArgs={'side':utk.LEFT, 'padx':10})
		G.app.init_kept_uvar(self.hosts_option_menu, 'deploy_host')

		deployed = [name[:-5] for name in dir(settings) if name.endswith('Files')]
		self.deployed_option_menu = utk.uOptionMenu(f1, deployed, gmArgs={'side':utk.LEFT, 'padx':5})
		G.app.init_kept_uvar(self.deployed_option_menu, 'deploy_target')

		utk.uButton(f1, self.deploy_to_one, 'Deploy', gmArgs = {'side':utk.LEFT, 'padx':20})

		self.merge_json_chbtn = utk.uCheckbutton(f1, 'merge\njson', gmArgs={'side':utk.LEFT, 'padx':7})
		G.app.init_kept_uvar(self.merge_json_chbtn, 'merge_json_chbtn', True)

		self.backup_files_chbtn = utk.uCheckbutton(f1, 'backup\nfiles', gmArgs={'side':utk.LEFT, 'padx':7})
		G.app.init_kept_uvar(self.backup_files_chbtn, 'backup_files_chbtn', True)

		utk.uButton(f1, self.deploy_to_obligatory, 'Obligatory deploy', width=15, gmArgs = {'side': utk.RIGHT, 'padx':20})

		f2 = utk.uFrame(root, relief=utk.FLAT, gmArgs=f_gmArgs)
		self.path = utk.uEntry(f2, gmArgs = {'side':utk.LEFT, 'fill':utk.X, 'expand':utk.YES})	# font=40
		utk.EntryPopupMenu(self.path, G.app.config["path1"])
		utk.uButton(f2, self.scp_pull, 'Pull', gmArgs = {'side':utk.LEFT, 'padx':10})
		utk.uButton(f2, self.scp_push, 'Push', gmArgs = {'side':utk.LEFT, 'padx':10})

		self.pull_dst_dir = self.settings.pull_init_dir
		self.push_src_dir = self.settings.push_init_dir

	def scp_pull(self):
		remote_src = self.path.getVar()
		if remote_src:
			self.pull_dst_dir = tkinter.filedialog.askdirectory(title='Save to', initialdir=self.pull_dst_dir)
			if self.pull_dst_dir:
				dst = mix_utils.new_destination_path(remote_src, self.pull_dst_dir)
				if not dst:
					G.app.report_error(f'pull arguments error: {remote_src} {self.pull_dst_dir}')
				else:
					host = self.settings.all_hosts_dict[self.hosts_option_menu.getVar()]
					sender = self.get_sender(host)
					try:
						sender.get(remote_src, dst)
					except Exception as e:
						G.app.report_exception(f'scp_pull fail, exception {e}')
						try:
							os.remove(dst)
						except Exception as e:
							G.app.lof.warning(f'cannot remove {dst}: {e}')
					else:
						G.app.report(f'Ok [{remote_src} <= {dst}]')

	def scp_push(self):
		remote_dst = self.path.getVar()
		src = tkinter.filedialog.askopenfilename(title = f'To load to \'{remote_dst}\'', initialdir=self.push_src_dir)
		if src:
			self.push_src_dir = os.path.dirname(src)
			host = self.settings.all_hosts_dict[self.hosts_option_menu.getVar()]
			sender = self.get_sender(host)
			if not remote_dst or remote_dst.endswith('/'):
				remote_dst += os.path.basename(src)
			try:
				sender.send(src, remote_dst)
			except Exception as e:
				G.app.report_exception(f'scp_push fail, exception {e}')
			else:
				G.app.report(f'Ok [{src} => {remote_dst}]')

	def get_sender(self, host):
		sender = self.senders.get(host.host)
		if sender is None:
			sender = self.senders.setdefault(host.host, SshInteraction(host))
		return sender

	def deploy_to(self, host, settings_object = None):
		if not try_set_creds(host):
			return

		if settings_object is None:
			settings_object = self.settings.__dict__.get(self.deployed_option_menu.getVar()+'Files')
		dirs, files = self._make_sent(settings_object, host)

		if 0:
			G.app.log.info(f'[deploy_to] {host.host} => dirs={dirs}; files={files}')
			return

		sender = self.get_sender(host)

		for d in dirs:
			sender.mkdir_if_not_exist(d)

		merge_json = self.merge_json_chbtn.getVar() and not (hasattr(settings_object, 'no_merge_json') and settings_object.no_merge_json)

		for src, dst in files.items():
			if merge_json and mix_utils.check_ext(src, '.json'):
				src = self.get_merged_json(src, dst, sender)
				if src is None:
					G.app.log.info(f'[deploy_to] no changes for {dst}, skip sending')
					continue

			if self.backup_files_chbtn.getVar():
				sender.exec_command(f'cp {dst} {dst}.bkp')

			sender.send(src, dst)

		return True
		# TODO добавить тут исполнение команд на удаленном хосте?
		#res = sender.exec_command('ls'); print('__deb', res[1].read())
		#res = sender.sudo_command('ls /root'); print('__deb', list(map(lambda s: s.read(), res[1:])))

	def exec_deploy_command(self, host_name, target):
		host = self.settings.all_hosts_dict.get(host_name)
		if not host:
			return False, 'unknown host'
		settings_object = self.settings.__dict__.get(target+'Files')
		if not settings_object:
			return False, 'unknown target'

		try:
			if not self.deploy_to(host, settings_object):
				return False, 'something went wrong'
		except Exception as e:
			return False, f'exception {e}'

		return True, ''


	def deploy_to_one(self):
		host = self.settings.all_hosts_dict[self.hosts_option_menu.getVar()]
		try:
			self.deploy_to(host)
		except Exception as e:
			G.app.report_exception(f'deploy fail, exception {e}')
		else:
			G.app.report('Ok')

	# TODO возможно деплой на несколько нужно сделать в отдельном скриптике
	def deploy_to_obligatory(self):
		deployed = self.deployed_option_menu.getVar()
		if not tkinter.messagebox.askyesno('Deploy to obligatory hosts', f'{deployed}'):
			return
		G.app.report(f'[deploy_to_obligatory] started deploy of "{deployed}"')
		for host in self.settings.obligatory_update_hosts:
			try:
				stor_level = G.app.log.level
				G.app.set_log_level(100)
				self.deploy_to(host)
				print(host.name, 'OK')
			except Exception as e:
				print(host.name, f'FAIL because of {e.__class__.__name__}: {e}')
			finally:
				G.app.log.setLevel(stor_level)
		G.app.report('[deploy_to_obligatory] finished')

	def _make_sent(self, fileSettings, host):
		if not fileSettings:
			raise RuntimeError('[DeployActions._make_sent]: no fileSettings')

		files = {}
		dirs = []

		def add_in_dirs(p):
			if p not in dirs: dirs.append(p)

		def recurs_addition(dir_, path_maker):
			for e in os.scandir(dir_):
				if e.is_file():
					files[e.path] = path_maker(e.path)
				elif e.is_dir():
					add_in_dirs(path_maker(e.path))
					recurs_addition(e.path, path_maker)
				else:
					raise RuntimeError(f'[DeployActions._make_sent]: bad src item {e.path}')

		def make_dest_path(path, host):	# fileSettings
			if path.startswith('/'):
				return path
			if path.startswith('~/'):
				return path[2:]						#f'/home/{host.user}/{path[2:]}' не работает для root
			if path.startswith('!/'):				# ! значит путь к mnt2
				#mnt_root = ttk_utils.__deprecated_get_mnt_directory(host.user)
				return f'{host.mnt_root}/{path[2:]}'
			raise AssertionError(f'[make_dest_path] wrong path {path}')

		for src, dst in fileSettings.files.items():
			dst = make_dest_path(dst, host)		# сразу заменим на реальный путь

			if not os.path.isabs(src):
				raise RuntimeError(f'[DeployActions._make_sent]: src must be absolute: {src}')	# тут можно и задавать относительно root в fileSettings, но непонятно зачем
			glob_src = glob.glob(src)
			if not glob_src:
				raise RuntimeError(f'[DeployActions._make_sent]: no src: {src}')

			if dst.endswith('/'):
				add_in_dirs(dst)

			# особый кейс, рекурсивно копируем только директорию в одноименную директорию
			# во избежание ошибок запись должна быть явной "/path/to/dir/":"*/dir/"
			src0 = glob_src[0]
			if len(glob_src)==1 and src0.endswith('/') and dst.endswith('/') and os.path.basename(src0[:-1]) == os.path.basename(dst[:-1]):
				if os.path.isdir(src0):
					recurs_addition(src0, lambda p: dst + os.path.relpath(p, src0))
				else:
					raise RuntimeError(f'[DeployActions._make_sent]: wrong src dir: {src}')
				continue

			for s in glob_src:
				if not os.path.isfile(s):
					raise RuntimeError(f'[DeployActions._make_sent]: is not a file: {s}')
				if dst.endswith('/'):
					files[s] = dst + os.path.basename(s)
				else:
					files[s] = dst

		return dirs, files

	# возвращает путь к файлу (или None если в remote_dst ничего не добавляем)
	def get_merged_json(self, src, remote_dst, sender):
		if not sender.lstat(remote_dst):
			G.app.log.info(f'[get_merged_json] No remote {remote_dst}, skip merging')
			return src

		tmp_dst = tempfile.mktemp(prefix=f'{os.path.basename(src)}_')
		sender.get(remote_dst, tmp_dst)

		with open(src) as sf, open(tmp_dst) as df:
			src_cfg = json.load(sf, object_hook=collections.OrderedDict)
			try:
				dst_cfg = json.load(df, object_hook=collections.OrderedDict)
			except json.JSONDecodeError:
				G.app.log.info(f'[get_merged_json] remote {remote_dst} is not valid, skip merging')
				return src

		# добавляем новые поля во временный файл tmp_dst
		def update_dict(src_dict, dst_dict, upd_list, key_path):
			for k, v in src_dict.items():
				#print('__deb', k, k not in dst_dict)
				if k not in dst_dict:
					dst_dict[k] = v
					upd_list.append(f'{key_path}{k}')
				elif type(v) != type(dst_dict[k]):
					G.app.log.warning(f'[get_merged_json] Types of {k} attr are different {type(v)} {type(dst_dict[k])}')
				elif isinstance(v, dict):
					update_dict(src_dict[k], dst_dict[k], upd_list, f'{key_path}{k}.')
		#
		upd_list = []
		update_dict(src_cfg, dst_cfg, upd_list, '')
		if upd_list:
			with open(tmp_dst, 'w') as df:
				json.dump(dst_cfg, df, indent=4)
			G.app.log.info(f'[get_merged_json] updated {tmp_dst}: {upd_list}')
			return tmp_dst


class SshInteraction:
	#class Error(RuntimeError): pass

	def __init__(self, conn):
		self.conn = conn	# должно иметь атрибуты для SSHClient.connect
		self.timeout = 10

		self.ssh_client = paramiko.SSHClient()
		self.ssh_client.load_system_host_keys()
		self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		self.reopen()

	def connect(self):
		G.app.log.info('SshInteraction is connecting to %s', self.conn.host)
		self.ssh_client.connect(hostname=self.conn.host, port=self.conn.port, username=self.conn.user, password=self.conn.pwd, timeout=self.timeout)

	def reopen(self):
		self.connect()
		self.sftp_client = self.ssh_client.open_sftp()
		self.sftp_client.get_channel().settimeout(self.timeout)
		# для put ещё можно добавить callback, см https://stackoverflow.com/questions/58844902/how-to-timeout-sftp-put-with-signal-module-or-other-python
		# ещё возможный костыль: запускать операции взаимодействия с удаленным хостом в отдельном процессе\потоке. Процесс можно убить а вот с зависшим потоком непонятно что делать


	# тут перехватываем ошибки которые можно решить переоткрытием соединения
	def __make(self, action, q_try = 2):
		for i in range(q_try):
			try:
				return action()
			except (paramiko.ssh_exception.SSHException, OSError) as exc:	# SshInteraction.Error
				if i == q_try-1:	# последняя попытка
					G.app.log.info('[SshInteraction.__make] reraise 1')
					raise

				#if isinstance(e, SshInteraction.Error): continue

				exc_desc = f'{exc.__class__.__name__} {exc}'
				for sign in ('closed', 'dropped', 'PipeTimeout', 'timeout'):
					if sign in exc_desc:
						G.app.log.warning(f'`{exc_desc}` Looks like channel was closed, trying to reopen ({i})')
						self.reopen()
						break
				else:
					G.app.log.info(f'[SshInteraction.__make] reraise 2: {exc_desc}')
					raise
		raise RuntimeError('SshInteraction.__make implementation error')

	def exec_command(self, command):
		G.app.log.info(f'[SshInteraction.exec_command] {self.conn.host} `{command}`')
		return self.__make(lambda: self.ssh_client.exec_command(command, timeout=self.timeout))

	def sudo_command(self, command):
		G.app.log.info(f'[SshInteraction.sudo_command] {self.conn.host} `{command}`')
		return self.__make(lambda: self.__sudo_command(command))

	def __sudo_command(self, command):
		def action():
			stdin, stdout, stderr = self.ssh_client.exec_command(f'sudo -S {command}', timeout=self.timeout)
			stdin.write(f'{self.conn.pwd}\n')
			stdin.flush()
			return stdin, stdout, stderr
		return self.__make(action)

	def get(self, src, trg):
		G.app.log.info(f'[SshInteraction.get] {self.conn.host} {trg} <= {src}')
		return self.__make(lambda: self.sftp_client.get(src, trg))

	def send(self, src, trg):
		G.app.log.info(f'[SshInteraction.send] {self.conn.host} {src} => {trg}')
		def action():
			try:
				self.sftp_client.put(src, trg)
			except (OSError, PermissionError) as e:
				if isinstance(e, PermissionError):
					G.app.log.warning(f'[SshInteraction.send] for {trg} invoked PermissionError, trying to remove target')	# чудесным образом удаляется без sudo
				elif isinstance(e, OSError):
					G.app.log.warning(f'[SshInteraction.send] for {trg} invoked "OSError", trying to remove target')
				else:
					G.app.log.info('[SshInteraction.send] reraise 1')
					raise
				self.sftp_client.remove(trg)	# self.sftp_client.remove(trg) ИЛИ self.__sudo_command(f'rm {trg}')
				# TODO?? сделать тут chown на родительскую директорию.
				# вообще проблему можно решить указывая цель не 'dir/file:'dir/file', а 'dir/file:'dir/' ибо в mkdir_if_not_exist chown есть
				# наверное, самое правильное сделать поддержку команд и явно вызывать chown
				self.sftp_client.put(src, trg)

			# очередное чудо, root-овый файл изменяется, но разрешения изменить не можем
			try:
				self.sftp_client.chmod(trg, os.stat(src).st_mode)
			except (OSError, PermissionError) as e:
				G.app.log.warning(f'[SshInteraction.send] for {trg}. Cannot execute chmod')


		return self.__make(action)

	def mkdir_if_not_exist(self, trg):
		G.app.log.info(f'[SshInteraction.mkdir_if_not_exist] {self.conn.host} {trg}')
		def action():
			stat = self.__lstat(trg)
			if not stat:
				G.app.log.info(f'mkdir {trg}')
				self.sftp_client.mkdir(trg)
			elif stat.st_uid == 0:
				G.app.log.warning(f'[SshInteraction.mkdir_if_not_exist] change permissions for {trg}')
				self.__sudo_command(f'chown {self.conn.user}:{self.conn.user} {trg}')	# self.sftp_client.chown(trg, 1000, 1000)

		return self.__make(action)

	def __lstat(self, trg):
		try:
			return self.sftp_client.lstat(trg)		# вызывается исключение PermissionError, хоть в доке и написано что то странное про возврат SFTP_PERMISSION_DENIED
		except FileNotFoundError:
			return None

	def lstat(self, trg):
		G.app.log.info(f'[SshInteraction.lstat] {self.conn.host} {trg}')
		return self.__make(lambda: self.__lstat(trg))



def try_set_creds(host):
	if host.user is not None and host.pwd is not None:
		return 1

	w = utk.uToplevel(G.app.root, True, f'Set creds for {host.name} ({host.host})', width=480, height=120)
	user_entry = pwd_entry = None
	if host.user is None:
		utk.uLabel(w, 'User', gmArgs = {'row':0, 'column':0})
		user_entry = utk.uEntry(w, gmArgs = {'row':0, 'column':1})
	if host.pwd is None:
		utk.uLabel(w, 'Password', gmArgs = {'row':1, 'column':0})
		pwd_entry = utk.uEntry(w, show='*', gmArgs = {'row':1, 'column':1})
	def on_ok_pressed():
		if user_entry:
			host.user = user_entry.getVar()
		if pwd_entry:
			host.pwd = pwd_entry.getVar()
		w.destroy()

	utk.uButton(w, on_ok_pressed, 'Ok', gmArgs = {'row':2, 'columnspan':2})
	w.wait_window()
	if not host.user or not host.pwd:
		G.app.log.warning(f'No credentials for {host.name} ({host.host})')
		return 0
	return 2


class ZmqComm:
	def __init__(self, endpoint, handler, run_in_tk_thread):
		self.handler = handler
		self.run_in_tk_thread = run_in_tk_thread
		self.tk_after_id = None

		App.log.info(f'[ZmqComm] started with endpoint={endpoint}')

		zmq_context = zmq.Context()
		self.socket = zmq_context.socket(zmq.REP)
		self.socket.bind(endpoint)

		self._recv_func()

	def close(self, w):
		self.socket.close()
		if self.tk_after_id:
			w.after_cancel(self.tk_after_id)

	def _recv_func(self):
		try:
			msg_in = self.socket.recv(zmq.NOBLOCK)
		except zmq.ZMQError:
			pass
		else:
			App.log.info(f'[ZmqComm] received {msg_in}')
			try:
				result, description = self.handler(json.loads(msg_in.decode()))
			except Exception as e:
				App.log.exception(f'[ZmqComm] error during processing of {msg_in}')
				result, description = False, 'internal receiver error'
			reply = json.dumps({'result': result, 'description': description})
			self.socket.send(reply.encode())
			App.log.info(f'[ZmqComm] replied {reply}')

		self.tk_after_id = self.run_in_tk_thread(300, self._recv_func)



if __name__ == '__main__':
	while G.restart:
		app = App()
		app.mainloop()
		app.cleanup()


# TODOs
# разобраться с ssh fingerprint (не писать руками yes)
# пароль в буфер (?)

# раскрывать ссш окна (?)
# xdotool windowraise 88104394
# xdotool search --name <заголовок окна>
# или прямо так, xdotool search --name 'anton@' windowraise
# ssh mnt@192.168.136.114 -t "cd ab; bash"
