import subprocess
import os

#DIR_FROM = '/home/berezin/strange_code/wayvnc'
DIR_FROM = '/home/berezin/dssl_files/code_backup/wayvnc/init_wayvnc-15d09b0f9f971792c1a09a5e53640951b8b74aac'
DIR_TO =   '/home/berezin/dssl_reps/rk3568/buildroot/output/rockchip_rk356x_nvr_spi_nand_dssl/build/wayvnc-15d09b0f9f971792c1a09a5e53640951b8b74aac'
LIST_ONLY = 0
CMD_DIFF_ONLY = 0

# get the need files
grep_filter = r'\.(c|h|build)$'
cmd = f'git diff --name-only {DIR_FROM} {DIR_TO} | grep -E "{grep_filter}"'
out = subprocess.getoutput(cmd)
files = [os.path.relpath(f, DIR_TO) for f in out.split('\n')]

if LIST_ONLY:
	for f in files:
		print(f)
	exit(0)

# get diff
extra_del = '\n'*2
new_files = []
for f in files:
	from_ = os.path.join(DIR_FROM, f)
	if not os.path.exists(from_):
		new_files.append(f)
		continue

	if not CMD_DIFF_ONLY:
		print(f'{f}\n{len(f)*"-"}')


	cmd = f'git diff -p {from_} {os.path.join(DIR_TO, f)}'
	if CMD_DIFF_ONLY:
		print(cmd)
		continue

	out = subprocess.getoutput(cmd)
	print (out, extra_del)

if new_files:
	print('\nNEW FILES', new_files)


