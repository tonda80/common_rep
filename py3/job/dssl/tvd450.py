#!/usr/bin/env python3
# coding=utf8

# Читаем и пишем по модбас в термомодуль ТВД-450
# sudo PYTHONPATH=$PYTHONPATH ./tvd450.py

from pymodbus.client import ModbusSerialClient		# pip install pymodbus

import cmd

from baseapp import BaseConsoleApp


# https://stackoverflow.com/a/17934865
# эти коэффициенты работают судя по трассиру
def r2y(R, G, B):
	Y =  0.257 * R + 0.504 * G + 0.098 * B +  16;
	U = -0.148 * R - 0.291 * G + 0.439 * B + 128;
	V =  0.439 * R - 0.368 * G - 0.071 * B + 128;
	return Y, U, V

def y2r(Y, U, V):
	Y -= 16;
	U -= 128;
	V -= 128;
	R = 1.164 * Y             + 1.596 * V;
	G = 1.164 * Y - 0.392 * U - 0.813 * V;
	B = 1.164 * Y + 2.017 * U;
	return R, G, B


class Parameters(list):
	def __init__(self, str_cmd):
		list.__init__(self, str_cmd.split())
		self.dict = {}
		for i in range(len(self)-1, -1, -1):
			k, s, v = self[i].partition('=')
			if s:
				self.dict[k] = v
				self.pop(i)

	def __str__(self):
		return f'[ {list(self)}, {self.dict} ]'

	def has_key(self, key):
		return key in self.dict

	def get(self, key, default = None):
		return self.dict.get(key, default) if isinstance(key, str) else self[key] if len(self) > key else default

	def geti(self, key, default = None, base = 10):
		ret = self.get(key)
		if ret and ret.startswith('0x'):
			base = 16
		return default if ret is None and default is not None else int(ret, base)


class App(BaseConsoleApp, cmd.Cmd):
	prompt = '> '

	def add_arguments(self):
		self.parser.add_argument('--device', '-d', default='/dev/ttyACM0', help='Device file')
		self.parser.add_argument('--slave', '-s', type=int, default='4', help='Sensor number')
		#self.parser.add_argument('--', '-', action='store_true', help='')

	def log_format(self):
		return {'format': '%(message)s'}

	def main(self):
		self.mb_client = ModbusSerialClient(self.args.device, baudrate=115200, parity='N', bytesize=8, stopbits=1, timeout=1)
		if self.mb_client.connect():
			self.log.info(f'Connected to {self.args.device}')
		else:
			raise RuntimeError(f'Can\'t connect to {self.args.device}')

		cmd.Cmd.__init__(self)
		self.cmdloop()	# Cmd method

	def cleanup(self):
		if self.mb_client:
			self.mb_client.close()

	def do_q(self, arg):
		'Exit'
		print('bye..')
		return True		# выход из cmdloop

	def do_r(self, arg):
		'Read. Pos: address, [count=1]. Keywords: s/lave/=self.args.slave, h/ex/=[1|x]'
		p = Parameters(arg)
		slave = p.geti('s', self.args.slave)
		hex_out = p.get('h') == '1'
		resp = self.mb_client.read_holding_registers(p.geti(0), p.geti(1, 1), slave)
		if resp.isError():
			self.log.error('Reading error')
			return

		for r in resp.registers:
			print(hex(r) if hex_out else r, end=' ')
		print()

	def do_w(self, arg):
		'Write. Pos: address, value'
		p = Parameters(arg)
		slave = p.geti('s', self.args.slave)
		resp = self.mb_client.write_register(p.geti(0), p.geti(1), slave)
		if resp.isError():
			self.log.error('Writing error')
			return


if __name__ == '__main__':
	App().start()
