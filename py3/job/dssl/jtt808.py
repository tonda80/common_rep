#!/usr/bin/env python3
# coding=utf8

import argparse
import binascii
import cmd
#import logging
import socket
import socketserver
import sys
import threading
import queue

sys.path.append('/home/berezin/trash_rep/py/dssl/')
import jt_message
jt_message.set_jtmsg_config('2013', '1234567890ab')


FLAG_BIT = 0x7e
B0 = 13     # first byte of body in whole package (if no subpackage!)


def data_view(data):
    return ' '.join(f'{i:02x}' for i in data)

def PW(data, start):
    # package word
    return data[1+start] << 8 | data[1+start+1]     # 1 - 7e shift

def is_subp(data):
    return data[3] & 0x20

def BA(data):
    # body all
    b0 = B0 + 4 if is_subp(data) else B0
    return data[b0:-2]

def BB(data, start):
    # body byte
    b0 = B0 + 4 if is_subp(data) else B0
    return data[b0+start]

def BW(data, start):
    # body word
    b0 = B0 + 4 if is_subp(data) else B0
    return data[b0+start] << 8 | data[b0+start+1]

def BD(data, start):
    # body dword
    b0 = B0 + 4 if is_subp(data) else B0
    return data[b0+start] << 24 | data[b0+start+1] << 16 | data[b0+start+2] << 8 | data[b0+start+3]

def BH(data, start, size):
    # body hex string
    b0 = B0 + 4 if is_subp(data) else B0
    return binascii.hexlify(data[b0+start:b0+start+size])


class JttServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    allow_reuse_address = True
    # daemon_threads = True     # с socketserver.ThreadingMixIn, когда False, тред обработчика будет join-ен в __exit__ сервера


class JttHandler(socketserver.BaseRequestHandler):
    _work = True

    @classmethod
    def stop(cls):
        cls._work = False

    _commands = queue.Queue()

    def _handle_command(self):
        try:
            cmd = self._commands.get_nowait()
        except queue.Empty:
            return
        #print(f'__deb COMMAND {cmd}')
        self.__send_message(cmd)

    @classmethod
    def add_command(cls, cmd):
        cls._commands.put_nowait(cmd)


    def handle(self):
        print(f'New handler for {self.client_address}')
        self.request.settimeout(1)
        buf = b''   # begin of message
        while self._work:
            try:
                data = self.request.recv(1024)
            except socket.timeout:
                self._handle_command()
                continue
            if not data:
                break   # client disconnected

            # тут буфер должен быть пустой или в нем лежит начало пакета без конца
            if buf and (buf[0] != FLAG_BIT or buf[-1] == FLAG_BIT):
                print(f'bufer error {buf}')
                raise RuntimeError('Discovered buffer state error')

            if   data[0] == FLAG_BIT and data[-1] == FLAG_BIT:    # целый пакет\ы
                self._split_and_unescape(data)
            elif data[0] == FLAG_BIT and data[-1] != FLAG_BIT:    # начало пакета без конца
                print(f'__deb MERGE0! buf={data_view(buf)} \n new={data_view(data)}')
                if buf:
                    print('Garbage in bufer, drop it')
                buf = data
            else:                                                 # данные без начала
                print(f'__deb MERGE1! buf={data_view(buf)} \n new={data_view(data)}')
                if not buf:
                    print(f'Garbage in new data, ignore it')
                elif data[-1] == FLAG_BIT:                        #   ^ но с концом
                    self._split_and_unescape(buf + data)
                    buf = b''
                else:                                             #   ^ и без конца
                    buf += data
                    if len(buf) > 64000:
                        print(f'bufer is too big {buf}')
                        raise RuntimeError('Discovered buffer size error')

    def _split_and_unescape(self, bdata):
        assert bdata[0] == bdata[-1] == FLAG_BIT, f'Unexpected data={data_view(bdata)}'
        data = bytearray(bdata)
        init_end_idx = end_idx = len(data)
        for i in range(end_idx - 2, 0, -1):
            if data[i] == 0x7d:
                if data[i+1] == 0x01:
                    data.pop(i+1)
                elif data[i+1] == 0x02:
                    data.pop(i+1)
                    data[i] = FLAG_BIT
                else:
                    print(f'> UNESCAPE ERROR 7d ({i}): {data_view(data)}')
                    return
            elif data[i] == FLAG_BIT and i != end_idx - 1:
                if data[i-1] != FLAG_BIT:
                    print(f'> UNESCAPE ERROR 7e ({i}): {data_view(data)}')
                    return
                print(f'__deb SPLIT! all data={data_view(data)}')
                self._process_message(data[i:end_idx])
                end_idx = i

        if init_end_idx == end_idx:     # чтоб не копировать лишний раз
            self._process_message(data)
        else:
            self._process_message(data[:end_idx])

    def _process_message(self, data):
        assert data[0] == data[-1] == FLAG_BIT, f'Unexpected data={data_view(data)}'
        msg_id = PW(data, 0)
        serial_no = PW(data, 10)
        if msg_id == 0x100:
            print(f'> registration: {data_view(data)}')
            jt_message.set_jtmsg_config('2013', binascii.hexlify(data[5:11]).decode())       #
            resp = jt_message.JTMessage()
            resp.set_message_id(0x8100)
            resp.set_serial_no(serial_no)
            b_serial_no = binascii.hexlify(data[11:13])
            resp.set_body((b_serial_no + b'00'+ binascii.hexlify(b'hello')).decode())
            self.__send_message(resp)
        elif msg_id == 0x0102:
            print(f'> auth: {data_view(data)}')
            resp = jt_message.T8001s()
            resp.set_serial_no(serial_no)
            resp.set_params(serial_no, msg_id, 0)
            self.__send_message(resp)
        elif msg_id == 0x0002:
            print(f'> heartbit: {data_view(data)}')
            resp = jt_message.T8001s()
            resp.set_serial_no(serial_no)
            resp.set_params(serial_no, msg_id, 0)
            self.__send_message(resp)
        elif msg_id == 0x0200:          # 8.18 page 29
            if args.quiet_mode:
                return
            body_len = PW(data, 2) & 0x3ff
            print(f'> Location information report blen={body_len} time={BH(data, 22, 6)}: {data_view(data)}')
            i = 28
            while i < body_len:
                add_id = BB(data, i)
                add_len = BB(data, i+1)
                size = 2 + add_len
                print(f' \t add item id={add_id:#04x} len={add_len}: {data_view(data[B0+i:B0+i+size])}')
                i += size
        elif msg_id == 0x0900:          # 8.62 page 52 + adas 14
            print(f'> Data uplink pass-through: message type {BB(data, 0):x}: {data_view(data)}')
        elif msg_id == 0x0805:
            print(f'> Camera response res={BB(data, 2):x}: {data_view(data)}')
        elif msg_id == 0x0802:
            print(f'> Response of store multimedia data body={data_view(BA(data))}: {data_view(data)}')
        elif msg_id == 0x1:
            print(f'> Terminal general response result={BB(data, 4)}: {data_view(data)}')
        elif msg_id == 0x0104:          # 8.12 page 25
            body_len = PW(data, 2) & 0x3ff
            subp_total = subp = 0
            if is_subp(data):
                subp_total = PW(data, 12)
                subp = PW(data, 14)
            print(f'> Check terminal parameter response, {subp}/{subp_total} blen={body_len}: {data_view(data)}')
            i = 3 if subp < 2 else 0
            while i < body_len:
                par_id = BD(data, i)
                par_len = BB(data, i+4)
                print(f' \t par id={par_id:#6x} len={par_len:4}: {BH(data, i+5, par_len).decode()}')
                i += 5 + par_len
            if i != body_len:
                print(f'Looks like we have parse error here!! i={i} body_len={body_len}')
        else:
            print(f'> UNKNOWN MESSAGE ID={hex(msg_id)}: {data_view(data)}')


    def __send_message(self, msg_obj):
        resp_data = msg_obj.message()[0][1]    # escaping is here
        self.request.sendall(resp_data)
        print(f'< {data_view(resp_data)} sent')


class Control(cmd.Cmd):
    intro = '\n`q` to stop or `?` to list commands\n'
    prompt = '# '

    def do_q(self, arg):
        'Выход'
        return True     # выход из cmdloop

    def do_c(self, arg):
        'Camera immediately taken command'
        args = CmdArgs(arg)
        msg = jt_message.T8801s()
        msg.set_params(
            channel=    args.geti('c', 1),
            resolution= args.geti('r', 2),
            quality=    args.geti('q', 1),
            brightness= args.geti('b', 127),
        )
        JttHandler.add_command(msg)

    def do_rsm(self, arg):
        'Retrieve of store multimedia data'
        args = CmdArgs(arg)
        msg = jt_message.T8802s()
        #msg.set_params()
        JttHandler.add_command(msg)

    def do_f(self, arg):
        'Quick custom test, send message from file'
        args = CmdArgs(arg)
        try:
            JttHandler.add_command(MsgFromFile())
        except RuntimeError:
            pass


class MsgFromFile(jt_message.JTMessage):
    def _read_file(self):
        fname = '/home/berezin/trash_rep/py/dssl/jt_message_debug.txt'
        with open(fname) as file_:
            for l in file_:
                l = l.strip()
                if l and not l.startswith('#'):
                    try:
                        msg_id, _, body = l.partition(':')
                        return int(msg_id, 16), body.replace(' ', '')
                    except Exception as e:
                        print(f'Error of TDebug messge format! line={l} exception={e}')
                        break
        raise RuntimeError('Malformed message')

    def __init__(self):
        super().__init__()

        msg_id, body = self._read_file()

        self.__message_id = msg_id
        self.set_message_id(msg_id)

        self.set_body(body)


class CmdArgs(list):
    def __init__(self, str_cmd):
        list.__init__(self, str_cmd.split())
        self.dict = {}
        for arg in self:
            k, s, v = arg.partition('=')
            if s:
                self.dict[k] = v

    def get(self, key, default = None):
        if isinstance(key, str):
            return self.dict.get(key, default)
        return self[key] if len(self) > key else default

    def geti(self, key, default = None, base = 10):
        ret = self.get(key)
        if ret is not None:
            try:
                return int(ret, base)
            except ValueError:
                pass
        return default


def init_args(arg_src = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('-q', '--quiet_mode', action='store_true', help='Not to output periodical reports')

    return parser.parse_args(arg_src)

args = None     # init_args('')


if __name__ == '__main__':
    args = init_args()

    with JttServer(('0.0.0.0', 6777), JttHandler) as server:
        try:
            server.handle_request()     #serve_forever()
            Control().cmdloop()
        except KeyboardInterrupt:
            pass
        finally:
            print('exiting')
            JttHandler.stop()
