#!/usr/bin/env python3

# Simulation tool for tvd-450 modbus device
#
# external tool for creation virtual serial ports pair
# socat -d -d pty,link=/home/berezin/temp/ptys/tty0,rawer pty,link=/home/berezin/temp/ptys/tty1,rawer
#
# ./tvd450_sim.py -s 4,5 -d /home/berezin/temp/ptys/tty0 -nl


from __future__ import annotations

import argparse
import asyncio
import dataclasses
import random
import re

from pymodbus.datastore import (
	#ModbusSequentialDataBlock,
	ModbusServerContext,
	ModbusSlaveContext,
	ModbusSparseDataBlock,
)
#from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server import StartAsyncSerialServer


SIZE = 32
NUM_POINTS = SIZE*SIZE


def get_args():
	parser = argparse.ArgumentParser()
	def str_to_ints(s):
		return tuple(map(int, s.split(',')))
	parser.add_argument('-s', '--slave_numbers', required=True, type=str_to_ints,
												 help='Emulated slave numbers (comma separated list)')
	parser.add_argument('-d', '--dev_path', required=True, help='Path to a virtual serial device')
	parser.add_argument('-c', '--scenario', help='Path to a scenario file')
	parser.add_argument('-nl', '--need_label', action='store_true', help='Add label on random frame')
	return parser.parse_args()

g_args = get_args()


def create_slaves():
	def tvd450_datablock():
		return ModbusSparseDataBlock({
		0x000: [3000]*NUM_POINTS,		# temperatures
		0x40a: 0x44,					# tvd ID
	})

	def emptydb():
		return ModbusSparseDataBlock(values={}, mutable=False)

	return {i: ModbusSlaveContext(hr=tvd450_datablock(), di=emptydb(), co=emptydb(), ir=emptydb(), zero_mode=True)
			for i in g_args.slave_numbers}


async def run_server(slaves):
	server_context = ModbusServerContext(slaves=slaves, single=False)

	await StartAsyncSerialServer(
		context = server_context,
		port = g_args.dev_path,
		baudrate = 115200,
		stopbits = 1,
		bytesize = 8,
		parity = "N",
		identity = None,
		timeout = 1,
		framer = 'rtu',
		ignore_missing_slaves = True,  # ignore request to a missing slave
		#handle_local_echo = False,  # Handle local echo of the USB-to-RS485 adaptor
		#custom_functions = [],  # allow custom handling
		#broadcast_enable = False,  # treat slave_id 0 as broadcast address,
	)

async def update_temperatures(slaves, scenario):
	while 1:
		for num, slave in slaves.items():
			if scenario:
				new_data = scenario.get_data()
			else:
				new_data = random_temperature_data(num)
			#print('__deb', len(new_data), new_data)
			slave.setValues(3, 0, new_data)		# 3 - modbus function (Read Holding Registers)
		await asyncio.sleep(2)
		if scenario:
			scenario.next_step()


def random_temperature_data(num):
	new_data = [random.randint(2800, 3050) for i in range(NUM_POINTS)]
	if g_args.need_label:
		add_label(num, new_data, 2700)
	return new_data

def add_label(num, data, label_value):
	# add label on temperature "frame"
	assert 0 < num <= 256
	digs = [int(x) for x in str(num)]
	cl = 32		# columns
	font = {
		0: (0, 1, 2, 1*cl,         1*cl+2, 2*cl, 2*cl+2,         3*cl, 3*cl+2, 4*cl, 4*cl+1, 4*cl+2),
		1: (      2,       1*cl+1, 1*cl+2,       2*cl+2,               3*cl+2,               4*cl+2),
		2: (0, 1, 2,               1*cl+2, 2*cl, 2*cl+1, 2*cl+2, 3*cl,         4*cl, 4*cl+1, 4*cl+2),
		3: (0, 1, 2,               1*cl+2, 2*cl, 2*cl+1, 2*cl+2, 3*cl+2,       4*cl, 4*cl+1, 4*cl+2),
		4: (0, 2,    1*cl,         1*cl+2, 2*cl, 2*cl+1, 2*cl+2, 3*cl+2,                     4*cl+2),
		5: (0, 1, 2, 1*cl,                 2*cl, 2*cl+1, 2*cl+2, 3*cl+2,       4*cl, 4*cl+1, 4*cl+2),
		6: (0, 1, 2, 1*cl,                 2*cl, 2*cl+1, 2*cl+2, 3*cl, 3*cl+2, 4*cl, 4*cl+1, 4*cl+2),
		7: (0, 1, 2,               1*cl+2,               2*cl+2,       3*cl+2,               4*cl+2),
		8: (0, 1, 2, 1*cl,         1*cl+2, 2*cl, 2*cl+1, 2*cl+2, 3*cl, 3*cl+2, 4*cl, 4*cl+1, 4*cl+2),
		9: (0, 1, 2, 1*cl,         1*cl+2, 2*cl, 2*cl+1, 2*cl+2,       3*cl+2, 4*cl, 4*cl+1, 4*cl+2),
	}
	pos = 33
	for d in digs:
		for i in map(lambda x, pos=pos: pos+x, font[d]):
			data[i] = label_value
		pos += 4


class Scenario:
	@staticmethod
	def create(path):
		if path:
			with open(path, encoding='utf-8') as file_:
				return Scenario(file_)
		return None

	class Error(RuntimeError):
		pass

	@dataclasses.dataclass
	class Pixel:
		pos: int
		temp_min: int
		temp_max: int|None = None

	@dataclasses.dataclass
	class Step:
		rep: int = 1
		bg_temp_min: int = 22
		bg_temp_max: int|None = None
		pixels: list[Scenario.Pixel] = dataclasses.field(default_factory=list)

		def init(self, line, line_num):
			def int_or_none(x):
				return int(x) if x else None

			for par in line.split():
				key, _, value = par.partition('=')
				if not value:
					raise Scenario.Error(f'Malformed parameter \'{par}\' in line {line_num}')
				unkn_par = False
				try:
					if key == 'rep':
						self.rep = int(value)
					elif key == 'bg':
						self.bg_temp_min, self.bg_temp_max = map(
								int_or_none, re.match(r'(\d+)-?(\d*)$', value).groups())
						if self.bg_temp_max is not None and self.bg_temp_max < self.bg_temp_min:
							raise ValueError('maximum less than minimum')
					elif key == 'px':
						row, col, temp_min, temp_max = map(
								int_or_none, re.match(r'(\d+),(\d+);(\d+)-?(\d*)', value).groups())
						if row < 0 or col < 0 or row >= SIZE or col >= SIZE:
							raise ValueError('Pixel coordinate is out range')
						if temp_max is not None and temp_max < temp_min:
							raise ValueError('maximum less than minimum')
						self.pixels.append(Scenario.Pixel(row*32+col, temp_min, temp_max))
					else:
						unkn_par = True
				except Exception as e:
					raise Scenario.Error(f'Wroпg parameter \'{par}\' in line {line_num}: {e}')
				if unkn_par:
					raise Scenario.Error(
							f'Unknown parameter \'{par}\' in line {line_num}. Possible values: rep, bg, px')

	def __init__(self, file_):
		self.steps = []
		for num, line in enumerate(file_):
			line = line.strip()
			if not line or line.startswith('#'):
				continue
			self.steps.append(Scenario.Step())
			self.steps[-1].init(line, num)
			#print('Added step', self.steps[-1])
		if not self.steps:
			raise Scenario.Error("No steps")
		print(f'Scenario has {len(self.steps)} steps')

		self.cur_idx = 0
		self.cur_step = dataclasses.replace(self.steps[self.cur_idx])

	def next_step(self):
		#print(f'__deb next_step step={self.cur_idx} substep={self.cur_step.rep}')
		self.cur_step.rep -= 1
		while 1:
			if self.cur_step.rep <= 0:
				self.cur_idx += 1
				if self.cur_idx == len(self.steps):
					self.cur_idx = 0
				self.cur_step = dataclasses.replace(self.steps[self.cur_idx])
			else:
				break


	def get_data(self):
		#print(f'__deb get_data step={self.cur_idx} substep={self.cur_step.rep}')

		def get_temp(min_, max_):
			if max_ is None:
				t = min_
			else:
				t = random.randint(min_, max_)
			return t*10 + 2731

		new_data = [get_temp(self.cur_step.bg_temp_min, self.cur_step.bg_temp_max) for i in range(NUM_POINTS)]
		for px in self.cur_step.pixels:
			new_data[px.pos] = get_temp(px.temp_min, px.temp_max)
		return new_data



async def main():
	slaves = create_slaves()
	try:
		scenario = Scenario.create(g_args.scenario)
	except Scenario.Error as e:
		print(f'Scenario error: {e}')
		return

	await asyncio.gather(
		run_server(slaves),
		update_temperatures(slaves, scenario)
	)


try:
	asyncio.run(main())
except KeyboardInterrupt:
	pass
