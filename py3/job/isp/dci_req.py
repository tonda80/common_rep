#!/usr/bin/env python3

# чуть более удобная замена вот такой команды
# curl -k -# -X POST -H "Host: instance-1" -H "Cookie: ses6=1-beb4045d-4e35-4618-abd1-3caca9a5a03d" -H "x-xsrf-token: 1-beb4045d-4e35-4618-abd1-3caca9a5a03d" -H "isp-box-instance: true" -o- "https://172.31.226.20/report/v4/report/server" -d '{"ids":[13], "report_type": "server_hardware_inventory"}
# в итоге есть неприятное ощущение, что стоит привыкнуть использовать баш. но таки будем верить, что исследовательская работа не пропадет(
# попробуем сделать из этого ui утилитку


import json
import sys
import requests
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
import tkinter.filedialog
import pathlib

from baseapp import BaseTkApp
import uTkinter as u
from commonconfig import CommonConfig


class App(BaseTkApp):
	def add_arguments(self):
		self.parser.add_argument('--debug', '-db', action='store_true', help='Sends nothing, just debug output')

	def log_format(self):
		return {'format': '[%(levelname)s] %(message)s'}

	def app_init(self):
		default_config = {
			'base_urls': {},
			'service_urls': {},
			'urls': {},
			'last_path': '',
			'last_method': 'GET',
		}
		self.config = CommonConfig('dci_req_20220720', **default_config)

	def create_gui(self):
		root = u.uTk('DCI req', 800, 520, createStatus=True)

		f = u.uFrame(root, relief = u.FLAT, gmArgs = {'side':u.TOP, 'fill':u.X, 'pady':10})
		layout_w = 15
		grid_layout = u.GridLayout(f, 0, layout_w)
		l_args = {'sticky': u.W, 'padx': 10}
		e_args = {'sticky': u.NSEW, 'columnspan': layout_w-1}

		u.uLabel(f, 'base url', gmArgs = l_args)
		self.base_url = u.uEntry(f, gmArgs = e_args)
		u.EntryPopupMenu(self.base_url, self.config['base_urls'])
		self.init_kept_uvar(self.base_url, 'base_url')

		u.uLabel(f, 'service url', gmArgs = l_args)
		self.service_url = u.uEntry(f, gmArgs = e_args)
		u.EntryPopupMenu(self.service_url, self.config['service_urls'])
		self.init_kept_uvar(self.service_url, 'service_url')

		u.uLabel(f, 'url', gmArgs = l_args)
		self.url = u.uEntry(f, gmArgs = e_args)
		u.EntryPopupMenu(self.url, self.config['urls'], limit = 20)
		self.init_kept_uvar(self.url, 'url')

		u.uLabel(f, 'ses6', gmArgs = l_args)
		self.ses6 = u.uEntry(f, gmArgs = e_args)
		self.init_kept_uvar(self.ses6, 'ses6')

		u.uLabel(f, 'data', gmArgs = l_args)
		self.data = u.uText(f, gmArgs = {'sticky': u.NSEW, 'columnspan': layout_w-1, 'rowspan': 4})
		self.init_kept_uvar(self.data, 'data')

		u.uLabel(f, 'is_data', gmArgs = l_args)
		self.is_data = u.uCheckbutton(f, gmArgs = e_args)

		u.uLabel(f, 'is_json', gmArgs = l_args)
		self.is_json = u.uCheckbutton(f, defaultValue = True, gmArgs = e_args)

		u.uLabel(f, 'send', gmArgs = l_args)
		btn_frame = u.uFrame(f, relief=u.FLAT, gmArgs = e_args)
		def send(btn):
			method = btn.cget('text')
			self.config.set('last_method', method)
			self.mark_send_button()
			self.send_request()
		self.send_buttons = []
		self.send_buttons.append(u.uButton(btn_frame, u.uCallback(send), 'GET', gmArgs = {'sticky': u.NSEW, 'row':0, 'column':0}))
		self.send_buttons.append(u.uButton(btn_frame, u.uCallback(send), 'POST', gmArgs = {'sticky': u.NSEW, 'row':0, 'column':1}))
		self.send_buttons.append(u.uButton(btn_frame, u.uCallback(send), 'DELETE', gmArgs = {'sticky': u.NSEW, 'row':0, 'column':2}))
		u.make_grid_stretchy(btn_frame)
		self.mark_send_button()

		#u.uLabel(f, '', gmArgs = l_args)
		#u.uButton(f, self.send_request, 'Send', gmArgs = e_args)

		grid_layout.make_stretchy()

		u.uSeparator(root)

		self.resp = u.uText(root, scrollmode='y', gmArgs = {'expand': u.YES, 'fill': u.BOTH})

		root.bind('<Return>', self.send_request)

		return root

	def mark_send_button(self):
		bas_btn = self.send_buttons[0]
		def_bg = bas_btn.config('bg')[3]
		def_ag = bas_btn.config('activebackground')[3]
		for btn in self.send_buttons:
			if btn.cget('text') == self.config.get('last_method'):
				btn.config(bg='#d93030')
				btn.config(activebackground='#ec3030')
			else:
				btn.config(bg=def_bg)
				btn.config(activebackground=def_ag)

	def get_headers(self):
		ses6 = self.ses6.getVar()
		return {
			'Host': 'instance-1',
			'Cookie': f'ses6={ses6}',
			'X-Xsrf-Token': ses6,
			'Isp-Box-Instance': 'true',
		}

	def _get_url_path(self, entry, end_part = False):
		url = entry.getVar()

		# normalize
		if not end_part and not url.endswith('/'):
			url += '/'
			entry.setVar(url)
		while url.startswith('/'):
			url = url[1:]
			entry.setVar(url)
		return url


	def send_request(self, ev=None):
		method = self.config.get('last_method')  # self.method.getVar()

		url = self._get_url_path(self.base_url) + self._get_url_path(self.service_url) + self._get_url_path(self.url, end_part = True)

		headers = self.get_headers()

		data = self.data.get_all()
		if not data.strip() and method == 'GET':
			data = None
		else:
			data = data.encode('utf8')	# на русские буквы в data ругается

		if self.args.debug:
			print(method, url, headers, data)
			return

		self.log.debug('%s %s headers=%s data=%s', method, url, headers, data)
		self.resp.clear_all()

		resp = requests.request(method, url, headers=headers, data=data, verify=False)
		if not resp.ok:
			self.set_status_bar('Ok')
			self.log.critical(f'Bad responce: {resp} {resp.text}')
			self.set_status_bar('Bad responce')

		self.output_responce(resp, self.is_data.get_var(), self.is_json.get_var())

	def output_responce(self, resp, is_data, is_json):
		print(resp.text)

		if is_data:
			path = pathlib.Path(self.config.get('last_path'))
			f = tkinter.filedialog.asksaveasfile(mode='wb', initialfile=path.name, initialdir=str(path.parent))
			if f:
				f.write(resp.content)
				self.config.set('last_path', f.name)
			return

		if is_json:
			obj = json.loads(resp.text)
			resp_out = json.dumps(obj, indent=2)
		else:
			resp_out = resp.text
		self.resp.insert(u.END, resp_out)


if __name__ == '__main__':
	App().start()
