#!/usr/bin/env python3

# тест websocket соединения через proxy и заодно пример асинхроной работы с websocket
# https://websockets.readthedocs.io/en/stable/
# js test: ws = new WebSocket("ws://localhost:9080/"); ws.send("hello from browser")


import websockets	# pip install websockets
import asyncio
import argparse
import logging
import json
from http import HTTPStatus


def init_logger(level, to_file):
	logging.basicConfig(
		filename = to_file,
		filemode = 'w',
		format = '[%(asctime)s.%(msecs)03d][%(levelname)s] %(message)s',
		datefmt = '%d %b %H:%M:%S')

	log = logging.getLogger('App')

	log.setLevel(level)

	root_level = logging.WARNING
	logging.getLogger().setLevel(root_level)

	return log


def init_args(arg_src = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('--host', default='127.0.0.1', help='Server/client host')
	parser.add_argument('-sp', '--server_port', type=int, help='Server port')
	parser.add_argument('-cp', '--client_port', type=int, help='Client port')
	parser.add_argument('-cu', '--client_uri', default='', help='Client uri path')
	parser.add_argument('-sr', '--send_request', action='store_true', help='Request for package send')
	parser.add_argument('-nss', '--no_server_stream', action='store_true', help='Don\'t run send data stream from server')
	parser.add_argument('--ses6', help='ses6 auth token')
	parser.add_argument('-d', '--delay', type=int, default=2, help='Send delay')
	parser.add_argument('-v', '--verbose', action='store_true', help='Verbose log')
	parser.add_argument('-of', '--out_to_file', help='Redirect all script output into file')

	args = parser.parse_args(arg_src)

	if not args.client_port and not args.server_port:
		raise RuntimeError('Server or client (or both) must be provided')

	return args


class ExtWebSocketServerProtocol(websockets.WebSocketServerProtocol):
	async def process_request(self, path, request_headers):
		#print('__deb process_request', path)
		if path == '/public/status':
			return (HTTPStatus.OK, {}, b'ok')


class App:
	def __init__(self):
		self.args = init_args()

		self.log = init_logger(logging.DEBUG if self.args.verbose else logging.INFO, self.args.out_to_file)
		self.msg_journal = {}

	# -- server --

	async def ws_server(self):
		if not self.args.server_port:
			return

		self.server_stop = asyncio.Future()		# may be useful to stop listening
		self.log.info('Run listening to %s:%s', self.args.host, self.args.server_port)
		async with websockets.serve(self.server_handler, self.args.host, self.args.server_port, create_protocol=ExtWebSocketServerProtocol):
			await self.server_stop

	async def server_handler(self, ws):
		self.log.info('New connection from %s, %s', ws.remote_address, ws.id)
		if not self.args.no_server_stream:
			sl_task = asyncio.create_task(self.sender_loop(ws))
		await self.receiver_loop(ws)

	# -- client --

	async def ws_client(self):
		if not self.args.client_port:
			return

		uri = f'ws://{self.args.host}:{self.args.client_port}/{self.args.client_uri}'

		extra_headers = {
			'instance-id': 1,
		}
		if self.args.ses6:
			extra_headers['X-Xsrf-Token'] = self.args.ses6

		await asyncio.sleep(0.2)		# для запуска клиента и сервера в одном приложении

		while 1:
			try:
				async with websockets.connect(uri, extra_headers=extra_headers) as ws:
					self.log.info('Connected to %s, %s', ws.remote_address, ws.id)
					sl_task = asyncio.create_task(self.sender_loop(ws))
					await self.receiver_loop(ws)
			except ConnectionRefusedError:
				self.log.warning('Connection refused. Trying again')
				await asyncio.sleep(5)

			input('> Proceed connect process?')


	# -- common --

	async def send(self, ws, msg):
		try:
			await ws.send(msg)
		except websockets.exceptions.ConnectionClosed:
			return False
		self.log.debug('Sent message %s', msg)
		return True


	async def receiver_loop(self, ws):
		while True:
			try:
				msg = await ws.recv()
			except websockets.exceptions.ConnectionClosed as e:
				self.log.debug('ConnectionClosed during recv %s: %s', ws.id, e)
				break

			self.log.info('Received message %s', msg)
			if reply := self.on_received(json.loads(msg)):
				if not await self.send(ws, reply):
					break
		self.log.info('Connection was closed, %s', ws.id)

	async def sender_loop(self, ws):
		await asyncio.sleep(0.2)		# иногда пакет с данными от сервера идет до ответа со 101 (переключение протокола)
										# таки подозреваю, что это неправильно(
		while 1:
			if self.args.send_request:
				answer = input('> send next? ')
				if answer == 'ls':	# debug long sleep
					await asyncio.sleep(60)
			msg = self.get_next_message(str(ws.id))
			if not msg:
				self.log.error('No reply. id="%s". self.msg_journal="%s". msg="%s"', ws.id, self.msg_journal, msg)
				raise RuntimeError(f'No reply for message for connection {ws.id}')

			if not await self.send(ws, msg):
				break

			await asyncio.sleep(self.args.delay)


	def on_received(self, rcv_obj):
		if not rcv_obj.get('reply'):	# если это не ответ на сообщение
			rcv_obj['reply'] = True		# то сами формируем такой ответ
			return json.dumps(rcv_obj)

		rec = self.msg_journal.get(rcv_obj['uid'])
		if not rec:
			self.log.error('Unexpecteds reply. rcv_obj="%s". self.msg_journal="%s"', rcv_obj, self.msg_journal)
			raise RuntimeError('Implementation error')
		rec['replied'] = True

	def get_next_message(self, uid):
		rec = self.msg_journal.get(uid)
		if not rec:
			rec = self.msg_journal.setdefault(uid, {'cnt': 0, 'uid': uid})
			return json.dumps(rec)

		if not rec.get('replied'):
			return	# no reply for previous message

		rec['cnt'] += 1
		del rec['replied']

		return json.dumps(rec)



	async def main(self):
		await asyncio.gather(self.ws_server(), self.ws_client())


if __name__ == '__main__':
	try:
		asyncio.run(App().main())
	except KeyboardInterrupt as e:
		pass
