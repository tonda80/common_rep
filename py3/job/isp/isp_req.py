#!/usr/bin/env python3

# заготовка для пакетной отправки запросов

import json
import sys
import requests
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
import tkinter.filedialog


class Host:
	def __init__(self, ip, ses6, debug = False):
		assert not ip.endswith('/')
		self.ip = ip
		self.ses6 = ses6
		self.debug = debug

	def service(self, name):
		assert not name.endswith('/') and not name.startswith('/')
		return Service(self.ip, name, self.ses6, self.debug)

class Service:
	def __init__(self, ip, name, ses6, debug):
		self.ip = ip
		self.ses6 = ses6
		self.name = name
		self.debug = debug

	def _get_headers(self):
		return {
			'Host': 'instance-1',
			'Cookie': f'ses6={self.ses6}',
			'X-Xsrf-Token': self.ses6,
			'Isp-Box-Instance': 'true',
		}

	def _send_request(self, method, path, data = None):
		assert method in ('GET', 'POST', 'DELETE')
		assert not path.startswith('/')

		url = f'https://{self.ip}/{self.name}/{path}'
		headers = self._get_headers()

		if isinstance(data, (dict, list)):
			data = json.dumps(data)

		print(method, url, headers, data)
		if self.debug:
			return {'id': 777}

		resp = requests.request(method, url, headers=headers, data=data, verify=False)
		if not resp.ok:
			print(f'[Error] Bad responce: {resp} {resp.text}')
			raise RuntimeError('bad responce')

		return json.loads(resp.text)

	def get(self, path, data = None):
		return self._send_request('GET', path, data)

	def post(self, path, data):
		return self._send_request('POST', path, data)

	def delete(self, path, data):
		return self._send_request('DELETE', path, data)


if __name__ == '__main__':
	host = Host('172.31.48.233', '8C6F29730A7071C1093BFB49')
	ip = host.service('ip/v3')
	vm = host.service('vm/v3')

	for i in range(50):
		net = f'10.4.{i+1}'
		resp = ip.post('userspace/public/ippool', {'name': f'scrpt2_pool_{i}', 'note': ''})
		pool_id = resp['id']
		vm.post(f'ippool/{pool_id}/range', {'name': f'{net}.10-{net}.15'})
		vm.post('userspace/public/ipnet', {'need_reserve':True, 'name': f'{net}.0/24', 'gateway': f'{net}.1', 'note': ''})
		vm.post(f'ippool/{pool_id}/cluster', {'clusters': [{'id':1, 'interface':0}]})
