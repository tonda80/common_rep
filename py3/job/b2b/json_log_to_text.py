#!/usr/bin/env python3

import json
import sys
import os


in_path = sys.argv[1]
out_path = in_path + '_out.txt'
assert os.path.isfile(in_path)



with open(out_path, 'w') as out_file, open(in_path) as in_file:
	for l in in_file:
		try:
			jo = json.loads(l)
		except json.JSONDecodeError:
			out_file.write(l)
			continue
		out_file.write(f'{jo["time"]} {jo["level"]} {jo["message"]}')		# {jo["file"]}
