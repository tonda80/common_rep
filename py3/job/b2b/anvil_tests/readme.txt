Подготовка к запуску
---

Установка зависимостей
--
sudo pip3 install -r requirements.txt

Настройка конфига
--
cp config.py.example config.py
установить нужные:
  proto_dir (путь к скомпилированным питон прото файлам)
  amqp.connection['host']



Запуск
---

python3 -m unittest discover . -v
запускает все тесты в директории

./test_groups.py
запускает все тесты в файле test_groups.py

./test_groups.py GroupServiceTest
запускает все тесты в GroupServiceTest файла test_groups.py

./test_groups.py GroupServiceTest.test_read
запускает один тест GroupServiceTest.test_read файла test_groups.py


Забрать стоящее из b2b репы
B2B=/home/ant/b2b_reps/anvil_art/scripts/anvil_tests/  &&  cp $B2B/amqp.py $B2B/sub_demo.py $B2B/common.py $B2B/config.py $B2B/config.py.example $B2B/test_groups.py $B2B/test_prices.py $B2B/test_symbols.py  .
