# общий код тестов

import logging
import sys

import config
import amqp

sys.path.append(config.proto_dir)
# добавим тут путь к импортируемым прото файлам

import generic.general_pb2

amqp.RpcReply.PbCommonError = generic.general_pb2.CommonError



def __init_logger(level, root_level, name):
	logging.basicConfig(format='[%(asctime)s.%(msecs)d][%(levelname)s] %(message)s',
			datefmt='%d %b %H:%M:%S')

	log = logging.getLogger(name)

	log.setLevel(level)
	logging.getLogger().setLevel(root_level)

	return log


# ---- протобаф хелперы ----

def pb_str(pb_Class, **kw):
	obj = pb_Class(**kw)
	return obj.SerializeToString()

def pb_obj_from_str(pb_Class, s):
	obj = pb_Class()
	obj.ParseFromString(s)
	return obj



# ---- объекты ----

log = __init_logger(config.log.level, config.log.root_level, 'AnvTest')


rpc_client = amqp.RpcClient(config.amqp.connection, config.amqp.exchange, config.amqp.req_queue, log)
