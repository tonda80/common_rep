#!/usr/bin/env python3

# тестирование символов


import unittest

from common import *
import config

import anvil.general_pb2		# pip3 install protobuf
import anvil.symbols.symbol_service_pb2



class SymbolService:

	# returns Symbol
	@staticmethod
	def Read(name):
		body = pb_str(anvil.symbols.symbol_service_pb2.Symbol, name = name)

		reply = rpc_client.sync_request('SymbolService.Read', body)

		return reply.pb_obj(anvil.symbols.symbol_service_pb2.Symbol)

	# returns list of Symbol
	@staticmethod
	def ReadByLimitOffset(limit, offset):
		limit_offset = anvil.general_pb2.LimitOffset(limit=limit, offset = offset)
		body = pb_str(anvil.symbols.symbol_service_pb2.SymbolLimitOffset, limit_offset = limit_offset)

		reply = rpc_client.sync_request('SymbolService.ReadByLimitOffset', body)
		symbols = reply.pb_obj(anvil.symbols.symbol_service_pb2.Symbols)
		return [s for s in symbols.symbols]



def symbol_generator(max_ = None):
	offset = 0
	limit = 100
	while 1:
		if max_ is not None and max_ < limit:
			limit = max_
		assert limit > 0

		symbols = SymbolService.ReadByLimitOffset(limit, offset)
		for s in symbols:
			yield s
			if max_ is not None:
				max_ -= 1

		if max_ is not None and max_ <= 0:
			break

		readed = len(symbols)
		assert readed <= limit
		if readed < limit:
			break
		else:
			offset += readed


def read_all_symbols():
	return list(symbol_generator())


def read_n_symbols(n):
	for s in symbol_generator(n):
		yield s



class SymbolServiceTest(unittest.TestCase):

	def symbol_is_valid(self, sym):
		for a in ('name', 'path'):
			self.assertTrue(getattr(sym, a))


	@classmethod
	def setUpClass(cls):
		cls._all_symbols = None

	@classmethod
	def all_symbols	(cls):
		if cls._all_symbols is None:
			cls._all_symbols = read_all_symbols()
			log.info(f'Total symbols: {len(cls._all_symbols)}')
		return cls._all_symbols

	def test_read(self):
		for s in self.all_symbols()[:20]:		# для части символов вычитанных ранее через read_all_symbols
			symbol = SymbolService.Read(s.name)
			self.assertEqual(symbol, s)


	def test_read_by_limit_offset(self):
		n_symbols = len(self.all_symbols())
		self.assertEqual(config.symbols.number, n_symbols)

		def impl(limit, offset, result):
			symbols = SymbolService.ReadByLimitOffset(limit, offset)
			self.assertEqual(result, len(symbols))
			for s in symbols:
				self.symbol_is_valid(s)

		impl(0, 33, 0)
		impl(8, 999999, 0)
		#
		assert n_symbols > 100
		impl(45, 30, 45)
		impl(80, 10, 80)
		impl(50, n_symbols - 20, 20)

	def test_nonexistent_symbol(self):
		self.assertRaises(amqp.RpcReply.Error, SymbolService.Read, "SomeIncredibleSymbolNameE.G.ZoombaBoomba!!")



if __name__ == '__main__':
	unittest.main()
