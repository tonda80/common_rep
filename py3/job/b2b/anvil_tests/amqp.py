# AMQP RPC client


import uuid

import threading
import queue

import pika		# pip3 install pika




class RpcClient:

	def __init__(self, conn_par_dict, exchange, req_queue, logger):
		self.exchange = exchange
		self.req_queue = req_queue
		self.log = logger

		self.connection = pika.BlockingConnection(pika.ConnectionParameters(**conn_par_dict))
		self.channel = self.connection.channel()

		# TODO channel.exchange_declare()
		self.channel.queue_declare(queue=req_queue)

		# TODO? use SelectConnection
		self.consum_thread = RpcClient.ConsumThread(conn_par_dict)
		self.rep_queue = self.consum_thread.rep_queue
		self.consum_thread.start()


	# не по науке и в тестах не работает, но не так критично
	def __del__(self):
		self.consum_thread.stop()
		self.connection.close()


	def sync_request(self, endpoint, body, headers = None, timeout = 3):
		rcv_queue = queue.Queue()

		correlation_id = self.async_request(lambda reply: rcv_queue.put(reply), endpoint, body, headers)

		try:
			reply = rcv_queue.get(timeout=timeout)
		except queue.Empty:
			self.log.warning(f'[RpcClient.sync_request] timeout')
			self.consum_thread.pop_waited(correlation_id)							# не ждем это больше
			return

		assert reply.correlation_id == correlation_id

		return reply


	def async_request(self, callback, endpoint, body, headers = None):
		correlation_id = str(uuid.uuid4())

		self.consum_thread.add_waited(correlation_id, callback)

		_headers = {'endpoint': endpoint}		# request_id
		if headers:
			_headers.update(headers)

		self.channel.basic_publish(exchange=self.exchange, routing_key=self.req_queue,
				properties=pika.BasicProperties(reply_to=self.rep_queue, correlation_id=correlation_id, headers=_headers),
				body=body)
				# TODO? except pika.exceptions.StreamLostError, channel state

		return correlation_id



	class ConsumThread(threading.Thread):
		def __init__(self, conn_par_dict):	# connection, channel
			threading.Thread.__init__(self, daemon=True)

			# свои соединение и канал, потому что с одним каналом получение в другом потоке не работает
			# все это (+ демонизация треда) не очень красиво, при случае переделаем на SelectConnection
			self.connection = pika.BlockingConnection(pika.ConnectionParameters(**conn_par_dict))
			self.channel = self.connection.channel()

			res = self.channel.queue_declare(queue='', exclusive=True)
			self.rep_queue = res.method.queue

			self.channel.basic_consume(queue=self.rep_queue,
						on_message_callback=self.on_reply,
						auto_ack=True
			)

			self._stop_ev = threading.Event()
			self._waited_reply = {}
			self._waited_reply_lock = threading.Lock()


		def run(self):
			while not self._stop_ev.is_set():
				self.connection.process_data_events(0.5)		# 0 - ASAP, None - till events

		def stop(self):
			self._stop_ev.set()

			if not self.daemon:	# на всякий случай
				self.join()

		def add_waited(self, key, value):
			with self._waited_reply_lock:
				self._waited_reply[key] = value

		def pop_waited(self, key):
			with self._waited_reply_lock:
				return self._waited_reply.get(key)

		def on_reply(self, ch, method, props, body):
			#print('[on_reply]', ch, method, props, body)

			callback = self.pop_waited(props.correlation_id)
			if callback is None:
				self.log.warning(f'unexpected correlation id {props.correlation_id}')
				return

			callback(RpcReply(props, body))






class RpcReply:
	PbCommonError = None	# will be inited into common!

	def __init__(self, props, body):
		self.body = body
		self.headers = props.headers
		self.correlation_id = props.correlation_id

	class Error(RuntimeError):
		def __init__(self, err_obj):
			RuntimeError.__init__(self, 'Cannot parse RPC reply')
			self.err_obj = err_obj

		def __str__(self):		# для отображения в unittest-овом выводе
			return f'{self.err_obj}'

	# getting protobuf from amqp.RpcReply
	def pb_obj(self, pb_Class):
		if self.headers['is_success']:
			obj = pb_Class()
			obj.ParseFromString(self.body)
			return obj

		# иначе, ругаемся исключением
		err_obj = RpcReply.PbCommonError()
		err_obj.ParseFromString(self.body)
		raise RpcReply.Error(err_obj)

	def is_success(self):
		return self.headers['is_success']
