#!/usr/bin/env python3

# скрипт для просмотра публикуемых энвилом сообщений
# использует общий код тестов


import pika


from common import *
del rpc_client		# ибо не тут не нужен
import config


import anvil.accounts.account_service_pb2



class SubApp:
	def __init__(self):
		connection = pika.BlockingConnection(pika.ConnectionParameters(**config.amqp.connection))
		channel = connection.channel()

		channel.exchange_declare(exchange=config.amqp.pub_exchange, exchange_type='topic')

		result = channel.queue_declare('', exclusive=True)
		queue_name = result.method.queue

		if not config.amqp.binding_keys:
			raise RuntimeError('no binding_keys')

		for k in config.amqp.binding_keys:
			binding_key = config.amqp.base_binding_key + k
			channel.queue_bind(exchange=config.amqp.pub_exchange, queue=queue_name, routing_key=binding_key)
			log.info(f'bound with key {binding_key}')

		channel.basic_consume(queue=queue_name, on_message_callback=self.on_message, auto_ack=True)

		print(f'Waiting for messages from {config.amqp.connection["host"]} {config.amqp.pub_exchange}. To exit press CTRL+C')
		channel.start_consuming()

	def on_message(self, ch, method, properties, body):
		if '.Account.' in method.routing_key:
			msg = pb_obj_from_str(anvil.accounts.account_service_pb2.Account, body)
		elif method.routing_key.endswith('.AccountMigration'):
			msg = pb_obj_from_str(anvil.accounts.account_service_pb2.AccountMigration, body)
		else:
			msg = f'UNRECOGNIZED BODY <{body}>'

		print(f"> recv {method.routing_key}:\n{msg}")



if __name__ == '__main__':
	SubApp()
