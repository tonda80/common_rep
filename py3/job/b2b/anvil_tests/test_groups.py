#!/usr/bin/env python3

# тестирование групп


import unittest

from common import *
import config

import anvil.general_pb2		# pip3 install protobuf
import anvil.groups.group_service_pb2



class GroupService:

	# returns Group
	@staticmethod
	def Read(name):
		body = pb_str(anvil.groups.group_service_pb2.Group, name = name)

		reply = rpc_client.sync_request('GroupService.Read', body)

		return reply.pb_obj(anvil.groups.group_service_pb2.Group)

	# returns list of Group
	@staticmethod
	def ReadByLimitOffset(limit, offset):
		limit_offset = anvil.general_pb2.LimitOffset(limit=limit, offset = offset)
		body = pb_str(anvil.groups.group_service_pb2.GroupLimitOffset, limit_offset = limit_offset)

		reply = rpc_client.sync_request('GroupService.ReadByLimitOffset', body)
		groups = reply.pb_obj(anvil.groups.group_service_pb2.Groups)
		return [g for g in groups.groups]


def read_all_groups():
	limit = 100
	offset = 0
	ret = []
	while 1:
		groups = GroupService.ReadByLimitOffset(limit, offset)
		ret.extend(groups)
		readed = len(groups)
		assert readed <= limit
		if readed < limit:
			break
		else:
			offset += readed
	return ret



class GroupServiceTest(unittest.TestCase):

	def group_is_valid(self, group):
		for a in ('name', ):	# 'currency'
			self.assertTrue(getattr(group, a))


	@classmethod
	def setUpClass(cls):
		cls._all_groups = None

	@classmethod
	def all_groups(cls):
		if cls._all_groups is None:
			cls._all_groups = read_all_groups()
			log.info(f'Total groups: {len(cls._all_groups)}')
		return cls._all_groups


	def test_read(self):
		for g in self.all_groups():		# для всех групп вычитанных ранее через read_all_groups
			group = GroupService.Read(g.name)
			self.assertEqual(group, g)


	def test_read_by_limit_offset(self):
		n_groups = len(self.all_groups())
		self.assertEqual(config.groups.number, n_groups)

		def impl(limit, offset, result):
			groups = GroupService.ReadByLimitOffset(limit, offset)
			self.assertEqual(result, len(groups))
			for g in groups:
				self.group_is_valid(g)

		impl(0, 20, 0)
		impl(8, 999999, 0)
		#
		assert n_groups > 4
		impl(8, n_groups - 4, 4)
		impl(2, 2, 2)
		if n_groups > 25:
			impl(10, 5, 10)
			impl(7, 12, 7)

	def test_nonexistent_group(self):
		self.assertRaises(amqp.RpcReply.Error, GroupService.Read, "SomeIncredibleGroupNameE.G.ZoombaBoomba!!")


if __name__ == '__main__':
	unittest.main()
