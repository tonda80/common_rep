#!/usr/bin/env python3



import unittest

from common import *
import config

import anvil.general_pb2		# pip3 install protobuf
import anvil.prices.chart_service_pb2
import anvil.prices.tick_service_pb2

import test_symbols



class ChartService:

	@staticmethod
	def ReadByTimeFrame(symbol, begin, end):
		begin_time = anvil.general_pb2.Time(time=begin, milliseconds=0);
		end_time = anvil.general_pb2.Time(time=end, milliseconds=0);
		time_frame = anvil.general_pb2.TimeFrame(begin=begin_time, end=end_time);
		bar = anvil.prices.chart_service_pb2.Bar(symbol=symbol)

		body = pb_str(anvil.prices.chart_service_pb2.BarTimeFrame, time_frame=time_frame, bar=bar)

		reply = rpc_client.sync_request('ChartService.ReadByTimeFrame', body)
		bars = reply.pb_obj(anvil.prices.chart_service_pb2.Bars)
		return [b for b in bars.bars]

	@staticmethod
	def ReadLast(symbol):
		body = pb_str(anvil.prices.chart_service_pb2.Bar, symbol=symbol)

		reply = rpc_client.sync_request('ChartService.ReadLast', body)
		return reply.pb_obj(anvil.prices.chart_service_pb2.Bar)


def read_all_bars(symbol):
	return ChartService.ReadByTimeFrame(symbol, 0, (1 << 31) - 1)	# ?? 1<<31 уже не работает



class TickService:

	@staticmethod
	def ReadLast(symbol):
		body = pb_str(anvil.prices.tick_service_pb2.Tick, symbol=symbol)

		reply = rpc_client.sync_request('TickService.ReadLast', body, timeout = 10)
		return reply.pb_obj(anvil.prices.tick_service_pb2.Tick)




class ChartServiceTest(unittest.TestCase):

	def bar_is_valid(self, bar):
		for a in ('symbol',):
			self.assertTrue(getattr(bar, a))

	@classmethod
	def setUpClass(cls):
		cls._charts = None

	@classmethod
	def charts(cls):
		if cls._charts is None:
			cls._charts = []
			for s in test_symbols.symbol_generator(config.prices.n_symbols):
				bars = read_all_bars(s.name)
				log.info(f'Total bars for {s.name}: {len(bars)}')
				if bars:
					cls._charts.append(bars)
		return cls._charts


	def test_ReadByTimeFrame(self):
		self.assertTrue(len(self.charts()) > 0)

		for bars in self.charts():
			for b in bars:
				self.bar_is_valid(b)


	def test_ReadLast(self):
		self.assertTrue(len(self.charts()) > 0)

		for bars in self.charts():
			symbol = bars[0].symbol
			last = ChartService.ReadLast(symbol)
			#self.assertEqual(last, bars[-1])		# volume часто становятся не равны
			self.assertEqual(last.symbol, bars[-1].symbol)		# ограничимся проверкой имени
			log.debug(f'last chart: {last}')




class TickServiceTest(unittest.TestCase):

	def tick_is_valid(self, tick, symbol):
		self.assertEqual(tick.symbol, symbol)

	def test_ReadLast(self):
		for s in test_symbols.symbol_generator(config.prices.n_symbols):
			try:
				tick = TickService.ReadLast(s.name)
			except amqp.RpcReply.Error:
				log.warning(f'no ticks for {s.name}')
				continue
			self.tick_is_valid(tick, s.name)

	def _test_ReadLastCustom(self):
		for s in ('XAUUSD.bak', 'NewSymb'):
			try:
				tick = TickService.ReadLast(s)
			except amqp.RpcReply.Error:
				log.warning(f'no ticks for {s}')
				continue
			self.tick_is_valid(tick, s)




if __name__ == '__main__':
	unittest.main()
