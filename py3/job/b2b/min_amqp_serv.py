#!/usr/bin/env python3

# min b2b AMQP RPC server

import argparse

import pika



def init_args(arg_src = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('--host', '-ht', required=True)
	parser.add_argument('--exchange', '-e', required=True)
	parser.add_argument('--queue', '-q', required=True)
	parser.add_argument('--prefetch_count', type=int, default='1')

	return parser.parse_args()
args = init_args()


# ------------

connection = pika.BlockingConnection(pika.ConnectionParameters(args.host, heartbeat=0))

channel = connection.channel()
channel.queue_declare(queue=args.queue)


def on_request(ch, method, props, body):
	print(f'got request {body}')

	#import generic.general_pb2
	#msg = generic.general_pb2.CommonError()
	#msg.code = '444'
	#msg.message = 'fake reply'
	#msg.SerializeToString()
	resp = b'\n\x03444\x12\nfake reply'

	headers = {'is_success': False}

	ch.basic_publish(exchange=args.exchange,
			 routing_key=props.reply_to,
			 properties=pika.BasicProperties(headers=headers, correlation_id = props.correlation_id),
			 body=resp)
	ch.basic_ack(delivery_tag=method.delivery_tag)
	print(f'rpc_server replied  to (reply_to={props.reply_to}, correlation_id={props.correlation_id}, delivery_tag={method.delivery_tag})')


channel.basic_qos(prefetch_count=args.prefetch_count)
channel.basic_consume(queue=args.queue, on_message_callback=on_request)

print('\nstarted RPC request listener loop (ctrl-c to stop)')
channel.start_consuming()
