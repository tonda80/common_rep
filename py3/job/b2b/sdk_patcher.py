#!/usr/bin/env python3

# https://b2btech.atlassian.net/browse/MTW-994
# [Anvil] реализовать патч SDK mt5

# Написать скрипт на Python, который должен:
# - выкачивать заданную версию SDK из репозитория
# - вносить изменения для возможности сборки под Linux
# - пушить изменения в репозиторий с SDK с тегом, например ver_patch, где ver - номер исправленной версии.


import argparse
import tempfile
import os
import subprocess
import re
import io
import shutil



DEF_REPO = 'https://anvil_arts:79-sLGqkEuqistcPtkAb@vcshl.b2broker.tech/sdk/mt5.git'


class App:
	def init_args(self, arg_src = None):
		parser = argparse.ArgumentParser()
		parser.add_argument('--tag', '-t', required= True, help='Repo tag (branch). Required.')
		parser.add_argument('--dir', help='Directory to load repo')
		parser.add_argument('--repo', default=DEF_REPO, help='Repo address')
		parser.add_argument('--debug_flags', '-d', type=int, default=0, help='Debug bit mask. 0: no patch, 1: no push)')
		return parser.parse_args(arg_src)

	def init_cwd(self):
		if self.args.dir:
			if not os.access(self.args.dir, os.W_OK):
				raise RuntimeError(f'directory {self.args.dir} is not writeable')
			#if os.listdir(self.args.dir):	# git clone error
			#	raise RuntimeError(f'directory {self.args.dir} is not empty')
			return self.args.dir

		new_dir = tempfile.mkdtemp(prefix='sdk_patcher_')
		print(f'script working directory is {new_dir}')
		return new_dir

	def __init__(self):
		self.args = self.init_args()
		self.cwd = self.init_cwd()
		self.patch_branch_name = self.args.tag + '_patched'

		self.include_slash_re = re.compile(rb'\s*#include\s*".*\\.*"')
		self.i64_postfix1_re = re.compile(rb'0[xX][0-9a-fA-F]+(i64)')
		self.i64_postfix2_re = re.compile(rb'[0-9]+(i64)')
		self.int64_cast_re = re.compile(rb'double\s*\(\s*(__int64)')	# (double|float|etc) - по необходимости

		self.is_patched = False


	def run(self, *args):
		print('>>', ' '.join(args))
		res = subprocess.run(args, cwd=self.cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)	#, check=True
		if res.returncode != 0:
			print(res.stdout.decode())
			raise RuntimeError('run error')
		return res


	def clone_repo(self):
		self.run('git', 'clone', '--depth', '1', '-b', self.args.tag, self.args.repo, self.cwd)

	def create_patch_branch(self):
		ls_remote_out = self.run('git', 'ls-remote', '--heads').stdout.decode()
		if f'heads/{self.patch_branch_name}' in ls_remote_out:
			raise RuntimeError(f'Looks like patch branch {self.patch_branch_name} exists already')

		self.run('git', 'checkout', '-b', self.patch_branch_name)

	def commit(self):
		if not self.is_patched:
			print('SKIPPED commit')
			return
		self.run('git', 'commit', '-a', '-m', f'Auto patch of {self.args.tag}')

	def push(self):
		if not self.is_patched or self.args.debug_flags & 2:
			print('SKIPPED push')
			return
		self.run('git', 'push', '--set-upstream', 'origin', self.patch_branch_name)



	def main(self):
		self.clone_repo()
		self.create_patch_branch()

		self.patch_files()

		self.commit()
		self.push()

		#git push --set-upstream origin test_branch_from_tag


	def patch_files(self):
		for dirpath, _, filenames in os.walk(os.path.join(self.cwd, 'include')):
			for f in filenames:
				low_ext = os.path.splitext(f)[1].lower()
				if low_ext == '.h':
					self.patch_file(os.path.join(dirpath, f))

	def patch_file(self, filepath):
		is_patch_needed = False
		buf = io.BytesIO()
		with open(filepath, 'rb') as file_:
			for i, line in enumerate(file_):
				if line:
					patch_line = (
						self.fix_include_slashes(line) or
						self.fix_various(line)
						) # только 1 фикс применится
					if patch_line:
						print(f'{i}:\n -', line, '\n +', patch_line)
						is_patch_needed = True
						line = patch_line
				buf.write(line)

		if is_patch_needed:
			if not self.args.debug_flags & 1:
				with open(filepath, 'wb') as file_:
					buf.seek(0)
					shutil.copyfileobj(buf, file_)
				self.is_patched = True
				print(filepath, 'is patched\n')
			else:
				print(filepath, 'would be patched\n')

	def fix_include_slashes(self, line):
		if self.include_slash_re.match(line):
			return line.replace(b'\\', b'/')

	def fix_various(self, line):
		mut_line = None
		def replace_to(mo, new):
			nonlocal mut_line
			if mut_line is None:
				mut_line = bytearray(line)
			mut_line[mo.start(1):mo.end(1)] = new

		for mo in reversed(list(self.i64_postfix1_re.finditer(line))):
			replace_to(mo, b'll')

		for mo in reversed(list(self.i64_postfix2_re.finditer(line))):
			replace_to(mo, b'll')

		for mo in reversed(list(self.int64_cast_re.finditer(line))):
			replace_to(mo, b'(__int64)')

		if mut_line:
			return bytes(mut_line)




if __name__ == '__main__':
	App().main()
