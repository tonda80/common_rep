#!/usr/bin/env python3


# деплоим файлы после сборки в заданную директорию



import argparse
import os
import shutil
import glob



class App:
	def init_args(self, arg_src = None):
		parser = argparse.ArgumentParser()
		parser.add_argument('--dest', required= True, help='Destination directory')
		parser.add_argument('--bin', required= True, help='Cmake binary directory')
		parser.add_argument('--src', required= True, help='Cmake source directory')
		parser.add_argument('--void_size', required= True, help='Extra library directory')
		parser.add_argument('--verbose', '-v', action='store_true', help='Verbose output')
		return parser.parse_args(arg_src)

	def __init__(self):
		self.args = self.init_args()

		self.art_name = os.path.basename(self.args.src)
		self.dest_dir = os.path.join(self.args.dest, self.art_name)

		if self.art_name == '4_art':
			self.sdk_libs_dir = os.path.realpath(os.path.join(self.args.src, '..', 'mt4', 'libs'))
			self.extra_libs = None
		elif self.art_name == '5_art':
			self.sdk_libs_dir = os.path.realpath(os.path.join(self.args.src, '..', 'mt5', 'libs'))
			self.extra_libs = os.path.realpath(os.path.join(self.args.src, '..', 'binaries', 'dlls')) + '/*.dll'
		else:
			raise RuntimeError(f'Unexpected artefact name: {self.art_name}')

		if self.args.void_size == '8':
			self.lib_postfix = '64'
		elif self.args.void_size == '4':
			self.lib_postfix = ''
		else:
			raise RuntimeError(f'Unexpected void_size: {self.args.void_size}')


	def report(self, msg):
		if self.args.verbose:
			print(msg)

	def copy_files(self, src_glob):
		self.report(f'copiyng {src_glob}')
		for p in glob.glob(src_glob):
			shutil.copy(p, self.dest_dir)
			self.report(f' copied {p}')


	def main(self):
		# mkdir
		# TODO? clear self.dest_dir
		if not os.path.isdir(self.dest_dir):
			os.mkdir(self.dest_dir)
		self.report(f'created {self.dest_dir}')

		# exe
		self.copy_files(f'{self.args.bin}/*.exe')

		# sdk libs
		self.copy_files(f'{self.sdk_libs_dir}/*{self.lib_postfix}.dll')

		# extra libs
		if self.extra_libs:
			self.copy_files(self.extra_libs)


if __name__ == '__main__':
	App().main()
