#!/usr/bin/env python3


# копируем исходники сабмодуля art_core из одной репы в другую

import subprocess
import os
import shutil


from baseapp import BaseConsoleApp



pj = os.path.join

def git(*args, cwd=None):
		cmd = ['git']
		cmd.extend(args)
		App.log.debug(f'[{cwd}$ {" ".join(cmd)}')
		return subprocess.check_output(cmd, cwd=cwd)


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('--anvil_art_path', default='/home/ant/b2b_reps/anvil_art', help='Путь к репозиторию')
		self.parser.add_argument('--art_builder_path', default='/home/ant/b2b_reps/art_builder', help='Путь к репозиторию')
		self.parser.add_argument('-s', '--source', required=True, choices=('a', 'b'), help='Источник anvil или builder')
		self.parser.add_argument('-i', '--info_only', action='store_true', help='Не копируем, только вывод')

	def main(self):
		anvil_path = os.path.realpath(self.args.anvil_art_path)
		builder_path = os.path.realpath(self.args.art_builder_path)
		src, dst = pj(anvil_path, 'art_core'), pj(builder_path, 'art_core')
		if self.args.source == 'b':
			src, dst = dst, src
		elif self.args.source != 'a':
			raise NotImplementedError
		assert os.path.isdir(src)
		assert os.path.isdir(dst)

		self.copy(src, dst)

	def copy(self, src, dst):
		file_list = git('ls-files', '-mo', cwd=src).splitlines()
		for f in file_list:
			sf = f.decode()
			s, d = pj(src, sf), pj(dst, sf)
			self.log.info(f'{s} => {d}')
			if not self.args.info_only:
				shutil.copy(s, d)





if __name__ == '__main__':
	App().main()
