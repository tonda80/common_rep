import json
import os
import sys



def bits2(num):
	# TODO не позориться и переделать на маски
    s = bin(num)[2:]
    for i in range(len(s)):
        if s[len(s) - 1 - i] == '1':
            print(f'Bit {i}')

def bits(num):
	# в mix_utils будет последнее
	i = 0
	while 1:
		mask = 1 << i
		if mask > num:
			break
		if mask & num:
			print(f'Bit {i}')
		i += 1



# put in utils
def suffix_path(path, suffix):
	spl = os.path.splitext(path)
	return spl[0] + suffix + spl[1]


def ast_reduce(fname):
	with open(fname) as file_:
		root = json.load(file_)

	def reduce_item(obj):
		if isinstance(obj, list):
			for item in obj:
				reduce_item(item)

		if isinstance(obj, dict):
			for key in ('loc', ):		# 'line', 'column'
				if key in obj:
					obj.pop(key) #obj[key] = 1

			for item in obj.values():
				reduce_item(item)

	reduce_item(root)

	with open(suffix_path(fname, '_red'), 'w') as file_:
		json.dump(root, file_, indent=2)



if __name__ == '__main__':
	ast_reduce(sys.argv[1])
