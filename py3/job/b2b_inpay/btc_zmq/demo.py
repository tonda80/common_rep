#!/usr/bin/env python3


import subprocess
import threading
import time
import sys
import os

import zmq

sys.path.append(os.path.realpath(__file__+'/../..'))
from b2b_inpay_common import get_container_ip, JrpcClient



class ZmqSubscriber(threading.Thread):
	def __init__(self, host, ports):
		threading.Thread.__init__(self, name='ZmqSubscriber')
		self.stop = threading.Event()

		zmq_context = zmq.Context()
		self.socket = zmq_context.socket(zmq.SUB)		# таки надо делать несколько сокетов, если зачем то надо иметь разный RCVHWM
		for p in ports:
			endpoint = f'tcp://{host}:{p}'
			self.socket.connect(endpoint)
		self.socket.setsockopt(zmq.SUBSCRIBE, b'')		# TODO? filter

		self.recv_count = 0

		time.sleep(.2)			# пауза для zmq рукопожатия (вроде бы)
		self.start()

	def stop_and_join(self):
		self.stop.set()
		self.join()


	def run(self):
		poller = zmq.Poller()
		poller.register(self.socket, zmq.POLLIN)
		while not self.stop.is_set():
			for socket, mask in poller.poll(500):
				if socket is self.socket and mask & zmq.POLLIN:
					msgs = socket.recv_multipart()
					self.on_msg_received(msgs)

	def on_msg_received(self, msgs):
		self.recv_count += 1
		print('zmq recv', self.recv_count, msgs)


def load_wallet(jrpc, wallet_name):
	res, err = jrpc.send_ee('loadwallet', wallet_name)
	err_code = err and err.get('code')
	if err_code == -18:
		jrpc.send('createwallet', wallet_name)
	elif not(err_code is None or err_code == -4):
		GentleExit.panic(f'loadwallet: unexpected reply: {res} {err}')

	jrpc.send('getbalance')

def get_address(jrpc, addr_label):
	addr_dict, _ = jrpc.send_ee('getaddressesbylabel', addr_label)
	if addr_dict:
		addr = next(iter(addr_dict))		# берем любой
	else:
		addr = jrpc.send('getnewaddress', addr_label, 'p2sh-segwit')
	return addr


class GentleExit:
	zmq_sub = None

	@staticmethod
	def stop():
		GentleExit.zmq_sub.stop_and_join()

	@staticmethod
	def panic(reason):
		GentleExit.stop()
		raise RuntimeError(reason)


if __name__ == '__main__':
	host0 = get_container_ip('btc_zmq_0')
	print(f'btc_zmq_0 has address {host0}')

	zmq_sub = ZmqSubscriber(host0, (34560, 34561, 34562, 34563, 34564))		# константы в docker-compose.yml
	GentleExit.zmq_sub = zmq_sub
	jrpc0 = JrpcClient(host0, 12345, 'abtc', 'aaaa')

	jrpc0.send('getzmqnotifications')

	load_wallet(jrpc0, 'w0')
	addr = get_address(jrpc0, 'wal_addr1')

	jrpc0.send('generatetoaddress', 100, addr)

	try:
		input('enter or ctrl-c to exit\n')
	except KeyboardInterrupt:
		pass
	GentleExit.stop()
