from __future__ import annotations
import zmq
import zmq.asyncio
from typing import Optional
import re

from b2binpay_node.config import Config
from b2binpay_node.utils.logging import app_logger as logger


# асинхронный zmq подписчик, скопирован из основной репы, применение см там


class ZmqSub(object):
    # subscriber to zmq events as described here https://b2btech.atlassian.net/wiki/spaces/B2BP2/pages/2695823441/ZeroMQ

    EVENT_NAMES = 'hashtx', 'hashblock', 'rawblock', 'rawtx', 'sequence'

    @staticmethod
    def create(config: Config, **callbacks) -> Optional[ZmqSub]:
        # callback argument name must be one from event_names
        for k in callbacks.keys():
            if k not in ZmqSub.EVENT_NAMES:
                raise NotImplementedError(f'unexpected callback name {k}')

        cfg_dict = {}
        for name in ZmqSub.EVENT_NAMES:
            endpoint_par = f'{name}_endpoint'
            endpoint = config.get('zmq', endpoint_par, None)
            if endpoint:
                if not re.match(r'tcp://[.\w]*:\d+', endpoint):
                    raise RuntimeError(f'incorrect zmq endpoint in config for "{name}": "{endpoint}"')

                callback = callbacks.get(name, 'not set')
                if callback is 'not set':
                    logger.warning('No callback for event "{name}", skip it')
                    continue
                elif callback is None:  # explicitly omitted
                    continue

                cfg_dict[name] = endpoint, callback

        if not cfg_dict:
            return

        return ZmqSub(cfg_dict)

    def __init__(self, cfg_dict):
        self._callbacks = {}
        self._context = zmq.asyncio.Context()

        self._socket = self._context.socket(zmq.SUB)
        self._socket.setsockopt(zmq.RCVHWM, 0)
        for name, cfg in cfg_dict.items():
            ep, clb = cfg
            self._callbacks[name.encode()] = clb

            self._socket.setsockopt_string(zmq.SUBSCRIBE, name)
            self._socket.connect(ep)
            logger.info(f'[zmq] subscribed to {name}: {ep}')

    async def cleanup(self, _):
        self._context.destroy()
        logger.debug(f'[zmq] destroyed')
        # print(f'[zmq] destroyed')    # looks like logger has no time

    async def handle_once(self):
        topic, body, seq = await self._socket.recv_multipart()
        logger.debug(f'[zmq] {topic}: {body.hex()}')

        await self._callbacks[topic]()
