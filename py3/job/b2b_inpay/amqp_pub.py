from __future__ import annotations
import aio_pika
from typing import Optional, Dict
import json
from collections import namedtuple
import enum

from b2binpay_node.config import Config
from b2binpay_node.utils.logging import app_logger as logger
from b2binpay_common.crypt import Session


# асинхронный amqp паблишер, скопирован из основной репы node, применение см там


class AmqpPub(object):
    # AMQP publisher described here https://b2btech.atlassian.net/browse/PAYV2-2367

    class Event(enum.Enum):
        new_block = 'new_block'
        new_transaction = 'new_transaction'
        change_transaction = 'change_transaction'

    _EVENT_OPTIONS = namedtuple('EventOptions', ['ttl'])

    _OP_TIMEOUT = 1     # timeout for network actions

    @staticmethod
    def create(config: Config, session: Session) -> Optional[AmqpPub]:
        section = 'amqp'
        if not config.get_bool(section, 'enable', False):
            return

        connect_options = dict()
        connect_options['host'] = config.get(section, 'host', 'localhost')
        connect_options['port'] = config.get_int(section, 'port', 5672)
        connect_options['user'] = config.get(section, 'user', 'guest')
        connect_options['password'] = config.get(section, 'password', 'guest')

        pub_exchange = config.get(section, 'pub_exchange', '')

        event_options = dict()
        for e in AmqpPub.Event:
            event_options[e] = AmqpPub._EVENT_OPTIONS(
                config.get_int(section, f'{e.value}_ttl', 24*3600),
            )

        if config.get_bool(section, 'debug_no_encryption', False):
            session = None

        return AmqpPub(connect_options, pub_exchange, event_options, session)

    def __init__(self, connect_options: dict, pub_exchange: str,
                 event_options: Dict[AmqpPub.Event, AmqpPub._EVENT_OPTIONS],
                 session: Optional[Session]):

        self._connect_options = connect_options
        self._pub_exchange = pub_exchange
        self._event_options = event_options
        self._session = session
        if not self._session:
            logger.warning('[amqp] pub encryption is disabled')

        self._connection = None
        self._exchange = None

    async def connect(self):
        self._connection = await aio_pika.connect_robust(timeout=self._OP_TIMEOUT, **self._connect_options)
        channel = await self._connection.channel()

        self._exchange = await channel.declare_exchange(self._pub_exchange, aio_pika.ExchangeType.TOPIC,
                                                        timeout=self._OP_TIMEOUT)
        for e in AmqpPub.Event:
            routing_key = e.value
            queue_name = f'{self._pub_exchange}_{e.value}'
            queue = await channel.declare_queue(queue_name, timeout=self._OP_TIMEOUT)
            await queue.bind(self._exchange, routing_key=routing_key, timeout=self._OP_TIMEOUT)

        logger.info(f'[amqp] connected')

    async def close(self, _):
        if self._connection:
            await self._connection.close()

    async def publish(self, event: AmqpPub.Event, obj):
        message = self._create_message(event, obj)
        routing_key = event.value
        try:
            await self._exchange.publish(message, routing_key, timeout=self._OP_TIMEOUT)
        except Exception as e:
            logger.warning(f'Exception {e.__class__} into exchange.publish callback: {e}')

    def _create_message(self, event: AmqpPub.Event, obj) -> aio_pika.Message:
        body = json.dumps(obj, separators=(',', ':')).encode()
        if self._session:
            body = self._session.encrypt(body)

        return aio_pika.Message(body, expiration=self._event_options[event].ttl)
