# разные хелперы для b2b_inpay

import requests
import subprocess

version = 5

def get_container_ip(name):
	cmd = ('docker', 'inspect', '-f', '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}', name)
	out = subprocess.run(cmd, check=True, stdout=subprocess.PIPE).stdout
	ret = out.strip().decode()
	print(f'Container {name} has address {ret}')
	return ret


class JrpcClient:
	def __init__(self, host, port, user, pwd):
		self.url = f'http://{user}:{pwd}@{host}:{port}/'

	def _send(self, no_err, wallet, method, *params):
		req = {
			'jsonrpc': '2.0', 'id': 0,		# TODO? id
			'method': method, 'params': params
		}
		url = self.url
		wallet_log_str = ''
		if wallet is not None:
			url += f'wallet/{wallet}'
			if len(wallet) < 10:
				wallet_log_str = f'[{wallet}]'
			else:
				wallet_log_str = f'[{wallet[:3]}..{wallet[-3:]}]'

		resp = requests.post(url, json=req).json()
		#print('__deb', resp)		# , resp.text

		error = resp.get('error')
		result = resp.get('result')

		if error is not None:
			print(f'ERROR {method}{params}{wallet_log_str} => {error}')
			if no_err:
				raise RuntimeError('failed request')
		else:
			str_res = str(result)
			n = 80
			if len(str_res) > n:
				str_res = str_res[:n] + ' ....'
			print(f'OK {method}{params}{wallet_log_str} => {str_res}')

		return result, error

	def send(self, method, *params, wallet=None):
		return self._send(True, wallet, method, *params)[0]

	def send_ee(self, method, *params, wallet=None):		# expected error
		return self._send(False, wallet, method, *params)


class ControllerClient:
	def __init__(self, url='http://127.0.0.1:8080/api/node'):
		self.url = url

	def send(self, method, **params):
		req = {
			'jsonrpc': '2.0', 'id': 0,		# TODO? id
			'method': method, 'params': params
		}
		resp = requests.post(self.url, json=req).json()
		#print('__deb', resp)		# , resp.text
		result = resp.get('result')

		return result
