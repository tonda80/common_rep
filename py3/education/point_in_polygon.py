#!/usr/bin/env python3

# алгоритм определения точки внутри полигона


from baseapp import BaseTkApp
import uTkinter as utk
import mix_utils
import point_segment_distance


Point = mix_utils.Struct


# TODO? вдруг тут что получится мало мальски переиспользуемое, перенести в grid_field (переименовав оное)
class DrawField(utk.uCanvas):
	def __init__(self, *a, **kw):
		super().__init__(*a, **kw)

	def draw_oval(self, x, y, r, color, tag, **kw):
		self.create_oval(x-r, y-r, x+r, y+r, fill=color, outline=color, tags=(tag,), **kw)

	def draw_line(self, v0, v1, color, tag, **kw):
		self.create_line(v0.x, v0.y, v1.x, v1.y, fill=color, tags=(tag,), **kw)


class App(BaseTkApp):
	polygon_color = 'green'
	polygon_point_rad = 2
	polygon_tag = 'polygon'

	point_color = 'red'
	point_rad = 4
	point_tag = 'point'

	def app_init(self):
		self.vertexes = []
		self.point = None

	def create_gui(self):
		root = utk.uTk('Point in poligon', 640, 480)

		f = utk.uFrame(root, gmArgs={'expand':utk.YES, 'fill':utk.BOTH})
		self.field = DrawField(f)
		self.field.bind('<Button-1>', self.left_mouse_click_handler)
		self.field.bind('<Button-3>', self.right_mouse_click_handler)
		utk.trace_control_l(self.field)
		utk.trace_shift_l(self.field)
		utk.trace_alt_l(self.field)
		self.field.focus_set()	# чтобы отслеживать нажатия клавиш

		f = utk.uFrame(root, gmArgs={'expand':utk.NO, 'fill':utk.X})
		utk.uButton(f, self.btn_clear_handler, 'Clear', gmArgs = {'side':utk.LEFT})

		return root

	def left_mouse_click_handler(self, ev):
		# клик с control - удаляем ближайшую, клик с shift - заменяем ближайшую
		# клик с альт - добавляем точку в конец, простой клик добавит точку к ближайшему отрезку
		p0 = Point(x=ev.x, y=ev.y)
		if self.field.is_control_l_pressed or self.field.is_shift_l_pressed:
			indexes = self.sorted_vertex_indexes(ev.x, ev.y)
			if not indexes:
				return
			if self.field.is_control_l_pressed:
				del self.vertexes[indexes[0]]
			else:
				self.vertexes[indexes[0]] = p0
		else:
			if len(self.vertexes) < 3 or self.field.is_alt_l_pressed:
				self.vertexes.append(p0)
			else:
				segments = self.sorted_segments(p0)
				self.vertexes.insert(segments[0][1], p0)

		self.redraw_polygon()

	def right_mouse_click_handler(self, ev):
		self.point = Point(x=ev.x, y=ev.y)
		self.redraw_point()

	# индексы точек полигона упорядоченные по расстоянию до точки
	def sorted_vertex_indexes(self, x, y):
		vv = ((i, (x - v.x)**2 + (y - v.y)**2) for i, v in enumerate(self.vertexes))
		return [e[0] for e in sorted(vv, key = lambda e: e[1])]

	# сегменты полигона упорядоченные по расстоянию до точки
	def sorted_segments(self, p0):
		vv = []
		for i, v0 in enumerate(self.vertexes):
			j = i + 1 if i < len(self.vertexes) - 1 else 0
			p1 = point_segment_distance.segment_closest_point(v0, self.vertexes[j], p0)
			vv.append((i, j, (p0.x - p1.x)**2 + (p0.y - p1.y)**2))
		vv.sort(key = lambda e: e[2])
		return [(e[0], e[1]) for e in vv]


	def redraw_polygon(self):
		self.field.delete(self.polygon_tag)

		for v in self.vertexes:
			self.field.draw_oval(v.x, v.y, self.polygon_point_rad, self.polygon_color, self.polygon_tag)

		for i in range(len(self.vertexes) - 1):
			v0 = self.vertexes[i]
			v1 = self.vertexes[i + 1]
			self.field.draw_line(v0, v1, self.polygon_color, self.polygon_tag)
		if len(self.vertexes) > 2:
			v0 = self.vertexes[0]
			v1 = self.vertexes[-1]
			self.field.draw_line(v0, v1, self.polygon_color, self.polygon_tag)

	def redraw_point(self):
		self.field.delete(self.point_tag)
		if self.point:
			self.field.draw_oval(self.point.x, self.point.y, self.point_rad, self.point_color, self.point_tag)

	def btn_clear_handler(self):
		self.vertexes.clear()
		self.point = None
		self.field.delete(utk.ALL)


if __name__ == '__main__':
	App().mainloop()
