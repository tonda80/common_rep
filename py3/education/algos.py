#!/usr/bin/env python3

# всякие алгоритмические задачки с собеседований (ещё есть py/education/sorts.py с сортировками, надо будет перенести сюда)
#

import random

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		trg_pars1 = self.add_subparser('llr', self.linked_list_turn_main, help='Linked list reverse')
		trg_pars1.add_argument('list_data', nargs = '*', help='Linked list data')

		#self.parser.add_argument('--c', required=True, help='Общий для обеих таргетов аргумент, в командной строке указывается до задания таргета')

	def main(self):
		# тут общий код, например обработка общих аргументов
		return self.sub_main()

	def linked_list_turn_main(self):
		lls = [
			LinkedList(self.args.list_data if self.args.list_data else range(10)),
			LinkedList(),
			LinkedList('q'),
			LinkedList(random.randint(0, 9) for i in range(random.randint(0, 20))),
		]
		for l in lls:
			print('----', l, sep='\n')
			l.reverse()
			print(l)



class LinkedList:
	class Node:
		def __init__(self, data_ = None, next_ = None):
			self.data = data_
			self.next = next_

	_tail = Node()

	def __init__(self, datas = ()):
		self.head = LinkedList._tail
		self.extend(datas)

	def add(self, data):
		self.head = LinkedList.Node(data, self.head)

	def extend(self, datas):	# добавляет в обратном порядке
		for d in datas:
			self.add(d)

	def remove(self):
		if self.head.next:
			self.head = self.head.next

	def get_all(self):
		c = self.head
		while c.next:
			yield c.data
			c = c.next

	def __str__(self):
		s = ', '.join(map(str, self.get_all()))
		return f'LinkedList[{s}]'

	# собственно это и есть популярная задачка: разворот связного списка
	def reverse(self):
		prev = LinkedList._tail
		c = self.head
		while c.next:
			tmp = c.next
			c.next = prev
			prev = c
			c = tmp
		self.head = prev


if __name__ == '__main__':
	App().main()
