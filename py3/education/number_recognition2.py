#!/usr/bin/env python3

# учимся распознавать цифры (на входе скриншоты тетравекс, судоку итп)
# https://www.pyimagesearch.com/2017/02/13/recognizing-digits-with-opencv-and-python/
# https://www.pyimagesearch.com/2014/09/01/build-kick-ass-mobile-document-scanner-just-5-minutes/

# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_image_display/py_image_display.html#py-display-image

# https://tproger.ru/translations/opencv-python-guide/
#https://towardsdatascience.com/object-detection-via-color-based-image-segmentation-using-python-e9b7c72f0e11

# https://docs.opencv.org/4.2.0/			см cv2.__version__


import cv2			# sudo pip3 install opencv-python
import imutils		# sudo pip3 install imutils
import imutils.perspective

import time

from baseapp import BaseConsoleApp


White = (255, 255, 255)
Red = (0, 0, 255)

class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('image_path', help='Путь к файлу с цифрами')

	def app_init(self):
		pass

	def main(self):
		image = cv2.imread(self.args.image_path)	# cv2.IMREAD_GRAYSCALE
		#self.show_image(image)
		resized = image #self.transform_demo(0, imutils.resize, image, height=500)
		gray = self.transform_demo(0, cv2.cvtColor, resized, cv2.COLOR_BGR2GRAY)
		#for i in range(1, 15, 4):
		gb_сoef = 3	# должен быть нечетным
		blurred = self.transform_demo(0, cv2.GaussianBlur, gray, (gb_сoef, gb_сoef), 0)
		edged = self.transform_demo(0, cv2.Canny, blurred, 50, 200, 255)

		cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

		#cv2.drawContours(image, cnts[:1], -1, Red, 7)
		#self.show_image(image)

		need_contour = None
		for c in cnts:
			peri = cv2.arcLength(c, True)
			approx = cv2.approxPolyDP(c, 0.02 * peri, True)
			if len(approx) == 4:
				need_contour = approx		# длиннейший контур с 4 вершинами
				break

		assert need_contour is not None
		need_contour = need_contour.reshape(4, 2)

		warped = self.transform_demo(0, imutils.perspective.four_point_transform, gray, need_contour)
		output = self.transform_demo(0, imutils.perspective.four_point_transform, image, need_contour)

		thresh = self.transform_demo(0, lambda *a : cv2.threshold(*a)[1], warped, 150, 255, cv2.THRESH_BINARY)		# cv2.THRESH_OTSU

		cnts = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)		# RETR_EXTERNAL RETR_LIST CHAIN_APPROX_SIMPLE
		for i in range(len(cnts[0])):
			c = cnts[0][i]
			hier = cnts[1][0][i]	# см findContours doc
			print(i, c, hier, sep='\n--\n', end='\n**********\n')
		cv2.drawContours(output, cnts[0], -1, Red, 2); self.show_image(output); return
		digit_cnts = []
		for i in range(len(cnts[0])):
			c = cnts[0][i]
			hier = cnts[1][0][i]	# см findContours doc
			#print(i, c, hier, sep='\n--\n', end='\n**********\n')
			peri = cv2.arcLength(c, True)
			approx = cv2.approxPolyDP(c, 0.02 * peri, True)
			if len(approx) > 4 and hier[3] == -1:
				digit_cnts.append(c)
		print(digit_cnts)
		cv2.drawContours(output, digit_cnts, -1, Red, 2)
		self.show_image(output)


	def transform_demo(self, show_flag, transform_method, image, *args, **kwargs):
		tranform_image = transform_method(image, *args, **kwargs)
		if show_flag:
			self.show_image(tranform_image, f'{transform_method.__name__} {args} {kwargs}')
		return tranform_image

	def show_image(self, image, name=None, d_action = None):
		if name is None:
			name = f'image_{int(time.time())}'
		cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)	# cv2.WINDOW_NORMAL итд
		cv2.imshow(name, image)
		while 1:
			key = cv2.waitKey(1000)
			if key == 27:
				cv2.destroyWindow(name)
				break		# непонятно как проверять что окно закрыли как-то ещё
			elif key == ord('d') and d_action:
				d_action()
				cv2.imshow(name, image)


if __name__ == '__main__':
	# %run /home/anton/_my/common_rep/py3/education/number_recognition2.py ~/temp/tetravex_numbers.png
	ret = App().main()
