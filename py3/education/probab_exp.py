#!/usr/bin/env python3

# эксперименты навеянные простым вопросом в книжке "Николенко Глубокое обучение, погружение в мир нейронных сетей"
# какова будет точная вероятность получить ровно 500 решек из 1000 бросаний честной монеты?
# пытаемся решить в более общем виде и заодно экспериментируем с matplotlib

# полезное про matplotlib
# https://github.com/whitehorn/Scientific_graphics_in_python

# пример встраивания matplotlib в tkinter
# https://matplotlib.org/stable/gallery/user_interfaces/embedding_in_tk_sgskip.html


import math
import itertools
import sys
import fractions

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure

from baseapp import BaseTkApp
import uTkinter as u



# вероятности получить r решек при n бросках
# возвращаем list из n элементов, в r-м элементе - вероятность получить r решек
# -----

# способ в лоб, для больших n - не работает, 25 - уже долго считает
# считаем сколько исходов всего и для каждого возможного исхода считаем сколько в нем решек (1 в двоичном представлении числа)
def calc_probabilities_naive(n):
	ret = [0 for r in range(n+1)]

	total_q = 2**n
	for i in range(total_q):
		q = f'{i:b}'.count('1')					#  орел или решка? - head[s] or tail[s]?
		ret[q] += 1

	for i, q in enumerate(ret):
		ret[i] /= total_q

	return ret

# -----
# и нормальный способ, комбинаторика рулит
# пронумеруем каждое из n испытаний, каждый интересующий нас случай выпадения r решек - это группа из r элементов номеров испытаний
# количество сочетаний без повторений n элементов группами по r: С = n! / (r! * (n - r)!)
# +
# используем fractions, чтобы считать n больше 1023. 1/2**1024 - ок, но 1./2**1024 -> OverflowError
# оно конечно для больших n тормозит (10000 я не дождался), но хоть работает
# эксперимент показал, что float(fr) == fr.numerator/fr.denominator
Fr = fractions.Fraction
#
def calc_probabilities(n):
	total_q = Fr(2**n)

	n_fact = Fr(math.factorial(n))
	def comb(r): return Fr(n_fact, math.factorial(r)*math.factorial(n - r))

	return [float(comb(r)/total_q) for r in range(n+1)]

def calc_probabilities_no_fractions_backup(n):
	total_q = 2**n

	n_fact = math.factorial(n)
	def comb(r): return n_fact/(math.factorial(r)*math.factorial(n - r))

	return [comb(r)/total_q for r in range(n+1)]



class App(BaseTkApp):
	def add_arguments(self):
		self.parser.add_argument('-t', '--test_only', action='store_true', help='Только тесты, проверяем умный метод наивным')
		self.parser.add_argument('-f', '--filter', type=float, default=1e-6, help='Отфильтруем для графика значения вероятности меньшие этого аргумента')

	def app_init(self):
		if self.args.test_only:
			for i in range(1, 21):
				prob1 = calc_probabilities(i)
				prob2 = calc_probabilities_naive(i)
				if prob1 != prob2:
					raise RuntimeError(f'{prob1} != {prob2}')
				if not math.isclose(sum(prob1), 1):
					raise RuntimeError(sum(prob1))
				print(i, 'checked', prob1)
			sys.exit(0)

	def create_gui(self):
		root = u.uTk(u'Даешь очевидное вероятное!', 800, 600)

		f1 = u.uFrame(root, gmArgs={'side':u.TOP, 'expand':u.YES, 'fill':u.BOTH})
		f2 = u.uFrame(root, gmArgs={'side':u.TOP, 'expand':u.NO, 'fill':u.X})

		u.uButton(f2, self.calculate, 'Calc', gmArgs={'side':u.LEFT})
		u.x_spacer(f2, 100)

		self.quantity_entry = u.uEntry(f2, 5, u.IntVar, #callback=lambda *a:self.calculate(),
									gmArgs={'side':u.LEFT, 'expand':u.YES, 'fill':u.X})
		def change_entry(dlt):
			self.quantity_entry.setVar(self.quantity_entry.getVar() + dlt)
		u.uButton(f2, lambda: change_entry(1), '\u25b2', gmArgs={'side':u.LEFT}, width=1)		# '\u2303'
		u.uButton(f2, lambda: change_entry(-1), '\u25bc', gmArgs={'side':u.LEFT}, width=1)

		self._init_plot(f1)

		root.bind('<Return>', lambda *a: self.calculate())
		root.bind('<Up>', lambda *a: change_entry(1))
		root.bind('<Down>', lambda *a: change_entry(-1))

		self.calculate()

		return root

	# тут настройка matplotlib для tkinter
	def _init_plot(self, master, use_toolbar = True):
		fig = Figure()	# figsize=(1, 1), dpi=100		# большой figsize перекрывает все остальные виджеты, большой dpi делает странное
		self.axes = fig.add_subplot()

		self.canvas = FigureCanvasTkAgg(fig, master=master)

		if use_toolbar:
			toolbar = NavigationToolbar2Tk(self.canvas, master, pack_toolbar=False)
			toolbar.update()
			toolbar.pack(side=u.BOTTOM, fill=u.X)

		self.canvas.get_tk_widget().pack(side=u.TOP, fill=u.BOTH, expand=u.YES)

	# тут перерисовка matplotlib объекта
	def _redraw_axes(self, x, height):
		self.axes.clear()

		# собственно рисуем в axes, другие варианты: self.axes.plot, self.axes.hist etc
		# ---
		bar = self.axes.bar(x, height)		# , tick_label=tick_label
		#
		# https://matplotlib.org/2.0.2/examples/api/barchart_demo.html
		for rect, label in zip(bar, x):
			self.axes.text( rect.get_x() + rect.get_width()/2,
							rect.get_height()/2,
							label,
							ha='center', va='center', #alignments
							color = 'white',
							fontsize = 'small',
							rotation = 90)
		# ---

		self.canvas.draw()


	def calculate(self):
		try:
			q = self.quantity_entry.getVar()
		except:
			self.set_status_bar('No int')
			return

		probalities = calc_probabilities(q)

		bars = ((i, p) for i, p in enumerate(probalities) if p > self.args.filter)
		# сюда можно засунуть и label
		# zip - работает в обе стороны! см list(zip(*zip('qwerty', [0,1,2,3,4,5])))

		self._redraw_axes(*zip(*bars))


if __name__ == '__main__':
	App().mainloop()
