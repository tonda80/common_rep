#!/usr/bin/env python3

# задачи со степик курса Алгоритмы: теория и практика. Методы https://stepik.org/course/217/


# 2.2 Числа Фибоначчи

# небольшое число Фибоначчи
# Дано целое число 1≤n≤40 1 \le n \le 40 1≤n≤40, необходимо вычислить n n n-е число Фибоначчи (напомним, что F0=0 F_0=0 F0​=0, F1=1 F_1=1 F1​=1 и Fn=Fn−1+Fn−2 F_n=F_{n-1}+F_{n-2} Fn​=Fn−1​+Fn−2​ при n≥2 n \ge 2 n≥2)
def fib(n):
    if n < 2:
        return n
    n -= 2
    fn1 = 1
    fn = 1
    while n > 0:
        fn, fn1 = fn + fn1, fn
        n -= 1
    return fn

def main():
    n = int(input())
    print(fib(n))

# последняя цифра большого числа Фибоначчи
# Дано число 1≤n≤107 1 \le n \le 10^7 1≤n≤107, необходимо найти последнюю цифру n n n-го числа Фибоначчи.
def fib_digit(n):
    if n < 2:
        return n
    n -= 2
    last_digit_n1 = 1
    last_digit_n = 1
    while n > 0:
        last_digit_n, last_digit_n1 = (last_digit_n + last_digit_n1)%10, last_digit_n
        n -= 1
    return last_digit_n


def main():
    n = int(input())
    print(fib_digit(n))



if __name__ == '__main__':
	App().main()
