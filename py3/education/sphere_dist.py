#!/usr/bin/env python3

# расстояние между точками на сфере
# https://medium.com/@congyuzhou/%D1%80%D0%B0%D1%81%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D0%B5-%D0%BC%D0%B5%D0%B6%D0%B4%D1%83-%D0%B4%D0%B2%D1%83%D0%BC%D1%8F-%D1%82%D0%BE%D1%87%D0%BA%D0%B0%D0%BC%D0%B8-%D0%BD%D0%B0-%D0%BF%D0%BE%D0%B2%D0%B5%D1%80%D1%85%D0%BD%D0%BE%D1%81%D1%82%D0%B8-%D0%B7%D0%B5%D0%BC%D0%BB%D0%B8-a398352bfbde
# https://planetcalc.ru/73/
# https://en.wikipedia.org/wiki/Great-circle_distance

from math import *

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('lat1')
		self.parser.add_argument('lon1')
		self.parser.add_argument('lat2')
		self.parser.add_argument('lon2')
		self.parser.add_argument('-f', '--formula', default = 'g', choices = ['g', 'c'], help='Формула расчета: гаверсинусов, косинусов')

	def main(self):
		if 1/2 != 0.5:
			raise RuntimeError('use python3')

		earth_radius = 6371.0088		# км по WGS 84

		def g2r(x): return x*pi/180
		coords = [g2r(float(getattr(self.args, x))) for x in ('lat1', 'lon1', 'lat2', 'lon2')]

		if self.args.formula == 'g':
			formula = self.haversin_formula
		angle = formula(*coords)

		print(earth_radius*angle)


	# формула гаверсинусов, углы в радианах
	def haversin_formula(self, lat1, lon1, lat2, lon2):
		d_lat = abs(lat1 - lat2)
		d_lon = abs(lon1 - lon2)
		def hav(x): return sin(x/2)**2

		return 2*asin(sqrt( hav(d_lat) + cos(lat1)*cos(lat2)*hav(d_lon) ))

	def cos_formula(self):
		pass #TODO


if __name__ == '__main__':
	App().main()
