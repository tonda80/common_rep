#!/usr/bin/env python3

# изучаем обратную польскую нотацию
# написано для переноса на с++, поэтому например есть функии is* возвращающие False в базовом RpnToken
#
# алгоритм из вики https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C
#	Пока есть ещё символы для чтения:
			# Читаем очередной символ.
			# Если символ является числом или постфиксной функцией (например, ! — факториал), добавляем его к выходной строке.
			# Если символ является префиксной функцией (например, sin — синус), помещаем его в стек.
			# Если символ является открывающей скобкой, помещаем его в стек.
			# Если символ является закрывающей скобкой:
				# До тех пор, пока верхним элементом стека не станет открывающая скобка, выталкиваем элементы из стека в выходную строку. При этом открывающая скобка удаляется из стека, но в выходную строку не добавляется. Если стек закончился раньше, чем мы встретили открывающую скобку, это означает, что в выражении либо неверно поставлен разделитель, либо не согласованы скобки.
					# Если существуют разные виды скобок, появление непарной скобки также свидетельствует об ошибке. Если какие-то скобки одновременно являются функциями (например, [x] — целая часть), добавляем к выходной строке символ этой функции.
			# Если символ является бинарной операцией о1, тогда:
			# 1) пока на вершине стека префиксная функция…
					# … ИЛИ операция на вершине стека приоритетнее o1
					# … ИЛИ операция на вершине стека левоассоциативная с приоритетом как у o1
				# … выталкиваем верхний элемент стека в выходную строку;
			# 2) помещаем операцию o1 в стек.
#	Когда входная строка закончилась, выталкиваем все символы из стека в выходную строку. В стеке должны были остаться только символы операций; если это не так, значит в выражении не согласованы скобки.



import cmd
import math

from baseapp import BaseConsoleApp



class G:
	log = None


class RpnCalculatorError(RuntimeError): pass


class StrViewer:
	def __init__(self, s):
		self.str = s
		self.upper = len(s)
		self.pos = 0
	def not_viewed(self):
		return self.pos < self.upper
	def inc(self, v):
		self.pos += v
		assert self.pos <= self.upper
	def curr(self, q = None):
		stop = self.pos + q if q is not None else None
		if stop is not None and stop > self.upper:
			raise ValueError(f'[StrViewer.curr] {self.str}; start={self.pos}, stop={"None" if stop is None else stop}')
		return self.str[self.pos:stop]

	def __str__(self):
		return f'{self.str}:pos={self.pos}'


class Priority:
	p0 = 0
	plus = 10
	mult = 20
	func = 30


class RpnToken:
	def is_operation(self):
		return False
	def is_operand(self):
		return False
	def is_variable(self):
		return False
	def is_(self, what):
		return False

	@staticmethod
	def create(str_v):
		for C in (RpnSymbol, RpnOperation, RpnOperand):		# RpnOperand строго последним, так как там числа и иначе неправильно обработается -2, +7 итп (но таки есть TODO про числа)
			token = C.create(str_v)
			if token:
				return token

	def __init__(self, sign):
		self.sign = sign		# это поле на усмотрение класса наследника, в базовом используется только для __str__ так что если совпадут тут - не страшно

	def __str__(self):
		return f'{self.sign}'
	def __repr__(self):
		return self.__str__()


class RpnSymbol(RpnToken):
	@staticmethod
	def create(str_v):
		c = str_v.curr(1)
		if c in '(),':
			str_v.inc(1)
			return RpnSymbol(c)

	def is_(self, what):
		return self.sign == what


class RpnOperation(RpnToken):
	@staticmethod
	def create(str_v):
		return BinOperation.create(str_v) or FunctionOperation.create(str_v)

	def is_operation(self):
		return True

	def __init__(self, sign, priority, q_arg):
		RpnToken.__init__(self, sign)
		self.priority = priority
		self.q_arg = q_arg

	def check_args(self, args):
		assert len(args) == self.q_arg, '[RpnOperation.eval] wrong argument quantity'

class BinOperation(RpnOperation):
	@staticmethod
	def create(str_v):
		c = str_v.curr(1)
		priority = None
		if c in '*/':
			priority = Priority.mult
		elif c in '+-':					# TODO как-то понимать что -2 это число а не операция и число
			priority = Priority.plus
		if priority is not None:
			str_v.inc(1)
			return BinOperation(c, priority, 2)

	def eval(self, args):
		self.check_args(args)
		if self.sign == '*':
			return args[0] * args[1]
		elif self.sign == '/':
			return args[0] / args[1]
		elif self.sign == '+':
			return args[0] + args[1]
		elif self.sign == '-':
			return args[0] - args[1]

class FunctionOperation(RpnOperation):
	@staticmethod
	def create(str_v):
		fmap = {
			'sin': SinOperation,
			'cos': CosOperation,
			'max3': Max3Operation,		# TODO? нет переменного числа аргументов, но по идее можно сделать задание аргументов в момент формирования rpn выражения по скобкам
		}
		for k,v in fmap.items():
			if str_v.curr().startswith(k):
				str_v.inc(len(k))
				return v.create()

class SinOperation(FunctionOperation):
	@staticmethod
	def create():
		return SinOperation('sin', Priority.func, 1)

	def eval(self, args):
		self.check_args(args)
		return math.sin(args[0]*2*math.pi/360)

class CosOperation(FunctionOperation):
	@staticmethod
	def create():
		return CosOperation('cos', Priority.func, 1)

	def eval(self, args):
		self.check_args(args)
		return math.cos(args[0]*2*math.pi/360)

class Max3Operation(FunctionOperation):
	@staticmethod
	def create():
		return Max3Operation('max3', Priority.func, 3)

	def eval(self, args):
		self.check_args(args)
		return max(args)


class RpnOperand(RpnToken):
	@staticmethod
	def create(str_v):
		return RpnVar.create(str_v) or RpnNumber.create(str_v)

	def is_operand(self):
		return True

	def __init__(self, sign, value):
		RpnToken.__init__(self, sign)
		self._value = value

	def value(self):
		if self._value is None:
			raise RpnCalculatorError(f'non initialized operand \'{self.sign}\'')
		return self._value

	def __str__(self):
		return '?' if self._value is None else f'{self._value}'

class RpnVar(RpnOperand):
	@staticmethod
	def create(str_v):
		c = str_v.curr(1)
		if c in 'x':
			str_v.inc(1)
			return RpnVar(c, None)

	def is_variable(self):
		return True

	def set_value_from(self, var_dict):
		self._value = var_dict.get(self.sign)

class RpnNumber(RpnOperand):
	@staticmethod
	def create(str_v):
		num_type = int
		i = 0
		try:
			while 1:
				s = str_v.curr(i+1)
				if s[-1] == '.':
					num_type = float
				num = num_type(s)
				i += 1
		except ValueError:
			pass

		if i > 0:
			str_v.inc(i)
			return RpnNumber('num', num)



class RpnExpression:
	def __init__(self, in_expr):
		str_expr = ''.join(c for c in in_expr if c not in '\t ')
		G.log.debug(f'expression: {str_expr}')

		if not str_expr:
			raise RpnCalculatorError('empty expression string')

		self.expr = []
		self.vars = []
		stack = []
		for token, pos in self.token_generator(str_expr):
			G.log.debug(f'token: {token} {token.is_operand()} {token.is_operation()}')

			if token.is_operand():
				self.expr.append(token)
				if token.is_variable():
					self.vars.append(token)
			elif token.is_operation():
				while stack and stack[-1].is_operation() and stack[-1].priority >= token.priority:
					self.expr.append(stack.pop())
				stack.append(token)
			elif token.is_('('):
				stack.append(token)
			elif token.is_(')'):
				while stack:
					stack_token = stack.pop()
					if stack_token.is_('('):
						break
					self.expr.append(stack_token)
				else:
					raise RpnCalculatorError(f'\'{str_expr}\' inconsistent opening bracket in {pos}')
			elif token.is_(','):
				pass	# только разделитель аргументов функции, может как то валидировать?
			else:
				raise AssertionError('unknown token')

		while stack:
			stack_token = stack.pop()
			if not stack_token.is_operation():
				raise RpnCalculatorError(f'\'{str_expr}\' malformed expression: no operation into stack (inconsistent brackets?)')
			self.expr.append(stack_token)

		self.str = str_expr
		G.log.debug(f'rpn expression {self.expr}')

	def token_generator(self, str_expr):
		str_v = StrViewer(str_expr)
		while str_v.not_viewed():
			el = RpnToken.create(str_v)
			if el is None:
				raise RpnCalculatorError(f'[RpnExpression.token_generator] \'{str_v}\' cannot get next element')
			yield el, str_v.pos

	def eval(self, **var_dict):
		for v in self.vars:
			v.set_value_from(var_dict)

		stack = []
		for token in self.expr:
			if token.is_operand():
				stack.append(token.value())
			elif token.is_operation():
				if len(stack) < token.q_arg:
					raise RpnCalculatorError(f'\'{self.str_expr}\' malformed expression: no operands for {token}')
				args = stack[-token.q_arg:]
				del stack[-token.q_arg:]
				stack.append(token.eval(args))
			else:
				raise AssertionError(f'unknown token {token}')

		if len(stack) != 1:
			raise RpnCalculatorError(f'\'{self.str_expr}\' malformed expression : no result after evaluation stack={stack}')
		return stack[0]



class App(BaseConsoleApp, cmd.Cmd):
	prompt = '> '

	def log_format(self): return {'format': '[%(levelname)s] %(message)s'}

	def add_arguments(self):
		self.parser.add_argument('-d', '--debug_mode', action='store_true')

	def main(self):
		G.log = self.log

		if self.args.debug_mode:
			self.do_test_cases_set1('')
			return

		cmd.Cmd.__init__(self)
		try:
			self.cmdloop()
		except KeyboardInterrupt:
			pass

	def test_case(self, expected_result, expr, **var_dict):
		if isinstance(expr, str):
			expr = RpnExpression(expr)
		result = expr.eval(**var_dict)
		var_dict_info = f'for {var_dict}' if var_dict else ''
		print(f'{"OK" if result == expected_result else "FAIL"}\t{expr.str} = {result} {var_dict_info}')

	def do_test_cases_set1(self, arg):
		'Тестирует набор кейсов 1'
		self.do_test_case('(1 + 4)*2 + 3*(100 -97) - 4 = 15')
		self.test_case(7, '1 + 2*3')
		self.test_case(12, '12/3 + (8-4) + 2*2')
		self.test_case(math.cos(math.pi/4), 'cos(45)')
		self.test_case(1, 'sin(90)')
		self.test_case(35.5, 'sin(90) + 11*3 + 3*cos(60)')
		self.test_case(4, 'max3(3,1,7)*2 - 100/max3(2, 10, 4)')
		self.test_case(4, 'max3(3,1,7)*2 - 100/max3(2, 10, 4)')
		self.test_case(12, 'x + 4', x=8)
		expr = RpnExpression('x*x - x*4')
		self.test_case(21, expr, x=7)
		self.test_case(45, expr, x=9)
		#self.test_case(, '')

		#self.do_quick_calc('1/(3 + (8-4) + 2*2')
		#self.do_quick_calc('1/0.0')

	def do_q(self, arg):
		'Выход'
		return True

	def do_quick_calc(self, arg):
		'Вычислить выражение без аргументов'
		expr = RpnExpression(arg)
		print(f'{expr.str} = {expr.eval()}')

	def do_test_case(self, arg):
		'Вычислить выражение без аргументов и сравнить с заданным. Формат "expr=result"'
		assert arg.count('=') == 1, 'test_case format error'
		expr_str, _, expected_res_str = arg.partition('=')
		self.test_case(float(expected_res_str), expr_str)



if __name__ == '__main__':
	App().main()
