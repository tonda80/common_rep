#!/usr/bin/env python3

# алгоритм определения расстояниz до отрезка


from baseapp import BaseTkApp
import uTkinter as utk
import mix_utils


# TODO? вдруг тут что получится мало мальски переиспользуемое, перенести в grid_field (переименовав оное)
class DrawField(utk.uCanvas):
	def __init__(self, *a, **kw):
		super().__init__(*a, **kw)

	def draw_oval(self, x, y, r, color, tag, **kw):
		self.create_oval(x-r, y-r, x+r, y+r, fill=color, outline=color, tags=(tag,), **kw)

	def draw_line(self, v0, v1, color, tag, **kw):
		self.create_line(v0.x, v0.y, v1.x, v1.y, fill=color, tags=(tag,), **kw)


class App(BaseTkApp):
	polygon_color = 'green'
	polygon_point_size = 2
	polygon_tag = 'polygon'

	point_color = 'orange'
	point_size = 4
	point_tag = 'point'

	clos_point_color = 'red'

	def app_init(self):
		self.segment = []
		self.point = None

	def create_gui(self):
		root = utk.uTk('Point to segment', 640, 480)

		f = utk.uFrame(root, gmArgs={'expand':utk.YES, 'fill':utk.BOTH})
		self.field = DrawField(f)
		self.field.bind('<Button-1>', self.left_mouse_click_handler)
		self.field.bind('<Button-3>', self.right_mouse_click_handler)

		f = utk.uFrame(root, gmArgs={'expand':utk.NO, 'fill':utk.X})
		self.lbl_result = utk.uLabel(f, '', gmArgs={'fill':utk.X, 'side':utk.LEFT})
		utk.uButton(f, self.btn_clear_handler, 'Clear', gmArgs = {'side':utk.RIGHT})

		return root

	def btn_clear_handler(self):
		self.segment.clear()
		self.point = None
		self.field.delete(utk.ALL)

	def right_mouse_click_handler(self, ev):
		self.point = mix_utils.Struct(x=ev.x, y=ev.y)
		self.redraw()

	def left_mouse_click_handler(self, ev):
		new_point = mix_utils.Struct(x=ev.x, y=ev.y)
		if len(self.segment) < 2:
			self.segment.append(new_point)
		else:
			if (point_point_dist(self.segment[0], new_point) < point_point_dist(self.segment[1], new_point)):
				self.segment[0] = new_point
			else:
				self.segment[1] = new_point
		self.redraw()

	def redraw(self):
		self.field.delete(utk.ALL)

		for v in self.segment:
			self.field.draw_oval(v.x, v.y, self.polygon_point_size, self.polygon_color, self.polygon_tag)
		if len(self.segment) > 1:
			self.field.draw_line(self.segment[0], self.segment[1], self.polygon_color, self.polygon_tag)

		if self.point:
			self.field.draw_oval(self.point.x, self.point.y, self.point_size, self.point_color, self.point_tag)

		self.calculate()

	def calculate(self):
		if len(self.segment) < 2 or not self.point:
			return

		closest_point = segment_closest_point(self.segment[0], self.segment[1], self.point)
		dist = point_point_dist(closest_point, self.point)

		self.field.draw_oval(closest_point.x, closest_point.y, self.point_size, self.clos_point_color, self.point_tag)
		self.lbl_result.set_text(f'{dist:.2f}  p={_p2s(self.point)} s=[{_p2s(self.segment[0])} {_p2s(self.segment[1])}]')



def _p2s(point):
		return f'{point.x, point.y}'

# тут собственно алгоритмы
# ----------

def point_point_dist(a, b):
	return ((a.x - b.x)**2 + (a.y - b.y)**2)**0.5


# https://stackoverflow.com/a/2233538
# http://paulbourke.net/geometry/pointlineplane/
def segment_closest_point(a, b, p):
	sd2 = (a.x - b.x)**2 + (a.y - b.y)**2
	u = ((p.x - a.x)*(b.x - a.x) + (p.y - a.y)*(b.y - a.y))/sd2
	if u < 0: u = 0
	elif u > 1: u = 1

	x = a.x + u*(b.x - a.x)
	y = a.y + u*(b.y - a.y)

	return mix_utils.Struct(x=x, y=y)

def segment_point_dist(a, b, p):
	return point_point_dist(segment_closest_point(a, b, p), p)

def segment_point_dist2(xs0, ys0, xs1, ys1, xp, yp):
	return segment_point_dist(mix_utils.Struct(x=xs0, y=ys0), mix_utils.Struct(x=xs1, y=ys1), mix_utils.Struct(x=xp, y=yp))


if __name__ == '__main__':
	App().mainloop()
