#!/usr/bin/env python3

# ничего особенного, просто пытаемся получить равномерное распределение цветов из rgb пространства

import time
import itertools

from baseapp import BaseTkApp
import uTkinter as utk
#import mix_utils



class App(BaseTkApp):
	def log_format(self):
		return {'format': '[%(levelname)s] %(message)s'}

	def add_arguments(self):
		self.parser.add_argument('--n', default=256, type=int, help='Количество цветов')
		self.parser.add_argument('way', choices=('1', '2'), help='Метод генерации цвета, см colors_N методы')

	def create_gui(self):
		root = utk.uTk(minw=300, minh=300, createStatus=True)
		root.set_exit_by_escape()

		main_frame = utk.uFrame(root, borderwidth=0, relief=utk.FLAT, gmArgs = {'expand':utk.YES, 'fill': utk.BOTH})
		grid_layout = utk.GridLayout(main_frame, 0, int(self.args.n**0.5))
		for i, color in enumerate(App.__dict__[f'colors_{self.args.way}'](self)):
			f = utk.uFrame(main_frame, bg=color, borderwidth=0, relief=utk.FLAT, gmArgs = {'sticky':'wens'})
			f.bind('<Enter>', lambda ev, i=i, color=color: self.set_status_bar(f'{i} {color}'))
			#self.log.debug(f'{i:3}: {color}')
			time.sleep(1.5/self.args.n)		# непонятно, но без паузы (явной или print) тут ломается отрисовка, надо распахнуть например окно
		grid_layout.make_stretchy()


		return root

	def colors_1(self):
		# такое толком не работает, цвета выдает не разные
		# для 256 например, выдает только серые оттенки, все составляющие меняются одинаково
		for i in range(self.args.n):
			n = int(0xff_ff_ff/(self.args.n - 1)*i)
			r, g, b = (n >> 16) & 0xff, (n >> 8) & 0xff, n & 0xff
			yield utk.rgb(r, g, b)

	def colors_2(self):
		rn = gn = bn = int(self.args.n**(1/3))
		if (rn + 1)**3 == self.args.n:	# например 216, надо понять/вспомнить наконец как тут вообще правильно поступать
			rn = gn = bn = rn + 1
		elif rn*bn*(gn + 1) <= self.args.n:
			gn += 1			# по возможности добавили градаций зеленого
			if gn*bn*(rn + 1) <= self.args.n:
				rn += 1		# и красного
		self.log.info(f'rgb gradation for {self.args.n}: {rn} {gn} {bn}')
		# как-то надо придумать как выдавать оставшиеся цвета!! например 6*7*6 = 252 < 256

		for i, j, k in itertools.product(range(rn), range(gn), range(bn)):
			#print(i, j, k)
			r = int(0xff/(rn - 1)*i)
			g = int(0xff/(gn - 1)*j)
			b = int(0xff/(bn - 1)*k)
			yield utk.rgb(r, g, b)


if __name__ == '__main__':
	App().mainloop()
