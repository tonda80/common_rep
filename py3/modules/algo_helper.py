#!/usr/bin/env python3

# иногда (обычно перед собеседованиями, и ещё чаще после неудачных оных)
# хочется порешать задачки по алгоритмам и структурам данных
# тут планируется вспомогательный код для этого, типа загрузка тестовых структур (бинарного дерева) из строк
# так же есть идея написать простенкий gui для создания таких тестовых структур
#



from baseapp import BaseTkApp
import uTkinter as u



# узел бинарного дерева
class Node2:
	def __init__(self):
		self.left = self.right = self.data = None



class Editor(BaseTkApp):
	def create_gui(self):
		root = u.uTk('Struct editor', 800, 600, createStatus=True)

		menubar = u.uMenu(root)
		#
		new_menu = menubar.create_submenu('New')
		new_menu.add_command(label="Binary tree", command=self.new_binary_tree)

		self.canvas = u.uCanvas(root)

		return root


	def new_binary_tree(self):
		pass



if __name__ == '__main__':
	Editor().mainloop()
