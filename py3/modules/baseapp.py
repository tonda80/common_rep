#coding=utf8

import argparse
import logging
import os
import sys


class BaseApp(object):
	log = None

	def __init__(self, args=None):
		self.parser = self._create_parser()
		self._subparsers = None
		self.add_arguments()
		self.args = self.parser.parse_args(args)
		self.__args_actions()

		self.log = self._create_logger()
		self.__log_actions()

		self.app_init()

	def app_init(self):
		pass

	def _create_logger(self):
		logging.basicConfig(**self.log_format())
		return logging.getLogger(self.__class__.__name__)

	def set_log_level(self, level):
		self.log.setLevel(level)

	def set_root_log_level(self, level):
		logging.getLogger().setLevel(level)

	def log_format(self):
		# https://docs.python.org/3/library/logging.html#logging.basicConfig
		# https://docs.python.org/3/library/logging.html#logrecord-attributes
		#
		# минимальный вариант    -  {'format': '%(message)s'}
		# с уровнем логирования  -  {'format': '[%(levelname)s] %(message)s'}
		# с уровнем и именем     -  {'format': '%(name)s [%(levelname)s] %(message)s'}
		return {
			'format': '[%(asctime)s.%(msecs)03d][%(levelname)s] %(message)s',		# раньше отображение msec по дефолту не работало, теперь такое ‘2003-07-08 16:49:45,896’
			'datefmt': '%d %b %H:%M:%S'
		}

	def __log_actions(self):
		self.set_log_level(logging.DEBUG if self.args.verbose else logging.INFO)
		self.set_root_log_level(logging.WARNING)
		if BaseApp.log is None:
			BaseApp.log = self.log


	def _create_parser(self):
		parser = argparse.ArgumentParser(parents=self.get_parent_parsers())
		parser.add_argument('--verbose', '-v', action='store_true', help='Verbose output')
		return parser

	def get_parent_parsers(self):	# тут иногда надо вернуть парсеры других модулей, чтобы они могли вытащить свои аргументы (oauth2client.tools.argparser)
		return ()

	def add_subparser(self, arg_name, target_main, help=''):
		if self._subparsers is None:
			self._subparsers = self.parser.add_subparsers()		# required=True  c 3.7
		subparser = self._subparsers.add_parser(arg_name, help=help)
		subparser.set_defaults(target_main=target_main)			# https://docs.python.org/3/library/argparse.html#argparse.ArgumentParser.add_subparsers
		return subparser

	# запускает выбранный аргументами запуска target_main заданный в add_subparser
	# то что он существует проверили в __init__ -> __args_actions
	def sub_main(self):
		assert self._subparsers, 'call sub_main without add_subparser'
		self.args.target_main()

	def add_arguments(self):
		pass

	def __args_actions(self):
		# в add_arguments вызывали add_subparser, но при запуска ничего не выбрали, т.е. выполнять будет нечего
		# вроде в 3.7 появился required https://docs.python.org/3/library/argparse.html#argparse.ArgumentParser.add_subparsers, но он работает как-то неправильно, да и 3.6 ещё жива
		if self._subparsers and 'target_main' not in self.args.__dict__:
			print(self.parser.format_usage())		# format_help подробнее
			print('Run target is not defined. Exiting\n')
			sys.exit(1)

	def import_settings(self, path):
		from importlib import util as importlib_util
		# https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
		spec = importlib_util.spec_from_file_location('__app_settings', path)
		settings_module = importlib_util.module_from_spec(spec)
		spec.loader.exec_module(settings_module)	# тут под капотом импорт или релоад
		return settings_module

	def cleanup(self):
		pass

	def main(self):
		pass

	def start(self):
		try:
			ret = self.main()
		except SystemExit as e:
			raise	# log e.code?
		except KeyboardInterrupt:
			ret = 130
		except Exception as e:
			#self.log.critical(f'Exception: {e.__class__.__name__} "{e}". Exiting.')
			raise
		finally:
			self.cleanup()

		return ret


class BaseConsoleApp(BaseApp):
	pass


class BaseTkApp(BaseApp):
	def __init__(self, args=None, exit_by_escape = True, auto_clear_status_bar = True):
		super().__init__(args)

		self._kept_uvars = {}
		self.root = self.create_gui()	# создаем Tk gui, должен вернуть uTk

		if exit_by_escape:
			self.root.set_exit_by_escape()

		if auto_clear_status_bar and hasattr(self.root, 'statusBar'):	# очищаем статус бар кликом по окну
			self.root.bind_left_click(lambda e: self.clear_status_bar())

	# True в конструкторе uTk (self.root) для создания статус бара
	def set_status_bar(self, msg):
		self.root.statusBar.set(msg)
		self.root.update()
	def clear_status_bar(self):
		self.set_status_bar('')

	# инициализируем "авто"-сохраняемые uTkinter.VariableWrapper-ы (нужен self.config)
	def init_kept_uvar(self, u_var, name, default=None):
		'''
			u_var   - uTkinter.VariableWrapper object (ar just object with setVar\getVar methods)
			name    - config parameter name
			default - config parameter default value

		'''
		assert hasattr(self, 'config'), 'init_kept_uvar requires the config attribute'

		value = self.config.get(name)
		if hasattr(u_var, 'options'):				# для optionsMenu:
			if value not in u_var.options:				# проверим, что сохраненное значение все ещё имеет смысл
				value = None
			if default is None and len(u_var.options):	# и возьмем дефолт из 0-й опции
				default = u_var.options[0]
		if value is None:
			value = default
		if value is not None:
			u_var.setVar(value)
		self._kept_uvars[name] = u_var

	def _on_delete(self):
		if hasattr(self, 'config'):
			self.save_config()

	def save_config(self):
		for k,v in self._kept_uvars.items():
			#print('__deb save_config', k)
			self.config.set(k, v.getVar())
		self.config.save()

	# clipboard
	def clipboard_get(self):
		try:
			return self.root.clipboard_get()
		except:		# если в буфере картинка наблюдал тут исключение на убунте
			return ''

	def mainloop(self):
		' deprecated, use start (?) '
		return self.root.mainloop()

	def main(self):
		' use start (?) '
		self.root.set_delete_callback(self._on_delete)
		return self.root.mainloop()


# консольное приложение которое бегает по ФС
class BaseWalkerApp(BaseConsoleApp):
	def __init__(self, default_root=None, args=None):
		self._default_root = default_root
		super().__init__(args)

		# класс наследник объявляет нужные коллбеки для обработки, остальные делаем None
		# см WalkerApp ниже как пример
		for name in ('dir_callback', 'file_callback', 'dirnames_callback', 'extension_filter'):
			if name not in self.__class__.__dict__:
				setattr(self, name, None)

	def add_arguments(self):
		self.parser.add_argument('--root_dir', default=self._default_root, required=not self._default_root, help='Root directory for search')

	def walk(self):
		root_path = os.path.realpath(self.args.root_dir)
		if not os.path.isdir(root_path):
			raise RuntimeError(f'Directory "{root_path}" does not exist')
		self.log.info(f'Started search from "{root_path}"')

		for dirpath, dirnames, filenames in os.walk(root_path, onerror=self.on_walk_error):
			if self.dir_callback:
				for d in dirnames:
					self.dir_callback(os.path.join(dirpath, d))
			if self.file_callback:
				for f in filenames:
					if not self.extension_filter or self.extension_filter(os.path.splitext(f)[1].lower()):
						self.file_callback(os.path.join(dirpath, f))

			if self.dirnames_callback and dirnames:
				self.dirnames_callback(dirpath, dirnames)	# тут можно например фильтровать список поддиректорий для дальнейшего поиска

	def on_walk_error(self, err):
		raise err

	def main(self):
		self.walk()


if __name__ == '__main__':
	class App(BaseConsoleApp):
		def add_arguments(self):
			self.parser.add_argument('--arg1')
			self.parser.add_argument('--arg2')

		def main(self):
			self.log.info('I do very useful work!')


	class WalkerApp(BaseWalkerApp):
		def dirnames_callback(self, dirpath, dirnames):
			self.log.info(f'SD {dirpath}: {dirnames}')
			test_exclude = 'puttyHelper'
			if test_exclude in dirnames:
				self.log.info(f'  removal "{test_exclude}" from the following search')
				dirnames.remove(test_exclude)

		def dir_callback(self, path):
			self.log.info('D %s'%path)
			if not os.path.isdir(path):
				RuntimeError('walk error')

		def file_callback(self, path):
			self.log.info('F %s'%path)
			if not os.path.isfile(path):
				RuntimeError('walk error')

		def extension_filter(self, ext):
			return ext != '.py'
			#return ext == '.json'


	# заготовка для типичного tk app
	class TkApp(BaseTkApp):
		def app_init(self):
			app_name = '{app_name}_{date_label}'
			default_config = {}
			# надо from commonconfig import CommonConfig
			#self.config = CommonConfig(app_name, **default_config)
			self.log.info(u'это вызывается в конструкторе, тут можно ещё добавить какую то раннюю инициализацию')
			# save_config вызываем в _on_delete (в cleanup не работает, виджетов уже нет) и еще добавляй куда надо (сразу после изменения)

		def create_gui(self):
			root = uTkinter.uTk('{title}', 100, 100, createStatus=True)
			# тут создаем нужный gui
			uTkinter.uButton(root, lambda: self.set_status_bar('Hello!'), 'press me')
			# и настраиваем поведение окошка
			root.set_delete_callback(self.save_config)
			return root		# !

	import mix_utils
	import uTkinter

	print('-- App --')
	App(args=()).main()

	print('-- WalkerApp --')
	WalkerApp(args=('--root_dir', mix_utils.path_to(__file__, '../../py/utils'), '-v')).start()

	print('-- TkApp --')
	TkApp().mainloop()
