#!/usr/bin/env python3
# coding=utf8


# разные полезные мелочи
# функции для работы в ipython

import os
import time
import datetime
import random


def bits(num):
	# Возвращает позиции единичных битов
	def gen(num):
		pos = 0
		while num:
			if 1 & num:
				yield pos
			num >>= 1
			pos += 1
	return list(gen(num))


# таймштамп => строку
# если надо локальное время: tz = None
# или можно передать tzinfo объект, например datetime.timezone https://docs.python.org/3/library/datetime.html#datetime.timezone
def ts2str(ts, tz = datetime.timezone.utc, fmt = None):
	dt = datetime.datetime.fromtimestamp(ts, tz)
		# the recommended way to create an object representing the current time in UTC is by calling datetime.now(timezone.utc)
	if fmt is None:
		return dt.isoformat()
	return dt.strftime(fmt)		# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior

# строку => datetime
# tz, fmt как в ts2str
def str2dt(s, tz = datetime.timezone.utc, fmt = None):
	if fmt is None:
		if hasattr(datetime.datetime, 'fromisoformat'):		# c 3.7
			dt = datetime.datetime.fromisoformat(s)
		else:
			try:
				dt = datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%z')
					# '1970-01-01T00:00:00+0000' такое выдает isoformat
			except ValueError:
				dt = datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S')		# и попробуем без смещения
	else:
		dt = datetime.datetime.strptime(s, fmt)
	if tz is not None and dt.tzinfo is None:	# интересует не локальное время и во входной строке инфы о зоне нет (как-то сложно?)
		dt = dt.replace(tzinfo=tz)
	return dt

# строку => таймштамп
# tz, fmt как в ts2str
def str2ts(s, tz = datetime.timezone.utc, fmt = None):
	return str2dt(s, tz, fmt).timestamp()


# проверка расширения
def check_ext(path, ext, ignore_case=True):
	path_ext = os.path.splitext(path)[1]
	if ignore_case:
		return path_ext.lower() == ext.lower()
	return path_ext == ext


# получаем путь к файлу относительно скрипта
# useful usage: path_to(__file__, 'relative/path/to/smth')
def path_to(root_file, nix_rel_path):
	return os.path.realpath(os.path.join(os.path.dirname(root_file), *nix_rel_path.split('/')))


# возвращает имя несуществующего файла (readme(12).txt) в директории dest_dir на основе src_path
def new_destination_path(src_path, dest_dir, *, limit = 100, sep = '/'):
	src = src_path.rpartition(sep)[2]		# sep нельзя брать из os потому что можем на винде разбивать linux путь
	if src and os.path.isdir(dest_dir):
		src_root, src_ext = os.path.splitext(src)
		dest_dir2 = os.path.realpath(dest_dir)
		for i in range(limit):
			suff = f'({i})' if i > 0 else ''
			dest = os.path.join(dest_dir2, f'{src_root}{suff}{src_ext}')
			if not os.path.isfile(dest):
				return dest


# получаем директорию скрипта
# usage curr_dir(__file__)
def curr_dir(file_):
	return os.path.realpath(os.path.dirname(file_))


# пробегаем по файлу TODO filter (по диапазону строк, началу строки, регэксп), подумать можно ли задавать все это красиво
def iter_file(path, clb, strip_trail=True):
	with open(path) as file_:
		for l in file_:
			if strip_trail:
				l = l.rstrip()
			clb(l)


# проверка концепции, типа структура легко задаваемая и понятно печатаемая
# посмотрим, есть ли смысл.. стоит делать функцию криейтор (staticmethod в наследнике) def create(a1, a2): return Struct(a1=a1, a2=a2)
class Struct:
	def __init__(self, **kw):
		self.__dict__.update(kw)

	def __str__(self):
		return str(self.__dict__)



# выводит таймштампы выполнения, с возможностью выводить каждое N-е
class DebugTimer:
	def __init__(self, one_of = None):
		self.time = time.time()
		self.counter = 0
		self.one_of = one_of

	def mark(self, label):
		t = time.time()
		self.counter += 1

		if self.one_of is not None and self.counter % self.one_of != 0:
			return

		print(f'[DebugTimer] "{label}" {t - self.time}:e {self.counter}')		# .4f
		self.time = t


# pop random item from list
def pop_rand(lst):
	return lst.pop(random.randint(0, len(lst)-1))


def _demo(test_str):
	try:
		res = eval(test_str)
	except Exception as e:
		res = f'Exception {e.__class__.__name__}: {e}'
	print(f'{test_str}  =>  {res}')


if __name__ == '__main__':
	#_demo('1/0')

	_demo('check_ext("/tmp/FILE.JSON", ".json")')
	_demo('check_ext("/tmp/FILE.JSON", ".json", False)')

	_demo('path_to(__file__, "../../trash_rep")')

	class DemoStruct(Struct): pass
	ds = DemoStruct(x=1, y=2)
	_demo('ds.x')
	_demo('ds.y')

	_demo('new_destination_path("/bla/bla/t.py", "/home/anton/temp")')
	_demo('new_destination_path("uu.py", "no/such/dir")')
	_demo('new_destination_path("u.py", "/home/anton/temp")')
