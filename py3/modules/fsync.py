#!/usr/bin/env python3


# базовые классы для синхронизации файлов
# мигрировано со 2-й версии и доработано


import fnmatch
import os
import subprocess
import logging



# -- Intefaces


class IAppObject:
	def __init__(self):
		# атрибуты задаем в IApp, чтобы наследники использовали дефолтный конструктор, те не было контракта
		self.log = None
		self.extra_params = None		# что-то кастомное для наследников


class IGetter(IAppObject):
	def __init__(self):
		IAppObject.__init__(self)
		self.root = None			# directory for analysis
		self.excluder = None

	# glob patterns for excluding (str, tuple or list)
	def set_exclude_patterns(self, exclude_patterns):
		if exclude_patterns is None:
			return

		if isinstance(exclude_patterns, str):
			exclude_patterns = (exclude_patterns, )

		def excluder(name):
			for p in exclude_patterns:
				if fnmatch.fnmatch(name, p):		# было fnmatchcase, но зачем?
					return False
			return True

		self.excluder = excluder


	def get_files(self):			# filtered by _find_files
		return filter(self.excluder, self._find_files())

	def _find_files(self):			# returns iterable file pathes (relative from root)
		raise NotImplementedError


class ISender(IAppObject):
	def __init__(self):
		IAppObject.__init__(self)
		self.connection = None			# object with settings of connection (e.g.: host, port, login, password )

	def open(self):						# opens connection, may be called for already opened connection
		raise NotImplementedError

	def send(self, src, trg):			# sends src to trg
		raise NotImplementedError


class IApp:

	# -- config mandatory attributes
	# Getter				type of used Getter
	# Sender				type of used Sender
	# connection			its attributes depends on corresponding Sender (e.g.: host, port, user, password)
	# source_root			root directory of the source
	# dest_root				root directory of the destination
	#
	# -- config optional attributes
	# exclude_patterns		glob patterns for excluding (str, tuple or list) E.g.: '.git' or ['*.txt', 'bad?.bmp']
	# sender_params			some specific for concret getter/sender parameters (e.g. perforcePath for PerforceGetter), they must not be transferred into base classes
	# getter_params			^^

	def __init__(self, config, log = None):
		self.config = config
		self.log = log if log else logging.getLogger(self.__class__.__name__)

		#
		self.sender = config.Sender()
		self.sender.log = self.log
		self.sender.extra_parameters = getattr(config, 'sender_params', None)
		self.sender.connection = config.connection

		#
		self.getter = config.Getter()
		self.getter.log = self.log
		self.getter.extra_parameters = getattr(config, 'getter_params', None)
		self.getter.root = config.source_root
		self.getter.set_exclude_patterns(getattr(config, 'exclude_patterns', None))

		#
		self.source_win_path = self.dest_win_path = False
		if '\\' in config.source_root:
			self.source_win_path = True
			self.log.info('Detected win separator for source files')
		if '\\' in config.dest_root:
			self.dest_win_path = True
			self.log.info('Detected win separator for destination files')

		#
		self.sent_change_times = {}		# src_full_path : {время изменения которое было отправлено}


	# TODO по идее надо бы и из вин слешей делать линуксовые, когда из винды на линукс шлем, но проблем вроде не было, а сейчас наверное и не понадобится никогда
	def src_full_path(self, f):
		path = self.config.source_root + '/' + f
		if self.source_win_path:
			path = path.replace('/', '\\')
		return path
	#
	def dest_full_path(self, f):
		path = self.config.dest_root + '/' + f
		if self.dest_win_path:
			path = path.replace('/', '\\')
		return path

	def get_files(self):
		return self.getter.get_files()

	# наследник, получает список вызывая get_files, опционально фильтрует (галочки), и вызывает get_files()
	def send_files(self, files):
		if files:
			self.sender.open()
			for f in files:
				sp = self.src_full_path(f)
				if self._need_to_send(sp):
					self.sender.send(sp, self.dest_full_path(f))

	def _need_to_send(self, f):
		change_time = self.change_time(f)		# если вдруг не хотим (не можем) проверять время изменения при отправке, пусть дочерний класс возвращает тут None
		if change_time is None:
			return True

		if change_time <= self.sent_change_times.get(f, -1):	# проверяем < на всякий случай, по факту всегда должно быть = (если только как-то руками отмотать время изменения файла?)
			return False

		#print(change_time, self.sent_change_times.get(f))
		self.sent_change_times[f] = change_time
		return True


	def change_time(self, f):
		return os.path.getmtime(f)


# -- Implementations


class FakeGetter(IGetter):
	def _find_files(self):
		return ('1.txt', 'dir1/subdir2/2.bmp', 'dir2/3.gif')

class FakeSender(ISender):
	def open(self):
		print('opening of fake connection')

	def send(self, src, trg):
		print(f'fake sending: {src} => {trg}')



class GitGetter(IGetter):
	def __init__(self):
		IGetter.__init__(self)
		self._submodules = None

	def submodules(self):
		if self._submodules is None:
			# https://stackoverflow.com/questions/12641469/list-submodules-in-a-git-repository
			cmd = ('git', 'config', '--file', '.gitmodules', '-z', '--get-regexp', 'path')		# '--name-only'
			data = subprocess.check_output(cmd, cwd=self.root, text=True).split('\x00')

			self._submodules = [l.split('\n')[1] for l in data if l]
		return self._submodules

	def _find_files(self):
		cmd = ('git', 'status', '-s', '-u', '-z', '--porcelain=v1')		# https://git-scm.com/docs/git-status#_options
		data = subprocess.check_output(cmd, cwd=self.root, text=True).split('\x00')

		for e in data:
			if not e: continue
			self.log.info(f'git item: {e}')
			status = e[0:2].strip()			# https://git-scm.com/docs/git-status#_output
			path = e[3:]

			if '->' in path:
				raise NotImplementedError

			if path in self.submodules():
				self.log.warning(f'submodules is not support yet: {path}')
				continue

			if status in ('M', '??'):	# modified, untracked
				yield path
			else:
				raise NotImplementedError(f'status {status} is not supported yet: {e}')





# -- mini test

if __name__ == '__main__':
	class Config:
		Getter = FakeGetter
		Sender = FakeSender
		connection = None
		source_root	= '/abs/path/to/src/root'
		dest_root = r'C:\win\path\to\dest\root'

		Getter = GitGetter
		source_root	= '/home/ant/b2b_reps/anvil_art'

	class App(IApp):
		def change_time(self, f):
			if self.config.Getter == FakeGetter:
				return None
			return IApp.change_time(self, f)

		def make_sync(self):
			ll = list(self.get_files())
			self.send_files(ll)
			self.send_files(ll)		# тест проверки времени


	App(Config).make_sync()
