

# Соберем тут код для получения словарей, так как не первый раз такие задачки возникают


import zipfile
import io

import mix_utils


# Показалось удобным хранить скачанный откуда то словарь (utf8 txt файл, 1 строка - 1 слово) в репе, в архиве для экономии места
# И по необходимости расширять его в отдельном файле
def __get_all_words(text_file):
	for s in text_file:
		word = s.strip()
		if word:
			yield word


def __get_all_words_from_zip_and_extra(zip_path, extra_path):
	with zipfile.ZipFile(zip_path) as zip_f:
		for z in zip_f.namelist():
			with zip_f.open(z) as main_bin_f:
				main_f = io.TextIOWrapper(main_bin_f, encoding='utf-8')
				for w in __get_all_words(main_f):
					yield w
	if extra_path:
		with open(extra_path, encoding='utf-8') as extra_f:
			for w in __get_all_words(extra_f):
				yield w


def zdf_win():
	for w in __get_all_words_from_zip_and_extra(
		mix_utils.path_to(__file__, '../../extra/zdf-win.txt.zip'),
		mix_utils.path_to(__file__, '../../extra/zdf-win.extra.txt')
	):
		yield w

def wordlist_10000():
	for w in __get_all_words_from_zip_and_extra(
		mix_utils.path_to(__file__, '../../extra/wordlist.10000.txt.zip'),
		mix_utils.path_to(__file__, '../../extra/wordlist.10000.extra.txt'),
	):
		yield w

def words_alpha():
	for w in __get_all_words_from_zip_and_extra(
		mix_utils.path_to(__file__, '../../extra/words_alpha.txt.zip'),
		None,
	):
		yield w



if __name__ == '__main__':
	wl_tt = list(wordlist_10000())
	assert len(wl_tt) >= 10000, 'Too few words'
	print(f'wordlist.10000 contains {len(wl_tt)} words. E.g. 100th is {wl_tt[100]}')
