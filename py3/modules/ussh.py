#!/usr/bin/env python3


# тут должны появиться классы обертки для работы с ssh

# http://docs.paramiko.org/en/stable/api/client.html

# sftp это протокол передачи и управления файлами
# scp - только передача. sftp дополнительно умеет удалять файлы, получать список содержимого директорий итп
# не будем без явной необходимости поддерживать scp


import paramiko
import logging



class SshError(RuntimeError):
	pass


class SshClient:

	def __init__(self, conn_parameters, timeout=10, log = None):
		self.conn = conn_parameters		# должно иметь атрибуты для SSHClient.connect: host, port, user, pwd
		self.timeout = timeout

		self.out_encoding = 'utf-8'

		self.log = log if log else logging.getLogger(self.__class__.__name__)

		self.ssh_client = paramiko.SSHClient()
		self.ssh_client.load_system_host_keys()
		self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		# может таки надо засунуть строки выше в __reconnect?

		self._reconnect()

	def _reconnect(self):
		self.log.info(f'ssh connect to {self.conn.host}')

		self.ssh_client.connect(hostname=self.conn.host, port=self.conn.port, username=self.conn.user, password=self.conn.pwd, timeout=self.timeout)

		self.sftp_client = self.ssh_client.open_sftp()
		self.sftp_client.get_channel().settimeout(self.timeout)

		# уже не особо понятно что коммент ниже значит, видимо есть какие то проблемы с таймаутом для put
		# для put ещё можно добавить callback, см https://stackoverflow.com/questions/58844902/how-to-timeout-sftp-put-with-signal-module-or-other-python
		# ещё возможный костыль: запускать операции взаимодействия с удаленным хостом в отдельном процессе\потоке. Процесс можно убить а вот с зависшим потоком непонятно что делать


	# все операции проводим через __make, чтобы перехватывать ошибки которые можно решить переоткрытием соединения
	def __make(self, func, *a, **kw):
		try:
			return func(*a, **kw)
		except Exception as exc:
			if not self._is_closed_connection_exception(exc):
				raise

		self._reconnect()

		return func(*a, **kw)


	# мутная конечно проверка, но почему-то вот такой зоопарк ошибок наблюдался
	def _is_closed_connection_exception(self, exc):
		if isinstance(exc, (paramiko.ssh_exception.SSHException, OSError)):
			exc_desc = f'{exc.__class__.__name__} {exc}'
			for sign in ('closed', 'dropped', 'PipeTimeout', 'timeout'):
				if sign in exc_desc:
					self.log.warning(f'`{exc_desc}`; looks like ssh channel was closed')
					return True
		return False


	# out - задаем что хотим видеть возвращаемым значением (см __take_out)
	# sudo - выполнить команду под sudo
	# exc_err - исключение, если stderr не пустой
	def exec_cmd(self, command, out = None, sudo = False, exc_err = True):
		sudo_str = 'sudo ' if sudo else ''
		self.log.info(f'[exec] {self.conn.host} `{sudo_str}{command}`')

		exec_command = self.ssh_client.exec_command
		if sudo:
			def sudo_command():
				stdin, stdout, stderr = self.ssh_client.exec_command(f'sudo -S {command}', timeout=self.timeout)
				stdin.write(f'{self.conn.pwd}\n')
				stdin.flush()
				return stdin, stdout, stderr
			exec_command = sudo_command

		raw_exec_command_ret = self.__make(exec_command, command, timeout=self.timeout)

		return self.__take_out(raw_exec_command_ret, exc_err, out)

	# получаем результат exec_command
	def __take_out(self, exec_command_return, exc_err, out_flag):
		out = err = None
		#
		def get_out():
			nonlocal out
			if out is None:
				out = exec_command_return[1].read().decode(self.out_encoding)
			return out
		#
		def get_err():
			nonlocal err
			if err is None:
				err = exec_command_return[2].read().decode(self.out_encoding)
			return err

		if exc_err:
			if get_err():
				self.log.info(f'stdout of ssh command:\n{get_out()}')
				self.log.error(f'stderr of ssh command is not empty:\n{get_err()}')
				raise SshError(get_err())

		# ничего не хотим
		if out_flag == None:
			return None

		# хотим сырое
		if out_flag == 0:
			return exec_command_return

		# stdout
		if out_flag == 1:
			return get_out()

		# stderr
		if out_flag == 2:
			return get_err()

		# stdout и stderr
		if out_flag == 3:
			return get_out(), get_err()

		raise NotImplementedError(f'unknown value of "out" argument: {out_flag}')


	def get(self, src, trg):
		self.log.info(f'[get] {self.conn.host} {trg} <= {src}')
		return self.__make(self.sftp_client.get, src, trg)


	# тут отрабатываем ошибку возникающую если таргет существует
	def put(self, src, trg, rm_trg = True):
		self.log.info(f'[send] {self.conn.host} {src} => {trg}')
		def action():
			try:
				self.sftp_client.put(src, trg)
			except Exception as exc:
				if not rm_trg and not self._is_target_exists_exception(exc):
					raise
				self.log.warning(f'{exc.__class__} was invoked, trying to remove target')

			# как ни странно, но наблюдал что и после PermissionError таргет потом удаляется без sudo
			self.sftp_client.remove(trg)	# ИЛИ self.__sudo_command(f'rm {trg}')

			self.sftp_client.put(src, trg)

			# отладить потом, если будет надо
			#if chmod_trg:
			#	try:
			#		self.sftp_client.chmod(trg, os.stat(src).st_mode)
			#	except (OSError, PermissionError) as e:		# очередное чудо, root-овый файл изменяется, но разрешения изменить не можем
			#		self.log.warning(f'[send] for {trg}. Cannot execute chmod')

		return self.__make(action)

	def _is_target_exists_exception(self, exc):
		if isinstance(exc, (PermissionError, OSError)):
			return True


	def mkdir_if_not_exist(self, trg, chmod_root_trg = False):
		self.log.info(f'[mkdir_if_not_exist] {self.conn.host} {trg}')
		def action():
			stat = self.__lstat(trg)
			if stat is None:
				self.log.info(f'mkdir {trg}')
				self.sftp_client.mkdir(trg)
			elif chmod_root_trg and stat.st_uid == 0:		# это было особенно актуально для ттм
				self.log.warning(f'[mkdir_if_not_exist] make user\'s permissions for {trg}')
				self.__sudo_command(f'chown {self.conn.user}:{self.conn.user} {trg}')	# self.sftp_client.chown(trg, 1000, 1000)

		return self.__make(action)

	def __lstat(self, trg):
		try:
			return self.sftp_client.lstat(trg)		# вызывается исключение PermissionError, хоть в доке и написано что то странное про возврат SFTP_PERMISSION_DENIED
		except FileNotFoundError:
			return None

	def lstat(self, trg):
		self.log.info(f'[lstat] {self.conn.host} {trg}')
		return self.__make(self.__lstat, trg)



if __name__ == '__main__':
	import os


	# U_HOST={need host name} python3 ussh.py
	class conn_params:
		host = os.environ['U_HOST']
		port = os.environ.get('U_PORT', 22)
		user = os.environ.get('U_USER')
		pwd = os.environ.get('U_PWD')

	ssh = SshClient(conn_params)

	ssh.exec_cmd('cd art_builder')
	print(ssh.exec_cmd('ls', out=1))
	# but
	print(ssh.exec_cmd('cd art_builder ; ls', out=1))

	#print(ssh.exec_cmd('qlmns', 4))		# не бывает такой команды и все равно только по stderr судим о ошибке (как минимум на винде)
	print(ssh.exec_cmd('ls ZZZ', 4))	# ну и тут понятно
