#!/usr/bin/env python3

# игровое поле-сетка
# gui объект на основе канваса
# много раз делал что то такое, см например field.py в py/modules, SeaBattle


import copy

import uTkinter as utk


class GridFieldError(RuntimeError):
	pass


class GridField:
	def __init__(self, master):
		self.canvas = utk.uCanvas(master)
		self.canvas.bind('<Configure>', self.redraw)

		self.initialized = False

	# не забудь что коллбек на евент один
	def add_key_handler(self, event_type, callback):
		def key_handler(event):
			callback(event)

		return self.canvas.master.bind(event_type, key_handler)
		# The canvas by default does not get keyboard focus.
		# Проще ловить события master-ом, чем поддерживать focus для канваса, но не факт что это правильно

	def add_mouse_handler(self, event_type, callback):
		def mouse_handler(event):
			if not self.initialized:
				return
			column = event.x / self.cell_width
			row = event.y / self.cell_height
			if column < self.columns and row < self.rows:
				callback(int(row), int(column), event)

		return self.canvas.bind(event_type, mouse_handler)

	def cancel_handler(self, event_type, funcid=None):
		self.canvas.unbind(event_type, funcid)
		# непонятно как оно работает, биндимся к мастеру, анбиндимся от канваса. Но таки работает.

	def reset(self, rows, columns, visible_grid = True, square_cell = True):
		self.canvas.delete('all')

		self.rows = rows
		self.columns = columns

		self.items = SparseItemCollection(rows, columns)

		self.square_cell = square_cell
		self.visible_grid = visible_grid

		self.cell_width = self.cell_height = 0

		if visible_grid:
			self.vert_grid_lines = [self.canvas.create_line(0,0,0,0) for i in range(columns + 1)]
			self.hor_grid_lines = [self.canvas.create_line(0,0,0,0) for i in range(rows + 1)]
		else:
			self.vert_grid_lines = []
			self.hor_grid_lines = []

		self.initialized = True
		self.redraw()

	def redraw(self, *a):
		if not self.initialized:
			return

		self.cell_width = self.canvas.winfo_width()/self.columns
		self.cell_height = self.canvas.winfo_height()/self.rows
		if self.square_cell:
			self.cell_width = self.cell_height = min(self.cell_width, self.cell_height)

		table_width = self.cell_width*self.columns
		table_height = self.cell_height*self.rows
		if self.visible_grid:
			for i in range(self.columns + 1):
				x = self.cell_width*i
				self.canvas.coords(self.vert_grid_lines[i], x, 0, x, table_height)
			for i in range(self.rows + 1):
				y = self.cell_height*i
				self.canvas.coords(self.hor_grid_lines[i], 0, y, table_width, y)

		for item in self.items.all():
			item.redraw()

	def add(self, item_pattern, row, column):
		if self.items.busy(row, column):
			raise GridFieldError(f'cell {row}x{column} is busy')
		new_item = item_pattern.copy()
		self.items.add(new_item, row, column)

	def remove(self, row, column):
		if not self.items.busy(row, column):
			raise GridFieldError(f'cell {row}x{column} is free')
		self.items.remove(row, column)



# Разреженная коллекция
class SparseItemCollection:
	def __init__(self, rows, columns):
		assert rows > 0 and columns > 0
		self.rows = rows
		self.columns = columns

		self.items = {}		# self.items[row][column]

	def add(self, item, row, column):
		if row < 0 or row >= self.rows or column < 0 or column >= self.columns:
			raise GridFieldError('[SparseItemCollection.add] index error')

		self.items.setdefault(row, {})[column] = item
		item.place(row, column)

	def remove(self, row, column):
		if row in self.items:
			item = self.items.get(row).pop(column)
			item.destroy()

	def busy(self, row, column):
		if row in self.items:
			return column in self.items.get(row)
		return False

	def get(self, row, column):
		if row in self.items:
			return self.items.get(row).get(column)

	def all(self):
		for row in self.items.values():
			for it in row.values():
				yield it


class FieldItem:
	def __init__(self, owner, color=None, scale=0.9, tag=None, **tk_options):
		self.owner = owner
		self.tag = tag
		self.scale = scale
		self.row = self.column = None

		self.tk_options = tk_options
		self.canvas_object = self.init_canvas_object()	# реализация наследника
		if color:
			tk_options.setdefault('outline', color)
			tk_options.setdefault('fill', color)
		owner.canvas.itemconfigure(self.canvas_object, tags=(tag,), **tk_options)

	def copy(self):
		oth = self.__class__(self.owner, None, self.scale, self.tag, **copy.copy(self.tk_options))
		# 								color скопируется через tk_options
		return oth

	def place(self, row, column):
		self.row = row
		self.column = column
		self.redraw()

	def destroy(self):
		# самостираемся
		self.owner.canvas.delete(self.canvas_object)

	def redraw(self):
		# саморисуемся
		self.owner.canvas.coords(self.canvas_object, *self._bbox())

	def _bbox(self):
		scale = max(self.scale, 0.01)	# больше 1 быть может, вылезет из клетки
		x = self.column*self.owner.cell_width
		y = self.row*self.owner.cell_height
		x_d = (1 - scale) * self.owner.cell_width // 2
		y_d = (1 - scale) * self.owner.cell_height // 2
		return (x + x_d, y + y_d, x + self.owner.cell_width - x_d, y + self.owner.cell_height - y_d)


class CircleItem(FieldItem):
	def init_canvas_object(self):
		return self.owner.canvas.create_oval(0,0,0,0)

class RectangleItem(FieldItem):
	def init_canvas_object(self):
		return self.owner.canvas.create_rectangle(0,0,0,0)


# универсальный хелпер для событий перемещений мыши
# не знаю зачем такой может пригодиться, больше как пример
class MotionEventHelper:
	def __init__(self, field, motion_callback = None, enter_callback = None, leave_callback = None):
		assert motion_callback or enter_callback or leave_callback
		self.motion_callback = motion_callback
		self.enter_callback = enter_callback
		self.leave_callback = leave_callback

		self.row = self.column = None

		field.add_mouse_handler('<Motion>', self.handler)

	def handler(self, row, column, event):
		if self.motion_callback:
			self.motion_callback(row, column, event)

		if self.enter_callback and (row != self.row or column != self.column):
			self.enter_callback(row, column, event)

		if self.leave_callback and (row != self.row or column != self.column) and self.row is not None:
			self.leave_callback(row, column, event)

		self.row, self.column = row, column


# полезное подмножество MotionEventHelper
class MouseEnterDetector:
	def __init__(self, field, enter_callback):
		self.enter_callback = enter_callback
		self.row = self.column = None
		field.add_mouse_handler('<Motion>', self.handler)

	def handler(self, row, column, event):
		if row != self.row or column != self.column:
			self.enter_callback(row, column, event)
		self.row, self.column = row, column


if __name__ == '__main__':
	class Demo:
		def __init__(self):
			root = utk.uTk(title='GridField demo', minw=640, minh=640, createStatus=False)

			self.field = GridField(root)
			self.red_circle = CircleItem(self.field, 'red')
			self.field.add_mouse_handler('<Button-1>', self.button1_handler)
			self.field.reset(10, 10)

			# не до конца понимаю, почему временный объект тут работает.. Наверное потому что атрибут handler используется
			MotionEventHelper(self.field, enter_callback = print)
			#MotionEventHelper(self.field, leave_callback = print)
			#MotionEventHelper(self.field, motion_callback = print)

			root.mainloop()


		def button1_handler(self, row, column, event):
			ctrl_pressed = event.state & 4			# см Event.__repr__
			try:
				if ctrl_pressed:
					self.field.remove(row, column)
				else:
					self.field.add(self.red_circle, row, column)
			except GridFieldError as e:
				print(e.__class__.__name__, e)

	Demo()
