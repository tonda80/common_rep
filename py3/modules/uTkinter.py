﻿#!/usr/bin/python

from tkinter import *
from tkinter import ttk		# TODO понять что это и может использовать эти виджеты

import itertools
import platform

try:
	import screeninfo	# pip install screeninfo
except ImportError:
	print('Warning. screeninfo is not imported. Some functionality will not be affordable.')


class uCallback:
	'The callback with the label and the source widget'
	def __init__(self, clb, *args1, **args2):
		self.clb = clb
		self.args1 = args1
		self.args2 = args2
		self.widget = None

	def setWidget(self, widget):
		self.widget = widget

	@staticmethod
	def trySetWidget(obj, wid):
		if isinstance(obj, uCallback):
			obj.setWidget(wid)

	def __call__(self, *unusedArgs):
		self.clb(self.widget, *self.args1, **self.args2)

class VariableWrapper:
	'Successors use self.var and methods'
	def __init__(self, ValueType, defaultValue):
		#if defaultValue == '' and (ValueType == IntVar or ValueType == DoubleVar): defaultValue = 0	непонятно надо ли

		# TODO не предусмотрел передачу в виджет готовой Variable, можно костыльно передавать через defaultValue или лучше добавить аргумент
		#if isinstance(defaultValue, Variable):
		#	self.var = defaultValue
		#else:
		self.var = ValueType(value=defaultValue)

		self.set_var = self.setVar
		self.get_var = self.getVar

	def setVar(self, v):
		self.var.set(v)

	def getVar(self):
		return self.var.get()


class GridLayout:
	# идея подсмотрена в qml, автоматически размещаем виджеты в сетке, автоинкрементируя row, column
	# TODO! переделать на контекстный менеджер

	Key = 'gridlayout'	# ключ для передачи объекта GridLayout (сперва через gmArgs, потом через мастер виджет)

	def __init__(self, master, rows = 0, columns = 0):
		self.master = master
		setattr(self.master, GridLayout.Key, self)

		# 0 - неограниченно
		if columns == 0 and rows != 1:
			raise RuntimeError(f'GridLayout cannot form grid {rows}x{columns}')		# TODO?? добавляем построчно, может ещё сделать "постолбцово"
		self.rows = rows
		self.columns = columns
		self.cnt = 0

	def real_options(self, options):
		assert not('row' in options or 'column' in options), f'wrong usage of GridLayout: options={options}'

		if self.columns == 0:
			row, column = 0, self.cnt
		else:
			row, column = divmod(self.cnt, self.columns)

		if self.rows and row + 1 > self.rows:
			raise RuntimeError(f'GridLayout cannot place widget in grid {self.rows}x{self.columns} with row={row}, column={column}')

		# обработка span-ов (сырое, кривое и косое)
		rowspan = options['rowspan'] if 'rowspan' in options else 1
		columnspan = options['columnspan'] if 'columnspan' in options else 1

		if (self.columns and column + columnspan > self.columns) or (self.rows and row + rowspan > self.rows):
			raise RuntimeError(f'GridLayout cannot place widget in grid {self.rows}x{self.columns} with row={row}, column={column}, rowspan={rowspan}, columnspan={columnspan}')

		self.cnt += self.columns*(rowspan - 1) + columnspan

		_options = options.copy()
		_options.update(row = row, column = column)
		#print('__deb2', _options, self.cnt, self.columns, rowspan, columnspan)
		return _options

	def make_stretchy(self):
		make_grid_stretchy(self.master)

def make_grid_stretchy(master):
		# делает ячейки сетки растягивающимися. вызывать после заполнения. если надо чтобы элементы внутри растягивались, добавить в gmArgs 'sticky':'wens'
		size = master.grid_size()
		for i in range(size[0]):
			master.grid_columnconfigure(i, weight=1)	# minsize
		for i in range(size[1]):
			master.grid_rowconfigure(i, weight=1)


class GeometryManager:
	def putOn(self, options, put_obj = None):
		if options == None:
			options = {}
		if put_obj == None:
			put_obj = self

		grid_signs = ('row', 'column')
		place_signs = ('x', 'y', 'relx', 'rely')

		def contains_any_from(seq1, seq2):
			# seq1 should be less
			# other variant => any(x in a for x in b)
			for x in seq1:
				if x in seq2:
					return True
			return False

		try:
			grid_layout = getattr(self.master, GridLayout.Key)
		except AttributeError:
			grid_layout = None

		if contains_any_from(grid_signs, options.keys()) or grid_layout:
			self.gmType = 'grid'
			self.setDefaultGridArgs(options)
			if grid_layout:
				options = grid_layout.real_options(options)
			put_obj.grid(**options)
		elif contains_any_from(place_signs, options.keys()):
			self.gmType = 'place'
			self.setDefaultPlaceArgs(options)
			put_obj.place(**options)
		else:															# pack is default
			self.gmType = 'pack'
			self.setDefaultPackArgs(options)
			put_obj.pack(**options)

	def setDefaultPackArgs(self, options): pass
	def setDefaultGridArgs(self, options): pass
	def setDefaultPlaceArgs(self, options): pass

# gmArgs = {'expand':YES, 'fill':BOTH}
class uFrame(Frame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)	# FLAT

	def __init__(self, master, gmArgs = None, **args):
		self.setDefaultArgs(args)
		Frame.__init__(self, master, **args)
		self.putOn(gmArgs)

class uCanvas(Canvas, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)
		args.setdefault('background', 'white')

	def setDefaultPackArgs(self, options):
		options.setdefault('expand', YES)
		options.setdefault('fill', BOTH)

	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		self.setDefaultArgs(args)
		Canvas.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	def draw_oval(self, x, y, size, **kw):
		self.create_oval(x - size/2, y - size/2, x + size/2, y + size/2, **kw)


# pack inside! i.e. any other managers are forbidden for the master widget!
def _set_scrolling(scrollmode, master, widget, bg=None):
		scrollmode = scrollmode.lower()
		assert scrollmode in ('x', 'y', 'xy', 'yx', ''), 'Wrong value of scrollmode: %s'%scrollmode

		x_scroll = y_scroll = None
		if 'x' in scrollmode:
			x_scroll = Scrollbar(master, command=widget.xview, orient=HORIZONTAL, background=bg)
			x_scroll.pack(side=BOTTOM, fill=X)
			widget.configure(xscrollcommand = x_scroll.set)
		if 'y' in scrollmode:
			y_scroll = Scrollbar(master, command=widget.yview, orient=VERTICAL, background=bg)
			y_scroll.pack(side=RIGHT, fill=Y)
			widget.configure(yscrollcommand = y_scroll.set)

		return (x_scroll, y_scroll)

class uScrollFrame(Frame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)

	def __init__(self, master, scrollmode, gmArgs = None, **args):
		self.setDefaultArgs(args)
		if 'bg' in args:
			bg = args['bg']
		elif 'background' in args:
			bg = args['background']
		else:
			bg = master.cget('bg')

		_base_frame = Frame(master, background=bg, **args)
		self.putOn(gmArgs, _base_frame)

		self._canvas = Canvas(_base_frame,  background=bg)

		_set_scrolling(scrollmode, _base_frame, self._canvas, bg)

		self._canvas.pack(expand='YES', fill=BOTH)

		Frame.__init__(self, self._canvas, background=bg)
		self._canvas.create_window((0,0), window=self, anchor='nw')
		self.bind("<Configure>", self._configure_clb)

	def _configure_clb(self, event):
		self._canvas.configure(scrollregion=self._canvas.bbox(ALL), width=20, height=20)

class uLabelFrame(LabelFrame, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('borderwidth', 2)
		args.setdefault('relief', GROOVE)

	def __init__(self, master, label, gmArgs = None, **args):
		self.setDefaultArgs(args)
		LabelFrame.__init__(self, master, text = label, **args)
		self.putOn(gmArgs)

class uButton(Button, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('width', 10)
		args.setdefault('takefocus', OFF)

	def __init__(self, master, command, text = 'Button', gmArgs = None, **args):
		self.setDefaultArgs(args)
		uCallback.trySetWidget(command, self)
		Button.__init__(self, master, text = text, command = command, **args)
		self.putOn(gmArgs)

class uEntry(Entry, GeometryManager, VariableWrapper):
	def setDefaultArgs(self, args):
		args.setdefault('width', 20)

	def __init__(self, master, defaultValue = '', ValueType = StringVar, callback = None, gmArgs = None, **args):
		self.setDefaultArgs(args)
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Entry.__init__(self, master, textvariable = self.var, **args)

		self._popup = None

		self.putOn(gmArgs)

		uCallback.trySetWidget(callback, self)
		if callback is not None:
			self.var.trace('w', callback)

		#self.var.trace('w', self._internal_w_callback)	# will be called earlier
		#def _internal_w_callback(self, *args):

	def select_all(self):
		self.select_range(0, END)

	# методы для связанного EntryPopupMenu
	def set_popup(self, popup):
		assert self._popup is None
		self._popup = popup
		self.getVar = self._getVarPopupOverriden
	# ^--
	def _getVarPopupOverriden(self):
		res = VariableWrapper.getVar(self)
		self._popup.entry_get_var_callback(res)
		return res

class uScale(Scale, GeometryManager, VariableWrapper):
	def __init__(self, master, from_, to, resolution=1, command = None, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Scale.__init__(self, master, from_ = from_, to = to, resolution = resolution, command = command, variable = self.var, **args)

		self.putOn(gmArgs)

		uCallback.trySetWidget(command, self)
# Scale selected options
# label, length, orient ('h', 'v'), resolution, showvalue, sliderlength, sliderrelief, width

# ATTENTION! 'command' is for the button actions, 'callback' is for any
class uSpinbox(Spinbox, GeometryManager, VariableWrapper):
	def __init__(self, master, from_, to, increment=1.0, callback = None, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Spinbox.__init__(self, master, from_ = from_, to = to, increment = increment, textvariable = self.var, **args)

		self.putOn(gmArgs)

		uCallback.trySetWidget(callback, self)
		if callback is not None:
			self.var.trace('w', callback)

class uCheckbutton(Checkbutton, GeometryManager, VariableWrapper):
	def __init__(self, master, text = '', callback = None, defaultValue = False, gmArgs = None, **args):
		VariableWrapper.__init__(self, BooleanVar, defaultValue)
		uCallback.trySetWidget(callback, self)
		Checkbutton.__init__(self, master, variable = self.var, command = callback, text = text, **args)

		self.putOn(gmArgs)

class uOptionMenu(OptionMenu, GeometryManager, VariableWrapper):
	def __init__(self, master, options, command = None, ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, options[0])
		uCallback.trySetWidget(command, self)
		OptionMenu.__init__(self, master, self.var, *options, command = command, **args)

		self.putOn(gmArgs)

		self.options = options	# не нашел такого списка в коде Menu.add_command

	def get_option(self):
		return self.var.get()
	def get_option_number(self):
		return self.options.index(self.var.get())

class uText(Text, GeometryManager):
	def setDefaultArgs(self, args):
		args.setdefault('font', 'Courier 10')
		args.setdefault('height', 10)

	def setDefaultPackArgs(self, packArgs):
		packArgs.setdefault('expand', YES)
		packArgs.setdefault('fill', BOTH)

	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		self.setDefaultArgs(args)
		Text.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	def get_state(self):
		return self.config()['state'][-1]	# проще?

	def get_all(self):
		return self.get('1.0', END)

	def get_line(self, n):
		return self.get(f'{n}.0', f'{n}.0 lineend')

	def append(self, txt):
		with uText.EnabledEditingContext(self):
			self.insert(END, txt)

	def clear_all(self):
		with uText.EnabledEditingContext(self):
			self.delete('1.0', END)
			# TODO delete marks? tags?

	def set_all(self, txt):
		with uText.EnabledEditingContext(self):
			self.delete('1.0', END)
			self.insert(END, txt)


	# TODO подумать, не бред ли это. Реализуем интерфейс VariableWrapper, для использования в init_kept_uvar
	def setVar(self, v):
		self.set_all(v)
	def getVar(self):
		return self.get('1.0', 'end - 1 chars')		# get('1.0', END) добавляет /n. тут это особенно неприятно

	class EnabledEditingContext:
		def __init__(self, utext):
			self.utext = utext
			self.init_state = utext.get_state()
		def __enter__(self):
			if self.init_state != NORMAL:
				self.utext.config(state=NORMAL)
		def __exit__(self, *a):
			if self.init_state != NORMAL:
				self.utext.config(state=self.init_state)

# заметки по Text
# индекс - положение в тексте: '4.5', 'end' конец текста, '2.4 linestart' начало строки 2, '2.4 lineend' конец строки 2 (видимо цифра после точки в line-индексах не важна)
# строки нумеруются с 1, а символы в строке с 0, то есть '1.0' начало текста
# '3.100500' не ошибка а тоже конец строки
# поддерживается даже математика в индексах
#   t.get("1.0", "end - 3 chars"); t.get("1.0", "end - 3 chars")
# можно получить абсолютное значение через t.index('10.8 lineend - 3 chars')
# бывают теги (см пример)


class uLabel(Label, GeometryManager):
	def __init__(self, master, text, gmArgs = None, **args):
		Label.__init__(self, master, text = text, **args)
		self.putOn(gmArgs)

	def set_text(self, text):
		self.config(text=text)

class uMutableLabel(Label, GeometryManager, VariableWrapper):
	def __init__(self, master, defaultValue = '', ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, defaultValue)
		Label.__init__(self, master, textvariable = self.var, **args)
		self.putOn(gmArgs)

class RadiobuttonGroup(VariableWrapper):
	def __init__(self, master, values, command = None, title = '', texts = (), intSide = TOP, ValueType = StringVar, gmArgs = None, **args):
		VariableWrapper.__init__(self, ValueType, values[0])

		uCallback.trySetWidget(command, self)

		self.frame = uLabelFrame(master, title, gmArgs)

		self.radiobuttons = []
		for value, text in itertools.zip_longest(values, texts):
			if value is None:
				break
			if text is None:
				text = str(value)
			self.radiobuttons.append(Radiobutton(self.frame, text=text, variable=self.var, value=value, command=command, **args))
			self.radiobuttons[-1].pack(side=intSide, anchor=W)

class uListbox(Listbox, GeometryManager):
	def setDefaultPackArgs(self, options):
		options.setdefault('expand', YES)
		options.setdefault('fill', Y)

	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		Listbox.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	def setMode(self, mode):
		# SINGLE, BROWSE (useful, default), MULTIPLE, EXTENDED (useful)
		self.config(selectmode=mode)

	def addMouseCallback(self, clb, event_type):
		# <Double-Button-1> etc
		def int_callback(ev):
			# self.index("@%d,%d"%(ev.x, ev.y)), self.get(ACTIVE)
			i = self.nearest(ev.y)
			if i >= 0:
				clb(i, self.get(i))
		self.bind(event_type, int_callback)

	def append(self, *items, **options):
		self.add(END, *items, **options)

	def add(self, index, *items, **options):
		self.insert(index, *items)
		if options:
			self.itemconfig(index, **options)

	def clear(self):
		self.delete(0, END)

# https://docs.python.org/2/library/ttk.html#treeview
class uTreeView(ttk.Treeview, GeometryManager):
	def __init__(self, master, scrollmode='', gmArgs = None, **args):
		ttk.Treeview.__init__(self, master, **args)
		_set_scrolling(scrollmode, master, self)
		self.putOn(gmArgs)

	# удаляем всех потомков item-а
	def clear_item(self, iid):
		self.delete(*self.get_children(iid))

# https://docs.python.org/2/library/ttk.html#separator
class uSeparator(ttk.Separator, GeometryManager):
	def __init__(self, master, horizontal=True, gmArgs = None):
		if gmArgs is None:
			gmArgs = {}
		if horizontal:
			orient = 'horizontal'
			gmArgs.setdefault('fill', X)
			gmArgs.setdefault('side', TOP)
		else:
			orient = 'vertical'
			gmArgs.setdefault('fill', Y)
			gmArgs.setdefault('side', LEFT)

		ttk.Separator.__init__(self, master, orient=orient)
		self.putOn(gmArgs)

#~ class u(, GeometryManager):
	#~ def setDefaultArgs(self, args):
	#~
	#~ def setDefaultPackArgs(self, packArgs):
#~
	#~ def __init__(self, master, , gmArgs = None, **args):
		#~ self.setDefaultArgs(args)
		#~ .__init__(self, master, , **args)
		#~ self.putOn(gmArgs)


class uToplevel(Toplevel):
	def __init__(self, master, modal, title=None, **args):
		Toplevel.__init__(self, master, **args)
		self.title(title)
		if modal:
			if not self.winfo_viewable():
				self.wait_visibility()
			self.grab_set()
			self.attributes('-topmost', 'true')

	def set_delete_callback(self, callback, args=()):
		self.protocol("WM_DELETE_WINDOW", lambda: callback(*args))


class uMenu(Menu):
	def __init__(self, master, **args):		# postcommand
		args.setdefault('tearoff', False)	# не хотим странно открепляющихся меню по дефолту
		Menu.__init__(self, master, **args)

		if isinstance(master, Tk):
			master.config(menu=self)		# это menubar

	def create_submenu(self, label, **args):
		submenu = uMenu(self, **args)
		self.add_cascade(label=label, menu=submenu)
		return submenu

	# add_command(label, command)
	# add_checkbutton, add_radiobutton, add_separator
	# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/menu.html


class PopupMenu(Menu):
	def __init__(self, master, other_callback=None):
		Menu.__init__(self, master, tearoff=0)
		self.label_font = None
		master.bind("<Button-3>", self.__popup)
		self.other_callback = other_callback		# для передачи <Button-3> события

	def __popup(self, event):
		if self.other_callback:
			self.other_callback(event)
		if self.winfo_ismapped():	# проверка на видимость https://stackoverflow.com/a/46377245
			self.unpost()
		else:
			self.post(event.x_root, event.y_root)

	def add_callback(self, labels, callback, index=None):		# labels may be a string or a iterable of strings
		if isinstance(labels, str):
			labels = (labels, )

		for label in labels:
			def cmd(label=label):  # closure in the loop (https://stackoverflow.com/questions/233673/lexical-closures-in-python)
				callback(label)
			if index is None:
				self.add_command(label=label, command=cmd, font=self.label_font)
			else:
				self.insert_command(index, label=label, command=cmd, font=self.label_font)
				index += 1	# не отлажено, но по идее должно быть так


# popup хранящий варианты выбора для Entry
# вариант с сортировкой по частоте использования был выпилен, как сырой и оказавшийся не особо удобным,
#	если захочется его допилить см бекап в trash_rep/py/EntryPopupMenuWithCounter.py)
# TODO? добавить создание EntryPopupMenu в Entry
class EntryPopupMenu(PopupMenu):

	class LabelsFifo:
		def __init__(self, config_labels, limit):
			assert isinstance(config_labels, list), "Wrong usage. Maybe you need remove old history("

			self._config_labels = config_labels
			self._limit = limit

		def get_first(self):
			if self._config_labels:
				return self._config_labels[0]
			return ''

		def get_all(self):
			return self._config_labels

		def remove(self, value):
			while value in self._config_labels:		# на всякий случай через while
				self._config_labels.remove(value)

		def clear(self):
			self._config_labels.clear()

		def update(self, value):
			if self._config_labels and self._config_labels[0] == value:
				return

			self.remove(value)
			self._config_labels.insert(0, value)

			del self._config_labels[self._limit:]	# удаление лишних

	def __init__(self, master_entry, config_labels, limit = 50, font = 'System 9', story_type = 'fifo'):
		# config_labels - list\dict из commonconfig с историей
		PopupMenu.__init__(self, master_entry)
		trace_control_l(master_entry)			# чтоб работало, фокус должен быть в entry

		self.label_font = font		# base class

		# удивительный баг с числовыми значениями, menu.index('N') вернет число N (если в меню есть элементы)
		# чтобы такого не было добавим ко всем меню невидимый нечисловой префикс
		# в _labels храним значения без префикса
		self._LABEL_PREF = ' '

		if story_type == 'fifo':
			self._labels = EntryPopupMenu.LabelsFifo(config_labels, limit)
		else:
			raise NotImplementedError

		master_entry.set_popup(self)
		master_entry.setVar(self._labels.get_first())
		self._master_entry = master_entry

		self.__create_all_menus()

	def __create_all_menus(self):
		self.add_callback(map(lambda l: self._LABEL_PREF + l, self._labels.get_all()), self.__on_click)
		self.add_separator()
		self.add_callback('Clear all', self.__clear_all)

	def __on_click(self, label):
		value = label[1:]	# removed self._LABEL_PREF
		if self._master_entry.is_control_l_pressed:
			self._labels.remove(value)
			self.delete(self.index(label))
		else:
			self._master_entry.setVar(value)
			self._labels.update(value)
			self.__menu_on_top(label)

	def entry_get_var_callback(self, entry_value):
		# может не надо на каждое чтение реагировать? и стоит например добавить колбек?
		if not entry_value:
			return
		self._labels.update(entry_value)
		self.__menu_on_top(self._LABEL_PREF + entry_value)

	def __menu_on_top(self, label):
		try:
			idx = self.index(label)
		except TclError:
			idx = None
		if idx == 0:
			return
		elif idx:
			self.delete(idx)
		self.add_callback(label, self.__on_click, 0)

	def __clear_all(self, *_):
		self._labels.clear()
		end = self.index(END) - 2	# до сепаратора
		if end >= 0:
			self.delete(0, end)


# делаем виджет отслеживающим состояние клавиши отображением в is_<KEY>_pressed, но работает только когда виджет в фокусе
def trace_key(widget, key):
	attr_name = f'is_{key.lower()}_pressed'
	widget.bind(f'<KeyPress-{key}>', lambda ev: setattr(widget, attr_name, True))
	widget.bind(f'<KeyRelease-{key}>', lambda ev: setattr(widget, attr_name, False))
	setattr(widget, attr_name, False)

def trace_control_l(widget):
	trace_key(widget, 'Control_L')
def trace_shift_l(widget):
	trace_key(widget, 'Shift_L')
def trace_alt_l(widget):
	trace_key(widget, 'Alt_L')


class StatusBar:
	Height = 30
	def __init__(self, master, font = 'System 11'):
		f = uFrame(master, height=self.Height, borderwidth=2, relief=GROOVE, gmArgs={'side':BOTTOM, 'fill':X, 'expand':NO})
		# For some reason "expand - YES" places the bar into a window middle
		f.pack_propagate(False)		# запрещаем растягивание длинными сообщениями
		self.infoLabel = uMutableLabel(f, font = font, gmArgs={'side':LEFT, 'pady':0, 'padx':3})
		self.rightInfoLabel = uMutableLabel(f, font = font, gmArgs={'side':RIGHT, 'pady':0, 'padx':3})

	def set(self, st):
		self.infoLabel.setVar(st)

	def add(self, st):
		self.infoLabel.setVar(self.infoLabel.getVar() + st)

	def set_r(self, st):
		self.rightInfoLabel.setVar(st)

	def add_r(self, st):
		self.rightInfoLabel.setVar(self.infoLabel.getVar() + st)


class uTk(Tk):
	def __init__(self, title='', minw=0, minh=0, createStatus=True):
		Tk.__init__(self)

		self.title(title)
		self.minsize(minw, minh + StatusBar.Height if createStatus else minh)

		if createStatus:
			self.statusBar = StatusBar(self)

		self.delete_callback = lambda:None
		self.delete_args = ()

	# в Tk нет метода с таким именем
	# при destroy не вызывается заданное в WM_DELETE_WINDOW
	def close(self):
		self.delete_callback(*self.delete_args)
		self.destroy()

	def set_delete_callback(self, callback, args=()):
		self.delete_callback = callback
		self.delete_args = args
		self.protocol("WM_DELETE_WINDOW", self.close)

	# FUB (frequently used bindings)
	def bind_return(self, callback):
		self.bind('<Return>', lambda e: callback())
	def set_exit_by_escape(self):
		self.bind('<Escape>', lambda e: self.close())
	def bind_left_click(self, callback):
		self.bind('<Button-1>', lambda e: callback(e))


def placeWindow(wnd, x, y, width, height):
	wnd.geometry('%dx%d+%d+%d'%(width, height, x, y))

def placeWindowTo(wnd, x, y):
	wnd.geometry('+%d+%d'%(x,y))


def screen_center(wnd):
	try:
		monitors = screeninfo.get_monitors()
	except NameError:
		return wnd.winfo_screenwidth()//2, wnd.winfo_screenheight()//2
	x = wnd.winfo_x(); y = wnd.winfo_y()
	for m in monitors:
		if m.x <= x < m.x+m.width and m.y <= y < m.y+m.height:
			return m.x+m.width//2, m.y+m.height//2
	return wnd.winfo_screenwidth()//2, wnd.winfo_screenheight()//2

def placeWindowToCenter(wnd, dX = 0, dY = 0):
	x, y = screen_center(wnd)
	x -= wnd.winfo_width()//2 + dX
	y -= wnd.winfo_height()//2 + dY
	#print('_deb', x, y, wnd.winfo_width(), wnd.winfo_height())
	wnd.geometry('+%d+%d'%(x,y))

def setWindowSize(wnd, w , h):
	wnd.geometry('%dx%d'%(w, h))

# https://stackoverflow.com/questions/15981000/tkinter-python-maximize-window
# похоже "нормального" пути нет
def maximizeWindow(wnd):
	w, h = wnd.winfo_screenwidth(), wnd.winfo_screenheight()
	if platform.system() == 'Windows':
		h -= 100	# отступ от панели. бред, а что делать.
	wnd.geometry("%dx%d+0+0" % (w, h))


def rgb(r, g, b):
	return f'#{r:02x}{g:02x}{b:02x}'


# см Event.__repr__ в tkinter/__init__.py
# или тут https://stackoverflow.com/a/61998948
def ctrl_is_pressed(event):
	return event.state & 4 != 0


def x_spacer(master, width, side = LEFT):
	return uFrame(master, relief=FLAT, borderwidth=0, gmArgs={'side': side, 'padx': width/2})

def y_spacer(master, height, side = TOP):
	return uFrame(master, relief=FLAT, borderwidth=0, gmArgs={'side': side, 'pady': height/2})


if __name__ == '__main__':

	def pp(a = None):
		print('Something pressed', a, type(a))

	def pp2(widget, *a1, **a2):
		print('%s from %s with %s %s'%(widget.getVar(), widget.__class__, a1, a2))

	w = Tk()

	f = uFrame(w, gmArgs = {'expand':YES, 'fill':BOTH})

	uButton(f, pp, 'Press me')

	e1 = uEntry(f, 12.34, DoubleVar, uCallback(pp2, 'e1'))
	e2 = uEntry(f, 1, callback=uCallback(pp2, 'e2'))

	cb1 = uCheckbutton(f, 'Check me', pp)
	cb2 = uCheckbutton(f, 'Named callback', uCallback(pp2, 'cb2'))
	cb3 = uCheckbutton(f, 'Named callback 2', uCallback(pp2, 'cb3'))

	uOptionMenu(f, (1, 2, 3), uCallback(pp2, 'opt menu 1'))

	f2 = uLabelFrame(w, 'Text', borderwidth=4, gmArgs = {'expand':YES, 'fill':BOTH})
	t = uText(f2, scrollmode='y')
	uButton(f2, t.clear_all, 'clear')
	b_tt = uButton(f2, None, 'toggle state')
	def toggle_text_state():
		new_state = 'normal' if t.get_state()=='disabled' else 'disabled'
		t.config(state=new_state)
		b_tt.config(text='text is '+new_state)
	b_tt.config(command=toggle_text_state)

	uLabel(f, 'Label')

	uOptionMenu(f, ('red', 'green', 'yellow'), lambda s: ml.setVar(s))
	ml = uMutableLabel(f, 'uMutableLabel')
	uButton(f, uCallback(lambda w, l: ml.setVar(l), ''), 'reset')	# uCallback just for demo

	RadiobuttonGroup(f, ('var1', 'other var'), uCallback(pp2, 'rb1'))
	rbg2 = RadiobuttonGroup(f, range(5), uCallback(pp2, 'rb2'), 'Numbers', ('zero', 'one'), LEFT)
	def rb_test(): print(rbg2.getVar())
	uButton(f, rb_test, 'rb_test')

	uScale(w, 1.0, 10.0, 0.1, command=uCallback(pp2), orient='h', label = 'Scale', gmArgs={'expand':YES, 'fill':X})

	uSpinbox(w, 1.0, 10.0, 0.1, callback=uCallback(pp2))

	def create_toplevel(title=None):
		tl = uToplevel(w, False, title)
		tl.protocol("WM_DELETE_WINDOW", w.destroy)
		return tl

	w2 = create_toplevel('w2')
	StatusBar(w2).set('Some very helpful line')
	setWindowSize(w2, 300, 400)
	placeWindowToCenter(w2)

	cnt = 0
	def add_btn():
		global cnt; uButton(scrFr, pp, 'Press %d'%cnt, gmArgs = {'pady':8}); cnt += 1
	uButton(w2, add_btn)
	scrFr = uScrollFrame(w2, 'y', gmArgs = {'expand':YES, 'fill':BOTH})
	for i in range(30):
		uButton(scrFr, pp, 'Press')

	w3 = create_toplevel('w3')
	lbx = uListbox(w3, 'yx', font=("Helvetica", 12))
	def p_lb(i, s): print(i, s)
	lbx.addMouseCallback(p_lb, '<Double-Button-1>')
	for s in 'one two three addCallback somethingelse'.split() + list(map(str, range(30))):
		lbx.append(s)
	lbx.append('really long string '*20)

	uButton(w3, lbx.clear, 'Clear')
	#print(lbx.config())

	popup_menu = PopupMenu(lbx)
	popup_menu.add_callback('test1', pp)
	popup_menu.add_callback('test2', pp)
	popup_menu.add_callback(['test3','test4','test5'], pp)

	w4 = create_toplevel('w4')
	tv = uTreeView(w4, 'xy', gmArgs={'expand':YES, 'fill':BOTH})
	columns = ['column%d'%i for i in range(5)]
	tv['columns'] = columns		# тут задаем id столбцов, а чуть ниже текст
	for c in columns:
		tv.heading(c, text=c)
		tv.column(c, minwidth=100)
	#tree.configure(selectmode = 'none')	# none extended browse(default)
	tv.tag_configure('special_tag', foreground='red')
	tv.tag_configure('another_tag', font=('Symbol', 10, 'bold'))
	for i in range(30):	# элементы верхнего уровня
		iid = 'item%d'%i	# это можно не передавать, тогда само сгенерирует и вернет из insert
		tv.insert('', 'end', iid=iid, text=iid, values=['column value %d %d'%(i,j) for j in range(len(columns))])
	tv.item('item2', tags=['special_tag'])
	tv.item('item1', tags=['another_tag'])
	for item in ('item4', 'item8', 'item10'):
		tv.insert(item, 'end', text='subitem of %s'%item, values=map(str, range(3)), tags=['special_tag'])

	w5 = create_toplevel('w5')
	grid_layout = GridLayout(w5, 3, 4)
	for i in range(12):
		uEntry(w5, width =3)

	w.mainloop()

# Tkinter memo
# ------------
# pack options: side, expand, fill, ipadx, ipady, padx, pady, anchor
#
# self.root.title(u'Title')
# self.root.bind('<Key>', self.hndl_key)
# self.root.minsize(500, 200)
# self.root.attributes('-fullscreen', True)
# geometry('%dx%d+%d+%d'%(w,h,x,y))
# packArgs.setdefault('side', TOP)
# packArgs.setdefault('fill', X)
# args.setdefault('font', 'Courier 10')
# args.setdefault('indicatoron', True)
# root.protocol("WM_DELETE_WINDOW", self.delete_callback)	AND root.destroy() into callback
# root.protocol("WM_TAKE_FOCUS", pp)
# root.iconify()
# root.wm_attributes('-topmost', 1)	always on top
# colors
#	RRGGBB tk_rgb = "#%02x%02x%02x" % (128, 192, 200)
#	'red', 'green', 'blue', ....   for Windows SystemActiveBorder, SystemActiveCaption,
# font
#	("Helvetica", 10, "bold italic"), ("Symbol", 8)
#	tkFont.Font(family="Times", size=10, weight=tkFont.BOLD)	more options - slant, underline, overstrike
#	system fonts

# tkMessageBox.askyesno('','Are you sure?', parent = root)
# tkSimpleDialog.askstring('title', 'Enter smth')

# btn['text']= 'New button name' OR b.config(text = 'New name')
#

# obj.config()  вернет все опции для объекта
# obj.config(opt)  вернет опцию как tuple с (option name, option database key, option database class, default value, current value)
# obj.cget(opt) или obj[opt]  вернет опцию как строку
# obj.config(opt=value)  изменит опцию


# <binding events>
# http://effbot.org/tkinterbook/tkinter-events-and-bindings.htm
# http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/event-types.html
# https://stackoverflow.com/a/32289245
# widget.bind(event_type, callback)
# Button-1, ButtonRelease-1, Double-Button-1 (2 middle, 3 right)
# Motion, B1-Motion, ..
# Enter, Leave (the mouse entered/left the widget)
# FocusIn, FocusOut
# Configure
# KeyPress (Key), KeyRelease (for any key)
# Shift-Up
# any letter (with or without <>)
# Return (the Enter key), Cancel(the Break key на линуксе не работает), BackSpace, Tab, Shift_L , Control_L , Alt_L (any key), Pause,
#   space, Caps_Lock, Escape, Prior (Page Up), Next (Page Down), End, Home, Left, Up, Right, Down, Print, Insert, Delete,
#   F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, Num_Lock, and Scroll_Lock.
# можно например так, KeyPress-Control_L, KeyPress-z, KeyPress-Z
# event attributes
# for a in (a_ for a_ in dir(event) if a_[0]!='_'): print(a, getattr(event, a))
# если вернуть из обработчика 'break', то евент не будет обрабатываться далее
