#!/usr/bin/env python3

# шлем wakeonlan magic packet
# https://ru.wikipedia.org/wiki/Wake-on-LAN

import argparse
import socket



def get_args(args = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('--ip', default='255.255.255.255', help='Ip address to send')
	parser.add_argument('--port', type=int, default=9, help='Port to send')
	parser.add_argument('mac', help='Mac address')
	return parser.parse_args(args)


def magic_packet(mac):
	mac_list = list(map(lambda s: int(s, 16), mac.split(':')))
	mac_data = bytes(mac_list)
	packet = b'\xff'*6 + mac_data*16
	return packet


args = get_args()
print(f'Sending magic packet with mac {args.mac} to {args.ip}:{args.port}')

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)		# UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)	# TODO check multicast
sock.sendto(magic_packet(args.mac), (args.ip, args.port))
