# считаем потерю зп

import argparse


def get_args(arg_src = None):
	parser = argparse.ArgumentParser()

	parser.add_argument('salary',             type=int, help='Зарплата')
	parser.add_argument('month_work_days',    type=int, help='Рабочих дней в месяце')
	parser.add_argument('vacation_days',      type=int, help='Всего дней отпуска')
	parser.add_argument('vacation_work_days', type=int, help='Дней отпуска выпадающих на рабочие дни')

	return parser.parse_args(arg_src)

a = get_args()

# для точного подсчета отпускных надо брать среднее за 12 месяцев значение, пренебрежем
vac_pay_part = a.salary / 29.3 * a.vacation_days	# отпускные
work_pay_part = a.salary / a.month_work_days * (a.month_work_days - a.vacation_work_days)

total_sum = vac_pay_part + work_pay_part
loss = a.salary - total_sum
loss_perc = loss / a.salary * 100

print(f'You get {total_sum:.2f}. Loss {loss:.2f} ({loss_perc:.2f}%)')
