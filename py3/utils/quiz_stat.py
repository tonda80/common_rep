#!/usr/bin/env python3

# типа демо по web scraping-у, но заодно и парсим рейтинг квиз-плиз команд
# варианты билиотек: lxml, BeautifulSoup. И то и то пригодно, но загадочно


import requests
import lxml.etree				# sudo pip3 install lxml
#from bs4 import BeautifulSoup	# sudo pip3 install beautifulsoup4


from baseapp import BaseConsoleApp
from mix_utils import Struct



BASE_URL = 'https://saratov.quizplease.ru/rating?QpRaitingSearch[general]={general}&QpRaitingSearch[league]={league}&QpRaitingSearch[text]=&page={{page}}'

class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-g', '--games', type=int, choices=(1, 2), default=1, help='Игры. 1 - обычные, 2 - кино и музыка')
		self.parser.add_argument('-p', '--period', type=int, choices=(1, 0), default=1, help='Период. 0 - за сезон, 1 - за все время')
		self.parser.add_argument('-mp', '--max_page', type=int, default=999, help='Ограничения количества просматриваемых страниц')
		self.parser.add_argument('--start_page', type=int, default=1)
		self.parser.add_argument('-mg', '--min_games', type=int, default=1, help='Ограничение по числу игр команды')
		self.parser.add_argument('-o', '--out_csv', default='quiz_out.csv', help='Выходной csv файл')

	def app_init(self):
		self.url = BASE_URL.format(general=self.args.period, league=self.args.games)
		self.commands = []
		self.parse_stop = False

	def main(self):
		page = self.args.start_page
		while page <= self.args.max_page and not self.parse_stop:
			self.parse_page(page)
			page += 1

		self.log.info(f'total commands {len(self.commands)}')

		self.write_result_in_csv()

	def parse_page(self, page):
		url = self.url.format(page=page)
		resp = requests.get(url)
		if resp.status_code != 200:
			raise RuntimeError(f'bad responce {resp.status_code} for url {url}')

		#soup = BeautifulSoup(resp.text, 'lxml')
		#for tag in soup.find_all('div', {'class': "item"}):

		tree = lxml.etree.HTML(resp.text)
		for item in tree.xpath('/html/body/div/section/div/div/div/div[@class="item"]'):
			self.parse_command_item(item)
			if self.parse_stop:
				break

		# возможно правильнее было бы искать 4 массива столбцов, т.е. было бы 4 запроса на страницу
		# но не факт, и уточнять лень, все равно основное время тратится на скачивание

		self.log.info(f'parsed page {page}')

	def parse_command_item(self, item):
		td1_span = item.find('./div/div[@class="rating-table-row-td1 rating-table-game-name"]/span')
		pos = int(td1_span.find('./strong').text)
		name = td1_span.tail.strip().replace('\t', ' ')

		td2_span = item.find('./div/div[@class="rating-table-row-td2 rating-table-kol-game"]/span')
		games = int(td2_span.tail)

		td3_span = item.find('./div/div[@class="rating-table-row-td3 rating-table-points"]/span')
		points = float(td3_span.tail)

		self.log.debug(f'parsed command: {pos}, {name}, {games}, {points}')
		self.add_command(pos, name, games, points)

	def add_command(self, pos, name, games, points):
		if self.commands and self.commands[-1].pos >= pos:
			self.parse_stop = True		# дошли до конца
			self.log.info(f'{pos} is repeated, looks like this is the end')
			return
		if games < self.args.min_games:		# не будем заморачиваться и пренебрежем командами с меньшим числом очков, но возможно большим числом игр
			self.parse_stop = True
			self.log.info(f'{games} games it is too little, looks like this is the end')
			return

		self.commands.append(Struct(pos=pos, name=name, games=games, points=points))


	def write_result_in_csv(self):
		with open(self.args.out_csv, 'w') as of:
			for cmd in self.commands:
				av_points = cmd.points/cmd.games
				of.write(f'{cmd.pos};{cmd.name};{cmd.games};{cmd.points};{av_points:.2f}\n')



if __name__ == '__main__':
	App().start()
