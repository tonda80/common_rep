#!/usr/bin/env python3

# снова игра жизнь
# планы сделать бесконечное поле и замкнутое


from baseapp import BaseConsoleApp
import uTkinter as utk
import grid_field


class LifeInfiniteField:
	def __init__(self, update_clb, survival_cond = (2, 3), born_cond = (3,)):
		self.rows = {}
		self.columns = {}

		self.update_clb = update_clb

		self.survival_cond = survival_cond
		self.born_cond = born_cond

	def add_unit(self, row, col):
		self.rows.setdefault(row, {})[col] = True
		self.columns.setdefault(col, {})[row] = True

	def remove_unit(self, row, col):
		if row in self.rows:
			self.rows.get(row).pop(col)
		if col in self.columns:
			self.columns.get(col).pop(row)

	def has_unit(self, row, col):
		return row in self.rows and self.rows.get(row).get(col)

	def count_neighbours(self, row, col):
		ret = 0
		if row in self.rows:
			if self.rows.get(row).get(col-1):
				ret += 1
			if self.rows.get(row).get(col+1):
				ret += 1
		for r in (row-1, row+1):
			if r in self.rows:
				for c in (col-1, col, col+1):
					if self.rows.get(r).get(c):
						ret += 1
		return ret

	def clean_up(self):
		# убираем пустые строки и столбцы
		# главное удалять крайние, иначе при анализе поля будем тратить время на анализ строк\столбцов в которых заведомо ничего не может быть
		def remove_empty(coll):
			empty = []
			for it in coll:
				if not it:
					empty.append(it)
			for it in empty:
				del coll[it]

		remove_empty(self.rows)
		remove_empty(self.columns)


	def analyse_area(self):
		def min_max(coll):
			return min(coll), max(coll)

		min_row, max_row = min_max(self.rows)
		min_col, max_col = min_max(self.columns)

		if not (self.rows[min_row] and self.rows[max_row] and self.columns[min_col] and self.columns[max_col]):
			self.clean_up()

		min_row, max_row = min_max(self.rows)
		min_col, max_col = min_max(self.columns)

		return min_row-1, max_row+2, min_col-1, max_col+2 		# отдаем [) диапазон

	def make_step(self):
		born_units = []
		lost_units = []

		row_start, row_end, col_start, col_end = self.analyse_area()

		for r in range(row_start, row_end):
			r_exists = r in self.rows
			for c in range(col_start, col_end):
				q_neighbours = self.count_neighbours(r, c)
				if r_exists and self.rows.get(r).get(c):	# self.has_unit(), но наверное чуть быстрее
					if q_neighbours not in self.survival_cond:
						lost_units.append((r, c))
				else:
					if q_neighbours in self.born_cond:
						born_units.append((r, c))

		for it in born_units:
			self.add_unit(*it)
		for it in lost_units:
			self.remove_unit(*it)

		self.update_clb(born_units, lost_units)



class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('--field_type', default='inf', choices=('inf', 'sphere'), help='Тип поля')

	def main(self):
		if self.args.field_type == 'inf':
			FieldClass = LifeInfiniteField
		else:
			raise NotImplementedError
		self.life_model = FieldClass(self.game_change_handler)

		root = self.create_gui()
		root.mainloop()

	def create_gui(self):
		root = utk.uTk(title='Life game', createStatus=False)
		root_width = root.winfo_screenwidth() - 50
		root_height = root.winfo_screenheight() - 100
		root.minsize(root_width, root_height)

		self.life_view = grid_field.GridField(root)
		self.unit_patetrn = grid_field.CircleItem(self.life_view, utk.rgb(10, 100, 20), 0.8)
		cell_size = 30
		self.life_view.reset(root_height//cell_size, root_width//cell_size)
		# handlers
		self.life_view.add_mouse_handler('<Button-1>', self.click_handler)

		control_frame = utk.uFrame(root, gmArgs={'side': utk.BOTTOM, 'expand': utk.NO, 'fill': utk.X})
		btn_gmArgs = {'side': utk.LEFT}
		utk.uButton(control_frame, self.step, 'Step', width = 7, gmArgs=btn_gmArgs)

		return root

	def game_change_handler(self, born_units, lost_units):
		for u in born_units:
			# TODO как-то придумать подстройку для бесконечного поля (скролл, уменьшение\сдвиг экрана)
			try:
				self.life_view.add(self.unit_patetrn, *u)
			except grid_field.GridFieldError:
				pass
		for u in lost_units:
			try:
				self.life_view.remove(*u)
			except grid_field.GridFieldError:
				pass

	def click_handler(self, row, col, event):
		if self.life_model.has_unit(row, col):
			self.life_model.remove_unit(row, col)
			self.life_view.remove(row, col)
		else:
			self.life_model.add_unit(row, col)
			self.life_view.add(self.unit_patetrn, row, col)

	def step(self):
		self.life_model.make_step()


if __name__ == '__main__':
	App().main()

