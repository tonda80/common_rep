#!/usr/bin/env python3

# утилита упрощающая взаимодействие с клавиатурой
#
# на линуксе требует дополнительный пакет
# https://pyperclip.readthedocs.io/en/latest/introduction.html#not-implemented-error
# возможно xsel лучше

import platform
import time

import keyboard			# pip3 install keyboard
import clipboard		# pip3 install clipboard; apt install xclip

from baseapp import BaseConsoleApp


_win = platform.system() == 'Windows'
_linux = platform.system() == 'Linux'


def print_event(e, *a):
	print(f'{e.name} ({e.scan_code:X}) {e.event_type}', *a)		# time device is_keypad modifiers to_json


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('--selected_translate_key', default='pause', help='Клавиша для перевода выделенного текста')
		#self.parser.add_argument('--translate_key', default='scroll lock', help='Клавиша для перевода набираемого текста')

	def init_translation(self):
		# набранные по порядку все символы на клавиатуре которые есть в русской раскладке, сперва с шифтом потом без ("№ итд только с шифтом)
		ru = 'ёйцукенгшщзхъфывапролджэячсмитьбю.Ё"№;:?ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭ/ЯЧСМИТЬБЮ,'
		en = '`qwertyuiop[]asdfghjkl;\'zxcvbnm,./~@#$^&QWERTYUIOP{}ASDFGHJKL:"|ZXCVBNM<>?'
		assert len(ru) == len(en) == len(set(ru)) == len(set(en)) == 74, f'Symbol table error {len(ru)} {len(en)}'

		self.ru_en_dict = dict(zip(ru, en))
		self.en_ru_dict = dict(zip(en, ru))
		self.ru_letters = tuple(k for k in self.ru_en_dict if k.isalpha())
		self.en_letters = tuple(k for k in self.en_ru_dict if k.isalpha())
		#print(self.ru_en_dict, self.en_ru_dict, (len(self.ru_letters),)+self.ru_letters, (len(self.en_letters),)+self.en_letters, sep='\n\n')

	def translate(self, buf):
		if any(map(lambda c: c in self.ru_letters, buf)):
			dict_ = self.ru_en_dict
		elif any(map(lambda c: c in self.en_letters, buf)):
			dict_ = self.en_ru_dict
		else:
			return buf

		return ''.join(dict_.get(c, c) for c in buf)

	def init_tracking(self):
		self.input_buf = []

		self.reset_buf_codes = (0x39, 0xf, 0x1c)	# space, tab, enter


	def main(self):
		self.init_translation()

		#keyboard.hook(print_event)	# debug

		if _win:	# на линуксе это легко включается в настройках, не колхозим
			keyboard.hook_key('caps lock', self.on_caps_lock, True)

		if self.args.selected_translate_key:
			keyboard.hook_key(self.args.selected_translate_key, self.on_selected_translate_key, True)

		#if self.args.translate_key:
		#	self.init_tracking()
		#	keyboard.hook(self.on_any_key)
		#	keyboard.hook_key(self.args.translate_key, self.on_translate_key, True)
		# че то пока непонятно как такое реализовывать :(

		keyboard.wait()

	def delay(self):
		if _linux:
			time.sleep(0.1)

	def copy_selected(self):
		clipboard.copy('')
		keyboard.press_and_release('ctrl+c')
		self.delay()
		ret = clipboard.paste()
		return ret

	def paste(self, s):
		clipboard.copy(s)
		keyboard.press_and_release('ctrl+v')

	def switch_layout(self):
		if _win:
			keyboard.press_and_release('ctrl+shift')
		elif _linux:
			keyboard.press_and_release('caps lock')


	def on_caps_lock(self, event):
		if _win:
			if keyboard.is_pressed('shift'):	# таки жмем капс
				keyboard.press_and_release('caps lock', event.event_type == 'down', event.event_type == 'up')
			elif event.event_type == 'up':		# переключаем раскладку
				self.switch_layout()

	def on_selected_translate_key(self, event):
		if event.event_type == 'up':
			buf = self.copy_selected()
			if buf:
				self.paste(self.translate(buf))
				self.switch_layout()
			else: print('__deb empty buf')
		pass#else: print_event(event, keyboard.is_pressed('shift'))

	def on_any_key(self, ev):
		if ev.event_type == 'down':
			print(f'{ev.name} ({ev.scan_code:X})')

	def on_translate_key(self, event):
		pass



if __name__ == '__main__':
	try:
		App().main()
	except (SystemExit, KeyboardInterrupt):
		pass
