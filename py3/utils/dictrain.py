#!/usr/bin/env python3

# утилита для тренировочных диктантов (quiz2 в listenaminute)
#
# pip3 install python-vlc (требует установки нативной vlc)
# apt install vlc (и возможно unifont если будут проблемы с рендерингом глифов)

# TODO? загружать mp3 и субтитры с ютуба
# а пока так: https://www.y2meta.com/ru27 и https://downsub.com/

# TODO справка по логике сравнения и keys
# TODO запоминать позицию файла при закрытии



import os
import time
import enum
import tkinter.filedialog
import tkinter.messagebox
import pickle
#
import uTkinter as u
from baseapp import BaseTkApp
from commonconfig import CommonConfig
import mix_utils
#
import vlc


# ---- Медиа ----

# отвяжем тут конкретную реализацию (сейчас vlc)
class MediaPlayer():
	def __init__(self):
		# наследоваться от vlc.MediaPlayer не получается из-за __new__, поэтому так
		self._mp = vlc.MediaPlayer()
		# set_position/get_position принимает/возвращает число [0, 1]
		for attr in ('pause', 'stop', 'set_position', 'get_position'):
			setattr(self, attr, getattr(self._mp, attr))

	def load(self, path):
		# выбирает файл для пригрывания, возвращает длительность в миллисекундах или None
		if os.path.isfile(path):
			media = vlc.Media(path)
			media.parse()
			duration = media.get_duration()
			if duration > 0:
				self._mp.set_media(media)
				return duration

	# вроде и не нужно, но пусть будет
	def _check_media(self, media):
		# https://stackoverflow.com/questions/11450981/verify-mediafiles-with-libvlc-and-python
		media.parse()	# get media info
		if media.get_duration() > 0:
			return True

	# может и не нужно
	def unload(self):
		self._mp.set_media(None)

	def play(self, rew_if_ended = True):
		if rew_if_ended and self.state() == MediaPlayer.State.Ended:
			self.stop()
		self._mp.play()

	# vlc.State
	# for n in filter(lambda s:s[0].isupper(), dir(vlc.State)): print(n, getattr(vlc.State, n).value)
	class State(enum.IntEnum):
		NothingSpecial = 0
		Opening = 1
		Buffering = 2
		Playing = 3
		Paused = 4
		Stopped = 5
		Ended = 6
		Error = 7

	def state(self):
		return MediaPlayer.State(self._mp.get_state())

	def media_duration(self):
		media = self._mp.get_media()
		if media:
			duration = media.get_duration()
			if duration > 0:
				return duration
		return 0

	def get_position_ms(self):
		pos = max(self._mp.get_position(), 0)
		return pos*self.media_duration()

	def set_position_ms(self, ms):
		duration = self.media_duration()
		if duration > 0:
			if ms < 0:
				ms = 0
			elif ms > duration:
				ms = duration
			pos = ms/duration
			self._mp.set_position(pos)
			#print('__deb', pos, ms, duration)

	def is_playing(self):
		# наверное с юзерской точки зрения, открытие и буферизация - уже проигрывание
		int_state = self._mp.get_state().value
		return 0 < int_state < 4



# ---- Представление текста ----


class TextPuzzle:

	class Word:
		def __init__(self, s):
			self.value = s
			self.view_letters = [[not c.isalnum(), c] for c in s]	# open: letter
			self.open = False

			self.comp_value = self.comparable(s)
			if not self.comp_value:
				self.open = True

		@staticmethod
		def comparable(s):
			# нижний регистр, удалим не буквы\цифры
			ret = ''.join(c for c in s.casefold() if c.isalnum())
			return ret

		def open_if_equal(self, s):
			#print('__deb', self.comp_value, s)
			if not self.open and self.comp_value == s:		# s уже comparable
				self.open = True
				return True

		def view(self, show_len):
			if self.open:
				return self.value
			elif show_len:
				return ''.join(l[1] if l[0] else '*' for l in self.view_letters)
			# формируем представление без длины (может как-то упостить это?)
			unk_part = ''
			res = ''
			for l in self.view_letters:
				if l[0]:
					res += unk_part + l[1]
					unk_part = ''
				else:
					unk_part = '_'
			res += unk_part
			#print('__deb', self.value, res)
			return res

		def hint(self):
			was_opened = False
			for l in self.view_letters:
				if not l[0]:
					if was_opened:	# открыли символ и есть неоткрытый
						return False
					l[0] = True
					was_opened = True

			self.open = True
			return True



	class Stats:
		def __init__(self):
			self.successes = set()
			self.fails = set()
			self.cnt_fail = 0

		def reset(self):
			self.successes.clear()
			self.fails.clear()
			self.cnt_fail = 0

		def add_event(self, ok, word):
			if ok:
				coll = self.successes
			else:
				coll = self.fails
				self.cnt_fail += 1

			if word not in coll:
				coll.add(word)
				return True

		def report(self):
			return f'''
Guessed words: {len(self.successes)}
Failed words: {len(self.fails)}
Total fails: {self.cnt_fail}
'''

	def __init__(self):
		self.words = []
		self.last_closed = -1
		self.stats = TextPuzzle.Stats()

	PICKLE_FILE = mix_utils.path_to(__file__, 'dicttrain_data.bin')

	def dump_state(self):
		pickle.dump((self.words, self.last_closed), open(self.PICKLE_FILE, 'wb'))

	def __load_state(self):
		if os.path.isfile(self.PICKLE_FILE):
			self.words, self.last_closed = pickle.load(open(self.PICKLE_FILE, 'rb'))
			self.refresh()

	def late_init(self, text_wdgt, chbtns):
		# utk виджеты для взаимодействия
		self.text_widget = text_wdgt
		self.fill_everyplace_chbtn = chbtns[0]
		self.show_word_len_chbtn = chbtns[1]

		self.__load_state()

	def refresh(self):
		text = ' '.join(w.view(self.show_word_len_chbtn.getVar()) for w in self.words)
		self.text_widget.clear_all()
		self.text_widget.append(text)

	def load(self, text):
		def split(text):
			for ss in text.split(' '):
				sss = ss.splitlines()
				if len(sss) == 1:
					yield ss
				else:
					for i, s in enumerate(sss):
						if i > 0:
							s = '\n' + s
						yield s

		self.words.clear()
		for w in split(text):
			self.words.append(TextPuzzle.Word(w))
		self.last_closed = 0 if self.words else -1
		self.stats.reset()
		self.refresh()


	class CheckResult(enum.IntEnum):
		NotChecked = 0
		Ok = 1
		Fail = 2
		Repeat = 3

	def check(self, word):
		if self.last_closed < 0 or self.words[self.last_closed].open:	# не задан текст или все открыто
			return TextPuzzle.CheckResult.NotChecked

		word = TextPuzzle.Word.comparable(word)

		fill_everyplace = self.fill_everyplace_chbtn.getVar()
		if fill_everyplace:
			ok = self._open_all(word, self.last_closed) > 0
		else:
			ok = self.words[self.last_closed].open_if_equal(word)

		is_new = self.stats.add_event(ok, word)

		if ok:
			self._last_closed_up()
			self.refresh()
			return TextPuzzle.CheckResult.Ok
		elif is_new or not fill_everyplace:		# если заполняем по порядку, то неправильный ввод ошибка даже когда он повторяется
			return TextPuzzle.CheckResult.Fail

		return TextPuzzle.CheckResult.Repeat

	def hint(self):
		res = self.words[self.last_closed].hint()
		if res:
			self._last_closed_up()
		self.refresh()
		return res


	def _open_all(self, word, i):
		assert i >= 0
		open_cnt = 0
		while i < len(self.words):
			if self.words[i].open_if_equal(word):
				open_cnt += 1
			i += 1
		return open_cnt

	def _last_closed_up(self):
		while self.last_closed < len(self.words) - 1 and self.words[self.last_closed].open:
			self.last_closed += 1

	def is_done(self):
		return self.last_closed == len(self.words) - 1 and self.last_closed > -1 and self.words[self.last_closed].open


# ---- Приложение ----

class App(BaseTkApp):
	def app_init(self):
		app_name = 'dictrain_220121'

		self.MEDIA_TICK_PERIOD = 250

		self.config = CommonConfig(app_name,
			last_media=None,
			media_jump = 1500,
			media_long_jump = 15000
		)

		self.player = MediaPlayer()
		self.text_puzzle = TextPuzzle()
		self.input_history = App.InputHistory()

	def save_config(self):
		self.text_puzzle.dump_state()
		super().save_config()

	def report_error(self, msg):
		self.log.error(msg)
		self.set_status_bar(msg)

	def create_gui(self):
		root = u.uTk(u'dictrain', 1000, 600, createStatus=True)

		self.create_media_gui(u.uFrame(root, gmArgs={'side': u.TOP, 'fill': u.X}))
		self.create_text_gui(u.uFrame(root, gmArgs={'side': u.TOP, 'fill': u.BOTH, 'expand': u.YES}))

		self.create_key_binding(root)
		root.set_delete_callback(self.save_config)
		return root

	def create_key_binding(self, root):
		root.bind('<Return>', self.check_word)
		root.bind('<Up>', self.history_up)
		root.bind('<Down>', self.history_down)
		root.bind('<F2>', lambda ev: self.checked_entry.set_var(''))

		# хотим чтобы некоторые "текстовые кнопки" управляли воспроизведением и не попадали в поле ввода checked_entry, если не alt
		def add_media_key(key, media_action):
			root.bind(key, lambda ev: self.root.is_alt_l_pressed or media_action(ev))
			self.checked_entry.bind(key, lambda ev: self.root.is_alt_l_pressed or media_action(ev) or 'break')
		add_media_key('<space>', self.play_pause_media)
		add_media_key('<Left>', self.rewind_media)		# ',' '<Left>'
		add_media_key('<Right>', self.forward_media)		# '.' '<Right>'

		u.trace_control_l(root)
		u.trace_alt_l(root)

	# ----

	def create_media_gui(self, mdFrame):
		f1 = u.uFrame(mdFrame, relief=u.FLAT, gmArgs={'side': u.TOP, 'fill':u.X})
		mdBtnGmArgs = {'side': u.LEFT, 'padx': 2, 'pady': 15}
		mdBtnArgs = {'width': 0, 'height': 1, 'gmArgs': mdBtnGmArgs}
		u.uButton(f1, self.play_pause_media, '\u25b6/\u23f8', **mdBtnArgs)
		#u.uButton(f1, self.pause_media, '\u23f8', **mdBtnArgs)
		u.uButton(f1, self.stop_media, '\u25a0', **mdBtnArgs)	# u23fe не работает
		u.uButton(f1, self.rewind_media, '\u23ea', **mdBtnArgs)
		u.uButton(f1, self.forward_media, '\u23e9', **mdBtnArgs)
		u.uButton(f1, self.open_media_settings, '\u2699', **mdBtnArgs)
		u.uButton(f1, self.load_media, '\u23cf', **mdBtnArgs)
		# https://en.wikipedia.org/wiki/Media_control_symbols
		# \ufe0f - forces it to be rendered as a colorful image. но что-то не работает

		self.media_state = u.uMutableLabel(f1, '', width=15, anchor=u.SE, font=('default', 7), gmArgs={'side': u.RIGHT, 'padx': 2})
		self.media_time = u.uMutableLabel(f1, '', gmArgs={'side': u.RIGHT, 'padx': 10})
		self.media_file = u.uMutableLabel(f1, '', gmArgs={'side': u.RIGHT, 'padx': 20})

		#u.uSeparator(mdFrame)
		self.media_slider = u.uScale(mdFrame, 0, 0, orient='h', command=self.on_media_slider_moved,
			resolution=500, ValueType=u.IntVar, showvalue=False, gmArgs={'side': u.TOP, 'fill': u.X, 'padx': 5, 'pady': 5})

		self._load_media_path(self.config.get('last_media'))
		self.media_after_id = None
		self.media_slider_was_moved = False

		self.media_jump_var = u.IntVar()

	def media_update_labels(self):
		def _time_str(ms):
			min_, sec = divmod(ms/1000, 60)
			return f'{int(min_)}:{int(sec):02d}'
		self.media_time.setVar(f'{_time_str(self.player.get_position_ms())}/{_time_str(self.player.media_duration())}')
		self.media_state.setVar(self.player.state().name)

	def load_media(self):
		last_media = self.config.get('last_media')
		path = tkinter.filedialog.askopenfilename(title='Выбор медиа файла', initialdir=os.path.dirname(last_media) if last_media else '~')
		self._load_media_path(path)

	def _load_media_path(self, path):
		if path and os.path.isfile(path):
			duration = self.player.load(path)
			if duration > 0:
				self.media_file.setVar(os.path.basename(path))
				self.media_update_labels()
				self._reset_slider(duration)
				self.config.set('last_media', path)
				self.log.debug(f'loaded media file {path} with duration={duration}ms')
			else:
				self.report_error(f'Не могу загрузить медиа {path}')

	def _reset_slider(self, duration):
		self.media_slider.config(to=int(duration))
		self.media_slider.setVar(0)
		self.media_slider_was_moved = False

	def on_media_slider_moved(self, value):
		#print('__deb', type(value), value, self.media_slider.getVar(), type(self.media_slider.getVar()))	# интересный спецэффект, getVar => int, а в callback приходит str
		self.media_slider_was_moved = True

	def media_tick(self):
		#print('__deb tick')
		self.media_update_labels()
		if self.player.is_playing():
			if self.media_slider_was_moved:
				self.player.set_position_ms(self.media_slider.getVar())
				self.media_slider_was_moved = False
			else:
				self.media_slider.setVar(self.player.get_position_ms())
			self.media_after_id = self.root.after(self.MEDIA_TICK_PERIOD, self.media_tick)
		else:
			self.media_after_id = None
			self.log.debug(f'Stopped after chain: {self.player.state().name}')

	def open_media_settings(self):
		w = u.uToplevel(self.root, True, 'Media settings')
		grid_layout = u.GridLayout(w, 0, 2)
		u.uLabel(w, '', gmArgs={'columnspan':2})
		# vvv
		u.uLabel(w, 'Jump', gmArgs={'sticky': u.W})
		e_media_jump = u.uEntry(w, width=8, defaultValue=self.config.get('media_jump'), ValueType=u.IntVar, gmArgs={'sticky': u.NSEW})
		u.uLabel(w, 'Long jump', gmArgs={'sticky': u.W})
		e_media_long_jump = u.uEntry(w, width=8, defaultValue=self.config.get('media_long_jump'), ValueType=u.IntVar, gmArgs={'sticky': u.NSEW})
		#
		def save_settings():
			try:
				media_jump = e_media_jump.getVar()
				media_long_jump = e_media_long_jump.getVar()
			except:
				err_label.setVar(' Invalid settings!!!')
				return
			self.config.set('media_jump', media_jump)
			self.config.set('media_long_jump', media_long_jump)
			w.destroy()
		err_label = u.uMutableLabel(w, '', anchor=u.E, gmArgs={'columnspan':2, 'sticky': u.W})
		u.uButton(w, save_settings, 'Ok')
		u.uButton(w, w.destroy, 'Cancel')
		#
		grid_layout.make_stretchy()

	def play_pause_media(self, ev=None):
		if self.player.state() == MediaPlayer.State.Playing:
			self.player.pause()
		else:
			self.player.play()
			if not self.media_after_id:
				self.log.debug('Started after chain')
				self.media_after_id = self.root.after(self.MEDIA_TICK_PERIOD, self.media_tick)

	def stop_media(self):
		self.player.stop()
		self.media_slider.setVar(0)

	def _jump_media(self, plus):
		if self.root.is_control_l_pressed:
			jump = self.config.get('media_long_jump')
		else:
			jump = self.config.get('media_jump')
		pos = self.media_slider.getVar() + jump*(1 if plus else -1)
		self.media_slider.setVar(pos)
		self.media_slider_was_moved = True

	def rewind_media(self, ev=None):
		self._jump_media(False)

	def forward_media(self, ev=None):
		self._jump_media(True)

	# ----

	def create_text_gui(self, txFrame):
		ft = u.uFrame(txFrame, relief=u.FLAT, gmArgs={'side': u.RIGHT, 'fill': u.BOTH, 'expand': u.YES})	#, bg='red'
		fc1 = u.uFrame(txFrame, relief=u.FLAT, gmArgs={'side': u.TOP, 'fill': u.X})
		fc2 = u.uFrame(txFrame, relief=u.FLAT, gmArgs={'side': u.BOTTOM, 'fill': u.X})

		u.y_spacer(fc1, 10)

		btn1Args = {'gmArgs': {'side': u.TOP, 'padx': 0, 'pady': 0, 'fill': u.X}}	#'anchor': u.CENTER,
		u.uButton(fc1, self.load_text_from_buffer, 'Load from buffer', **btn1Args)
		u.uButton(fc1, self.load_text_from_file, 'Load from file', **btn1Args)

		u.y_spacer(fc1, 25)

		cbArgs = {'anchor': u.W, 'gmArgs': {'side': u.TOP, 'padx': 0, 'pady': 1, 'fill': u.X}}
		cb1 = u.uCheckbutton(fc1, 'Fill everyplace', callback = self.text_puzzle.refresh, **cbArgs)
		cb2 = u.uCheckbutton(fc1, 'Show word lengths', callback = self.text_puzzle.refresh, **cbArgs)

		self.init_kept_uvar(cb1, 'fill_everyplace_chbtn')
		self.init_kept_uvar(cb2, 'show_word_len_chbtn')

		self.checked_entry = u.uEntry(fc2, gmArgs={'side': u.TOP, 'fill': u.X, 'expand': u.YES})

		u.uButton(fc2, self.check_word, 'Check', width=5, height=0, gmArgs={'side': u.LEFT, 'padx': 5, 'pady': 5})
		u.uButton(fc2, self.hint, 'Hint', width=5, height=0, gmArgs={'side': u.RIGHT, 'padx': 5, 'pady': 5})

		text_font = ('Arial', 12)		# TODO? customize
		tw = u.uText(ft, state=u.DISABLED, font=text_font, gmArgs={'fill': u.BOTH, 'expand': u.YES})

		self.text_puzzle.late_init(tw, (cb1, cb2))

	def load_text_from_buffer(self):
		if not tkinter.messagebox.askyesno('Точно ли', 'Загрузить текст из буфера?'):
			return
		text = self.clipboard_get()
		if text:
			self.text_puzzle.load(text)
		else:
			self.report_error('Empty clipboard!')

	def load_text_from_file(self):
		last_media = self.config.get('last_media')
		path = tkinter.filedialog.askopenfilename(title='Выбор файла с текстом', initialdir=os.path.dirname(last_media) if last_media else '~')
		if path and os.path.isfile(path):
			with open(path) as f:
				self.config.set('last_media', path)
				try:
					text = f.read()
				except:
					self.report_error('File reading error!')
					return
				self.text_puzzle.load(text)


	class InputHistory:
		def __init__(self):
			self.storage = []
			self.curr = -1

		def add(self, word):
			if word:
				if word in self.storage:
					if word == self._get_curr():		# перемотали сюда, ничего не делаем
						return
					else:
						self.storage.remove(word)		# заново ввели, переносим в конец
				self.storage.append(word)
				self.curr = len(self.storage) - 1

		def _get_curr(self):
			return self.storage[self.curr] if self.curr > -1 else ''	# не может стать больше длины-1

		def up(self):
			if self.curr > 0:
				self.curr -= 1
			return self._get_curr()

		def down(self):
			if self.curr < len(self.storage) - 1:
				self.curr += 1
			return self._get_curr()


	def history_up(self, ev=None):
		self.checked_entry.setVar(self.input_history.up())
		self.checked_entry.select_clear()
		self.checked_entry.icursor(u.END)

	def history_down(self, ev=None):
		self.checked_entry.setVar(self.input_history.down())
		self.checked_entry.select_clear()
		self.checked_entry.icursor(u.END)


	def check_word(self, ev=None):
		self.checked_entry.focus_set()
		self.checked_entry.select_all()

		checked = self.checked_entry.getVar().strip()
		if checked:
			self.input_history.add(checked)
			check_res = self.text_puzzle.check(checked)
			self.log.debug(f'check result = {check_res}, position = {self.text_puzzle.last_closed}')

			if check_res == TextPuzzle.CheckResult.Fail:
				self.report_error(f"There is no '{checked}' here")
			elif check_res == TextPuzzle.CheckResult.Ok and self.text_puzzle.is_done():
				tkinter.messagebox.showinfo('Congratulations!', self.text_puzzle.stats.report())

	def hint(self, ev=None):
		if self.text_puzzle.hint() and self.text_puzzle.is_done():
			tkinter.messagebox.showinfo('Congratulations!', self.text_puzzle.stats.report())


if __name__ == '__main__':
	try:
		App().mainloop()
	except (KeyboardInterrupt, ):
		pass
