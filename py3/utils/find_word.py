#!/usr/bin/env python3

# ищем слова в словаре
# # http://speakrus.ru/dict/index.htm http://blog.harrix.org/article/3334


import zipfile
import itertools
import io

from baseapp import BaseConsoleApp
import mix_utils


class App(BaseConsoleApp):
	def add_arguments(self):
		l_parser = self.add_subparser('l', self.find_by_letters_main, help='ищем слова по заданным буквам')
		s_parser = self.add_subparser('s', self.find_by_syllables_main, help='ищем слова с заданным количеством слогов')
		w_parser = self.add_subparser('w', self.find_words_in_word_main, help='ищем слова в другом слове')

	def main(self):
		self.init_dicts()
		print()
		try:
			self.sub_main()
		except KeyboardInterrupt:
			print('\nbye..')

	def init_dicts(self):
		zip_path = mix_utils.path_to(__file__, '../../extra/zdf-win.txt.zip')
		extra_path = mix_utils.path_to(__file__, '../../extra/zdf-win.extra.txt')

		word_cnt = 0
		self.dict_all = set()
		self.dict_len = {}		# словарь с доступом по длине
		with zipfile.ZipFile(zip_path) as zip_f, zip_f.open(r'zdf-win.txt') as main_f, open(extra_path, encoding='utf-8') as extra_f:
			for s in itertools.chain(io.TextIOWrapper(main_f, encoding='utf-8'), extra_f):
				w = s.strip()
				self.dict_all.add(w)
				self.dict_len.setdefault(len(w), set()).add(w)
				word_cnt += 1

		self.log.info(f'[init_dicts] total words {word_cnt}')

	def get_syllable_quantity(self, word):
		q = 0
		for c in word:
			if c in 'уеыаоэяиюё':
				q += 1
		return q


	def find_by_letters_main(self):
		while 1:
			symbols = input('Буквы: ').lower()
			word_len = int(input('Длина слова: '))

			for word in self.dict_len[word_len]:
				for s in set(word):
					if word.count(s) > symbols.count(s):
						break
				else:
					print(word)
			print()

	def find_by_syllables_main(self):
		while 1:
			q_letters = int(input('Количество букв: '))
			q_syllable  = int(input('Количество слогов: '))

			word_result = []
			for word in self.dict_len[q_letters]:
				if self.get_syllable_quantity(word) == q_syllable:
					word_result.append(word)

			self._sort_and_print(word_result)

	def find_words_in_word_main(self):
		while 1:
			in_word = input('Слово: ').lower()

			word_result = set()
			len_ = len(in_word)
			for i in range(len_ - 1):
				for j in range(i + 2, len_ + 1):
					w = in_word[i:j]
					#print('_deb', w, i, j)
					if w in self.dict_all and w != in_word:
						word_result.add(w)

			self._sort_and_print(list(word_result))

	def _sort_and_print(self, list_):
		list_.sort()
		for word in list_:
			print (word)
		print()


if __name__ == '__main__':
	App().main()
