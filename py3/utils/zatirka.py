# https://www.isolux.ru/articles/index/view/id/76/
# (Длина + Ширина) / (Длина х Ширина) х Толщина х Ширина швов х 1.6 + 10% = Расход (кг/м2)

def zatirka(s_m2, l_mm, w_mm, t_mm, j_mm):
    k = (l_mm + w_mm) / (l_mm * w_mm) * t_mm * j_mm * 1.6 * 1.1
    return k*s_m2
