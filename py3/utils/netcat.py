#!/usr/bin/env python3
# coding=utf8

# утилитка для отладки сетевых соединений
# для теста с обратной стороны 'nc 127.0.0.1 PORT'
# для теста basic auth - r.get('https://jigsaw.w3.org/HTTP/Basic/', auth=('guest', 'guest'))  вообще см https://jigsaw.w3.org/HTTP/


import socket
import threading
import time

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('ports', nargs='+', help='listened ports')
		self.parser.add_argument('-e', '--echo', action='store_true', help='send echo message')
		self.parser.add_argument('--ip', default='127.0.0.1', help='ip to listening')

	def main(self):
		self.threads = []
		stop_work = threading.Event()
		for p in tuple(map(int, self.args.ports)):
			self.threads.append(threading.Thread(target = self.listen_thread, args = (p, stop_work)))
			self.threads[-1].start()

		try:
			for t in self.threads: t.join()
		except KeyboardInterrupt:
			self.log.info('Exiting')
			stop_work.set()
		for t in self.threads: t.join()

	def listen_thread(self, port, stop_work):
		timeout = 1
		with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as sock:
			sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			sock.settimeout(timeout)
			bind_addr = (self.args.ip, port)
			sock.bind(bind_addr)
			self.log.info(f'Listening to {bind_addr}')
			sock.listen()
			# input('wait')		# интересно что активный сокет создается уже здесь см netstat (ss)

			while not stop_work.is_set():
				try:
					conn, addr = sock.accept()
				except socket.timeout:
					continue

				self.log.info(f'Connection from {addr} to {port}')
				#self.log.info(f'conn_peer={conn.getpeername()} conn_sock={conn.getsockname()} sock_sock={sock.getsockname()}')	# sock.getpeername() - OSError: [Errno 107] Transport endpoint is not connected
				# conn.getpeername() gives addr value

				conn.settimeout(timeout)

				self.threads.append(threading.Thread(target = self.recv_thread, args = (conn, stop_work, port)))
				self.threads[-1].start()


	def recv_thread(self, conn, stop_work, port):
		while not stop_work.is_set():
			try:
				data = conn.recv(65536)
			except socket.timeout:
				continue
			except ConnectionResetError:
				break
			if not data:
				self.log.info(f'Connection closed: {port}')
				break
			try:
				self.process_data(data, conn)
			except Exception as e:
				self.log.exception(f'exception in process_data: {port}')

	def process_data(self, data, conn):
		self.log.info(f'{data} recieved. own {conn.getsockname()}. remote {conn.getpeername()}')
		if self.args.echo:
			conn.send(b'echoed: ' + data)




if __name__ == '__main__':
	App().main()
