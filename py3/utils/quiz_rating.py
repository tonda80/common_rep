#!/usr/bin/env python3

# ещё одно демо по web scraping-у, в quiz_stat.py более основательное что-то, но и запутанное
# тут просто смотрим сезонный рейтинг команд
# а ещё в py3/examples/e_async5_min.py есть поиск игры в истории)

import argparse
import aiohttp
import asyncio
import itertools
from bs4 import BeautifulSoup	# sudo pip3 install beautifulsoup4
from dataclasses import dataclass


@dataclass
class Command:
	pos: int
	name: str
	games: int
	points: float
	average: float = -1

	def __post_init__(self):
		self.average = self.points / self.games


def parse_page(page):
	soup = BeautifulSoup(page, "html.parser")
	rating_table = soup.find('div', class_='rating-table')
	html_items =  rating_table.find_all('div', class_='item')
	for item in html_items:
		#print('__deb', item.text, item)
		# парсить text было бы проще чем дальше влазить в html структуру item-а, но не получается (
		n_and_name = item.find('div', class_='rating-table-row-td1 rating-table-game-name')
		pos = int(n_and_name.find('strong').text)
		name = n_and_name.text.splitlines()[2].strip()
		games = int(item.find('div', class_='rating-table-row-td2 rating-table-kol-game').text.partition('Игры ')[2])
		points = float(item.find('div', class_='rating-table-row-td3 rating-table-points').text.partition('Баллы ')[2])
		yield Command(pos, name, games, points)


async def get_commands(session, page_num = 1):
	url = f'https://saratov.quizplease.ru/rating?page={page_num}'
	async with session.get(url) as resp:
		return list(parse_page(await resp.text()))


def init_args(arg_src = None):
	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--pages', default=3, type=int, help='Number of read pages')
	parser.add_argument('-s', '--sort', default='a', choices='an', help='Sorting mode: [a]verage, [n]one')
	return parser.parse_args(arg_src)


async def main():
	args = init_args()
	async with aiohttp.ClientSession(raise_for_status=True) as session:
		tasks = (get_commands(session, i) for i in range(1, args.pages + 1))
		commands = itertools.chain.from_iterable(await asyncio.gather(*tasks))
		#print('__deb', list(commands))
		if args.sort == 'a':
			commands = sorted(commands, key=lambda c: c.average, reverse=True)

		print(f'{"№":<4}', f'{"Имя":<45}', f'{"Среднее":>10}', f'{"Баллы":>10}', f'{"Игры":>10}', f'{"Позиция":>10}')
		print('-'*10)
		# TODO подумать о более умном задании шапки и печати таблицы вообще
		for i, c in enumerate(commands):
			print(f'{i+1:<4}', f'{c.name:<45}', f'{c.average:>10.3f}', f'{c.points:>10.1f}', f'{c.games:>10}', f'{c.pos:>10}')


if __name__ == '__main__':
	asyncio.run(main())
