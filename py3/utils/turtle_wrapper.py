#!/usr/bin/env python3

# пытаемся расширить функционал turtle
# запускаем из под ipython так
'''
%gui tk
%run utils/turtle_wrapper.py
'''

from turtle import *

import uTkinter as utk
import math


home()


class Extra:
	def __init__(self):
		self.canvas = getcanvas()
		self.screen = getscreen()

		self.controls = []
		for obj in (
			utk.uButton(None, reset, 'Сброс'),
			utk.uCheckbutton(None, 'Догонять', utk.uCallback(self.follow_mode)),
			#add(utk.uButton(None, lambda: self.canvas.config(scrollregion=self.canvas.bbox(utk.ALL)), 'Показать всё')),
		):
			self.controls.append(self.canvas.create_window((0, 0), window = obj))

		self.canvas.bind("<Configure>", self.configure)
		self.configure(None)	#?

	def configure(self, ev):
		height = ev.height if ev else self.canvas.winfo_height()
		x = self.canvas.canvasx(0) + 60
		y = self.canvas.canvasy(height) - 30
		for i, btn_id in enumerate(self.controls):
			self.canvas.coords(btn_id, (x+i*100, y))

	def follow_mode(self, ch_btn):
		def follow(x, y):
			x0, y0 = position()
			setheading(math.atan2(y-y0, x-x0)*180/math.pi)
			goto(x, y)
		if ch_btn.getVar():
			self.screen.onclick(follow)
		else:
			self.screen.onclick(None)

e = Extra()


def run_from_ipython():
	try:
		__IPYTHON__
	except NameError:
		return False
	return True


if not run_from_ipython():
	mainloop()
